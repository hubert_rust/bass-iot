package com.bx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

//@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"com.bx"})
public class SimpleColdApplication {
    public static void main(String[] args) {
        SpringApplication.run(SimpleColdApplication.class, args);
    }
}
