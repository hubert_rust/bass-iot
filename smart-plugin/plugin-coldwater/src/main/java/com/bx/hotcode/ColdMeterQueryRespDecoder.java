package com.bx.hotcode;

import com.bx.utils.ByteUtil;
import com.bx.utils.CharacterUtil;
import com.bx.utils.StrUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-11 16:49
 **/
public class ColdMeterQueryRespDecoder {
    private static final Logger log = LoggerFactory.getLogger(ColdMeterQueryRespDecoder.class);

    public static Map queryResp(byte[] bytes,
                                int iMeterTypePos,
                                int iDataLenPos,
                                byte[] meter_diser_cmd_data,
                                byte[] meter_cmd_data,
                                int iHeadPos,
                                int iMeterAppDataLen,
                                Map map) {
        byte fbyte_enum_hot_meter = (byte) 0x50;

        // 冷水抄表
        if (bytes[iDataLenPos + 1] == (byte)0x1F
                && bytes[iDataLenPos + 2] == (byte)0x90) {

            if (iMeterAppDataLen != (byte)0x16) {
                log.error(">>> ColdMeterQueryRespDecoder, queryResp, iMeterAppDataLen: {}", iMeterAppDataLen);
                map.put("result", "FAIL");
                return null;
            }
            byte index_trans_head_offset_meter_type = 1;
            rcv_read_amount_resp_proc(meter_cmd_data, bytes[iHeadPos + index_trans_head_offset_meter_type],map);
            log.info(">>> ColdMeterQueryRespDecoder, queryResp, rcv_read_amount_resp_proc: {}",
                    StrUtil.splitSpace(CharacterUtil.bytesToHexString(bytes)));
        }
        else if (bytes[iDataLenPos + 1] == (byte)0x11
                && bytes[iDataLenPos + 2] == (byte)0x82) {
            //总充值量
            rechargeAmountResp(meter_cmd_data, iMeterAppDataLen, map);
            log.info(">>> ColdMeterQueryRespDecoder, queryResp, rechargeAmountResp: {}",
                    StrUtil.splitSpace(CharacterUtil.bytesToHexString(bytes)));
        }

        return null;
    }

    //冷水查询充值总量
    public static void rechargeAmountResp(
            byte[] meter_cmd_data,
            int iMeterAppDataLen,
            Map map) {
        byte[] arb_rcv_app_data_resp = { // 0x11, (byte) 0x82,/*
                // 命令 */0x01,/* seq
                // */
                0x01,/* 报警剩余吨数 */0x00,/* 关阀时刻 */
                0x25, 0x00, 0x01, 0x00,/* 用量模式下的总量 */
                0x01,/* 用量模式 */};
        if (iMeterAppDataLen - 3 != arb_rcv_app_data_resp.length) {
            map.put("result", "FAIL");
        }
        int index_meter_appdata_amount_limit_start = 2;
        int len_meter_appdata_amount_limit = 4;
        String amount = "";
        amount = ByteUtil.bytes2str(Arrays.copyOfRange(
                meter_cmd_data,
                index_meter_appdata_amount_limit_start,
                index_meter_appdata_amount_limit_start
                        + len_meter_appdata_amount_limit),
                false, 2);
        map.put("useTotalRecharge", amount);
        map.put("unit", "立方米");
        map.put("result", "SUCCESS");
    }


    // 查询水表用量 返回数据分析
    public static boolean rcv_read_amount_resp_proc(byte[] bytes,
                                                    byte water_meter_type, Map<String, Object> data) {
        /* 冷水表查询用量响应 */
        byte[] arb_data_rcv = { 0x60, 0x00, 0x00, 0x00,
                0x2C, // /*仪表数据，0x2C表示立方米*/
                0x00, 0x00, 0x00, 0x00, 0x2C, 0x48, 0x01, 0x15, 0x13, 0x01,
                0x18, 0x20, /* 日期时间 */
                (byte) 0x80, /*
							 * bit1~bit0:
							 * 00-阀门打开，01-阀门关闭，11-阀门故障；bit2:1-电池欠压，0-正常
							 */
                0x00 };

        byte index_water_meter_amount = 0;
        byte index_water_meter_amount_len = 4;
        byte index_water_meter_amount_unit = (byte) (index_water_meter_amount + index_water_meter_amount_len);
        byte index_water_meter_amount_remain = (byte) (index_water_meter_amount_unit + 1);
        byte index_water_meter_amount_remain_len = 5;
        byte index_water_meter_datetime = (byte) (index_water_meter_amount_remain + index_water_meter_amount_remain_len);
        byte index_water_meter_datetime_len = 7;
        byte index_water_meter_valve_state = (byte) (index_water_meter_datetime + index_water_meter_datetime_len);
        byte index_water_valve_state_offset = 0;

        String amount = "";
        byte fbyte_enum_hot_meter = (byte)0x50;
        if (fbyte_enum_hot_meter == water_meter_type) {
            amount = ByteUtil.bytes2str(Arrays.copyOfRange(bytes,
                    index_water_meter_amount, index_water_meter_amount
                            + index_water_meter_amount_len), false, 1);
            data.put("useTotalAmount", amount);
            if (0x2C == bytes[index_water_meter_amount_unit]) {
                data.put("unit", "升");
            } else {
                data.put("unit", "未知：" + bytes[index_water_meter_amount_unit]);
            }
        } else {
            amount = ByteUtil.bytes2str(Arrays.copyOfRange(bytes,
                    index_water_meter_amount, index_water_meter_amount
                            + index_water_meter_amount_len), false, 2);
            data.put("useTotalAmount", amount);
            if (0x2C == bytes[index_water_meter_amount_unit]) {
                data.put("unit", "立方米");
            } else {
                data.put("unit", "未知：" + bytes[index_water_meter_amount_unit]);
            }
        }

        data.put("当前时间", ByteUtil.bytes2str(Arrays.copyOfRange(bytes,
                index_water_meter_datetime, index_water_meter_datetime
                        + index_water_meter_datetime_len), false, 0));

        String sValveState = new String();
        byte bState = (byte) (bytes[index_water_meter_valve_state] & 0x3);
        if (0 == bState) {
            sValveState = "open";
        } else if (1 == bState) {
            sValveState = "close";
        } else if (3 == bState) {
            sValveState = "fault";
        } else {
            sValveState = "other";
        }

        String sBatteryState = new String();
        byte bBatteryState = (byte) (bytes[index_water_meter_valve_state] & 0x4); // bit
        // 2
        if (4 == bBatteryState) {
            sBatteryState = "欠压";
        } else {
            sBatteryState = "正常";
        }

        data.put("gateState", sValveState);
        data.put("meterState", sBatteryState);

        return true;
    }

}