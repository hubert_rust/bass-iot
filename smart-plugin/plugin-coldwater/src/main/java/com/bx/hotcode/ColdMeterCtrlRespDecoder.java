package com.bx.hotcode;

import com.bx.utils.CharacterUtil;
import com.bx.utils.StrUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-11 16:48
 **/
public class ColdMeterCtrlRespDecoder {
    private static final Logger log = LoggerFactory.getLogger(ColdMeterCtrlRespDecoder.class);

    public static void ctrlResp(byte[] bytes, int iDataLenPos, Map map) {
        /* 开关阀响应 */
        if (bytes[iDataLenPos] == (byte)0x05
                && bytes[iDataLenPos + 1] == (byte)0x17
                && bytes[iDataLenPos + 2] == (byte)0xA0) {
            map.put("cmd", "开阀");
        }
        // 冷水充值响应
        else if (bytes[iDataLenPos] == (byte)0x03
                && bytes[iDataLenPos + 1] == (byte)0x10
                && bytes[iDataLenPos + 2] == (byte)0xA1) {
            map.put("cmd", "冷水充值");
        }
        else {
            log.error(">>> ColdMeterCtrlRespDecoder, ctrlResp: {}",
                    StrUtil.splitSpace(CharacterUtil.bytesToHexString(bytes)));
            map.put("result", "FAIL");
            return;
        }

        map.put("result", "SUCCESS");
    }
}
