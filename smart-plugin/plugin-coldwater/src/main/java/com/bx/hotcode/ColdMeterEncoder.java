package com.bx.hotcode;

import com.bx.config.ColdMsgByteTplReg;
import com.bx.plugin.CodeHandle;
import com.bx.plugin.CodeService;
import com.bx.plugin.HDType;
import com.bx.plugin.ISendMessageEncode;
import com.bx.regcode.CodeHandleRegister;
import com.bx.utils.ByteUtil;
import com.bx.utils.CharacterUtil;
import com.bx.utils.StrUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.smart.command.*;
import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/*
 * @ program: plugin-hotwater
 * @ description: d
 * @ author: admin
 * @ create: 2019-04-03 09:40
 **/
@CodeService(hdType= HDType.HDTYPE_COLD_METER, dir=HDType.MSG_DIR_SEND)
public class ColdMeterEncoder implements ISendMessageEncode {
    private static final Logger LOGGER = LoggerFactory.getLogger(ColdMeterEncoder.class);

    @CodeHandle(name="codeHandle")
    public byte[] getOpen(String str) {
        return  null;
    }

    @PostConstruct
    public void init() {
        System.out.println();
    }

    /**
     * @ Description: 发送消息到硬件, 根据commandCode参数(命令),获取到字节码
     * @ Author: Admin
    **/
    @Override
    public byte[] getSendMessage(String commandCode,
                                   String conAddress,
                                   String meterAddress,
                                   byte frameSeq,
                                   Map map) throws Exception{
        byte[] byteTpl = null;
        try {
            LOGGER.info(">>> ColdMeterEncoder, getSendMessageByte.");
            int bps = Integer.valueOf(map.get("bps").toString());
            int commPort = Integer.valueOf(map.get("commPort").toString());
            switch (commandCode) {
                case CommandCode.CMD_TIME_SYNC: {
                    SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
                    String dateStr = df.format(new Date());
                    byte[] dateByte = CharacterUtil.hexString2ByteslowHigh(dateStr);
                    byteTpl = ColdMsgByteTplReg.coldMeterSyncTime(conAddress,
                            meterAddress,
                            dateByte,
                            frameSeq);
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                case CommandCode.CMD_ON: {
                    byteTpl = ColdMsgByteTplReg.meterOpenClose(conAddress, meterAddress, true, frameSeq);
                    LOGGER.info(">>> ColdMeterEncoder, getSendMessage, CMD_ON: {}",
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                case CommandCode.CMD_OFF: {
                    byteTpl = ColdMsgByteTplReg.meterOpenClose(conAddress, meterAddress, false, frameSeq);
                    LOGGER.info(">>> ColdMeterEncoder, getSendMessage, CMD_OFF: {}",
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                //冷水，
                //@Deprecated
                case CommandCode.CMD_QUERY_UNIT_PRICE: {
                    byteTpl = ColdMsgByteTplReg.queryUnitPrice(conAddress, meterAddress, frameSeq);
                    LOGGER.info(">>> ColdMeterEncoder, getSendMessage, CMD_QUERY_UNIT_PRICE: {}",
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                case CommandCode.CMD_QUERY_METER_AMOUNT: {
                    //冷水总用量
                    byteTpl = ColdMsgByteTplReg.queryMeterAmount(conAddress, meterAddress, frameSeq);
                    LOGGER.info(">>> ColdMeterEncoder, getSendMessage, CMD_METER_AMOUNT: {}",
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                case CommandCode.CMD_QUERY_METER_RECHARGE_ALL: {
                    //查询冷水总充值量
                    //如果要查询冷水的剩余量，需要先查询出冷水的总充值量-冷水的总用量，在平台管理层实现
                    byteTpl = ColdMsgByteTplReg.queryMeterRechargeAll(conAddress, meterAddress, frameSeq);
                    LOGGER.info(">>> ColdMeterEncoder, getSendMessage, CMD_QUERY_METER_RECHARGE_ALL: {}",
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                case CommandCode.CMD_METER_RECHARGE: {

                    BigDecimal bigDecimal = new BigDecimal(map.get("leftAmount").toString());
                    float leftAmout = Float.valueOf(bigDecimal.toString());

                    byteTpl = ColdMsgByteTplReg.coldMeterRecharge(conAddress,
                            meterAddress,
                            "cold-meter",
                            leftAmout,
                            frameSeq);
                    LOGGER.info(">>> ColdMeterEncoder, getSendMessage, CMD_METER_RECHARGE: {}",
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }

                default: {
                    LOGGER.error(">>> ElecMeterEncoder, getSendMessage, commandCode invalid: {}",
                            commandCode);
                    break;
                }
            }
        } catch (Exception e) {
           LOGGER.error(">>> ElecMeterEncoder, getSendMessage, e: {}", e.getMessage());
        }

        return byteTpl;
    }

}
