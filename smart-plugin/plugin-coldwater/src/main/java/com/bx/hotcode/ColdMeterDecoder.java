package com.bx.hotcode;

import com.bx.PluginContainer;
import com.bx.constants.MsgConst;
import com.bx.message.inside.IMessage;
import com.bx.message.inside.MsgCommon;
import com.bx.message.msgtpl.MsgByteTpl;
import com.bx.plugin.CodeService;
import com.bx.plugin.HDType;
import com.bx.plugin.IRespMessageDecode;
import com.bx.utils.CharacterUtil;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Map;

import static com.bx.utils.ByteUtil.calc_sum_crc;

/*
 * @ program: plugin-hotwater
 * @ description: 热水表消息解码器
 * @ author: admin
 * @ create: 2019-04-03 14:35
 **/
@CodeService(hdType=HDType.HDTYPE_COLD_METER, dir=HDType.MSG_DIR_RESP)
public class ColdMeterDecoder implements IRespMessageDecode {
    private static final Logger log = LoggerFactory.getLogger(ColdMeterDecoder.class);

    @Override
    public IMessage getRespMessageObj(byte[] resp, String respMsg) {
        IMessage map = null;
        switch (respMsg) {
            case MsgConst.MSG_TYPE_LOGIN: {
                map = getLogin(resp);
                break;
            }
            case MsgConst.MSG_TYPE_BEAT: {
                map = getBeat(resp);
                break;
            }
            case MsgConst.MSG_TYPE_RESP: {
                map = com.bx.hotcode.ColdMeterRespDecoder.respMessageProc(resp);
                break;
            }
            default: {
                break;
            }
        }
        return map;
    }


    private IMessage getNormal(byte[] resp) {
        return null;
    }

    private MsgByteTpl.ByteWrapper getLoginResp(byte[] bytes) {
        //byte[] arb_send_data_login_resp = { 0x68, 0x31, 0, 0x31, 0, 0x68, 0x0B,
        //        0x49, 0, 1, 0, 0, 0, 0x6B, 0, 0, 1, 0, (byte) 0xC1, 0x16 };
        byte[] wrapper = MsgByteTpl.getByteTplByMsg(MsgConst.MSG_TYPE_LOGIN_RESP, true);
        System.arraycopy(bytes, 7, wrapper, 7, 5);
        wrapper[13] = bytes[13];
        wrapper[wrapper.length - 2] = calc_sum_crc(wrapper, 6,wrapper.length - 3);
        return new MsgByteTpl.ByteWrapper(wrapper);
    }
    private IMessage getLogin(byte[] resp) {
        //登录需要校验集中器是否已经配置
        byte[] address = Arrays.copyOfRange(resp, 7,(7 + 5));
        String hexAddress = CharacterUtil.bytesToHexStringlowHigh(address);

        log.info(">>> ColdMeterDecode, getLogin, hexAddress: {}", hexAddress);
        if (!PluginContainer.getHubMap().containsKey(hexAddress)) {
            log.error(">>> ColdMeterDecode, getLogin, not config address: {}", hexAddress);
            return null;
        }
        else {
            MsgByteTpl.ByteWrapper wrapper = getLoginResp(resp);
            MsgCommon iMessage = MsgCommon.MsgCommonBuilder.aMsgCommon()
                    .withMsgType(MsgConst.MSG_TYPE_LOGIN_RESP)
                    .withMsgAddr(hexAddress)
                    .withState("ok")
                    .withByteWrapper(wrapper)
                    .build();
            return iMessage;
        }
    }

    private MsgByteTpl.ByteWrapper getBeatResp(byte[] bytes) {
        byte[] wrapper = MsgByteTpl.getByteTplByMsg(MsgConst.MSG_TYPE_BEAT_RESP, true);

        System.arraycopy(bytes, 7, wrapper, 7, (7+5));
        wrapper[13] = bytes[13];
        wrapper[wrapper.length - 2] = calc_sum_crc(wrapper, 6,wrapper.length - 3);
        return new MsgByteTpl.ByteWrapper(wrapper);
    }
    private IMessage getBeat(byte[] resp) {
        Map<String, Object> map = Maps.newHashMap();
        IMessage ret = MsgConst.getMessage(MsgConst.MSG_BODY_TYPE_COMMON);
        byte[] address = Arrays.copyOfRange(resp, 7, (7 + 5));
        String hexAddress = CharacterUtil.bytesToHexStringlowHigh(address);

        log.info(">>> ColdMeterDecode, getBeat, hexAddress: {}", hexAddress);
        if (!PluginContainer.getHubMap().containsKey(hexAddress)) {
            log.error(">>> ColdMeterDecode, getLogin, not config address: {}", hexAddress);
            return null;
        }
        else {
            MsgByteTpl.ByteWrapper wrapper = getBeatResp(resp);
            MsgCommon iMessage = MsgCommon.MsgCommonBuilder.aMsgCommon()
                    .withMsgType(MsgConst.MSG_TYPE_BEAT_RESP)
                    .withMsgAddr(hexAddress)
                    .withState("ok")
                    .withByteWrapper(wrapper)
                    .build();
            return iMessage;
        }
    }

}
