package com.bx.hotcode;

import com.alibaba.fastjson.JSONObject;
import com.bx.config.LockerInitConfig;
import com.bx.constants.MsgConst;
import com.bx.enginecore.ConcentratorContextInterface;
import com.bx.enginecore.ConcentratorHolder;
import com.bx.message.inside.MsgCommon;
import com.bx.stream.StreamContainerWrap;
import com.bx.utils.HttpUtils;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.smart.EventType;
import com.smart.command.CommandCode;
import com.smart.common.PFConstDef;
import com.smart.common.ResponseEntity;
import com.smart.common.ResponseUtils;
import com.smart.common.ResultConst;
import com.smart.request.BassHardwardRequest;
import io.netty.buffer.ByteBuf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.IdGenerator;

import java.io.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static com.smart.common.ResponseUtils.getFailResponse;
import static com.smart.common.ResponseUtils.getResponseBody;
import static com.smart.common.ResponseUtils.getSuccessResponse;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-07-04 11:06
 **/

@Component
public class LockerUpgradeProcess {
    private static final Logger log = LoggerFactory.getLogger(LockerUpgradeProcess.class);
    private static final String lockerPackage = "LOCKAPP.bin";    //升级集中器软件包
    private static final String lockerSysPackage = "LOCKSYSAPP.bin";    //升级集中器软件包
    private static final String hubPackage = "LockMaster.bin";
    private static final String hubSysPackage = "LockSYSMaster.bin";
    public static Map<Integer, String> UPGRADE_FILE_MAP = null;

    //1,3表示正在升级hub, 2,4表示正在升级locker
    public AtomicInteger UPGRADE_FLAG = new AtomicInteger(0);
    public String UPGRADE_HUBADDR = "";
    public String UPGRADE_SUBADDR = "";

    static {
        UPGRADE_FILE_MAP = Maps.newHashMap();
        UPGRADE_FILE_MAP.put(1, hubPackage);
        UPGRADE_FILE_MAP.put(2, lockerPackage);
        UPGRADE_FILE_MAP.put(3, hubSysPackage);
        UPGRADE_FILE_MAP.put(4, lockerSysPackage);
    }

    /**
    * @Description: 升级理论上可以不断从头开始（08命令）
    * @Param:
    * @return:
    * @Author: admin
    * @Date: 2019/7/8
    */
    public String checkValid(BassHardwardRequest request) throws Exception {
        synchronized (this) {
            if (request.getCmdCode().equals(CommandCode.CMD_LOCK_UPDATE_SOFTWARE)) {
                if (UPGRADE_FLAG.get() <=0) {
                    return null;
                }
                else {
                    return getResponseBody()
                            .put(PFConstDef.RESP_RETURN_CODE, PFConstDef.RESP_RETURN_CODE_FAIL)
                            .put(PFConstDef.RESP_RETURN_MSG, "操作失败: 集中器正在升级, 稍后重试")
                            .getResponseBody();
                }
            }
            String hubAddr = request.getHubAddress();
            String subAddr = request.getSubAddress();
            int flag = UPGRADE_FLAG.get();
            if (flag == CommandCode.UPGRADE_TYPE_NO) {
                return null;
            }

            if (flag == CommandCode.UPGRADE_TYPE_HUB) {
                if (hubAddr.equals(UPGRADE_HUBADDR)) {
                    return getResponseBody()
                            .put(PFConstDef.RESP_RETURN_CODE, PFConstDef.RESP_RETURN_CODE_FAIL)
                            .put(PFConstDef.RESP_RETURN_MSG, "操作失败: 集中器正在升级, 稍后重试")
                            .getResponseBody();
                }
            } else if (flag == CommandCode.UPGRADE_TYPE_LOCKER) {
                if (subAddr.equals(UPGRADE_SUBADDR) && hubAddr.equals(UPGRADE_HUBADDR)) {
                    return getResponseBody()
                            .put(PFConstDef.RESP_RETURN_CODE, PFConstDef.RESP_RETURN_CODE_FAIL)
                            .put(PFConstDef.RESP_RETURN_MSG, "操作失败: 锁正在升级, 稍后重试")
                            .getResponseBody();
                }
            }
            return null;

        }
    }
    private String prepareUpgrade(BassHardwardRequest request) throws Exception {
        Map<String, Object> datas = (Map)request.getEntitys().get(0);

        Object obj = (datas.get("upgradeType"));
        Preconditions.checkArgument(!Objects.isNull(obj), "升级参数: upgradeType未指定");

        int upgradeType = Integer.valueOf(obj.toString());
        Preconditions.checkArgument((upgradeType == 1 || upgradeType == 2),
                "升级参数: upgradeType is 1表示升级集中器, upgradeType is 2表示升级锁");
        obj = (datas.get("meterAddress"));
        Preconditions.checkArgument(!Objects.isNull(obj), "entitys中未知道meterAddress字段");
        String meterAddress = obj.toString();
        if (upgradeType == CommandCode.UPGRADE_TYPE_HUB) {
            if (!meterAddress.equals("00")) {
                throw  new Exception("升级集中器软件包, 锁地址meterAddress必须为00");
            }
        } else {
            if (meterAddress.equals("00") || meterAddress.equals("FF")) {
                throw new Exception("升级锁软件包, 锁地址非法");
            }
        };

        String shpath = System.getProperty("user.dir");
        String fileName = UPGRADE_FILE_MAP.get(upgradeType);
        Preconditions.checkArgument(!Strings.isNullOrEmpty(fileName),
                "升级软件包名称未定义");
        shpath += (File.separator + fileName);
        File file = new File(shpath);
        Preconditions.checkArgument(file.exists(), "未找到升级软件包");
        return null;
    }
    public String startUpgrade(BassHardwardRequest request) throws Exception {
        Object resp = null;
        LocalDateTime localDateTime = LocalDateTime.now();
        String mainOrder = request.getCommandOrder();
        //升级前准备
        try {
            resp = prepareUpgrade(request);
            if (resp != null) {
                return getFailResponse((String)resp);
            }
        } catch (Exception e) {
            e.printStackTrace();
            resp = getFailResponse(e.getMessage());
            return (String)resp;
        }
        //发送升级命令
        try {
            resp = sendStartUpgradeCmd(request, mainOrder);
            boolean ret = release(request, resp,  mainOrder, localDateTime, false);
            if (! ret) {
                return (String)resp;
            }
        } catch (Exception e) {
            e.printStackTrace();
            resp = getFailResponse(e.getMessage());
            boolean ret = release(request, resp,  mainOrder, localDateTime, false);
            if (! ret) {
                return (String)resp;
            }
        }

        //发送升级数据命令
        try {
            resp = sendUpgradeData(request, mainOrder);
            boolean ret = release(request, resp, mainOrder, localDateTime, false);
            if (! ret) {
                return (String)resp;
            }
        } catch (Exception e) {
            e.printStackTrace();
            resp = getFailResponse(e.getMessage());
            boolean ret = release(request, resp, mainOrder, localDateTime, false);
            if (! ret) {
                return (String)resp;
            }
        }

        //发送升级结束命令
        try {
            resp = sendUpgradeFinishCmd(request, mainOrder);
        } catch (Exception e) {
            e.printStackTrace();
            resp = getFailResponse(e.getMessage());
        }

        release(request, resp, mainOrder, localDateTime, true);
        return (String)resp;
    }

    private Object sendUpgradeFinishCmd(BassHardwardRequest request,
                                        String mainOrder) throws Exception {
        Object resp = null;
        try {
            resp = postCommand(request,
                    mainOrder,
                    CommandCode.CMD_LOCK_UPDATE_SOFTWARE,
                    "02",
                    String.valueOf("finish"));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseUtils.getFailResponse(" e: " + e.getMessage());
        }
        return (String) resp;
    }
    private Object sendStartUpgradeCmd(BassHardwardRequest request,
                                       String mainOrder) throws Exception {
        Object resp = null;
        String upgradeType;
        BassHardwardRequest send = new BassHardwardRequest();
        List<Map<String, Object>> reqList = request.getEntitys();
        if (Objects.isNull(reqList) || reqList.size() <= 0) {
            return getFailResponse("操作失败: entitys 字段非法");
        } else {
            Object reqObj = reqList.get(0).get("upgradeType");
            if (Objects.isNull(reqObj) || Strings.isNullOrEmpty(reqObj.toString())) {
                return getFailResponse("操作失败: upgradeType 字段非法(1:集中器应用软件, 2:锁应用软件)");
            }
            upgradeType = reqObj.toString();
            //设置升级标志
            UPGRADE_FLAG.set(Integer.valueOf(upgradeType));
            UPGRADE_HUBADDR = request.getHubAddress();
            UPGRADE_SUBADDR = (Integer.valueOf(upgradeType) == 1 || Integer.valueOf(upgradeType) == 3) ? "00" : request.getSubAddress();
            log.info(">>> LockerServiceImpl, sendStartUpgradeCmd, upgradeType: {}", upgradeType);
        }

        try {
            resp = postCommand(request,
                    mainOrder,
                    CommandCode.CMD_LOCK_UPDATE_SOFTWARE,
                    CommandCode.UPGRADE_STATE_BEGIN,
                    "0");
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }

        return resp;
    }

    /**
     * @Description: 升级集中器的时候锁地址是00
     * @Param:
     * @return:
     * @Author: admin
     * @Date: 2019/7/2
     */
    private Object postCommand(BassHardwardRequest request,
                               String mainOrder,
                               String command,
                               String hexStr,
                               String index) throws Exception {
        LocalDateTime localDateTime = LocalDateTime.now();
        Object resp = null;
        BassHardwardRequest send = new BassHardwardRequest();
        send = request;
        send.setCommandOrder(mainOrder + "-" + index);
        send.setCmdCode(command);
        send.getEntitys().clear();
        List<Map<String, Object>> list = Lists.newArrayList();
        Map<String, Object> map = Maps.newHashMap();
        list.add(map);
        map.put("hubAddress", UPGRADE_HUBADDR);
        map.put("meterAddress", UPGRADE_SUBADDR);
        map.put("content", hexStr);
        if (hexStr.equals(CommandCode.UPGRADE_STATE_FINISH)) {
            map.put("upgradeState", CommandCode.UPGRADE_STATE_FINISH);
        }
        send.setEntitys(list);

        ConcentratorContextInterface contextInterface = (ConcentratorContextInterface)
                ConcentratorHolder.getConMap().get(request.getHubAddress());
        if (!contextInterface.getConState()) {
            //throw new Exception("hubAddress: "+ conAddr + " state is fault");
        }

        log.info(">>> LockerUpgradeProcess,starting in queue, {}", System.currentTimeMillis());
        CountDownLatch countDownLatch = null;
        try {
            countDownLatch = ConcentratorHolder
                    .putRequest(request.getCmdType(),
                            request.getCmdCode(),
                            request.getHubAddress(),
                            request.getSubAddress(),
                            send.getCommandOrder(),
                            map);

        } catch (Exception e) {
            e.printStackTrace();
            return getFailResponse(e.getMessage());
        }

        long tm = command.equals(CommandCode.CMD_LOCK_UPDATE_SOFTWARE) ? 18 : 8;
        log.info(">>> PluginFuncProcess, process, tm: {}", tm);
        countDownLatch.await(tm, TimeUnit.SECONDS);

        //等待响应
        return resultProcess(send, localDateTime);
    }
    private String resultProcess(BassHardwardRequest req,
                                 LocalDateTime localDateTime) throws Exception {
        MsgCommon respData = (MsgCommon) ConcentratorHolder.getRespData(req.getHubAddress(), req.getCommandOrder());
        ResponseEntity<Map<String, Object>> resp = new ResponseEntity<>();
        resp.setCommandOrder(req.getCommandOrder());

        if (respData == null) {
            log.error(">>> resultProcess, ");
            return "";
        }

        int isRetReqBytes = req.getIsRetSendBytes();
        int isRetRespBytes = req.getIsRetRespBytes();

        //判断状态
        String state = respData.state();
        if (state == MsgConst.MSG_STATE_RESP) {
            resp.setReturnCode("SUCCESS");
            resp.setReturnMsg("OK");
            //正常响应release

            Map<String, Object> byteMap = Maps.newHashMap();
            resp.setRetBytes(byteMap);
            if (isRetReqBytes > 0) { byteMap.put("req", respData.getReqStr()); }
            if (isRetRespBytes> 0) { byteMap.put("resp", respData.getRespStr()); }

            //将响应结果保存
            List<Map<String, Object>> data = (List<Map<String, Object>>)respData.getResp();
            resp.getDatas().addAll(data);
            resp.setResultCode((Objects.nonNull(data) && data.size()>0) ? "SUCCESS" : "FAIL");
            resp.setResultMsg((Objects.nonNull(data) && data.size()>0) ? "OK" : "no response data");
            //这里实际就是回响应返回给管理层的时间
            respData.upMsgTm();
            resp.setMsgMonitor(respData.getMsgMonitor());

            //获取数据返回, 将返回结果map转化为String,
            String var = JSONObject.toJSONString(resp);//getRespString(true, "", resp);
            //最后清理
            ConcentratorHolder.sendReleaseMessage(req.getHubAddress(),
                    req.getCommandOrder(),
                    MsgConst.MSG_TYPE_RELEASE);
            log.info(">>> resultProcess, normal relase, conAddress: {}, commondOrder: {}",
                    req.getHubAddress(), req.getCommandOrder());

            saveJobLog(req, req.getCommandOrder(), localDateTime, ResultConst.SUCCESS, var);
            //返回String
            return var;
        }
        else if (state == MsgConst.MSG_STATE_READY) {
            //返回超时, 发送消息到
            if (respData != null) {
                //重新获取下
            }
            resp.setReturnCode("SUCCESS");
            resp.setReturnMsg("OK");
            resp.setResultCode("FAIL");
            resp.setResultMsg("response timeout");

            Map<String, Object> byteMap = Maps.newHashMap();
            resp.setRetBytes(byteMap);
            if (isRetReqBytes > 0) { byteMap.put("req", respData.getReqStr()); }
            if (isRetRespBytes> 0) { byteMap.put("resp", respData.getRespStr()); }
            String var = JSONObject.toJSONString(resp);//getRespString(false, "定时器超时, timeout=5s", resp);
            //最后清理
            ConcentratorHolder.sendReleaseMessage(req.getHubAddress(),
                    req.getCommandOrder(),
                    MsgConst.MSG_CAUSE_RELEASE_TIMEOUT);
            saveJobLog(req, req.getCommandOrder(), localDateTime, ResultConst.FAIL, var);
            return var;
        }
        else {
            resp.setReturnCode(ResultConst.FAIL);
            resp.setReturnMsg(state + "");
            log.error(">>> invalid state: {}", state);
            saveJobLog(req, req.getCommandOrder(), localDateTime, ResultConst.FAIL, JSONObject.toJSONString(resp));
        }
        return JSONObject.toJSONString(resp);
    }


    private boolean release(BassHardwardRequest request,
                            Object resp,
                            String mainOrder,
                            LocalDateTime localDateTime,
                            boolean finish) {
        Map map = (Map)JSONObject.parseObject((String)resp, Map.class);
        ResultConst.ReturnObject ret = ResultConst.checkResult(map);
        if (finish || ret.getCode().equals(ResultConst.FAIL)) {
            UPGRADE_FLAG.set(0);
            UPGRADE_HUBADDR = "";
            UPGRADE_SUBADDR = "";
        }
        saveJobLog(request, mainOrder, localDateTime, ret.getCode(), (String) resp);

        if (finish || ret.getCode().equals(ResultConst.FAIL)) {
            saveMainJobLog(request, mainOrder, ret.getCode(), localDateTime, (String) resp);
        }
        return ret.getCode().equals(ResultConst.SUCCESS) ? true : false;
    }
    private void saveJobLog(BassHardwardRequest request,
                            String mainOrder,
                            LocalDateTime startTime,
                            String result,
                            String remark) {
        Map<String, Object> jobLogEntity = Maps.newHashMap();
        jobLogEntity.put("pluginId", request.getServiceId());
        jobLogEntity.put("hubAddress", request.getHubAddress());
        jobLogEntity.put("mainOrder", mainOrder);
        jobLogEntity.put("commandOrder", request.getCommandOrder());
        jobLogEntity.put("hubType", request.getCmdType());
        jobLogEntity.put("jobHandle", "lockerUpgradeProcess");
        jobLogEntity.put("subAddress", request.getSubAddress());
        jobLogEntity.put("startTime", startTime);

        jobLogEntity.put("createTime", LocalDateTime.now());
        jobLogEntity.put("finishTime", LocalDateTime.now());
        jobLogEntity.put("result", result);
        jobLogEntity.put("remark", remark);

        jobLogEntity.put("mainEvent", EventType.EVENT_TYPE_JOB_LOG);
        StreamContainerWrap.sendEvent(jobLogEntity);
    }

    private void saveMainJobLog(BassHardwardRequest request,
                                String mainOrder,
                                String state,
                                LocalDateTime startTime,
                                String remark) {

        Map<String, Object> mainJobLogEntity = Maps.newHashMap();
        mainJobLogEntity.put("mainOrder", mainOrder);
        mainJobLogEntity.put("startTime", startTime);
        mainJobLogEntity.put("jobHandle", "lockerUpgradeProcess");
        mainJobLogEntity.put("state", state);
        mainJobLogEntity.put("createTime", LocalDateTime.now());
        mainJobLogEntity.put("finishTime", LocalDateTime.now());
        mainJobLogEntity.put("remark", remark);
        mainJobLogEntity.put("mainEvent", EventType.EVENT_TYPE_MAIN_JOB_LOG);
        StreamContainerWrap.sendEvent(mainJobLogEntity);
    }


    private String getFile() throws Exception {
        String shpath = System.getProperty("user.dir");
        String fileName = UPGRADE_FILE_MAP.get(UPGRADE_FLAG.get());
        if (Strings.isNullOrEmpty(fileName)) {
            throw new Exception("升级软件包名称未定义");
        }
        shpath += (File.separator + fileName);
        File file = new File(shpath);
        if (!file.exists()) {
            throw new Exception("未找到升级软件包");
        }
        return shpath;
    }

    private Object sendUpgradeData(BassHardwardRequest request,
                                   String mainOrder) throws Exception {
        Object resp = null;
        String fileName = null;
        try {
            fileName = getFile();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseUtils.getFailResponse(e.getMessage());
        }

        try {
            resp = sendFile(request, fileName, mainOrder);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseUtils.getFailResponse(e.getMessage());
        }

        return resp;
    }

    //发送升级包
    public Object sendFile(BassHardwardRequest request,
                         String upgradeFile,
                         String mainOrder) throws Exception {
        Object resp = null;
        File file = new File(upgradeFile);
        InputStream in = null;
        in = new FileInputStream(file);

        try {
            log.info(">>> 准备开始读取更新包--------------->");
            ByteBuf clientMessage = null;
            StringWriter sw = new StringWriter();

            int cycleNo = 0;
            int len = 1;
            //发送次数
            int sendNo = 1;
            byte[] temp = new byte[len];
            int number = 0;
            /*16进制转化模块*/
            for (; (in.read(temp, 0, len)) != -1; ) {

                String data = Integer.toHexString(temp[0]);
                if (temp[0] > 0xf && temp[0] <= 0xff) {
                    sw.write(data);
                } else if (temp[0] >= 0x0 && temp[0] <= 0xf) {//对于只有1位的16进制数前边补“0”
                    sw.write("0" + data);
                } else { //对于int<0的位转化为16进制的特殊处理，因为Java没有Unsigned int，所以这个int可能为负数
                    sw.write(data.substring(6));
                }
                if (cycleNo == 127) {
                    String xh = Integer.toHexString(number);
                    //序号 对于少于4位的16进制数前边补“0”
                    if (xh.length() != 4) {
                        for (int i = xh.length(); i < 4; i++) {
                            xh = "0" + xh;
                        }
                    }
                    sw.write(xh);
                    String content = String.format("%s", sw);
                    try {
                        resp = postCommand(request,
                                mainOrder,
                                CommandCode.CMD_LOCK_TRANSMIT_DATA,
                                content,
                                String.valueOf(sendNo));

                        Map map = (Map)JSONObject.parseObject((String)resp, Map.class);
                        ResultConst.ReturnObject ret = ResultConst.checkResult(map);
                        if (ret.getCode().equals(ResultConst.FAIL)) {
                            //release();
                            return (String)resp;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        return ResponseUtils.getFailResponse(sendNo + ", e: " + e.getMessage());
                    }
                    log.info("<-----------------第" + sendNo + "次发送升级包内容---------------->:" + content);
                    sw.getBuffer().setLength(0);
                    cycleNo = 0;
                    number++;
                    sendNo++;
                } else {
                    cycleNo++;
                }

            }
            //判断最后一包数据,如果不够128,补齐ff
            if (cycleNo != 0) {
                String sxh = Integer.toHexString(number);
                //序号 对于少于4位的16进制数前边补“0”
                if (sxh.length() != 4) {
                    for (int i = sxh.length(); i < 4; i++) {
                        sxh = "0" + sxh;
                    }
                }
                for (int i = 0; i < 128 - cycleNo; i++) {
                    sw.write("ff");
                }
                sw.write(sxh);
                //校验(数据求和)
                log.info("<-----------------最后一包升级数据---------------->:" + sw.toString());
                //发送

                String content = String.format("%s", sw);
                try {
                    resp = postCommand(request,
                            mainOrder,
                            CommandCode.CMD_LOCK_TRANSMIT_DATA,
                            content,
                            String.valueOf(sendNo));
                    Map map = (Map)JSONObject.parseObject((String)resp, Map.class);
                    ResultConst.ReturnObject ret = ResultConst.checkResult(map);
                    if (ret.getCode().equals(ResultConst.FAIL)) {
                        //release();
                        return (String)resp;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return ResponseUtils.getFailResponse(sendNo + ", e: " + e.getMessage());
                }
            } else {
                //升级包最后一包刚好128,发送升级完成命令
                log.info("<-------------发送升级完成命令------------>");
                /*try {
                    resp = postCommand(request,
                            mainOrder,
                            CommandCode.CMD_LOCK_UPDATE_SOFTWARE,
                            "02",
                            String.valueOf(sendNo));
                    Map map = (Map)JSONObject.parseObject((String)resp, Map.class);
                    ResultConst.ReturnObject ret = ResultConst.checkResult(map);
                    if (ret.getCode().equals(ResultConst.FAIL)) {
                        release();
                        return (String)resp;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return ResponseUtils.getFailResponse(sendNo + ", e: " + e.getMessage());
                }*/
            }
        } catch (Exception e) {
            log.info("<-------------异常---[" + e + "]--------->");
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        List<Map<String, Object>> list = Lists.newArrayList();
        Map map = Maps.newHashMap();
        map.put("result", ResultConst.SUCCESS);
        map.put("desc", "升级成功");
        list.add(map);
        return getResponseBody()
                .put("returnCode", ResultConst.SUCCESS)
                .put("returnMsg", ResultConst.STATE_OK)
                .put("resultCode", ResultConst.SUCCESS)
                .put("resultMsg", ResultConst.STATE_OK)
                .put("datas", list).getResponseBody();
    }

}
