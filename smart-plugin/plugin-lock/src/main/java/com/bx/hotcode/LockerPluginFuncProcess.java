package com.bx.hotcode;

import com.alibaba.fastjson.JSONObject;
import com.bx.PluginContainer;
import com.bx.constants.MsgConst;
import com.bx.enginecore.ConcentratorContextInterface;
import com.bx.enginecore.ConcentratorHolder;
import com.bx.message.inside.MsgCommon;
import com.bx.plugin.CodeService;
import com.bx.plugin.HDType;
import com.bx.service.IPluginFuncProcess;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.smart.command.CommandCode;
import com.smart.common.ResponseEntity;
import com.smart.request.BassHardwardRequest;
import com.smart.utils.MapEntityUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @program: plugin
 * @description: 将业务service独立出来，每个插件一个
 * @author: admin
 * @create: 2019-05-06 14:30
 **/
@Service("lockerFuncProcess")
@Scope("prototype")
@CodeService(hdType = HDType.HDTYPE_LOCK, dir = HDType.MSG_DIR_SERVICE_LOCK)
public class LockerPluginFuncProcess implements IPluginFuncProcess {
    private static final Logger log = LoggerFactory.getLogger(LockerPluginFuncProcess.class);
    //1,3表示正在升级hub, 2,4表示正在升级locker
    public volatile Integer UPGRADE_FLAG = 0;
    public volatile String UPGRADE_HUBADDRESS = "";
    public volatile String UPGRADE_SUBADDRESS = "";

    private ReentrantLock lock = new ReentrantLock(true);

    @Autowired
    LockerUpgradeProcess upgradeProcess;


    private String checkValid(Map<String, Object> map) {

        String conAddr = map.get("hubAddress").toString();
        String cmdOrder = map.get("commandOrder").toString();
        if (map == null || map.size() <= 0 || StringUtils.isEmpty(conAddr) || StringUtils.isEmpty(cmdOrder)) {
            return "请求数据非法";
        }
        if (map.get("hd_type") == null) {
            return "请求数据非法, 没有:hd_type";
        }

        ConcentratorContextInterface con = ConcentratorHolder.conMap.get(conAddr);
        if (con == null) {
            return conAddr + " :获取不到虚拟集中器";
        }
        if (!con.getConState()) {
            return conAddr + " :集中器状态不可用";
        }

        return null;
    }

    public String checkUpgrade(Map<String, Object> map, String subType) throws Exception {
        return null;
    }

    /**
     * @Description: 在发送消息入队列之前, 将消息码流封装好，message loop中直接发送
     * @Param:
     * @return:
     * @Author: admin
     * @Date: 2019/5/8
     */
    @Override
    public String process(Map<String, Object> map, String subType) throws Exception {
        lock.lock();
        Object retObj = null;
        try {
            long in = System.currentTimeMillis();
            ResponseEntity<Map<String, Object>> resp = new ResponseEntity<>();
            String conAddr = map.get("hubAddress").toString();
            String cmdOrder = map.get("commandOrder").toString();
            String cmdCode = map.get("cmdCode").toString();
            String cmdType = map.get("cmdType").toString();

            //超时时间，通过配置获取
            long tm = PluginContainer.configEntity.getPlugin().getCountDownTm();

            Map<String, Object> entity = (Map) ((List) map.get("entitys")).get(0);
            if (Objects.isNull(entity) || entity.size() <= 1) {
                throw new Exception("entitys invalid");
            }

            if (!cmdType.equals("locker")) {
                throw new Exception("cmdType is invalid, cmdType: " + cmdType + ", right cmdType: locker");
            }

            Preconditions.checkArgument(Objects.nonNull(entity.get("meterAddress")),
                    "entitys中未指定meterAddress");
            Preconditions.checkArgument(Objects.nonNull(entity.get("hubAddress")),
                    "entitys中未指定hubAddress");

            String meterAddr = entity.get("meterAddress").toString();
            String hubAddr = entity.get("hubAddress").toString();
            if (!conAddr.equals(hubAddr)) {
                throw new Exception("hubAddress is not match");
            }


            BassHardwardRequest request = null;
            try {
                request = (BassHardwardRequest) MapEntityUtil.mapToObject(map, BassHardwardRequest.class);
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception(e.getMessage());
            }
            String check = upgradeProcess.checkValid(request);
            if (!Strings.isNullOrEmpty(check)) {
                return check;
            }
            if (cmdCode.equals(CommandCode.CMD_LOCK_UPDATE_SOFTWARE)) {
                return upgradeProcess.startUpgrade(request);
            }

            ConcentratorContextInterface contextInterface = (ConcentratorContextInterface)
                    ConcentratorHolder.getConMap().get(conAddr);
            if (contextInterface == null) {
                throw new Exception("获取集中器网关失败");
            }
            if (contextInterface.getBindChannel() == null) {
                throw new Exception("和集中器未建立连接");
            }
            if (!contextInterface.getConState()) {
                throw new Exception("hubAddress: " + conAddr + " state is fault");
            }


            log.info(">>> PluginFuncProcess, process, starting in queue, {}", System.currentTimeMillis());
            CountDownLatch countDownLatch = null;


            //writeLock.lock();
            try {
                countDownLatch = ConcentratorHolder
                        .putRequest(cmdType, cmdCode, conAddr, meterAddr, cmdOrder, entity);

            } catch (Exception e) {
                resp.setReturnCode("FAIL");

                resp.setReturnMsg(e.getMessage());
                return JSONObject.toJSONString(resp);
            } finally {
            }

            //log.info(">>> PluginFuncProcess, process, tm: {}", tm);
            countDownLatch.await(tm, TimeUnit.SECONDS);
            //释放升级资源, 如果升级中断了，需要处理

            log.info(">>> PluginFuncProcess, process, starting out queue, {}", System.currentTimeMillis());

            retObj = resultProcess(entity, cmdCode, conAddr, cmdOrder, map, resp);
            long wait = in - Long.valueOf(map.get("reserved").toString());
            log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>this: {}, wait: {}, timeSpan: {}",
                    this.toString(), wait, System.currentTimeMillis() - in);
        } catch (Exception e) {
            throw new Exception(e.getMessage());

        } finally {
            lock.unlock();
        }
        return (String)retObj;
    }


    private String resultProcess(Map<String, Object> entity,
                                 String cmdCode,
                                 String conAddr,
                                 String cmdOrder,
                                 Map request,
                                 ResponseEntity resp) throws Exception {
        MsgCommon respData = (MsgCommon) ConcentratorHolder.getRespData(conAddr, cmdOrder);
        resp.setCommandOrder(cmdOrder);
        if (respData == null) {
            log.error(">>> resultProcess, ");
            return "";
        }

        int isRetReqBytes = 0;
        int isRetRespBytes = 0;
        Object obj = request.get("isRetSendBytes");
        if (Objects.nonNull(obj)) {
            isRetReqBytes = Integer.valueOf(request.get("isRetSendBytes").toString());
        }
        obj = request.get("isRetRespBytes");
        if (Objects.nonNull(obj)) {
            isRetRespBytes = Integer.valueOf(request.get("isRetRespBytes").toString());
        }

        //判断状态
        String state = respData.state();
        if (state == MsgConst.MSG_STATE_RESP) {

            PluginContainer.sendMeterState(respData.getMsgAddr(),
                    respData.getMsgSubAddr(),
                    "nok");
            resp.setReturnCode("SUCCESS");
            resp.setReturnMsg("OK");
            //正常响应release

            Map<String, Object> byteMap = Maps.newHashMap();
            resp.setRetBytes(byteMap);
            if (isRetReqBytes > 0) {
                byteMap.put("req", respData.getReqStr());
            }
            if (isRetRespBytes > 0) {
                byteMap.put("resp", respData.getRespStr());
            }

            //将响应结果保存
            List<Map<String, Object>> data = (List<Map<String, Object>>) respData.getResp();
            resp.getDatas().addAll(data);
            resp.setResultCode((Objects.nonNull(data) && data.size() > 0) ? "SUCCESS" : "FAIL");
            resp.setResultMsg((Objects.nonNull(data) && data.size() > 0) ? "OK" : "no response data");
            //这里实际就是回响应返回给管理层的时间
            respData.upMsgTm();
            resp.setMsgMonitor(respData.getMsgMonitor());

            //获取数据返回, 将返回结果map转化为String,
            String var = JSONObject.toJSONString(resp);//getRespString(true, "", resp);
            //最后清理
            ConcentratorHolder.sendReleaseMessage(conAddr, cmdOrder, MsgConst.MSG_TYPE_RELEASE);
            //log.info(">>> resultProcess, conAddress: {}, commondOrder: {}", conAddr, cmdOrder);

            //返回String
            return var;
        } else if (state == MsgConst.MSG_STATE_READY) {

            //返回超时, 发送消息到
            if (respData != null) {
                //重新获取下
            }

            PluginContainer.sendMeterState(respData.getMsgAddr(),
                    respData.getMsgSubAddr(),
                    "nok");
            resp.setReturnCode("SUCCESS");
            resp.setReturnMsg("OK");
            resp.setResultCode("FAIL");
            resp.setResultMsg("response timeout");

            Map<String, Object> byteMap = Maps.newHashMap();
            resp.setRetBytes(byteMap);
            if (isRetReqBytes > 0) {
                byteMap.put("req", respData.getReqStr());
            }
            if (isRetRespBytes > 0) {
                byteMap.put("resp", respData.getRespStr());
            }
            String var = JSONObject.toJSONString(resp);//getRespString(false, "定时器超时, timeout=5s", resp);
            //最后清理
            ConcentratorHolder.sendReleaseMessage(conAddr, cmdOrder, MsgConst.MSG_CAUSE_RELEASE_TIMEOUT);
            return var;
        } else {
            resp.setReturnMsg(state + "");
            log.error(">>> invalid state: {}", state);
        }
        return JSONObject.toJSONString(resp);
    }

}
