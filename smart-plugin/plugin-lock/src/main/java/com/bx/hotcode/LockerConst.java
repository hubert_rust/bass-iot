package com.bx.hotcode;

import org.apache.commons.lang.StringUtils;
import org.apache.kafka.common.protocol.types.Field;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-06-14 15:34
 **/
public class LockerConst {
    private static final Logger log = LoggerFactory.getLogger(LockerConst.class);

    //帧头
    public static final String FRAME_HEADER = "AA";
    //帧尾
    public static final String FRAME_END = "55";

    //命令码

    public static final String CMD_CODE_SEND_HEART_BEAT = "00";  //心跳码流 "AA03000001010155"
    public static final String CMD_CODE_HEART_BEAT = "00";       //心跳码流 "AA03000001010155"
    public static final String CMD_CODE_TIME_SYNC = "01";
    public static final String CMD_CODE_SET_ADMIN_PWD = "02";    //最多设置10个
    public static final String CMD_CODE_SET_ADMIN_CARD = "03";   //最多设置10个
    public static final String CMD_CODE_REMOTE_UNLOCK = "04";
    public static final String CMD_CODE_QUERY_LOCK_STATE = "05";
    public static final String CMD_CODE_UP_MESSAGE = "06";       //上行消息
    public static final String CMD_CODE_SET_OFFLINE_SECRETKEY= "07";

    //远程更新软件
    public static final String CMD_CODE_UPDATE_SOFTWARE = "08";
    //传输数据
    public static final String CMD_CODE_TRANSMIT_DATA = "09";

    public static final String CMD_CODE_UPDATE_LOCKER_ADDRLIST = "0A"; //更新锁地址列表
    public static final String CMD_CODE_UPDATE_TEMP_CARD = "0B";
    public static final String CMD_CODE_REMOTE_RESTART_HUB = "0C";
    public static final String CMD_CODE_QUERY_TIME = "0D";

    public static final String CMD_CODE_HEART_BEAT_RESP = "1E";

    public static final String REPLY_DATA_FAIL = "00";
    public static final String REPLY_DATA_SUCCESS = "01";
    public static final String LOCKER_OPEN = "open";
    public static final String LOCKER_CLOSE = "close";

    public static String makeChecksum(String data) {
        if (StringUtils.isEmpty(data)) {
            return "";
        }
        //log.info("<---计算16进制字符串的和--->" + data);
        int total = 0;
        int len = data.length();
        int num = 0;
        while (num < len) {
            String s = data.substring(num, num + 2);
            total += Integer.parseInt(s, 16);
            num = num + 2;
        }
        /*
         * 用256求余最大是255，即16进制的FF
         */
        int mod = total % 256;
        String hex = Integer.toHexString(mod);
        len = hex.length();
        // 如果不够校验位的长度，补0,这里用的是两位校验
        if (len < 2) {
            hex = "0" + hex;
        }
        return hex;
    }

    public static String tensSwitchSixteen(String hexAddress){
        if("FF".equals(hexAddress)){
            return hexAddress;
        }
        String address = Integer.toHexString(Integer.parseInt(hexAddress));
        if(address.length() < 2){
            address = "0" + address;
        }
        return address;
    }


    public static String sixteenSwitchTens(String address){
        if("FF".equals(address)){
            return address;
        }
        String addres = String.valueOf(Integer.parseInt(address,16));
        if(addres.length() < 2){
            addres = "0" + addres;
        }
        return addres;
    }

    //计数数据位的长度
    public static String countDataLength(String data) {
        String dataLength = Long.toHexString(data.length()/2).toUpperCase();
        if(dataLength.length() < 2){
            dataLength = "0" + dataLength;
        }
        return dataLength;
    }


    /**
     * 补齐10个8个字节的密码
     *
     * @return
     */
    public static String setPassword(List<Long> pass) {

        StringBuffer cmdstr = new StringBuffer();
        for (Long pas : pass) {
            String data = Long.toHexString(pas).toUpperCase();
            for (int i = data.length(); i < 8; i++) {
                data = "0" + data;
            }
            cmdstr.append(data);
        }
        int len = cmdstr.length() / 8;
        for (int i = len; i < 10; i++) {
            cmdstr.append("FFFFFFFF");
        }
        return cmdstr.toString();
    }

    public static String setCard(List<String> pass) {

        StringBuffer cmdstr = new StringBuffer();
        for (String pas : pass) {
            for (int i = pas.length(); i < 8; i++) {
                pas = "0" + pas;
            }
            cmdstr.append(pas);
        }
        int len = cmdstr.length() / 8;
        for (int i = len; i < 10; i++) {
            cmdstr.append("FFFFFFFF");
        }
        return cmdstr.toString();
    }

    public static String TimeStamp2Date(String timestampString) {
        Long timestamp = Long.parseLong(timestampString) * 1000;
        String formats = "yyyy-MM-dd HH:mm:ss";
        String date = new SimpleDateFormat(formats).format(new Date(timestamp));
        return date;
    }
    /**
     * 将学生卡号反转
     * @param data
     * @return
     */
    public static String inversionCardNo(String data) {
        if (org.springframework.util.StringUtils.isEmpty(data)) {
            return "";
        }
        StringBuffer total = new StringBuffer();
        int len = data.length();
        int num = 0;
        while (num < data.length()) {
            String s = data.substring(len-2, len);
            total.append(s);
            num = num + 2;
            len = len - 2;
        }
        String hex = total.toString();
        // 如果不够校验位的长度，补0,这里用的是两位校验
        return hex;
    }
}
