package com.bx.hotcode;

import com.alibaba.fastjson.JSONObject;
import com.bx.PluginContainer;
import com.bx.config.LockerInitConfig;
import com.bx.constants.MsgConst;
import com.bx.message.inside.IMessage;
import com.bx.message.inside.MsgCommon;
import com.bx.message.msgtpl.MsgByteTpl;
import com.bx.plugin.CodeService;
import com.bx.plugin.HDType;
import com.bx.plugin.IRespMessageDecode;
import com.bx.stream.StreamContainer;
import com.bx.stream.StreamContainerWrap;
import com.bx.utils.ByteUtil;
import com.bx.utils.CharacterUtil;
import com.bx.utils.StrUtil;
import com.google.common.collect.Maps;
import com.smart.EventType;
import com.smart.command.CommandCode;
import com.smart.common.ResultConst;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Map;

import static com.bx.constants.MsgConst.FAILURE;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: admin
 * @Date: 2019/6/19
 */
@CodeService(hdType = HDType.HDTYPE_LOCK, dir = HDType.MSG_DIR_RESP)
public class LockerDecoder implements IRespMessageDecode {
    private static final Logger log = LoggerFactory.getLogger(LockerDecoder.class);
    private StreamContainer streamContainer = null;

    @Override
    public IMessage getRespMessageObj(byte[] resp, String respMsg) {
        IMessage map = null;

        String cmd = CharacterUtil.bytesToHexString(resp);
        //log.info(">>> LockerDecoder, getRespMessageObj, cmd: {}", cmd);
        //BxLockMessage gateMessage = new BxLockMessage();
        //gateMessage.setCmd(cmd);
        // 获取集中器地址
        String hexAddress = String.format("%02d", (resp[1] & 0xFF));
        // 获取锁地址
        String lockAddress = String.format("%02d", (resp[2] & 0xFF));
        // 获取远程命令
        String command = CharacterUtil.bytesToHexString(resp[3]);
        // 获取接收数据 (回复0为失败，非0为成功)
        String receiveData = CharacterUtil.bytesToHexString(resp[5]);

        String msgCommonType = MsgConst.MSG_TYPE_RESP;
        String hexStr = "";
        // 远程操作指令
        Map<String, Object> data = Maps.newHashMap();
        data.put("hubAddr", hexAddress);
        data.put("lockAddr", lockAddress);
        data.put("commandCode", command);
        data.put("result", MsgConst.SUCCESS);
        switch (command) {
            case LockerConst.CMD_CODE_HEART_BEAT: {
                log.info(">>> LockerDecoder, receive heart");
                //心跳和其他消息处理不同
                //收到集中器发送的心跳消息，返回结果中携带响应消息码流
                msgCommonType = MsgConst.MSG_TYPE_BEAT_RESP;
                String HEARTBEAT = "AA%CONADDR%LOCKADDR0001010155";
                hexStr = HEARTBEAT.replace("%CONADDR", hexAddress)
                        .replace("%LOCKADDR", lockAddress);
                byte[] respByte = CharacterUtil.hexString2Bytes(hexStr);
                MsgByteTpl.ByteWrapper wrapper = new MsgByteTpl.ByteWrapper(respByte);
                MsgCommon iMessage = MsgCommon.MsgCommonBuilder.aMsgCommon()
                        .withMsgType(msgCommonType)
                        .withMsgAddr(hexAddress)
                        .withMsgSubAddr(lockAddress)
                        .withState("ok")
                        .withByteWrapper(wrapper)
                        .build();
                iMessage.setHubType(CommandCode.HUB_TYPE_LOCKER);
                iMessage.getResp().add(data);
                return iMessage;
            }
            case LockerConst.CMD_CODE_QUERY_TIME: {
                if (resp[3] == 0x0D && resp[4] == 0x04) {
                    String hex = CharacterUtil.bytesToHexString(resp[5]);
                    hex += CharacterUtil.bytesToHexString(resp[6]);
                    hex += CharacterUtil.bytesToHexString(resp[7]);
                    hex += CharacterUtil.bytesToHexString(resp[8]);
                    hex = new BigInteger(hex, 16).toString();
                    Long time = Long.valueOf(hex);

                    LocalDateTime dateTime = LocalDateTime.ofEpochSecond(time, 0, ZoneOffset.ofHours(8));

                    data.put("result", ResultConst.SUCCESS);
                    data.put("time", dateTime.toString().replace("T", " "));
                } else {
                    data.put("result", ResultConst.FAIL);
                }
                data.put("desc", CommandCode.CMD_LOCK_GET_TIME);
                break;
            }
            case LockerConst.CMD_CODE_TIME_SYNC: {
                if (!receiveData.equals(LockerConst.REPLY_DATA_SUCCESS)) {
                    data.put("result", FAILURE);
                }
                data.put("desc", CommandCode.CMD_LOCK_TIME_SYNC);
                break;
            }
            case LockerConst.CMD_CODE_SET_ADMIN_CARD: {
                if (!receiveData.equals(LockerConst.REPLY_DATA_SUCCESS)) {
                    data.put("result", FAILURE);
                }
                data.put("desc", CommandCode.CMD_LOCK_SET_ADMIN_CARDNO);
                break;
            }
            case LockerConst.CMD_CODE_SET_ADMIN_PWD: {
                if (!receiveData.equals(LockerConst.REPLY_DATA_SUCCESS)) {
                    data.put("result", FAILURE);
                }
                data.put("desc", CommandCode.CMD_LOCK_SET_ADMIN_PWD);
                break;
            }
            case LockerConst.CMD_CODE_QUERY_LOCK_STATE: {
                if (receiveData.equals("01")) {
                    data.put("state", LockerConst.LOCKER_OPEN);
                } else {
                    data.put("state", LockerConst.LOCKER_CLOSE);
                }

                data.put("desc", CommandCode.CMD_LOCK_QUERY_STATE);
                break;
            }
            case LockerConst.CMD_CODE_UP_MESSAGE: {
                log.info(">>> LockerDecoder, getRespMessageObj, CMD_CODE_UP_MESSAGE: {}", command);
                //发送事件到消息队列
                eventParse(cmd, hexAddress, lockAddress);

                StringBuffer cmdstr = new StringBuffer();
                cmdstr.append(LockerConst.FRAME_HEADER);
                cmdstr.append(hexAddress);
                cmdstr.append(lockAddress);
                cmdstr.append(LockerConst.CMD_CODE_UP_MESSAGE);
                cmdstr.append(LockerConst.CMD_CODE_TIME_SYNC);
                cmdstr.append(LockerConst.CMD_CODE_TIME_SYNC);
                cmdstr.append(LockerConst.CMD_CODE_TIME_SYNC);
                cmdstr.append(LockerConst.FRAME_END);

                msgCommonType = MsgConst.MSG_TYPE_IM_RESP;
                byte[] respByte = CharacterUtil.hexString2Bytes(cmdstr.toString());
                MsgByteTpl.ByteWrapper wrapper = new MsgByteTpl.ByteWrapper(respByte);
                MsgCommon iMessage = MsgCommon.MsgCommonBuilder.aMsgCommon()
                        .withMsgType(msgCommonType)
                        .withMsgAddr(hexAddress)
                        .withMsgSubAddr(lockAddress)
                        .withState("ok")
                        .withByteWrapper(wrapper)
                        .build();
                iMessage.getResp().add(data);
                return iMessage;
            }
            case LockerConst.CMD_CODE_SET_OFFLINE_SECRETKEY: {
                if (!receiveData.equals(LockerConst.REPLY_DATA_SUCCESS)) {
                    data.put("result", FAILURE);
                }
                data.put("desc", CommandCode.CMD_LOCK_SET_OFFLINE_SECRETKEY);
                break;
            }
            case LockerConst.CMD_CODE_UPDATE_LOCKER_ADDRLIST: {
                //更新锁地址列表每天晚上定时同步，增加删除修改配置同步
                if (!receiveData.equals(LockerConst.REPLY_DATA_SUCCESS)) {
                    data.put("result", FAILURE);
                }
                data.put("desc", CommandCode.CMD_LOCK_UPDATE_LOCK_ADDRLIST);
                break;
            }
            case LockerConst.CMD_CODE_UPDATE_TEMP_CARD: {
                //更新临时卡是通过定时任务下发
                if (!receiveData.equals(LockerConst.REPLY_DATA_SUCCESS)) {
                    data.put("result", FAILURE);
                }
                data.put("desc", CommandCode.CMD_LOCK_UPDATE_TEMP_CARD);
                break;
            }
            case LockerConst.CMD_CODE_REMOTE_UNLOCK: {
                if (!receiveData.equals(LockerConst.REPLY_DATA_SUCCESS)) {
                    data.put("result", FAILURE);
                }
                data.put("desc", CommandCode.CMD_LOCK_REMOTE_UNLOCK);
                break;
            }
            case LockerConst.CMD_CODE_REMOTE_RESTART_HUB: {
                if (!receiveData.equals(LockerConst.REPLY_DATA_SUCCESS)) {
                    data.put("result", FAILURE);
                }
                data.put("desc", CommandCode.CMD_LOCK_REMOTE_RESTART_HUB);
                break;
            }
            case LockerConst.CMD_CODE_UPDATE_SOFTWARE: {
                log.info(">>> LockerDecoder, getRespMessageObj, resp: {}", CharacterUtil.bytesToHexString(resp));
                data.put("desc", CommandCode.CMD_LOCK_UPDATE_SOFTWARE);
                break;
            }
            case LockerConst.CMD_CODE_TRANSMIT_DATA: {
                if (!receiveData.equals(LockerConst.REPLY_DATA_SUCCESS)) {
                    data.put("result", FAILURE);
                }
                data.put("desc", CommandCode.CMD_LOCK_REMOTE_RESTART_HUB);
                break;
            }
            default: {
                log.error(">>> LockerDecoder, getRespMessageObj, invalid cmd: {}", cmd);
                break;
            }
        }

        MsgCommon iMessage = MsgCommon.MsgCommonBuilder.aMsgCommon()
                .withMsgType(msgCommonType)
                .withMsgAddr(hexAddress)
                .withMsgSubAddr(lockAddress)
                .withState("ok")
                .build();
        iMessage.setRespStr(StrUtil.splitSpace(CharacterUtil.bytesToHexString(resp)));
        iMessage.getResp().add(data);
        return iMessage;
    }

    /*  处理上报记录
     * 上报记录，A1刷卡开门，A0刷卡未开，B1密码开门，B0密码未开，
     * C1远程开门（无卡号密码），C0远程未开（无卡号密码），D0机械钥匙（或异常）开门，E0关门（无卡号密码）
     *   AA 01 01 06 09 A1 00 00 00 00 00 00 00 00 00 55
     */
    private void eventParse(String cmd, String hexAddress, String lockAddress) {
        if (cmd.length() <= 16) {
            log.error(">>> LockerDecoder, eventParse, up cmd invalid: {}", cmd);
            return;
        }
        Map<String, Object> evt = Maps.newHashMap();
        try {

            //类型
            String type = cmd.substring(10, 12);
            //时间
            String unixSixteen = cmd.substring(12, 20);
            String unixTen = String.valueOf(Integer.parseInt(unixSixteen, 16));
            String time = LockerConst.TimeStamp2Date(unixTen);
            String password = null;
            if (!"C1".equals(type) && !"C0".equals(type) && !"E0".equals(type) && !"D0".equals(type)) {
                if ("B1".equals(type) || "B0".equals(type)) {
                    //密码
                    String passwordSixteen = cmd.substring(20, 28);
                    //16进制转为10进制
                    password = String.valueOf(Integer.parseInt(passwordSixteen, 16));
                    evt.put("password", password);
                } else {
                    //刷卡
                    String passwordSixteen = cmd.substring(20, 28);
                    //卡号反转
                    password = LockerConst.inversionCardNo(passwordSixteen);
                    evt.put("cardNo", password);
                }
            }
            String decodeDate = time.substring(0, 10);
            //拼成这种数据格式9:00-10:00
            String startTime = time.substring(11, 13);
            StringBuilder timeUnit = new StringBuilder(startTime);
            timeUnit.append(":00-");
            timeUnit.append(Integer.parseInt(String.valueOf(startTime)) + 1);
            timeUnit.append(":00");
            //获取琴房编号
            //hexAddress = LockerConst.sixteenSwitchTens(hexAddress);
            //lockAddress = LockerConst.sixteenSwitchTens(lockAddress);
            //String pianoNo = lockDataHandle.selectPianoNo(hexAddress,lockAddress);
            String userState = "正在使用";
            String userNumber = null;
            switch (type) {
                case "A0":
                    type = "刷卡未开";
                    break;
                case "A1":
                    type = "刷卡开门";
                    break;
                case "B0":
                    type = "密码未开";
                    break;
                case "B1":
                    type = "密码开门";
                    break;
                case "C0":
                    type = "远程未开";
                    break;
                case "C1":
                    type = "远程开门";
                    break;
                case "D0":
                    type = "机械钥匙开门";
                    break;
                case "E0":
                    type = "关门";
                    break;
                default:
                    type = "默认";
                    break;
            }


            Map<String, Object> mapEvent = Maps.newHashMap();
            mapEvent.put("pluginId", LockerInitConfig.getLockerPluginId());
            mapEvent.put("mainEvent", EventType.EVENT_TYPE_LOCKER_COMMON);
            mapEvent.put("eventType", type);
            mapEvent.put("hex", cmd);
            mapEvent.put("fireTime", time);
            mapEvent.put("conAddress", hexAddress);
            mapEvent.put("subAddress", lockAddress);
            mapEvent.put("events", JSONObject.toJSONString(evt));

            StreamContainerWrap.sendEvent(mapEvent);

        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }
}
