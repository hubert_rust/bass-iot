package com.bx.hotcode;

import com.bx.constants.MsgConst;
import com.bx.plugin.CodeHandle;
import com.bx.plugin.CodeService;
import com.bx.plugin.HDType;
import com.bx.plugin.ISendMessageEncode;
import com.bx.utils.ByteUtil;
import com.bx.utils.CharacterUtil;
import com.bx.utils.StrUtil;
import com.smart.command.CommandCode;
//import org.apache.curator.framework.recipes.locks.Locker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/*
 * @ program: plugin-hotwater
 * @ description: d
 * @ author: admin
 * @ create: 2019-04-03 09:40
 **/
@CodeService(hdType= HDType.HDTYPE_LOCK, dir=HDType.MSG_DIR_SEND)
public class LockerEncoder implements ISendMessageEncode {
    private static final Logger LOGGER = LoggerFactory.getLogger(LockerEncoder.class);

    @CodeHandle(name="codeHandle")
    public byte[] getOpen(String str) {
        return  null;
    }

    @PostConstruct
    public void init() {
        System.out.println();
    }

    /**
     * @ Description: 发送消息到硬件, 根据commandCode参数(命令),获取到字节码
     * @ Author: Admin
    **/
    @Override
    public byte[] getSendMessage(String commandCode,
                                   String conAddress,
                                   String meterAddress,
                                   byte frameSeq,
                                   Map map) throws Exception{
        //数据格式AA 01(集中器地址) 01(门锁地址) 04(命令码) 01(长度) 01 00(CRC) 55(结束符)

        byte[] byteTpl = null;
        try {
            //LOGGER.info(">>> LockerEncoder, getSendMessageByte.");
            switch (commandCode) {
                case CommandCode.CMD_SEND_HEART_BEAT: {
                    String  hexStr = "AA%CONADDR000001010155";
                    hexStr = hexStr.replace("%CONADDR", conAddress);
                    byteTpl = CharacterUtil.hexString2Bytes(hexStr);
                    break;
                }
                case CommandCode.CMD_LOCK_GET_TIME: {
                    StringBuffer cmdstr = new StringBuffer();
                    cmdstr.append(LockerConst.FRAME_HEADER);
                    cmdstr.append(LockerConst.tensSwitchSixteen(conAddress));
                    cmdstr.append(LockerConst.tensSwitchSixteen(meterAddress));
                    cmdstr.append(LockerConst.CMD_CODE_QUERY_TIME);
                    cmdstr.append(LockerConst.CMD_CODE_HEART_BEAT);
                    cmdstr.append(LockerConst.CMD_CODE_HEART_BEAT);
                    cmdstr.append(LockerConst.FRAME_END);
                    byteTpl = CharacterUtil.hexString2Bytes(cmdstr.toString());

                    break;
                }
                case CommandCode.CMD_LOCK_TIME_SYNC: {
                    long time = System.currentTimeMillis();
                    String data = Long.toHexString(time / 1000).toUpperCase();
                    StringBuffer cmdstr = new StringBuffer();
                    cmdstr.append(LockerConst.FRAME_HEADER);
                    cmdstr.append(LockerConst.tensSwitchSixteen(conAddress));
                    cmdstr.append(LockerConst.tensSwitchSixteen(meterAddress));
                    cmdstr.append(LockerConst.CMD_CODE_TIME_SYNC);
                    cmdstr.append(LockerConst.countDataLength(data));
                    cmdstr.append(data);
                    //校验位,计算数据位之和
                    cmdstr.append(LockerConst.makeChecksum(data));
                    cmdstr.append(LockerConst.FRAME_END);
                    byteTpl = CharacterUtil.hexString2Bytes(cmdstr.toString());
                    break;
                }
                case CommandCode.CMD_LOCK_REMOTE_UNLOCK: {
                    //远程开锁
                    String content = "01";
                    StringBuffer cmdstr = new StringBuffer();
                    cmdstr.append(LockerConst.FRAME_HEADER);
                    cmdstr.append(LockerConst.tensSwitchSixteen(conAddress));
                    cmdstr.append(LockerConst.tensSwitchSixteen(meterAddress));
                    cmdstr.append(LockerConst.CMD_CODE_REMOTE_UNLOCK);
                    cmdstr.append(LockerConst.countDataLength(content));
                    cmdstr.append(content);
                    //校验位,计算数据位之和
                    cmdstr.append(LockerConst.makeChecksum(content));
                    cmdstr.append(LockerConst.FRAME_END);
                    byteTpl = CharacterUtil.hexString2Bytes(cmdstr.toString());
                    break;
                }
                case CommandCode.CMD_LOCK_QUERY_STATE: {
                    //查询锁状态
                    StringBuffer cmdstr = new StringBuffer();
                    cmdstr.append(LockerConst.FRAME_HEADER);
                    cmdstr.append(LockerConst.tensSwitchSixteen(conAddress));
                    cmdstr.append(LockerConst.tensSwitchSixteen(meterAddress));
                    cmdstr.append(LockerConst.CMD_CODE_QUERY_LOCK_STATE);
                    cmdstr.append(LockerConst.CMD_CODE_HEART_BEAT);
                    cmdstr.append(LockerConst.CMD_CODE_HEART_BEAT);
                    cmdstr.append(LockerConst.FRAME_END);
                    byteTpl = CharacterUtil.hexString2Bytes(cmdstr.toString());
                    break;
                }
                case CommandCode.CMD_LOCK_UPDATE_TEMP_CARD: {
                    long time = System.currentTimeMillis();
                    String tmStr = Long.toHexString(time/1000).toUpperCase();
                    //String tmSpan = map.get("cardSpan").toString();
                    //String tmStr = Long.toHexString(Long.valueOf(tmSpan)).toUpperCase();

                    String content = map.get("cardNo").toString();
                    content = LockerConst.inversionCardNo(content) + tmStr;
                    //更新临时卡
                    StringBuffer cmdstr = new StringBuffer();
                    cmdstr.append(LockerConst.FRAME_HEADER);
                    cmdstr.append(LockerConst.tensSwitchSixteen(conAddress));
                    cmdstr.append(LockerConst.tensSwitchSixteen(meterAddress));
                    cmdstr.append(LockerConst.CMD_CODE_UPDATE_TEMP_CARD);
                    cmdstr.append(LockerConst.countDataLength(content));
                    cmdstr.append(content);
                    //校验位,计算数据位之和
                    cmdstr.append(LockerConst.makeChecksum(content));
                    cmdstr.append(LockerConst.FRAME_END);
                    byteTpl = CharacterUtil.hexString2Bytes(cmdstr.toString());

                    break;
                }
                case CommandCode.CMD_LOCK_REMOTE_RESTART_HUB: {
                    //远程重启集中器
                    String content = "01";
                    StringBuffer cmdstr = new StringBuffer();
                    cmdstr.append(LockerConst.FRAME_HEADER);
                    cmdstr.append(LockerConst.tensSwitchSixteen(conAddress));
                    cmdstr.append(LockerConst.tensSwitchSixteen(meterAddress));
                    cmdstr.append(LockerConst.CMD_CODE_REMOTE_RESTART_HUB);
                    cmdstr.append(LockerConst.countDataLength(content));
                    cmdstr.append(content);
                    //校验位,计算数据位之和
                    cmdstr.append(LockerConst.makeChecksum(content));
                    cmdstr.append(LockerConst.FRAME_END);
                    byteTpl = CharacterUtil.hexString2Bytes(cmdstr.toString());
                    break;
                }
                case CommandCode.CMD_LOCK_SET_OFFLINE_SECRETKEY: {
                    //离线密钥
                    String content = map.get("secretKey").toString();
                    StringBuffer cmdstr = new StringBuffer();
                    cmdstr.append(LockerConst.FRAME_HEADER);
                    cmdstr.append(LockerConst.tensSwitchSixteen(conAddress));
                    cmdstr.append(LockerConst.tensSwitchSixteen(meterAddress));
                    cmdstr.append(LockerConst.CMD_CODE_SET_OFFLINE_SECRETKEY);
                    cmdstr.append(LockerConst.countDataLength(content));
                    cmdstr.append(content);
                    //校验位,计算数据位之和
                    cmdstr.append(LockerConst.makeChecksum(content));
                    cmdstr.append(LockerConst.FRAME_END);
                    byteTpl = CharacterUtil.hexString2Bytes(cmdstr.toString());
                    break;
                }
                case CommandCode.CMD_LOCK_SET_ADMIN_PWD: {
                    //设置管理员密码
                    //管理员密码以逗号分隔
                    String adminPwd = map.get("adminPwd").toString();
                    List<Long> pwdList = Arrays.asList(adminPwd.split(","))
                            .stream()
                            .map(s -> { return Long.parseLong(s.trim()); })
                            .collect(Collectors.toList());
                    if (pwdList.size() <=0) {
                        pwdList.add(Long.valueOf(adminPwd.trim()));
                    }
                    String adminPass = LockerConst.setPassword(pwdList);
                    StringBuffer cmdstr = new StringBuffer();
                    cmdstr.append(LockerConst.FRAME_HEADER);
                    cmdstr.append(LockerConst.tensSwitchSixteen(conAddress));
                    cmdstr.append(LockerConst.tensSwitchSixteen(meterAddress));
                    cmdstr.append(LockerConst.CMD_CODE_SET_ADMIN_PWD);
                    cmdstr.append(LockerConst.countDataLength(adminPass));
                    cmdstr.append(adminPass);
                    //校验位,计算数据位之和
                    cmdstr.append(LockerConst.makeChecksum(adminPass));
                    cmdstr.append(LockerConst.FRAME_END);
                    byteTpl = CharacterUtil.hexString2Bytes(cmdstr.toString());
                    break;
                }
                case CommandCode.CMD_LOCK_SET_ADMIN_CARDNO: {
                    //设置管理员卡号
                    //管理员卡号以逗号分隔
                    String adminCard = map.get("adminCard").toString();
                    List<String> cardList = Arrays.asList(adminCard.split(","))
                            .stream()
                            .map(s -> { return s.trim(); })
                            .collect(Collectors.toList());
                    if (cardList.size() <=0) {
                        cardList.add(LockerConst.inversionCardNo(adminCard.trim()));
                    }
                    String adminCardStr = LockerConst.setCard(cardList);
                    StringBuffer cmdstr = new StringBuffer();
                    cmdstr.append(LockerConst.FRAME_HEADER);
                    cmdstr.append(LockerConst.tensSwitchSixteen(conAddress));
                    cmdstr.append(LockerConst.tensSwitchSixteen(meterAddress));
                    cmdstr.append(LockerConst.CMD_CODE_SET_ADMIN_CARD);
                    cmdstr.append(LockerConst.countDataLength(adminCardStr));
                    cmdstr.append(adminCardStr);
                    //校验位,计算数据位之和
                    cmdstr.append(LockerConst.makeChecksum(adminCardStr));
                    cmdstr.append(LockerConst.FRAME_END);
                    byteTpl = CharacterUtil.hexString2Bytes(cmdstr.toString());
                    break;
                }
                case CommandCode.CMD_LOCK_UPDATE_LOCK_ADDRLIST: {
                    //更新集中器锁地址列表
                    String lockerAddress= "00";
                    String lockerAddr = map.get("lockerAddr").toString().trim();
                    List<String> addressList = Arrays.asList(lockerAddr.split(","))
                            .stream()
                            .map(s -> LockerConst.tensSwitchSixteen(s.trim()))
                            .collect(Collectors.toList());
                    if (addressList.size() <=0) { addressList.add(LockerConst.tensSwitchSixteen(lockerAddr)); }
                    //拼接16进制的锁地址
                    StringBuffer lockPhyAddr = new StringBuffer();
                    for(String lock : addressList){
                        lockPhyAddr.append(lock);
                    }
                    StringBuffer cmdstr = new StringBuffer();
                    cmdstr.append(LockerConst.FRAME_HEADER);
                    cmdstr.append(LockerConst.tensSwitchSixteen(conAddress));
                    cmdstr.append(LockerConst.tensSwitchSixteen(lockerAddress));
                    cmdstr.append(LockerConst.CMD_CODE_UPDATE_LOCKER_ADDRLIST);
                    cmdstr.append(LockerConst.countDataLength(lockPhyAddr.toString()));
                    cmdstr.append(lockPhyAddr.toString());
                    //校验位,计算数据位之和
                    cmdstr.append(LockerConst.makeChecksum(lockPhyAddr.toString()));
                    cmdstr.append(LockerConst.FRAME_END);
                    byteTpl = CharacterUtil.hexString2Bytes(cmdstr.toString());
                    break;
                }
                case CommandCode.CMD_LOCK_UPDATE_SOFTWARE: {
                    //电子门锁远程更新软件开始, start
                    //String type = map.get("state").toString();
                    //String content = type.equals(CommandCode.UPGRADE_STATE_BEGIN) ? "01" : "02";
                    String content = map.get("content").toString();
                    //1,3表示升级集中器，集中器地址为00
                    String LOCK_UPDATE_BEGIN = "AA%CONADDR%LOCKADDR0801%MSG%CRC55";
                    LOCK_UPDATE_BEGIN = LOCK_UPDATE_BEGIN.replace("%CONADDR", conAddress)
                            .replace("%LOCKADDR", meterAddress)
                            .replace("%MSG", content)  //01表示开始，02表示结束
                            .replace("%CRC", LockerConst.makeChecksum(content));
                    byteTpl = CharacterUtil.hexString2Bytes(LOCK_UPDATE_BEGIN);
                    break;
                }

                //pending
                case CommandCode.CMD_LOCK_TRANSMIT_DATA: {
                    //电子门锁远程更新软件数据
                    String transmitData = map.get("content").toString();
                    //String TRANSMIT_DATA = "AA%s%s0982%s%s55";
                    StringBuffer cmdstr = new StringBuffer();
                    cmdstr.append(LockerConst.FRAME_HEADER);
                    cmdstr.append(LockerConst.tensSwitchSixteen(conAddress));
                    cmdstr.append(LockerConst.tensSwitchSixteen(meterAddress));
                    cmdstr.append(LockerConst.CMD_CODE_TRANSMIT_DATA);
                    cmdstr.append("82"); //128 + 2个字节(FFFF)
                    cmdstr.append(transmitData);
                    cmdstr.append(LockerConst.makeChecksum(transmitData));
                    cmdstr.append(LockerConst.FRAME_END);
                    byteTpl = CharacterUtil.hexString2Bytes(cmdstr.toString());
                    break;
                }

                default: {
                    LOGGER.error(">>> LockerEncoder, getSendMessage, commandCode invalid: {}",
                            commandCode);
                    break;
                }
            }
        } catch (Exception e) {
           LOGGER.error(">>> LockerEncoder, getSendMessage, e: {}", e.getMessage());
        }

        return byteTpl;
    }

    public static void main(String[] args) {

        String LOCK_UPDATE_BEGIN = "AA%CONADDR%LOCKADDR0801%MSG%CRC55";

        LOCK_UPDATE_BEGIN = LOCK_UPDATE_BEGIN.replace("%CONADDR", "0B");
        System.out.println(LOCK_UPDATE_BEGIN);
    }

}
