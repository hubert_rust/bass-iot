package com.bx.hotcode;

import com.bx.message.inside.IMessage;
import com.bx.plugin.CodeService;
import com.bx.plugin.HDType;
import com.bx.plugin.IRespMessageAnalysis;
import com.bx.regcode.CodeHandleRegister;
import com.bx.utils.CharacterUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-07 13:43
 **/
@CodeService(hdType= HDType.HDTYPE_LOCK, dir=HDType.MSG_DIR_ANALYSIS)
public class LockerMessageRespAnalysis implements IRespMessageAnalysis {
    private static final Logger log = LoggerFactory.getLogger(LockerMessageRespAnalysis.class);


    public static LockerMessageRespAnalysis instance = new LockerMessageRespAnalysis();

    public LockerMessageRespAnalysis getInstance() {
        return instance;
    }

    /**
     * @Description: 硬件响应消息处理
     * @Param: addressConvert对locker无用
     * @return:
     * @Author: admin
     * @Date: 2019/5/7
     */
    @Override
    public IMessage respMessageProcess(byte[] bytes, Channel channel, boolean addressConvert) {
        if (!checkValid(bytes))  { return null; }

        IMessage map = null;
        LockerDecoder decoder = (LockerDecoder) CodeHandleRegister
                .funDecodeHandle.get(HDType.HDTYPE_LOCK);

        map = decoder.getRespMessageObj(bytes, null);

        return map;
    }


    /**
    * @Description: 数据格式AA(起始帧 固定) 01(集中器地址) 01(门锁地址) 04(命令码)
     * 01(数据长度 可变) 01(数据 可变) 00(CRC校验码) 55(结束帧 固定)
    * @Param:
    * @return:
    * @Author: admin
    * @Date: 2019/6/14
    */
    @Override
    public void splicingPackage(ByteBuf bufferIn, List<Object> out) {
        //log.info(">>> splicingPackage, readableBytes: {}", bufferIn.readableBytes());
        if (bufferIn.readableBytes() < 5) {
            return;
        }

        // 读取开始的位置
        int beginIndex = bufferIn.readerIndex();

        int num = 0;
        byte findAA = 0x00;
        while(num<8) {
            findAA = bufferIn.readByte();
            if ((findAA & 0xFF) == 0xAA) {
                beginIndex += num;
                bufferIn.readerIndex(beginIndex);
                break;
            }
            num++;
        }


        byte header = bufferIn.readByte();
        byte hubAddr = bufferIn.readByte();
        byte lockAddr = bufferIn.readByte();
        byte cmd = bufferIn.readByte();
        byte dataLength = bufferIn.readByte();
        dataLength += 2;   //CRC和结束符

        //log.info(">>> splicingPackage, beginIndex: {}, readableBytes: {}, dataLength: {}",
        //        beginIndex, bufferIn.readableBytes(), dataLength);
        // 如果小于数据长度 重置读取索引位置
        if (bufferIn.readableBytes() < dataLength) {
            bufferIn.readerIndex(beginIndex);
            return;
        }

        bufferIn.readerIndex(beginIndex + dataLength+5);  //指向下一个位置
        ByteBuf otherByteBufRef = bufferIn.slice(beginIndex, dataLength+5);
        otherByteBufRef.retain();
        out.add(otherByteBufRef);
    }

    public static void main(String[] args) {
        LockerMessageRespAnalysis msg = new LockerMessageRespAnalysis();
        String var1 = "AA03000001010155";
        byte[] bytes = CharacterUtil.hexString2Bytes(var1);
        msg.checkValid(bytes);
    }
    @Override
    public boolean checkValid(byte[] bytes) {
        try {
            int hvLen = 0;
            int backshankLen = 0;
            for(int i = 0; i < bytes.length; i++){
                String hexString = CharacterUtil.bytesToHexString(bytes[i]);
                if ("AA".equals(hexString)) {
                    hvLen = i;
                } else if ("55".equals(hexString)) {
                    backshankLen = i;
                }
            }

            // 针头判断
            String hv = CharacterUtil.bytesToHexString(bytes[hvLen]) ;

            if (!hv.equals(LockerConst.FRAME_HEADER)) {
                return false;
            }
            //针尾判断
            String backshank = CharacterUtil.bytesToHexString(bytes[backshankLen]) ;
            if (!LockerConst.FRAME_END.equals(backshank)) {
                return false;
            }
            //数据长度
            String dataLen = CharacterUtil.bytesToHexString(bytes[hvLen + 4]);
            int parseInt = Integer.parseInt(dataLen, 16);
            //数据
            StringBuffer cmdstr = new StringBuffer();
            for(int i = 0; i < parseInt;i++){
                cmdstr.append(CharacterUtil.bytesToHexString(bytes[hvLen + i + 5 ]));
            }
            //校验
            String checkout = CharacterUtil.bytesToHexString(bytes[backshankLen - 1]);
            //求和
            String verify = LockerConst.makeChecksum(cmdstr.toString()).toUpperCase();
            if(checkout.equals(verify)){
                return true;
            }else {
                return false;
            }
        } catch (Exception e) {
            log.error(">>> Message RespAnalysis, Exception: {}", e);
            return false;
        }



    }


}
