package com.bx.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-06-22 15:57
 **/
@Component
@Order(value = 1)
public class LockerInitConfig implements ApplicationRunner {

    public static String lockerPluginId;

    public static String getLockerPluginId() {
        return lockerPluginId;
    }

    @Value("${plugin.pluginId}")
    private String pluginId;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        LockerInitConfig.lockerPluginId = this.pluginId;
    }
}
