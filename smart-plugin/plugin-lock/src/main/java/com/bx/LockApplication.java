package com.bx;

import com.bx.stream.EventSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-06-14 10:05
 **/
//@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"com.bx"})
public class LockApplication {
    public static void main(String[] args) {
        SpringApplication.run(LockApplication.class, args);
    }
}
