package com.bx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"com.bx"})
public class SimpleElecApplication {
    public static void main(String[] args) {
        SpringApplication.run(SimpleElecApplication.class, args);
    }
}
