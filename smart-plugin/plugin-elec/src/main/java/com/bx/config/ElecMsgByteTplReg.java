package com.bx.config;

import com.bx.PluginContainer;
import com.bx.constants.MsgConst;
import com.bx.message.msgtpl.MsgByteTpl;
import com.bx.utils.ByteUtil;
import com.bx.utils.CRCEncode;
import com.bx.utils.CharacterUtil;
import com.bx.utils.HepCRC16;
import com.google.common.collect.Maps;
import com.smart.command.CommandCode;
import com.smart.command.CommandVal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Map;

/**
 * @program: plugin, 消息模板
 * @description:
 * @author: admin
 * @create: 2019-05-08 09:06
 **/
@Component
public class ElecMsgByteTplReg implements ApplicationRunner {
    private static final Logger log = LoggerFactory.getLogger(ElecMsgByteTplReg.class);

    public static final String HUB_ADDR_TPL = "0002461002";
    public static final String HOT_METER_ADDR_TPL = "07051807000021";

    public static final Map<String, MsgByteTpl.ByteWrapper> tplMap = Maps.newHashMap();

    public final static byte[] rcv_data_login_req = {0x68, 0x31, 0, 0x31, 0,
            0x68, (byte) 0xC9, 0x49, 0, 1, 0, 0, 2, 0x7B, 0, 0, 1, 0,
            (byte) 0x91, 0x16};

    public final static byte[] send_data_login_resp = { 0x68, 0x31, 0, 0x31, 0, 0x68, 0x0B,
            0x49, 0, 1, 0, 0, 0, 0x6B, 0, 0, 1, 0, (byte) 0xC1, 0x16 };

    public final static byte[] rcv_data_heartbeat_req = {0x68, 0x31, 0, 0x31, 0,
            0x68, (byte) 0xC9, 0x49, 0, 1, 0, 0, 2, 0x7C, 0, 0, 4, 0,
            (byte) 0x95, 0x16};
    public final static  byte[] send_data_heartbeat_resp = { 0x68, 0x49, 0x00, 0x49, 0x00,
            0x68, 0x0B, 0x49, 0x00, 0x01, 0x00, 0x00, 0x00, 0x6C, 0x00,
            0x00, 0x04, 0x00, 0x02, 0x00, 0x00, 0x04, 0x00, 0x00,
            (byte) 0xCB, 0x16 };

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info(">>> HotWaterInit, run...");

        //登录
        tplMap.put(MsgConst.MSG_TYPE_LOGIN,
                new MsgByteTpl.ByteWrapper(rcv_data_login_req));
        tplMap.put(MsgConst.MSG_TYPE_LOGIN_RESP,
                new MsgByteTpl.ByteWrapper(send_data_login_resp));
        //心跳
        tplMap.put(MsgConst.MSG_TYPE_BEAT,
                new MsgByteTpl.ByteWrapper(rcv_data_heartbeat_req));
        tplMap.put(MsgConst.MSG_TYPE_BEAT_RESP,
                new MsgByteTpl.ByteWrapper(send_data_heartbeat_resp));

        //暂时保存两份
        PluginContainer.msgTpl.putAll(tplMap);
    }
    //时间同步
    // 设置电表时间
    public static byte[] cmdTimeSync(String hubAddr,
                                     byte ammeterAddr,
                                     byte[] datetime,
                                     byte seq) {

        byte[] arb_send_data_req = { 0x68, (byte) 0x97, 0x00, (byte) 0x97, 0,
                0x68, 0x4B,/* 控制域:发送-0x4B,接收-0x88 */
                0x49, 0, 1, 0, 2, 0x10,/* afn:0x10-转发 */0x60,/* frame seq */
                0, 0,/* DA */1, 0,/* DT:1,0-f1~p0,表示转发 */
                2/* 转发通道：01-mbus,02-485 */, (byte) 0xCB, (byte) 0xFF,/* 帧响应超时时间 */
                0x32,/* 字节超时时间 */0x16, /* transmit data len */
                /* 转发数据内容开始 */
                (byte) 0xBC, /* 地址 */
                (byte) 0x10, // /*控制码：03-读数据，06-写数据，0x10-设置多个寄存器，0x05-设置单个线圈*/
                (byte) 0xEA, (byte) 0x60, /* 起始寄存器地址，高字节在前，寄存器10000，写电价 float类型 */
                (byte) 0x00, (byte) 0x04, /* 需要读取寄存器的个数,高字节在前，4个寄存器 */
                (byte) 0x08, /* 数据字节长度 */
                (byte) 0x00, (byte) 0x00, /* 年，月  年需用现在得年减去2000，保留可默认为0 */
                (byte) 0x00, (byte) 0x00, /* 日，时 */
                (byte) 0x00, (byte) 0x00, /* 分，秒 */
                (byte) 0x00, (byte) 0x00, /* 保留，保留 */
                (byte) 0x3D, (byte) 0xA2, /* 效验 CRC16，对应前面6个字节数据的 */
                0xE, 0x16 };

        //arb_send_data_req[18] = comm_port;
        //byte curByte=(byte) (arb_send_data_req[index_trans_communication_port_offset + 1] & 0x1F);
        //arb_send_data_req[index_trans_communication_port_offset + 1]=(byte) ((bps << 5) | curByte);
        /* 集中器地址 */
        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddr);
        byte index_hub_addr_start =7;
        byte fi_hub_addr_byte_len = 5;
        System.arraycopy(hubAddrByte, 0, arb_send_data_req, index_hub_addr_start,
                fi_hub_addr_byte_len);
        /* 表地址 */
        byte index_send_ammeter_addr = 23;
        byte index_frame_seq = 13;
        arb_send_data_req[index_send_ammeter_addr] = ammeterAddr;

        arb_send_data_req[index_frame_seq] = seq;
        setLen(arb_send_data_req);


        byte index_send_ammeter_appdata_len = (byte)29;
        byte index_send_ammeter_appdata_start = (byte)(index_send_ammeter_appdata_len + 1);
        byte fbyte_len_ammeter_recharge_appdata = 8;

        System.arraycopy(datetime, 0, arb_send_data_req,
                index_send_ammeter_appdata_start, 6);

        /* 填写表应用数据校验CRC16 */
        byte[] crc16Data = HepCRC16.CRC16_Set(Arrays.copyOfRange(
                arb_send_data_req, index_send_ammeter_addr,
                index_send_ammeter_addr + 17), 15);

        System.arraycopy(crc16Data, 0, arb_send_data_req,
                index_send_ammeter_addr, 17);

        /* 填写转发包校验位 */
        byte index_ctrl_domain = (byte)0x06;
        int index_trans_data_crc = arb_send_data_req.length - 2;
        arb_send_data_req[index_trans_data_crc] = ByteUtil.calc_sum_crc(
                arb_send_data_req, index_ctrl_domain, index_trans_data_crc - 1);

        // send msg

        return arb_send_data_req;
    }

    //电表充值
    public static byte[] cmdMeterRecharge(String hubAddr,
                                          byte ammeterAddr,
                                          float balance,
                                          String times,
                                          byte seq) {
        /* 充值请求 */
        byte[] arb_send_data_req = { 0x68, (byte) 0xA1, 0x00, (byte) 0xA1, 0,
                0x68, 0x4B,/* 控制域:发送-0x4B,接收-0x88 */
                0x49, 0, 1, 0, 2, 0x10,/* afn:0x10-转发 */0x60,/* frame seq */
                0, 0,/* DA */1, 0,/* DT:1,0-f1~p0,表示转发 */
                2/* 转发通道：01-mbus,02-485 */, (byte) 0xCB,/* head */(byte) 0xFF,/* 帧响应超时时间 */
                0x32,/* 字节超时时间 */0x14, /* transmit data len */
                /* 转发数据内容开始 */
                (byte) 0xBC, /* 地址 */
                (byte) 0x10, /* 读功能码：03-读，06-写,0x10-写多个寄存器 */
                (byte) 0x27, (byte) 0x13, /* 起始寄存器地址，高字节在前，10100寄存器，当前电价float类型 */
                (byte) 0x00, (byte) 0x07, /* 写寄存器的个数,高字节在前，2个寄存器 */
                (byte) 0x0E, /* 数据字节长度 */
                /* 下面14字节设置后要进行加密处理 */
                (byte) 0x2B, (byte) 0xFD, (byte) 0x72, (byte) 0x33, /*
																	 * 4字节金额：高位字节在前
																	 * ，低位字节在后
																	 */
                (byte) 0x99, (byte) 0x33, (byte) 0x66, (byte) 0x8B, /*
																	 * 4字节次数：高位字节在前
																	 * ，低位字节灾后
																	 */
                (byte) 0x24, (byte) 0x53, (byte) 0x62, (byte) 0x92, /* 4字节备用 */
                (byte) 0x73, (byte) 0x9F, /* 电表应用数据域效验 CRC16（代码给的），对应前面6个字节数据的 */
                (byte) 0xFB, (byte) 0x5C, /* 电表数据帧CRC效验 CRC16（文档给的），对应前面6个字节数据的 */
                0xE,/* 集中器帧crc */0x16 };
        /* 充值请求应用数据部分 */
        byte[] arb_send_app_data_req = { /* 下面14字节设置后要进行加密处理 */
                (byte) 0x2B, (byte) 0xFD, (byte) 0x72, (byte) 0x33, /*
															 * 4字节金额：高位字节在前
															 * ，低位字节在后
															 */
                (byte) 0x99, (byte) 0x33, (byte) 0x66, (byte) 0x8B, /*
															 * 4字节次数：高位字节在前
															 * ，低位字节灾后
															 */
                (byte) 0x24, (byte) 0x53, (byte) 0x62, (byte) 0x92, /* 4字节备用 */
                (byte) 0x73, (byte) 0x9F /* 电表应用数据域效验 CRC16（代码给的），对应前面6个字节数据的 */};

        byte[] balanceByte = CRCEncode.float2ByteArray(balance);

        byte timesByte[] = CRCEncode.intToBytes(Integer.parseInt(times.trim()) + 1);
        /* 集中器地址 */
        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddr);
        byte index_hub_addr_start =7;
        byte fi_hub_addr_byte_len = 5;
        System.arraycopy(hubAddrByte, 0, arb_send_data_req, index_hub_addr_start,
                fi_hub_addr_byte_len);
        /* 表地址 */
        byte index_send_ammeter_addr = 23;
        byte index_frame_seq = 13;
        arb_send_data_req[index_send_ammeter_addr] = ammeterAddr;
        /* 冷水表 */
        arb_send_data_req[index_frame_seq] = seq;

        setLen(arb_send_data_req);


        byte index_send_ammeter_appdata_len = (byte)29;

        byte index_send_ammeter_appdata_start = (byte)(index_send_ammeter_appdata_len + 1);
        byte fbyte_len_ammeter_recharge_appdata = 14;

        System.arraycopy(balanceByte, 0, arb_send_data_req,
                index_send_ammeter_appdata_start, 4);
        System.arraycopy(timesByte, 0, arb_send_data_req,
                index_send_ammeter_appdata_start + 4, 4);

        /* 表应用数据加密 */
        byte[] in = Arrays.copyOfRange(arb_send_data_req,
                index_send_ammeter_appdata_start,
                index_send_ammeter_appdata_start
                        + fbyte_len_ammeter_recharge_appdata);
        byte[] out = new byte[in.length];
        CRCEncode.EncodeData(in, out, fbyte_len_ammeter_recharge_appdata - 2,
                (char) (arb_send_data_req[index_send_ammeter_addr] & 0xFF));

        System.arraycopy(out, 0, arb_send_data_req,
                index_send_ammeter_appdata_start,
                fbyte_len_ammeter_recharge_appdata);
        /* 填写表应用数据校验CRC16 */

        byte[] crc16Data = HepCRC16.CRC16_Set(Arrays.copyOfRange(
                arb_send_data_req, index_send_ammeter_addr,
                index_send_ammeter_addr + 23), 21);

        System.arraycopy(crc16Data, 0, arb_send_data_req,
                index_send_ammeter_addr, 23);

        /* 填写转发包校验位 */
        byte index_ctrl_domain = (byte)0x06;
        int index_trans_data_crc = arb_send_data_req.length - 2;
        arb_send_data_req[index_trans_data_crc] = ByteUtil.calc_sum_crc(
                arb_send_data_req, index_ctrl_domain, index_trans_data_crc - 1);

        return arb_send_data_req;
    }


    //电表控制：执行保电，取消保电，执行限电，取消限电，超功率清除
    public static byte[] send_ammeter_ctrl_req_proc(String hubAddr,
                                                    byte ammeterAddr,
                                                    int lineNo,
                                                    String strCtrlType,
                                                    byte seq) {
        /* 设置电价 */
        byte[] arb_send_data_req = { 0x68, (byte) 0x95, 0x00, (byte) 0x95, 0,
                0x68, 0x4B,/* 控制域:发送-0x4B,接收-0x88 */
                0x49, 0, 1, 0, 2, 0x10,/* afn:0x10-转发 */0x60,/* frame seq */
                0, 0,/* DA */1, 0,/* DT:1,0-f1~p0,表示转发 */
                2/* 转发通道：01-mbus,02-485 */, (byte) 0xCB, (byte) 0xFF,/* 帧响应超时时间 */
                0x32,/* 字节超时时间 */0x14, /* transmit data len */
                /* 转发数据内容开始 */
                (byte) 0xBC, /* 地址 */
                (byte) 0x05, // /*控制码：03-读数据，06-写数据，0x10-设置多个寄存器，0x05-设置单个线圈*/
                (byte) 0x27, (byte) 0x10, /* 起始寄存器地址，高字节在前，寄存器10000，写电价 float类型 */
                (byte) 0x00, (byte) 0x01, /* 输出值cmd: */
                (byte) 0x3D, (byte) 0xA2, /* 效验 CRC16，对应前面6个字节数据的 */
                0xE, 0x16 };
        /* 集中器地址 */
        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddr);
        byte index_hub_addr_start =7;
        byte fi_hub_addr_byte_len = 5;
        System.arraycopy(hubAddrByte, 0, arb_send_data_req, index_hub_addr_start,
                fi_hub_addr_byte_len);
        /* 表地址 */
        byte index_send_ammeter_addr = 23;
        byte index_frame_seq = 13;
        arb_send_data_req[index_send_ammeter_addr] = ammeterAddr;

        arb_send_data_req[index_frame_seq] = seq;

        final  char ammeter_reg_coil_ensure_l1 = 60032;
        final  char ammeter_reg_coil_ensure_l2 = 60035;
        final  char ammeter_reg_coil_limit_l1 = 60033;
        final  char ammeter_reg_coil_limit_l2 = 60036;
        final  char ammeter_reg_coil_overpower_cancel_l1 = 60034;
        final  char ammeter_reg_coil_overpower_cancel_l2 = 60037;

        /* 寄存器填写 */
        byte index_send_ammeter_reg_h = (byte)25;
        byte index_send_ammeter_reg_l = (byte)26;
        byte index_send_ammeter_reg_out_cmd_h = (byte)(index_send_ammeter_reg_l + 1);
        byte index_send_ammeter_reg_out_cmd_l = (byte) (index_send_ammeter_reg_out_cmd_h + 1);
        if (strCtrlType.equals(CommandVal.SET_PROTECT_ELEC)) {
            //执行保电
            int reg = 0;
            if (1 == lineNo) {
                reg = ammeter_reg_coil_ensure_l1;
            } else {
                reg = ammeter_reg_coil_ensure_l2;
            }
            arb_send_data_req[index_send_ammeter_reg_h] = (byte) (reg / 256);
            arb_send_data_req[index_send_ammeter_reg_l] = (byte) (reg & 0xff);
            arb_send_data_req[index_send_ammeter_reg_out_cmd_h] = (byte) 0xAA;
            arb_send_data_req[index_send_ammeter_reg_out_cmd_l] = (byte) 0xAA;
        }
        if (strCtrlType.equals(CommandVal.CANCEL_PROTECT_ELEC)) {
            //取消保电
            int reg = 0;
            if (1 == lineNo) {
                reg = ammeter_reg_coil_ensure_l1;
            } else {
                reg = ammeter_reg_coil_ensure_l2;
            }
            arb_send_data_req[index_send_ammeter_reg_h] = (byte) (reg / 256);
            arb_send_data_req[index_send_ammeter_reg_l] = (byte) (reg & 0xff);
            arb_send_data_req[index_send_ammeter_reg_out_cmd_h] = (byte) 0x55;
            arb_send_data_req[index_send_ammeter_reg_out_cmd_l] = (byte) 0x55;
        } else if (strCtrlType.equals(CommandVal.SET_LIMIT_ELEC)) {
            //执行限电
            int reg = 0;
            if (1 == lineNo) {
                reg = ammeter_reg_coil_limit_l1;
            } else {
                reg = ammeter_reg_coil_limit_l2;
            }
            arb_send_data_req[index_send_ammeter_reg_h] = (byte) (reg / 256);
            arb_send_data_req[index_send_ammeter_reg_l] = (byte) (reg & 0xff);

            arb_send_data_req[index_send_ammeter_reg_out_cmd_h] = (byte) 0xAA;
            arb_send_data_req[index_send_ammeter_reg_out_cmd_l] = (byte) 0xAA;
        } else if (strCtrlType.equals(CommandVal.CANCEL_LIMIT_ELEC)) {
            //取消限电
            int reg = 0;
            if (1 == lineNo) {
                reg = ammeter_reg_coil_limit_l1;
            } else {
                reg = ammeter_reg_coil_limit_l2;
            }
            arb_send_data_req[index_send_ammeter_reg_h] = (byte) (reg / 256);
            arb_send_data_req[index_send_ammeter_reg_l] = (byte) (reg & 0xff);

            arb_send_data_req[index_send_ammeter_reg_out_cmd_h] = (byte) 0x55;
            arb_send_data_req[index_send_ammeter_reg_out_cmd_l] = (byte) 0x55;
        } else if (strCtrlType.equals(CommandVal.OVER_POWER_CLEAR)) {
            //超功率清除
            int reg = 0;
            if (1 == lineNo) {
                reg = ammeter_reg_coil_overpower_cancel_l1;
            } else {
                reg = ammeter_reg_coil_overpower_cancel_l2;
            }
            arb_send_data_req[index_send_ammeter_reg_h] = (byte) (reg / 256);
            arb_send_data_req[index_send_ammeter_reg_l] = (byte) (reg & 0xff);
            arb_send_data_req[index_send_ammeter_reg_out_cmd_h] = (byte) 0xFF;
            arb_send_data_req[index_send_ammeter_reg_out_cmd_l] = (byte) 0x00;
        }

        setLen(arb_send_data_req);
        /* 填写表数据校验CRC16 */
        byte[] crc16Data = HepCRC16.CRC16_Set(Arrays.copyOfRange(
                arb_send_data_req, index_send_ammeter_addr,
                index_send_ammeter_addr + 8), 6);

        System.arraycopy(crc16Data, 0, arb_send_data_req,
                index_send_ammeter_addr, 8);

        /* 填写转发包校验位 */
        byte index_ctrl_domain = (byte) 6;
        int index_trans_data_crc = arb_send_data_req.length - 2;
        arb_send_data_req[index_trans_data_crc] = ByteUtil.calc_sum_crc(
                arb_send_data_req, index_ctrl_domain, index_trans_data_crc - 1);

        return arb_send_data_req;
    }


    //定时开闸关闸
    public static byte[] setTimingOpenCloseBrake(String hubAddr,
                                                 byte ammeterAddr,
                                                 int line_no,
                                                 String state,
                                                 byte frameSeq) {

        byte[] arb_send_app_data_req = { (byte) 0xBC, /* 地址 */
                (byte) 0x03, //
                // /*控制码：03-读数据，06-写数据，0x10-设置多个寄存器，0x05-设置单个线圈*/
                (byte) 0xAB, (byte) 0xE0, /* 起始寄存器地址，高字节在前，40000寄存器 */
                (byte) 0x00, (byte) 0x01, /* 需要读取寄存器的个数,高字节在前，30个寄存器 */
                0x02, /* 数据长度 */
                0x00, (byte) 0x01 /* 数据 */
        };
        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddr);
        arb_send_app_data_req[0] = ammeterAddr;
        arb_send_app_data_req[1] = 0x10;

        char ammeter_reg_address;

        if (line_no == 1) {
            ammeter_reg_address = 44000;
        } else {
            ammeter_reg_address = 44001;
        }

        arb_send_app_data_req[2] = (byte) (ammeter_reg_address / 256);
        arb_send_app_data_req[3] = (byte) (ammeter_reg_address & 0xff);

        if (CommandVal.OPEN.equals(state)) {
            arb_send_app_data_req[8] = 0x01;
        } else if (CommandVal.CLOSE.equals(state)){
            arb_send_app_data_req[8] = 0x00;
        }

        byte[] arb_send_ammeter_data_req = ByteUtil.send_ammeter_head_tail_fill_req_proc2(arb_send_app_data_req);

        byte fb_afn_transmit = (byte)0x10;
        byte fbyte_enum_ammeter = (byte)0xCB;
        byte[] arb_send_trans_frame_data_req = ByteUtil.send_trans_head_tail_fill_req_proc(
                hubAddrByte, fb_afn_transmit, fbyte_enum_ammeter, frameSeq,
                arb_send_ammeter_data_req);

        return arb_send_trans_frame_data_req;
    }


    //设置电价
    public static byte[] cmdSetUnitPrice(String hubAddr,
                                         byte ammeterAddr,
                                         float price,
                                         byte seq) {
        /* 设置电价 */
        byte[] arb_send_data_req = { 0x68, (byte) 0x95, 0x00, (byte) 0x95, 0,
                0x68, 0x4B,/* 控制域:发送-0x4B,接收-0x88 */
                0x49, 0, 1, 0, 2, 0x10,/* afn:0x10-转发 */0x60,/* frame seq */
                0, 0,/* DA */1, 0,/* DT:1,0-f1~p0,表示转发 */
                2/* 转发通道：01-mbus,02-485 */, (byte) 0xCB, (byte) 0xFF,/* 帧响应超时时间 */
                0x32,/* 字节超时时间 */0x14, /* transmit data len */
                /* 转发数据内容开始 */
                (byte) 0xBC, /* 地址 */
                (byte) 0x10, // /*控制码：03-读数据，06-写数据，0x10-设置多个寄存器，0x05-设置单个线圈*/
                (byte) 0x27, (byte) 0x10, /* 起始寄存器地址，高字节在前，寄存器10000，写电价 float类型 */
                (byte) 0x00, (byte) 0x03, /* 需要读取寄存器的个数,高字节在前，3个寄存器 */
                (byte) 0x06, /* 数据字节长度 */
                (byte) 0x29, (byte) 0xEC, (byte) 0xCE, (byte) 0x5C, /* 加密后的电价 */
                (byte) 0x5D, (byte) 0x20, /* 电价CRC */
                (byte) 0x3D, (byte) 0xA2, /* 效验 CRC16，对应前面6个字节数据的 */
                0xE, 0x16 };
        /* 集中器地址 */
        byte index_hub_addr_start =7;
        byte fi_hub_addr_byte_len = 5;
        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddr);
        byte[] priceByte =CRCEncode.float2ByteArray(price);
        System.arraycopy(hubAddrByte, 0, arb_send_data_req, index_hub_addr_start,
                fi_hub_addr_byte_len);
        /* 表地址 */
        byte index_send_ammeter_addr = 23;
        byte index_frame_seq = 13;
        arb_send_data_req[index_send_ammeter_addr] = ammeterAddr;
        arb_send_data_req[index_frame_seq] = seq;

        setLen(arb_send_data_req);

        byte index_send_ammeter_appdata_len = (byte)29;
        byte index_send_ammeter_appdata_start = (byte)(index_send_ammeter_appdata_len + 1);
        byte fbyte_len_ammeter_recharge_appdata = 6;

        System.arraycopy(priceByte, 0, arb_send_data_req,
                index_send_ammeter_appdata_start, 4);

        /* 表应用数据加密 */
        byte[] in = Arrays.copyOfRange(arb_send_data_req,
                index_send_ammeter_appdata_start,
                index_send_ammeter_appdata_start
                        + fbyte_len_ammeter_recharge_appdata);
        byte[] out = new byte[in.length];
        CRCEncode.EncodeData(in, out, fbyte_len_ammeter_recharge_appdata - 2,
                (char) (arb_send_data_req[index_send_ammeter_addr] & 0xFF));

        // byte [] out2 = new byte[4];
        // CRCEncode.DecodeData(out, out2, 6, (char)0xbc);
        // log.info(CRCEncode.bytes2Float(out2)+"");

        System.arraycopy(out, 0, arb_send_data_req,
                index_send_ammeter_appdata_start,
                fbyte_len_ammeter_recharge_appdata);
        /* 填写表应用数据校验CRC16 */

        byte[] crc16Data = HepCRC16.CRC16_Set(Arrays.copyOfRange(
                arb_send_data_req, index_send_ammeter_addr,
                index_send_ammeter_addr + 15), 13);

        System.arraycopy(crc16Data, 0, arb_send_data_req,
                index_send_ammeter_addr, 15);

        /* 填写转发包校验位 */
        byte index_ctrl_domain = (byte) 6;
        int index_trans_data_crc = arb_send_data_req.length - 2;
        arb_send_data_req[index_trans_data_crc] = ByteUtil.calc_sum_crc(
                arb_send_data_req, index_ctrl_domain, index_trans_data_crc - 1);

        return arb_send_data_req;
    }

    /**
     * 读取门限功率
     *
     * @param hubAddr
     * @param ammeterAddr
     * @param frameSeq
     * @return
     */
    public static byte[] queryThresholdPower(String hubAddr,
                                             byte ammeterAddr,
                                             int line_no,
                                             byte frameSeq) {
        byte[] arb_send_app_data_req = {
                // (byte) 0xBC, /* 地址 */
                // (byte) 0x03, //
                // /*控制码：03-读数据，06-写数据，0x10-设置多个寄存器，0x05-设置单个线圈*/
                (byte) 0xA0, (byte) 0xF0, /* 起始寄存器地址，高字节在前，40000寄存器 */
                (byte) 0x00, (byte) 0x01, /* 需要读取寄存器的个数,高字节在前，30个寄存器 */
        };

        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddr);

        char ammeter_reg_address;
        if (line_no == 1) {
            ammeter_reg_address = 41200;  //L1的总功率限制
        } else {
            ammeter_reg_address = 41204;  //L2的总功率限制
        }

        byte ammeter_enum_ctrl_code_read = (byte)0x03;
        byte[] arb_send_ammeter_data_req = ByteUtil.send_ammeter_head_tail_fill_req_proc(
                ammeterAddr, ammeter_enum_ctrl_code_read, ammeter_reg_address,
                1, null);

        byte fb_afn_transmit = (byte)0x10;
        byte fbyte_enum_ammeter = (byte)0xCB;
        byte[] arb_send_trans_frame_data_req = ByteUtil.send_trans_head_tail_fill_req_proc(
                hubAddrByte, fb_afn_transmit, fbyte_enum_ammeter, frameSeq,
                arb_send_ammeter_data_req);

        return arb_send_trans_frame_data_req;
    }

    /**
     * 单一门限功率
     *
     * @param hubAddr
     * @param ammeterAddr
     * @param frameSeq
     * @return
     */
    public static byte[] querySingleThresholdPower(String hubAddr,
                                                   byte ammeterAddr,
                                                   int line_no,
                                                   byte frameSeq) {
        byte[] arb_send_app_data_req = {
                // (byte) 0xBC, /* 地址 */
                // (byte) 0x03, //
                // /*控制码：03-读数据，06-写数据，0x10-设置多个寄存器，0x05-设置单个线圈*/
                (byte) 0xA0, (byte) 0xF0, /* 起始寄存器地址，高字节在前，40000寄存器 */
                (byte) 0x00, (byte) 0x01, /* 需要读取寄存器的个数,高字节在前，30个寄存器 */
        };

        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddr);
        char ammeter_reg_address;
        if (line_no == 1) {
            ammeter_reg_address = 41201; //L1的单一电气最大功率限制
        } else {
            ammeter_reg_address = 41205; //L2的单一电气最大功率限制
        }

        byte ammeter_enum_ctrl_code_read = (byte)0x03;
        byte[] arb_send_ammeter_data_req = ByteUtil.send_ammeter_head_tail_fill_req_proc(
                ammeterAddr, ammeter_enum_ctrl_code_read, ammeter_reg_address,
                1, null);

        byte fb_afn_transmit = (byte)0x10;
        byte fbyte_enum_ammeter = (byte)0xCB;
        byte[] arb_send_trans_frame_data_req = ByteUtil.send_trans_head_tail_fill_req_proc(
                hubAddrByte, fb_afn_transmit, fbyte_enum_ammeter, frameSeq,
                arb_send_ammeter_data_req);

        return arb_send_trans_frame_data_req;
    }


    //查询定时拉闸状态
    public static byte[] queryTimingBrakeState(String hubAddr,
                                               byte ammeterAddr,
                                               int line_no,
                                               byte frameSeq) {
        byte[] arb_send_app_data_req = {
                // (byte) 0xBC, /* 地址 */
                // (byte) 0x03, //
                // /*控制码：03-读数据，06-写数据，0x10-设置多个寄存器，0x05-设置单个线圈*/
                (byte) 0xAB, (byte) 0xE0, /* 起始寄存器地址，高字节在前，40000寄存器 */
                (byte) 0x00, (byte) 0x01, /* 需要读取寄存器的个数,高字节在前，30个寄存器 */
        };

        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddr);
        char ammeter_reg_address;
        if (line_no == 1) {
            ammeter_reg_address = 44000;
        } else {
            ammeter_reg_address = 44001;
        }

        byte ammeter_enum_ctrl_code_read = (byte)0x03;
        byte[] arb_send_ammeter_data_req = ByteUtil.send_ammeter_head_tail_fill_req_proc(
                ammeterAddr, ammeter_enum_ctrl_code_read, ammeter_reg_address,
                1, null);

        byte fb_afn_transmit = (byte)0x10;
        byte fbyte_enum_ammeter = (byte)0xCB;
        byte[] arb_send_trans_frame_data_req = ByteUtil.send_trans_head_tail_fill_req_proc(
                hubAddrByte, fb_afn_transmit, fbyte_enum_ammeter, frameSeq,
                arb_send_ammeter_data_req);

        return arb_send_trans_frame_data_req;
    }

    // 电表充值次数
    public static byte[] queryTimesRecharge(String hubAddr,
                                            byte ammeterAddr,
                                            byte seq) {
        /* 查询购电次数 */
        byte[] arb_send_data_req = { 0x68, (byte) 0x95, 0x00, (byte) 0x95, 0,
                0x68, 0x4B,/* 控制域:发送-0x4B,接收-0x88 */
                0x49, 0, 1, 0, 2, 0x10,/* afn:0x10-转发 */0x60,/* frame seq */
                0, 0,/* DA */1, 0,/* DT:1,0-f1~p0,表示转发 */
                2/* 转发通道：01-mbus,02-485 */, (byte) 0xCB, (byte) 0xFF,/* 帧响应超时时间 */
                0x32,/* 字节超时时间 */0x14, /* transmit data len */
                /* 转发数据内容开始 */
                (byte) 0xBC, /* 地址 */
                (byte) 0x03, /* 读功能码：03-读，06-写 */
                (byte) 0x27, (byte) 0x7A, /* 起始寄存器地址，高字节在前，10100寄存器，当前电价float类型 */
                (byte) 0x00, (byte) 0x02, /* 需要读取寄存器的个数,高字节在前，2个寄存器 */
                (byte) 0xF4, (byte) 0x4B, /* 效验 CRC16，对应前面6个字节数据的 */
                0xE, 0x16 };
        /* 集中器地址 */
        byte index_hub_addr_start =7;
        byte fi_hub_addr_byte_len = 5;
        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddr);
        System.arraycopy(hubAddrByte, 0, arb_send_data_req, index_hub_addr_start,
                fi_hub_addr_byte_len);
        /* 表地址 */
        byte index_send_ammeter_addr = 23;
        byte index_frame_seq = 13;
        arb_send_data_req[index_send_ammeter_addr] = ammeterAddr;
        /* 冷水表 */
        arb_send_data_req[index_frame_seq] = seq;

        setLen(arb_send_data_req);
        /* 填写表数据校验CRC16 */

        byte[] crc16Data = HepCRC16.CRC16_Set(Arrays.copyOfRange(
                arb_send_data_req, index_send_ammeter_addr,
                index_send_ammeter_addr + 8), 6);

        System.arraycopy(crc16Data, 0, arb_send_data_req,
                index_send_ammeter_addr, 8);

        /* 填写转发包校验位 */
        byte index_ctrl_domain = (byte) 6;
        int index_trans_data_crc = arb_send_data_req.length - 2;
        arb_send_data_req[index_trans_data_crc] = ByteUtil.calc_sum_crc(
                arb_send_data_req, index_ctrl_domain, index_trans_data_crc - 1);

        // send msg

        return arb_send_data_req;
    }
    //查询线圈状态
    public static byte[] queryCircleState(String hubAddr,
                                          byte ammeterAddr,
                                          byte frameSeq) {
        byte[] arb_send_app_data_req = {
                // (byte) 0xBC, /* 地址 */
                // (byte) 0x03, //
                // /*控制码：03-读数据，06-写数据，0x10-设置多个寄存器，0x05-设置单个线圈*/
                (byte) 0x9C, (byte) 0x40, /* 起始寄存器地址，高字节在前，40000寄存器 */
                (byte) 0x00, (byte) 0x1E, /* 需要读取寄存器的个数,高字节在前，30个寄存器 */
        };

        byte ammeter_enum_ctrl_code_read = (byte)0x03;
        char ammeter_reg_coil_state = 40000;
        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddr);
        byte[] arb_send_ammeter_data_req = ByteUtil.send_ammeter_head_tail_fill_req_proc(
                ammeterAddr, ammeter_enum_ctrl_code_read,
                ammeter_reg_coil_state, 1, null);


        byte fb_afn_transmit = (byte)0x10;
        byte fbyte_enum_ammeter = (byte)0xCB;
        byte[] arb_send_trans_frame_data_req = ByteUtil.send_trans_head_tail_fill_req_proc(
                hubAddrByte, fb_afn_transmit, fbyte_enum_ammeter, frameSeq,
                arb_send_ammeter_data_req);

        return arb_send_trans_frame_data_req;
    }
    private static void setLen(byte[] arb_send_data_req) {

        int iFrameLength = (arb_send_data_req.length - 8) * 4;
        int index_frame_len_start = 1;
        arb_send_data_req[index_frame_len_start] = (byte) (iFrameLength + 1);
        arb_send_data_req[index_frame_len_start + 1] = (byte) (iFrameLength / 256);
        arb_send_data_req[index_frame_len_start + 2] = (byte) (iFrameLength + 1);
        arb_send_data_req[index_frame_len_start + 3] = (byte) (iFrameLength / 256);

        byte  index_send_com_transmit_len = (byte)22;
        arb_send_data_req[index_send_com_transmit_len] = (byte) (arb_send_data_req.length - 25);
    }
    /**
     * 电表通用查询
     *
     * @param hubAddr
     * @param ammeterAddr
     * @return
     */
    public static byte[] send_ammeter_read_req_proc(String hubAddr,
                                                    byte ammeterAddr,
                                                    byte seq,
                                                    String strCmd) {
        byte[] arb_send_data_req = { 0x68, (byte) 0x65, 0x00, (byte) 0x65, 0,
                0x68, 0x4B,/* 控制域:发送-0x4B,接收-0x88 */
                0x49, 0, 1, 0, 2, 0x10,/* afn:0x10-转发 */0x60,/* frame seq */
                0, 0,/* DA */1, 0,/* DT:1,0-f1~p0,表示转发 */
                2/* 转发通道：01-mbus,02-485 */, (byte) 0xCB, (byte) 0xFF,/* 帧响应超时时间 */
                0x32,/* 字节超时时间 */0x08, /* transmit data len */
                /* 转发数据内容开始 */
                (byte) 0xBC, /* 地址 */
                (byte) 0x03, /* 读功能码：03-读，06-写 */
                (byte) 0x27, (byte) 0x74, /* 起始寄存器地址，高字节在前，10100寄存器，当前电价float类型 */
                (byte) 0x00, (byte) 0x02, /* 需要读取寄存器的个数,高字节在前，2个寄存器 */
                (byte) 0x95, (byte) 0x88, /* 效验 CRC16，对应前面6个字节数据的 */
                0xE, 0x16 };
        /* 集中器地址 */
        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddr);
        byte index_hub_addr_start =7;
        byte fi_hub_addr_byte_len = 5;
        System.arraycopy(hubAddrByte, 0, arb_send_data_req, index_hub_addr_start,
                fi_hub_addr_byte_len);
        /* 表地址 */
        byte index_send_ammeter_addr = 23;
        byte index_frame_seq = 13;
        arb_send_data_req[index_send_ammeter_addr] = ammeterAddr;
        arb_send_data_req[index_frame_seq] = seq;

        byte index_send_ammeter_reg_h = (byte)25;
        byte index_send_ammeter_reg_l = (byte)26;
        if (CommandCode.CMD_QUERY_LEFT_MONEY.equals(strCmd)) {
            //电表余额
            arb_send_data_req[index_send_ammeter_reg_h] = (byte) 0x27;
            arb_send_data_req[index_send_ammeter_reg_l] = (byte) 0x76;
        } else if (CommandCode.CMD_AMOUNT_RECHARGE.equals(strCmd)) {
            //CMD_AMOUNT_RECHARGE
            //累计充值金额
            arb_send_data_req[index_send_ammeter_reg_h] = (byte) 0x27;
            arb_send_data_req[index_send_ammeter_reg_l] = (byte) 0x78;
        } else if (CommandCode.CMD_CTRL_STATE.equals(strCmd)) {
            //CMD_CTRL_STATE
            //电表控制状态
            arb_send_data_req[index_send_ammeter_reg_h] = (byte) 0x9C;
            arb_send_data_req[index_send_ammeter_reg_l] = (byte) 0x40;
        }
        setLen(arb_send_data_req);

        /* 填写表数据校验CRC16 */
        byte[] crc16Data = HepCRC16.CRC16_Set(Arrays.copyOfRange(
                arb_send_data_req, index_send_ammeter_addr,
                index_send_ammeter_addr + 8), 6);

        System.arraycopy(crc16Data, 0, arb_send_data_req,
                index_send_ammeter_addr, 8);

        /* 填写转发包校验位 */
        byte index_ctrl_domain = (byte) 6;
        int index_trans_data_crc = arb_send_data_req.length - 2;
        arb_send_data_req[index_trans_data_crc] = ByteUtil.calc_sum_crc(
                arb_send_data_req, index_ctrl_domain, index_trans_data_crc - 1);

        return arb_send_data_req;
    }
    //查询电价
    public static byte[] queryUintPrice(String hubAddr,
                                        byte ammeterAddr,
                                        byte seq) {
        byte[] arb_send_data_req = { 0x68, (byte) 0x65, 0x00, (byte) 0x65, 0,
                0x68, 0x4B,/* 控制域:发送-0x4B,接收-0x88 */
                0x49, 0, 1, 0, 2, 0x10,/* afn:0x10-转发 */0x60,/* frame seq */
                0, 0,/* DA */1, 0,/* DT:1,0-f1~p0,表示转发 */
                2/* 转发通道：01-mbus,02-485 */, (byte) 0xCB, (byte) 0xFF,/* 帧响应超时时间 */
                0x32,/* 字节超时时间 */0x08, /* transmit data len */
                /* 转发数据内容开始 */
                (byte) 0xBC, /* 地址 */
                (byte) 0x03, /* 读功能码：03-读，06-写 */
                (byte) 0x27, (byte) 0x74, /* 起始寄存器地址，高字节在前，10100寄存器，当前电价float类型 */
                (byte) 0x00, (byte) 0x02, /* 需要读取寄存器的个数,高字节在前，2个寄存器 */
                (byte) 0x95, (byte) 0x88, /* 效验 CRC16，对应前面6个字节数据的 */
                0xE, 0x16 };

        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddr);
        /* 集中器地址 */
        byte index_hub_addr_start =7;
        byte fi_hub_addr_byte_len = 5;
        System.arraycopy(hubAddrByte, 0, arb_send_data_req, index_hub_addr_start,
                fi_hub_addr_byte_len);
        /* 表地址 */
        byte index_send_ammeter_addr = 23;
        byte index_frame_seq = 13;
        arb_send_data_req[index_send_ammeter_addr] = ammeterAddr;
        /* 冷水表 */
        arb_send_data_req[index_frame_seq] = seq;

        // setLen(arb_send_data_req);

        /* 填写表数据校验CRC16 */

        byte[] crc16Data = HepCRC16.CRC16_Set(Arrays.copyOfRange(
                arb_send_data_req, index_send_ammeter_addr,
                index_send_ammeter_addr + 8), 6);

        System.arraycopy(crc16Data, 0, arb_send_data_req,
                index_send_ammeter_addr, 8);

        /* 填写转发包校验位 */
        byte index_ctrl_domain = (byte) 6;
        int index_trans_data_crc = arb_send_data_req.length - 2;
        arb_send_data_req[index_trans_data_crc] = ByteUtil.calc_sum_crc(
                arb_send_data_req, index_ctrl_domain, index_trans_data_crc - 1);

        return arb_send_data_req;
    }


    //查询电表状态
    public static byte[] queryMeterState(String hubAddr,
                                          byte meterAddr,
                                          byte frameSeq) {
        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddr);

        byte[] arb_send_data_req = { 0x68, (byte) 0x65, 0x00, (byte) 0x65, 0,
                0x68, 0x4B, 0x49, 0, 1, 0, 2, 0x10,/* afn:0x10-转发 */0x60,/*
																		 * frame
																		 * seq
																		 */
                0, 0,/* DA */1, 0,/* DT:1,0-f1~p0,表示转发 */
                2/* 转发通道：01-mbus,02-485 */, (byte) 0xCB,/*
														 * 转发控制字：0x6B-2400水表用，0xDB
														 * -9600电表用
														 */
                (byte) 0xFF,/* 帧响应超时时间 */0x32,/* 字节超时时间 */
                0x08, /* transmit data len */
                /* 转发数据内容开始 */
                (byte) 0xBC, /* 地址 */
                (byte) 0x03, // /*控制码：03-读数据，06-写数据，0x10-设置多个寄存器，0x05-设置单个线圈*/
                (byte) 0x9C, (byte) 0x50, /* 起始寄存器地址，高字节在前，40016寄存器 */
                (byte) 0x00, (byte) 0x1E, /* 需要读取寄存器的个数,高字节在前，30个寄存器 */
                (byte) 0xF0, (byte) 0xAE, /* 校验 CRC16，对应前面6个字节数据的 */
                0xE, 0x16 };
        /* 集中器地址 */
        byte index_hub_addr_start =7;
        byte fi_hub_addr_byte_len = 5;
        System.arraycopy(hubAddrByte, 0, arb_send_data_req, index_hub_addr_start,
                fi_hub_addr_byte_len);
        /* 表地址 */
        byte index_send_ammeter_addr = 23;
        arb_send_data_req[index_send_ammeter_addr] = meterAddr;

        byte index_frame_seq = 13;
        arb_send_data_req[index_frame_seq] = frameSeq;
        /* 填写表数据校验CRC16 */

        byte[] crc16Data = HepCRC16.CRC16_Set(Arrays.copyOfRange(
                arb_send_data_req, index_send_ammeter_addr,
                index_send_ammeter_addr + 8), 6);

        System.arraycopy(crc16Data, 0, arb_send_data_req,
                index_send_ammeter_addr, 8);

        /* 填写转发包校验位 */
        byte index_ctrl_domain = (byte) 6;
        int index_trans_data_crc = arb_send_data_req.length - 2;
        arb_send_data_req[index_trans_data_crc] = ByteUtil.calc_sum_crc(
                arb_send_data_req, index_ctrl_domain, index_trans_data_crc - 1);

        return arb_send_data_req;
    }
    // 设置电表时间
    public static byte[] elec_meter_time_sync_proc(byte[] hubAddr,
                                                   byte ammeterAddr,
                                                   byte[] datetime,
                                                   byte seq,
                                                   byte bps,
                                                   byte comm_port) {
        byte[] arb_send_data_req = { 0x68, (byte) 0x97, 0x00, (byte) 0x97, 0,
                0x68, 0x4B,/* 控制域:发送-0x4B,接收-0x88 */
                0x49, 0, 1, 0, 2, 0x10,/* afn:0x10-转发 */0x60,/* frame seq */
                0, 0,/* DA */1, 0,/* DT:1,0-f1~p0,表示转发 */
                2/* 转发通道：01-mbus,02-485 */, (byte) 0xCB, (byte) 0xFF,/* 帧响应超时时间 */
                0x32,/* 字节超时时间 */0x16, /* transmit data len */
                /* 转发数据内容开始 */
                (byte) 0xBC, /* 地址 */
                (byte) 0x10, // /*控制码：03-读数据，06-写数据，0x10-设置多个寄存器，0x05-设置单个线圈*/
                (byte) 0xEA, (byte) 0x60, /* 起始寄存器地址，高字节在前，寄存器10000，写电价 float类型 */
                (byte) 0x00, (byte) 0x04, /* 需要读取寄存器的个数,高字节在前，4个寄存器 */
                (byte) 0x08, /* 数据字节长度 */
                (byte) 0x00, (byte) 0x00, /* 年，月  年需用现在得年减去2000，保留可默认为0 */
                (byte) 0x00, (byte) 0x00, /* 日，时 */
                (byte) 0x00, (byte) 0x00, /* 分，秒 */
                (byte) 0x00, (byte) 0x00, /* 保留，保留 */
                (byte) 0x3D, (byte) 0xA2, /* 效验 CRC16，对应前面6个字节数据的 */
                0xE, 0x16 };

        //arb_send_data_req[18] = comm_port;
        //byte curByte=(byte) (arb_send_data_req[index_trans_communication_port_offset + 1] & 0x1F);
        //arb_send_data_req[index_trans_communication_port_offset + 1]=(byte) ((bps << 5) | curByte);
        /* 集中器地址 */
        byte index_hub_addr_start = 7;
        byte fi_hub_addr_byte_len = 5;
        System.arraycopy(hubAddr, 0, arb_send_data_req, index_hub_addr_start,
                fi_hub_addr_byte_len);
        /* 表地址 */
        byte index_send_ammeter_addr = 23;
        arb_send_data_req[index_send_ammeter_addr] = ammeterAddr;

        byte index_frame_seq = 13;
        arb_send_data_req[index_frame_seq] = seq;
        setLen(arb_send_data_req);

        byte index_send_ammeter_appdata_len = (byte)29;
        int index_send_ammeter_appdata_start = (index_send_ammeter_appdata_len + 1);
        byte fbyte_len_ammeter_recharge_appdata = 8;

        System.arraycopy(datetime, 0, arb_send_data_req,
                index_send_ammeter_appdata_start, 6);

        /* 表应用数据加密 */
		/*byte[] in = Arrays.copyOfRange(arb_send_data_req,
				index_send_ammeter_appdata_start,
				index_send_ammeter_appdata_start
						+ fbyte_len_ammeter_recharge_appdata);
		byte[] out = new byte[in.length];
		CRCEncode.EncodeData(in, out, fbyte_len_ammeter_recharge_appdata - 2,
				(char) (arb_send_data_req[index_send_ammeter_addr] & 0xFF));

		// byte [] out2 = new byte[4];
		// CRCEncode.DecodeData(out, out2, 6, (char)0xbc);
		// log.info(CRCEncode.bytes2Float(out2)+"");

		System.arraycopy(out, 0, arb_send_data_req,
				index_send_ammeter_appdata_start,
				fbyte_len_ammeter_recharge_appdata);
		*/
        /* 填写表应用数据校验CRC16 */
        byte[] crc16Data = HepCRC16.CRC16_Set(Arrays.copyOfRange(
                arb_send_data_req, index_send_ammeter_addr,
                index_send_ammeter_addr + 17), 15);

        System.arraycopy(crc16Data, 0, arb_send_data_req,
                index_send_ammeter_addr, 17);

        /* 填写转发包校验位 */
        byte index_ctrl_domain = (byte)0x06;
        int index_trans_data_crc = arb_send_data_req.length - 2;
        arb_send_data_req[index_trans_data_crc] = ByteUtil.calc_sum_crc(arb_send_data_req,
                index_ctrl_domain,
                index_trans_data_crc - 1);

        // send msg

        return arb_send_data_req;
    }
    /**
     * 门限功率
     *
     * @param hubAddr
     * @param ammeterAddr
     * @param frameSeq
     * @return
     */
    public static byte[] send_ammeter_set_power_req_proc(String hubAddr,
                                                         byte ammeterAddr,
                                                         String type,
                                                         int line_no,
                                                         int limit_power,
                                                         byte frameSeq) {

        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddr);
        byte[] arb_send_app_data_req = { (byte) 0xBC, /* 地址 */
                (byte) 0x03, //
                // /*控制码：03-读数据，06-写数据，0x10-设置多个寄存器，0x05-设置单个线圈*/
                (byte) 0xA0, (byte) 0xF0, /* 起始寄存器地址，高字节在前，40000寄存器 */
                (byte) 0x00, (byte) 0x01, /* 需要读取寄存器的个数,高字节在前，30个寄存器 */
                0x02, /* 数据长度 */
                0x0B, (byte) 0xB8 /* 数据 */
        };
        arb_send_app_data_req[0] = ammeterAddr;
        arb_send_app_data_req[1] = 0x10;

        char ammeter_reg_address;

        if ("total".equals(type)) {
            if (line_no == 1) {
                ammeter_reg_address = 41200;
            } else {
                ammeter_reg_address = 41204;
            }
        } else {
            if (line_no == 1) {
                ammeter_reg_address = 41201;
            } else {
                ammeter_reg_address = 41205;
            }
        }

        arb_send_app_data_req[2] = (byte) (ammeter_reg_address / 256);
        arb_send_app_data_req[3] = (byte) (ammeter_reg_address & 0xff);

        arb_send_app_data_req[7] = (byte) (limit_power / 256);
        arb_send_app_data_req[8] = (byte) (limit_power & 0xff);

        byte[] arb_send_ammeter_data_req = ByteUtil.send_ammeter_head_tail_fill_req_proc2(arb_send_app_data_req);

        byte fb_afn_transmit = (byte)0x10;
        byte fbyte_enum_ammeter = (byte)0xCB;
        byte[] arb_send_trans_frame_data_req = ByteUtil.send_trans_head_tail_fill_req_proc(
                hubAddrByte, fb_afn_transmit, fbyte_enum_ammeter, frameSeq,
                arb_send_ammeter_data_req);

        return arb_send_trans_frame_data_req;
    }

}
