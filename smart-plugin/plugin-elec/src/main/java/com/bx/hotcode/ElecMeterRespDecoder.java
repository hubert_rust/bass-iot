package com.bx.hotcode;

import com.bx.constants.MsgConst;
import com.bx.enginecore.ConcentratorHolder;
import com.bx.message.inside.IMessage;
import com.bx.message.inside.MsgCommon;
import com.bx.utils.ByteUtil;
import com.bx.utils.CharacterUtil;
import com.bx.utils.StrUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.smart.command.CommandCode;
import com.smart.common.ResultConst;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-09 16:53
 **/
public class ElecMeterRespDecoder {
    private static final Logger log = LoggerFactory.getLogger(ElecMeterRespDecoder.class);

    public static IMessage respMessageProc(byte[] bytes) {

        Map<String, Object> data = Maps.newHashMap();
        MsgCommon<Map> mapMsgCommon = null;
        byte[] address = Arrays.copyOfRange(bytes, 7, 7 + 5);
        String hexAddress = CharacterUtil.bytesToHexStringlowHigh(address);
        byte index_rcv_com_transmit_len = 18;
        byte fbyte_head = 0x68;
        int iHeadPos = ByteUtil.byte_array_locate(bytes, index_rcv_com_transmit_len + 1, fbyte_head);
        if (iHeadPos < 0) {
            log.error(">>> ElecMeterRespDecoder, respMessageProc, iHeadPos <0");
            //elecMeterResp(bytes, data);
            //return null;
        } else {
        }

        elecMeterResp(bytes, data);
        List<Map> list = Lists.newArrayList();
        list.add(data);

        byte seq = (byte) (int) Integer.valueOf(data.get("seq").toString());
        String hubAddrResp = data.get("hubAddress").toString();
        String meterAddrResp = data.get("meterAddress").toString();
        data.put("result", ResultConst.SUCCESS);

        //投递到消息loop中
        mapMsgCommon = MsgCommon.MsgCommonBuilder.aMsgCommon()
                .withMsgType(MsgConst.MSG_TYPE_RESP)
                .withMsgAddr(hubAddrResp)
                .withMsgSubAddr(meterAddrResp)
                .withMsgSeq(seq)
                .build();
        mapMsgCommon.setRespStr(StrUtil.splitSpace(CharacterUtil.bytesToHexString(bytes)));
        //使用这种方式
        mapMsgCommon.getResp().add(data);

        return mapMsgCommon;
    }

    public static int elecMeterResp(byte[] baRcvBuf, Map<String, Object> data) {

        int value = -1;
        byte[] arb_data_rcv = {0x68, (byte) 0xCD, 0x00, (byte) 0xCD, 0x00,
                0x68,/* head 2nd */(byte) 0x88,/* 控制域:发送-0x4B,接收-0x88 */
                0x49, 0x00, 0x01, 0x00, 0x02,/* 集中器地址 */
                0x10,/* afn:02-链路检测，0x10-转发 */0x60, 0x00, 0x00, 0x01, 0x00,
                0x26,/* transmit data len */
                (byte) 0xBC,/* 电表地址 */
                (byte) 0x03, // /*控制码：03-读数据，06-写数据，0x10-设置多个寄存器，0x05-设置单个线圈*/
                (byte) 0x02,/* 长度，只有查询才有，其他没有 */
                (byte) 0x00, (byte) 0xBC, /* 40016寄存器，当前电压 */
                (byte) 0xD5, (byte) 0xEE, // /*CRC16*/
                (byte) 0xAB, 0x16};

        int index_afn = 12;
        if (0x10 != baRcvBuf[index_afn]) {
            data.put("result", MsgConst.FAILURE);
            return -1;
        }

        int index_frame_seq = 13;
        int index_hub_addr_start = 7;
        data.put("seq", baRcvBuf[index_frame_seq]);
        String hubAddr = ByteUtil.bytes2str(Arrays.copyOfRange(baRcvBuf,
                index_hub_addr_start, index_hub_addr_start + 5), false, 0);
        data.put("hubAddress", hubAddr);

        //电表地址3位
        int index_rcv_ammeter_addr = 19;
        int meterAddrInt = Integer.parseInt(CharacterUtil.bytesToHexString(baRcvBuf[index_rcv_ammeter_addr]), 16);
        String meterAddr = String.format("%03d", meterAddrInt);
        data.put("meterAddress", meterAddr);

        String cmdStr = ConcentratorHolder.getCmdCodeByHubAddr(hubAddr);

        byte ammeter_enum_ctrl_code_read = (byte) 0x03;
        byte ammeter_enum_ctrl_code_write = (byte)0x06;
        byte ammeter_enum_ctrl_code_write_set_multi_reg = (byte)0x10;
        byte ammeter_enum_ctrl_code_write_set_coil = (byte)0x05;

        byte index_rcv_ammeter_ctrl_code = (byte) 20;
        byte index_rcv_ammeter_app_data_len = (byte) 21;
        if (ammeter_enum_ctrl_code_read == baRcvBuf[index_rcv_ammeter_ctrl_code]) {
            int amMeterDataLen = baRcvBuf[index_rcv_ammeter_app_data_len] & 0xff;
            if (baRcvBuf.length >= 26
                    && baRcvBuf.length != (amMeterDataLen + 26)) {
                data.put("result", MsgConst.FAILURE);
                return -2;
            }

            int index_rcv_ammeter_appdata_start = 22;
            byte[] rcvAmmeterData = Arrays.copyOfRange(baRcvBuf,
                    index_rcv_ammeter_addr, baRcvBuf.length - 2);
            byte[] rcvAmmeterAppData = Arrays.copyOfRange(baRcvBuf,
                    index_rcv_ammeter_appdata_start,
                    index_rcv_ammeter_appdata_start + amMeterDataLen);

            //byte index_rcv_ammeter_app_data_len = index_rcv_ammeter_ctrl_code + 1;
            // rcv_ammeter_read_realtime_data_resp_proc(baRcvBuf, data);
            // 读取实时数据
            //String cmdStr = ReqRspUtil.getRequestType(hubAddr, meterAddr,
            //        baRcvBuf[index_frame_seq]);
            if (CommandCode.CMD_QUERY_METER_STATE.equals(cmdStr)) {
                rcv_ammeter_read_realtime_data_resp_proc(rcvAmmeterAppData,
                        data);
            } else if (CommandCode.CMD_QUERY_CIRCLE_STATE.equals(cmdStr)) {
                rcv_ammeter_read_ctrl_state_resp_proc(rcvAmmeterAppData, data);
            } else if (CommandCode.CMD_QUERY_UNIT_PRICE.equals(cmdStr)) {

                /* BC 03 04 3F 0F 5C 29 ----电价密文，解密后为0.5600 53 F1
                 */

                /* 表应用数据解密 */
                byte[] in = Arrays.copyOfRange(rcvAmmeterData, 3, 3 + 4);
                // 电价
                String am_price = Float.toString(ByteUtil.bytes2Float(in));
                // byte[] plainBytes = CRCEncode.float2ByteArray(0.5600f);
                //data.put("单价", am_price);
                data.put("price", am_price);
            } else if (CommandCode.CMD_QUERY_LEFT_MONEY.equals(cmdStr)) {
                /* BC 03 04 --数据长度 41 2F D7 0A 6D 3A
                 */
                /* 表应用数据解密 */
                byte[] in = Arrays.copyOfRange(rcvAmmeterData, 3, 3 + 4);
                byte[] out = in;

                // 电余额
                float am_balance = ByteUtil.bytes2Float(out);
                log.info(">>> ElecMeterRespDecoder, rcvAmmeterData: {}, CMD_LEFT_MONEY: {}",
                        CharacterUtil.bytesToHexString(rcvAmmeterData), am_balance);
                //data.put("剩余量", am_balance);
                data.put("leftMoney", String.valueOf(am_balance));
            } else if (CommandCode.CMD_TIMES_RECHARGE.equals(cmdStr)) {
                // BC 03 04 00 00 00 01 56 F8,
                String chargeCount = ByteUtil.fourBytes2Float(
                        rcvAmmeterAppData, 0, rcvAmmeterAppData.length, 1f,
                        0);
                //data.put("充值次数", chargeCount);
                data.put("rechargeTimes", chargeCount);
            } else if (CommandCode.CMD_AMOUNT_RECHARGE.equals(cmdStr)) {
                // BC 03 04 41 A8 00 00 02 E4,
                // String chargeSum =
                // CRCEncode.fourBytes2Float(rcvAmmeterAppData, 0,
                // rcvAmmeterAppData.length, 1f, 0);

                byte[] out = Arrays.copyOfRange(rcvAmmeterData, 3, 3 + 4);
                float chargeSum = ByteUtil.bytes2Float(out);
                //data.put("累计充值金额", chargeSum);
                data.put("rechargeAmount", chargeSum);
            } else if (CommandCode.CMD_CTRL_STATE.equals(cmdStr)) {
                // BC 03 02 41 2F 6D 3A,
                String balance = ByteUtil.fourBytes2Float(
                        rcvAmmeterAppData, 0, rcvAmmeterAppData.length,
                        10000f, 4);
                //data.put("控制状态", balance);
                data.put("ctrlState", balance);
            } else if (CommandCode.CMD_TIMING_BRAKE_STATE.equals(cmdStr)) {

                int state1 = (rcvAmmeterAppData[0] & 0xff);
                int state2 = rcvAmmeterAppData[1] & 0xff;
                int state = state1 + state2;
                if (state == 0) {
                    data.put("定时拉闸状态", "否");
                } else {
                    data.put("定时拉闸状态", "是");
                }
                data.put("result", "SUCCESS");

            } else if (CommandCode.CMD_THRESHOLD_POWER.equals(cmdStr)) {

                int power1 = (rcvAmmeterAppData[0] & 0xff) * 256;
                int power2 = rcvAmmeterAppData[1] & 0xff;
                int power = power1 + power2;
                data.put("门限功率", power);
                data.put("result", MsgConst.SUCCESS);

            } else if (CommandCode.CMD_SINGLE_THRESHOLD_POWER.equals(cmdStr)) {
                int power1 = (rcvAmmeterAppData[0] & 0xff) * 256;
                int power2 = rcvAmmeterAppData[1] & 0xff;
                int power = power1 + power2;
                data.put("单一门限功率", power);
                data.put("result", MsgConst.SUCCESS);
            }
        } else if (ammeter_enum_ctrl_code_write == baRcvBuf[index_rcv_ammeter_ctrl_code]) {
            ;
        } else if (ammeter_enum_ctrl_code_write_set_multi_reg == baRcvBuf[index_rcv_ammeter_ctrl_code]) {
            byte index_rcv_ammeter_write_success_reg_count_h = (byte)(index_rcv_ammeter_ctrl_code + 3);
            byte index_rcv_ammeter_write_success_reg_count_l = (byte) ((index_rcv_ammeter_write_success_reg_count_h & 0xff) + 1);
            int reg_count = (int) ((baRcvBuf[index_rcv_ammeter_write_success_reg_count_h]) & 0xff)
                    * 256
                    + (int) (baRcvBuf[index_rcv_ammeter_write_success_reg_count_l] & 0xff);

            /* 设置电价响应 */
            if (0x27 == baRcvBuf[index_rcv_ammeter_ctrl_code + 1]
                    && 0x10 == baRcvBuf[index_rcv_ammeter_ctrl_code + 2]) {
                data.put("operate", "设置电价");
                data.put("result", ((3 == reg_count) ? MsgConst.SUCCESS
                        : MsgConst.FAILURE));
            }
            // 充值响应 BC 10 27 13 00 07 61 97
            else if (0x27 == baRcvBuf[index_rcv_ammeter_ctrl_code + 1]
                    && 0x13 == baRcvBuf[index_rcv_ammeter_ctrl_code + 2]) {
                data.put("operate", "充值");
                data.put("result", ((7 == reg_count) ? MsgConst.SUCCESS
                        : MsgConst.FAILURE));
            } else if (0xA0 == (baRcvBuf[index_rcv_ammeter_ctrl_code + 1] & 0xff)
                    && 0xF0 == (baRcvBuf[index_rcv_ammeter_ctrl_code + 2] & 0xff)) {

                data.put("operate", "设置L1总功率限制");
                data.put("result", MsgConst.SUCCESS);

            } else if (0xA0 == (baRcvBuf[index_rcv_ammeter_ctrl_code + 1] & 0xff)
                    && 0xF1 == (baRcvBuf[index_rcv_ammeter_ctrl_code + 2] & 0xff)) {

                data.put("operate", "设置L1单一电器功率限制");
                data.put("result", MsgConst.SUCCESS);

            } else if (0xA0 == (baRcvBuf[index_rcv_ammeter_ctrl_code + 1] & 0xff)
                    && 0xF4 == (baRcvBuf[index_rcv_ammeter_ctrl_code + 2] & 0xff)) {

                data.put("operate", "设置L2总功率限制");
                data.put("result", MsgConst.SUCCESS);

            } else if (0xA0 == (baRcvBuf[index_rcv_ammeter_ctrl_code + 1] & 0xff)
                    && 0xF5 == (baRcvBuf[index_rcv_ammeter_ctrl_code + 2] & 0xff)) {
                data.put("operate", "设置L2单一电器功率限制");
                data.put("result", MsgConst.SUCCESS);
            } else if (0xAB == (baRcvBuf[index_rcv_ammeter_ctrl_code + 1] & 0xff)
                    && 0xE0 == (baRcvBuf[index_rcv_ammeter_ctrl_code + 2] & 0xff)) {
                data.put("operate", "定时器开关");
                data.put("result", MsgConst.SUCCESS);
            } else if (0xAB == (baRcvBuf[index_rcv_ammeter_ctrl_code + 1] & 0xff)
                    && 0xE1 == (baRcvBuf[index_rcv_ammeter_ctrl_code + 2] & 0xff)) {
                data.put("operate", "定时器开关");
                data.put("result", MsgConst.SUCCESS);
            } else if (0xAD == (baRcvBuf[index_rcv_ammeter_ctrl_code + 1] & 0xff)
                    && 0x0C == (baRcvBuf[index_rcv_ammeter_ctrl_code + 2] & 0xff)) {
                data.put("operate", "设置工作日定时方案");
                data.put("result", MsgConst.SUCCESS);
            } else if (0xAD == (baRcvBuf[index_rcv_ammeter_ctrl_code + 1] & 0xff)
                    && 0x70 == (baRcvBuf[index_rcv_ammeter_ctrl_code + 2] & 0xff)) {
                data.put("operate", "设置双休日定时方案");
                data.put("result", MsgConst.SUCCESS);
            } else if (0xAD == (baRcvBuf[index_rcv_ammeter_ctrl_code + 1] & 0xff)
                    && 0xD4 == (baRcvBuf[index_rcv_ammeter_ctrl_code + 2] & 0xff)) {
                data.put("operate", "设置节假日定时方案");
                data.put("result", MsgConst.SUCCESS);
            }else if (0xAB == (baRcvBuf[index_rcv_ammeter_ctrl_code + 1] & 0xff)
                    && 0xF4 == (baRcvBuf[index_rcv_ammeter_ctrl_code + 2] & 0xff)) {
                data.put("operate", "设置映射方案");
                data.put("result", MsgConst.SUCCESS);
            }else if (0xAC == (baRcvBuf[index_rcv_ammeter_ctrl_code + 1] & 0xff)
                    && 0x44 == (baRcvBuf[index_rcv_ammeter_ctrl_code + 2] & 0xff)) {
                data.put("operate", "设置节假日");
                data.put("result", MsgConst.SUCCESS);
            }
            else if (0xEA ==(baRcvBuf[index_rcv_ammeter_ctrl_code + 1] & 0xff)
                    && 0x60 ==(baRcvBuf[index_rcv_ammeter_ctrl_code + 2] & 0xff)) {
                data.put("operate", "同步时间");
                data.put("result", MsgConst.SUCCESS);
            }
        }		// 0x05-设置单个线圈
        else if (ammeter_enum_ctrl_code_write_set_coil == baRcvBuf[index_rcv_ammeter_ctrl_code]) {
            // BC 05 EA 84 AA AA 5C 09
            byte index_reg_h = (byte) (index_rcv_ammeter_ctrl_code + 1);
            byte index_reg_l = (byte) ((index_reg_h & 0xff) + 1);
            int index_ctrl_para_h = (byte) (index_reg_l + 1);
            int index_ctrl_para_l = (byte) (index_ctrl_para_h + 1);
            int reg = (baRcvBuf[index_reg_h] & 0xff) * 256 + (baRcvBuf[index_reg_l] & 0xff);


            char ammeter_reg_coil_state = 40000;
            final char ammeter_reg_coil_ensure_l1 = 60032;
            final char ammeter_reg_coil_ensure_l2 = 60035;
            final char ammeter_reg_coil_limit_l1 = 60033;
            final char ammeter_reg_coil_limit_l2 = 60036;
            final char ammeter_reg_coil_overpower_cancel_l1 = 60034;
            final char ammeter_reg_coil_overpower_cancel_l2 = 60037;
            switch (reg) {
                case ammeter_reg_coil_ensure_l1: {
                    data.put("operate", "L1保电");
                    break;
                }
                case ammeter_reg_coil_limit_l1: {
                    data.put("operate", "L1限电");
                    break;
                }
                case ammeter_reg_coil_overpower_cancel_l1: {
                    data.put("operate", "L1超功率检测清除");
                    if ((byte) 0xFF == baRcvBuf[index_ctrl_para_h]
                            && (byte) 0x00 == baRcvBuf[index_ctrl_para_l]) {
                        data.put("type", "执行");
                    }
                    break;
                }
                case ammeter_reg_coil_ensure_l2: {
                    data.put("operate", "L2保电");
                    break;
                }
                case ammeter_reg_coil_limit_l2: {
                    data.put("operate", "L2限电");
                    break;
                }
                case ammeter_reg_coil_overpower_cancel_l2: {
                    data.put("operate", "L2超功率检测清除");
                    if ((byte) 0xFF == baRcvBuf[index_ctrl_para_h]
                            && (byte) 0x00 == baRcvBuf[index_ctrl_para_l]) {
                        data.put("type", "执行");
                    }
                    break;
                }
                default: {
                    data.put("operate", "其他");
                    break;
                }
            }

            if (reg != 60034 && reg != 60037) {
                if ((byte) 0xAA == baRcvBuf[index_ctrl_para_h]
                        && (byte) 0xAA == baRcvBuf[index_ctrl_para_l]) {
                    data.put("type", "执行");
                } else {
                    data.put("type", "取消");
                }
            }
            data.put("result", MsgConst.SUCCESS);

        } else if (0x80 == (baRcvBuf[index_rcv_ammeter_ctrl_code] & 0x80)) {
            data.put("result", MsgConst.FAILURE);
        } else {
            return -1;
        }

        return value;
    }


    public static boolean rcv_ammeter_read_ctrl_state_resp_proc(
            byte[] rcv_data, Map<String, Object> data) {

        /* 电表状态查询：明文 */
        byte[] arb_data_rcv = {(byte) 0x57, (byte) 0x48, // 0 40016寄存器，当前电压
                (byte) 0x00, (byte) 0x00, // 2,40017-18寄存器，当前2路总电流
                (byte) 0x00, (byte) 0x00, // 40019寄存器，当前1路电流 6
                (byte) 0x00, (byte) 0x00, // 40020寄存器，当前2路电流 8
                (byte) 0x00, (byte) 0x00, // 10,40021-22寄存器
        };

        // data.put("ammeterAddr", ammeterAddr);
        // data.put("waterAmount",sAmount);
        // data.put("dateTime",sDateTime);
        // data.put("valveState",sValveState);
        // data.put("batteryState",sBatteryState);

        // 电压
        int index_state = 0;
        byte bState1 = rcv_data[index_state];
        byte bState2 = rcv_data[index_state + 1];

        data.put("L1遥控保电状态", (0x1 == (bState1 & 0x1)) ? "是" : "否");
        data.put("L1遥控限电状态", (0x2 == (bState1 & 0x2)) ? "是" : "否");
        data.put("L2遥控保电状态", (0x4 == (bState1 & 0x4)) ? "是" : "否");
        data.put("L2遥控限电状态", (0x8 == (bState1 & 0x8)) ? "是" : "否");

        data.put("result", MsgConst.SUCCESS);

        return true;
    }

    public static boolean rcv_ammeter_read_realtime_data_resp_proc(
            byte[] rcv_data, Map<String, Object> data) {

        /* 电表状态查询：明文 */
        byte[] arb_data_rcv = {(byte) 0x57, (byte) 0x48, // 0 40016寄存器，当前电压
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, // 2,40017-18寄存器，当前2路总电流
                (byte) 0x00, (byte) 0x00, // 40019寄存器，当前1路电流 6
                (byte) 0x00, (byte) 0x00, // 40020寄存器，当前2路电流 8
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, // 10,40021-22寄存器
                // ，当前有功功率
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, // 14,40023-24寄存器
                // ，1路有功功率
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, // 18,
                // 40025-26寄存器，2路有功功率
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, // 22,40027-28寄存器，无功功率
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, // 26,40029-30寄存器，1路无功功率
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, // 30,
                // 40031-32寄存器，2路无功功率
                (byte) 0x03, (byte) 0xE8, // 34, 40033寄存器，总功率因数
                (byte) 0x03, (byte) 0xE8, // 36, 40034寄存器，1路总功率因数
                (byte) 0x03, (byte) 0xE8, // 38, 40035寄存器，2路总功率因数
                (byte) 0x13, (byte) 0x8B, // 40, 40036寄存器，电网频率 50hz
                (byte) 0x00, (byte) 0x00, // 42, 备用
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, // 44,40038-39寄存器/，电池电压/
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x90, // 48,40040-41寄存器，有功电度，总的已知累加
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, // 52,40042-43寄存器，1路，有功电度，总的已知累加
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x90, // 56,40044-45寄存器，2路，有功电度，总的已知累加
                (byte) 0x5E, (byte) 0x5A, // 60,CRC16
                (byte) 0xAB, 0x16};

        // data.put("ammeterAddr", ammeterAddr);
        // data.put("waterAmount",sAmount);
        // data.put("dateTime",sDateTime);
        // data.put("valveState",sValveState);
        // data.put("batteryState",sBatteryState);

        // 电压
        int index_voltage = 0;
        int index_voltage_end = index_voltage + 2;
        String voltage = ByteUtil.twoBytes2Voltage(rcv_data, index_voltage, 2);
        //data.put("电压", voltage);
        data.put("voltage", voltage);
        // 总电流
        int index_current = index_voltage_end;
        int index_current_end = index_voltage + 4;
        String current = ByteUtil.fourBytes2Float(rcv_data, index_current, 4,
                1000f, 3);
        //data.put("电流", current);
        data.put("current", current);
        // 有功功率
        int index_active_power = index_current_end + 6;
        int index_active_power_end = index_active_power + 4;
        String activePower = ByteUtil.fourBytes2Float(rcv_data,
                index_active_power, 4, 10f, 1);
        //data.put("有功功率", activePower);
        data.put("activePower", activePower);
        // 无功功率
        int index_no_active_power = index_active_power_end + 8;
        int index_no_active_power_end = index_no_active_power + 4;
        String inActivePower = ByteUtil.fourBytes2Float(rcv_data,
                index_no_active_power, 4, 10f, 1);
        //data.put("无功功率", inActivePower);
        data.put("reactivePower", inActivePower);
        // 电池电压
        int index_battery_voltage = index_no_active_power_end + 18;
        int index_battery_voltage_end = index_battery_voltage + 4;
        String batteryVoltage = ByteUtil.fourBytes2Float(rcv_data,
                index_battery_voltage, 4, 100f, 2);
        //data.put("电池电压", batteryVoltage);
        data.put("batteryVoltage", batteryVoltage);
        // 有功电度
        int index_active_amount = index_battery_voltage_end;
        int index_active_amount_end = index_active_amount + 4;
        String activeAmount = ByteUtil.fourBytes2Float(rcv_data,
                index_active_amount, 4, 100f, 2);
        //data.put("有功电度", activeAmount);
        data.put("activeDegree", activeAmount);
        // 有功电度1路
        int index_active_amount_l1 = index_active_amount_end;
        int index_active_amount_end_l1 = index_active_amount_l1 + 4;
        String actieAmountL1 = ByteUtil.fourBytes2Float(rcv_data,
                index_active_amount_l1, index_active_power_end, 100f, 2);
        //data.put("1路度数", actieAmountL1);
        data.put("firstDegree", actieAmountL1);
        // 有功电度2路
        int index_active_amount_l2 = index_active_amount_end_l1;
        int index_active_amount_end_l2 = index_active_amount_l2 + 4;
        String activeAmount2 = ByteUtil.fourBytes2Float(rcv_data,
                index_active_amount_l2, index_active_power_end, 100f, 2);
        //data.put("2路度数", activeAmount2);
        data.put("secondDegree", activeAmount2);
        data.put("result", MsgConst.SUCCESS);

        return true;
    }


}
