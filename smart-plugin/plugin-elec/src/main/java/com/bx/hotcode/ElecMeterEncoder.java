package com.bx.hotcode;

import com.bx.config.ElecMsgByteTplReg;
import com.bx.plugin.CodeHandle;
import com.bx.plugin.CodeService;
import com.bx.plugin.HDType;
import com.bx.plugin.ISendMessageEncode;
import com.bx.utils.ByteUtil;
import com.bx.utils.CharacterUtil;
import com.bx.utils.StrUtil;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.smart.command.*;
import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

/*
 * @ program: plugin-hotwater
 * @ description: d
 * @ author: admin
 * @ create: 2019-04-03 09:40
 **/
@CodeService(hdType= HDType.HDTYPE_ELEC_METER, dir=HDType.MSG_DIR_SEND)
public class ElecMeterEncoder implements ISendMessageEncode {
    private static final Logger LOGGER = LoggerFactory.getLogger(ElecMeterEncoder.class);

    @CodeHandle(name="codeHandle")
    public byte[] getOpen(String str) {
        return  null;
    }

    @PostConstruct
    public void init() {
        System.out.println();
    }

    public static void main(String[] args) {
        BigDecimal bigDecimal = new BigDecimal("3.145");
        bigDecimal = bigDecimal.setScale(2, RoundingMode.HALF_UP);
        System.out.println(bigDecimal);

    }
    /**
     * @ Description: 发送消息到硬件, 根据commandCode参数(命令),获取到字节码
     * @ Author: Admin
    **/
    @Override
    public byte[] getSendMessage(String commandCode,
                                   String conAddress,
                                   String meterAddress,
                                   byte frameSeq,
                                   Map map) throws Exception{
        byte[] byteTpl = null;
        try {
            LOGGER.info(">>> ElecMeterEncoder, getSendMessageByte.");
            int bps = Integer.valueOf(map.get("bps").toString());
            int commPort = Integer.valueOf(map.get("commPort").toString());
            switch (commandCode) {
                case CommandCode.CMD_QUERY_METER_STATE: {
                    int meter = Integer.valueOf(meterAddress);
                    byteTpl = ElecMsgByteTplReg.queryMeterState(conAddress, (byte)meter, frameSeq);
                    LOGGER.info(">>> ElecMeterEncoder, getSendMessage, CMD_QUERY_METER_STATE: {}",
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                //线圈状态
                case CommandCode.CMD_QUERY_CIRCLE_STATE: {
                    int meter = Integer.valueOf(meterAddress);
                    byteTpl = ElecMsgByteTplReg.queryCircleState(conAddress, (byte)meter, frameSeq);
                    LOGGER.info(">>> ElecMeterEncoder, getSendMessage, CMD_QUERY_CIRCLE_STATE: {}",
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                //电表单价
                case CommandCode.CMD_QUERY_UNIT_PRICE: {
                    int meter = Integer.valueOf(meterAddress);
                    byteTpl = ElecMsgByteTplReg.queryUintPrice(conAddress, (byte)meter, frameSeq);
                    LOGGER.info(">>> ElecMeterEncoder, getSendMessage, CMD_QUERY_UNIT_PRICE: {}",
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                //电表余额
                case CommandCode.CMD_QUERY_LEFT_MONEY: {
                    int meter = Integer.valueOf(meterAddress);
                    byteTpl = ElecMsgByteTplReg.send_ammeter_read_req_proc(conAddress, (byte)meter, frameSeq, commandCode);
                    LOGGER.info(">>> ElecMeterEncoder, getSendMessage, CMD_QUERY_LEFT_MONEY: {}",
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                //电表累计充值金额
                case CommandCode.CMD_AMOUNT_RECHARGE: {
                    int meter = Integer.valueOf(meterAddress);
                    byteTpl = ElecMsgByteTplReg.send_ammeter_read_req_proc(conAddress, (byte)meter, frameSeq, commandCode);
                    LOGGER.info(">>> ElecMeterEncoder, getSendMessage, CMD_AMOUNT_RECHARGE: {}",
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                //电表累计充值次数
                case CommandCode.CMD_TIMES_RECHARGE: {
                    int meter = Integer.valueOf(meterAddress);
                    byteTpl = ElecMsgByteTplReg.queryTimesRecharge(conAddress, (byte)meter, frameSeq);
                    LOGGER.info(">>> ElecMeterEncoder, getSendMessage, CMD_TIMES_RRECHARGE: {}",
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                //电表控制状态,pending,
                case CommandCode.CMD_CTRL_STATE: {
                    int meter = Integer.valueOf(meterAddress);
                    byteTpl = ElecMsgByteTplReg.send_ammeter_read_req_proc(conAddress, (byte)meter, frameSeq, commandCode);
                    LOGGER.info(">>> ElecMeterEncoder, getSendMessage, CMD_CTRL_STATE: {}",
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                //定时拉闸状态
                case CommandCode.CMD_TIMING_BRAKE_STATE: {
                    int meter = Integer.valueOf(meterAddress);
                    if (Objects.isNull(map.get("lineNo")) || Strings.isNullOrEmpty(map.get("lineNo").toString())) {
                        throw new Exception("lineNo is invalid, must equal 1 or 2");
                    }
                    int lineNo = Integer.valueOf(map.get("lineNo").toString());
                    byteTpl = ElecMsgByteTplReg.queryTimingBrakeState(conAddress, (byte)meter, lineNo, frameSeq);
                    LOGGER.info(">>> ElecMeterEncoder, getSendMessage, CMD_TIMING_BRAKE_STATE: {}",
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }

                //查询门限功率,线路总功率现在
                case CommandCode.CMD_THRESHOLD_POWER: {
                    int meter = Integer.valueOf(meterAddress);
                    if (Objects.isNull(map.get("lineNo")) || Strings.isNullOrEmpty(map.get("lineNo").toString())) {
                        throw new Exception("lineNo is invalid, must equal 1 or 2");
                    }
                    int lineNo = Integer.valueOf(map.get("lineNo").toString());
                    if (lineNo !=1 && lineNo !=2) {
                        throw new Exception("lineNo is invalid, must equal 1 or 2");
                    }
                    byteTpl = ElecMsgByteTplReg.queryThresholdPower(conAddress, (byte)meter, lineNo, frameSeq);
                    LOGGER.info(">>> ElecMeterEncoder, getSendMessage, CMD_TIMING_BRAKE_STATE: {}",
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                //查询单一门限功率，电器功率限制
                case CommandCode.CMD_SINGLE_THRESHOLD_POWER: {
                    int meter = Integer.valueOf(meterAddress);
                    if (Objects.isNull(map.get("lineNo")) || Strings.isNullOrEmpty(map.get("lineNo").toString())) {
                        throw new Exception("lineNo is invalid, must equal 1 or 2");
                    }
                    int lineNo = Integer.valueOf(map.get("lineNo").toString());
                    if (lineNo !=1 && lineNo !=2) {
                        throw new Exception("lineNo is invalid, must equal 1 or 2");
                    }
                    byteTpl = ElecMsgByteTplReg.querySingleThresholdPower(conAddress, (byte)meter, lineNo, frameSeq);
                    LOGGER.info(">>> ElecMeterEncoder, getSendMessage, CMD_TIMING_BRAKE_STATE: {}",
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }

                //======
                //设置电价
                case CommandCode.CMD_SET_UNIT_PRICE: {
                    int meter = Integer.valueOf(meterAddress);
                    if (Objects.isNull(map.get("price")) || Strings.isNullOrEmpty(map.get("price").toString())) {
                        throw new Exception("price is invalid");
                    }
                    float price = Float.valueOf(map.get("price").toString());
                    byteTpl = ElecMsgByteTplReg.cmdSetUnitPrice(conAddress, (byte)meter, price, frameSeq);
                    LOGGER.info(">>> ElecMeterEncoder, getSendMessage, CMD_TIMING_BRAKE_STATE: {}",
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }

                //定时开闸关闸
                case CommandCode.CMD_SET_TIMING_OPENCLOSE: {
                    int meter = Integer.valueOf(meterAddress);
                    if (Objects.isNull(map.get("lineNo")) || Strings.isNullOrEmpty(map.get("lineNo").toString())) {
                        throw new Exception("lineNo is invalid, must equal 1 or 2");
                    }
                    int lineNo = Integer.valueOf(map.get("lineNo").toString());
                    if (lineNo !=1 && lineNo !=2) {
                        throw new Exception("lineNo is invalid, must equal 1 or 2");
                    }
                    if (Objects.isNull(map.get("state")) || Strings.isNullOrEmpty(map.get("state").toString())) {
                        throw new Exception("state is invalid, must equal OPEN or CLOSE");
                    }
                    String state = map.get("state").toString();
                    if (!state.equals(CommandVal.CLOSE) && !state.equals(CommandVal.OPEN)) {
                        throw new Exception("state is invalid, must equal OPEN or CLOSE");
                    }
                    byteTpl = ElecMsgByteTplReg.setTimingOpenCloseBrake(conAddress, (byte)meter, lineNo, state, frameSeq);
                    LOGGER.info(">>> ElecMeterEncoder, getSendMessage, CMD_TIMING_BRAKE_STATE: {}",
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                case CommandCode.CMD_SET_PROTECT_ELEC:
                case CommandCode.CMD_CANCEL_PROTECT_ELEC:
                case CommandCode.CMD_SET_LIMIT_ELEC:
                case CommandCode.CMD_CANCEL_LIMIT_ELEC:
                case CommandCode.CMD_OVER_POWER_CLEAR: {
                    int meter = Integer.valueOf(meterAddress);
                    if (Objects.isNull(map.get("lineNo")) || Strings.isNullOrEmpty(map.get("lineNo").toString())) {
                        throw new Exception("lineNo is invalid, must equal 1 or 2");
                    }
                    int lineNo = Integer.valueOf(map.get("lineNo").toString());
                    if (lineNo !=1 && lineNo !=2) {
                        throw new Exception("lineNo is invalid, must equal 1 or 2");
                    }
                    String ctrlType= "";
                    if (commandCode.equals(CommandCode.CMD_SET_PROTECT_ELEC)) { ctrlType = CommandVal.SET_PROTECT_ELEC; }
                    else if (commandCode.equals(CommandCode.CMD_CANCEL_PROTECT_ELEC)) { ctrlType = CommandVal.CANCEL_PROTECT_ELEC; }
                    else if (commandCode.equals(CommandCode.CMD_SET_LIMIT_ELEC)) { ctrlType = CommandVal.SET_LIMIT_ELEC; }
                    else if (commandCode.equals(CommandCode.CMD_CANCEL_LIMIT_ELEC)) { ctrlType = CommandVal.CANCEL_LIMIT_ELEC; }
                    else if (commandCode.equals(CommandCode.CMD_OVER_POWER_CLEAR)) { ctrlType = CommandVal.OVER_POWER_CLEAR; }

                    byteTpl = ElecMsgByteTplReg.send_ammeter_ctrl_req_proc(conAddress, (byte)meter, lineNo, ctrlType, frameSeq);
                    LOGGER.info(">>> ElecMeterEncoder, getSendMessage, commandCode: {}, {}", commandCode,
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                //设置门限功率
                case CommandCode.CMD_OVER_POWER_SET: {
                    int meter = Integer.valueOf(meterAddress);
                    //ctlType值为total设置的1或者2号线路的门限总功率，为single的话设置的是1或者2号线路的电器功率
                    String ctrlType = map.get("ctrl").toString();
                    int limitPower = Integer.valueOf(map.get("limitPower").toString());
                    if (Objects.isNull(map.get("lineNo")) || Strings.isNullOrEmpty(map.get("lineNo").toString())) {
                        throw new Exception("lineNo is invalid, must equal 1 or 2");
                    }
                    int lineNo = Integer.valueOf(map.get("lineNo").toString());
                    if (lineNo !=1 && lineNo !=2) {
                        throw new Exception("lineNo is invalid, must equal 1 or 2");
                    }
                    byteTpl = ElecMsgByteTplReg.send_ammeter_set_power_req_proc(
                            conAddress,
                            (byte)meter,
                            ctrlType,
                            lineNo,
                            limitPower,
                            frameSeq);
                    LOGGER.info(">>> ElecMeterEncoder, getSendMessage, commandCode: {}, {}", commandCode,
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }

                //充值
                case CommandCode.CMD_METER_RECHARGE: {
                    int meter = Integer.valueOf(meterAddress);
                    if (Objects.isNull(map.get("balance")) || Strings.isNullOrEmpty(map.get("balance").toString())) {
                        throw new Exception("balance is invalid");
                    }
                    if (Objects.isNull(map.get("times")) || Strings.isNullOrEmpty(map.get("times").toString())) {
                        throw new Exception("recharge times is invalid");
                    }
                    float balance = Float.valueOf(map.get("balance").toString());
                    String times = map.get("times").toString();
                    byteTpl = ElecMsgByteTplReg.cmdMeterRecharge(conAddress, (byte)meter, balance, times, frameSeq);
                    LOGGER.info(">>> ElecMeterEncoder, getSendMessage, command: {}, {}", commandCode,
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                //时间同步
                case CommandCode.CMD_TIME_SYNC: {

                    SimpleDateFormat df = new SimpleDateFormat("yy-yy-MM-dd-HH-mm-ss");// 设置日期格式
                    String dateStr = df.format(new Date());

                    String[] dateList = dateStr.split("-");
                    String dateHexStr  = "";
                    //dateList[1] = "17";
                    //dateList[5] ="55";
                    for(int i=0; i<6; i++) {
                        int data   = Integer.valueOf(dateList[i+1]);
                        String hexStr = String.format("%02x", data).toUpperCase();
                        dateHexStr += hexStr;
                    }

                    byte[] dateByte = CharacterUtil.hexString2Bytes(dateHexStr);

                    //byte seq = (byte)map.get("seq");
                    byte[] hubAddressBytes = CharacterUtil.getHubBinAddrFromString(conAddress);
                    byte meterByte =  (byte)(int)Integer.valueOf(meterAddress);

                    byte bpsParam      = (byte)(Integer.parseInt(StrUtil.getStr(map.get("bps"))) & 0xff);
                    byte commPortParam = (byte)(Integer.parseInt(StrUtil.getStr(map.get("commPort"))) & 0xff);
                    byte[] ret = ElecMsgByteTplReg.elec_meter_time_sync_proc(hubAddressBytes,
                            meterByte,
                            dateByte,
                            frameSeq,
                            bpsParam,
                            commPortParam);

                    ret[18] = commPortParam;
                    byte index_trans_communication_port_offset = 18;
                    byte curByte=(byte) (ret[index_trans_communication_port_offset + 1] & 0x1F);
                    ret[index_trans_communication_port_offset + 1]=(byte) ((bps << 5) | curByte);

                    LOGGER.info(">>> ElecMeterEncoder, getSendMessage, command: {}, {}", commandCode,
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(ret)));
                    byteTpl = ret;
                    break;
                }
                /*case CommondCode.CMD_ON: {
                    byteTpl = MsgByteTplReg.meterOpenClose(conAddress, meterAddress, true, frameSeq);
                    LOGGER.info(">>> ElecMeterEncoder, getSendMessage, CMD_ON: {}",
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                case CommondCode.CMD_OFF: {
                    byteTpl = MsgByteTplReg.meterOpenClose(conAddress, meterAddress, false, frameSeq);
                    LOGGER.info(">>> ElecMeterEncoder, getSendMessage, CMD_OFF: {}",
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                case CommondCode.CMD_QUERY_UNIT_PRICE: {
                    byteTpl = MsgByteTplReg.queryUnitPrice(conAddress, meterAddress, frameSeq);
                    LOGGER.info(">>> ElecMeterEncoder, getSendMessage, CMD_QUERY_UNIT_PRICE: {}",
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                case CommondCode.CMD_QUERY_METER_AMOUNT: {
                    //冷水总用量
                    byteTpl = MsgByteTplReg.queryMeterAmount(conAddress, meterAddress, frameSeq);
                    LOGGER.info(">>> ElecMeterEncoder, getSendMessage, CMD_METER_AMOUNT: {}",
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                case CommondCode.CMD_QUERY_METER_RECHARGE_ALL: {
                    //查询冷水总充值量
                    //如果要查询冷水的剩余量，需要先查询出冷水的总充值量-冷水的总用量，在平台管理层实现
                    byteTpl = MsgByteTplReg.queryMeterRechargeAll(conAddress, meterAddress, frameSeq);
                    LOGGER.info(">>> ElecMeterEncoder, getSendMessage, CMD_QUERY_METER_RECHARGE_ALL: {}",
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }*/


                default: {
                    LOGGER.error(">>> ElecMeterEncoder, getSendMessage, commandCode invalid: {}",
                            commandCode);
                    throw new Exception("no method process for cmdCode: " + commandCode);
                    //break;
                }
            }
        } catch (Exception e) {
           LOGGER.error(">>> ElecMeterEncoder, getSendMessage, e: {}", e.getMessage());

           throw new Exception(e.getMessage());
        }

        return byteTpl;
    }
}
