package com.bx;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Maps;
import com.google.common.collect.Table;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;


@EnableSwagger2
@SpringBootApplication
public class Application {


    public static Table<String, String, Queue<Object>> table;

    static {
        table=HashBasedTable.create();
    }

    public static volatile String HEP1;
    public static volatile String METER1;
    public static volatile String HEP2;
    public static volatile String METER2;

    public static void main(String[] args) {
        initTable();
        SpringApplication.run(Application.class, args);
    }

    public static void initTable() {
        Queue<Object> var1=new LinkedBlockingQueue<>();
        table.put("0001", "750000", var1);
        table.put("0001", "750001", var1);
        table.put("0001", "750002", var1);
        table.put("0001", "750003", var1);
        table.put("0001", "750004", var1);
        table.put("0001", "750005", var1);
        table.put("0001", "750006", var1);
        table.put("0001", "750007", var1);
        table.put("0001", "750008", var1);
        table.put("0001", "750009", var1);

        Queue<Object> var2=new LinkedBlockingQueue<>();
        table.put("0002", "850001", var2);
        table.put("0002", "850002", var2);
        table.put("0002", "850003", var2);

        Queue<Object> var3=new LinkedBlockingQueue<>();
        table.put("0003", "950001", var3);
        table.put("0003", "950001", var3);
        table.put("0003", "950002", var3);

        new Thread() {
            @Override
            public void run() {

                while (true) {
                    try {
                        sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Random ra=new Random();
                    int num=ra.nextInt(5);
                    if (num >=5) {
                        num = num -5;
                    }

                    String hep="0001";
                    String meter="75000" + num;
                    HEP1 = hep;
                    METER1 = meter;

                    System.out.println(">>>1 meter: " + meter);
                    CountDownLatch countDownLatch=new CountDownLatch(1);

                    Queue<Object> queue=table.get(hep, meter);
                    Map<String, Object> map=Maps.newHashMap();
                    map.put("hep", hep);
                    map.put("meter", meter);
                    map.put("lock", countDownLatch);
                    queue.offer(map);
                    System.out.println(">>>1 start send message...");

                    try {
                        countDownLatch.await(5, TimeUnit.SECONDS);

                        System.out.println(">>>1 get resp...");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        }.start();

        new Thread() {
            @Override
            public void run() {

                while (true) {
                    try {
                        sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Random ra=new Random();
                    int num=ra.nextInt(5);
                    if (num <5) num = num + 5;

                    String hep="0001";
                    String meter="75000" + num;
                    HEP2 = hep;
                    METER2 = meter;

                    System.out.println(">>>2 meter: " + meter);
                    CountDownLatch countDownLatch=new CountDownLatch(1);

                    Queue<Object> queue=table.get(hep, meter);
                    Map<String, Object> map=Maps.newHashMap();
                    map.put("hep", hep);
                    map.put("meter", meter);
                    map.put("lock", countDownLatch);
                    queue.offer(map);
                    System.out.println(">>>2 start send message...");

                    try {
                        countDownLatch.await(5, TimeUnit.SECONDS);

                        System.out.println(">>>2 get resp...");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        }.start();

        new Thread() {
            @Override
            public void run() {
                while (true) {
/*                    Random ra=new Random();
                    int num=ra.nextInt(5);

                    String hep="0001";
                    String meter="75000" + num;*/

                    try {
                        sleep(1500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("<<< receive message, meter: " );
                    Queue<Object> queue=table.get(HEP2, METER2);
                    if (queue == null) continue;

                    Map<String, Object> map=(Map<String, Object>) queue.poll();
                    System.out.println("<<< receive map,");

                    CountDownLatch countDownLatch=(CountDownLatch) map.get("lock");
                    countDownLatch.countDown();
                }
            }
        }.start();
    }

    public static Table<String, String, Queue<Object>> getTable() {
        return table;
    }


    public void init() throws Exception {
        /*System.out.println(">>> init");
        String url="http://crh.gdcjxy.com:9000/api/login?account=hujian&password=1&type=1";
        String account="hujian";
        String password="1";
        String type="1";

        String json="{\n" +
                "\t\"account\": \"$account\",\n" +
                "\t\"password\": \"$password\",\n" +
                "\t\"type\": \"$type\"\n" +
                "}";
        json=json.replace("$account", account)
                .replace("$password", password)
                .replace("$type", type);
        CloseableHttpResponse response=HttpUtil.sendHttpRequest(url, "", json);
        if (response != null) {
            response.close();
        }*/

        System.out.println();

    }


}
