package com.bx.controller;

import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


@RestController
@RequestMapping("elecmeter")
@Api(value="elecmeter", description="elecmeter", tags={"电表"})
@Scope("prototype")
public class ElecMeterController {
    private static final Logger log=LoggerFactory.getLogger(ElecMeterController.class);


    @RequestMapping(value="/elec", method=RequestMethod.POST)
    public Map<String, Object> reqIndex(@RequestBody Map<String, Object> map) throws Exception {

        CountDownLatch countDownLatch = new CountDownLatch(1);
        System.out.println(">>> elec request...");

        countDownLatch.await(5, TimeUnit.SECONDS);

        System.out.println("");

        return Maps.newHashMap();
    }

}
