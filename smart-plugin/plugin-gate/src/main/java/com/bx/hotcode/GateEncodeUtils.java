package com.bx.hotcode;

import com.bx.utils.CRC16;
import com.bx.utils.CharacterUtil;

import java.io.Serializable;
import java.util.Map;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-08-01 13:37
 **/
public class GateEncodeUtils {

    public static byte[] getChannelParamsBytes(String conAddress,
                                               String meterAddress,
                                               byte frameSeq,
                                               Map map) throws Exception {

        StringBuffer cmdstr = new StringBuffer();
        cmdstr.append(GateConst.GATE_FRAME_HEADER);
        cmdstr.append(GateConst.GATE_FRAME_TYPE_DOWN);
        String conHex = Integer.toHexString(Integer.valueOf(conAddress));
        cmdstr.append("00" + CharacterUtil.hexStringToString(conHex));
        //cmdstr.append(GateConst.DEVICE_ADDRESS);
        cmdstr.append(GateConst.FRAME_INDEX);
        cmdstr.append(GateConst.DEVICE_TYPE);
        cmdstr.append(GateConst.GATE_CODE_GROUP_PARAM_SETTING);
        cmdstr.append(GateConst.GATE_CODE_READ_CHANNEL_PARAM);
        cmdstr.append("0000");
        cmdstr.append(GateConst.GATE_FRAME_END);
        byte[] bt = CharacterUtil.hexString2Bytes(cmdstr.toString());
        cmdstr.append(CRC16.calcCrc16(bt));
        cmdstr.append(GateConst.GATE_MARK_END);
        return CharacterUtil.hexString2Bytes(cmdstr.toString());
    }



    public static byte[] getAddUserBytes(String conAddress,
                                 String meterAddress,
                                 byte frameSeq,
                                 Map map) throws Exception {
        String cardNo = map.get("cardNo").toString();
        String cardType = map.get("cardType").toString();
        String cardStatus = map.get("cardStatus").toString();
        String startTime = map.get("startTime").toString();
        String endTime = map.get("endTimd").toString();
        String name = map.get("name").toString();
        String job = map.get("job").toString();

        StringBuffer cmdstr = new StringBuffer();
        cmdstr.append(GateConst.GATE_FRAME_HEADER);
        cmdstr.append(GateConst.GATE_FRAME_TYPE_DOWN);

        String conHex = Integer.toHexString(Integer.valueOf(conAddress));
        cmdstr.append("00" + CharacterUtil.hexStringToString(conHex));

        cmdstr.append(GateConst.FRAME_INDEX);
        cmdstr.append(GateConst.DEVICE_TYPE);
        cmdstr.append(GateConst.GATE_CODE_GROUP_USER_MANAGE);
        cmdstr.append(GateConst.GATE_CODE_ADD_USER);
        cmdstr.append(GateConst.GATE_CODE_ADD_USER_DATA_LEN);
        // 卡号+类型+状态+SUM+CNT+开始时间+结束时间+姓名+职称
        GateConst.setCardHex(cardNo, cmdstr);
        cmdstr.append(CharacterUtil.toHexString(cardType));
        cmdstr.append(CharacterUtil.toHexString(cardStatus));
        cmdstr.append("2032333320353636");  //sum+cnt
        cmdstr.append(CharacterUtil.toHexString(startTime));
        cmdstr.append(CharacterUtil.toHexString(endTime));
        cmdstr.append(CharacterUtil.unicodeToHexString(name));
        cmdstr.append(CharacterUtil.unicodeToHexString(job));
        cmdstr.append(GateConst.GATE_FRAME_END);
        byte[] bt = CharacterUtil.hexString2Bytes(cmdstr.toString());
        cmdstr.append(CRC16.calcCrc16(bt));
        cmdstr.append(GateConst.GATE_MARK_END);
        return CharacterUtil.hexString2Bytes(cmdstr.toString());
    }

    public static byte[] getDeleteUserBytes(String conAddress,
                                            String meterAddress,
                                            byte frameSeq,
                                            Map map) throws Exception {
        String cardNo = map.get("cardNo").toString();
        StringBuffer cmdstr = new StringBuffer();
        cmdstr.append(GateConst.GATE_FRAME_HEADER);
        cmdstr.append(GateConst.GATE_FRAME_TYPE_DOWN);
        String conHex = Integer.toHexString(Integer.valueOf(conAddress));
        cmdstr.append("00" + CharacterUtil.hexStringToString(conHex));
        //cmdstr.append(GateConst.DEVICE_ADDRESS);
        cmdstr.append(GateConst.FRAME_INDEX);
        cmdstr.append(GateConst.DEVICE_TYPE);
        cmdstr.append(GateConst.GATE_CODE_GROUP_USER_MANAGE);
        cmdstr.append(GateConst.GATE_CODE_DELETE_USER);
        cmdstr.append(GateConst.GATE_CODE_DELETE_USER_DATA_LEN);
        GateConst.setCardHex(cardNo, cmdstr);
        cmdstr.append(GateConst.GATE_FRAME_END);
        byte[] bt = CharacterUtil.hexString2Bytes(cmdstr.toString());
        cmdstr.append(CRC16.calcCrc16(bt));
        cmdstr.append(GateConst.GATE_MARK_END);
        return CharacterUtil.hexString2Bytes(cmdstr.toString());
    }

    public static byte[] getDeleteAllUserBytes(String conAddress,
                                            String meterAddress,
                                            byte frameSeq,
                                            Map map) throws Exception {
        StringBuffer cmdstr = new StringBuffer();
        cmdstr.append(GateConst.GATE_FRAME_HEADER);
        cmdstr.append(GateConst.GATE_FRAME_TYPE_DOWN);
        String conHex = Integer.toHexString(Integer.valueOf(conAddress));
        cmdstr.append("00" + CharacterUtil.hexStringToString(conHex));
        //cmdstr.append(GateConst.DEVICE_ADDRESS);
        cmdstr.append(GateConst.FRAME_INDEX);
        cmdstr.append(GateConst.DEVICE_TYPE);
        cmdstr.append(GateConst.GATE_CODE_GROUP_USER_MANAGE);
        cmdstr.append(GateConst.GATE_CODE_DELETE_ALL_USER);
        cmdstr.append("0000");
        cmdstr.append(GateConst.GATE_FRAME_END);
        byte[] bt = CharacterUtil.hexString2Bytes(cmdstr.toString());
        cmdstr.append(CRC16.calcCrc16(bt));
        cmdstr.append(GateConst.GATE_MARK_END);
        return CharacterUtil.hexString2Bytes(cmdstr.toString());
    }

    public static byte[] getRemoteOpen(String conAddress,
                                       String meterAddress,
                                       byte frameSeq,
                                       Map map) throws Exception {

        String cardNo = map.get("cardNo").toString();
        String dir = map.get("dir").toString();
        StringBuffer cmdstr = new StringBuffer();
        cmdstr.append(GateConst.GATE_FRAME_HEADER);
        cmdstr.append(GateConst.GATE_FRAME_TYPE_DOWN);
        String conHex = Integer.toHexString(Integer.valueOf(conAddress));
        cmdstr.append("00" + CharacterUtil.hexStringToString(conHex));
        //cmdstr.append(GateConst.DEVICE_ADDRESS);
        cmdstr.append(GateConst.FRAME_INDEX);
        cmdstr.append(GateConst.DEVICE_TYPE);
        cmdstr.append(GateConst.GATE_CODE_GROUP_REMOTE_CTRL);
        cmdstr.append(GateConst.GATE_CODE_REMOTE_OPEN);
        cmdstr.append(GateConst.GATE_CODE_REMOTE_OPEN_DOOR_DATA_LEN);
        //卡号+方向
        GateConst.setCardHex(cardNo, cmdstr);
        cmdstr.append(CharacterUtil.toHexString(dir));
        cmdstr.append(GateConst.GATE_FRAME_END);

        byte[] bt = CharacterUtil.hexString2Bytes(cmdstr.toString());
        cmdstr.append(CRC16.calcCrc16(bt));
        cmdstr.append(GateConst.GATE_MARK_END);
        return CharacterUtil.hexString2Bytes(cmdstr.toString());
    }

    private static byte[] getBarFreeBytes(String conAddress,
                                          String meterAddress,
                                          byte frameSeq,
                                          Map map,
                                          String status) throws  Exception {

        String cardNo = map.get("cardNo").toString();
        String dir = map.get("dir").toString();
        StringBuffer cmdstr = new StringBuffer();
        cmdstr.append(GateConst.GATE_FRAME_HEADER);
        cmdstr.append(GateConst.GATE_FRAME_TYPE_DOWN);
        String conHex = Integer.toHexString(Integer.valueOf(conAddress));
        cmdstr.append("00" + CharacterUtil.hexStringToString(conHex));
        //cmdstr.append(GateConst.DEVICE_ADDRESS);
        cmdstr.append(GateConst.FRAME_INDEX);
        cmdstr.append(GateConst.DEVICE_TYPE);
        cmdstr.append(GateConst.GATE_CODE_GROUP_REMOTE_CTRL);
        cmdstr.append(GateConst.GATE_CODE_BAR_FREE);
        cmdstr.append(GateConst.GATE_CODE_BAR_FREE_DATA_LEN);
        // 卡号+方向+开启
        GateConst.setCardHex(cardNo, cmdstr);
        cmdstr.append(CharacterUtil.toHexString(dir));

        //"1"表示enable, "0"表示disable
        cmdstr.append(CharacterUtil.toHexString(status));
        cmdstr.append(GateConst.GATE_FRAME_END);
        byte[] bt = CharacterUtil.hexString2Bytes(cmdstr.toString());
        cmdstr.append(CRC16.calcCrc16(bt));
        cmdstr.append(GateConst.GATE_MARK_END);
        return CharacterUtil.hexString2Bytes(cmdstr.toString());
    }

    public static byte[] getBarFreeEnableBytes(String conAddress,
                                               String meterAddress,
                                               byte frameSeq,
                                               Map map) throws Exception {
        return getBarFreeBytes(conAddress,
                meterAddress,
                frameSeq,
                map,
                "1");
    }

    public static byte[] getBarFreeDisableBytes(String conAddress,
                                                String meterAddress,
                                                byte frameSeq,
                                                Map map) throws Exception {
        return getBarFreeBytes(conAddress,
                meterAddress,
                frameSeq,
                map,
                "0");

    }


    private static byte[] getRemoteAlwaysOpenBytes(String conAddress,
                                                   String meterAddress,
                                                   byte frameSeq,
                                                   Map map,
                                                   String status) throws Exception {
        String cardNo = map.get("cardNo").toString();
        String dir = map.get("dir").toString();
        StringBuffer cmdstr = new StringBuffer();
        cmdstr.append(GateConst.GATE_FRAME_HEADER);
        cmdstr.append(GateConst.GATE_FRAME_TYPE_DOWN);
        String conHex = Integer.toHexString(Integer.valueOf(conAddress));
        cmdstr.append("00" + CharacterUtil.hexStringToString(conHex));
        //cmdstr.append(GateConst.DEVICE_ADDRESS);
        cmdstr.append(GateConst.FRAME_INDEX);
        cmdstr.append(GateConst.DEVICE_TYPE);
        cmdstr.append(GateConst.GATE_CODE_GROUP_REMOTE_CTRL);
        cmdstr.append(GateConst.GATE_CODE_REMOTE_ALWAYS_OPEN);
        cmdstr.append(GateConst.GATE_CODE_REMOTE_ALWAYS_OPEN_DATA_LEN);
        // 卡号+方向+开启
        GateConst.setCardHex(cardNo, cmdstr);
        cmdstr.append(CharacterUtil.toHexString(dir));
        cmdstr.append(CharacterUtil.toHexString(status));
        cmdstr.append(GateConst.GATE_FRAME_END);
        byte[] bt = CharacterUtil.hexString2Bytes(cmdstr.toString());
        cmdstr.append(CRC16.calcCrc16(bt));
        cmdstr.append(GateConst.GATE_MARK_END);
        return CharacterUtil.hexString2Bytes(cmdstr.toString());
    }
    public static byte[] getRemoteAlwaysOpenEnableBytes(String conAddress,
                                                String meterAddress,
                                                byte frameSeq,
                                                Map map) throws Exception {
        return getRemoteAlwaysOpenBytes(conAddress,
                meterAddress,
                frameSeq,
                map,
                "1");
    }

    public static byte[] getRemoteAlwaysOpenDisableBytes(String conAddress,
                                                        String meterAddress,
                                                        byte frameSeq,
                                                        Map map) throws Exception {
        return getRemoteAlwaysOpenBytes(conAddress,
                meterAddress,
                frameSeq,
                map,
                "0");
    }




    private static byte[] getRemoteAlwaysCloseBytes(String conAddress,
                                                   String meterAddress,
                                                   byte frameSeq,
                                                   Map map,
                                                   String status) throws Exception {
        String cardNo = map.get("cardNo").toString();
        String dir = map.get("dir").toString();
        StringBuffer cmdstr = new StringBuffer();
        cmdstr.append(GateConst.GATE_FRAME_HEADER);
        cmdstr.append(GateConst.GATE_FRAME_TYPE_DOWN);
        String conHex = Integer.toHexString(Integer.valueOf(conAddress));
        cmdstr.append("00" + CharacterUtil.hexStringToString(conHex));
        //cmdstr.append(GateConst.DEVICE_ADDRESS);
        cmdstr.append(GateConst.FRAME_INDEX);
        cmdstr.append(GateConst.DEVICE_TYPE);
        cmdstr.append(GateConst.GATE_CODE_GROUP_REMOTE_CTRL);
        cmdstr.append(GateConst.GATE_CODE_REMOTE_ALWAYS_CLOSE);
        cmdstr.append(GateConst.GATE_CODE_REMOTE_ALWAYS_CLOSE_DATA_LEN);
        // 卡号+方向+开启
        GateConst.setCardHex(cardNo, cmdstr);
        cmdstr.append(CharacterUtil.toHexString(dir));
        cmdstr.append(CharacterUtil.toHexString(status));
        cmdstr.append(GateConst.GATE_FRAME_END);
        byte[] bt = CharacterUtil.hexString2Bytes(cmdstr.toString());
        cmdstr.append(CRC16.calcCrc16(bt));
        cmdstr.append(GateConst.GATE_MARK_END);
        return CharacterUtil.hexString2Bytes(cmdstr.toString());
    }
    public static byte[] getRemoteAlwaysCloseEnableBytes(String conAddress,
                                                        String meterAddress,
                                                        byte frameSeq,
                                                        Map map) throws Exception {
        return getRemoteAlwaysCloseBytes(conAddress,
                meterAddress,
                frameSeq,
                map,
                "1");
    }

    public static byte[] getRemoteAlwaysCloseDisableBytes(String conAddress,
                                                         String meterAddress,
                                                         byte frameSeq,
                                                         Map map) throws Exception {
        return getRemoteAlwaysCloseBytes(conAddress,
                meterAddress,
                frameSeq,
                map,
                "0");
    }



}
