package com.bx.hotcode;

import com.bx.plugin.CodeHandle;
import com.bx.plugin.CodeService;
import com.bx.plugin.HDType;
import com.bx.plugin.ISendMessageEncode;
import com.bx.utils.CRC16;
import com.bx.utils.CharacterUtil;
import com.google.common.base.Strings;
import com.smart.command.CommandCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

//import org.apache.curator.framework.recipes.locks.Locker;


/*
 * @ program: plugin-hotwater
 * @ description: d
 * @ author: admin
 * @ create: 2019-04-03 09:40
 **/
@CodeService(hdType= HDType.HDTYPE_GATE, dir=HDType.MSG_DIR_SEND)
public class GateEncoder implements ISendMessageEncode {
    private static final Logger LOGGER = LoggerFactory.getLogger(GateEncoder.class);

    @CodeHandle(name="codeHandle")
    public byte[] getOpen(String str) {
        return  null;
    }

    @PostConstruct
    public void init() {
        System.out.println();
    }

    /**
     * @ Description: 发送消息到硬件, 根据commandCode参数(命令),获取到字节码
     * 闸机消息内容发送的是字符格式
     * @ Author: Admin
    **/
    @Override
    public byte[] getSendMessage(String commandCode,
                                   String conAddress,
                                   String meterAddress,
                                   byte frameSeq,
                                   Map map) throws Exception{
        //帧段: |帧头STX|帧类型|设备地址|帧索引|设备类型|命令组|命令号|数据长度|数据内容|帧尾ETX|CRC16|结束EOT|
        //内容: |0x02   |  									                        0x03          0x04
        //长度: |  1    | 1	  | 2	  |2    | 1     | 1    |1    |   2   |       | 	1    |2字节 |1字节  |
        byte[] byteTpl = null;
        try {
            switch (commandCode) {
                //读取通道参数
                case CommandCode.CMD_GATE_READ_CHANNEL_PARAM: {
                    byteTpl = GateEncodeUtils.getChannelParamsBytes(conAddress,
                            meterAddress,
                            frameSeq,
                            map);
                    break;
                }
                //用户管理
                case CommandCode.CMD_GATE_ADD_USER: {
                    byteTpl = GateEncodeUtils.getAddUserBytes(conAddress,
                            meterAddress,
                            frameSeq,
                            map);
                    break;
                }
                //删除用户
                case CommandCode.CMD_GATE_DELETE_USER: {
                    byteTpl = GateEncodeUtils.getDeleteUserBytes(conAddress,
                            meterAddress,
                            frameSeq,
                            map);
                    break;
                }
                //删除所有用户
                case CommandCode.CMD_GATE_DELETE_ALL_USER: {
                    byteTpl = GateEncodeUtils.getDeleteAllUserBytes(conAddress,
                            meterAddress,
                            frameSeq,
                            map);
                    break;
                }
                //远程开闸
                case CommandCode.CMD_GATE_REMOTE_OPEN: {
                    byteTpl = GateEncodeUtils.getRemoteOpen(conAddress,
                            meterAddress,
                            frameSeq,
                            map);
                    break;
                }
                //无障碍模式开启
                case CommandCode.CMD_GATE_BAR_FREE_ENABLE: {
                    byteTpl = GateEncodeUtils.getBarFreeEnableBytes(conAddress,
                            meterAddress,
                            frameSeq,
                            map);
                    break;
                }
                //无障碍模式关闭
                case CommandCode.CMD_GATE_BAR_FREE_DISABLE: {
                    byteTpl = GateEncodeUtils.getBarFreeDisableBytes(conAddress,
                            meterAddress,
                            frameSeq,
                            map);
                    break;
                }
                //远程常开打开
                case CommandCode.CMD_GATE_REMOTE_ALWAYS_OPEN_ENABLE: {
                    byteTpl = GateEncodeUtils.getRemoteAlwaysOpenEnableBytes(conAddress,
                            meterAddress,
                            frameSeq,
                            map);
                    break;
                }
                //远程常开关闭
                case CommandCode.CMD_GATE_REMOTE_ALWAYS_OPEN_DISABLE: {
                    byteTpl = GateEncodeUtils.getRemoteAlwaysOpenDisableBytes(conAddress,
                            meterAddress,
                            frameSeq,
                            map);
                    break;
                }
                //远程常关打开
                case CommandCode.CMD_GATE_REMOTE_ALWAYS_CLOSE_ENABLE: {
                    byteTpl = GateEncodeUtils.getRemoteAlwaysCloseEnableBytes(conAddress,
                            meterAddress,
                            frameSeq,
                            map);
                    break;
                }
                //远程常关关闭
                case CommandCode.CMD_GATE_REMOTE_ALWAYS_CLOSE_DISABLE: {
                    byteTpl = GateEncodeUtils.getRemoteAlwaysCloseDisableBytes(conAddress,
                            meterAddress,
                            frameSeq,
                            map);
                    break;
                }

                default: {
                    LOGGER.error(">>> GateEncoder, getSendMessage, commandCode invalid: {}",
                            commandCode);
                    break;
                }
            }
        } catch (Exception e) {
           LOGGER.error(">>> GateEncoder, getSendMessage, e: {}", e.getMessage());
        }

        return byteTpl;
    }

    public static void main(String[] args) {

        String LOCK_UPDATE_BEGIN = "AA%CONADDR%LOCKADDR0801%MSG%CRC55";

        LOCK_UPDATE_BEGIN = LOCK_UPDATE_BEGIN.replace("%CONADDR", "0B");
        System.out.println(LOCK_UPDATE_BEGIN);
        int var1 = 1;
        String str = Integer.toHexString(Integer.valueOf("13"));
        String s = Strings.padStart(str, 2, '0');
        System.out.println(s);

        byte[] ret = CharacterUtil.hexString2Bytes("00ad");
        System.out.println(ret);

        int aa= Integer.valueOf("01");
        String hex = CharacterUtil.hexStringToString(String.valueOf(aa));
        System.out.println(hex);
        String var2 = CharacterUtil.toHexString("4");
        System.out.println(var2);

    }

}
