package com.bx.hotcode;

import com.alibaba.fastjson.JSONObject;
import com.bx.config.LockerInitConfig;
import com.bx.constants.MsgConst;
import com.bx.message.inside.IMessage;
import com.bx.message.inside.MsgCommon;
import com.bx.message.msgtpl.MsgByteTpl;
import com.bx.plugin.CodeService;
import com.bx.plugin.HDType;
import com.bx.plugin.IRespMessageDecode;
import com.bx.stream.StreamContainer;
import com.bx.stream.StreamContainerWrap;
import com.bx.utils.CharacterUtil;
import com.bx.utils.StrUtil;
import com.google.common.collect.Maps;
import com.smart.EventType;
import com.smart.command.CommandCode;
import com.smart.common.ResultConst;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Map;

import static com.bx.constants.MsgConst.FAILURE;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: admin
 * @Date: 2019/6/19
 */
@CodeService(hdType = HDType.HDTYPE_GATE, dir = HDType.MSG_DIR_RESP)
public class GateDecoder implements IRespMessageDecode {
    private static final Logger log = LoggerFactory.getLogger(GateDecoder.class);
    private StreamContainer streamContainer = null;
    //帧段: |帧头STX|帧类型|设备地址|帧索引|设备类型|命令组|命令号|数据长度|数据内容|帧尾ETX|CRC16|结束EOT|
    //内容: |0x02   |  									                        0x03          0x04
    //长度: |  1    | 1	  | 2	  |2    | 1     | 1    |1    |   2   |       | 	1    |2字节 |1字节  |
    @Override
    public IMessage getRespMessageObj(byte[] resp, String respMsg) {
        IMessage map = null;

        String cmd = CharacterUtil.bytesToHexString(resp);
        // 获取锁地址
        String lockAddress = String.format("%02d", (resp[2] & 0xFF));
        int deviceAddress = Integer.parseInt(CharacterUtil.bytesToHexString(resp[2]), 16);
        deviceAddress += Integer.parseInt(CharacterUtil.bytesToHexString(resp[3]), 16);
        // 获取集中器地址
        String hexAddress = String.format("%02d", (deviceAddress));
        // 获取命令组
        String commandGroup = CharacterUtil.bytesToHexString(resp[7]);
        // 获取远程命令
        String command = CharacterUtil.bytesToHexString(resp[8]);

        // 获取接收数据 (回复0为失败，非0为成功)
        String receiveData = CharacterUtil.bytesToHexString(resp[5]);

        String msgCommonType = MsgConst.MSG_TYPE_RESP;
        String hexStr = "";
        // 远程操作指令
        Map<String, Object> data = Maps.newHashMap();
        data.put("hubAddr", hexAddress);
        data.put("meterAddress", "00");
        data.put("commandGroup", commandGroup);
        data.put("commandCode", command);
        data.put("result", MsgConst.SUCCESS);
        switch (commandGroup) {
            case GateConst.GATE_CODE_GROUP_PARAM_SETTING: {
                if (command.equals(GateConst.GATE_CODE_READ_CHANNEL_PARAM)) {
                    //心跳
                }
                else {

                }
                break;
            }
            case GateConst.GATE_CODE_GROUP_USER_MANAGE: {
                int val = (int)resp[11];
                if (command.equals(GateConst.GATE_CODE_ADD_USER)) {
                    data.put("status", String.valueOf(val));
                    data.put("result", val == 0 ? ResultConst.SUCCESS : ResultConst.FAIL);
                }
                else if (command.equals(GateConst.GATE_CODE_DELETE_USER)) {
                    data.put("status", String.valueOf(val));
                    data.put("result", val == 0 ? ResultConst.SUCCESS : ResultConst.FAIL);

                }
                else if (command.equals(GateConst.GATE_CODE_DELETE_ALL_USER)) {
                    data.put("status", String.valueOf(val));
                    data.put("result", val == 0 ? ResultConst.SUCCESS : ResultConst.FAIL);
                }
                else {
                    log.error(">>> GateDecoder, getRespMessageObj, invalid command: {}, commandGroup: {}",
                            command, commandGroup);
                }
                break;
            }
            case GateConst.GATE_CODE_GROUP_REMOTE_CTRL: {
                //远程开门
                if (command.equals(GateConst.GATE_CODE_REMOTE_OPEN)) {
                    int val = (int)resp[11];
                    data.put("status", String.valueOf(val));
                    data.put("result", val == 0 ? ResultConst.SUCCESS : ResultConst.FAIL);
                }
                else if (command.equals(GateConst.GATE_CODE_REMOTE_ALWAYS_OPEN)) {
                    int val = (int)resp[11];
                    data.put("status", String.valueOf(val));
                    data.put("result", val == 0 ? ResultConst.SUCCESS : ResultConst.FAIL);
                }
                else if (command.equals(GateConst.GATE_CODE_REMOTE_ALWAYS_CLOSE)) {
                    int val = (int)resp[11];
                    data.put("status", String.valueOf(val));
                    data.put("result", val == 0 ? ResultConst.SUCCESS : ResultConst.FAIL);
                }
                else if (command.equals(GateConst.GATE_CODE_BAR_FREE)) {
                    int val = (int)resp[11];
                    data.put("status", String.valueOf(val));
                    data.put("result", val == 0 ? ResultConst.SUCCESS : ResultConst.FAIL);
                }
                break;
            }
            case GateConst.GATE_CODE_GROUP_RECORD_MANAGE: {

                break;
            }
            case GateConst.GATE_CODE_GROUP_UP_INFO: {
                if (command.equals(GateConst.GATE_CODE_UP_INFO_OR_RESP)) {

                }
                else if (command.equals(GateConst.GATE_CODE_UP_INFO_NO_RESP)) {

                }
                else {

                }

                break;
            }
            default: {
                log.error(">>> GateDecoder, getRespMessageObj, invalid cmdGroup: {}", commandGroup);
                break;
            }
        }

        MsgCommon iMessage = MsgCommon.MsgCommonBuilder.aMsgCommon()
                .withMsgType(msgCommonType)
                .withMsgAddr(hexAddress)
                .withMsgSubAddr(lockAddress)
                .withState("ok")
                .build();
        iMessage.setRespStr(StrUtil.splitSpace(CharacterUtil.bytesToHexString(resp)));
        iMessage.getResp().add(data);
        return iMessage;
    }

}
