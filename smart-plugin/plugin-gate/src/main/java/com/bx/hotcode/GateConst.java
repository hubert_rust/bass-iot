package com.bx.hotcode;

import com.bx.utils.CharacterUtil;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-08-01 17:09
 **/
public class GateConst {

    public static final String GATE_FRAME_HEADER = "0x02";
    public static final String GATE_FRAME_END = "03"; //帧尾
    public static final String GATE_MARK_END = "04";       //结束符
    public static final String GATE_FRAME_TYPE_DOWN ="0x01";       //PC下发设置包;
    public static final String GATE_FRAME_TYPE_DOWN_RESP = "0x02"; //DEVICE应答PC下发设置包;
    public static final String GATE_FRAME_TYPE_UP = "0x03";        //DEVICE主动上传请求包(PC需要应答)
    public static final String GATE_FRAME_TYPE_UP_RESP = "0x04";   //PC应答Device上传请求包
    public static final String GATE_FRAME_TYPE_UP_NO_RESP = "0x05";//DEVICE 上传状态包(可不回复)

    //设备地址,下行消息可以固定, 上行消息必须带
    public static final String DEVICE_ADDRESS = "0001";

    //帧索引
    public static final String FRAME_INDEX = "0001";
    //设备类型(闸机设备)
    public static final String DEVICE_TYPE = "02";

	// 查询用户数据长度
	public static final String GATE_CODE_ADD_USER_DATA_LEN="0062";     //16进制
	// 删除用户数据长度
    public static final String GATE_CODE_DELETE_USER_DATA_LEN="0014";
    //远程开闸数据长度
    public static final String GATE_CODE_REMOTE_OPEN_DOOR_DATA_LEN="0015";
	//无障碍模式开启数据长度
    public static final String GATE_CODE_BAR_FREE_DATA_LEN="0016";
	//远程常开数据长度
    public static final String GATE_CODE_REMOTE_ALWAYS_OPEN_DATA_LEN="0016";
    //远程常关数据长度
    public static final String GATE_CODE_REMOTE_ALWAYS_CLOSE_DATA_LEN="0016";



    public static final String GATE_CODE_GROUP_PARAM_SETTING = "0xA0"; //命令组: 参数设置
    public static final String GATE_CODE_READ_CHANNEL_PARAM = "0x04";  //读取通道参数


    public static final String GATE_CODE_GROUP_USER_MANAGE = "0xA1";   //命令组: 用户管理
    public static final String GATE_CODE_ADD_USER = "0x05";            //添加用户
    public static final String GATE_CODE_DELETE_USER = "0x09";         //删除用户
    public static final String GATE_CODE_DELETE_ALL_USER = "0xA0";     //删除所有用户

    public static final String GATE_CODE_GROUP_REMOTE_CTRL = "0xA2";   //远程控制
    public static final String GATE_CODE_REMOTE_OPEN = "0x0D";         //远程开闸
    public static final String GATE_CODE_REMOTE_ALWAYS_OPEN = "0x0E";  //远程常开
    public static final String GATE_CODE_REMOTE_ALWAYS_CLOSE = "0x0F"; //远程常闭
    public static final String GATE_CODE_BAR_FREE = "0x41";            //无障碍模式



    public static final String GATE_CODE_GROUP_RECORD_MANAGE = "0xA3"; //命令组: 记录管理
    public static final String GATE_CODE_GET_UNREAD_RECORD = "0x11";   //顺序读取未读记录
    public static final String GATE_CODE_GET_NEW_RECORD = "0x12";      //读取最新记录
    public static final String GATE_CODE_GET_CLEAR_ALL_RECORD = "0x13";//清除所有记录(无法恢复)

    public static final String GATE_CODE_GROUP_UP_INFO = "0xB1";       //主动上传刷卡
    public static final String GATE_CODE_UP_INFO_OR_RESP = "0x30";     //主动上传请求刷卡或条码信息，设备等等应答
    public static final String GATE_CODE_UP_INFO_NO_RESP = "0x32";     //主动上传设备状态，PC无需应答





    /**
     * 设置卡号内容
     *
     * @param cardno
     * @param cmdstr
     */
    public static void setCardHex(String cardno, StringBuffer cmdstr) {
        String cardno_hex = CharacterUtil.toHexString(cardno);
        int len = cardno_hex.length();
        if (cardno_hex.length() > 40) {
            cardno_hex = cardno_hex.substring(0, 40);
            cmdstr.append(cardno_hex);
        } else {
            for (int i = 0; i < 20 - len / 2; i++) {
                cmdstr.append("20");
            }
            cmdstr.append(cardno_hex);
        }
    }



}
