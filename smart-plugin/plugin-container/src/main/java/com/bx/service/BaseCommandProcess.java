package com.bx.service;

import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/*
 * @ program: plugin-elec
 * @ description: 维护命令
 * @ author: admin
 * @ create: 2019-03-27 13:52
 **/
@Service
public class BaseCommandProcess {
    private static final Logger LOGGER=LoggerFactory.getLogger(BaseCommandProcess.class);

    public static CountDownLatch countDownLatch;
    public static String currentMTCommand="";

    public static Map<String, String> cmdFun=Maps.newHashMap();

    public static String mtCommandProcess(Map<String, Object> map, String request) {

        if (countDownLatch != null && countDownLatch.getCount() > 0) {
            return ("其他维护命令正在执行,请稍后重试, cmd: " + currentMTCommand);
        }
        countDownLatch = new CountDownLatch(1);

        String ret="";
        try {
            String cmdCode=map.get("cmdCode").toString();
            String cmdMethod=cmdFun.get(cmdCode);

            LOGGER.info(">>> mtCommandProcess start, cmd: {}, method: {} ", cmdCode, cmdMethod);
            Method method=null;
            try {
                method= PluginMTCommandProcess.class.getMethod(cmdMethod, new Class[]{String.class});
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } finally {
                countDownLatch.countDown();
            }
            try {
                ret=(String) method.invoke(null, request);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } finally {
                countDownLatch.countDown();
            }
        }catch (Exception e) {
            countDownLatch.countDown();
        }

        return ret;
    }

}
