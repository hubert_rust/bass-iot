package com.bx.service;

import com.alibaba.fastjson.JSONObject;
import com.bx.PluginContainer;
import com.bx.constants.MsgConst;
import com.bx.enginecore.ConcentratorContextInterface;
import com.bx.enginecore.ConcentratorHolder;
import com.bx.message.inside.MsgCommon;
import com.google.common.collect.Maps;
import com.smart.command.CommandCode;
import com.smart.common.ResponseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-06 14:30
 **/
public interface IPluginFuncProcess {
    String process(Map<String, Object> map, String subType) throws Exception;
}

