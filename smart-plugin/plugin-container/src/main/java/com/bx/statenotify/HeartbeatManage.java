package com.bx.statenotify;

import com.bx.PluginContainer;
import com.bx.commonpool.AsyncNotifyThreadPool;
import com.bx.commonpool.Worker;
import com.bx.enginecore.ConcentratorHolder;
import com.bx.message.inside.IMessage;
import com.google.common.collect.Maps;
import com.google.common.collect.Queues;
import com.smart.command.CommandVal;
import com.smart.event.HWStateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @program: plugin, 从STATE_QUEUE队列中获取消息: 包括登录消息，心跳消息
 * 该STATE_QUEUE队列中的消息是收到对端的消息后，解析完，放入message loop中
 * message loop将消息传递给STATE_QUEUE
 *
 * 2019-07-12重新修改
 * 集中器状态是通过对端发心跳，登录等信息同步更新管理层
 * sub通过命令和主动检测：
 * 主动检测是判断
 * @description:
 * @author: admin
 * @create: 2019-05-07 09:49
 **/
@Component
public class HeartbeatManage {
    private static final Logger log = LoggerFactory.getLogger(HeartbeatManage.class);
    public static final int HEART_BEAT_PLUGIN_SPAN = 10; //second
    public static final int HEART_BEAT_HUB_SPAN = 10; //second,
    public static final int HEART_BEAT_METER_SPAN = 600; //second
    public static Queue<HWStateEvent> STATE_QUEUE = Queues.newConcurrentLinkedQueue();

    public static HeartBeatWorker pluginWorker = null;
    public static HeartBeatWorker hubWorker = null;  //集中器依赖对端心跳来
    public static HeartBeatWorker meterWorker = null; //将meter的监测放在keep, 进行主动监测

    static {
        pluginWorker = new HeartBeatWorker(new HeartBeatPluginTask(), HEART_BEAT_PLUGIN_SPAN);
        hubWorker = new HeartBeatWorker(new HeartBeatHubTask(), HEART_BEAT_HUB_SPAN);
        //meterWorker = new HeartBeatWorker(new HeartBeatMeterTask(), HEART_BEAT_METER_SPAN);
    }


    private static ThreadPoolExecutor executorService = new ThreadPoolExecutor(5,
            25, 5,
            TimeUnit.SECONDS,
            new ArrayBlockingQueue<>(ConcentratorHolder.conMap.size() + 1));

    public static void submitProbeWorker(String meterType, String hubAddress, String meterAddress) {
        if (ConcentratorHolder.getCon(hubAddress).getDoorState() == true) {
            HeartBeatWorker probeWorker = new HeartBeatWorker(new MeterProbeTask(meterType, hubAddress, meterAddress), 0);
            executorService.submit(probeWorker);
        }
    }
    /**
    * @Description: 集中器监测
    * @Param:
    * @return:
    * @Author: admin
    * @Date: 2019/7/16
    */
    @Deprecated
    public static void offer(String hubAddr, IMessage iMessage) {
        HWStateEvent stateEvent = new HWStateEvent();
        stateEvent.setMainClass(CommandVal.MAIN_CLASS_HUB);
        stateEvent.setPluginId(PluginContainer.pluginId);
        stateEvent.setHubAddress(hubAddr);
        stateEvent.setState(iMessage.state());
        HeartbeatManage.STATE_QUEUE.offer(stateEvent);
    }
    public static void startup() {
        pluginWorker.start();
        hubWorker.start();
        //meterWorker.start();
    }
    public static boolean restartHubWorker() {
        hubWorker.interrupt();
        hubWorker.start();
        return true;
    }

}
