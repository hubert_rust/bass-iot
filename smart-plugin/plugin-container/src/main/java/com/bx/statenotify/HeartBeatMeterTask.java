package com.bx.statenotify;

import com.alibaba.fastjson.JSONObject;
import com.bx.PluginContainer;
import com.bx.commonpool.Task;
import com.bx.enginecore.ConcentratorContext;
import com.bx.enginecore.ConcentratorHolder;
import com.bx.stream.StreamContainerWrap;
import com.google.common.collect.Lists;
import com.google.common.collect.Queues;
import com.google.common.collect.Table;
import com.smart.command.CommandVal;
import com.smart.common.ResponseState;
import com.smart.common.ResultConst;
import com.smart.event.HWStateEvent;
import com.smart.monitor.PluginMeterMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @program:
 * @description: 集中器状态监测直接查询就可以了, 批量上报，一次上报20个,
 * 在插件入口统一上报给管理层，
 * 20个大概长度1.4k
 * @author: admin
 * @create: 2019-07-12 10:48
 **/
@Deprecated
public class HeartBeatMeterTask extends Task {
    private static final Logger log = LoggerFactory.getLogger(HeartBeatMeterTask.class);
    public static final int BATCH_NUM = 10;

    public volatile int hubNum = 0;

    public Queue<HWStateEvent> METER_STATE_QUEUE = Queues.newConcurrentLinkedQueue();

    public HeartBeatMeterTask() {
    }

    public static void main(String[] args) {

        System.out.println();
    }


    /**
    * @Description: 通过集中器来分组, 超时需要在集中器设置nextseq
    * @Param:
    * @return:
    * @Author: admin
    * @Date: 2019/7/16
    */
    @Override
    public void process() throws Exception {
        hubNum = ConcentratorHolder.getConMap().size();
        while (true) {
            //保证每个集中器在同样的时间间隔都能上报
            long var1 = (long) Math.ceil((float) hubNum / BATCH_NUM);
            long probeSpan = PluginContainer.configEntity.getProbeConfig().getMeter();
            long tmSpan = (long) Math.ceil((float) (probeSpan * 1000) / var1);
            try {
                TimeUnit.MILLISECONDS.sleep(tmSpan <=0 ? 10 : tmSpan);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ResponseState sender = null;
            int index = 0;
            while (index < BATCH_NUM) {
                HWStateEvent stateEvent = METER_STATE_QUEUE.poll();
                if (Objects.isNull(stateEvent)) {
                    break;
                }

                ConcentratorContext con = null;
                try {
                    con = (ConcentratorContext) ConcentratorHolder.getConMap().get(stateEvent.getHubAddress());
                    Map<String, PluginMeterMonitor> map = con.getMeterMonitorTable().row(con.getConAddr());
                    for(String key: map.keySet()) {
                        if (con.getMeterMonitorSeq() == map.get(key).getSeqNum()) {
                            PluginMeterMonitor meterMonitor = map.get(key);
                            HeartbeatManage.submitProbeWorker(meterMonitor.getMeterType(),
                                    meterMonitor.getConcentratorAddress(),
                                    meterMonitor.getMeterAddress());
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                index++;
            }

            if (Objects.isNull(sender)) {
                putHub();
                continue;
            }

            hubNum = ConcentratorHolder.getConMap().size();
            //一次性将该plugin中的meter加入queue
            putHub();
        }
    }
    private void putHub() {
        if (METER_STATE_QUEUE.isEmpty()) {
            for (String key : ConcentratorHolder.getConMap().keySet()) {
                ConcentratorContext con = (ConcentratorContext) ConcentratorHolder.getConMap().get(key);
                HWStateEvent stateEvent = new HWStateEvent();
                stateEvent.setState("na");
                stateEvent.setHubAddress(con.getAddress());
                this.METER_STATE_QUEUE.offer(stateEvent);
            }
            ;
        }
    }

    private void put() {
        if (METER_STATE_QUEUE.isEmpty()) {
            for (Table.Cell<String, String, Object> cell: PluginContainer.getTable().cellSet()) {
                String row = cell.getRowKey();
                String col = cell.getColumnKey();
                PluginMeterMonitor meterMonitor = (PluginMeterMonitor)cell.getValue();

                HWStateEvent stateEvent = new HWStateEvent();
                stateEvent.setEventType(CommandVal.EVENT_TYPE_STATE);
                stateEvent.setPluginId(PluginContainer.pluginId);
                stateEvent.setMainClass(CommandVal.MAIN_CLASS_METER);
                stateEvent.setHubAddress(row);
                stateEvent.setSubAddress(col);
                stateEvent.setState("na");

                this.METER_STATE_QUEUE.offer(stateEvent);
            }
        }
    }
}
