package com.bx.statenotify;

import com.alibaba.fastjson.JSONObject;
import com.bx.PluginContainer;
import com.bx.commonpool.Task;
import com.bx.stream.StreamContainer;
import com.bx.stream.StreamContainerWrap;
import com.google.common.collect.Lists;
import com.smart.command.CommandCode;
import com.smart.command.CommandVal;
import com.smart.common.ResponseModel;
import com.smart.common.ResultConst;
import com.smart.event.HWStateEvent;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @program: plugin状态监测是直接上报
 * @description:
 * @author: admin
 * @create: 2019-07-12 10:48
 **/
public class HeartBeatPluginTask extends Task {
    @Override
    public void process() throws Exception {
        while (true) {
            try {
                TimeUnit.SECONDS.sleep(PluginContainer.configEntity.getProbeConfig().getPlugin());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            ResponseModel<HWStateEvent> sender = new ResponseModel<>();
            sender.setReturnCode(ResultConst.SUCCESS);
            sender.setReturnMsg(ResultConst.STATE_OK);

            List<HWStateEvent> list = Lists.newArrayList();
            HWStateEvent event = new HWStateEvent();
            event.setPluginId(PluginContainer.pluginId);
            event.setEventType(CommandVal.EVENT_TYPE_STATE);
            event.setMainClass(CommandVal.MAIN_CLASS_PLUGIN);
            event.setMeterType(CommandVal.MAIN_CLASS_PLUGIN);
            event.setState("ok");
            list.add(event);
            sender.setDatas(list);
            StreamContainerWrap.sendState(JSONObject.toJSON(sender));
        }
    }
}
