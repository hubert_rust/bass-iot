package com.bx.statenotify;

import com.alibaba.fastjson.JSONObject;
import com.bx.PluginContainer;
import com.bx.commonpool.Task;
import com.bx.enginecore.ConcentratorContext;
import com.bx.enginecore.ConcentratorHolder;
import com.bx.stream.StreamContainerWrap;
import com.google.common.collect.Lists;
import com.google.common.collect.Queues;
import com.smart.command.CommandVal;
import com.smart.common.ResponseModel;
import com.smart.common.ResponseState;
import com.smart.common.ResultConst;
import com.smart.event.HWStateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

/**
 * @program:
 * @description: 集中器状态监测直接查询就可以了, 批量上报，一次上报20个
 * 20个大概长度1.4k
 * @author: admin
 * @create: 2019-07-12 10:48
 **/
public class HeartBeatHubTask extends Task {
    private static final Logger log = LoggerFactory.getLogger(HeartBeatHubTask.class);
    public static final int BATCH_NUM = 20;

    public volatile int hubNum = 0;

    public static Queue<HWStateEvent> HUB_STATE_QUEUE = Queues.newConcurrentLinkedQueue();

    public HeartBeatHubTask() {
    }

    public static void main(String[] args) {

        System.out.println();
    }

    /**
    * @Description: 批量发送，集中器默认10发送一批，基本每个集中器60秒检测一次
    * @Param:
    * @return:
    * @Author: admin
    * @Date: 2019/7/15
    */
    @Override
    public void process() throws Exception {
        while (true) {
            hubNum = ConcentratorHolder.getConMap().size();
            //保证每个集中器在同样的时间间隔都能上报
            long var1 = (long) Math.ceil((float) hubNum / BATCH_NUM);
            long probeSpan = PluginContainer.configEntity.getProbeConfig().getHub();
            long tmSpan = (long) Math.ceil((float) (probeSpan * 1000) / var1);
            try {
                TimeUnit.MILLISECONDS.sleep(tmSpan <=0 ? 10 : tmSpan);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            ResponseState sender = null;
            int index = 0;
            while (index < BATCH_NUM) {
                HWStateEvent stateEvent = HUB_STATE_QUEUE.poll();
                if (Objects.nonNull(stateEvent)) {
                    if (sender == null) {
                        sender = new ResponseState();
                        List<HWStateEvent> list = Lists.newArrayList();
                        sender.setDatas(list);
                    }
                    String hubAddress = stateEvent.getHubAddress();
                    ConcentratorContext con = null;
                    try {
                        con = (ConcentratorContext) ConcentratorHolder.getConMap().get(hubAddress);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (Objects.nonNull(con)) {
                        stateEvent.setEventType(CommandVal.EVENT_TYPE_STATE);
                        stateEvent.setPluginId(PluginContainer.pluginId);
                        stateEvent.setMainClass(CommandVal.MAIN_CLASS_HUB);
                        stateEvent.setMeterType(con.getCmdType());
                        stateEvent.setState((con.getConState() == true) ? "ok" : "nok");

                        sender.getDatas().add(stateEvent);
                    } else {
                        log.error(" >>>HeartBeatHubTask, get con is null");
                    }
                } else {
                    break;
                }

                index++;
            }

            if (Objects.isNull(sender)) {
                put();
                continue;
            }
            sender.setReturnCode(ResultConst.SUCCESS);
            sender.setReturnMsg(ResultConst.STATE_OK);
            StreamContainerWrap.sendState(JSONObject.toJSON(sender));

            hubNum = ConcentratorHolder.getConMap().size();

            //将集中器一次加入队列,
            put();
        }
    }


    private void put() {
        if (HUB_STATE_QUEUE.isEmpty()) {
            for (String key : ConcentratorHolder.getConMap().keySet()) {
                ConcentratorContext con = (ConcentratorContext) ConcentratorHolder.getConMap().get(key);
                HWStateEvent stateEvent = new HWStateEvent();
                stateEvent.setState("na");
                stateEvent.setHubAddress(con.getAddress());
                stateEvent.setMeterType(con.getCmdType());
                this.HUB_STATE_QUEUE.offer(stateEvent);
            }
            ;
        }
    }
}
