package com.bx.plugin;

/*
 * @ program: plugin-hotwater
 * @ description: time span
 * @ author: admin
 * @ create: 2019-04-03 13:50
 **/
public class TimeSpan {
    public static final String ENCODE_START = "encode_start";
    public static final String SEND_TIME = "send_time";
    public static final String ENTER_TIME = "enter_time";
    public static final String LEAVE_TIME = "leave_time";
    public static final String RESP_TIME = "resp_time";

}
