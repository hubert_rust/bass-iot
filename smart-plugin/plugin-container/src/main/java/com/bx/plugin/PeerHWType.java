package com.bx.plugin;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-08 18:15
 **/
public class PeerHWType {
    public static final String PEER_TYPE_485 = "peer_485";
    public static final String PEER_TYPE_IP  = "peer_ip";
    public static final String PEER_TYPE_HUB  = "peer_hub";
}
