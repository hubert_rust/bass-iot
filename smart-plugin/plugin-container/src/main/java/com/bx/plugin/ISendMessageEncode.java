package com.bx.plugin;

import com.bx.message.inside.IMessage;

import java.util.Map;

/**
 * @ Description: 发生消息接口
 * @ Author: Admin
**/
public interface ISendMessageEncode {
    /**
     * @ Description: 其中meterAddress对没有集中器的(例如闸机),可能没有值
     * @ Author: Admin
    **/

    byte[] getSendMessage(String commandCode,
                                String conAddress,
                                String meterAddress,
                                byte frameSeq,
                                Map map) throws Exception;
}
