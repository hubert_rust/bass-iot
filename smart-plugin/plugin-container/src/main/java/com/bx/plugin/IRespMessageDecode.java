package com.bx.plugin;

import com.bx.message.inside.IMessage;

import java.util.Map;

/*
 * @ program: plugin-hotwater
 * @ description: 响应消息解码
 * @ author: admin
 * @ create: 2019-04-03 14:32
 **/
public interface IRespMessageDecode {
    /**
     * @ Description:
     * @ Author: Admin
    **/
    IMessage getRespMessageObj(byte[] resp, String respMsg);
}
