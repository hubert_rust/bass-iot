package com.bx.plugin;

import com.bx.message.inside.IMessage;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;

import java.util.List;
import java.util.Map;

/*
 * @ program: plugin-hotwater
 * @ description: 响应消息解码
 * @ author: admin
 * @ create: 2019-04-03 14:32
 **/
public interface IRespMessageAnalysis {
    /**
     * @ Description:
     * @ Author: Admin
     **/

    /**
     * @Description: 粘包
     * @Param:
     * @return:
     * @Author: admin
     * @Date: 2019/5/7
     */
    void splicingPackage(ByteBuf bufferIn, List<Object> out);

    /**
     * @Description: 数据有效性校验, 通常是crc校验
     * @Param:
     * @return:
     * @Author: admin
     * @Date: 2019/5/7
     */
    boolean checkValid(byte[] bytes);


    /**
     * @Description: 每个插件返回的Map中需要包含字段
     * "RESP_TYPE": LOGIN, HEART, "NORMAL"
     * @Param:
     * @return:
     * @Author: admin
     * @Date: 2019/5/7
     */
    IMessage respMessageProcess(byte[] bytes, Channel channel, boolean addressConvert);

    IRespMessageAnalysis getInstance();

}
