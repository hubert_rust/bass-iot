package com.bx.plugin;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-08 10:43
 **/
public class WorkerType {
    public final static String WORKER_TYPE_LOOP = "loop";
    public final static String WORKER_TYPE_ONCE = "once";
}
