package com.bx.plugin;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface CodeService {

    //对接物理硬件类型:
    String hdType();

    //消息方向: send, resp
    String dir();

}