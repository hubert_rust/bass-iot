package com.bx.commonpool;

import com.bx.plugin.WorkerType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class Worker  extends Thread{
    private static final Logger LOGGER = LoggerFactory.getLogger(Worker.class);
    public Task task;
    public String workType;
    public long ticker = 100;

    /**
    * @Description:
    * @Param:  ticker: 单位毫秒
    * @return:
    * @Author: admin
    * @Date: 2019/5/8
    */
    public Worker(Task task, String workType, long ticker) {
        this.task=task;
        this.workType = workType;
        this.ticker = ticker;
    }

    @Override
    public void run() {
        try {
            if (workType.equals(WorkerType.WORKER_TYPE_LOOP)) {
                while (true) {
                    //请使用TimeUnit的sleep
                    TimeUnit.MILLISECONDS.sleep(this.ticker);
                    //sleep(this.ticker);
                    task.process();
                }
            }
            else if (workType.equals(WorkerType.WORKER_TYPE_ONCE)) {
                task.process();
            }
            else {
                LOGGER.error(">>> Worker, run, workType invalid: {}", workType);
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(">>> worker, run, e: {}", e.getMessage());
        }
    }

}
