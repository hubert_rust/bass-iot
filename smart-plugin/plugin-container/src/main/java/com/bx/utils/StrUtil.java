package com.bx.utils;

import org.apache.commons.lang.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public class StrUtil {

	private static Long number = 100L;

	// 服务器配置生成唯一编号的前缀
	private static String prefix;

	public static String getPrefix() {
		return prefix;
	}

	public static void setPrefix(String prefix) {
		StrUtil.prefix = prefix;
	}

	private static String uniqueNumber = "";

	public static String getStr(Object object) {
		return object == null ? "" : object.toString();
	}

	/**
	 * 生成唯一编码
	 * 
	 * @return
	 */
	public static synchronized String getUniqueNumber() {

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMddHHmmssSSS");
		String currentuniqueNumber = sdf.format(date);
		if ("".equals(uniqueNumber)) {
			uniqueNumber = currentuniqueNumber;
		} else {

			if (currentuniqueNumber.equals(uniqueNumber)) {

				number++;
			} else {
				number = 100L;
				uniqueNumber = currentuniqueNumber;
			}
		}

		if (StringUtils.isEmpty(prefix)) {
			return uniqueNumber + number;

		} else {
			return prefix + (uniqueNumber + number).substring(2);
		}

	}

	public static String toBinaryString(Integer mask, int length) {
		checkNotNull(mask);
		checkArgument(length > 0);

		return StringUtils.leftPad(Integer.toBinaryString(mask), length, "0");
	}

	/**
	 * 数组取差值
	 * 
	 * @param arr1
	 * @param arr2
	 * @return
	 */
	public static String[] substract(String[] arr1, String[] arr2) {
		LinkedList<String> list = new LinkedList<String>();
		for (String str : arr1) {

			str = str.toLowerCase();
			if (!list.contains(str)) {
				list.add(str);
			}
		}
		for (String str : arr2) {
			str = str.toLowerCase();
			if (list.contains(str)) {
				list.remove(str);
			}
		}
		String[] result = {};
		return list.toArray(result);
	}

	/**
	 * 16进制格式输出
	 * 
	 * @param input
	 * @return
	 */
	public static String splitSpace(String input) {

		String regex = "(.{2})";
		input = input.replaceAll(regex, "$1 ");
		return input;
	}

	public static void main(String[] args) {
		byte by = 0x60;
		System.out.println(getBit(by));
		
		byte by2 = 0x6b;
		System.out.println(getBit(by2));
//		
//		byte ou = decodeBinaryString("11100000");
//		System.out.println(Integer.toHexString( ou& 0xFF));
		
		//System.out.println(ou& 0xFF);
	}

	/**
	 * 字节转bit
	 * @param by
	 * @return
	 */
	public static String getBit(byte by) {
		StringBuffer sb = new StringBuffer();
		sb.append((by >> 7) & 0x1).append((by >> 6) & 0x1)
				.append((by >> 5) & 0x1).append((by >> 4) & 0x1)
				.append((by >> 3) & 0x1).append((by >> 2) & 0x1)
				.append((by >> 1) & 0x1).append((by >> 0) & 0x1);
		return sb.toString();
	}
	
	/**
	 * bit转字节
	 * @param byteStr
	 * @return
	 */
	public static byte decodeBinaryString(String byteStr) {  
	    int re, len;  
	    if (null == byteStr) {  
	        return 0;  
	    }  
	    len = byteStr.length();  
	    if (len != 4 && len != 8) {  
	        return 0;  
	    }  
	    if (len == 8) {// 8 bit处理  
	        if (byteStr.charAt(0) == '0') {// 正数  
	            re = Integer.parseInt(byteStr, 2);  
	        } else {// 负数  
	            re = Integer.parseInt(byteStr, 2) - 256;  
	        }  
	    } else {// 4 bit处理  
	        re = Integer.parseInt(byteStr, 2);  
	    }  
	    return (byte) re;  
	}

}
