package com.bx.utils;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

/**
 * @program: service
 * @description: restful req
 * @author: Admin
 * @create: 2019-04-19 19:17
 **/

public class RestfulUtils<T> {

    public static RestTemplate restTemplate = new RestTemplate();

    public static Object get(String url) throws Exception {
        return restTemplate.getForObject(url, String.class);
    }
}