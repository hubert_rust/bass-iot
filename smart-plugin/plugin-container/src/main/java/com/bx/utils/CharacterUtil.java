
package com.bx.utils;

import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 字符工具
 * 
 * @author Administrator
 *
 */
public class CharacterUtil {

	private static final Logger logger = LoggerFactory.getLogger(CharacterUtil.class);


	public static byte charToByte(char c) {
		return (byte) "0123456789ABCDEF".indexOf(c);
	}

	/**
	 * 字符串转16进制字符串
	 * 
	 * @param s
	 * @return
	 */
	public static String toHexString(String s) {
		StringBuffer str = new StringBuffer("");
		for (int i = 0; i < s.length(); i++) {
			int ch = (int) s.charAt(i);
			String s16 = Integer.toHexString(ch);
			str.append(s16);
		}
		return str.toString().toUpperCase();
	}

	/**
	 * 16进制字符串转字节数组
	 * 
	 * @param hex
	 * @return
	 */
	public static byte[] hexString2Bytes(String hex) {
		
		if(hex.length()%2!=0){
			hex=hex+"0";
		}

		if ((hex == null) || (hex.equals(""))) {
			return null;
		} else if (hex.length() % 2 != 0) {
			return null;
		} else {
			hex = hex.toUpperCase();
			int len = hex.length() / 2;
			byte[] b = new byte[len];
			char[] hc = hex.toCharArray();
			for (int i = 0; i < len; i++) {
				int p = 2 * i;
				b[i] = (byte) (charToByte(hc[p]) << 4 | charToByte(hc[p + 1]));
			}
			
			
			return b;
		}

	}
	
	
	/**
	 * 16进制字符串转字节数组 低字节在前
	 * 
	 * @param hex
	 * @return
	 */
	public static byte[] hexString2ByteslowHigh(String hex) {

		if ((hex == null) || (hex.equals(""))) {
			return null;
		} else if (hex.length() % 2 != 0) {
			return null;
		} else {
			hex = hex.toUpperCase();
			int len = hex.length() / 2;
			byte[] b = new byte[len];
			char[] hc = hex.toCharArray();
			for (int i = 0; i < len; i++) {
				int p = 2 * i;
				b[len-(i+1)] = (byte) (charToByte(hc[p]) << 4 | charToByte(hc[p + 1]));
			}
			
			
			return b;
		}

	}
	
	

	/**
	 * 字节数组转16进制字符串
	 * 
	 * @param src
	 * @return
	 */
	public static String bytesToHexString(byte[] src) {
		StringBuilder stringBuilder = new StringBuilder("");
		if (src == null || src.length <= 0) {
			return null;
		}
		for (int i = 0; i < src.length; i++) {
			int v = src[i] & 0xFF;
			String hv = Integer.toHexString(v);
			if (hv.length() < 2) {
				stringBuilder.append(0);
			}
			stringBuilder.append(hv);
		}
		return stringBuilder.toString().toUpperCase();
	}
	
	
	/**
	 * 字节数组转16进制字符串
	 * 
	 * @param src
	 * @return
	 */
	public static String bytesToHexStringlowHigh(byte[] src) {
		StringBuilder stringBuilder = new StringBuilder("");
		if (src == null || src.length <= 0) {
			return null;
		}
		for (int i = 0; i < src.length; i++) {
			int v = src[src.length-(i+1)] & 0xFF;
			String hv = Integer.toHexString(v);
			if (hv.length() < 2) {
				stringBuilder.append(0);
			}
			stringBuilder.append(hv);
		}
		return stringBuilder.toString().toUpperCase();
	}

	/**
	 * 16进制字节转16进制字符串
	 * 
	 * @param src
	 * @return
	 */
	public static String bytesToHexString(byte src) {

		int v = src & 0xFF;
		String hv = Integer.toHexString(v);
		if (hv.length() < 2) {
			hv = "0" + hv;
		}
		return hv.toUpperCase();
	}
	
	

	/**
	 * 中文转换 高定位呼唤
	 * 
	 * @param content
	 * @return
	 */
	public static String unicodeToHexString(String content) {

		StringBuffer str = new StringBuffer();
		char[] chars = content.toCharArray();

		for (int i = 0; i < chars.length; i++) {
			String uncoide = Integer.toHexString(chars[i]);
			int x = Integer.parseInt(uncoide, 16);
			str.append(decToHex(x));

		}

		for (int i = 0; i < 10 - chars.length; i++) {
			str.append("2000");
		}
		return str.toString();
	}

	public static String decToHex(int dec) {
		String hex = "";
		while (dec != 0) {
			String h = Integer.toString(dec & 0xff, 16);
			if ((h.length() & 0x01) == 1)
				h = '0' + h;
			hex = hex + h;
			dec = dec >> 8;
		}
		return hex.toUpperCase();
	}

	/**
	 * 16进制字符串 转字符串
	 * 
	 * @param s
	 * @return
	 */
	public static String hexStringToString(String s) {
		if (s == null || s.equals("")) {
			return null;
		}
		s = s.replace(" ", "");
		byte[] baKeyword = new byte[s.length() / 2];
		for (int i = 0; i < baKeyword.length; i++) {
			try {
				baKeyword[i] = (byte) (0xff & Integer.parseInt(
						s.substring(i * 2, i * 2 + 2), 16));
			} catch (Exception e) {
				logger.error("Exception", e);
			}
		}
		try {
			s = new String(baKeyword, "gbk");
			new String();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return s;
	}
	public static byte[] setUserNo(byte[] user_no) {
		byte[] return_user_no = new byte[10];

		if (user_no.length <= 10) {
			int len = user_no.length;
			for (int i = 0; i < len; i++) {
				return_user_no[i] = user_no[i];
			}
			for (int i = len; i < 10; i++) {
				return_user_no[i] = 0x00;
			}
		}
		return return_user_no;
	}
	public static byte[] getHubBinAddrFromString(String s) {
		Preconditions.checkNotNull(s,
				">>> getHubBinAddrFromString valid s.");
		Preconditions.checkArgument(s.length() == 10,
				">>> getHubBinAddrFromString s length valid");
		Preconditions.checkArgument(s.length()%2 == 0,
				">>> getHubBinAddrFromString s length valid");

		String strAddr = s.toUpperCase();
		byte[] hubAddr = new byte[s.length()/2];
		for (int i = 0; i<s.length()/2; i++) { hubAddr[i] = 0x00; };

		//A1:2字节BCD码
		hubAddr[0] = (byte)((charToByte(strAddr.charAt(8)) << 4) | (charToByte(strAddr.charAt(9))));
		hubAddr[1] = (byte)((charToByte(strAddr.charAt(6)) << 4) | (charToByte(strAddr.charAt(7))));

		//A2:2字节二进制码
		String strHexA2 = String.format("%06x", Integer.parseInt(strAddr.substring(0,6)));
		strHexA2 = strHexA2.toUpperCase();
		hubAddr[2] = (byte)((charToByte(strHexA2.charAt(4)) << 4) | (charToByte(strHexA2.charAt(5))));
		hubAddr[3] = (byte)((charToByte(strHexA2.charAt(2)) << 4) | (charToByte(strHexA2.charAt(3))));
		hubAddr[4] = (byte)((charToByte(strHexA2.charAt(0)) << 4) | (charToByte(strHexA2.charAt(1))));

		return hubAddr;
	}

}
