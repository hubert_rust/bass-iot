package com.bx.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-07 15:20
 **/
public class ByteUtil {
    private static final Logger log = LoggerFactory.getLogger(ByteUtil.class);
    public static byte calc_sum_crc(byte[] msg, int startPos, int endPos) {
        byte sum_crc = 0;
        for (int i = startPos; i <= endPos; i++) {
            sum_crc += (byte) msg[i];
        }

        return sum_crc;
    }


    public static byte[] setUserNo(byte[] user_no) {
        byte[] return_user_no = new byte[10];

        if (user_no.length <= 10) {
            int len = user_no.length;
            for (int i = 0; i < len; i++) {
                return_user_no[i] = user_no[i];
            }
            for (int i = len; i < 10; i++) {
                return_user_no[i] = 0x00;
            }
        }
        return return_user_no;
    }
    public static byte[] byteMerger(byte[] bt1, byte[] bt2) {
        byte[] btout = new byte[bt1.length + bt2.length];
        System.arraycopy(bt1, 0, btout, 0, bt1.length);
        if (bt2 != null) {
            System.arraycopy(bt2, 0, btout, bt1.length, bt2.length);
        }
        return btout;
    }

    public static String twoBytes2Voltage(byte[] bytes, int start, int len) {
        int high = (bytes[start] & 0xff) * 256;
        int low = (bytes[start + 1] & 0xff);
        float fval = (high + low) / 100f;
        return (String.format("%.2f ", fval));
    }
    public static byte[] float2BcdBytes(float f, boolean direction) {
        int scale = 10000, step = 100, sum = (int) f;
        byte[] ba = new byte[4];

        for (int i = 0; i < 3; i++) {
            Integer octval = sum / scale;
            int hexval = Integer.parseInt(octval.toString(), 16);
            ba[i] = (byte) hexval;
            scale /= step;
        }

        Integer decimal = (int) (f * 100 - sum * 100);
        int hexval = Integer.parseInt(decimal.toString(), 16);
        ba[3] = (byte) hexval;

        if (direction == false) {
            byte temp = ba[0];
            ba[0] = ba[3];
            ba[3] = temp;

            temp = ba[1];
            ba[1] = ba[2];
            ba[2] = temp;
        }

        return ba;
    }
    /**
    * @Description: 水电表使用
    * @Param:
    * @return:
    * @Author: admin
    * @Date: 2019/5/9
    */
    public static byte[] send_req_meter_head_tail_fill_proc(byte[] meterAddr,
                                                            byte meterTypeByte,
                                                            byte controlCode,
                                                            byte[] meterAppData) {
        byte[] arb_send_meter_head = {
                /* 转发数据内容开始 */
                (byte) 0xFE, (byte) 0xFE, (byte) 0xFE, (byte) 0xFE, /* 前置帧 */
                (byte) 0x68,/* 表数据头，表数据校验位开始 */(byte) 0x50,/* 0x10-冷水，0x50-热水 */
                (byte) 0x21, (byte) 0x03, (byte) 0x00, (byte) 0x11, (byte) 0x17,
                (byte) 0x05, (byte) 0x07, /* 表地址 */
                (byte) 0x04, /* ctrl code:01-读命令,04-写 */
                (byte) 0x08, /* 数据域字节长度 length */
                // (byte) 0x09, (byte) 0xE1,/* 数据标识 cmd： */(byte) 0x01, // data_seq：帧序号
        };

        int index_meter_data_head_byte = 4;
        int index_meter_type = index_meter_data_head_byte + 1;
        int index_meter_addr_start = index_meter_type + 1;
        int len_meter_addr_byte = 7;
        int index_meter_control_code = index_meter_addr_start
                + len_meter_addr_byte;
        int index_data_domain_len = 14;

        arb_send_meter_head[index_data_domain_len] = (byte) meterAppData.length;
        arb_send_meter_head[index_meter_control_code] = controlCode;
        /* 表地址 */
        System.arraycopy(meterAddr, 0, arb_send_meter_head,
                index_meter_addr_start, len_meter_addr_byte);
        arb_send_meter_head[index_meter_type] = meterTypeByte;

        byte[] arb_meter_data_no_tail = byteMerger(arb_send_meter_head,
                meterAppData);
        // 填写表数据校验
        byte[] arb_meter_data_tail = { (byte) 0xff, 0x16 };
        arb_meter_data_tail[0] = ByteUtil.calc_sum_crc(arb_meter_data_no_tail,
                index_meter_data_head_byte, arb_meter_data_no_tail.length - 1);

        return byteMerger(arb_meter_data_no_tail, arb_meter_data_tail);
    }


    public static byte[] send_trans_head_tail_fill_req_proc(byte[] hubAddr,
                                                            byte afn,
                                                            byte meterType,
                                                            byte frameSeq,
                                                            byte[] transData) {
        byte[] arb_send_trans_head = { 0x68, (byte) 0xA9, 0x00, (byte) 0xA9,
                0x00, // 总帧长度，从控制域4B（包含）开始，到倒数第三个字节，该消息固定
                0x68,/* head 2nd */(byte) 0x4B,/* 控制域:发送-0x4B,接收-0x88 */
                0x49, 0, 1, 0, 2, 0x10,/* afn:02-链路检测，0x10-转发 */0x60,/* frame seq */
                0, 0,/* DA */1, 0,/* DT:1,0-f1~p0,表示转发 */
                0x02/* 转发通道：01-mbus,02-485 */, 0x6B, // 下行通信控制字：0x6B冷热水表用，0xCB电表用；
                (byte) 0xFF,/* 帧响应超时时间 */
                0x32,/* 字节超时时间 */
                0x19, /* transmit data len */
        };
        /* 集中器地址 */
        byte index_hub_addr_start = 7;
        byte fi_hub_addr_byte_len = 5;
        System.arraycopy(hubAddr, 0, arb_send_trans_head, index_hub_addr_start,
                fi_hub_addr_byte_len);

        int iFrameLength = (arb_send_trans_head.length - 6 + transData.length) * 4;


        byte index_frame_len_start = 1;
        byte index_frame_seq = 13;
        arb_send_trans_head[index_frame_len_start] = (byte) (iFrameLength + 1);
        arb_send_trans_head[index_frame_len_start + 1] = (byte) (iFrameLength / 256);
        arb_send_trans_head[index_frame_len_start + 2] = (byte) (iFrameLength + 1);
        arb_send_trans_head[index_frame_len_start + 3] = (byte) (iFrameLength / 256);
        arb_send_trans_head[index_frame_seq] = frameSeq;

        int iPosComPara = index_frame_seq + 6;
        byte fbyte_enum_ammeter = (byte)0xCB;
        if (fbyte_enum_ammeter == meterType) {
            arb_send_trans_head[iPosComPara] = meterType;
        } else {
            arb_send_trans_head[iPosComPara] = (byte) 0x6B;
        }

        byte index_send_com_transmit_len = (17 + 5);
        arb_send_trans_head[index_send_com_transmit_len] = (byte) (transData.length);

        byte[] arb_send_trans_frame = byteMerger(arb_send_trans_head, transData);
        /* 填写转发包校验位及结束符 */
        byte[] arb_send_trans_data_tail = { (byte) 0xff, 0x16 };

        byte index_ctrl_domain = (5 + 1);
        arb_send_trans_data_tail[0] = ByteUtil.calc_sum_crc(arb_send_trans_frame,
                index_ctrl_domain, arb_send_trans_frame.length - 1);

        return byteMerger(arb_send_trans_frame, arb_send_trans_data_tail);
    }

    public static byte[] send_ammeter_head_tail_fill_req_proc(byte ammeterAddr,
                                                              byte controlCode, char startRegAddr, int regCount,
                                                              byte[] meterRegData) {
        byte[] arb_send_app_data_head_req = { (byte) 0xBC, /* 地址 */
                (byte) 0x03, // /*控制码：03-读数据，06-写数据，0x10-设置多个寄存器，0x05-设置单个线圈*/
                (byte) 0x9C, (byte) 0x50, /* 起始寄存器地址，高字节在前，40016寄存器 */
                (byte) 0x00, (byte) 0x1E, /* 需要读取寄存器的个数,高字节在前，30个寄存器 */
                // (byte) 0xF0, (byte) 0xAE, /* 校验 CRC16，对应前面6个字节数据的 */
        };

        int index_ammeter_addr = 0;
        int index_ctrl_code = index_ammeter_addr + 1;
        int index_reg_addr_start = index_ctrl_code + 1;
        int len_reg_addr_byte = 2;
        int index_reg_count_start = index_reg_addr_start + len_reg_addr_byte;
        // int index_data_domain_len = 14;

        /* 表地址 */
        arb_send_app_data_head_req[index_ammeter_addr] = ammeterAddr;
        arb_send_app_data_head_req[index_ctrl_code] = controlCode;

        // 起始寄存器地址
        arb_send_app_data_head_req[index_reg_addr_start] = (byte) (startRegAddr / 256);
        arb_send_app_data_head_req[index_reg_addr_start + 1] = (byte) (startRegAddr & 0xff);

        // 寄存器个数
        arb_send_app_data_head_req[index_reg_count_start] = (byte) (regCount / 256);
        arb_send_app_data_head_req[index_reg_count_start + 1] = (byte) (regCount & 0xff);

        byte[] arb_meter_data_no_tail;
        if (meterRegData != null) {
            arb_meter_data_no_tail = byteMerger(arb_send_app_data_head_req,
                    meterRegData);
        } else {
            arb_meter_data_no_tail = arb_send_app_data_head_req;
        }

        // 填写表数据校验
        byte[] arb_ammeter_data_tail_crc16 = { (byte) 0xF0, (byte) 0xAE };

        byte[] arb_send_ammeter_data_frame_req = byteMerger(
                arb_meter_data_no_tail, arb_ammeter_data_tail_crc16);

        /* 填写表应用数据校验CRC16 */
        byte[] crc16Data = HepCRC16.CRC16_Set(arb_send_ammeter_data_frame_req,
                arb_send_ammeter_data_frame_req.length - 2);
        /*
         * byte[] crc16Data = HepCRC16.CRC16_Set(Arrays.copyOfRange(
         * arb_send_data_req, index_send_ammeter_addr, index_send_ammeter_addr +
         * 23), 21);
         */
        /*
         * System.arraycopy(crc16Data, 0, arb_send_data_req,
         * index_send_ammeter_addr, 23);
         */
        return crc16Data;
    }

    public static byte[] send_ammeter_head_tail_fill_req_proc2(
            byte[] meterRegData) {

        byte[] arb_meter_data_no_tail = meterRegData;
        // 填写表数据校验
        byte[] arb_ammeter_data_tail_crc16 = { (byte) 0xF0, (byte) 0xAE };

        byte[] arb_send_ammeter_data_frame_req = byteMerger(
                arb_meter_data_no_tail, arb_ammeter_data_tail_crc16);
        /* 填写表应用数据校验CRC16 */
        byte[] crc16Data = HepCRC16.CRC16_Set(arb_send_ammeter_data_frame_req,
                arb_send_ammeter_data_frame_req.length - 2);

        return crc16Data;
    }


    public static String bytes2str(byte[] bytes, boolean direction, int precision) {
        checkArgument(precision >= 0);
        int delta = direction ? 1 : -1, start = direction ? 0 : bytes.length - 1;
        StringBuilder sb = new StringBuilder(128);
        for (int i = 0; i < bytes.length; i++) {
            sb.append(String.format("%02X", bytes[start]));
            start += delta;
        }

        if (precision > 0) {
            int intCount = bytes.length * 2 - precision;
            return sb.toString().substring(0, intCount) + "." + sb.toString().substring(intCount);
        } else {
            return sb.toString();
        }
    }

    public static int byte_array_locate(byte[] bArray, int startPos,
                                         byte toFind) {
        for (int i = startPos; i < bArray.length; i++) {
            if (bArray[i] == toFind) {
                return i;
            }
        }

        return -1;
    }
    /**
     * 集中器设置通道01H modbus(电表); 02H 水表
     */
    public static void setHub485Channel(byte[] data, int channel) {
        if (data == null) {
            log.error(">>> call setHub485Channel invalid data.");
            return;
        }
        byte index_trans_communication_port_offset = 18;
        data[index_trans_communication_port_offset] = (byte)channel;
    }

    /**
     * 水电表设置波特率:波特率:0表示300,1表示600,2表示1200,3表示2400,4表示4800,5表示7200,6表示9600,
     * 7表示19200(电表9600,水表2400,具体根据厂家产品确定)
     */
    public static void setMeterBps(byte[] data, int bps) {
        if (data == null) {
            log.error(">>> call setMeterBps invalid data.");
            return;
        }
        byte index_trans_communication_port_offset = 18;
        byte curByte=(byte) (data[index_trans_communication_port_offset + 1] & 0x1F);
        data[index_trans_communication_port_offset + 1]=(byte) ((bps << 5) | curByte);
    }

    //byte转float
    public static float bytes2Float(byte[] ba) {
        return  ByteBuffer.wrap(ba).order(ByteOrder.BIG_ENDIAN).getFloat();

    }

    //float转byte
    public static byte[] float2ByteArray(float value) {
        return ByteBuffer.allocate(4).putFloat(value).array();
    }

    public static String fourBytes2Float(byte[] bytes, int start, int len, float scale, int precision) {
        int b1 = (bytes[start] & 0xff) << 24;
        int b2 = (bytes[start + 1] & 0xff) << 16;
        int b3 = (bytes[start + 2] & 0xff) << 8;
        int b4 = (bytes[start + 3] & 0xff);

        float fval = (b1 + b2 + b3 + b4) / scale;
        return (String.format("%." + precision + "f ", fval));
    }

    private static byte[] getBytes(char[] chars) {
        byte bytes[] = new byte[chars.length];
        for (int i = 0; i < chars.length; i++) {
            bytes[i] = (byte) chars[i];
        }
        return bytes;
    }
    public static boolean bytesCompare(byte[] src, int srcPos, byte[] dest,
                                       int destPos, int length) {

        /* 判断 */
        for (int i = 0; i < length; i++) {
            if (src[srcPos + i] != dest[destPos + i]) {
                return false;
            }
        }

        return true;
    }

}
