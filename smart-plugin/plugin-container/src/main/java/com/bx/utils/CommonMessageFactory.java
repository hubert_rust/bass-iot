package com.bx.utils;

import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/*
 * @ program: plugin-elec
 * @ description: message factory
 * @ author: admin
 * @ create: 2019-04-01 11:39
 **/
public class CommonMessageFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommonMessageFactory.class);

    public static Map getCommonMessage(String conAddr, String messageType) {
        Map<String, Object> map =Maps.newHashMap();
        map.put("con_address", conAddr);
        map.put("message_type", messageType);
        return map;
    }


}
