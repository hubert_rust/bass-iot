package com.bx.stream;

import com.bx.PluginContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;

/**
 * @program: 数据变更通知消息，在此处去请求数据，暂时可以不考虑阻塞消息
 * @description:
 * @author: admin
 * @create: 2019-05-16 15:34
 **/
@EnableBinding(IPluginDataSink.class)
public class PluginDataSink {
    private static final Logger log = LoggerFactory.getLogger(PluginDataSink.class);

    @StreamListener(IPluginDataSink.INPUT_PLUGINDATA)
    public void process(Message<?> message) {
        log.info(">>> {}, PluginDataSink: {}", PluginContainer.pluginId, message.getPayload());

        Acknowledgment acknowledgment = message.getHeaders().get(KafkaHeaders.ACKNOWLEDGMENT, Acknowledgment.class);
        if (acknowledgment != null) {
            System.out.println("Acknowledgement provided");
            acknowledgment.acknowledge();
        }
    }
}
