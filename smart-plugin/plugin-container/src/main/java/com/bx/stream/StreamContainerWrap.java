package com.bx.stream;

import com.bx.utils.SpringContextUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-06-22 14:55
 **/
@Component
public class StreamContainerWrap implements ApplicationRunner {
    public static StreamContainer streamContainer;



    @Override
    public void run(ApplicationArguments args) throws Exception {

        if (streamContainer == null) {
            streamContainer = SpringContextUtils.getBean("streamContainer");
        }
    }
    public static void sendEvent(Object obj) {
        streamContainer.sendEventMessage(obj);
    }
    public static void sendState(Object obj) {
       streamContainer.sendStateMessage(obj);
    }
}
