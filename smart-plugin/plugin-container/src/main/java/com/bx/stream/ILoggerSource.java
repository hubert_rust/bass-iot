package com.bx.stream;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.SubscribableChannel;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-16 15:30
 **/

public interface ILoggerSource {

    String OUTPUT_LOGGER = "logger";

    @Output(OUTPUT_LOGGER)
    SubscribableChannel output();

/*    String INPUT0 = "input0";
    @Input(INPUT0)
    SubscribableChannel input0();*/
}
