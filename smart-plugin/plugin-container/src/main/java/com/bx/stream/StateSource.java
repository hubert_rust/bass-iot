package com.bx.stream;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

/**
 * @program: smart-service
 * @description: 硬件事件上报
 * @author: admin
 * @create: 2019-05-16 15:14
 **/
@Component("stateSource")
@EnableBinding(IStateSource.class)
public class StateSource {
    private static final Logger log = LoggerFactory.getLogger(StateSource.class);
    @Autowired
    private IStateSource source;

    public void sendMessage(Object obj) {
        String sendStr = JSONObject.toJSONString(obj);
        source.output().send(MessageBuilder.withPayload(sendStr).build());
    }
}
