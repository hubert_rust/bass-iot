package com.bx.stream;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-16 15:30
 **/

public interface IPluginDataSink {

    String INPUT_PLUGINDATA = "plugindata";//MessageTopic.MESSAGE_TOPIC_MAINTANCE;
    @Input(INPUT_PLUGINDATA)
    SubscribableChannel input();

/*    String INPUT0 = "input0";
    @Input(INPUT0)
    SubscribableChannel input0();*/
}
