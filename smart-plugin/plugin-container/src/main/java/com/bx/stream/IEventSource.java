package com.bx.stream;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.SubscribableChannel;

/**
 * @program: smart-service
 * @description: 硬件事件上报
 * @author: admin
 * @create: 2019-05-16 15:30
 **/

public interface IEventSource {

    String OUTPUT_EVENT = "eventreport";

    @Output(OUTPUT_EVENT)
    SubscribableChannel output();

/*    String INPUT0 = "input0";
    @Input(INPUT0)
    SubscribableChannel input0();*/
}
