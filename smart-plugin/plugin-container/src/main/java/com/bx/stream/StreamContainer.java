package com.bx.stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-06-22 13:37
 **/
@Component(value = "streamContainer")
public class StreamContainer {
    public static StreamContainer instance = new StreamContainer();

    @Autowired
    StateSource stateSource;

    @Autowired
    EventSource eventSource;

    public static StreamContainer getInstance() {
        return instance;
    }

    public void sendStateMessage(Object obj) {
        stateSource.sendMessage(obj);
    }

    public void sendEventMessage(Object obj) {
        eventSource.sendMessage(obj);
    }
}
