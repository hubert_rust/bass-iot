package com.bx;

import com.alibaba.fastjson.JSONObject;
import com.bx.enginecore.ConcentratorHolder;
import com.bx.entity.*;
import com.bx.message.msgtpl.MsgByteTpl;
import com.bx.plugin.ISendMessageEncode;
import com.bx.server.CommonServer;
import com.bx.stream.StreamContainerWrap;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Table;
import com.smart.command.CommandVal;
import com.smart.common.ResponseState;
import com.smart.event.HWStateEvent;
import com.smart.monitor.PluginMeterMonitor;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;


/*
@EnableSwagger2
@SpringBootApplication
*/

public class PluginContainer {

    public static Map<String, ISendMessageEncode> sendMessageEncodeMap = null;

    public static ConfigEntity configEntity = new ConfigEntity();
    public static CommonServer commonServer;
    public static Table<String, String, Object> table;
    public static Map<String, Object> hubMap;

    //消息模板
    public static Map<String, MsgByteTpl.ByteWrapper> msgTpl;

    public static AtomicBoolean applicationState;
    public static ConcentratorHolder concentratorHolder;

    public static AtomicBoolean meterProbe;

    public static String keepUrl;
    public static String pluginId;
    public static String dcName;


    static {
        sendMessageEncodeMap = Maps.newHashMap();
        applicationState = new AtomicBoolean(true);
        concentratorHolder = new ConcentratorHolder();

        meterProbe = new AtomicBoolean(false);
        //和物理集中器的心跳
        //heartbeatThread = new HeartbeatThread();
        //heartbeatThread.start();
        //HeartbeatThread.concentratorHolder = concentratorHolder;

        table=HashBasedTable.create();
        hubMap= Maps.newHashMap();
        msgTpl = Maps.newHashMap();

        //此处代码需要检查
        configEntity = new ConfigEntity();
        //configEntity.setWeb(new WebServerConfig());
        configEntity.setPlugin(new PluginConfig());
        //configEntity.setPeer(new PeerConfig());
        configEntity.setNetty(new NettyConfig());
        configEntity.setReactorConfig(new ReactorConfig());
        configEntity.setProbeConfig(new ProbeConfig());

    }
    public static void sendMeterState(String hubAddress, String meterAddress, String state) {
        ResponseState responseState = new ResponseState();
        HWStateEvent stateEvent = new HWStateEvent();
        List<HWStateEvent> list = Lists.newArrayList();
        list.add(stateEvent);
        responseState.setDatas(list);
        stateEvent.setEventType(CommandVal.EVENT_TYPE_STATE);
        stateEvent.setPluginId(PluginContainer.pluginId);
        stateEvent.setMainClass(CommandVal.MAIN_CLASS_METER);
        stateEvent.setHubAddress(hubAddress);
        stateEvent.setSubAddress(meterAddress);
        stateEvent.setState(state);
        StreamContainerWrap.sendState(JSONObject.toJSON(responseState));
    }
    public static long getMeterMonitorTime(String hubAddr, String meter) {
        PluginMeterMonitor meterMonitor = (PluginMeterMonitor) getTable().get(hubAddr, meter);
        if (Objects.nonNull(meterMonitor)) {
            return meterMonitor.getLastMonitorTime();
        }
        return System.currentTimeMillis();
    }
    /**
    * @Description: 只在同一个地方修改
    * @Param:
    * @return:
    * @Author: admin
    * @Date: 2019/7/16
    */
    public static void updateMeterMonitorTime(String hubAddr, String meter) {
        PluginMeterMonitor meterMonitor = (PluginMeterMonitor) getTable().get(hubAddr, meter);
        if (Objects.nonNull(meterMonitor)) {
            long currentTime = System.currentTimeMillis();
            long probe = PluginContainer.configEntity.getProbeConfig().getMeter();
            meterMonitor.setLastMonitorTime(currentTime);
            if (currentTime - meterMonitor.getLastMonitorTime() > (probe*1000)) {
                meterMonitor.setState(false);
            }
        }
    }
    public static Table<String, String, Object> getTable() {
        return table;
    }
    public static Map<String, Object> getHubMap() {
        return hubMap;
    }

}
