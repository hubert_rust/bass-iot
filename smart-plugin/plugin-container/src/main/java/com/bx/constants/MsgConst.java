package com.bx.constants;

import com.bx.PluginContainer;
import com.bx.message.inside.IMessage;
import com.bx.message.inside.MessageFactory;
import com.bx.message.inside.MsgCommon;
import com.bx.message.msgtpl.MsgByteTpl;
import com.bx.utils.CharacterUtil;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Map;

import static com.bx.utils.ByteUtil.calc_sum_crc;

/**
 * @ Description: 消息体定义, Map格式
 * @ msgType:"",
 * @ msgCause:"",
 * @ msgAddr:"",消息关联地址, 就是集中器地址;
 * @ msgSubAddr:"", 消息附加地址, 可以是集中器地址;
 * @ msgSeq:"", 仪表会需要此字段
 * @ reqData:"",请求消息数据Map格式
 * @ respData:"",响应消息数据Map格式
 * @ Author: Admin
 **/
/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-07 16:28
 **/
public class MsgConst {

    private static final Logger log = LoggerFactory.getLogger(MsgConst.class);
    //编解码
    public static final String SUCCESS = "SUCCESS";
    public static final String FAILURE = "FAIL";

    //消息实体类型
    public static final String MSG_BODY_TYPE_COMMON = "msg_body_common";

    public static final String MSG_TYPE_LOGIN = "login";
    public static final String MSG_TYPE_LOGIN_RESP = "login_resp";
    public static final String MSG_TYPE_BEAT = "beat";
    public static final String MSG_TYPE_BEAT_RESP = "beat_resp";
    public static final String MSG_TYPE_IM_RESP = "im_resp";   //立即回响应
    public static final String MSG_TYPE_UNKNOW = "unkonw";


    public static final String MSG_TYPE_RESP = "resp";
    public static final String MSG_TYPE_REQ = "req";
    public static final String MSG_TYPE_TIMEOUT = "timeout";
    public static final String MSG_TYPE_RELEASE = "release";
    //登录消息,传递channel
    public static final String MSG_TYPE_CON_STATE = "CON_STATE";

    /**
     * @ Description: 消息原因
     * @ Author: Admin
     **/
    public static final String MSG_CAUSE_NORMAL = "NORMAL";
    public static final String MSG_CAUSE_TIMEOUT = "TIMEOUT";
    public static final String MSG_CAUSE_EXCEPTION = "EXCEPTION";
    public static final String MSG_CAUSE_LOGIN = "LOGIN";
    public static final String MSG_CAUSE_RELEASE_TIMEOUT = "RELEASE_TIMEOUT";
    //心跳超时
    public static final String MSG_CAUSE_HEART_TIMEOUT = "HEART_TIMEOUT";



    //1: 正常请求, 加入请求队列和resp Map中
    //2: 心跳处理，物理集中器响应
    //3: 清理缓存
    //正常响应

    //初始状态
    public static final String MSG_STATE_READY = "state_ready";

    //controller超时的消失
    public static final String MSG_STATE_TIMEOUT = "state_timeout";

    //正常release的消息
    public static final String MSG_STATE_RESP = "state_resp";



    public static MessageFactory messageFactory = null;
    static {

        //消息类型定义
        messageFactory = MessageFactory.factory(builder -> {
            builder.add(MsgConst.MSG_BODY_TYPE_COMMON, MsgCommon::new);
        });
    }

    public static IMessage getMessage(String msgType) {
        return messageFactory.create(msgType);
    }

    public static void main(String[] args) {
        IMessage var1 = getMessage(MsgConst.MSG_TYPE_LOGIN_RESP);
        MsgCommon v1 = (MsgCommon) var1;
        v1.setMsgType("11");

        IMessage var2 = getMessage(MsgConst.MSG_TYPE_LOGIN_RESP);
        MsgCommon v2 = (MsgCommon)var2;
        System.out.println();
    }

    public static  MsgByteTpl.ByteWrapper getLoginResp(byte[] bytes) {
        //byte[] arb_send_data_login_resp = { 0x68, 0x31, 0, 0x31, 0, 0x68, 0x0B,
        //        0x49, 0, 1, 0, 0, 0, 0x6B, 0, 0, 1, 0, (byte) 0xC1, 0x16 };
        byte[] wrapper = MsgByteTpl.getByteTplByMsg(MsgConst.MSG_TYPE_LOGIN_RESP, true);
        System.arraycopy(bytes, 7, wrapper, 7, 5);
        wrapper[13] = bytes[13];
        wrapper[wrapper.length - 2] = calc_sum_crc(wrapper, 6,wrapper.length - 3);
        return new MsgByteTpl.ByteWrapper(wrapper);
    }
    public static  IMessage getLogin(byte[] resp) {
        //登录需要校验集中器是否已经配置
        byte[] address = Arrays.copyOfRange(resp, 7,(7 + 5));
        String hexAddress = CharacterUtil.bytesToHexStringlowHigh(address);

        //log.info(">>> ColdMeterDecode, getLogin, hexAddress: {}", hexAddress);
        if (!PluginContainer.getHubMap().containsKey(hexAddress)) {
            //log.error(">>> ColdMeterDecode, getLogin, not config address: {}", hexAddress);
            return null;
        }
        else {
            MsgByteTpl.ByteWrapper wrapper = getLoginResp(resp);
            MsgCommon iMessage = MsgCommon.MsgCommonBuilder.aMsgCommon()
                    .withMsgType(MsgConst.MSG_TYPE_LOGIN_RESP)
                    .withMsgAddr(hexAddress)
                    .withState("ok")
                    .withByteWrapper(wrapper)
                    .build();
            return iMessage;
        }
    }

    public static  MsgByteTpl.ByteWrapper getBeatResp(byte[] bytes) {
        byte[] wrapper = MsgByteTpl.getByteTplByMsg(MsgConst.MSG_TYPE_BEAT_RESP, true);

        System.arraycopy(bytes, 7, wrapper, 7, (7+5));
        wrapper[13] = bytes[13];
        wrapper[wrapper.length - 2] = calc_sum_crc(wrapper, 6,wrapper.length - 3);
        return new MsgByteTpl.ByteWrapper(wrapper);
    }
    public static IMessage getBeat(byte[] resp) {
        Map<String, Object> map = Maps.newHashMap();
        IMessage ret = MsgConst.getMessage(MsgConst.MSG_BODY_TYPE_COMMON);
        byte[] address = Arrays.copyOfRange(resp, 7, (7 + 5));
        String hexAddress = CharacterUtil.bytesToHexStringlowHigh(address);

        //log.info(">>> ColdMeterDecode, getBeat, hexAddress: {}", hexAddress);
        if (!PluginContainer.getHubMap().containsKey(hexAddress)) {
            log.error(">>> ColdMeterDecode, getLogin, not config address: {}", hexAddress);
            return null;
        }
        else {
            MsgByteTpl.ByteWrapper wrapper = getBeatResp(resp);
            MsgCommon iMessage = MsgCommon.MsgCommonBuilder.aMsgCommon()
                    .withMsgType(MsgConst.MSG_TYPE_BEAT_RESP)
                    .withMsgAddr(hexAddress)
                    .withState("ok")
                    .withByteWrapper(wrapper)
                    .build();
            return iMessage;
        }
    }

}
