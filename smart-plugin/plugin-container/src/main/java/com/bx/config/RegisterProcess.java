package com.bx.config;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bx.PluginContainer;
import com.bx.enginecore.ConcentratorContext;
import com.bx.enginecore.ConcentratorHolder;
import com.bx.enginecore.MessageReactorPool;
import com.bx.entity.BassHardwardRequest;
import com.bx.regcode.CodeHandleRegister;
import com.bx.service.IPluginFuncProcess;
import com.bx.service.PluginFuncProcess;
import com.bx.utils.HttpUtils;
import com.bx.utils.SpringContextUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.smart.common.ResponseModel;
import com.smart.common.ResponseUtils;
import com.smart.common.ResultConst;
import com.smart.monitor.PluginMeterMonitor;
import com.smart.utils.MapEntityUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;

import java.io.*;
import java.util.List;
import java.util.Map;

/*
 * @ program: plugin-elec
 * @ description: 注册模块, 下载集中器数据和表数据, 集中器和表分开
 * @ author: admin
 * @ create: 2019-03-27 08:00
 **/
public class RegisterProcess {
    private static final Logger LOGGER=LoggerFactory.getLogger(RegisterProcess.class);

    public static String getSendRegisterInfo(String pluginId) throws Exception {

        String json="{\n" +
                "\t\"plugin_type\": \"$pluginType\",\n" +
                "\t\"ip_address\": \"$ipAddress\",\n" +
                "\t\"ip_port\": \"$ipPort\",\n" +
                "\t\"uuid\": \"$uuid\",\n" +
                "\t\"server_name\": \"$serverName\",\n" +
                "\t\"plugin_id\": \"$pluginId\"\n" +
                "}";


        List<Map<String, Object>> list = Lists.newArrayList();
        list.add(Maps.newHashMap());
        list.get(0).put("pluginId", pluginId);

        BassHardwardRequest<Map<String, Object>> hardwardRequest = new BassHardwardRequest();
        hardwardRequest.setServiceType("KEEP");
        hardwardRequest.setSubType("REGISTER");
        hardwardRequest.setDcName(PluginContainer.dcName);
        hardwardRequest.setEntitys(list);

/*        ObjectMapper objectMapper=new ObjectMapper();
        Map<String, Object> map=objectMapper.convertValue(PluginContainer.configEntity.getWeb(), Map.class);
        json=json.replace("$ipAddress", map.get("ipAddress").toString());
        json=json.replace("$pluginType", map.get("pluginType").toString());
        json=json.replace("$ipPort", map.get("ipPort").toString());
        json=json.replace("$uuid", map.get("uuid").toString());
        json=json.replace("$serverName", map.get("serverName").toString());
        json=json.replace("$pluginId", PluginContainer.configEntity.getPlugin().getPluginId());*/

        return JSONObject.toJSONString(hardwardRequest);
    }

    //这里是注册的时候download数据，业务平台还应该提供手动刷新配置功能
    public static void initConcentratorTable(Map<String, Object> objectMap) {
        //Map<String, Object> saveMap=Maps.newHashMap();
        //saveMap.putAll(objectMap);
        //检查是否存在
        try {
            PluginMeterMonitor meterMonitor = new PluginMeterMonitor();
            meterMonitor.setConcentratorAddress(objectMap.get("hubAddress").toString());
            meterMonitor.setMeterAddress(objectMap.get("meterAddress").toString());
            meterMonitor.setMeterType(objectMap.get("meterType").toString());


            meterMonitor.setSeqNum(PluginContainer.getTable().size());
            PluginContainer.getTable().put(meterMonitor.getConcentratorAddress(),
                    meterMonitor.getMeterAddress(),
                    (Object)meterMonitor);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void initHub(Map<String, Object> objectMap) {
        Map<String, Object> saveMap=Maps.newHashMap();
        saveMap.putAll(objectMap);
        String hubAddress= objectMap.get("hubAddress").toString();
        PluginContainer.getHubMap().put(hubAddress, saveMap);

        String hubType = objectMap.get("hubType").toString();
        try {
            IPluginFuncProcess funcProcess = SpringContextUtils.getBean(hubType + "FuncProcess");
            CodeHandleRegister.funProcess.put(hubAddress, funcProcess);
        } catch (NoSuchBeanDefinitionException ex) {
            //IPluginFuncProcess funcProcess = SpringContextUtils.getBean(hubType + "FuncProcess");
            //CodeHandleRegister.funProcess.put(hubAddress, funcProcess);
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static String syncHubOrMeterConfig(String request) {
        //add

        //update

        //remove
        return ResponseUtils.getSuccessResponse(ResultConst.SUCCESS);
    }

    public static void saveJson(JSONArray jsonArray) throws Exception{
        String jsonFilePath = System.getProperty("user.dir");
        jsonFilePath += File.separator;

        File file = new File(jsonFilePath + "hub.json");
        file.delete();

        FileOutputStream ou = null;
        try {
            ou = new FileOutputStream(file);
            ou.write(jsonArray.toJSONString().getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }

        if (ou != null) ou.close();
    }
    public static boolean registerRespProcess(String resp) {

        Map<String, Object> map=JSON.parseObject(resp);
        Object obj = map.get("returnCode");
        if (obj == null || !obj.toString().equals("SUCCESS")) {
            return false;
        }

        JSONArray jsonHubArray=(JSONArray) map.get("hubs");
        JSONArray jsonArray=(JSONArray) map.get("datas");

        try {
            saveJson(jsonHubArray);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //先初始化集中器
        PluginContainer.getHubMap().clear();
        jsonHubArray.forEach(val->{
            Map<String, Object> objectMap=(Map<String, Object>) val;
            initHub(objectMap);
        });
        Map<String, Object> con = Maps.newHashMap();
        con.put("hubAddress", "FFFF");
        con.put("hubType", PluginContainer.configEntity.getPlugin().getPluginType());
        initHub(con);

        PluginContainer.getTable().clear();
        jsonArray.forEach(val -> {
            System.out.println(val);
            Map<String, Object> objectMap=(Map<String, Object>) val;
            initConcentratorTable(objectMap);
        });

        //初始化虚拟集中器
        createVConcentrator();
        PluginContainer.concentratorHolder.registerCon("FFFF", ConcentratorContext.class);

        //ConcentratorHolder ch =  PluginContainer.concentratorHolder;
        //ch.getConMap();
        return true;
    }

    public static boolean createVConcentrator() {
        //Set key = Application.getTable().rowKeySet();
        PluginContainer.getHubMap().forEach((k, v)->{
            PluginContainer.concentratorHolder.registerCon(k, ConcentratorContext.class);
        });

        PluginContainer.getTable().cellSet().forEach(v->{
            PluginContainer.concentratorHolder.registerMeter(v.getRowKey(), v.getColumnKey());
        });

        /*key.stream().forEach(v-> {

            Application.concentratorHolder.registerCon(v  (String)v, ConcentratorContext.class);
        });*/
        //启动
        MessageReactorPool.startup();
        return true;
    }

    //注册成功后返回该插件配置的集中器信息和该集中器下配置的表信息
    //注册信息包括: 服务器ip地址和端口号，该模块唯一标识(动态生成:UUID，每次注册都不一样)
    public static boolean register(String pluginId) throws Exception {
        String json=getSendRegisterInfo(pluginId);

        try {
            LOGGER.info(">>> register starting...");
            Object ret=HttpUtils.commonSendHttpPost(PluginContainer.keepUrl, json);

            return registerRespProcess((String) ret);

        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info(">>> register fail, e, " + e.getMessage());
        }

        return false;
    }

}
