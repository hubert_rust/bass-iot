package com.bx.config;

import com.bx.entity.ConfigEntity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;

/*
 * @ program: plugin-elec
 * @ description: 心跳定时器,和业务平台之间, 心跳功能承担配置信息校验，是否更新功能
 * @ author: admin
 * @ create: 2019-03-26 18:42
 **/
public class HeartBeatTimer {
    public static ConfigEntity configEntity;

    public static int timeSpan = 60 *1000;
    public static Timer timer = null;

    public static void setConfigEntity(ConfigEntity configEntity) {
        HeartBeatTimer.configEntity = configEntity;
    }

    public static void startTimer(int timeSpan) {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
                String timeStr = df.format(new Date());
                System.out.println(">>> timer: " + timeStr);

                //
            }
        }, 0, timeSpan);
    }

    //向业务平台发生信息, 校验配置信息是否改变
    public static void sendHttpPostForCheckConfig() {

    }
    public static void cancelTimer() {
        timer.cancel();
    }

    public static void main(String[] args) {
        CountDownLatch countDownLatch = new CountDownLatch(1);

        new Thread() {
            @Override
            public void run() {
                try {
                    System.out.println(">>>1, start countdown");
                    sleep(2000);
                    //countDownLatch.await();

                    System.out.println(">>>1, release");
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }.start();

        new Thread() {
            @Override
            public void run() {
                try {
                    //sleep(3000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                long count = countDownLatch.getCount();
                System.out.println(">>>2, count: " + count);

                countDownLatch.countDown();
                count = countDownLatch.getCount();
                System.out.println(">>>2, after release, count: " + count);
            }
        }.start();
    }
}
