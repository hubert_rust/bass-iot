package com.bx.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Executors;

/*
 * @ program: plugin-elec
 * @ description: shell command
 * @ author: admin
 * @ create: 2019-03-28 10:24
 **/
public class ShellCommand {
    private static final Logger LOGGER = LoggerFactory.getLogger(ShellCommand.class);
    public static final String RESTART_JAR_SHELL_SCRIPT_NAME = "restart.sh";
    public static String linuxShellScriptExec(String fileName) {

        String shpath = System.getProperty("user.dir");
        shpath += File.separator + fileName;

        LOGGER.info("ShellCommand, linuxShellScriptExec, shpath: {}", shpath);
        File file = new File(shpath);
        if (!file.exists()) {
            return "重启脚本不存在";
        }

        Process ps =null;
        try {
            ps=Runtime.getRuntime().exec(shpath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            ps.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String restartJar() {
        return  linuxShellScriptExec(RESTART_JAR_SHELL_SCRIPT_NAME);
    }
}
