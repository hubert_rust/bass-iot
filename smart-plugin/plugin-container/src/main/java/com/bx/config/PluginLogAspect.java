package com.bx.config;

import com.bx.stream.LoggerSourceMessage;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-10 17:58
 **/

@Aspect
@Component
public class PluginLogAspect {
    private static final Logger log = LoggerFactory.getLogger(PluginLogAspect.class);

    //@Autowired
    //LoggerSourceMessage loggerSourceMessage;

    public PluginLogAspect() {
        log.info(">>> PluginLogAspect, starting");
    }

    //配置拦截规则
    @Pointcut("execution(* com..controller..*Controller.*(..))")
    public void aspect() {
        log.info(">>> execute aspect ...");
    }

    @Around("aspect()")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {

        Object obj = null;
        try {
            obj = pjp.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        //loggerSourceMessage.sendMessage(obj);
        //long span = System.currentTimeMillis() - start;



        return obj;

    }


    @AfterThrowing(pointcut = "aspect() ", throwing = "e")
    public void throwEx(JoinPoint joinPoint, Exception e) {
        log.error(">>> method exception");
    }


}
