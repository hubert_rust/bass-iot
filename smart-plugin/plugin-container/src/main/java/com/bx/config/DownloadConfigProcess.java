package com.bx.config;

import com.alibaba.fastjson.JSONObject;
import com.bx.PluginContainer;
import com.bx.entity.*;
import com.bx.utils.HttpUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

/*
 * @ program: plugin-elec
 * @ description: download,主要下载配置文件
 * @ author: admin
 * @ create: 2019-03-27 19:27
 **/
public class DownloadConfigProcess {
    private static final Logger LOGGER = LoggerFactory.getLogger(DownloadConfigProcess.class);

    public static Map<String, Object> web;
    public static Map<String, Object> peer;
    public static Map<String, Object> netty;
    public static Map<String, Object> plugin;
    public static Map<String, Object> reactor;
    public static Map<String, Object> heart;
    public static Map<String ,Object> probe;


    public static boolean download(String pluginId) throws Exception {
        String json="{\n" +
                "\t\"plugin_id\": \"$pluginId\",\n" +
                "}";
        //json = json.replace("$pluginId", PluginContainer.configEntity.getPlugin().getPluginId());
        //Object ret=HttpUtils.commonSendHttpPost(Container.configEntity.getPeer().getUrl() + "download", json);

        List<Map<String, Object>> list = Lists.newArrayList();
        list.add(Maps.newHashMap());
        list.get(0).put("pluginId", pluginId);

        BassHardwardRequest<Map<String, Object>> hardwardRequest = new BassHardwardRequest();
        hardwardRequest.setServiceType("KEEP");
        hardwardRequest.setDcName(PluginContainer.dcName);
        hardwardRequest.setSubType("DOWNLOAD");
        hardwardRequest.setEntitys(list);
        Object ret = HttpUtils.commonSendHttpPost(PluginContainer.keepUrl, JSONObject.toJSONString(hardwardRequest));

        //json to map
        Map<String, Object> map = JSONObject.parseObject((String)ret, Map.class);
        String returnCode = map.get("returnCode").toString();
        if (!returnCode.equals("SUCCESS")) {
            LOGGER.error(">>> DownloadConfigProcess, download, ret:　{}", ret.toString());
            return false;
        }

        Map<String, Object> config = (Map<String, Object>) JSONObject.parseObject(map.get("plugin_config").toString(), Map.class);


        //web = JSONObject.parseObject(config.get("web").toString(), Map.class);
        //peer =  JSONObject.parseObject(config.get("peer").toString(), Map.class);
        //netty =  JSONObject.parseObject(config.get("netty").toString(), Map.class);
        //plugin =  JSONObject.parseObject(config.get("plugin").toString(), Map.class);

        netty = (Map)config.get("netty");
        plugin = (Map)config.get("plugin");
        reactor = (Map)config.get("reactor");
        heart = (Map)config.get("hwheart");
        probe = (Map)config.get("probe");
        LOGGER.info(">>> download, start refresh Application config...");

        //保存
        return saveConfigData();
    }

    public static boolean saveConfigData() {
        ConfigEntity configEntity = new ConfigEntity();
        ObjectMapper objectMapper = new ObjectMapper();
        //WebServerConfig webServerConfig = (WebServerConfig) objectMapper.convertValue(web, WebServerConfig.class);
        //PeerConfig peerConfig = (PeerConfig) objectMapper.convertValue(peer, PeerConfig.class);
        NettyConfig nettyConfig = (NettyConfig) objectMapper.convertValue(netty, NettyConfig.class);
        PluginConfig pluginConfig = (PluginConfig) objectMapper.convertValue(plugin, PluginConfig.class);
        ReactorConfig reactorConfig = (ReactorConfig) objectMapper.convertValue(reactor, ReactorConfig.class);
        HWHeartBeatConfig heartConfig = (HWHeartBeatConfig)objectMapper.convertValue(heart, HWHeartBeatConfig.class);
        ProbeConfig probeConfig = (ProbeConfig) objectMapper.convertValue(probe, ProbeConfig.class);


        configEntity.setNetty(nettyConfig);
        //configEntity.setPeer(peerConfig);
        configEntity.setPlugin(pluginConfig);
        //configEntity.setWeb(webServerConfig);
        configEntity.setReactorConfig(reactorConfig);

        configEntity.setHeartBeatConfig(heartConfig);
        configEntity.setProbeConfig(probeConfig);

        return PluginFileConfig.saveConfigToLocal(configEntity);
    }
}
