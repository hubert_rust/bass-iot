package com.bx.config;

import com.bx.PluginContainer;
import com.bx.enginecore.MessageReactorPool;
import com.bx.server.CommonServer;
import com.bx.statenotify.HeartbeatManage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.bx.config.PluginFileConfig.loadLocalConfigToApplication;

/*
 * @ program: plugin-elec
 * @ description: 控制下载配置文件，注册，心跳
 * @ author: admin
 * @ create: 2019-03-27 19:41
 **/
public class DataRequestManager extends Thread {
    private static final Logger LOGGER=LoggerFactory.getLogger(DataRequestManager.class);
    private static String pluginId;

    public static int RETRY_TIME_SPAN=5 * 1000;

    public String getPluginId() { return pluginId; }
    public  void setPluginId(String pluginId) { this.pluginId = pluginId; }
    public static void startNetty() {
        new Thread() {
            @Override
            public void run() {
                CommonServer.startServer();
            }
        }.start();
    }
    @Override
    public void run() {
        while (true) {
            try { sleep(RETRY_TIME_SPAN); } catch (InterruptedException e) { e.printStackTrace(); }

            try {
                //如果配置文件不存在, download配置文件
                if (!PluginFileConfig.existConfigFile()) {
                    //先下载配置文件, 临时创建文件,重启
                    if (DownloadConfigProcess.download(pluginId)) {
                        LOGGER.info(">>> run, downloadConfigProcess success, ");

                        //下载文件成功后, 自动重启
                        ShellCommand.restartJar();
                    } else {
                        continue;
                    }
                }

                //然后注册
                if (RegisterProcess.register(pluginId)) {
                    //注册成功后, 启动netty
                    startNetty();

                    /*if (PluginContainer.configEntity.getPlugin().isBeatHeart()) {
                        HeartBeatTimer.startTimer(PluginContainer.configEntity.getPlugin().getBeatHeartSpan());
                    }*/

                    HeartbeatManage.startup();

                    return;
                }
                else {
                    loadLocalConfigToApplication();
                    LOGGER.info(">>> run, register failed, retry register...");
                    MessageReactorPool.reset();
                }
            } catch (Exception e) {
                e.printStackTrace();
                LOGGER.error(">>> run, register exception, e: " + e.getMessage());
            }
        }
    }
}
