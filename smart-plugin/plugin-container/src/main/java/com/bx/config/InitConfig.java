package com.bx.config;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bx.PluginContainer;
import com.bx.entity.ConfigEntity;
import com.bx.plugin.WorkerType;
import com.bx.service.PluginFuncProcess;
import com.bx.statenotify.HeartbeatManage;
import com.bx.stream.EventSource;
import com.bx.utils.RestfulUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.snakeyaml.DumperOptions;
import com.fasterxml.jackson.dataformat.yaml.snakeyaml.Yaml;
import com.google.common.collect.Maps;
import com.smart.common.BassRespEntity;
import com.smart.common.ResponseEntity;
import com.smart.common.ResponseModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
public class InitConfig implements ApplicationRunner {
    private static final Logger log = LoggerFactory.getLogger(InitConfig.class);
    public static final String SETTING_FILE = "settings.yml";
    public static final String DEFAULT_CONF = "conf.yml";

    @Autowired
    private DefaultWebConfig defaultWebConfig;


    //@Autowired
    //private CuratorFramework curatorFramework;

    //private TreeCache treeCache;

    @Value("${bass.url}")
    private String bassUrl;

    @Value("${plugin.pluginId}")
    private String pluginId;

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        PluginContainer.pluginId = this.pluginId;

        //获取平台层keep地址,通过bass-hardware已经能唯一确定一条记录了，不需要pluginId
        String url = bassUrl + "/service/list/bass-hardware?pluginId=" + this.pluginId;
        Object ret = RestfulUtils.get(url);
        BassRespEntity map = JSONObject.parseObject((String) ret, BassRespEntity.class);
        if (map.getServiceList().size()<=0) {
            log.info(">>> InitConfig, serviceList is empty");
            throw new Exception("InitConfig, serviceList is empty");
        }
        else {
            map.getServiceList().forEach(v->{
                if (v.get("service_name").toString().equals("keep")) {
                    PluginContainer.keepUrl = v.get("service_url").toString();
                    PluginContainer.dcName  = v.get("dc_name").toString();
                    log.info(">>> keepUrl: {}, dcName: {}", PluginContainer.keepUrl, PluginContainer.dcName);
                }
            });
        }


        /*if (((JSONArray) (map.get("datas"))).size() <= 0) {

        } else {
            Map<String, Object> urlMap = (Map) ((JSONArray) (map.get("datas"))).get(0);
            PluginContainer.keepUrl = urlMap.get("bass_unified_url").toString();
            PluginContainer.dcName  = urlMap.get("dc_name").toString();
            log.info(">>> keepUrl: {}, dcName: {}", PluginContainer.keepUrl, PluginContainer.dcName);
        }
*/
        //如果配置文件存在,
        if (PluginFileConfig.existConfigFile()) {
            PluginFileConfig.loadLocalConfigToApplication();
        }

        startup(defaultWebConfig);

        //不用主动上报，监测功能在管理层fun完成2019-07-02
        //HeartbeatManage.startup(WorkerType.WORKER_TYPE_LOOP);

        //zookeeperInit();
    }


    private void zookeeperInit() {
/*        try {

            treeCache = new TreeCache(curatorFramework, "/datas");
            treeCache.start();
            TreeCacheListener treeCacheListener = new TreeCacheListener() {
                @Override
                public void childEvent(CuratorFramework client, TreeCacheEvent event) throws Exception {
                    switch (event.getType()) {
                        case NODE_ADDED:
                            System.out.println("Node add " + ZKPaths.getNodeFromPath(event.getData().getPath()));
                            break;
                        case NODE_REMOVED:
                            System.out.println("Node removed " + ZKPaths.getNodeFromPath(event.getData().getPath()));
                            break;
                        case NODE_UPDATED:
                            System.out.println("Node updated " + ZKPaths.getNodeFromPath(event.getData().getPath()));
                            break;

                    }
                }
            };
            treeCache.getListenable().addListener(treeCacheListener);
        } catch (Exception e) {
            log.error(">>> PluginContainer, zookeeperInit, e: {}", e.getMessage());
        }*/

    }

    private void startup(DefaultWebConfig defaultPeerConfig) throws Exception {
        DataRequestManager dataRequestManager = new DataRequestManager();
        dataRequestManager.setPluginId(pluginId);

        //这个必须设置, 重要
        //PluginContainer.configEntity.getPeer().setUrl(defaultPeerConfig.getKeepUrl());
        //PluginContainer.configEntity.getPeer().setRequestUrl(defaultPeerConfig.getRequestUrl());
        //PluginContainer.configEntity.getPlugin().setPluginId(defaultPeerConfig.getPluginId());

        //向服务平台注册该插件
        dataRequestManager.start();
    }

    private void saveYmlConfig(ConfigEntity configEntity) throws Exception {
        DumperOptions dumperOptions = new DumperOptions();
        dumperOptions.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        dumperOptions.setDefaultScalarStyle(DumperOptions.ScalarStyle.PLAIN);
        dumperOptions.setPrettyFlow(false);
        FileWriter fw = new FileWriter(SETTING_FILE);
        Yaml yaml = new Yaml(dumperOptions);
        yaml.dump(configEntity, fw);
        fw.close();
    }

    private ConfigEntity loadYmlConfig(String file, boolean isDefault) throws Exception {

        if (isDefault) {
            URL ymlUrl = PluginContainer.class.getClassLoader().getResource(file);
            //URL ymlUrl=Thread.currentThread().getContextClassLoader().getResource(file);
            ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
            return objectMapper.readValue(new FileInputStream(ymlUrl.getFile()), ConfigEntity.class);
        } else {

            String path = System.getProperty("user.dir");
            path += (File.separator + file);
            URL ymlUrl = Thread.currentThread().getContextClassLoader().getResource(path + File.separator + file);
            ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
            return objectMapper.readValue(new FileInputStream(path), ConfigEntity.class);
        }
    }
}
