package com.bx.entity;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-06 08:40
 **/
public class ReactorConfig {
    private int reactorNum;
    private int reactorSleepTm; //ms
    private int hearBeatTm;     //心跳定时器轮询时间ms
    private int hearBeatSpan;  //多长时间检测, ms

    public int getReactorNum() {
        return reactorNum;
    }

    public void setReactorNum(int reactorNum) {
        this.reactorNum = reactorNum;
    }

    public int getReactorSleepTm() {
        return reactorSleepTm;
    }

    public void setReactorSleepTm(int reactorSleepTm) {
        this.reactorSleepTm = reactorSleepTm;
    }

    public int getHearBeatTm() {
        return hearBeatTm;
    }

    public void setHearBeatTm(int hearBeatTm) {
        this.hearBeatTm = hearBeatTm;
    }

    public int getHearBeatSpan() {
        return hearBeatSpan;
    }

    public void setHearBeatSpan(int hearBeatSpan) {
        this.hearBeatSpan = hearBeatSpan;
    }
}
