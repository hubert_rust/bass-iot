package com.bx.entity;

import org.springframework.stereotype.Component;

/*
 * @ program: plugin-elec
 * @ description: config
 * @ author: admin
 * @ create: 2019-03-26 14:14
 **/
public class ConfigEntity {
    //private PeerConfig peer;
    private NettyConfig netty;
    //private WebServerConfig web;
    private PluginConfig plugin;
    private ReactorConfig reactorConfig;
    private ProbeConfig probeConfig;  //监测


    //心跳功能
    private HWHeartBeatConfig heartBeatConfig;

    public NettyConfig getNetty() {
        return netty;
    }

    public void setNetty(NettyConfig netty) {
        this.netty = netty;
    }

    public PluginConfig getPlugin() {
        return plugin;
    }

    public void setPlugin(PluginConfig plugin) {
        this.plugin = plugin;
    }

    public ReactorConfig getReactorConfig() { return reactorConfig; }
    public void setReactorConfig(ReactorConfig reactorConfig) { this.reactorConfig = reactorConfig; }

    public HWHeartBeatConfig getHeartBeatConfig() {
        return heartBeatConfig;
    }

    public void setHeartBeatConfig(HWHeartBeatConfig heartBeatConfig) {
        this.heartBeatConfig = heartBeatConfig;
    }

    public ProbeConfig getProbeConfig() {
        return probeConfig;
    }

    public void setProbeConfig(ProbeConfig probeConfig) {
        this.probeConfig = probeConfig;
    }
}
