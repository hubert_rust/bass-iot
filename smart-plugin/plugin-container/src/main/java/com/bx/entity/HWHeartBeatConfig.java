package com.bx.entity;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-08 18:30
 **/
public class HWHeartBeatConfig {
    public String peerType;
    public String heartType;
    public long peerHeartBeatSpan;
    public long localHeartBeatSpan;
    public long rcvHeartBeat;
    public long sendHeartBeat;

    public String getPeerType() { return peerType; }
    public void setPeerType(String peerType) { this.peerType = peerType; }
    public String getHeartType() { return heartType; }
    public void setHeartType(String heartType) { this.heartType = heartType; }
    public long getPeerHeartBeatSpan() { return peerHeartBeatSpan; }
    public void setPeerHeartBeatSpan(long peerHeartBeatSpan) { this.peerHeartBeatSpan = peerHeartBeatSpan; }
    public long getLocalHeartBeatSpan() { return localHeartBeatSpan; }
    public void setLocalHeartBeatSpan(long localHeartBeatSpan) { this.localHeartBeatSpan = localHeartBeatSpan; }
    public long getRcvHeartBeat() { return rcvHeartBeat; }
    public void setRcvHeartBeat(long rcvHeartBeat) { this.rcvHeartBeat = rcvHeartBeat; }
    public long getSendHeartBeat() { return sendHeartBeat; }
    public void setSendHeartBeat(long sendHeartBeat) { this.sendHeartBeat = sendHeartBeat; }
}
