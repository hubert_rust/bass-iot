package com.bx.entity;


/*
 * @ program: plugin-elec
 * @ description: peer config
 * @ author: admin
 * @ create: 2019-03-27 08:56
 **/
//@Data
public class PeerConfig {
    String url; //注册中心url

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    String requestUrl; //和平台的url
}
