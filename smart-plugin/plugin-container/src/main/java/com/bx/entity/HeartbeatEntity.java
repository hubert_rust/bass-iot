package com.bx.entity;

import com.bx.enginecore.ConcentratorContextInterface;

public class HeartbeatEntity {
    volatile long updateTime;

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public String getConAddr() {
        return conAddr;
    }

    public void setConAddr(String conAddr) {
        this.conAddr = conAddr;
    }

    public HeartbeatEntity(String conAddr) {
        this.conAddr = conAddr;
    }

    String conAddr;
}
