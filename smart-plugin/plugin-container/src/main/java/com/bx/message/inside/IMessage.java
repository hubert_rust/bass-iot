package com.bx.message.inside;

import com.bx.message.msgtpl.MsgByteTpl;
import io.netty.channel.Channel;

import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-08 14:27
 **/
public interface IMessage<T> {

    Channel channel();
    void withChannel(Channel channel);

    List<T> datas();
    List<T> resp();

    byte msgSeq();
    void withMsgSeq(byte msgSeq);
    //
    void withCountDownLatch(CountDownLatch downLatch);
    CountDownLatch countdownLatch();

    void withCommandOrder(String commandOrder);
    String getOrder();

    void withState(String str);
    String state();

    String getPeerAddr();
    void withPeerAddr(String hubAddr);

    MsgByteTpl.ByteWrapper getWraper();

    String getCommand();
    void withCommand(String cmdCode);
    String getType();
    void withType(String msgType);
    void downMsgTm();          //插件层收到请求时间
    void upMsgTm();            //插件层收到响应时间,
    void downEncodeStartTm();  //编码开始时间
    void downEncodeEndTm();    //编码完成时间
    void upDecodeStartTm();    //解码开始时间,
    void upDecodeEndTm();      //解码完成时间
    void downMsgInQueueTm();   //入队列时间,请求消息,下行消息
    void downMsgOutQueueTm();  //出队列时间,请求消息,下行消息
    void upMsgInQueueTm();     //上行消息入队列时间
    void upMsgOutQueueTm();    //上行消息出队列时间

    long getDownMsgOutQueueTm(); //发生消息时间
}
