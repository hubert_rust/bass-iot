package com.bx.server;

import com.bx.regcode.CodeHandleRegister;
import com.bx.utils.CharacterUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class MessageDecoder extends ByteToMessageDecoder {

	private static final Logger log = LoggerFactory.getLogger(MessageDecoder.class);
	

	/**
	 * @ Description: 主要是拆包
	 * @ Author: Admin
	**/
	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf bufferIn,
			List<Object> out) throws Exception {
		//由各插件自己实现
		CodeHandleRegister.respMessageAnalysis.splicingPackage(bufferIn, out);
	}
}
