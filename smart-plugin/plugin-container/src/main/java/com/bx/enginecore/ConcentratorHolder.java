package com.bx.enginecore;

import com.alibaba.fastjson.JSONObject;
import com.bx.PluginContainer;
import com.bx.constants.MsgConst;
import com.bx.message.inside.IMessage;
import com.bx.message.inside.MsgCommon;
import com.bx.message.msgtpl.MsgByteTpl;
import com.bx.plugin.HeartBeatType;
import com.bx.plugin.ISendMessageEncode;
import com.bx.plugin.PeerHWType;
import com.bx.regcode.CodeHandleRegister;
import com.bx.utils.CharacterUtil;
import com.bx.utils.StrUtil;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

import static com.bx.service.BaseCommandProcess.countDownLatch;

/*
 * @ program: plugin-elec
 * @ description: 集中器管理
 * @ author: admin
 * @ create: 2019-03-29 17:55
 **/
public class ConcentratorHolder {
    private static final Logger LOGGER=LoggerFactory.getLogger(ConcentratorHolder.class);
    public static Map<String, ConcentratorContextInterface> conMap=new ConcurrentHashMap<>();

    public static ConcentratorContextInterface getCon(String conAddr) {
        return  conMap.get(conAddr);
    }

    public static void initConData(String hubAddress, ConcentratorContextInterface con) {
        Map obj = (Map)PluginContainer.getHubMap().get(hubAddress);
        String hubType = obj.get("hubType").toString();
        //if (Objects.nonNull(obj) && obj.size()>0 && Objects.nonNull(obj.get("hubParam"))) {
        if (Objects.nonNull(obj) && obj.size()>0) {
            //Map<String, Object> map = (Map<String, Object>) JSONObject.parse(obj.get("hubParam").toString());
            //if (Objects.nonNull(map) && map.size() > 0) {
            if (true) {
                //map = (Map)map.get("hwheart");
                con.setCmdType(hubType);
                con.setAddress(hubAddress);
                String peerType = PluginContainer.configEntity.getHeartBeatConfig().getPeerType();
                con.setPeerType(Objects.nonNull(peerType) ? peerType : PeerHWType.PEER_TYPE_IP);

                String heartType = PluginContainer.configEntity.getHeartBeatConfig().getHeartType();
                con.setHeartType(Objects.nonNull(heartType) ? heartType : HeartBeatType.HEART_BEAT_TYPE_PEER);

                con.setConWait(PluginContainer.configEntity.getPlugin().getCountDownTm());
                //默认值设置,单位毫秒
                long defValLocal = 5000;
                long defValPeer = 5000;
                if (hubType.equals("locker") || hubType.equals("gate")) {
                    defValLocal = 5000;
                    defValPeer = 10000;
                }
                else {
                    defValLocal = 60000;
                    defValPeer = 60000;
                }
                long peerHBSpan = PluginContainer.configEntity.getHeartBeatConfig().getPeerHeartBeatSpan();
                con.setPeerHeartBeatSpan(Objects.nonNull(peerHBSpan) ? peerHBSpan : defValPeer);

                long localHBSpan = PluginContainer.configEntity.getHeartBeatConfig().getLocalHeartBeatSpan();
                con.setLocalHeartBeatSpan(Objects.nonNull(localHBSpan) ? localHBSpan : defValLocal);
            }
        }
    }
    public boolean registerCon(String conAddr, Class<? extends ConcentratorContextInterface> cls) {
        try {
            ConcentratorContextInterface con=cls.newInstance();
            conMap.put(conAddr, con);
            initConData(conAddr, con);

            //注意该状态
            con.setDoorState(true);
            MessageReactorPool.register(con);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return true;
    }

    public boolean registerMeter(String conAddr,
                                 String meterAddr) {
        ConcentratorContextInterface contextInterface = conMap.get(conAddr);
        if (contextInterface == null) {
            LOGGER.error(">>> ConcentratorHolder, registerMeter, " +
                    "conAddr: {} not match meterAddr: {}", conAddr, meterAddr);
            return false;
        }
        contextInterface.setMeterAddr(meterAddr);
        return true;
    }

    //controller调用接口, 发送请求消息, 在message loop中将message放入Map中
    //
    public static CountDownLatch putRequest(String cmdType,
                                            String cmdCode,
                                            String conAddr,
                                            String meterAddr,
                                            String cmdOrder,
                                            Map entity)  throws Exception{


        //IMessage最容器包分配, 插件实现里面只返回码流
        MsgCommon iMessage = MsgCommon.MsgCommonBuilder.aMsgCommon()
                .withMsgType(MsgConst.MSG_TYPE_REQ)
                .withState(MsgConst.MSG_STATE_READY)
                .withMsgAddr(conAddr)
                .withMsgSubAddr(meterAddr)
                .withCommandOrder(cmdOrder)
                .withDownLatch(new CountDownLatch(1))
                .build();
        iMessage.getReqData().putAll(entity);
        iMessage.setCmdType(cmdType);
        iMessage.setMsgCmdCode(cmdCode);
        iMessage.downMsgTm();

        try {


            /*ConcentratorContextInterface contextInterface = conMap.get(conAddr);
            byte seq = contextInterface.getMeterSeq(conAddr, meterAddr);
            iMessage.setMsgSeq(seq);

            ISendMessageEncode messageEncode = CodeHandleRegister.funEncodeHandle.get(cmdType);
            iMessage.downEncodeStartTm();
            byte[] sendBytes = messageEncode.getSendMessage(cmdCode, conAddr, meterAddr, seq, entity);
            iMessage.downEncodeEndTm();

            iMessage.setReqStr(StrUtil.splitSpace(CharacterUtil.bytesToHexString(sendBytes)));
            iMessage.setByteWrapper(new MsgByteTpl.ByteWrapper(sendBytes));*/
            ConcentratorContextInterface contextInterface = conMap.get(conAddr);
            contextInterface.setRequest(cmdOrder, iMessage);
        } catch (Exception e) {
           LOGGER.error(">>> ConcentratorHolder, putRequest, e: {}", e.getMessage());
           throw new Exception("encode fail: " + e.getMessage());
        }

        return iMessage.countdownLatch();
    }

    //controller调用接口, 发生清理消息
    public static void sendReleaseMessage(String conAddr,
                                          String cmdOrder,
                                          String cause) {

        ConcentratorContextInterface contextInterface=conMap.get(conAddr);
        contextInterface.sendReleaseMessage(cmdOrder, cause);
    }

    public static IMessage getRespData(String conAddr,
                                                  String cmdOrder) {
        ConcentratorContextInterface contextInterface=conMap.get(conAddr);
        return contextInterface.getRequestMap(cmdOrder);
    }

    public static String getCmdCodeByHubAddr(String hubAddr) {
        ConcentratorContextInterface contextInterface=conMap.get(hubAddr);
        return contextInterface.getCommandCode();
    }
    public static Map<String, ConcentratorContextInterface> getConMap() {
        return conMap;
    }
}
