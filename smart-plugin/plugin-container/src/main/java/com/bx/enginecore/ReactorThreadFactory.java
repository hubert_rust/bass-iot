package com.bx.enginecore;

import com.smart.command.CommandCode;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-07-09 11:02
 **/
public class ReactorThreadFactory {
    public static BaseReactorThread createReactor(String pluginType, String name) {
        if (pluginType.equals(CommandCode.HUB_TYPE_ELECMETER)) {
            return new DefaultReactorThread(name);
        }
        else if (pluginType.equals(CommandCode.HUB_TYPE_COLDMETER)) {
            return new DefaultReactorThread(name);
        }
        else if (pluginType.equals(CommandCode.HUB_TYPE_LOCKER)) {
            return new LockerReactorThread(name);
        }
        else if (pluginType.equals(CommandCode.HUB_TYPE_GATE)) {
            return new DefaultReactorThread(name);
        }
        else {
            return new DefaultReactorThread(name);
        }
    }
}
