package com.bx.enginecore;

import com.bx.PluginContainer;
import com.bx.config.RegisterProcess;
import com.bx.message.inside.IMessage;
import com.bx.message.inside.MsgCommon;
import com.bx.message.msgtpl.MsgByteTpl;
import com.bx.plugin.ISendMessageEncode;
import com.bx.regcode.CodeHandleRegister;
import com.bx.utils.CharacterUtil;
import com.bx.utils.StrUtil;
import com.google.common.collect.Maps;

import java.util.Map;

/*
 * @ program: 需要有检测机制，虚拟集中器因为是有状态的，
 * @ description: 线程处理
 * @ author: admin
 * @ create: 2019-03-29 09:15
 **/
public class BaseReactorThread extends Thread {
    public boolean exit() {return true;};
    public boolean register(ConcentratorContextInterface con){return true;};

    //先遍历响应消息
    public void loopRespQueue(ConcentratorContextInterface con){};

    public void loopRequestQueue(ConcentratorContextInterface con){};

    public void releaseTimeoutReq(ConcentratorContextInterface con){};
    public void loopReleaseCache(ConcentratorContextInterface con){};

    public void registHubByMessage(IMessage msg, String currentHubAddress) {
        MsgCommon iMessage = (MsgCommon)msg;
        if (currentHubAddress.equals("FFFF")) {
            Map<String, Object> map = Maps.newHashMap();
            map.put("hubAddress", iMessage.getMsgAddr());
            map.put("hubType", iMessage.getHubType());
            RegisterProcess.initHub(map);
            PluginContainer.concentratorHolder.registerCon(iMessage.getMsgAddr(), ConcentratorContext.class);

            try {
                ConcentratorContextInterface con = ConcentratorContext.class.newInstance();
                ConcentratorHolder.conMap.put(iMessage.getMsgAddr(), con);
                ConcentratorHolder.initConData(iMessage.getMsgAddr(), con);
                con.setConState(true);
                con.setRcvHeartBeat(System.currentTimeMillis());
                con.setDoorState(true);
                con.setBindChannel(iMessage.getChannel());
                MessageReactorPool.register(con);
            } catch (Exception e) {

            }

            //注意该状态
/*            con.setDoorState(true);
            MessageReactorPool.register(con);

            ConcentratorContext con = (ConcentratorContext) ConcentratorHolder.getCon(iMessage.getMsgAddr());

            con.setDoorState(true);*/
        }
    }

    protected byte[] getEncodeMessage(MsgCommon iMessage)  {
        ConcentratorContextInterface contextInterface = ConcentratorHolder.getConMap().get(iMessage.getMsgAddr());
        byte seq = 0x60;
        try {
            seq = contextInterface.getMeterSeq(iMessage.getMsgAddr(), iMessage.getMsgSubAddr());
        } catch (Exception e) {
            contextInterface.setMeterAddr(iMessage.getMsgSubAddr());
            e.printStackTrace();
        }
        iMessage.setMsgSeq(seq);

        ISendMessageEncode messageEncode = CodeHandleRegister.funEncodeHandle.get(iMessage.getCmdType());
        iMessage.downEncodeStartTm();
        //byte[] sendBytes = new byte[0];
        byte[] sendBytes = null;
        try {
            sendBytes = messageEncode.getSendMessage(iMessage.getMsgCmdCode(),
                    iMessage.getMsgAddr(),
                    iMessage.getMsgSubAddr(),
                    seq,
                    iMessage.getReqData());
        } catch (Exception e) {
            e.printStackTrace();
        }
        iMessage.downEncodeEndTm();

        try {
            iMessage.setReqStr(StrUtil.splitSpace(CharacterUtil.bytesToHexString(sendBytes)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sendBytes;
        //iMessage.setByteWrapper(new MsgByteTpl.ByteWrapper(sendBytes));
        //contextInterface.setRequest(iMessage.getOrder(), iMessage);
    }

}
