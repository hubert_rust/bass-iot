package com.bx.enginecore;


import com.bx.plugin.HeartBeatType;

import java.util.concurrent.atomic.AtomicLong;

/*
 * @ program: plugin-elec
 * @ description: 集中器基本信息
 * @ author: admin
 * @ create: 2019-03-29 11:38
 **/
public class BaseConcentratorInfo implements PeerLocalMonitor{
    public String conAddr;
    public String hubType;
    public int bps;
    public int commonPort;
    public String cmdType;
    public String commandCode;

    public String peerType;     //peer_485, peer_ip
    public String heartType;    //peer_send, local_send, dual_send
    public long peerHeartBeatSpan;
    public long localHeartBeatSpan;
    //public AtomicLong rcvHeartBeat = new AtomicLong(System.currentTimeMillis());   //动态更新
    public long rcvHeartBeat = 0;
    public long sendHeartBeat;  //动态更新


    @Override
    public boolean isTimeout() {
        long curTime = System.currentTimeMillis();
        //long timeSpan = curTime - this.rcvHeartBeat.get();
        long timeSpan = curTime - this.rcvHeartBeat;

        //两个心跳周期时间
        if (this.heartType.equals(HeartBeatType.HEART_BEAT_TYPE_PEER)) {
            return (timeSpan - this.peerHeartBeatSpan * 2) > 0 ? true : false;
        }

        return true;
    }

    @Override
    public String getCmdType() {
        return this.cmdType;
    }

    @Override
    public void setCmdType(String cmdType) {
        this.cmdType = cmdType;
    }

    @Override
    public String getPeerType() { return this.peerType; }
    @Override
    public void setPeerType(String peerType) { this.peerType = peerType; }

    @Override
    public void setHeartType(String heartType) { this.heartType = heartType; }
    @Override
    public String getHeartType() { return this.heartType; }

    @Override
    public long getPeerHeartBeatSpan() { return this.peerHeartBeatSpan; }
    @Override
    public void setPeerHeartBeatSpan(long beatSpan) { this.peerHeartBeatSpan = beatSpan; }

    @Override
    public long getLocalHeartBeatSpan() { return this.localHeartBeatSpan; }
    @Override
    public void setLocalHeartBeatSpan(long beatSpan) { this.localHeartBeatSpan = beatSpan; }

    @Override
    public long getRcvHeartBeat() { return this.rcvHeartBeat; }
    @Override
    public void setRcvHeartBeat(long rcvTime) { this.rcvHeartBeat = (rcvTime); }

    @Override
    public long getSendHeartBeat() { return this.sendHeartBeat; }
    @Override
    public void setSendHeartBeat(long sendTime) { this.sendHeartBeat = sendTime; }

    public String getCommandCode() { return commandCode; }
    public void setCommandCode(String commandCode) { this.commandCode = commandCode; }

    public String getConAddr() {
        return conAddr;
    }

    public void setConAddr(String conAddr) {
        this.conAddr = conAddr;
    }

    public int getBps() {
        return bps;
    }

    public void setBps(int bps) {
        this.bps = bps;
    }


}
