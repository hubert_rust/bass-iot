package com.bx.enginecore;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-08 18:04
 **/
public interface PeerLocalMonitor {

    boolean isTimeout();

    //就是hubType, pluginType
    String getCmdType();
    void setCmdType(String cmdType);

    //增加对IP的支持(非485): PEER_485, PEER_IP
    String getPeerType();
    void setPeerType(String peerType);

    //增加心跳功能: PEER_SEND, LOCAL_SEND, DUAL_SEND
    void setHeartType(String heartType);
    String getHeartType();

    //对端发送心跳,对端心跳时长
    long getPeerHeartBeatSpan();
    void setPeerHeartBeatSpan(long beatSpan);

    //本端发送心跳,心跳时长
    long getLocalHeartBeatSpan();
    void setLocalHeartBeatSpan(long beatSpan);

    //System.
    long getRcvHeartBeat();
    void setRcvHeartBeat(long rcvTime);

    long getSendHeartBeat();
    void setSendHeartBeat(long sendTime);
}
