package com.bx.enginecore;

import com.bx.message.inside.IMessage;
import io.netty.channel.Channel;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/*
 * @ program: plugin-elec
 * @ description: 集中器接口抽象
 * @ author: admin
 * @ create: 2019-03-29 10:30
 **/
public interface ConcentratorContextInterface extends PeerLocalMonitor{
    void setHubType(String hubType);
    String getHubType();

    //单位是毫秒, 设置集中器超时时间
    void setConWait(long conWait);
    long getConWait();
    //当前任务占用集中器开始时间, 内部需要判断是否超时
    void setCmdStartTm(long cmdStartTm); //
    long getCmdStartTm();

    //commandCode
    void setCommandCode(String commandCode);
    String getCommandCode();

    //channel
    Channel getBindChannel();
    void setBindChannel(Channel channel);


    String getAddress();
    void setAddress(String conAddr);
    void setMeterAddr(String meterAddr);


    boolean getConState();
    void setConState(boolean state);

    boolean getDoorState();
    boolean setDoorState(boolean state);

    //请求缓存, 响应数据也放入该缓存
    boolean setRequestMap(String commandOrder, IMessage map);
    IMessage getRequestMap(String commandOrder);

    public String getCurrentMessageTick() ;
    public void setCurrentMessageTick(String currentMessageTick);

    boolean prepareSendMessage();
    boolean sendMessage(String conAddr, IMessage map) throws Exception;

    //心跳消息, 集中器状态，响应，超时维护
    boolean setResponseMessage(String conAddr, IMessage map);
    IMessage getReponseMessage();

    String getCurrentTicket();

    //这两个是一对
    boolean setRequest(String messageTick, IMessage map);
    IMessage getRequest();

    //主要是是否request来的请求
    Map getRelease();
    void sendReleaseMessage(String commandOrder, String cause);
    void releaseCache(Map<String, Object> map);

    //这个是消息发送端和消息接收端通信的唯一标识.
    byte getMeterSeq(String conAddr, String meterAddr) throws Exception;

}
