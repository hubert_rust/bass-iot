package com.bx.regcode;

import com.bx.plugin.*;
import com.bx.service.IPluginFuncProcess;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.io.*;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/*
 * @ program: plugin-core
 * @ description: code handle register,
 * @ author: admin
 * @ create: 2019-04-02 17:04
 **/
@Component
public class CodeHandleRegister {

    private static final Logger LOGGER = LoggerFactory.getLogger(CodeHandleRegister.class);
    public final static String JAR_NAME = "plugin-hotwater-0.0.1-SNAPSHOT.jar";

    public final static String PACKAGE_PREFIX = "com.bx.hotcode";
    public final static String PACKAGE_PATH_FLAG = "com/bx/hotcode";

    public static IRespMessageAnalysis respMessageAnalysis;


    //编码器
    public final static Map<String, ISendMessageEncode> funEncodeHandle =Maps.newHashMap();
    //解码器
    public final static Map<String, IRespMessageDecode> funDecodeHandle =Maps.newHashMap();
    //业务处理
    public final static Map<String, IPluginFuncProcess> funProcess = Maps.newHashMap();
    //
    private String findJar(String path) {
        File file = new File(path);
        File[] files = file.listFiles();
        for (int i = 0; i < files.length; i++) {
            if (files[i].isFile() && files[i].getName().lastIndexOf("-SNAPSHOT.jar") >0) {
                return files[i].getName();
            }
        }
        return null;
    }

    @PostConstruct
    public void init() throws Exception{


        String jarFilePath = System.getProperty("user.dir");
        jarFilePath += File.separator;
        String jarName = findJar(jarFilePath);
        jarFilePath += jarName;

        //jarFilePath = "D:\\backend\\smart-hardware-iot\\smart-plugin\\plugin\\plugin-hotwater\\build\\libs\\plugin-hotwater-0.0.1-SNAPSHOT.jar";
        LOGGER.info(">>> CodeHandleRegister, init, jarFilePaht: {}", jarFilePath);
        JarFile jarFile = new JarFile(jarFilePath);
        Enumeration<JarEntry> enumeration = jarFile.entries();
        while (enumeration.hasMoreElements()) {
            JarEntry jarEntry = enumeration.nextElement();
            if (jarEntry.isDirectory() || !jarEntry.getName().endsWith(".class")
                    || jarEntry.getName().indexOf(PACKAGE_PATH_FLAG) <0
                    || jarEntry.getName().indexOf("$") >=0 ) {
                continue;
            }
            LOGGER.info(jarEntry.getName());

            String entryName = jarEntry.getName();
            String className = entryName.substring(entryName.indexOf(PACKAGE_PATH_FLAG));
            className = className.substring(0, className.lastIndexOf(".class"));
            className = className.replace(File.separator, ".");
            className = className.replace("/", ".");
            Class<?> cls = null;
            try {
            cls = Class.forName(className);
            if (cls == null) {
                LOGGER.error(">>> CodeHandleRegister, init, cls is null, {}", className);
                continue;
            }
            } catch (Exception e) {

                LOGGER.error(">>> CodeHandleRegister, init, get class exception, {}", className);
                e.printStackTrace();
                continue;
            }

            if (cls.getAnnotation(CodeService.class) == null) {continue;}

            String dir = cls.getAnnotation(CodeService.class).dir();
            String key = cls.getAnnotation(CodeService.class).hdType();
            if (StringUtils.isEmpty(key) || StringUtils.isEmpty(dir)) {
                LOGGER.error(">>> CodeHandleRegister, init, get key exception, {}", className);
                continue;
            }

            Object obj = cls.newInstance();
            if (obj == null) {
                LOGGER.error(">>> CodeHandleRegister, init, get Object exception, {}", className);
                continue;

            }

            if (dir.equals(HDType.MSG_DIR_SEND)) {
                //2019-07-18,增加水电表混合网关
                if (key.equals(HDType.HDTYPE_MIX_METER)) {
                    if (obj instanceof IMixSendMessageEncode) {
                        funEncodeHandle.putAll(((IMixSendMessageEncode) obj).getEncode());
                    }
                }
                else {
                    funEncodeHandle.put(key, (ISendMessageEncode) obj);
                }
            }
            else if (dir.equals(HDType.MSG_DIR_RESP)) {
                funDecodeHandle.put(key, (IRespMessageDecode) obj);
            }
            else if (dir.equals(HDType.MSG_DIR_ANALYSIS)) {
                respMessageAnalysis = (IRespMessageAnalysis) obj;
            }
            else if (dir.equals(HDType.MSG_DIR_SERVICE_LOCK)) {

            }
            else {
                LOGGER.error(">>> CodeHandleRegister, init, get dir exception, dir: {}", dir);
            }
        }
        LOGGER.info(">>> Decode: {}, Encode: {}", funEncodeHandle.size(), funEncodeHandle.size());
        /*URL ymlUrl = Application.class.getClassLoader().getResource(file);
        //URL ymlUrl=Thread.currentThread().getContextClassLoader().getResource(file);
        ObjectMapper objectMapper=new ObjectMapper(new YAMLFactory());
        return objectMapper.readValue(new FileInputStream(ymlUrl.getFile()), ConfigEntity.class);
*/
    }
}
