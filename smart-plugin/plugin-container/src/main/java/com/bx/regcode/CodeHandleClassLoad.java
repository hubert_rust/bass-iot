package com.bx.regcode;

import java.io.*;
import java.net.URL;
import java.net.URLClassLoader;

/*
 * @ program: plugin-core
 * @ description: code class load
 * @ 类加载的几个方法:1、loadClass 2、findLoadedClass 3、findClass
 * @ author: admin
 * @ create: 2019-04-02 16:40
 **/
@Deprecated
public class CodeHandleClassLoad extends ClassLoader {
    private String classPath;

    public CodeHandleClassLoad(String classPath) {
        this.classPath=classPath;
    }


    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {

        try {
            byte[] classByte = getClassByte(name);
            if (classByte == null) {
                System.out.println(">>> CodehandleClassLoad, findClass, getClassByte return null.");
            }
            else {
                return defineClass(name, classByte, 0, classByte.length);
            }
        } catch (Exception e) {
            System.out.println(">>> CodehandleClassLoad, findClass, Exception, name: " + name);
            e.printStackTrace();
        }
        return super.findClass(name);
    }
    private byte[] getClassByte(String className) throws Exception {

        classPath = classPath.replace("\\", "//");
        String path = classPath + File.separator +
                className.replace('.', File.separatorChar) + ".class";

        InputStream in =null;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            in=new FileInputStream(path);
            out = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int len = 0;
            while((len = in.read(buffer)) != -1) {
                out.write(buffer, 0, len);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            if (in != null) in.close();
            if (out!= null) out.close();
        }
        return out.toByteArray();
    }
}
