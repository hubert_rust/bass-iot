package com.bx.config;
import com.bx.utils.CRCEncode;
import com.google.common.base.Strings;
import com.smart.command.*;
import com.bx.PluginContainer;
import com.bx.constants.MsgConst;
import com.bx.message.msgtpl.MsgByteTpl;
import com.bx.utils.ByteUtil;
import com.bx.utils.CharacterUtil;
import com.bx.utils.StrUtil;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.bx.config.MixConst.fbyte_enum_ctrl_code_write;

/**
 * @program: plugin, 消息模板
 * @description:
 * @author: admin
 * @create: 2019-05-08 09:06
 **/
@Component
public class HotMsgByteTplReg implements ApplicationRunner {
    private static final Logger log = LoggerFactory.getLogger(HotMsgByteTplReg.class);

    public static final String HUB_ADDR_TPL = "0002461002";
    public static final String HOT_METER_ADDR_TPL = "07051807000021";

    public static final Map<String, MsgByteTpl.ByteWrapper> tplMap = Maps.newHashMap();

    public final static byte[] rcv_data_login_req = {0x68, 0x31, 0, 0x31, 0,
            0x68, (byte) 0xC9, 0x49, 0, 1, 0, 0, 2, 0x7B, 0, 0, 1, 0,
            (byte) 0x91, 0x16};

    public final static byte[] send_data_login_resp = { 0x68, 0x31, 0, 0x31, 0, 0x68, 0x0B,
            0x49, 0, 1, 0, 0, 0, 0x6B, 0, 0, 1, 0, (byte) 0xC1, 0x16 };

    public final static byte[] rcv_data_heartbeat_req = {0x68, 0x31, 0, 0x31, 0,
            0x68, (byte) 0xC9, 0x49, 0, 1, 0, 0, 2, 0x7C, 0, 0, 4, 0,
            (byte) 0x95, 0x16};
    public final static  byte[] send_data_heartbeat_resp = { 0x68, 0x49, 0x00, 0x49, 0x00,
            0x68, 0x0B, 0x49, 0x00, 0x01, 0x00, 0x00, 0x00, 0x6C, 0x00,
            0x00, 0x04, 0x00, 0x02, 0x00, 0x00, 0x04, 0x00, 0x00,
            (byte) 0xCB, 0x16 };

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info(">>> HotWaterInit, run...");

        //登录
        tplMap.put(MsgConst.MSG_TYPE_LOGIN,
                new MsgByteTpl.ByteWrapper(rcv_data_login_req));
        tplMap.put(MsgConst.MSG_TYPE_LOGIN_RESP,
                new MsgByteTpl.ByteWrapper(send_data_login_resp));
        //心跳
        tplMap.put(MsgConst.MSG_TYPE_BEAT,
                new MsgByteTpl.ByteWrapper(rcv_data_heartbeat_req));
        tplMap.put(MsgConst.MSG_TYPE_BEAT_RESP,
                new MsgByteTpl.ByteWrapper(send_data_heartbeat_resp));

        //热水表用户查询模板
        byte[] ret = queryMeterUser(HUB_ADDR_TPL, HOT_METER_ADDR_TPL, (byte)1, (byte)4, (byte)0);
        tplMap.put(CommandCode.CMD_QUERY_METER_USER,
                new MsgByteTpl.ByteWrapper(ret));
        log.info(">>> MsgByteTplReg, CMD_QUERY_METER_USER: {}",
                StrUtil.splitSpace(CharacterUtil.bytesToHexString(ret)));

        //热水表单价模板
        ret = queryUnitPrice(HUB_ADDR_TPL, HOT_METER_ADDR_TPL, (byte)0x16);
        tplMap.put(CommandCode.CMD_QUERY_UNIT_PRICE,
                new MsgByteTpl.ByteWrapper(ret));
        log.info(">>> MsgByteTplReg, CMD_QUERY_METER_USER: {}",
                StrUtil.splitSpace(CharacterUtil.bytesToHexString(ret)));

        //暂时保存两份
        PluginContainer.msgTpl.putAll(tplMap);
    }

    //更新用户信息
    public static byte[] cmdUpdateUser(String hubAddress,
                                       String meterAddress,
                                       String cardNo,
                                       String userNo,
                                       String cardPasswd,
                                       float balance,
                                       float leftAmount,
                                       byte frameSeq) {

        byte[] arb_send_app_data_req = {(byte) 0x08, (byte) 0xE1,/* 数据标识DI cmd： */
                (byte) 0x01, // 序号ser：数据序号
                0x11, 0x22, 0x33,
                0x44,// 卡号
                0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11,
                0x11, // 用户编号
                (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
                (byte) 0xFF, (byte) 0xFF,// 读卡密码
                0x00, 0x00, 0x00, 0x00,// 余额
                0x00, 0x00, 0x00, 0x00 // 余量
        };

        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddress);
        byte[] meterAddrByte = CharacterUtil.hexString2ByteslowHigh(meterAddress);
        byte[] cardNoByte = CharacterUtil.hexString2Bytes(cardNo);
        byte[] userNoByte = CharacterUtil.hexString2Bytes(userNo);
        userNoByte = ByteUtil.setUserNo(userNoByte);

        byte[] balanceByte = CRCEncode.float2BcdBytes(balance, false);
        byte[] leftAmountByte = CRCEncode.float2BcdBytes(leftAmount/10, false);
        byte[] cardPasswdByte = CharacterUtil.hexString2Bytes(cardPasswd);

        int len_hot_meter_card_no_len = 4;
        int len_hot_meter_user_no_len = 10;
        int len_hot_meter_balance_len = 4;
        int len_hot_meter_left_amount_len = 4;
        int len_hot_card_pwd_len = 6;
        byte fbyte_enum_hot_meter = (byte)0x50;
        byte fb_afn_transmit = (byte)0x10;

        System.arraycopy(cardNoByte, 0, arb_send_app_data_req, 3,
                len_hot_meter_card_no_len);

        System.arraycopy(userNoByte, 0, arb_send_app_data_req, 7,
                len_hot_meter_user_no_len);

        System.arraycopy(cardPasswdByte, 0, arb_send_app_data_req, 17,
                len_hot_card_pwd_len);

        System.arraycopy(balanceByte, 0, arb_send_app_data_req, 23,
                len_hot_meter_balance_len);

        System.arraycopy(leftAmountByte, 0, arb_send_app_data_req, 27,
                len_hot_meter_left_amount_len);

        byte[] arb_send_meter_data_req = ByteUtil.send_req_meter_head_tail_fill_proc(
                meterAddrByte, fbyte_enum_hot_meter, fbyte_enum_ctrl_code_write,
                arb_send_app_data_req);

        byte[] arb_send_trans_frame = ByteUtil.send_trans_head_tail_fill_req_proc(
                hubAddrByte, fb_afn_transmit, fbyte_enum_hot_meter, frameSeq,
                arb_send_meter_data_req);

        return arb_send_trans_frame;

    }
    //添加用户
    public static byte[] cmdAddUser(String hubAddress,
                                    String meterAddress,
                                    String cardNo,
                                    String userNo,
                                    float balance,
                                    float leftAmount,
                                    String cardPasswd,
                                    byte frameSeq) {

        byte[] arb_send_app_data_req = {(byte) 0x07, (byte) 0xE1,/* 数据标识DI cmd： */
                (byte) 0x01, // 序号ser：数据序号
                0x01,// 用户个数
                0x11, 0x22, 0x33,
                0x44,// 卡号
                0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11,
                0x11, // 用户编号
                (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
                (byte) 0xFF, (byte) 0xFF,// 读卡密码
                0x00, 0x00, 0x00, 0x00,// 余额
                0x00, 0x00, 0x00, 0x00 // 余量
        };

        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddress);
        byte[] meterAddrByte = CharacterUtil.hexString2ByteslowHigh(meterAddress);
        byte[] cardNoByte = CharacterUtil.hexString2Bytes(cardNo);
        byte[] userNoByte = CharacterUtil.hexString2Bytes(userNo);
        userNoByte = ByteUtil.setUserNo(userNoByte);

        byte[] balanceByte = CRCEncode.float2BcdBytes(balance, false);
        byte[] leftAmountByte = CRCEncode.float2BcdBytes(leftAmount/10, false);
        byte[] cardPasswdByte = CharacterUtil.hexString2Bytes(cardPasswd);

        int len_hot_meter_card_no_len = 4;
        int len_hot_meter_user_no_len = 10;
        int len_hot_meter_balance_len = 4;
        int len_hot_meter_left_amount_len = 4;
        int len_hot_card_pwd_len = 6;
        byte fbyte_enum_hot_meter = (byte)0x50;
        byte fb_afn_transmit = (byte)0x10;

        System.arraycopy(cardNoByte, 0, arb_send_app_data_req, 4,
                len_hot_meter_card_no_len);

        System.arraycopy(userNoByte, 0, arb_send_app_data_req, 8,
                len_hot_meter_user_no_len);

        System.arraycopy(cardPasswdByte, 0, arb_send_app_data_req, 18,
                len_hot_card_pwd_len);

        System.arraycopy(balanceByte, 0, arb_send_app_data_req, 24,
                len_hot_meter_balance_len);

        System.arraycopy(leftAmountByte, 0, arb_send_app_data_req, 28,
                len_hot_meter_left_amount_len);

        byte[] arb_send_meter_data_req = ByteUtil.send_req_meter_head_tail_fill_proc(
                meterAddrByte, fbyte_enum_hot_meter, fbyte_enum_ctrl_code_write,
                arb_send_app_data_req);

        byte[] arb_send_trans_frame = ByteUtil.send_trans_head_tail_fill_req_proc(
                hubAddrByte, fb_afn_transmit, fbyte_enum_hot_meter, frameSeq,
                arb_send_meter_data_req);

        return arb_send_trans_frame;

    }
    //删除用户
    public static byte[] cmdDelUser(String hubAddr,
                                    String meterAddr,
                                    String userId,
                                    byte frameSeq) {
        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddr);
        byte[] meterAddrByte = CharacterUtil.hexString2ByteslowHigh(meterAddr);
        byte[] userNo = CharacterUtil.hexString2Bytes(userId);
        userNo = ByteUtil.setUserNo(userNo);
        byte[] arb_send_app_data_req = { (byte) 0x09, (byte) 0xE1,/* 数据标识DI cmd： */
                (byte) 0x01, // 序号ser：数据序号
                0x01,// 用户个数
                0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11 // 用户编号
        };

        int len_hot_meter_user_no_len = 10;
        System.arraycopy(userNo, 0, arb_send_app_data_req, 4,
                len_hot_meter_user_no_len);

        byte fbyte_enum_hot_meter = (byte)0x50;
        byte fbyte_enum_ctrl_code_write = (byte)0x04;
        byte[] arb_send_meter_data_req = ByteUtil.send_req_meter_head_tail_fill_proc(
                meterAddrByte, fbyte_enum_hot_meter, fbyte_enum_ctrl_code_write,
                arb_send_app_data_req);

        byte fb_afn_transmit = (byte)0x10;
        byte[] arb_send_trans_frame = ByteUtil.send_trans_head_tail_fill_req_proc(
                hubAddrByte, fb_afn_transmit, fbyte_enum_hot_meter, frameSeq,
                arb_send_meter_data_req);

        return arb_send_trans_frame;
    }


    // 查询热水表状态
    public static byte[] queryMeterState(String hubAddr,
                                         String meterAddr,
                                         byte seq) {
        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddr);
        byte[] meterAddrByte = CharacterUtil.hexString2ByteslowHigh(meterAddr);
        /* 冷水表开阀请求 */
        byte[] arb_data_send = { 0x68, (byte) 0x95, 0x00, (byte) 0x95, 0x00,
                0x68,/* head 2nd */(byte) 0x4B,/* 控制域:发送-0x4B,接收-0x88 */
                0x49, 0, 1, 0, 2, 0x10,/* afn:0x10-转发 */0x60,/* frame seq */
                0, 0,/* DA */1, 0,/* DT:1,0-f1~p0,表示转发 */
                2/* 转发通道：01-mbus,02-485 */, 0x6B, (byte) 0xFF,/* 帧响应超时时间 */0x32,/* 字节超时时间 */
                0x14, /* transmit data len */
                /* 转发数据内容开始 */
                (byte) 0xFE, (byte) 0xFE, (byte) 0xFE, (byte) 0xFE,
                (byte) 0x68, (byte) 0x50,/* 0x50-热水 */
                (byte) 0x21, (byte) 0x03, (byte) 0x00, (byte) 0x11,
                (byte) 0x17, (byte) 0x05, (byte) 0x07, /* 表地址 */
                (byte) 0x01, /* ctrl code:01-读命令,04-写 */
                (byte) 0x03, /* 数据域字节长度 length */
                (byte) 0x06, (byte) 0x31,/* 数据标识 cmd：查询水控状态 */(byte) 0x01, // seq：帧序号
                (byte) 0x4C, (byte) 0x16, /* 表数据校验，从第三个0x68开始 */
                0x60, 0x16 };

        byte index_hub_addr_start =7;
        byte fi_hub_addr_byte_len = 5;
        System.arraycopy(hubAddrByte, 0, arb_data_send, index_hub_addr_start,
                fi_hub_addr_byte_len);
        /* 表地址 */

        byte index_trans_send_valve_addr_start = 29;
        byte fi_meter_addr_len = 7;
        byte index_frame_seq = 13;
        System.arraycopy(meterAddrByte, 0, arb_data_send,
                index_trans_send_valve_addr_start, fi_meter_addr_len);
        byte fb_afn_transmit = 0x10;
        arb_data_send[index_frame_seq] = seq;
        /* 填写表数据校验 */
        byte index_trans_send_head_3rd = 27;
        int index_valve_data_crc = arb_data_send.length - 4;
        arb_data_send[index_valve_data_crc] = ByteUtil.calc_sum_crc(arb_data_send,
                index_trans_send_head_3rd, index_valve_data_crc - 1);

        arb_data_send[arb_data_send.length - 2] = ByteUtil.calc_sum_crc(arb_data_send,
                6, arb_data_send.length - 3);

        return arb_data_send;
    }

    //查询使用记录总数
    public static byte[] queryUseTotalNum(String hubAddr,
                                         String meterAddr,
                                         byte frameSeq) {
        byte[] arb_send_app_data_req = { (byte) 0x0A, (byte) 0x31,/* 数据标识DI cmd： */
                (byte) 0x01 // 序号ser：数据序号
        };

        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddr);
        byte[] meterAddrByte = CharacterUtil.hexString2ByteslowHigh(meterAddr);

        byte fbyte_enum_hot_meter = 0x50;
        byte fbyte_enum_ctrl_code_read = 0x01;
        byte[] arb_send_meter_data_req = ByteUtil.send_req_meter_head_tail_fill_proc(
                meterAddrByte, fbyte_enum_hot_meter, fbyte_enum_ctrl_code_read,
                arb_send_app_data_req);

        byte fb_afn_transmit = 0x10;
        byte[] arb_send_trans_frame = ByteUtil.send_trans_head_tail_fill_req_proc(
                hubAddrByte, fb_afn_transmit, fbyte_enum_hot_meter, frameSeq,
                arb_send_meter_data_req);

        return arb_send_trans_frame;
    }
    //修改存储块
    public static byte[] setStoreBlock(String hubAddress,
                                                                String meterAddress,
                                                                byte storage_block,
                                                                byte frameSeq) {

        byte[] arb_send_app_data_req = {(byte) 0x0B, (byte) 0xE1,/* 数据标识DI cmd： */
                (byte) 0x01, // 序号ser：数据序号
                0x15 // 存储块号

        };
        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddress);
        byte[] meterAddrByte = CharacterUtil.hexString2ByteslowHigh(meterAddress);
        arb_send_app_data_req[3] = storage_block;

        byte fb_afn_transmit = 0x10;
        byte fbyte_enum_hot_meter = 0x50;
        byte[] arb_send_meter_data_req = ByteUtil.send_req_meter_head_tail_fill_proc(
                meterAddrByte, fbyte_enum_hot_meter, fbyte_enum_ctrl_code_write,
                arb_send_app_data_req);

        byte[] arb_send_trans_frame = ByteUtil.send_trans_head_tail_fill_req_proc(
                hubAddrByte, fb_afn_transmit, fbyte_enum_hot_meter, frameSeq,
                arb_send_meter_data_req);

        return arb_send_trans_frame;
    }
    //查询存储块
    public static byte[] queryStoreBlock(String hubAddr,
                                         String meterAddr,
                                         byte frameSeq) {

        byte[] arb_send_app_data_req = { (byte) 0x0D, (byte) 0x31,/* 数据标识DI cmd： */
                (byte) 0x01 // 序号ser：数据序号
        };
        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddr);
        byte[] meterAddrByte = CharacterUtil.hexString2ByteslowHigh(meterAddr);

        byte fbyte_enum_hot_meter = 0x50;
        byte fbyte_enum_ctrl_code_read = 0x01;
        byte[] arb_send_meter_data_req = ByteUtil.send_req_meter_head_tail_fill_proc(
                meterAddrByte, fbyte_enum_hot_meter, fbyte_enum_ctrl_code_read,
                arb_send_app_data_req);

        byte fb_afn_transmit = 0x10;
        byte[] arb_send_trans_frame = ByteUtil.send_trans_head_tail_fill_req_proc(
                hubAddrByte, fb_afn_transmit, fbyte_enum_hot_meter, frameSeq,
                arb_send_meter_data_req);

        return arb_send_trans_frame;
    }
    //用户总数
    public static byte[] queryUserNum(String hubAddr,
                                      String meterAddr,
                                      byte frameSeq) {

        byte[] arb_send_app_data_req = { (byte) 0x07, (byte) 0x31,/* 数据标识DI cmd： */
                (byte) 0x01, // 序号ser：数据序号
        };

        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddr);
        byte[] meterAddrByte = CharacterUtil.hexString2ByteslowHigh(meterAddr);


        byte fbyte_enum_hot_meter = 0x50;
        byte fbyte_enum_ctrl_code_read = 0x01;

        byte[] arb_send_meter_data_req = ByteUtil.send_req_meter_head_tail_fill_proc(
                meterAddrByte, fbyte_enum_hot_meter, fbyte_enum_ctrl_code_read,
                arb_send_app_data_req);

        byte fb_afn_transmit = 0x10;
        byte[] arb_send_trans_frame = ByteUtil.send_trans_head_tail_fill_req_proc(
                hubAddrByte, fb_afn_transmit, fbyte_enum_hot_meter, frameSeq,
                arb_send_meter_data_req);

        return arb_send_trans_frame;
    }
    //查询热水表总用量
    public static byte[] queryMeterAmount(String conAddress,
                                      String meterAddr,
                                      byte frameSeq) {
        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(conAddress);
        byte[] meterAddrByte = CharacterUtil.hexString2ByteslowHigh(meterAddr);

        byte[] arb_send_data_req = {0x68, (byte) 0x95, 0x00, (byte) 0x95, 0,
                0x68, 0x4B, 0x49, 0, 1, 0, 2, 0x10,/* afn:0x10-转发 */0x60,/*
																		 * frame
																		 * seq
																		 */
                0, 0,/* DA */1, 0,/* DT:1,0-f1~p0,表示转发 */2, 0x6B, (byte) 0xFF,
                0x32, 0x14, /* transmit data len */
                (byte) 0xFE, (byte) 0xFE, (byte) 0xFE, (byte) 0xFE, 0x68, 0x10,
                0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, /* 水表地址 */
                1,/* 1-read,4-write */3, /* data len */0x1F, (byte) 0x90, /* cmd */
                1, (byte) 0xA3, 0x16, 0xE, 0x16};
        // /* 集中器地址 */
        int index_hub_addr_start = 7;
        int fi_hub_addr_byte_len = 5;
        System.arraycopy(hubAddrByte, 0, arb_send_data_req, index_hub_addr_start,
                fi_hub_addr_byte_len);
        // /* 表地址 */
        byte index_trans_send_valve_addr_start = (byte) 29;
        int fi_meter_addr_len = 7;
        System.arraycopy(meterAddrByte, 0, arb_send_data_req,
                index_trans_send_valve_addr_start, fi_meter_addr_len);

        byte index_trans_send_valve_type = (byte) 28;
        byte fbyte_enum_hot_meter = 0x50;
        arb_send_data_req[index_trans_send_valve_type] = fbyte_enum_hot_meter;
        /* 冷水表 */
        /*if (strMeterType.equals("冷水表")) {
            arb_send_data_req[index_trans_send_valve_type] = fbyte_enum_cool_meter;
        } else if (strMeterType.equals("热水表")) {
            arb_send_data_req[index_trans_send_valve_type] = fbyte_enum_hot_meter;
        } else {
            arb_send_data_req[index_trans_send_valve_type] = fbyte_enum_ammeter;
        }*/
        byte index_frame_seq = (byte) 13;
        arb_send_data_req[index_frame_seq] = frameSeq;
        /* 填写表数据校验 */
        byte index_trans_send_head_3rd = (byte) 27;
        int index_valve_data_crc = arb_send_data_req.length - 4;
        arb_send_data_req[index_valve_data_crc] = ByteUtil.calc_sum_crc(
                arb_send_data_req, index_trans_send_head_3rd,
                index_valve_data_crc - 1);
        /* 填写转发包校验位 */
        int index_trans_data_crc = arb_send_data_req.length - 2;
        byte index_ctrl_domain = (byte) 6;
        arb_send_data_req[index_trans_data_crc] = ByteUtil.calc_sum_crc(
                arb_send_data_req, index_ctrl_domain, index_trans_data_crc - 1);

        return arb_send_data_req;
    }

    //设置热水表单价
    public static byte[] setUnitPrice(String hubAddress,
                                      String meterAddress,
                                      float price,
                                      byte frameSeq) {

        byte[] arb_send_app_data_req = {(byte) 0x04, (byte) 0xE1,/* 数据标识DI cmd： */
                (byte) 0x01, // 序号ser：数据序号
                0x01, 0x11, 0x22, 0x33 // 单价
        };



        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddress);
        byte[] meterAddrByte = CharacterUtil.hexString2ByteslowHigh(meterAddress);
        byte[] priceByte = CRCEncode.float2BcdBytes(price,false);
        System.arraycopy(priceByte, 0, arb_send_app_data_req, 3, 4);

        byte fbyte_enum_hot_meter = 0x50;
        byte fbyte_enum_ctrl_code_read = 0x01;
        byte fb_afn_transmit = 0x10;
        byte[] arb_send_meter_data_req = ByteUtil.send_req_meter_head_tail_fill_proc(
                meterAddrByte, fbyte_enum_hot_meter, fbyte_enum_ctrl_code_write,
                arb_send_app_data_req);

        byte[] arb_send_trans_frame = ByteUtil.send_trans_head_tail_fill_req_proc(
                hubAddrByte, fb_afn_transmit, fbyte_enum_hot_meter, frameSeq,
                arb_send_meter_data_req);

        return arb_send_trans_frame;

    }


    //热水表单价
    public static byte[] queryUnitPrice(String conAddress,
                                        String meterAddr,
                                        byte frameSeq) {
        byte[] arb_send_app_data_req = { (byte) 0x04, (byte) 0x31,/* 数据标识DI cmd： */
                (byte) 0x01 // 序号ser：数据序号
        };


        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(conAddress);
        byte[] meterAddrByte = CharacterUtil.hexString2ByteslowHigh(meterAddr);

        byte fbyte_enum_hot_meter = 0x50;
        byte fbyte_enum_ctrl_code_read = 0x01;
        byte fb_afn_transmit = 0x10;

        byte[] arb_send_meter_data_req = ByteUtil.send_req_meter_head_tail_fill_proc(
                meterAddrByte, fbyte_enum_hot_meter, fbyte_enum_ctrl_code_read,
                arb_send_app_data_req);

        byte[] arb_send_trans_frame = ByteUtil.send_trans_head_tail_fill_req_proc(
                hubAddrByte, fb_afn_transmit, fbyte_enum_hot_meter, frameSeq,
                arb_send_meter_data_req);

        return arb_send_trans_frame;
    }

    //指定用户查询
    public static byte[] queryMeterSpecUser(String hubAddress,
                                            String meterAddress,
                                            String userNo,
                                            byte frameSeq) {

        byte[] arb_send_app_data_req = {(byte) 0x09, (byte) 0x31,/* 数据标识DI cmd： */
                (byte) 0x01, // 序号ser：数据序号
                0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, // 用户编号
        };

        int len_hot_meter_user_no_len = 10;
        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddress);
        byte[] meterAddrByte = CharacterUtil.hexString2ByteslowHigh(meterAddress);

        byte[] userNoByte = CharacterUtil.hexString2Bytes(userNo);
        userNoByte = CharacterUtil.setUserNo(userNoByte);
        System.arraycopy(userNoByte, 0, arb_send_app_data_req, 3,
                len_hot_meter_user_no_len);

        byte fbyte_enum_ctrl_code_read = 0x01;
        byte fbyte_enum_hot_meter = 0x50;
        byte[] arb_send_meter_data_req = ByteUtil.send_req_meter_head_tail_fill_proc(
                meterAddrByte, fbyte_enum_hot_meter, fbyte_enum_ctrl_code_read,
                arb_send_app_data_req);

        byte fb_afn_transmit = 0x10;
        byte[] arb_send_trans_frame = ByteUtil.send_trans_head_tail_fill_req_proc(
                hubAddrByte, fb_afn_transmit, fbyte_enum_hot_meter, frameSeq,
                arb_send_meter_data_req);

        return arb_send_trans_frame;
    }


    //表用户信息
    public static byte[] queryMeterUser(String conAddress,
                                        String meterAddr,
                                        byte startIndex,
                                        byte endIndex,
                                        byte frameSeq) {
        byte[] arb_send_app_data_req = { (byte) 0x08, (byte) 0x31,/* 数据标识DI cmd： */
                (byte) 0x01, // 序号ser：数据序号
                0x01, 0x05 // 开始与结束
        };
        //68 9D 00 9D 00 68 4B 02 10 E4 00 00 10 6D 00 00 01 00 02 6B FF 32 16 FE FE FE FE 68 50 03 02 00 08 18 05 07 01 05 08 31 01 01 05 2F 16 DF 16
        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(conAddress);
        byte[] meterAddrByte = CharacterUtil.hexString2ByteslowHigh(meterAddr);

        //整包长度的从后往前倒数第6个(倒数从1开始)
        arb_send_app_data_req[3] = (byte)startIndex;
        //整包长度的从后往前倒数第5个(倒数从1开始)
        arb_send_app_data_req[4] = (byte)endIndex;

        byte fbyte_enum_ctrl_code_read = 0x01;
        byte fbyte_enum_hot_meter = 0x50;

        byte[] arb_send_meter_data_req = ByteUtil.send_req_meter_head_tail_fill_proc(
                meterAddrByte, fbyte_enum_hot_meter, fbyte_enum_ctrl_code_read,
                arb_send_app_data_req);

        byte fb_afn_transmit = 0x10;
        byte[] arb_send_trans_frame = ByteUtil.send_trans_head_tail_fill_req_proc(
                hubAddrByte, fb_afn_transmit, fbyte_enum_hot_meter, frameSeq,
                arb_send_meter_data_req);

        return  arb_send_trans_frame;
    }


    //热水表开阀门
    public static byte[] openGate(String hubAddress,
                                  String meterAddress,
                                  String userNo,
                                  byte seq) {

        byte[] arb_send_data_req = {0x68, (byte) 0xCD, 0x00, (byte) 0xCD, 0,
                0x68, 0x4B, 0x49, 0, 1, 0, 2, 0x10, 0x60, 0, 0, 1, 0, 2, 0x6B,
                (byte) 0xFF, 0x32, 0x22,/* transmit data len */
                (byte) 0xFE, (byte) 0xFE, (byte) 0xFE, (byte) 0xFE, 0x68, 0x50,
                0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, /* 水表地址 */
                0x04,/* ctrl type:1-read,4-write */0x11, /* data len */
                0x06, (byte) 0xE1, /* cmd */1,/* frame seq */
                0x55,/* 0x55-开阀，0x99-关阀 */
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, (byte) 0x00,
                (byte) 0x00, /*
							 * 用户号，此处改为16字节，全0是超级用户，只开阀，此时后面的限制参数不起作用
							 */
                (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, /* 当次用水量的限制，单位L,高位在后 */
                (byte) 0x59, 0x16, (byte) 0x82, 0x16};

        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddress);
        byte[] meterAddrByte = CharacterUtil.hexString2ByteslowHigh(meterAddress);

        // final byte fb_byte_open_type_open = (byte) 0x55; // 开阀门
        // final byte fb_byte_open_type_close = (byte) 0x99; // 关阀门
        /* 集中器地址 */
        byte index_hub_addr_start =7;
        byte fi_hub_addr_byte_len = 5;
        System.arraycopy(hubAddrByte, 0, arb_send_data_req, index_hub_addr_start,
                fi_hub_addr_byte_len);
        /* 表地址 */
        byte index_trans_send_valve_addr_start = 29;
        byte fi_meter_addr_len = 7;
        byte index_frame_seq = 13;
        System.arraycopy(meterAddrByte, 0, arb_send_data_req,
                index_trans_send_valve_addr_start, fi_meter_addr_len);

        arb_send_data_req[index_frame_seq] = seq;

        if (!Strings.isNullOrEmpty(userNo)) {
            byte[] userNoByte = CharacterUtil.hexString2Bytes(userNo);
            userNoByte = CharacterUtil.setUserNo(userNoByte);
            System.arraycopy(userNoByte, 0, arb_send_data_req, 42, 10);
        }

        /* 填写表数据校验 */
        int index_valve_data_crc = arb_send_data_req.length - 4;

        byte index_trans_send_head_3rd = 27;
        arb_send_data_req[index_valve_data_crc] = ByteUtil.calc_sum_crc(
                arb_send_data_req, index_trans_send_head_3rd,
                index_valve_data_crc - 1);
        /* 填写转发包校验位 */
        byte index_ctrl_domain = (byte) 6;
        int index_trans_data_crc = arb_send_data_req.length - 2;
        arb_send_data_req[index_trans_data_crc] = ByteUtil.calc_sum_crc(
                arb_send_data_req, index_ctrl_domain, index_trans_data_crc - 1);

        return arb_send_data_req;
    }

    //热水表关闸
    public static byte[] closeGate(String hubAddress,
                                   String meterAddress,
                                   byte seq) {
        /* 热水开阀请求 */
        byte[] arb_send_data_req = {0x68, (byte) 0x99, 0x00, (byte) 0x99, 0,
                0x68, 0x4B, 0x49, 0, 1, 0, 2, 0x10, 0x60, 0, 0, 1, 0, 2, 0x6B,
                (byte) 0xFF, 0x32, 0x15,/* transmit data len */
                (byte) 0xFE, (byte) 0xFE, (byte) 0xFE, (byte) 0xFE, 0x68, 0x50,
                0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, /* 水表地址 */
                0x04,/* ctrl type:1-read,4-write */0x04, /* data len */
                0x05, (byte) 0xE1, /* cmd */1,/* frame seq */
                (byte) 0x99,/* 0x55-开阀，0x99-关阀 */
                (byte) 0x98, 0x16, (byte) 0xF9, 0x16};
        final byte fb_byte_open_type_open = (byte) 0x55; // 开阀门
        final byte fb_byte_open_type_close = (byte) 0x99; // 关阀门
        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddress);
        byte[] meterAddrByte = CharacterUtil.hexString2ByteslowHigh(meterAddress);
        /* 集中器地址 */
        byte index_hub_addr_start =7;
        byte fi_hub_addr_byte_len = 5;
        System.arraycopy(hubAddrByte, 0, arb_send_data_req, index_hub_addr_start,
                fi_hub_addr_byte_len);
        /* 表地址 */
        byte index_trans_send_valve_addr_start = 29;
        byte fi_meter_addr_len = 7;
        byte index_frame_seq = 13;
        System.arraycopy(meterAddrByte, 0, arb_send_data_req,
                index_trans_send_valve_addr_start, fi_meter_addr_len);

        arb_send_data_req[index_frame_seq] = seq;
        /* 填写表数据校验 */
        byte index_trans_send_head_3rd = 27;
        int index_valve_data_crc = arb_send_data_req.length - 4;
        arb_send_data_req[index_valve_data_crc] = ByteUtil.calc_sum_crc(
                arb_send_data_req, index_trans_send_head_3rd,
                index_valve_data_crc - 1);
        /* 填写转发包校验位 */
        byte index_ctrl_domain = (byte) 6;
        int index_trans_data_crc = arb_send_data_req.length - 2;
        arb_send_data_req[index_trans_data_crc] = ByteUtil.calc_sum_crc(
                arb_send_data_req, index_ctrl_domain, index_trans_data_crc - 1);

        return arb_send_data_req;
    }

    public static byte[] hotMeterSyncTime(String hubAddress,
                                          String meterAddress,
                                          byte[] dateTime,
                                          byte seq) {

        //数据长度7+3
        byte[] arb_send_app_data_req = {(byte) 0x15, (byte) 0xA0,/* 数据标识DI cmd： */
                (byte) 0x01, // 序号ser：数据序号
                0x01, 0x11, 0x22, 0x33, 0x33, 0x18, 0x20 // 日期(BCD码): 秒,分,时,日,月,年（2018->0x18, 0x20),
        };

        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddress);
        byte[] meterAddrByte = CharacterUtil.hexString2ByteslowHigh(meterAddress);

        System.arraycopy(dateTime, 0, arb_send_app_data_req, 3, 7);

        byte fbyte_enum_hot_meter = (byte)0x50;
        byte fb_afn_transmit = (byte)0x10;
        byte[] arb_send_meter_data_req = ByteUtil.send_req_meter_head_tail_fill_proc(
                meterAddrByte, fbyte_enum_hot_meter, fbyte_enum_ctrl_code_write,
                arb_send_app_data_req);

        byte[] arb_send_trans_frame = ByteUtil.send_trans_head_tail_fill_req_proc(
                hubAddrByte, fb_afn_transmit, fbyte_enum_hot_meter, seq,
                arb_send_meter_data_req);

        return arb_send_trans_frame;

    }

    //热水使用记录查询
    public static byte[] useRecord(String hubAddress,
                                   String meterAddress,
                                   String startIndex,
                                   byte frameSeq) {

        byte[] arb_send_app_data_req = {(byte) 0x0B, (byte) 0x31,/* 数据标识DI cmd： */
                (byte) 0x01, // 序号ser：数据序号
                01, 00, // 起始编号
                04 // 查询条数
        };

        byte[] hubAddrByte = CharacterUtil.getHubBinAddrFromString(hubAddress);
        byte[] meterAddrByte = CharacterUtil.hexString2ByteslowHigh(meterAddress);
        int start_index = Integer.valueOf(startIndex);
        int l = start_index % 256;
        int h = start_index / 256;
        byte[] start_pos = new byte[2];
        start_pos[0] = (byte) l;
        start_pos[1] = (byte) h;

        System.arraycopy(start_pos, 0, arb_send_app_data_req, 3, 2);

        byte fbyte_enum_hot_meter = (byte)0x50;
        byte fb_afn_transmit = (byte)0x10;
        byte fbyte_enum_ctrl_code_read = 0x01;
        byte[] arb_send_meter_data_req = ByteUtil.send_req_meter_head_tail_fill_proc(
                meterAddrByte, fbyte_enum_hot_meter, fbyte_enum_ctrl_code_read,
                arb_send_app_data_req);

        byte[] arb_send_trans_frame = ByteUtil.send_trans_head_tail_fill_req_proc(
                hubAddrByte, fb_afn_transmit, fbyte_enum_hot_meter, frameSeq,
                arb_send_meter_data_req);

        return arb_send_trans_frame;
    }

}
