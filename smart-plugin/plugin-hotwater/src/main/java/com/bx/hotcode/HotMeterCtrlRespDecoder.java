package com.bx.hotcode;

import com.bx.utils.CharacterUtil;
import com.bx.utils.StrUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-11 16:48
 **/
public class HotMeterCtrlRespDecoder {
    private static final Logger log = LoggerFactory.getLogger(HotMeterCtrlRespDecoder.class);

    public static void ctrlResp(byte[] bytes,
                                int iMeterTypePos,
                                int iDataLenPos,
                                byte[] diCmdData,
                                Map map) {
        //删除用户
        if (bytes[iDataLenPos] == (byte) 0x03
                && bytes[iDataLenPos + 1] == (byte) 0x09
                && bytes[iDataLenPos + 2] == (byte) 0xE1) {
            map.put("cmd", "删除用户");
        }
        //添加用户
        else if (bytes[iDataLenPos] == (byte)0x03
                && bytes[iDataLenPos + 1] == (byte)0x07
                && bytes[iDataLenPos + 2] == (byte)0xE1) {
            map.put("cmd", "添加用户");
        }
        // 修改用户
        else if (bytes[iDataLenPos] == (byte)0x03
                && bytes[iDataLenPos + 1] == (byte)0x08
                && bytes[iDataLenPos + 2] == (byte)0xE1) {
            map.put("cmd", "修改用户");
        }
        // 设置单价
        else if (bytes[iDataLenPos] == (byte)0x03
                && bytes[iDataLenPos + 1] == (byte)0x04
                && bytes[iDataLenPos + 2] == (byte)0xE1) {
            map.put("cmd", "set-price");
        }
        // 广播设置时间
        else if (bytes[iDataLenPos] == (byte)0x08
                && bytes[iDataLenPos + 1] == (byte)0x14
                && bytes[iDataLenPos + 2] == (byte)0xA1) {
            map.put("cmd", "广播设置时间");
        }
        else if (bytes[iDataLenPos] == (byte)0x03
                && bytes[iDataLenPos + 1] == (byte)0x15
                && bytes[iDataLenPos + 2] == (byte)0xA0) {
            map.put("cmd","时间同步");
        }
        // 热水开阀
        else if (bytes[iDataLenPos] == (byte)0x03
                && bytes[iDataLenPos + 1] == (byte)0x06
                && bytes[iDataLenPos + 2] == (byte)0xE1) {
            map.put("cmd", "open");
        }
        // 热水关阀
        else if (bytes[iDataLenPos] == (byte)0x05
                && bytes[iDataLenPos + 1] == (byte)0x05
                && bytes[iDataLenPos + 2] == (byte)0xE1) {
            map.put("cmd", "close");
        }
        else if (bytes[iMeterTypePos] == (byte)0x50
                && bytes[iDataLenPos + 1] == (byte)0x0B
                && bytes[iDataLenPos + 2] == (byte)0xE1) {
            map.put("cmd", "set-block");
        }
        else if (bytes[iMeterTypePos] == (byte)0x50
                && bytes[iDataLenPos + 1] == (byte)0x0D
                && bytes[iDataLenPos + 2] == (byte)0x31) {

            map.put("block", diCmdData[3]);
        }
        else {
            log.error(">>> HotMeterCtrlRespDecoder, ctrlResp: {}",
                    StrUtil.splitSpace(CharacterUtil.bytesToHexString(bytes)));
            map.put("result", "FAIL");
            return;
        }

        map.put("result", "SUCCESS");
    }
}
