package com.bx.hotcode;

import com.bx.utils.ByteUtil;
import com.bx.utils.CRCEncode;
import com.bx.utils.CharacterUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.smart.common.ResultConst;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.transform.Result;
import java.util.*;

import static com.bx.config.MixConst.*;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-11 16:49
 **/
public class HotMeterQueryRespDecoder {
    private static final Logger log = LoggerFactory.getLogger(HotMeterQueryRespDecoder.class);

    public static Map queryResp(byte[] bytes,
                                int iMeterTypePos,
                                int iDataLenPos,
                                byte[] meter_diser_cmd_data,
                                byte[] meter_cmd_data,
                                int iHeadPos,
                                Map map) {
        byte  fbyte_enum_hot_meter = (byte)0x50;
        // 批量热水用户信息查询
        if (bytes[iMeterTypePos] == (byte)0x50
                && bytes[iDataLenPos + 1] == (byte)0x08
                && bytes[iDataLenPos + 2] == (byte)0x31) {
            hotMeterUsersQuery(meter_diser_cmd_data, map);
        }
        else if (bytes[iMeterTypePos] == fbyte_enum_hot_meter
                && bytes[iDataLenPos + 1] == (byte)0x04
                && bytes[iDataLenPos + 2] == (byte)0x31) {
            //热水表单价
            hotMeterUnitPrice(meter_diser_cmd_data, map);
        }
        else if (bytes[iMeterTypePos] == fbyte_enum_hot_meter
                && bytes[iDataLenPos + 1] == (byte)0x07
                && bytes[iDataLenPos + 2] == (byte)0x31) {

            //热水表用户总数
            hotMeterUsreNum(meter_diser_cmd_data, map);
        }
        //存储块
        else if (bytes[iMeterTypePos] == fbyte_enum_hot_meter
                && bytes[iDataLenPos + 1] == (byte)0x0D
                && bytes[iDataLenPos + 2] == (byte)0x31) {
            map.put("block", meter_diser_cmd_data[3]);
            map.put("result", ResultConst.SUCCESS);
        }
        else if (bytes[iMeterTypePos] == fbyte_enum_hot_meter
                && bytes[iDataLenPos + 1] == (byte)0x0A
                && bytes[iDataLenPos + 2] == (byte)0x31) {
            //热水表用户总数查询
            hotMeterUseRecordNum(meter_diser_cmd_data, map);
        }
        else if (bytes[iDataLenPos] == (byte)(0x15 + 6)
                && bytes[iMeterTypePos] == fbyte_enum_hot_meter
                && bytes[iDataLenPos + 1] == (byte)0x06
                && bytes[iDataLenPos + 2] == (byte)0x31) {
            //热水表状态查询
            hotMeterMeterState(bytes, iDataLenPos, map);
        }
        else if (bytes[iDataLenPos + 1] == (byte)0x1F
                && bytes[iDataLenPos + 2] == (byte)0x90) {
            //热水表总用量
            if (bytes[iDataLenPos] != (byte)0x16) {
                log.error(">>> HotMeterRespDecoder, queryResp, iMeterAppDataLen != 0x16");
                return null;
            }

            int index_trans_head_offset_meter_type = 1;
            queryMetarAmountResp(meter_cmd_data,
                    bytes[iHeadPos+ index_trans_head_offset_meter_type], map);

        }
        else if (bytes[iDataLenPos + 1] == fbyte_cmd_hot_valve_spec_user_query_data_cmd1
                && bytes[iDataLenPos + 2] == fbyte_cmd_hot_valve_spec_user_query_data_cmd2) {
            hotMeterSpecUserResp(meter_diser_cmd_data, map);
        }
        else if (bytes[iMeterTypePos] == fbyte_enum_hot_meter
                && bytes[iDataLenPos + 1] == fbyte_cmd_hot_valve_use_record_query_data_cmd1
                && bytes[iDataLenPos + 2] == fbyte_cmd_hot_valve_use_record_query_data_cmd2) {
            useRecordResp(meter_diser_cmd_data, map);
        }

        return null;
    }


    public static void useRecordResp(byte[] cmdData, Map<String, Object> data) {

        // 0B 31 01
        // 01 返回了几条数据
        // 01 00 返回的序号

        // 20 82 EC 0C 卡号
        // 00112233445566778899 用户号
        // 55 用水方式
        // 28 50 19 12 06 14 20 开始时间
        // 47501912061420 结束时间
        // 00000000 本次消费金额
        // 00200000 剩余金额
        // 00000000 本次消费水量
        // 00020000 剩余水量
        // 3016

        int num = cmdData[3];

        List<Map<String, Object>> listRecordinfo = new ArrayList<Map<String, Object>>();
        int index = 6;
        int recordNum = (cmdData[5] & 0xff) * 256 + (cmdData[4] & 0xff);

        for (int i = 0; i < num; i++) {

            // 卡号
            String card_no = CRCEncode.bytes2str(
                    Arrays.copyOfRange(cmdData, index, index = index + 4),
                    true, 0);

            // 用户号
            String user_no = CRCEncode.bytes2str(
                    Arrays.copyOfRange(cmdData, index, index = index + 10),
                    true, 0);
            String use_type = "remote";
            if (0x55 == cmdData[index]) {
                use_type = "card";
            }
            index = index + 1;
            // 开始时间
            String datetime_start = CRCEncode.bytes2str(
                    Arrays.copyOfRange(cmdData, index, index = index + 7),
                    false, 0);
            // 结束时间
            String datetime_end = CRCEncode.bytes2str(
                    Arrays.copyOfRange(cmdData, index, index = index + 7),
                    false, 0);
            // 本次消费金额
            String use_money = CRCEncode.bytes2str(
                    Arrays.copyOfRange(cmdData, index, index = index + 4),
                    false, 2);
            // 剩余金额
            String balance = CRCEncode.bytes2str(
                    Arrays.copyOfRange(cmdData, index, index = index + 4),
                    false, 2);

            String use_amonut = CRCEncode.bytes2str(
                    Arrays.copyOfRange(cmdData, index, index = index + 4),
                    false, 1);

            String lave_amount = CRCEncode.bytes2str(
                    Arrays.copyOfRange(cmdData, index, index = index + 4),
                    false, 1);

            log.error(">>> read hot meter recordNum: " + recordNum);
            Map<String, Object> userMap = new HashMap<String, Object>();
            userMap.put("recordIndex", recordNum);
            recordNum = (cmdData[index + 1] & 0xff) * 256 + (cmdData[index] & 0xff);

            index = index + 2;

            userMap.put("cardNo", card_no);    //卡号
            userMap.put("userNo", user_no);    //用户编号
            userMap.put("useType", use_type);  //用水方式，remote，card
            userMap.put("startUseTime", datetime_start);
            userMap.put("endUseTime", datetime_end);
            userMap.put("useAmount", use_amonut);
            userMap.put("leftAmount", lave_amount);
            userMap.put("unit", "升");
            userMap.put("useMoney", use_money);
            userMap.put("balance", balance);
            listRecordinfo.add(userMap);

        }
        data.put("result", ResultConst.SUCCESS);
        data.put("use_record_list", listRecordinfo);

    }
    // 查询水表用量 返回数据分析
    public static boolean queryMetarAmountResp(byte[] rcv_data,
                                               byte water_meter_type,
                                               Map<String, Object> data) {
        /* 冷水表查询用量响应 */
        byte[] arb_data_rcv = { 0x60, 0x00, 0x00, 0x00,
                0x2C, // /*仪表数据，0x2C表示立方米*/
                0x00, 0x00, 0x00, 0x00, 0x2C, 0x48, 0x01, 0x15, 0x13, 0x01,
                0x18, 0x20, /* 日期时间 */
                (byte) 0x80, /*
							 * bit1~bit0:
							 * 00-阀门打开，01-阀门关闭，11-阀门故障；bit2:1-电池欠压，0-正常
							 */
                0x00 };

        byte index_water_meter_amount = 0;
        byte index_water_meter_amount_len = 4;
        byte index_water_meter_amount_unit = (byte) (index_water_meter_amount + index_water_meter_amount_len);
        byte index_water_meter_amount_remain = (byte) (index_water_meter_amount_unit + 1);
        byte index_water_meter_amount_remain_len = 5;
        byte index_water_meter_datetime = (byte) (index_water_meter_amount_remain + index_water_meter_amount_remain_len);
        byte index_water_meter_datetime_len = 7;
        byte index_water_meter_valve_state = (byte) (index_water_meter_datetime + index_water_meter_datetime_len);
        byte index_water_valve_state_offset = 0;

        String amount = "";
        byte fbyte_enum_hot_meter = 0x50;
        if (fbyte_enum_hot_meter == water_meter_type) {
            amount = ByteUtil.bytes2str(Arrays.copyOfRange(rcv_data,
                    index_water_meter_amount, index_water_meter_amount
                            + index_water_meter_amount_len), false, 1);
            data.put("totalAmount", amount); //总用量
            if (0x2C == rcv_data[index_water_meter_amount_unit]) {
                data.put("unit", "升");
            } else {
                data.put("unit", "unknow：" + rcv_data[index_water_meter_amount_unit]);
            }
        } else {
            amount = ByteUtil.bytes2str(Arrays.copyOfRange(rcv_data,
                    index_water_meter_amount, index_water_meter_amount
                            + index_water_meter_amount_len), false, 2);
            data.put("totalAmount", amount);
            if (0x2C == rcv_data[index_water_meter_amount_unit]) {
                data.put("unit", "立方米");
            } else {
                data.put("unit", "unknow：" + rcv_data[index_water_meter_amount_unit]);
            }
        }

        data.put("currentTime", ByteUtil.bytes2str(Arrays.copyOfRange(rcv_data,
                index_water_meter_datetime, index_water_meter_datetime
                        + index_water_meter_datetime_len), false, 0));

        String sValveState = new String();
        byte bState = (byte) (rcv_data[index_water_meter_valve_state] & 0x3);
        if (0 == bState) {
            sValveState = "open";
        } else if (1 == bState) {
            sValveState = "close";
        } else if (3 == bState) {
            sValveState = "fault";
        } else {
            sValveState = "other";
        }

        String sBatteryState = new String();
        byte bBatteryState = (byte) (rcv_data[index_water_meter_valve_state] & 0x4); // bit
        // 2
        if (4 == bBatteryState) {
            sBatteryState = "欠压";
        } else {
            sBatteryState = "正常";
        }

        data.put("gateState", sValveState);
        data.put("meterState", sBatteryState);

        data.put("result", ResultConst.SUCCESS);
        return true;
    }


    //热水表状态
    public static void hotMeterMeterState(byte[] bytes,
                                          int iDataLenPos,
                                          Map<String, Object> data) {
        // 有人用水
        if (0 != bytes[(iDataLenPos + 1) + 3]) {

            data.put("使用状态", "有人用水");
            int index = (iDataLenPos + 1) + 4;
            String card_no = ByteUtil.bytes2str(Arrays.copyOfRange(bytes, index,
                    index = index + 4), true, 0);

            String user_no = ByteUtil.bytes2str(Arrays.copyOfRange(bytes, index,
                    index = index + 10), true, 0);

            String use_type = (0x55 == bytes[index = index + 1] ? "卡"
                    : "远程");

            String valve_state = (0x01 == bytes[index = index + 1] ? "关"
                    : "开");

            data.put("cardNo", card_no);
            data.put("userNo", user_no);
            data.put("useState", use_type);
            data.put("gateState", valve_state);
        } else {
            data.put("useState", "无人用水");
        }
        data.put("result", "SUCCESS");

    }

    //热水消费记录总数查询
    public static void hotMeterUseRecordNum(byte[] diCmdData,
                                            Map<String, Object> data) {

        // 0A 31 01
        // 03 00 总记录数
        // 01 00 记录起始编号
        // 03 00 记录结束编号
        // 03 00 未读记录总数
        // 01 00 未读起始编号
        // 03 00 未读结束编号
        // E116

        int lnum = diCmdData[3] & 0xff;
        int hnum = (diCmdData[4] & 0xff) * 256;
        data.put("totalNum", lnum + hnum);
        int lstart_index = diCmdData[5] & 0xff;
        int hstart_index = (diCmdData[6] & 0xff) * 256;
        data.put("startIndex", lstart_index + hstart_index);
        int lend_index = diCmdData[7] & 0xff;
        int hend_index = (diCmdData[8] & 0xff) * 256;
        data.put("endIndex", lend_index + hend_index);
        int lun_read_num = diCmdData[9] & 0xff;
        int hun_read_num = (diCmdData[10] & 0xff) * 256;
        data.put("unreadNum", lun_read_num + hun_read_num);
        int lun_read_start_index = diCmdData[11] & 0xff;
        int hun_read_start_index = (diCmdData[12] & 0xff) * 256;
        data.put("unreadStartIndex", lun_read_start_index + hun_read_start_index);
        int lun_read_end_index = diCmdData[13] & 0xff;
        int hun_read_end_index = (diCmdData[14] & 0xff) * 256;
        data.put("unreadEndIndex", lun_read_end_index + hun_read_end_index);

        data.put("result", ResultConst.SUCCESS);
    }

    //用户总数查询
    private static void hotMeterUsreNum(byte[] resp, Map data) {
        data.put("userNums", resp[3]);
        data.put("result", ResultConst.SUCCESS);
    }
    private static void hotMeterUnitPrice(byte[] meter_diser_cmd_data,
                                          Map<String, Object> data) {
        byte[] price = Arrays.copyOfRange(meter_diser_cmd_data, 3, 7);
        StringBuffer pricef = new StringBuffer();
        for (int i = 3; i >= 0; i--) {
            if (i == 0) {
                pricef.append("." + CharacterUtil.bytesToHexString(price[i]));
            } else {
                pricef.append(CharacterUtil.bytesToHexString(price[i]));
            }
        }

        data.put("result", ResultConst.SUCCESS);
        data.put("unitPrice", Float.valueOf(pricef.toString()));
    }

    /**
     * @Description: 查询热水表中用户
     * @Param:
     * @return:
     * @Author: admin
     * @Date: 2019/5/9
     */
    public static void hotMeterUsersQuery(byte[] diCmdData, Map<String, Object> data) {

        // byte[] arb_rcv_diser_cmd_data_resp = {
        // // 数据标识DI，序号SER
        // (byte) 0x08, (byte) 0x31, (byte) 0x01,
        // (byte) 0x03, // 请求回来的用户个数
        // (byte) 0x01, //用户编号
        // (byte) 0x11, (byte) 0x22, (byte) 33,(byte) 44, // 卡号
        // (byte) 0x20, (byte) 0x21, (byte) 22, (byte) 23, (byte) 24,(byte) 25,
        // (byte) 0x26, (byte) 0x27, (byte) 28, (byte) 29,// 用户号
        // (byte) 0xFF,
        // (byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,//密码
        // (byte) 0x50, (byte) 0x10, (byte) 00, (byte) 00, // 余额
        // (byte) 0x50, (byte) 0x10, (byte) 00, (byte) 00//余量
        // };

        int user_count = diCmdData[3];
        List<Map<String, Object>> listUserinfo = Lists.newArrayList();

        int end = 0;
        for (int i = 0; i < user_count; i++) {

            Map<String, Object> userMap = Maps.newHashMap();
            int iPosCard = 5;
            if (i != 0) {
                iPosCard = end;
            }

            //热水卡号长度
            int len_hot_meter_card_no_len = 4;
            //热水用户编号长度
            int len_hot_meter_user_no_len = 10;
            //热水读卡密码长度
            int len_hot_card_pwd_len = 6;
            //热水余额长度
            int len_hot_meter_balance_len = 4;
            //热水剩余量长度
            int len_hot_meter_left_amount_len = 4;

            int iPosUserNo = iPosCard + len_hot_meter_card_no_len;
            int icard_pwd = iPosUserNo + len_hot_meter_user_no_len;
            int iPosBalance = icard_pwd + len_hot_card_pwd_len;
            int iPosLeftAmount = iPosBalance + len_hot_meter_balance_len;
            String card_no = ByteUtil.bytes2str(
                    Arrays.copyOfRange(diCmdData, iPosCard, iPosUserNo), true,
                    0);

            String user_no = ByteUtil.bytes2str(
                    Arrays.copyOfRange(diCmdData, iPosUserNo, icard_pwd), true,
                    0);

            String card_pwd = ByteUtil.bytes2str(
                    Arrays.copyOfRange(diCmdData, icard_pwd, iPosBalance),
                    true, 0);

            String balance = ByteUtil.bytes2str(
                    Arrays.copyOfRange(diCmdData, iPosBalance, iPosLeftAmount),
                    false, 2);

            String leftAmount = ByteUtil.bytes2str(Arrays.copyOfRange(
                    diCmdData, iPosLeftAmount, iPosLeftAmount
                            + len_hot_meter_left_amount_len), false, 1);

            end = iPosLeftAmount + len_hot_meter_left_amount_len + 1;
            userMap.put("result", ResultConst.SUCCESS);
            userMap.put("cardNo", card_no);
            userMap.put("userNo", user_no);
            userMap.put("balance", balance);
            userMap.put("leftAmount", leftAmount);
            userMap.put("password", card_pwd);
            listUserinfo.add(userMap);

        }
        data.put("use_record_list", listUserinfo);
    }
    public static void hotMeterSpecUserResp(byte[] diCmdData, Map<String, Object> data) {

        // byte[] arb_rcv_diser_cmd_data_resp = {
        // // 数据标识DI，序号SER
        // (byte) 0x09, (byte) 0x31, (byte) 0x01,
        // 0x11, 0x22, 0x33,0x44,// 卡号
        // 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11,
        // 0x11, // 用户编号
        // (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
        // (byte) 0xFF, (byte) 0xFF,// 读卡密码
        // 0x00, 0x00, 0x00, 0x00,// 余额
        // 0x00, 0x00, 0x00, 0x00 // 余量
        //
        // };
        int iPosCard = 3;
        int len_hot_meter_left_amount_len = 4;
        // 热水卡号长度
        int len_hot_meter_card_no_len = 4;
        // 热水用户编号长度
        int len_hot_meter_user_no_len = 10;
        // 热水余额长度
        int len_hot_meter_balance_len = 4;

        int len_hot_card_pwd_len = 6;


        int iPosUserNo = iPosCard + len_hot_meter_card_no_len;

        int icard_pwd = iPosUserNo + len_hot_meter_user_no_len;

        int iPosBalance = icard_pwd + len_hot_card_pwd_len;

        int iPosLeftAmount = iPosBalance + len_hot_meter_balance_len;

        String card_no = CRCEncode.bytes2str(
                Arrays.copyOfRange(diCmdData, iPosCard, iPosUserNo), true, 0);

        String user_no = CRCEncode.bytes2str(
                Arrays.copyOfRange(diCmdData, iPosUserNo, icard_pwd), true, 0);

        String card_pwd = CRCEncode.bytes2str(
                Arrays.copyOfRange(diCmdData, icard_pwd, iPosBalance), true, 0);

        String balance = CRCEncode.bytes2str(
                Arrays.copyOfRange(diCmdData, iPosBalance, iPosLeftAmount),
                false, 2);

        String leftAmount = CRCEncode.bytes2str(
                Arrays.copyOfRange(diCmdData, iPosLeftAmount, iPosLeftAmount
                        + len_hot_meter_left_amount_len), false, 1);

        data.put("result", ResultConst.SUCCESS);
        data.put("cardNo", card_no);
        data.put("userNo", user_no);
        data.put("balance", balance);
        data.put("leftAmount", leftAmount);
        data.put("password", card_pwd);

    }

}
