package com.bx.hotcode;

import com.bx.config.HotMsgByteTplReg;
import com.bx.plugin.CodeHandle;
import com.bx.plugin.CodeService;
import com.bx.plugin.HDType;
import com.bx.plugin.ISendMessageEncode;
import com.bx.utils.ByteUtil;
import com.bx.utils.CRCEncode;
import com.bx.utils.CharacterUtil;
import com.bx.utils.StrUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.smart.command.*;
import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Objects;


/*
 * @ program: plugin-hotwater
 * @ description: d
 * @ author: admin
 * @ create: 2019-04-03 09:40
 **/
@CodeService(hdType= HDType.HDTYPE_HOT_METER, dir=HDType.MSG_DIR_SEND)
public class HotMeterEncoder implements ISendMessageEncode {
    private static final Logger LOGGER = LoggerFactory.getLogger(HotMeterEncoder.class);

    @CodeHandle(name="codeHandle")
    public byte[] getOpen(String str) {
        return  null;
    }

    @PostConstruct
    public void init() {
        System.out.println();
    }

    /**
     * @ Description: 发送消息到硬件, 根据commandCode参数(命令),获取到字节码
     * @ Author: Admin
    **/
    @Override
    public byte[] getSendMessage(String commandCode,
                                   String conAddress,
                                   String meterAddress,
                                   byte frameSeq,
                                   Map map) throws Exception{
        byte[] byteTpl = null;
        try {
            LOGGER.info(">>> HotMeterEncoder, getSendMessageByte.");
            int bps = Integer.valueOf(map.get("bps").toString());
            int commPort = Integer.valueOf(map.get("commPort").toString());
            switch (commandCode) {
                case CommandCode.CMD_TIME_SYNC: {
                    SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
                    String dateStr = df.format(new Date());
                    byte[] dateByte = CharacterUtil.hexString2ByteslowHigh(dateStr);

                    byteTpl = HotMsgByteTplReg.hotMeterSyncTime(conAddress,
                            meterAddress,
                            dateByte,
                            frameSeq);
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                case CommandCode.CMD_ON: {
                    String userNo = Objects.nonNull(map.get("userNo")) ? map.get("userNo").toString() : null;
                    byteTpl = HotMsgByteTplReg.openGate(conAddress,
                            meterAddress,
                            userNo,
                            frameSeq);
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                case CommandCode.CMD_OFF: {
                    byteTpl = HotMsgByteTplReg.closeGate(conAddress,
                            meterAddress,
                            frameSeq);
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                //指定用户查询
                case CommandCode.CMD_QUERY_METER_SPEC_USER: {
                    String userNo = Objects.nonNull(map.get("userNo")) ? map.get("userNo").toString() : null;
                    byteTpl = HotMsgByteTplReg.queryMeterSpecUser(conAddress,
                            meterAddress,
                            userNo,
                            frameSeq);
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                //查询表中用户，一次只能查询4个
                case CommandCode.CMD_QUERY_METER_USER: {
                    byte startIndex = Byte.valueOf(map.get("startIndex").toString());
                    byte endIndex = Byte.valueOf(map.get("endIndex").toString());
                    byteTpl = HotMsgByteTplReg.queryMeterUser(conAddress, meterAddress, startIndex, endIndex, frameSeq);

                    //LOGGER.info(">>> HotMeterEncoder, getSendMessage, CMD_QUERY_METER_USER: {}",
                    //        StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                case CommandCode.CMD_SET_UNIT_PRICE: {
                    String price = map.get("price").toString();
                    BigDecimal bigDecimal = new BigDecimal(price).setScale(2, BigDecimal.ROUND_UP);
                    byteTpl = HotMsgByteTplReg.setUnitPrice(conAddress,
                            meterAddress,
                            bigDecimal.floatValue(),
                            frameSeq);
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                case CommandCode.CMD_QUERY_UNIT_PRICE: {
                    byteTpl = HotMsgByteTplReg.queryUnitPrice(conAddress, meterAddress, frameSeq);
                    //LOGGER.info(">>> HotMeterEncoder, getSendMessage, CMD_QUERY_UNIT_PRICE: {}",
                    //        StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                case CommandCode.CMD_QUERY_USER_NUMBER: {
                    byteTpl = HotMsgByteTplReg.queryUserNum(conAddress, meterAddress, frameSeq);
                    //LOGGER.info(">>> HotMeterEncoder, getSendMessage, CMD_QUERY_USER_NUM: {}",
                    //        StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                case CommandCode.CMD_SET_STORE_BLOCK: {
                    String block = Objects.nonNull(map.get("block")) ? map.get("block").toString() : "60";
                    byte blockByte = (byte)(int)Integer.valueOf(block);
                    byteTpl = HotMsgByteTplReg.setStoreBlock(conAddress, meterAddress, blockByte, frameSeq);
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                case CommandCode.CMD_QUERY_STORE_BLOCK: {
                    byteTpl = HotMsgByteTplReg.queryStoreBlock(conAddress, meterAddress, frameSeq);
                    //LOGGER.info(">>> HotMeterEncoder, getSendMessage, CMD_STORE_BLOCK: {}",
                    //        StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                case CommandCode.CMD_USE_RECORD_NUM: {
                    byteTpl = HotMsgByteTplReg.queryUseTotalNum(conAddress, meterAddress, frameSeq);
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                case CommandCode.CMD_USE_BATCH_RECORD: {
                    String startIndex = map.get("startIndex").toString();
                    byteTpl =HotMsgByteTplReg.useRecord(conAddress,
                            meterAddress,
                            startIndex,
                            frameSeq);

                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                case CommandCode.CMD_QUERY_METER_STATE: {
                    byteTpl = HotMsgByteTplReg.queryMeterState(conAddress, meterAddress, frameSeq);
                    //LOGGER.info(">>> HotMeterEncoder, getSendMessage, CMD_METER_STATE: {}",
                    //        StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                case CommandCode.CMD_QUERY_METER_AMOUNT: {
                    byteTpl = HotMsgByteTplReg.queryMeterAmount(conAddress, meterAddress, frameSeq);
                    //LOGGER.info(">>> HotMeterEncoder, getSendMessage, CMD_METER_AMOUNT: {}",
                    //        StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                //删除用户
                case CommandCode.CMD_DEL_USER: {
                    String user = map.get("userNo").toString();
                    byteTpl = HotMsgByteTplReg.cmdDelUser(conAddress, meterAddress, user, frameSeq);
                    //LOGGER.info(">>> HotMeterEncoder, getSendMessage, CMD_DEL_USER: {}",
                    //        StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }
                //添加用户
                case CommandCode.CMD_ADD_USER: {
                    String cardNo = map.get("cardNo").toString();
                    String userNo = map.get("userNo").toString();
                    String balance = Objects.nonNull(map.get("balance")) ? map.get("balance").toString() : "0.00";
                    String leftAmount = Objects.nonNull(map.get("leftAmount")) ? map.get("leftAmount").toString() : "0.00";
                    String cardPasswd = map.get("cardPasswd").toString();

                    byteTpl = HotMsgByteTplReg.cmdAddUser(conAddress,
                            meterAddress,
                            cardNo,
                            userNo,
                            new BigDecimal(balance).setScale(2, BigDecimal.ROUND_UP).floatValue(),
                            new BigDecimal(leftAmount).setScale(1, BigDecimal.ROUND_UP).floatValue(),
                            cardPasswd,
                            frameSeq);
                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);

                    break;
                }
                case CommandCode.CMD_METER_CLEAR: {
                    String cardNo = map.get("cardNo").toString();
                    String userNo = map.get("userNo").toString();
                    String balance = "0.00";
                    String leftAmount =  "0.00";
                    String cardPasswd = map.get("cardPasswd").toString();
                    byteTpl = HotMsgByteTplReg.cmdUpdateUser(conAddress,
                            meterAddress,
                            cardNo,
                            userNo,
                            cardPasswd,
                            new BigDecimal(balance).setScale(2, BigDecimal.ROUND_UP).floatValue(),
                            new BigDecimal(leftAmount).setScale(1, BigDecimal.ROUND_UP).floatValue(),
                            frameSeq);

                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;

                }
                case CommandCode.CMD_METER_RECHARGE: {

                    String cardNo = map.get("cardNo").toString();
                    String userNo = map.get("userNo").toString();
                    String balance = map.get("balance").toString();
                    String leftAmount =  map.get("leftAmount").toString();
                    String cardPasswd = map.get("cardPasswd").toString();
                    byteTpl = HotMsgByteTplReg.cmdUpdateUser(conAddress,
                            meterAddress,
                            cardNo,
                            userNo,
                            cardPasswd,
                            new BigDecimal(balance).setScale(2, BigDecimal.ROUND_UP).floatValue(),
                            new BigDecimal(leftAmount).setScale(1, BigDecimal.ROUND_UP).floatValue(),
                            frameSeq);

                    ByteUtil.setHub485Channel(byteTpl, commPort);
                    ByteUtil.setMeterBps(byteTpl, bps);
                    break;
                }

                default: {
                    LOGGER.error(">>> HotMeterEncoder, getSendMessage, commandCode invalid: {}",
                            commandCode);
                    break;
                }
            }
        } catch (Exception e) {
           LOGGER.error(">>> HotMeterEncoder, getSendMessage, e: {}", e.getMessage());
        }

        return byteTpl;
    }

    public static void main(String[] args) {
        String var1 = "0.43";
        BigDecimal bigDecimal = new BigDecimal(var1).setScale(2, BigDecimal.ROUND_UP);
        float price = bigDecimal.floatValue();
        System.out.println(price);

        byte[] bytes = CharacterUtil.hexString2Bytes("abce");
        System.out.println(bytes);
    }
}
