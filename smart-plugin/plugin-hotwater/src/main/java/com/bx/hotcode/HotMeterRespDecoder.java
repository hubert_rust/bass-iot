package com.bx.hotcode;

import com.bx.constants.MsgConst;
import com.bx.message.inside.IMessage;
import com.bx.message.inside.MsgCommon;
import com.bx.utils.ByteUtil;
import com.bx.utils.CharacterUtil;
import com.bx.utils.StrUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-09 16:53
 **/
public class HotMeterRespDecoder {
    private static final Logger log = LoggerFactory.getLogger(HotMeterRespDecoder.class);

    public static IMessage respMessageProc(byte[] bytes) {

        Map<String, Object> data = Maps.newHashMap();
        MsgCommon<Map> mapMsgCommon = null;
        byte[] address = Arrays.copyOfRange(bytes, 7,
                7 + 5);
        String hexAddress = CharacterUtil.bytesToHexStringlowHigh(address);
        byte index_rcv_com_transmit_len = 18;
        byte fbyte_head = 0x68;
        int iHeadPos = ByteUtil.byte_array_locate(bytes, index_rcv_com_transmit_len + 1, fbyte_head);
        if (iHeadPos < 0) {
            log.error(">>> HotMeterRespDecoder, respMessageProc, iHeadPos <0");
            return null;
        }

        byte index_com_transmit_dt1 = 15 + 1; // 16;
        byte index_com_transmit_da0 = 13 + 1;

        byte[] ar_rcv_com_da0da1_p0 = {0, 0};
        byte[] ar_rcv_com_dt1dt2_f1 = {1, 0};
        if (ByteUtil.bytesCompare(bytes, index_com_transmit_da0,
                ar_rcv_com_da0da1_p0, 0, ar_rcv_com_da0da1_p0.length)
                && ByteUtil.bytesCompare(bytes, index_com_transmit_dt1,
                ar_rcv_com_dt1dt2_f1, 0,
                ar_rcv_com_da0da1_p0.length)) {


            int trans_len = Integer.parseInt(CharacterUtil.bytesToHexString(bytes[index_rcv_com_transmit_len]),
                    16);

            if (trans_len < 0x10) {
                log.error("数据转发响应长度不够, hexAddress: {}", hexAddress);
                return null;
            }

            byte index_frame_seq = 13;
            byte index_hub_addr_start = 7;
            byte index_rcv_offset_meter_addr_start = 2;

            String hubAddrResp = ByteUtil.bytes2str(Arrays.copyOfRange(bytes, index_hub_addr_start,
                    index_hub_addr_start + 5), false, 0);
            byte index_valve_addr = (byte) (iHeadPos + index_rcv_offset_meter_addr_start);
            String meterAddrResp = ByteUtil.bytes2str(Arrays.copyOfRange(
                    bytes, index_valve_addr, index_valve_addr + 7), false, 0);

            byte seq = bytes[index_frame_seq];

            data.put("seq", seq);
            data.put("hubAddress", hubAddrResp);
            data.put("meterAddress", meterAddrResp);

            /* 表返回结果 */
            byte respResultPos = (byte) (index_valve_addr + 7);

            switch (bytes[respResultPos]) {
                case (byte) 0x81: {
                    //查询响应
                    int iMeterTypePos = iHeadPos + 1;
                    /* 用量查询响应 */
                    int iDataLenPos = iHeadPos + (byte) 10;
                    int iMeterAppData = iDataLenPos + 1;

                    int iMeterAppDataLen = (bytes[iDataLenPos] & 0xff);
                    //iMeterAppDataLen = iMeterAppDataLen & 0xff;

                    byte meter_diser_cmd_data[] = Arrays.copyOfRange(bytes, iDataLenPos + 1, iDataLenPos
                            + 4 + iMeterAppDataLen);

                    byte meter_cmd_data[] = Arrays.copyOfRange(bytes, iDataLenPos + 4, iDataLenPos
                            + 4 + iMeterAppDataLen);

                    // DI，序号ser为必备内容
                    if (iMeterAppDataLen < 3) {
                        log.error(">>> HotMeterRespDecoder, respMessageProc, iMeterAppDataLen < 3");
                        break;
                    }

                    //热水表
                    com.bx.hotcode.HotMeterQueryRespDecoder.queryResp(bytes, iMeterTypePos, iDataLenPos,
                            meter_diser_cmd_data, meter_cmd_data, iHeadPos, data);
                    break;
                }
                case (byte)0x84: {
                    //表控制响应
                    int iMeterTypePos = iHeadPos + 1;
                    int iDataLenPos = iHeadPos + 10;
                    int iMeterAppDataLen = (bytes[iDataLenPos] & 0xff);
                    byte meter_diser_cmd_data[] = Arrays.copyOfRange(bytes, iDataLenPos + 1, iDataLenPos
                            + 4 + iMeterAppDataLen);
                    data.put("type", "控制响应");
                    com.bx.hotcode.HotMeterCtrlRespDecoder.ctrlResp(bytes,
                            iMeterTypePos,
                            iDataLenPos,
                            meter_diser_cmd_data,
                            data);
                    break;
                }
                // 查询失败
                case (byte)0xC1: {
                    data.put("type", "查询响应");
                    data.put("result", "FAIL");
                    break;
                }
                // 控制失败
                case (byte)0xC4: {
                    data.put("type", "控制响应");
                    data.put("result", "FAIL");
                    break;
                }
                default: {
                    log.error(">>> HotMeterRespDecoder, respMessageProc,bytes[respResultPos]: {}",
                            bytes[respResultPos]);
                    break;
                }

            }


            //List<Map> list = Lists.newArrayList();
            //list.add(data);

            mapMsgCommon = MsgCommon.MsgCommonBuilder.aMsgCommon()
                    .withMsgType(MsgConst.MSG_TYPE_RESP)
                    .withMsgAddr(hubAddrResp)
                    .withMsgSubAddr(meterAddrResp)
                    .withMsgSeq(seq)
                    .build();
            mapMsgCommon.setRespStr(StrUtil.splitSpace(CharacterUtil.bytesToHexString(bytes)));
            //使用这种方式
            mapMsgCommon.getResp().add(data);
        }
        else {
            log.error(">>> HotMeterRespDecoder, respMessageProc, error Message: {}",
                    StrUtil.splitSpace(CharacterUtil.bytesToHexString(bytes)));
        }

        return mapMsgCommon;
    }

}
