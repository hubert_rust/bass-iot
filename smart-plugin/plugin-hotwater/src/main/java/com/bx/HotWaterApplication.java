package com.bx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

//@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"com.bx"})
public class HotWaterApplication {
    public static void main(String[] args) {
        SpringApplication.run(HotWaterApplication.class, args);
    }
}
