package com.bx.enginecore;

import com.bx.PluginContainer;
import com.bx.constants.MsgConst;
import com.bx.message.inside.IMessage;
import com.bx.plugin.ISendMessageEncode;
import com.bx.regcode.CodeHandleRegister;
import com.bx.utils.CommonMessageFactory;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Maps;
import com.google.common.collect.Queues;
import com.google.common.collect.Table;
import com.smart.command.CommandCode;
import com.smart.monitor.PluginMeterMonitor;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/*
 * @ program: plugin-elec
 * @ description: 集中器模型, 每个集中器对应一个模型(根据平台层配置)
    每个集中器有自己的消息队列，集中器消息阻塞，不会影响其他集中器消息接收和发送
 * @ author: admin
 * @ create: 2019-03-27 07:42
 **/

public class ConcentratorContext extends BaseConcentratorInfo implements ConcentratorContextInterface {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConcentratorContext.class);
    public static final int DEFAULT_CACHE_MESSAGE = 5;

    @Deprecated
    public Table<String, String, PluginMeterMonitor> meterMonitorTable = HashBasedTable.create();

    @Deprecated
    public Table<String, String, PluginMeterMonitor> getMeterMonitorTable() {
        return meterMonitorTable;
    }

    @Deprecated
    public void setMeterMonitorTable(Table<String, String, PluginMeterMonitor> meterMonitorTable) {
        this.meterMonitorTable = meterMonitorTable;
    }

    //状态检测, m
    public volatile long meterMonitorSeq = 0;

    public long getMeterMonitorSeq() { return meterMonitorSeq; }
    public long setNextMeterMonitorSeq() {
        long meters = this.meterMonitorTable.size();
        long next = this.meterMonitorSeq + 1;
        meterMonitorSeq = (next >= meters) ? 0 : next;
        return meterMonitorSeq;
    }
    public void setMeterMonitorSeq(long meterMonitorSeq) {
        this.meterMonitorSeq = (meterMonitorSeq);
    }

    //临时保存请求消息, (通过command_order作为key: 废弃), 需要清理
    //响应消息通过这个找到,key为con_addr+meter_addr_seq;
    public Map<String, IMessage> requestMap = Maps.newHashMap();

    @Override
    public void setHubType(String hubType) { super.hubType = hubType; }
    @Override
    public String getHubType() { return super.hubType; }

    //commondCode for elec
    @Override
    public String getCommandCode() { return super.getCommandCode(); }
    @Override
    public void setCommandCode(String commandCode) { super.setCommandCode(commandCode); }

    @Override
    public boolean setRequestMap(String commandOrder, IMessage map) {
        requestMap.put(commandOrder, map);
        return true;
    }

    @Override
    public IMessage getRequestMap(String commandOrder) {
        return requestMap.get(commandOrder);
    }

    //集中器状态,true表示ok,false表示心态端口或者故障等,默认为false,为了测试设置为true
    public volatile boolean conState = true;

    @Override
    public boolean getConState() {
        return conState;
    }

    @Override
    public void setConState(boolean conState) {
        this.conState = conState;
    }


    //集中器注册到插件上之后, 每个集中器会绑定一个Channel
    public Channel bindChannel;
    @Override
    public Channel getBindChannel() {
        return bindChannel;
    }

    @Override
    public void setBindChannel(Channel bindChannel) {
        this.bindChannel = bindChannel;
    }

    //开关,true表示开:集中器可以下发消息, false表示关: 当前集中器等待消息相应
    //设置场景:1、超时(如果当前是自己的需要开), 2、收到响应后开 3、发送消息关
    //发送消息前需要将消息从conRequest取出
    //public AtomicBoolean conDoor = new AtomicBoolean(true);
    public boolean conDoor = true;

    @Override
    public boolean setDoorState(boolean state) {
        //this.conDoor.set(state);
        this.conDoor = state;
        return true;
    }

    @Override
    public boolean getDoorState() {
        //return this.conDoor.get();
        return this.conDoor;
    }


    //对于发送消息给集中器,但是没有收到响应, 此时conDoor是关闭的
    //需要注意，在超时结束后，需要判断conDoor当前正在处理的messageTick
    //是否和自己的一致，如果相同，需要将conDoor设置为开
    //因为发送消息是在一个线程处理, 该变量是线程安全的
    //就是command_order
    public String currentMessageTick;

    @Override
    public String getCurrentMessageTick() {
        return currentMessageTick;
    }

    @Override
    public void setCurrentMessageTick(String currentMessageTick) {
        this.currentMessageTick = currentMessageTick;
    }


    //收到响应消息后将
    public Long respMessageTick = 0L;


    //请求过来后，放入队列，然后用Countdownlantch wait, 每次从队列取消息后
    //需要检查（可能会超时）
    //public Queue<Map<String, Object>> conRequestQueue = Queues.newConcurrentLinkedQueue();
    public Queue<IMessage> conRequestQueue = Queues.newConcurrentLinkedQueue();


    //响应消息队列: 1、物理集中器正常响应, 2、controller请求超时, 3、集中器状态
    //状态维护在一个线程处理,其中请求超时和集中器状态在commonQueue中处理
    //{"message_type":"RESP, TIMEOUT, STATE"}
    //public Queue<Map<String, Object>> commonQueue = Queues.newConcurrentLinkedQueue();
    public Queue<IMessage> commonQueue = Queues.newConcurrentLinkedQueue();

    //
    //public Map<Long, Object> conRequest = Maps.newConcurrentMap();

    //从1开始, 0有用
    private AtomicLong messageTicket = new AtomicLong(1);

    @Override
    public String getCurrentTicket() {
        return this.currentMessageTick;
    }

    @Override
    public void setAddress(String conAddr) {
        this.conAddr = conAddr;
    }

    @Override
    public String getAddress() {
        return this.conAddr;
    }

    @Override
    public boolean prepareSendMessage() {
        return getConState();
    }

    public boolean sendMessage(String conAddr, byte[] bytes) {
        if (bytes == null || bytes.length <= 0) {
            return false;
        }

        ByteBuf msg = Unpooled.buffer(bytes.length);
        msg.writeBytes(bytes);
        if (getBindChannel() == null) {
            LOGGER.error(">>> ConcenteratorCentext, sendMessage, channle is null");
            return false;
        }
        getBindChannel().writeAndFlush(msg);

        //设置消息占用开始时间
        this.setCmdStartTm(System.currentTimeMillis());
        //如果是发送心跳消息

        return true;
    }

    @Override
    public boolean sendMessage(String conAddr, IMessage map) throws Exception{

        if (map == null || map.getWraper() == null
                || map.getWraper().getByte() == null
                || map.getWraper().getByte().length <=0) {
            return false;
        }
        map.downMsgOutQueueTm();
        byte[] ret = map.getWraper().getByte(); //encode.getSendMessage(conAddr, meterAddr, commandCode, map);
        ByteBuf msg = Unpooled.buffer(ret.length);
        msg.writeBytes(ret);
        if (getBindChannel() == null) {
            LOGGER.error(">>> ConcenteratorCentext, sendMessage, channle is null");
            return false;
        }
        getBindChannel().writeAndFlush(msg);

        //设置消息占用开始时间
        this.setCmdStartTm(System.currentTimeMillis());
        //如果是发送心跳消息
        //pending
        if (map.getCommand().equals(CommandCode.CMD_SEND_HEART_BEAT)) {
            this.setDoorState(true);
        }

        return true;
    }

    @Override
    public boolean setRequest(String messageTick, IMessage map) {
        if (map == null) {
            LOGGER.error(">>> ConcentratorContext, setRequest, map is null, messageTick: {}", messageTick);
            return false;
        }
        //入队列时间
        map.downMsgInQueueTm();
        this.conRequestQueue.offer(map);
        return true;
    }

    @Override
    public IMessage getRequest() {
        return this.conRequestQueue.poll();
    }

    @Override
    public boolean setResponseMessage(String conAddr, IMessage map) {
        if (map == null) {
            return false;
        }
        map.upMsgInQueueTm();
        boolean ret =  this.commonQueue.offer(map);
        if (!ret) {
            LOGGER.error(">>> put resp message to commonQueue fail");
        }
        return ret;
    }

    @Override
    public IMessage getReponseMessage() {
        return commonQueue.poll();
    }

    //集中器注册到插件上之后, 每个集中器会绑定一个Channel
    //后续也放到消息队列里面
    public boolean bindChannel(Channel channel) {
        synchronized (this) {
            this.bindChannel = channel;
        }
        return true;
    }

    public boolean unbindChannel(Channel channel) {
        synchronized (this) {
            this.bindChannel = null;
        }
        return true;
    }


    /**
     * @ Description:
     * @ 消息格式: {}
     * @ Author: Admin
     **/


    /**
     * @ Description: 获取排队序号, 类似一个医生的就诊排队序号,
     * 凌晨通过定时任务将messageTicket重置为1
     * @ Author: Admin
     **/
    public Long getMessageTicket() {
        return messageTicket.getAndIncrement();
    }

    public void resetMessageTicket() {
        //需要检查request请求，如果没有，重置
        messageTicket.set(1);
    }


    //清理消息
    public Queue<Map<String, Map<String, Object>>> releaseQueue = Queues.newConcurrentLinkedQueue();

    @Override
    public void sendReleaseMessage(String commandOrder, String cause) {
        //pending
        Map map = CommonMessageFactory.getCommonMessage(this.conAddr, MsgConst.MSG_TYPE_RELEASE);
        map.put("commandOrder", commandOrder);
        map.put("messageCause", cause);
        releaseQueue.offer(map);
    }

    @Override
    public void releaseCache(Map<String, Object> map) {
        if (map == null || map.size() <= 0) return;
        LOGGER.info(">>> loopReleaseCash, {} get release map", getAddress());
        String cmdOrder = map.get("commandOrder").toString();
        this.requestMap.remove(cmdOrder);
    }

    @Override
    public Map getRelease() {
        return this.releaseQueue.poll();
    }


    /**
     * @ Description: seq
     * @ Author: Admin
     **/
    public Map<String, Wrapbyte> meterSeqMap = Maps.newHashMap();

    @Override
    public void setMeterAddr(String meterAddr) {
        meterSeqMap.put(meterAddr, new Wrapbyte());
/*        PluginMeterMonitor meterMonitor = (PluginMeterMonitor) PluginContainer.getTable().get(this.getAddress(), meterAddr);

        meterMonitor.setSeqNum(this.meterMonitorTable.size());
        this.meterMonitorTable.put(this.getConAddr(),
                meterAddr,
                meterMonitor);*/
    }

    /**
     * @Description: 进入队列之前分配, 提供效率
     * @Param:
     * @return:
     * @Author: admin
     * @Date: 2019/5/9
     */
    @Override
    public  byte getMeterSeq(String conAddr, String meterAddr) throws  Exception{
        Wrapbyte wrapbyte = meterSeqMap.get(meterAddr);
        if (wrapbyte == null) {
            //锁表地址FF表示广播
            if (meterAddr.equals("FF") || meterAddr.equals("00")) {
                return (byte)0x01;
            }
            else {
                throw new Exception("meterAddr:" + meterAddr + "没有配置");
            }
        }
        //此处算法完全移植
        int index = wrapbyte.getIndex();
        byte seq = wrapbyte.getSeq();
        if (index == 0) {
        } else if (index >= 15) {
            seq = ((byte) 0x60);
            index = 0;
        } else {
            seq = (byte) (seq + 1);
        }
        index = index + 1;
        wrapbyte.setIndex(index);
        wrapbyte.setSeq(seq);
        LOGGER.info(">>> getMeterSeq, seq: {}-{}-{}", conAddr, meterAddr, seq);
        return seq;
    }

    public class Wrapbyte {
        public byte seq = 0x60;
        public int index = 0;

        public byte getSeq() {
            return seq;
        }

        public void setSeq(byte seq) {
            this.seq = seq;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }
    }

    /**
    * @Description: 集中器自检
    * @Param:
    * @return:
    * @Author: admin
    * @Date: 2019/6/26
    */
    public long conWait;
    public long cmdStartTm;

    @Override
    public long getConWait() {
        return conWait;
    }

    @Override
    public void setConWait(long conWait) {
        this.conWait = conWait;
    }

    @Override
    public long getCmdStartTm() {
        return cmdStartTm;
    }

    @Override
    public void setCmdStartTm(long cmdStartTm) {
        this.cmdStartTm = cmdStartTm;
    }
}
