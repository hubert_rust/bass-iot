package com.bx.enginecore;

import com.bx.PluginContainer;
import com.bx.constants.MsgConst;
import com.bx.message.inside.IMessage;
import com.bx.message.inside.MsgCommon;
import com.bx.statenotify.HeartbeatManage;
import com.smart.command.CommandCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/*
 * @ program: 需要有检测机制，虚拟集中器因为是有状态的，
 * @ description: 线程处理
 * @ author: admin
 * @ create: 2019-03-29 09:15
 **/
public interface IReactorThread  {
    public boolean exit();
    public boolean register(ConcentratorContextInterface con);

    //先遍历响应消息
    public void loopRespQueue(ConcentratorContextInterface con);

    public void loopRequestQueue(ConcentratorContextInterface con);

    public void releaseTimeoutReq(ConcentratorContextInterface con);
    public void loopReleaseCache(ConcentratorContextInterface con);
}
