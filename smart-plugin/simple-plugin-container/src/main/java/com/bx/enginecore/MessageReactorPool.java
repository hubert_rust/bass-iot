package com.bx.enginecore;

import com.bx.PluginContainer;
import com.bx.entity.ReactorConfig;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/*
 * @ program: plugin-elec
 * @ description: 消息
 * @ author: admin
 * @ create: 2019-03-28 17:42
 **/
public class MessageReactorPool {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageReactorPool.class);

    public static boolean isInit = false;
    public static int REACTOR_NUM = 1;

    //注册集中器的时候，采用轮循;
    public static AtomicInteger loopNum = new AtomicInteger(0);

    //保存线程
    public static Map<Integer, BaseReactorThread> reactorMap = Maps.newConcurrentMap();

    public static HeartBeatThread heartBeatThread = new HeartBeatThread();

    public static Integer getLoopNum() {
        Integer get = loopNum.getAndIncrement();
        if (get >= REACTOR_NUM) {
            get = 0;
            loopNum.set(0);
        }
        return get;
    }

    public static boolean reset() {
        isInit = false;
        reactorMap.forEach((k,v)->{
            if (v!=null && v instanceof IReactorThread) {
                v.exit();
            }
        });
        return true;
    }
    //注册集中器,一般是根据平台层配置来的,
    public static boolean register(ConcentratorContextInterface con) {
        if (!isInit) {
            Integer reactorNum = PluginContainer.configEntity.getReactorConfig().getReactorNum();
            init(reactorNum, con.getCmdType());
        }
        LOGGER.info(">>> MessageReactor, register con's name: {}.", con.getAddress());
        Integer index = getLoopNum();
        return reactorMap.get(index).register(con);
    }

    public static boolean startup() {
        for (int i = 0; i < REACTOR_NUM; i++) {
            reactorMap.get(i).start();
        }

        //心跳启动2019-07-03,Depraceted
        //heartBeatThread.start();
        return true;
    }

    //解绑集中器
    //理论上不会调用这个接口,
    public static boolean unregister(ConcentratorContextInterface con) {
        return true;
    }

    public static void init(int reactorNum, String hubType) {
        MessageReactorPool.REACTOR_NUM = reactorNum;

        for (int i = 0; i < reactorNum; i++) {
            //ReactorThread reactorThread = new ReactorThread(String.valueOf(i));
            BaseReactorThread reactorThread = ReactorThreadFactory.createReactor(hubType, String.valueOf(i));
            reactorMap.put(Integer.valueOf(i), reactorThread);
        }
        isInit = true;
    }
}
