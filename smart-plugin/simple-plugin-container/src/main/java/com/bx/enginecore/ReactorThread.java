package com.bx.enginecore;

import com.bx.PluginContainer;
import com.bx.message.inside.IMessage;
import com.bx.message.inside.MsgCommon;
import com.bx.constants.MsgConst;
import com.bx.plugin.TimeSpan;
import com.bx.statenotify.HeartbeatManage;
import com.smart.command.CommandCode;
import org.apache.logging.log4j.spi.LoggerRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/*
 * @ program: 需要有检测机制，虚拟集中器因为是有状态的，
 * @ description: 线程处理
 * @ author: admin
 * @ create: 2019-03-29 09:15
 **/
public class ReactorThread extends Thread {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReactorThread.class);
    public String reactorName;
    public volatile boolean stop = false;

    //注册的集中器, 需要有队列保存
    //这里集中器应该抽象一个接口出来, 先实现功能, 每个集中器有自己的消息队列，
    //集中器消息阻塞，不会影响其他集中器消息
    public CopyOnWriteArrayList<ConcentratorContextInterface> registeredCon = new CopyOnWriteArrayList<>();
    public int listLen = 0;

    public ReactorThread(String reactorName) {
        this.reactorName = reactorName;
    }

    public boolean exit() {
        this.stop = true;
        this.interrupt();
        return true;
    }

    public boolean register(ConcentratorContextInterface con) {
        LOGGER.info(">>> {} thread register", reactorName);
        registeredCon.add(con);
        listLen = registeredCon.size();
        return true;
    }

    private void deliverFaultToQueue(String conAddr) {

        MsgCommon iMessage = MsgCommon.MsgCommonBuilder.aMsgCommon()
                .withMsgAddr(conAddr)
                .withMsgType("")
                .withState("nok")
                .build();
        HeartbeatManage.offer(conAddr, iMessage);
    }
    //先遍历响应消息
    public void loopRespQueue(ConcentratorContextInterface con) {

        try {

            IMessage map = con.getReponseMessage();
            boolean current = con.getConState();
            //定时器超时
            if (con.isTimeout()) {
                //投递到消息心跳队列, 创建新消息
                if (current == true && map ==null) {
                    deliverFaultToQueue(con.getAddress());
                }
                con.setConState(false);
            }

            if (map == null) { return; }
            //只要收到响应消息, 就更新
            con.setRcvHeartBeat(System.currentTimeMillis());
            map.upMsgOutQueueTm();

            String hubAddr = map.getPeerAddr();
            String type = map.getType();

            LOGGER.info(">>> loopRespQueue{}, {} get resp map", reactorName, con.getAddress());
            if (type.equals(MsgConst.MSG_TYPE_BEAT_RESP)) {
                //心跳状态
                boolean conState = map.state().equals("ok") == true ? true : false;
                con.setConState(conState);
                long curTm = System.currentTimeMillis();
                con.setRcvHeartBeat(curTm);
                LOGGER.info(">>> loopRespQueue{}, {} state change from {} to {}, curTm: {}",
                        reactorName, con.getAddress(), current, conState, curTm);

                con.setBindChannel(map.channel());
                //投递到消息心跳队列
                HeartbeatManage.offer(hubAddr, map);
            } else if (type.equals(MsgConst.MSG_TYPE_LOGIN_RESP)) {
                //登录消息
                con.setBindChannel(map.channel());
                con.setConState(true);

                con.setRcvHeartBeat(System.currentTimeMillis());
                HeartbeatManage.offer(hubAddr, map);
            } else if (type.equals(MsgConst.MSG_TYPE_RESP)) {

                //找到请求时候保存的, 校验seq, 将响应后解析的结果放入请求的IMessage
                MsgCommon req = (MsgCommon) con.getRequestMap(con.getCurrentMessageTick());
                MsgCommon resp = (MsgCommon) map;

                byte reqMsgSeq = req.msgSeq();
                byte respMsgSeq = map.msgSeq();
                if (reqMsgSeq != respMsgSeq) {
                    LOGGER.error(">>> loopRespQueue{}, {} get resp map", reactorName, con.getAddress());
                }

                req.setRespStr(resp.getRespStr());
                req.getMsgMonitor().setUpDecodeStartTm(resp.getMsgMonitor().getUpDecodeStartTm());
                req.getMsgMonitor().setUpDecodeEndTm(resp.getMsgMonitor().getUpDecodeEndTm());
                req.getMsgMonitor().setUpMsgInQueueTm(resp.getMsgMonitor().getUpMsgInQueueTm());
                req.getMsgMonitor().setUpMsgOutQueueTm(resp.getMsgMonitor().getUpMsgOutQueueTm());

                req.resp().addAll(map.resp());
                String requestState = req.state();
                String currentOrder = map.getOrder();

                req.withState(MsgConst.MSG_STATE_RESP);

                //释放controller的Countdownlatch, 注意先后顺序
                CountDownLatch countDownLatch = req.countdownLatch();
                countDownLatch.countDown();

                //需要设置door状态为开
                boolean curDoor = con.getDoorState();
                con.setDoorState(true);

                LOGGER.info(">>> loopRespQueue{}, {} chang doorstat from {} to {}, commandOrder: {}",
                        reactorName, con.getAddress(), curDoor, con.getDoorState(), currentOrder);
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info(">>> loopRespQueue, e: {}", e.getMessage());
        }
    }

    public void loopRequestQueue(ConcentratorContextInterface con) {
        //检查集中器door状态,true表现可以发送消息
        long currentTm = System.currentTimeMillis();
        String command = con.getCommandCode();
        long wait = con.getConWait()/2;
        if (!con.getDoorState()) {
            //针对锁如果是开始升级命令，等候15秒超时
            if (command.equals(CommandCode.CMD_LOCK_UPDATE_SOFTWARE)) {
                wait = 45000;
            }
            //占用时间过长, 主动释放
            if (currentTm-con.getCmdStartTm() > (wait)) {
                LOGGER.error(">>> ReactorThread, loopRequestQueue, wait timeout: {},con: {}",
                        currentTm-con.getCmdStartTm(),con.toString());
                releaseTimeoutReq(con);
                con.setDoorState(true);
            }
            return;
        }

        //如果集中器状态不ok, 需要设置异常
        //pending
        //if (!con.getConState()) { return; }

        IMessage map = con.getRequest();
        if (map == null) { return; }
        LOGGER.info(">>> loopReuestQueue{}, {} get request map", reactorName, con.getAddress());

        //在队列中已经超时, 将不发生
        if (map.getDownMsgOutQueueTm() > 0
                && currentTm-map.getDownMsgOutQueueTm() > (wait)) {
            LOGGER.error(">>> ReactorThread, loopRequestQueue, timeout: {}, in queue: {}",
                    currentTm-map.getDownMsgOutQueueTm(), map.toString());
            releaseTimeoutReq(con);
            return;
        }

        String current = map.getOrder();
        boolean curDoor = con.getDoorState();
        //设置door状态为关闭状态
        con.setDoorState(false);
        con.setCurrentMessageTick(current);
        con.setCommandCode(map.getCommand());

        LOGGER.info(">>> loopReuestQueue{}, {} chang doorstat from {} to {}, commandOrder: {}",
                reactorName, con.getAddress(), curDoor, con.getDoorState(), current);
        //同时将map加入,清除的时候也在ReactorThread中清除
        String cmdOrder = map.getOrder();
        con.setRequestMap(cmdOrder, map);
        try {
            con.sendMessage(con.getAddress(), map);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info(">>> loopReuestQueue{}, {} sendMessage fail: {}",
                    reactorName, con.getAddress(), e.getMessage());
        }
    }

    private void releaseTimeoutReq(ConcentratorContextInterface con) {
        try {
            MsgCommon req = (MsgCommon) con.getRequestMap(con.getCurrentMessageTick());
            if (req != null) {
                CountDownLatch countDownLatch = req.countdownLatch();
                if (countDownLatch != null) {
                    countDownLatch.countDown();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void loopReleaseCache(ConcentratorContextInterface con) {
        try {
            Map map = con.getRelease();
            con.releaseCache(map);
            if (Objects.nonNull(map)) {
                //对于超时无响应消息, 需要将door状态打开,
                String cause = map.get("messageCause").toString();
                if (cause.equals(MsgConst.MSG_CAUSE_RELEASE_TIMEOUT)) {
                    LOGGER.info(">>> loopReleaseCache, open the door: {}", con.getDoorState());
                    con.setDoorState(true);
                }
            }
        } catch (Exception e) {
            LOGGER.error(">>> loopReleaseCache, e: {}", e.getMessage());
        }
    }

    @Override
    public void run() {
        long tm = PluginContainer.configEntity.getReactorConfig().getReactorSleepTm();
        while (!stop) {
            try {
                TimeUnit.MILLISECONDS.sleep(tm);
                //TimeUnit.MICROSECONDS.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            for (int i = 0; i < listLen; i++) {
                ConcentratorContextInterface contextInterface = registeredCon.get(i);
                //先处理响应队列中消息
                loopRespQueue(registeredCon.get(i));

                //处理controller请求
                loopRequestQueue(registeredCon.get(i));
                //清理缓存
                loopReleaseCache(registeredCon.get(i));
            }

        }
    }
}
