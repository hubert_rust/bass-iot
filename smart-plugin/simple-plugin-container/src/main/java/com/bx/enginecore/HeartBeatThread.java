package com.bx.enginecore;

import com.bx.constants.MsgConst;
import com.bx.message.inside.MsgCommon;
import com.bx.message.msgtpl.MsgByteTpl;
import com.bx.plugin.ISendMessageEncode;
import com.bx.regcode.CodeHandleRegister;
import com.smart.command.CommandCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * @program: plugin
 * @description: 主动给集中器发送心跳, 门锁和闸机，此处只能是读取，不修改状态
 *               2019-7-03, 集中器发送心跳，插件层不发送
 * @author: admin
 * @create: 2019-06-24 14:59
 **/
@Deprecated
public class HeartBeatThread extends Thread {
    private static final Logger log = LoggerFactory.getLogger(HeartBeatThread.class);

    @Override
    public void run() {
        //super.run();
        while (true) {
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //检测超过心跳时长就发送
/*
            ConcentratorHolder.getConMap().values().forEach((v)-> {
                ConcentratorContext con = (ConcentratorContext) v;
                long localSpan = con.getLocalHeartBeatSpan();
                long curTime = System.currentTimeMillis();

                //sendHeartBeat在收到心跳响应后更新，此处不更新状态
                //注意这里使用的是RcvHeartBeat，
                long histTime = con.getRcvHeartBeat();
                if (curTime - histTime >= localSpan) {
                    log.info(">>> HeartBeatThread, timeSpan: {}, curTime: {}, revHeartBeat: {}",
                            curTime-histTime, curTime, histTime);
                    try {
                        String conAddr = con.getConAddr();
                        MsgCommon iMessage = MsgCommon.MsgCommonBuilder.aMsgCommon()
                                .withMsgType(MsgConst.MSG_TYPE_REQ)
                                .withState(MsgConst.MSG_STATE_READY)
                                .withMsgAddr(conAddr)
                                .build();
                        iMessage.setMsgCmdCode(CommandCode.CMD_SEND_HEART_BEAT);
                        String cmdType = con.getCmdType();
                        ISendMessageEncode messageEncode = CodeHandleRegister.funEncodeHandle.get(cmdType);

                        byte[] sendBytes = messageEncode.getSendMessage(CommandCode.CMD_SEND_HEART_BEAT,
                                conAddr, null, (byte)0x0, null);
                        iMessage.setByteWrapper(new MsgByteTpl.ByteWrapper(sendBytes));
                        con.setRequest(String.valueOf(curTime), iMessage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            });
*/

        }
    }
}
