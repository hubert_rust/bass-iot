package com.bx.message.inside;

import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/*
 * @ program: plugin-elec
 * @ description: 内部消息生成
 * @ author: admin
 * @ create: 2019-04-02 11:44
 **/
public class FactoryInsideMessage {
    private static final Logger LOGGER = LoggerFactory.getLogger(FactoryInsideMessage.class);

    //内部消息:
    //1、controller消息
    //2、和物理集中器(TCP链接设备)心跳消息
    //3、物理集中器(TCP连接设备)响应消息
    //4、controller超时消息(清理cache)

    public static Map getCommonMessage(String conAddr, String messageType) {
        Map<String, Object> map =Maps.newHashMap();
        map.put("con_address", conAddr);
        map.put("message_type", messageType);
        return map;
    }
}
