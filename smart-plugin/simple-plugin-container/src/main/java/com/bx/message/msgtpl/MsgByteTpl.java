package com.bx.message.msgtpl;

import com.bx.PluginContainer;

import java.util.Arrays;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-07 17:09
 **/

public class MsgByteTpl {

    /**
    * @Description: clone: true新生成一份，false，使用模板，不允许修改
    * @Param:
    * @return:
    * @Author: admin
    * @Date: 2019/5/7
    */
    public static byte[] getByteTplByMsg(String msg, boolean clone) {
        if (clone) {
            return PluginContainer.msgTpl.get(msg).getByteOfCopy();
        }
        else {
            return PluginContainer.msgTpl.get(msg).getByte();
        }
    }


    public static class ByteWrapper {
        byte[] byteWrapper;

        public ByteWrapper(byte[] byteWrapper) {
            //this.byteWrapper = Arrays.copyOfRange(byteWrapper, 0, byteWrapper.length);
            this.byteWrapper = byteWrapper;
        }

        public byte[] getByte() {
            return byteWrapper;
        }

        public byte[] getByteOfCopy() {
            return Arrays.copyOfRange(byteWrapper, 0, byteWrapper.length);
        }
    }

}
