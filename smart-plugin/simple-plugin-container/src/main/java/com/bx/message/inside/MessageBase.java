package com.bx.message.inside;

import com.bx.message.msgtpl.MsgByteTpl;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import io.netty.channel.Channel;
import org.apache.tomcat.util.digester.ArrayStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/*
 * @ program: plugin-hotwater
 * @ description: base
 * @ author: admin
 * @ create: 2019-04-03 15:49
 **/
public class MessageBase<T> {
    /**
     * @ Description: 消息体定义, Map格式
     * @ msgType:"",
     * @ msgCause:"",
     * @ msgAddr:"",消息关联地址, 就是集中器地址;
     * @ msgSubAddr:"", 消息附加地址, 可以是集中器地址;
     * @ msgSeq:"", 仪表会需要此字段
     * @ reqData:"",请求消息数据Map格式
     * @ respData:"",响应消息数据Map格式
     * @ Author: Admin
     **/
    protected String cmdType;       //区分是哪个插件
    protected String msgCmdCode;
    protected String msgType;
    protected String msgCause;
    protected String msgAddr;
    protected String msgSubAddr;
    protected byte msgSeq;
    protected String commandOrder;
    //@Deprecated
    protected MsgByteTpl.ByteWrapper byteWrapper;

    protected byte[] bytes;
    protected List<T> resp = Lists.newArrayList();
    protected Map<String, Object> reqData = Maps.newHashMap();
    protected List<T> datas;
    protected String state;   //请求消息，中消息状态; 心跳登录消息也会用到
    protected CountDownLatch downLatch;
    protected Channel channel;


    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public String getCmdType() {
        return cmdType;
    }

    public void setCmdType(String cmdType) {
        this.cmdType = cmdType;
    }

    public Map<String, Object> getReqData() {
        return reqData;
    }

    public void setReqData(Map<String, Object> reqData) {
        this.reqData = reqData;
    }

    public String getMsgCmdCode() { return msgCmdCode; }
    public void setMsgCmdCode(String msgCmdCode) { this.msgCmdCode = msgCmdCode; }
    public String getMsgType() { return msgType; }
    public void setMsgType(String msgType) { this.msgType = msgType; }
    public String getMsgCause() { return msgCause; }
    public void setMsgCause(String msgCause) { this.msgCause = msgCause; }
    public String getMsgAddr() { return msgAddr; }
    public void setMsgAddr(String msgAddr) { this.msgAddr = msgAddr; }
    public String getMsgSubAddr() { return msgSubAddr; }
    public void setMsgSubAddr(String msgSubAddr) { this.msgSubAddr = msgSubAddr; }
    public byte getMsgSeq() { return msgSeq; }
    public void setMsgSeq(byte msgSeq) { this.msgSeq = msgSeq; }
    public String getState() { return state; }
    public void setState(String state) { this.state = state; }

    public String getCommandOrder() { return commandOrder; }
    public void setCommandOrder(String commandOrder) { this.commandOrder = commandOrder; }

    public MsgByteTpl.ByteWrapper getByteWrapper() { return byteWrapper; }
    public void setByteWrapper(MsgByteTpl.ByteWrapper byteWrapper) { this.byteWrapper = byteWrapper; }

    public List<T> getResp() { return resp; }
    public void setResp(List<T> resp) { this.resp = resp; }

    public List<T> getDatas() { return datas; }
    public void setDatas(List<T> datas) { this.datas = datas; }

    public CountDownLatch getDownLatch() { return downLatch; }
    public void setDownLatch(CountDownLatch downLatch) { this.downLatch = downLatch; }

    public Channel getChannel() { return channel; }
    public void setChannel(Channel channel) { this.channel = channel; }

}
