package com.bx.message.inside;

import java.util.function.Supplier;

public interface Builder {
    void add(String msgType, Supplier<IMessage> supplier);
}
