package com.bx.message.inside;

import com.google.common.collect.Maps;

import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Supplier;

/*
 * @ program: plugin-hotwater
 * @ description: 消息工厂
 * @ author: admin
 * @ create: 2019-04-03 15:48
 **/
public interface MessageFactory {
    IMessage create(String msgType);

    static MessageFactory factory(Consumer<Builder> consumer) {
        Map<String, Supplier<IMessage>> map =Maps.newHashMap();
        consumer.accept(map::put);
        return key -> map.get(key).get();
    }
}
