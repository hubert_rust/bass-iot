package com.bx.message.inside;

import com.bx.message.msgtpl.MsgByteTpl;
import com.smart.common.MsgMonitor;
import io.netty.channel.Channel;
import lombok.Builder;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/*
 * @ program: plugin-hotwater
 * @ description: con state message
 * @ author: admin
 * @ create: 2019-04-03 16:04
 **/

public class MsgCommon<T> extends MessageBase<T> implements  IMessage<T>, Closeable {
    private MsgMonitor msgMonitor = new MsgMonitor();
    private MsgByteStr msgByteStr = new MsgByteStr();

    private String hubType;

    @Override
    public void close() throws IOException {
        msgByteStr = null;
        msgByteStr = null;
        reqData.clear();
        reqData = null;

    }

    public String getHubType() {
        return hubType;
    }

    public void setHubType(String hubType) {
        this.hubType = hubType;
    }

    public String getReqStr() {
        return msgByteStr.getReqStr();
    }

    public void setReqStr(String reqStr) {
        this.msgByteStr.setReqStr(reqStr);
    }

    public String getRespStr() {
        return this.msgByteStr.getRespStr();
    }

    public void setRespStr(String respStr) {
        this.msgByteStr.setRespStr(respStr);
    }

    public MsgMonitor getMsgMonitor() {
        return msgMonitor;
    }

    public void setMsgMonitor(MsgMonitor msgMonitor) {
        this.msgMonitor = msgMonitor;
    }

    public MsgByteStr getMsgByteStr() {
        return msgByteStr;
    }

    public void setMsgByteStr(MsgByteStr msgByteStr) {
        this.msgByteStr = msgByteStr;
    }

    @Override
    public void downMsgTm() { msgMonitor.setDownMsgTm(System.currentTimeMillis()); }
    @Override
    public void upMsgTm() { msgMonitor.setUpMsgTm(System.currentTimeMillis()); }
    @Override
    public void downEncodeStartTm() { msgMonitor.setDownEncodeStartTm(System.currentTimeMillis()); }
    @Override
    public void downEncodeEndTm() { msgMonitor.setDownEncodeEndTm(System.currentTimeMillis()); }
    @Override
    public void upDecodeStartTm() {msgMonitor.setUpDecodeStartTm(System.currentTimeMillis());}
    @Override
    public void upDecodeEndTm() { msgMonitor.setUpDecodeEndTm(System.currentTimeMillis()); }
    @Override
    public void downMsgInQueueTm() {msgMonitor.setDownMsgInQueueTm(System.currentTimeMillis());}
    @Override
    public void downMsgOutQueueTm() {msgMonitor.setDownMsgOutQueueTm(System.currentTimeMillis());}
    @Override
    public void upMsgInQueueTm() {msgMonitor.setUpMsgInQueueTm(System.currentTimeMillis()); }
    @Override
    public void upMsgOutQueueTm() {msgMonitor.setUpMsgOutQueueTm(System.currentTimeMillis()); }

    @Override
    public long getDownMsgOutQueueTm() {
        return msgMonitor.getDownMsgOutQueueTm();
    }

    @Override
    public List<T> datas() { return super.getDatas(); }

    @Override
    public List<T> resp() { return super.getResp(); }

    @Override
    public byte msgSeq() {return super.getMsgSeq();}
    @Override
    public void withMsgSeq(byte msgSeq) {super.setMsgSeq(msgSeq);}

    @Override
    public void withCountDownLatch(CountDownLatch downLatch) { super.setDownLatch(downLatch); }
    @Override
    public CountDownLatch countdownLatch() { return super.getDownLatch(); }

    @Override
    public Channel channel() { return super.getChannel(); }
    @Override
    public void withChannel(Channel channel) { super.setChannel(channel); }

    @Override
    public String getType() { return super.getMsgType(); }

    @Override
    public String getCommand() { return super.getMsgCmdCode(); }
    @Override
    public void withCommand(String cmdCode) { super.setMsgCmdCode(cmdCode);}

    @Override
    public void withType(String msgType) { super.setMsgType(msgType); }

    @Override
    public String getPeerAddr() { return super.getMsgAddr(); }

    @Override
    public void withPeerAddr(String hubAddr) { super.setMsgAddr(hubAddr); }

    @Override
    public MsgByteTpl.ByteWrapper getWraper() { return super.getByteWrapper(); }

    @Override
    public void withCommandOrder(String commandOrder) { super.setCommandOrder(commandOrder); }
    @Override
    public String getOrder() { return super.getCommandOrder(); }

    @Override
    public String state() { return super.getState(); }
    @Override
    public void withState(String str) { super.setState(str); }


    public static final class MsgCommonBuilder<T> {
        protected String msgType;
        protected String msgCause;
        protected String msgAddr;
        protected String msgSubAddr;
        protected byte msgSeq;
        protected String commandOrder;
        protected MsgByteTpl.ByteWrapper byteWrapper;
        protected String state;
        protected CountDownLatch downLatch;
        private MsgCommonBuilder() {
        }

        public static MsgCommonBuilder aMsgCommon() {
            return new MsgCommonBuilder();
        }

        public MsgCommonBuilder withMsgType(String msgType) {
            this.msgType = msgType;
            return this;
        }

        public MsgCommonBuilder withMsgCause(String msgCause) {
            this.msgCause = msgCause;
            return this;
        }

        public MsgCommonBuilder withMsgAddr(String msgAddr) {
            this.msgAddr = msgAddr;
            return this;
        }

        public MsgCommonBuilder withMsgSubAddr(String msgSubAddr) {
            this.msgSubAddr = msgSubAddr;
            return this;
        }

        public MsgCommonBuilder withMsgSeq(byte msgSeq) {
            this.msgSeq = msgSeq;
            return this;
        }

        public MsgCommonBuilder withCommandOrder(String commandOrder) {
            this.commandOrder = commandOrder;
            return this;
        }

        public MsgCommonBuilder withByteWrapper(MsgByteTpl.ByteWrapper byteWrapper) {
            this.byteWrapper = byteWrapper;
            return this;
        }

        public MsgCommonBuilder withState(String state) {
            this.state = state;
            return this;
        }

        public MsgCommonBuilder withDownLatch(CountDownLatch downLatch) {
            this.downLatch = downLatch;
            return this;
        }
        public MsgCommon build() {
            MsgCommon msgCommon = new MsgCommon();
            msgCommon.setMsgType(msgType);
            msgCommon.setMsgCause(msgCause);
            msgCommon.setMsgAddr(msgAddr);
            msgCommon.setMsgSubAddr(msgSubAddr);
            msgCommon.setMsgSeq(msgSeq);
            msgCommon.setCommandOrder(commandOrder);
            msgCommon.setByteWrapper(byteWrapper);
            msgCommon.setState(state);
            msgCommon.setDownLatch(downLatch);
            return msgCommon;
        }
    }
}
