package com.bx.message.inside;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-10 14:25
 **/
public class MsgByteStr {
    String reqStr;
    String respStr;

    public String getReqStr() {
        return reqStr;
    }

    public void setReqStr(String reqStr) {
        this.reqStr = reqStr;
    }

    public String getRespStr() {
        return respStr;
    }

    public void setRespStr(String respStr) {
        this.respStr = respStr;
    }
}
