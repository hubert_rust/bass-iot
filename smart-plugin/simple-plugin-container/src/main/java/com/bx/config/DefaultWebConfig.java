package com.bx.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/*
 * @ program: plugin-elec
 * @ description: web
 * @ author: admin
 * @ create: 2019-03-27 08:20
 **/
@Component
@PropertySource(value = { "application.yml" })
public class DefaultWebConfig {
    //@Value("${platform.keepUrl}")
    private String keepUrl;

    //@Value("${plugin.pluginId}")
    private String pluginId;

    //@Value("${platform.requestUrl}")
    private String requestUrl;
    //@Value("${server.port}")
    private int ipPort;

    public String getKeepUrl() {
        return keepUrl;
    }

    public void setKeepUrl(String keepUrl) {
        this.keepUrl = keepUrl;
    }

    public String getPluginId() {
        return pluginId;
    }

    public void setPluginId(String pluginId) {
        this.pluginId = pluginId;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public int getIpPort() {
        return ipPort;
    }

    public void setIpPort(int ipPort) {
        this.ipPort = ipPort;
    }
}
