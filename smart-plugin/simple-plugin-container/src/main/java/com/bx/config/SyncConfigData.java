package com.bx.config;

import com.alibaba.fastjson.JSONObject;
import com.bx.PluginContainer;
import com.bx.enginecore.ConcentratorContext;
import com.bx.entity.BassHardwardRequest;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.smart.common.PFConstDef;
import com.smart.common.ResponseUtils;
import com.smart.common.ResultConst;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-08-15 15:03
 **/
public class SyncConfigData {

    public static String syncHubOrMeterConfig(String request) throws Exception {
        BassHardwardRequest bassHardwardRequest = JSONObject.parseObject(request, BassHardwardRequest.class);

        try {
            if (bassHardwardRequest.getServiceType().equals("meter")) {
                syncMeterConfig(bassHardwardRequest);
            } else if (bassHardwardRequest.getServiceType().equals("hub")) {
                syncHubConfig(bassHardwardRequest);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseUtils.getFailResponse(e.getMessage());
        }

        return ResponseUtils.getSuccessResponse(ResultConst.SUCCESS);
    }

    private static String meterCheck(Map<String, Object> map) {

        Object obj = map.get("hubAddress");
        if (Objects.isNull(obj)) {
            return "hubAddress invalid";
        }
        if (Strings.isNullOrEmpty(obj.toString())) {
            return "hubAddress invalid";
        }

        obj = map.get("meterAddress");
        if (Objects.isNull(obj)) {
            return "meterAddress invalid";
        }
        if (Strings.isNullOrEmpty(obj.toString())) {
            return "meterAddress invalid";
        }
        obj = map.get("meterType");
        if (Objects.isNull(obj)) {
            return "meterType invalid";
        }
        if (Strings.isNullOrEmpty(obj.toString())) {
            return "meterType invalid";
        }
        return null;
    }
    private static String hubCheck(Map<String, Object> map) {
        Object obj = map.get("hubAddress");
        if (Objects.isNull(obj)) {
            return "hubAddress invalid";
        }
        if (Strings.isNullOrEmpty(obj.toString())) {
            return "hubAddress invalid";
        }
        obj = map.get("hubType");
        if (Objects.isNull(obj)) {
            return "hubType invalid";
        }
        if (Strings.isNullOrEmpty(obj.toString())) {
            return "hubType invalid";
        }
        obj = map.get("hubMode");
        if (Objects.isNull(obj)) {
            return "hubMode invalid";
        }
        if (Strings.isNullOrEmpty(obj.toString()) && (obj.toString().equals("mix") || obj.toString().equals("single"))) {
            return "hubMode must be mix or single";
        }

        obj = map.get("hubIp");
        if (Objects.isNull(obj)) {
            return "hubIp invalid";
        }
        if (Strings.isNullOrEmpty(obj.toString())) {
            return "hubIp invalid";
        }
        return null;
    }

    private static void syncHubConfig(BassHardwardRequest request) throws Exception {
        List<Map<String, Object>> list = request.getEntitys();

        if (request.getCmdCode().equals(PFConstDef.REQ_CMD_CODE_ADD)) {
            for (Map<String, Object> val: list) {
                String check = hubCheck(val);
                if (!Strings.isNullOrEmpty(check)) {
                    throw new Exception(check);
                }

                String hubAddress = val.get("hubAddress").toString();
                RegisterProcess.initHub(val);
                PluginContainer.concentratorHolder.registerCon(hubAddress, ConcentratorContext.class);
            }
        }
        else if (request.getCmdCode().equals(PFConstDef.REQ_CMD_CODE_MOD)) {

            for (Map<String, Object> val: list) {
                String check = hubCheck(val);
                if (!Strings.isNullOrEmpty(check)) {
                    throw new Exception(check);
                }

                String hubAddress = val.get("hubAddress").toString();
                RegisterProcess.initHub(val);
                PluginContainer.concentratorHolder.registerCon(hubAddress, ConcentratorContext.class);
            }
        }
        else if (request.getCmdCode().equals(PFConstDef.REQ_CMD_CODE_DEL)) {

        }

    }

    private static void syncMeterConfig(BassHardwardRequest request) throws Exception {
        List<Map<String, Object>> list = request.getEntitys();

        if (request.getCmdCode().equals(PFConstDef.REQ_CMD_CODE_ADD)) {
            for (Map<String, Object> val: list) {
                String check = meterCheck(val);
                if (!Strings.isNullOrEmpty(check)) {
                    throw new Exception(check);
                }

                String hubAddress = val.get("hubAddress").toString();
                String meterAddress = val.get("meterAddress").toString();

                RegisterProcess.initConcentratorTable(val);
                PluginContainer.concentratorHolder.registerMeter(hubAddress, meterAddress);
            }

        }
        else if (request.getCmdCode().equals(PFConstDef.REQ_CMD_CODE_MOD)) {
            for (Map<String, Object> val: list) {
                String check = meterCheck(val);
                if (!Strings.isNullOrEmpty(check)) {
                    throw new Exception(check);
                }

                String hubAddress = val.get("hubAddress").toString();
                String meterAddress = val.get("meterAddress").toString();

                RegisterProcess.initConcentratorTable(val);
                PluginContainer.concentratorHolder.registerMeter(hubAddress, meterAddress);
            }

        }
        else if (request.getCmdCode().equals(PFConstDef.REQ_CMD_CODE_DEL)) {

        }

    }
}
