package com.bx.config;

import com.bx.PluginContainer;
import com.bx.entity.ConfigEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.snakeyaml.DumperOptions;
import com.fasterxml.jackson.dataformat.yaml.snakeyaml.Yaml;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

/*
 * @ program: plugin-elec
 * @ description: plugin file config
 * @ author: admin
 * @ create: 2019-03-28 08:10
 **/
public class PluginFileConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(PluginFileConfig.class);
    public static final String CONFIG_FILE_NAME = "setting.yml";

    //传入的是文件名,文件保存在jar包的当前目录
    public static boolean existConfigFile() {
        String path=System.getProperty("user.dir");
        path+=(File.separator + CONFIG_FILE_NAME);

        File tmp = new File(path);
        return new File(path).exists();
    }

    //如果文件不存在
    public static boolean saveConfigToLocal(ConfigEntity configEntity) {
        DumperOptions dumperOptions=new DumperOptions();
        dumperOptions.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        dumperOptions.setDefaultScalarStyle(DumperOptions.ScalarStyle.PLAIN);
        dumperOptions.setPrettyFlow(false);

        Yaml yaml=new Yaml(dumperOptions);
        String path=System.getProperty("user.dir");
        path+=(File.separator + CONFIG_FILE_NAME);

        FileWriter fw=null;
        try {
            fw=new FileWriter(path);
            yaml.dump(configEntity, fw);
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        finally {
        }

        return true;
    }

    public static boolean refreshConfig() {
        return true;
    }

    public static boolean loadLocalConfigToApplication() {
        if (!existConfigFile()) {
            LOGGER.info(">>> loadLocalConfigToApplication fail, config file not exist.");
            return false;
        }

        String path=System.getProperty("user.dir");
        path+=(File.separator + CONFIG_FILE_NAME);
        ObjectMapper objectMapper=null;
        try {
            objectMapper=new ObjectMapper(new YAMLFactory());
            PluginContainer.configEntity = objectMapper.readValue(new FileInputStream(path), ConfigEntity.class);
        } catch (Exception e) {
            LOGGER.info(">>> loadLocalConfigToApplication fail, e: " + e.getMessage());
            e.printStackTrace();
            return false;
        }
        return true;

    }
}
