package com.bx.demo;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.*;
import com.google.common.primitives.Ints;

import java.util.*;

public class App {
    public static void main(String[] args) {
        List<User> list=Lists.newArrayList();
        Map<String, User> map=Maps.newLinkedHashMap();

        String nu= "xx";
        boolean ret = Strings.isNullOrEmpty(nu);
        String var1 = Strings.emptyToNull(nu);
        String var2 = Strings.padEnd(nu, 6, 'a');
        String var3 = Strings.padStart(nu, 7, 'b');

        var2 = "aab";
        var3 = "aacc";
        String var4 = Strings.commonPrefix(var2, var3);

        String toSplitString = "a=b;c=d,e=f";
        Map<String, String> kvs = Splitter.onPattern("[,;]{1,}").withKeyValueSeparator('=')
                .split(toSplitString);
        kvs.keySet().stream().forEach(a->{
            System.out.println("key: " +a + ",val: " + kvs.get(a));
        });

        List<String> listString = Lists.newArrayList("wang", "test", "a");
        Collections.sort(listString, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return Ints.compare(o1.length(), o2.length());
            }
        });

        Collections.sort(listString, Ordering.usingToString());


        List<String> ls=Lists.newArrayListWithCapacity(10);

        Set<User> set=Sets.newHashSet();

        set.add(new User("Aber", "20"));
        set.add(new User("John", "21"));
        Iterator<User> iterable=set.iterator();
        set.stream().forEach(user -> {
            System.out.println(user.age);
        });

        String str=Joiner.on(";").join(Arrays.asList("1b", "d"));

        System.out.println(str);

        MultiMapDemo();

        Iterable<String> iter=Splitter.on(",").trimResults().omitEmptyStrings().split("foo,bar,,   qux");
        iter.forEach(a -> {
            System.out.println(a);
        });

        List<List<Object>> lists = Lists.newArrayList();

        lists.add(Arrays.asList("张三", "男", 22, 10000, "T2", "贵州遵义"));
        lists.add(Arrays.asList("李四", "女", 23, 11000, "T2", "贵州遵义"));
        lists.add(Arrays.asList("王五", "女", 23, 12000, "T3", "北京海淀"));
        lists.add(Arrays.asList("王六", "男", 23, 13000, "T3", "北京昌平"));
        lists.add(Arrays.asList("王七", "男", 24, 14000, "T3", "北京昌平"));
        lists.add(Arrays.asList("王八", "女", 23, 12000, "T2", "北京昌平"));
        lists.add(Arrays.asList("胡三", "男", 26, 12000, "T3", "北京朝阳"));
        lists.add(Arrays.asList("胡四", "男", 26, 13000, "T3", "北京朝阳"));
        lists.add(Arrays.asList("张五", "女", 26, 14000, "T3", "北京海淀"));
        lists.add(Arrays.asList("张六", "男", 27, 17000, "T4", "北京朝阳"));
        lists.add(Arrays.asList("张七", "男", 23, 12000, "T3", "北京朝阳"));
        lists.add(Arrays.asList("黄五", "女", 24, 11000, "T2", "北京海淀"));
        lists.add(Arrays.asList("黄三", "男", 24, 10000, "T2", "北京大兴"));
        lists.add(Arrays.asList("刘爱", "男", 22, 9000, "T1", "北京大兴"));
        lists.add(Arrays.asList("刘跟", "男", 27, 18000, "T4", "贵州遵义"));
        lists.add(Arrays.asList("李根", "男", 28, 20000, "T5", "贵州贵阳"));
        lists.add(Arrays.asList("郭艾琳", "男", 24, 12000, "T3", "贵州贵阳"));

        Map<Integer, Boolean> colSortMaps = Maps.newHashMap();
        colSortMaps.put(2, true); // 年龄升序
        colSortMaps.put(3, true); // 薪资升序
        colSortMaps.put(4, true); // 级别升序

        lists.sort(listComparator(colSortMaps));
        List<List<String>> out = Lists.newArrayList();


        lists.stream().forEach(index-> {
            String aa=Joiner.on(";").join(index);
            System.out.println(aa);
        });

    }
    private static Comparator<List<Object>> listComparator(Map<Integer, Boolean> colSortMaps) {
        Ordering ordering = Ordering.natural();

        return (list1, list2) -> {
            ComparisonChain compareChain = ComparisonChain.start();

            for (Integer index : colSortMaps.keySet()) {
                Object currObj = Optional.ofNullable(list1.get(index)).orElse("");
                Object compObj = Optional.ofNullable(list2.get(index)).orElse("");

                Comparator<Object> objComparator = objComparator(ordering, colSortMaps.getOrDefault(index, true));
                compareChain = compareChain.compare(currObj, compObj, objComparator);
            }

            return compareChain.result();
        };

    }
    private static Comparator<Object> objComparator(Ordering ordering, boolean asc) {
        if (asc) {
            return ordering::compare;
        } else {
            return ordering.reverse()::compare;
        }
    }

    public static void MultiMapDemo() {
        List<Person> personList=new ArrayList<Person>();
        Person person=new Person("孙刚1", "21", "男");
        Person person2=new Person("孙刚2", "21", "男");
        Person person3=new Person("孙刚3", "21", "女");
        Person person4=new Person("孙刚4", "21", "男");
        Person person5=new Person("孙刚5", "21", "女");
        Person person6=new Person("孙刚6", "21", "男");

        personList.add(person);
        personList.add(person2);
        personList.add(person3);
        personList.add(person4);
        personList.add(person5);
        personList.add(person6);
        Multimap<String, Person> myMultimap=ArrayListMultimap.create();
        for (Person val : personList) {
            String sex=val.getSex();
            myMultimap.put(sex, val);
        }

        myMultimap.entries().forEach(a -> {
            System.out.println();
        });

        Map<String, Collection<Person>> map1=myMultimap.asMap();
        map1.entrySet().stream().forEach(a -> {
            a.getValue().stream().forEach(b -> {
                System.out.println(b.getName() + ":" + b.getAge());
            });
        });
    }
}
