package com.bx.demo;


import java.util.Objects;

public class Person {
    String name;
    String age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name=name;
    }


    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age=age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex=sex;
    }

    String sex;

    public Person(String name, String age, String sex) {
        this.name=name;
        this.age=age;
        this.sex=sex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person=(Person) o;
        return com.google.common.base.Objects.equal(name, person.name) &&
                com.google.common.base.Objects.equal(age, person.age) &&
                com.google.common.base.Objects.equal(sex, person.sex);
    }

    @Override
    public int hashCode() {
        return com.google.common.base.Objects.hashCode(name, age, sex);
    }


}
