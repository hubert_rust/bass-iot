package com.bx.service;

import com.bx.PluginContainer;
import com.bx.config.DownloadConfigProcess;
import com.bx.config.RegisterProcess;
import com.bx.config.ShellCommand;
import com.bx.server.CommonServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;

import static com.bx.config.PluginFileConfig.CONFIG_FILE_NAME;

/*
 * @ program: plugin-elec
 * @ description: 维护命令
 * @ author: admin
 * @ create: 2019-03-27 13:52
 **/
@Service
public class PluginMTCommandProcess extends BaseCommandProcess {
    private static final Logger LOGGER=LoggerFactory.getLogger(PluginMTCommandProcess.class);

    static {
        cmdFun.put("MT_STOP_PLUGIN", "mtStopPlugin");
        cmdFun.put("MT_START_PLUGIN", "mtStartPlugin");
        cmdFun.put("MT_RESTART_SERVER", "mtRestartServer");
        cmdFun.put("MT_RESTART_SERVER_REDATA", "mtRestartServerRedata");
        cmdFun.put("MT_RESTART_NETTY", "mtRestartNetty");
        cmdFun.put("MT_SHUTDOWN_NETTY", "mtShutDownNetty");
        cmdFun.put("MT_RELOAD_CONFIG", "mtReloadConfig");

        //

        cmdFun.put("MT_RELOAD_HARDWARE_DATA", "mtReloadHardwareData");
        cmdFun.put("MT_RELOAD_PLUGIN_DATA", "mtReloadPluginData");
        cmdFun.put("MT_METER_PROBE_STOP", "mtMeterProbeStop");
        cmdFun.put("MT_METER_PROBE_START", "mtMeterProbeStart");
    }


    //关闭jar包服务,一般不需要，该命令jar包还在运行，只不过不接收平台层服务
    public static String mtStopPlugin(String request) {
        LOGGER.info(">>> mtStopPlugin, before: applicationState: {}", PluginContainer.applicationState);
        PluginContainer.applicationState.set(false);
        LOGGER.info(">>> mtStopPlugin, after: applicationState: {}", PluginContainer.applicationState);
        return "";
    }

    public static String mtMeterProbeStop(String request) {
        LOGGER.info(">>> mtMeterProbeStop, before: mtMeterProbeStop: {}", PluginContainer.meterProbe);
        PluginContainer.meterProbe.set(false);
        LOGGER.info(">>> mtMeterProbeStop, after: mtMeterProbeStop: {}", PluginContainer.meterProbe);
        return "";
    }

    public static String mtMeterProbeStart(String request) {
        LOGGER.info(">>> mtMeterProbeStart, before: mtMeterProbeStart: {}", PluginContainer.meterProbe);
        PluginContainer.meterProbe.set(true);
        LOGGER.info(">>> mtMeterProbeStart, after: mtMeterProbeStart: {}", PluginContainer.meterProbe);
        return "";
    }
    public static String mtStartPlugin(String request) {
        LOGGER.info(">>> mtStopPlugin, before: applicationState: {}", PluginContainer.applicationState);
        PluginContainer.applicationState.set(true);
        LOGGER.info(">>> mtStopPlugin, after: applicationState: {}", PluginContainer.applicationState);
        return "";
    }
    //重启jar包
    public static String mtRestartServer(String request) {
        LOGGER.info(">>> mtRestartServer");
        return ShellCommand.restartJar();
    }

    //重启jar包
    public static String mtRestartServerRedata(String request) {
        LOGGER.info(">>> mtRestartServerRedata");
        String path=System.getProperty("user.dir");
        path+=(File.separator + CONFIG_FILE_NAME);

        File tmp = new File(path);
        boolean ret = tmp.delete();
        return ShellCommand.restartJar();
    }
    public static String mtRestartNetty(String request) {

        LOGGER.info(">>> mtRestartNetty");
        new Thread() {
            @Override
            public void run() {
                CommonServer.restartServer();
            }
        }.start();

        return "";
    }

    public static String mtReloadHardwareData(String request) {
        LOGGER.info(">>> mtReloadHardwareData");
        RegisterProcess.registerRespProcess(request);
        return "";
    }


    /**
    * @Description: 重新加载yaml配置文件,删除setting.yml文件，然后重启
    * @Param:
    * @return:
    * @Author: admin
    * @Date: 2019/5/6
    */
    public static String mtReloadPluginData(String request) {
        LOGGER.info(">>> mtReloadPluginData");
        String ymlPath = System.getProperty("user.dir") + File.separator + "setting.yml";
        File file = new File(ymlPath);
        boolean delflag = file.delete();
        LOGGER.info(">>>meReloadPluginData, delflag: {}", delflag);

        try {
            DownloadConfigProcess.download(PluginContainer.pluginId);
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
        ShellCommand.restartJar();
        return "";
    }

}
