package com.bx.statenotify;

import com.bx.commonpool.Task;


/**
 * @program: plugin
 * @description: 心跳检测worker
 * @author: admin
 * @create: 2019-07-12 10:45
 **/
public class HeartBeatWorker extends Thread {
    private long secondTimeout;
    private Task task;

    public HeartBeatWorker(Task task, long secondTimeout) {
        this.secondTimeout = secondTimeout;
        this.task = task;
    }

    @Override
    public void run() {
        long tm = secondTimeout <=0 ? 60 : secondTimeout;
        try {
            task.process();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
