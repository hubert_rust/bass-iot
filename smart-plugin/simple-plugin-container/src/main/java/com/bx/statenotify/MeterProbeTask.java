package com.bx.statenotify;

import com.alibaba.fastjson.JSONObject;
import com.bx.PluginContainer;
import com.bx.commonpool.Task;
import com.bx.controller.command.CommonPluginCMDController;
import com.bx.controller.command.ServiceLockObject;
import com.bx.regcode.CodeHandleRegister;
import com.bx.service.IPluginFuncProcess;
import com.bx.utils.HttpUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.smart.command.CommandCode;
import com.smart.monitor.PluginMeterMonitor;
import com.smart.request.BassHardwardRequest;
import com.smart.utils.MapEntityUtil;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-07-16 15:24
 **/
@Deprecated
public class MeterProbeTask extends Task {
    private String meterType;
    private String hubAddress;
    private String meterAddress;

    public MeterProbeTask(String meterType, String hubAddress, String meterAddress) {
        this.meterType = meterType;
        this.hubAddress = hubAddress;
        this.meterAddress = meterAddress;
    }

    @Override
    public void process() throws Exception {
        if (!PluginContainer.meterProbe.get()) {
            return;
        }
        Object ret= null;
        Map<String, Object> request = null;
        try {

            IPluginFuncProcess funcProcess = CodeHandleRegister.funProcess.get(hubAddress);
            ServiceLockObject object = CommonPluginCMDController.getHubLockObject(hubAddress);
            synchronized (object) {
                request = getRequest(this.meterType,
                        this.hubAddress,
                        this.meterAddress);
                ret = funcProcess.process(request, null);
            }
        } catch (Exception e) {
            CommonPluginCMDController.sendRespState(request, false);
            e.printStackTrace();
        }

        CommonPluginCMDController.sendRespState(request, true);
    }

    private static Map getRequest(String meterType,
                                                  String hubAddress,
                                                  String meterAddress) throws Exception{
        BassHardwardRequest request = new BassHardwardRequest();
        request.setCommandOrder(UUID.randomUUID().toString());
        request.setCmdType(meterType);
        request.setHubAddress(hubAddress);
        request.setSubAddress(meterAddress);
        request.setServiceId(PluginContainer.pluginId);
        request.setServiceType("FUNC");
        request.setSubType("func");
        request.setIsRetRespBytes(0);
        request.setIsRetSendBytes(0);
        List<Map<String, Object>> list = Lists.newArrayList();
        Map<String, Object> map = Maps.newHashMap();
        map.put("hubAddress", hubAddress);
        map.put("meterAddress", meterAddress);
        list.add(map);
        request.setEntitys(list);

        //通过meter_type确定
        PluginMeterMonitor meterMonitor = (PluginMeterMonitor) PluginContainer.getTable().get(hubAddress, meterAddress);

        if (meterMonitor.getMeterType().equals(CommandCode.HUB_TYPE_LOCKER)) {
            request.setCmdCode("CMD_LOCK_QUERY_STATE");
        } else if (meterMonitor.getMeterType().equals(CommandCode.HUB_TYPE_ELECMETER)) {
            request.setCmdCode("CMD_METER_STATE");
            map.put("commPort", "2");
            map.put("bps", "6");
        } else if (meterMonitor.getMeterType().equals(CommandCode.HUB_TYPE_COLDMETER)) {
            request.setCmdCode(CommandCode.CMD_QUERY_METER_AMOUNT);
            map.put("commPort", "2");
            map.put("bps", "3");

        } else if (meterMonitor.getMeterType().equals(CommandCode.HUB_TYPE_HIGHMETER)) {
            request.setCmdCode(CommandCode.HIGH_POWER_STATE);
            map.put("commPort", "2");
            map.put("bps", "3");

        } else if (meterMonitor.getMeterType().equals(CommandCode.HUB_TYPE_HOTMETER)) {
            request.setCmdCode(CommandCode.CMD_QUERY_METER_STATE);
            map.put("commPort", "2");
            map.put("bps", "3");

        } else if (meterMonitor.getMeterType().equals(CommandCode.HUB_TYPE_GATE)) {

        }

        return MapEntityUtil.objToMap(request);
    }
}
