package com.bx.entity;


import com.smart.common.MsgMonitor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/*
 * @ program: register-center
 * @ description: 回结果
 * @ author: admin
 * @ create: 2019-03-27 09:30
 **/
public class ResponseEntityD<T> {
    private  String returnCode;
    private  String returnMsg;
    private  String resultCode;
    private  String resultMsg;
    private  String commandOrder;
    private MsgMonitor msgMonitor = new MsgMonitor();
    private Map<String, Object> retBytes;       //码流
    private  List<T> datas = new ArrayList<>(); //保存返回结果

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public List<T> getDatas() {
        return datas;
    }

    public void setDatas(List<T> datas) {
        this.datas = datas;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMsg() {
        return resultMsg;
    }

    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }

    public String getCommandOrder() {
        return commandOrder;
    }

    public void setCommandOrder(String commandOrder) {
        this.commandOrder = commandOrder;
    }

    public MsgMonitor getMsgMonitor() {
        return msgMonitor;
    }

    public void setMsgMonitor(MsgMonitor msgMonitor) {
        this.msgMonitor = msgMonitor;
    }

    public Map<String, Object> getRetBytes() {
        return retBytes;
    }

    public void setRetBytes(Map<String, Object> retBytes) {
        this.retBytes = retBytes;
    }
}
