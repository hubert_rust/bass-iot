package com.bx.entity;


/*
 * @ program: plugin-elec
 * @ description: 插件配置: 心跳
 * @ author: admin
 * @ create: 2019-03-27 12:46
 **/
public class PluginConfig {
    String pluginMode;   //single, mix
    String pluginType;
    boolean beatHeart;  //是否启用心跳
    int  beatHeartSpan; //心跳时长
    boolean download;   //重启后是否下载配置文件
    String pluginId;    //插件ID, 应用层分配
    int countDownTm;    //countdown超时时间

    public String getPluginMode() { return pluginMode; }
    public void setPluginMode(String pluginMode) { this.pluginMode = pluginMode; }
    public String getPluginType() { return pluginType; }
    public void setPluginType(String pluginType) { this.pluginType = pluginType; }
    public boolean isBeatHeart() { return beatHeart; }
    public void setBeatHeart(boolean beatHeart) { this.beatHeart = beatHeart; }
    public int getBeatHeartSpan() { return beatHeartSpan; }
    public void setBeatHeartSpan(int beatHeartSpan) { this.beatHeartSpan = beatHeartSpan; }
    public boolean isDownload() { return download; }
    public void setDownload(boolean download) { this.download = download; }
    public String getPluginId() { return pluginId; }
    public void setPluginId(String pluginId) { this.pluginId = pluginId; }
    public int getCountDownTm() { return countDownTm; }
    public void setCountDownTm(int countDownTm) { this.countDownTm = countDownTm; }
}
