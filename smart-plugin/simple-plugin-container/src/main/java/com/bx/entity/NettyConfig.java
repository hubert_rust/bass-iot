package com.bx.entity;


/*
 * @ program: plugin-elec
 * @ description: netty config
 * @ author: admin
 * @ create: 2019-03-26 16:47
 **/
public class NettyConfig {
    private String ipAddress;
    private int ipPort;
    private int bossGroup;
    private int  workGroup;
    private int soBackLog;
    private int soSndBuf;
    private int soRcvBuf;

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public int getIpPort() {
        return ipPort;
    }

    public void setIpPort(int ipPort) {
        this.ipPort = ipPort;
    }

    public int getBossGroup() {
        return bossGroup;
    }

    public void setBossGroup(int bossGroup) {
        this.bossGroup = bossGroup;
    }

    public int getWorkGroup() {
        return workGroup;
    }

    public void setWorkGroup(int workGroup) {
        this.workGroup = workGroup;
    }

    public int getSoBackLog() {
        return soBackLog;
    }

    public void setSoBackLog(int soBackLog) {
        this.soBackLog = soBackLog;
    }

    public int getSoSndBuf() {
        return soSndBuf;
    }

    public void setSoSndBuf(int soSndBuf) {
        this.soSndBuf = soSndBuf;
    }

    public int getSoRcvBuf() {
        return soRcvBuf;
    }

    public void setSoRcvBuf(int soRcvBuf) {
        this.soRcvBuf = soRcvBuf;
    }
}
