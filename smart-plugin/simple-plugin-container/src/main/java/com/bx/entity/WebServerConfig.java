package com.bx.entity;


/*
 * @ program: plugin-elec
 * @ description: web服务器配置
 * @ author: admin
 * @ create: 2019-03-27 08:14
 **/
public class WebServerConfig {
    private String pluginType;
    private String ipAddress;
    private int ipPort;
    private String uuid;
    private String serverName;
    private int cacheMessageNum;

    public String getPluginType() {
        return pluginType;
    }

    public void setPluginType(String pluginType) {
        this.pluginType = pluginType;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public int getIpPort() {
        return ipPort;
    }

    public void setIpPort(int ipPort) {
        this.ipPort = ipPort;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public int getCacheMessageNum() {
        return cacheMessageNum;
    }

    public void setCacheMessageNum(int cacheMessageNum) {
        this.cacheMessageNum = cacheMessageNum;
    }
}
