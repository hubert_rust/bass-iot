package com.bx.entity;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-07-16 16:43
 **/
public class ProbeConfig {
    private int plugin;
    private int hub;
    private int meter;

    public int getPlugin() {
        return plugin;
    }

    public void setPlugin(int plugin) {
        this.plugin = plugin;
    }

    public int getHub() {
        return hub;
    }

    public void setHub(int hub) {
        this.hub = hub;
    }

    public int getMeter() {
        return meter;
    }

    public void setMeter(int meter) {
        this.meter = meter;
    }
}
