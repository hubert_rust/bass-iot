package com.bx.server;

import com.bx.PluginContainer;
import com.bx.entity.NettyConfig;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 水电表服务端
 * 
 * @author Administrator
 *
 */
public class CommonServer {
	private static final Logger log=LoggerFactory.getLogger(CommonServer.class);

	private static NettyConfig configEntity;


	protected static final int BIZGROUPSIZE = 4; // 处理线程数

	/** 业务出现线程大小 */
	protected static final int BIZTHREADSIZE = 50;

	/**
	 * NioEventLoopGroup实际上就是个线程池,
	 * NioEventLoopGroup在后台启动了n个NioEventLoop来处理Channel事件,
	 * 每一个NioEventLoop负责处理m个Channel,
	 * NioEventLoopGroup从NioEventLoop数组里挨个取出NioEventLoop来处理Channel
	 */
	private static EventLoopGroup bossGroup = null;
	private static EventLoopGroup workerGroup = null;

	public static void setConfig(NettyConfig config) {
		configEntity = config;
	}

	public static void shutdownServer() {
		log.info(">>> shutdownServer...");
		bossGroup.shutdownGracefully();
		workerGroup.shutdownGracefully();
	}

	public static void restartServer() {
		log.info(">>> restartServer, shutdownServer...");
		shutdownServer();

		log.info(">>> startServer, startServer...");
		startServer();
	}


	public static void startServer() {
		log.info(">>> startServer, IP: {}, PORT: {}",
				PluginContainer.configEntity.getNetty().getIpAddress(),
				PluginContainer.configEntity.getNetty().getIpPort());

		bossGroup = new NioEventLoopGroup(PluginContainer.configEntity.getNetty().getBossGroup());
		workerGroup = new NioEventLoopGroup(PluginContainer.configEntity.getNetty().getWorkGroup());

		try {
			ServerBootstrap b = new ServerBootstrap();
			b.group(bossGroup, workerGroup);
			b.channel(NioServerSocketChannel.class);
			b.option(ChannelOption.SO_BACKLOG, PluginContainer.configEntity.getNetty().getSoBackLog());
			b.option(ChannelOption.SO_SNDBUF, PluginContainer.configEntity.getNetty().getSoSndBuf());
			b.option(ChannelOption.SO_RCVBUF, PluginContainer.configEntity.getNetty().getSoRcvBuf());
			b.childHandler(new CommonChannelHandler());
			// 绑定端口、同步等待
			ChannelFuture futrue = b.bind(PluginContainer.configEntity.getNetty().getIpAddress(),
					PluginContainer.configEntity.getNetty().getIpPort()).sync();
			// 等待服务监听端口关闭
			futrue.channel().closeFuture().sync();
		} catch (InterruptedException e) {

		} catch (Exception e) {
		} finally {
			// 退出，释放线程等相关资源
			bossGroup.shutdownGracefully();
			workerGroup.shutdownGracefully();
		}

	}

}
