package com.bx.server;

import com.bx.constants.MsgConst;
import com.bx.enginecore.ConcentratorContextInterface;
import com.bx.enginecore.ConcentratorHolder;
import com.bx.message.inside.IMessage;
import com.bx.message.inside.MsgCommon;
import com.bx.message.msgtpl.MsgByteTpl;
import com.bx.regcode.CodeHandleRegister;
import com.bx.utils.CharacterUtil;
import com.bx.utils.StrUtil;
import com.google.common.base.Strings;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

/*
 * @ program: plugin-hotwater
 * @ description: resp handle
 * @ author: admin
 * @ create: 2019-04-03 15:05
 **/
public class CommonMixRespHandle extends ChannelInboundHandlerAdapter {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommonMixRespHandle.class);

    /**
     * @ Description: 响应消息解码
     * @ Author: Admin
    **/

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        //super.channelRead(ctx, msg);

        ByteBuf byteBuf = (ByteBuf)msg;
        byte[] bytes = new byte[byteBuf.readableBytes()];
        byteBuf.readBytes(bytes);

        Channel channel = ctx.channel();
        //收到响应的消息
        long start = System.currentTimeMillis();
        LOGGER.info(">>> CommonRespHandle, channelRead, receive resp msg1, {}", start);
        IMessage ret = CodeHandleRegister.respMessageAnalysis.respMessageProcess(bytes, channel, false);
        LOGGER.info(">>> CommonRespHandle, channelRead, receive resp msg2, {}", System.currentTimeMillis());


        if (ret == null) {
            LOGGER.error(">>> CommonRespHandle, channelRead, ret is invalid, rcvBytes: {}",
                    StrUtil.splitSpace(CharacterUtil.bytesToHexString(bytes)));
            return;
        }
        long end = System.currentTimeMillis();
        ret.withChannel(channel);

        String respMsg = ret.getType();
        String peerAddr = ret.getPeerAddr();
        if (Strings.isNullOrEmpty(respMsg)) {
            LOGGER.error(">>> CommonRespHandle, channelRead, resp is invalid, rcvBytes: {}",
                    StrUtil.splitSpace(CharacterUtil.bytesToHexString(bytes)));
            return;
        }
        if (Strings.isNullOrEmpty(peerAddr)) {
            LOGGER.error(">>> CommonRespHandle, channelRead, peerAddr is invalid, {}",
                    StrUtil.splitSpace(CharacterUtil.bytesToHexString(bytes)));
            return;
        }
        if (respMsg.equals(MsgConst.MSG_TYPE_LOGIN_RESP)
                || respMsg.equals(MsgConst.MSG_TYPE_BEAT_RESP)
                || respMsg.equals(MsgConst.MSG_TYPE_IM_RESP)) {
            //登录和心跳消息是和对端tcp链接的，可以直接发生响应消息,同时需要将消息投递到消息队列
            MsgByteTpl.ByteWrapper wrapper = (MsgByteTpl.ByteWrapper) ret.getWraper();
            byte[] sendByte = ((MsgCommon)ret).getBytes();
            ByteBuf clientMessage = Unpooled.buffer(sendByte.length);
            clientMessage.writeBytes(sendByte);
            channel.writeAndFlush(clientMessage);

            //发送到队列中
            ConcentratorContextInterface contextInterface = ConcentratorHolder.getCon(peerAddr);
            if (Objects.isNull(contextInterface)) {
                contextInterface = ConcentratorHolder.getCon("FFFF");
                contextInterface.setResponseMessage("FFFF", ret);
            }
            else {
                contextInterface.setResponseMessage(peerAddr, ret);
            }
        }
        else if (respMsg.equals(MsgConst.MSG_TYPE_RESP)) {
            ((MsgCommon)ret).getMsgMonitor().setUpDecodeStartTm(start);
            ((MsgCommon)ret).getMsgMonitor().setUpDecodeEndTm(end);
            //((MsgCommon)ret).withChannel(ctx.channel());

            //发送到队列中
            ConcentratorContextInterface contextInterface = ConcentratorHolder.getCon(peerAddr);
            contextInterface.setResponseMessage(peerAddr, ret);
        }
        else {
            LOGGER.error(">>> CommonRespHandle, channelRead, respMsg not process, respMsg: {}",
                    respMsg.toString());
        }
    }
    @Override
    public void channelActive(final ChannelHandlerContext ctx)
            throws InterruptedException {
        LOGGER.info(">>> channelActive ctx.name: " + ctx.name());
        LOGGER.info(">>> channelActive ctx.channel: " + ctx.channel());

    }

    /**
     * 水电表 集中器 连接ji
     */
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        LOGGER.info(">>> channelInactive ctx.name: " + ctx.name());
        LOGGER.info(">>> channelInactive ctx.channel: " + ctx.channel());

    }

    /**
     * 异常中断捕获异常
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
            throws Exception {
        LOGGER.info(">>> exceptionCaught msg: " + cause.getMessage());
        if (!ctx.channel().isActive()) {
            LOGGER.error(">>> exceptionCaught msg: channel not active");
            ctx.close();
        }
    }
}
