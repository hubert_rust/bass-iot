package com.bx.server;

import java.util.Map;

/*
 * @ program: plugin-hotwater
 * @ description: 响应消息解码
 * @ author: admin
 * @ create: 2019-04-03 14:32
 **/
@Deprecated
public interface IRespMessageDecode {
    /**
     * @ Description:
     * @ Author: Admin
    **/
    Map getRespMessageObj(byte[] resp);
}
