package com.bx.server;


import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 通过接收业务 处理
 * 
 * @author Administrator
 *
 */
@Deprecated
public class CommonServerHandler extends ChannelInboundHandlerAdapter {

	private static final Logger log = LoggerFactory .getLogger(CommonServerHandler.class);
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg)
			throws Exception {

		
		
		
		
		ByteBuf buf = (ByteBuf) msg;
		byte[] req =new byte[buf.readableBytes()]; 
		buf.readBytes(req);
		
		
		
		// 如果数据有效
		/*boolean isValidateResult = DataHandle.validate(req);

		if (isValidateResult) {

			//DataHandle.saveDatatodb(req, "集中器发送到服务器", null);

			byte[] rsp = DataHandle.analysis(req, ctx.channel());

			log.info("confirm_request:"
					+ StrUtil.splitSpace(CharacterUtil.bytesToHexString(req)));
			NettyWriteUtil.writeLoginHeartHex(rsp, ctx.channel());
			//NettyWriteUtil.writeHex(rsp,  ctx.channel(), null);

		} else {

			byte seq=0x00;
			DataHandle.saveDatatodb(req,null,"集中器发送到服务器校验失败", seq, null, null, null);
			log.info("数据校验失败:"
					+ StrUtil.splitSpace(CharacterUtil.bytesToHexString(req)));
			// byte[] rsp = DataHandle.analysis(req, ctx.channel());
		}
*/
	}

	/**
	 * 水
	 */
	@Override
	public void channelActive(final ChannelHandlerContext ctx)
			throws InterruptedException {
		log.info(">>> channelActive ctx.name: " + ctx.name());
		log.info(">>> channelActive ctx.channel: " + ctx.channel());

	}

	/**
	 * 水电表 集中器 连接ji
	 */
	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		log.info(">>> channelInactive ctx.name: " + ctx.name());
		log.info(">>> channelInactive ctx.channel: " + ctx.channel());

	}

	/**
	 * 异常中断捕获异常
	 */
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
			throws Exception {

		log.info(">>> exceptionCaught msg: " + cause.getMessage());
		ctx.close();
	}
}