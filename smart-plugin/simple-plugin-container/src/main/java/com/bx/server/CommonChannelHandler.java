package com.bx.server;

import com.bx.PluginContainer;
import com.smart.command.CommandCode;
import com.smart.common.PFConstDef;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;

/**
 * 水电表通道处理接口定义
 * 
 * @author Administrator
 *
 */
public class CommonChannelHandler extends ChannelInitializer<SocketChannel> {

	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		if (PluginContainer.configEntity.getPlugin().getPluginMode().equals(PFConstDef.PLUGIN_MODE_MIX)) {
			ChannelPipeline p = ch.pipeline();
			p.addLast(new MessageDecoder());
			ch.pipeline().addLast(new CommonMixRespHandle());
		}
		else {
			ChannelPipeline p = ch.pipeline();
			p.addLast(new MessageDecoder());
			ch.pipeline().addLast(new CommonRespHandle());
		}

	}
}
