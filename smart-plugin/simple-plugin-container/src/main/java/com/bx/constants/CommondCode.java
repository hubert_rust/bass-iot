package com.bx.constants;

/*
 * @ program: plugin-elec
 * @ description: command code
 * @ author: admin
 * @ create: 2019-03-26 17:56
 **/
public class CommondCode {

    //维护命令
    //停止服务
    public static final String MT_SHUTDOWN_SERVER = "MT_STOP_PLUGIN";
    //重启服务器
    public static final String MT_RESTART_SERVER = "MT_START_PLUGIN";
    //重启Netty
    public static final String MT_RESTART_NETTY = "MT_RESTART_NETTY";
    //停止Netty
    public static final String MT_SHUTDOWN_NETTY = "MT_SHUTDOWN_NETTY";
    //更新配置
    public static final String MT_RELOAD_CONFIG = "MT_RELOAD_CONFIG";

    public static final String MT_RELOAD_HARDWARE_DATA = "MT_RELOAD_HARDWARE_DATA";
    public static final String MT_RELOAD_PLUGIN_DATA = "MT_RELOAD_PLUGIN_DATA";


    public static final String CMD_ON = "CMD_ON";
    public static final String CMD_OFF = "CMD_OFF";




    //

    //电表操作
    public static final String CMD_METER_ON = "CMD_METER_ON";
    public static final String CMD_METER_OFF = "CMD_METER_OFF";

    //消息解码类型定义
    //普通电表
    public static final String MESSAGE_CODE_SIMPLE_ELEC = "simple_elec_code";
    public static final String MESSAGE_CODE_HIGHPOWER_ELEC = "high_elec_code";
    public static final String MESSAGE_CODE_HOT_WATER = "hot_water_code";
    public static final String MESSAGE_CODE_COLD_WATER = "cold_water_code";
    public static final String MESSAGE_CODE_GATE = "gate_code";
    public static final String MESSAGE_CODE_PIANO = "piano_code";
    public static final String MESSAGE_CODE_LOCK = "lock_code";




}
