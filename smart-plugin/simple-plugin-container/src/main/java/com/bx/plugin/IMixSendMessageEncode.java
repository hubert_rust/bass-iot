package com.bx.plugin;

import java.util.Map;

/**
 * @ Description: 发生消息接口
 * @ Author: Admin
**/
public interface IMixSendMessageEncode extends ISendMessageEncode {
    /**
     * @ Description: 其中meterAddress对没有集中器的(例如闸机),可能没有值
     * @ Author: Admin
    **/
    Map<String, ISendMessageEncode> getEncode();
}
