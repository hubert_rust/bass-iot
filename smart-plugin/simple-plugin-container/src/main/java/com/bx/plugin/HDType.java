package com.bx.plugin;

/*
 * @ program: plugin-hotwater
 * @ description: 对接物理硬件类型
 * @ author: admin
 * @ create: 2019-04-03 12:01
 **/
public class HDType {
    public static final String HDTYPE_HOT_METER = "hot-meter";
    public static final String HDTYPE_COLD_METER = "cold-meter";
    public static final String HDTYPE_ELEC_METER = "elec-meter";
    public static final String HDTYPE_HIGH_METER = "high-meter";
    public static final String HDTYPE_MIX_METER = "mix-meter";
    public static final String HDTYPE_GATE = "gate";
    public static final String HDTYPE_LOCK = "locker";
    public static final String HDTYPE_PIANO = "piano";

    public static final String MSG_DIR_SEND = "send";
    public static final String MSG_DIR_RESP = "resp";
    public static final String MSG_DIR_ANALYSIS = "analysis";
    public static final String MSG_DIR_SERVICE_LOCK = "funcProcess";

}
