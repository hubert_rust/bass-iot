package com.bx.plugin;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-08 18:16
 **/
public class HeartBeatType {
    public static final String HEART_BEAT_TYPE_PEER = "peer_send";
    public static final String HEART_BEAT_TYEP_LOCAL = "local_send";
    public static final String HEART_BEAT_TYPE_DUAL = "dual_send";
}
