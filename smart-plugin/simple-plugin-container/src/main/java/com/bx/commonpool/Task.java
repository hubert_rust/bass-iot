package com.bx.commonpool;

public abstract class Task {
    public abstract  void process() throws Exception;
}
