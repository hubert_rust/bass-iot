package com.bx.regcode;

import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/*
 * @ program: plugin-core
 * @ description: code handle register
 * @ author: admin
 * @ create: 2019-04-02 17:04
 **/
@Deprecated
public class CodeHandleRegisterDemo {

    public static void test1() {
        String classFile2 = "com.bx.loadcode.HotMessageCode";
        try {
            String jarFileName="D:\\backend\\smart-hardware-iot\\smart-plugin\\plugin\\plugin-hotwater\\build\\libs\\plugin-hotwater-0.0.1-SNAPSHOT.jar";
            File file = new File(jarFileName);
            String url = file.toURL().toString();
            URL uu = new URL("jar:"+url+"!/"+"BOOT-INF/classes/com/bx/loadcode/");

            URLClassLoader loader = new URLClassLoader(new URL[]{uu},Thread.currentThread().getContextClassLoader());
            Class<?> p1 = loader.loadClass(classFile2);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }
    public void init() throws Exception{

        test1();
        test();

        String pkgName = "com/code";
        String jarFileName="D:\\backend\\smart-hardware-iot\\smart-plugin\\plugin\\plugin-hotwater\\build\\libs\\plugin-hotwater-0.0.1-SNAPSHOT.jar";
        String path="D:\\backend\\smart-hardware-iot\\smart-plugin\\plugin\\plugin-hotwater\\build\\libs\\plugin-hotwater-0.0.1-SNAPSHOT.jar";

        //pkgName = pkgName.replace(".", "-");
        pkgName = pkgName.replace(".", File.separator);
        try {
            JarFile jarFile = new JarFile(jarFileName);
            Enumeration<JarEntry> en = jarFile.entries();

            String classFile = "BOOT-INF\\classes\\com\\code\\handle\\HotMessageCode.class";
            String classFile2 = "com.bx.loadcode.HotMessageCode";
            String jarUrl = "file://" + path;
            URL url = new URL(jarUrl);

            URLClassLoader loader = new URLClassLoader(new URL[]{url},Thread.currentThread().getContextClassLoader());
            Class<?> p1 = loader.loadClass(classFile2);
            Class<?> p2 = loader.loadClass(classFile);


            //CodeHandleClassLoad classLoad = new CodeHandleClassLoad(path);
            while (en.hasMoreElements()) {
                JarEntry jarEntry = en.nextElement();
                //System.out.println(jarEntry.getName());
                if (jarEntry.isDirectory() || !jarEntry.getName().endsWith(".class")
                        || jarEntry.getName().indexOf(pkgName) <0) {
                    continue;
                }

                String className = jarEntry.getName();
                className = className.substring(0, className.lastIndexOf("."));
                //Class<?>  clz = classLoad.loadClass(className);
                System.out.println("");
            }
            System.out.println("");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void test() {

        String path0="D:\\backend\\smart-hardware-iot\\smart-plugin\\plugin\\plugin-hotwater\\build\\libs\\plugin-hotwater-0.0.1-SNAPSHOT.jar";
        String path="jar:file:D:/backend/smart-hardware-iot/smart-plugin/plugin/plugin-hotwater/build/libs/plugin-hotwater-0.0.1-SNAPSHOT.jar";

        path += "!/";
        String classFile = "com.bx.loadcode.HotMessageCode";
        String pkgName = "com/bx/loadcode";
        File file = new File("file:/" +path0);
        URL url = null;
        try {
            url = new URL("file:" + path0);
            //url=file.toURI().toURL();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        URLClassLoader loader = new URLClassLoader(new URL[]{url},  CodeHandleRegisterDemo.class.getClassLoader());//自己定义的classLoader类，把外部路径也加到load路径里，使系统去该路经load对象
        Class<?> c= null;
        try {
            c = loader.loadClass(classFile);//自己定义的loader路径可以找到
            System.out.println("1");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println();
    }

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException{

        test();

        CodeHandleRegisterDemo ne = new CodeHandleRegisterDemo();
        String path0="D:\\backend\\smart-hardware-iot\\smart-plugin\\plugin\\plugin-hotwater\\build\\libs\\plugin-hotwater-0.0.1-SNAPSHOT.jar";
        String path="jar:file:D:/backend/smart-hardware-iot/smart-plugin/plugin/plugin-hotwater/build/libs/plugin-hotwater-0.0.1-SNAPSHOT.jar";

        path += "!/";
        String classFile = "com.bx.loadcode.HotMessageCode";
        String pkgName = "com/bx/loadcode";
        File file = new File("c:\\free-universe-games-llc\\app.jar");
        URL url =null;
        /*try {
            url=file.toURI().toURL();
            ChildFirstClassLoader urlClassLoader = new ChildFirstClassLoader(new URL[]{url}, CodeHandleRegister.class.getClassLoader());
            Class<?> aClass = urlClassLoader.loadClass(classFile);
            Object o = aClass.newInstance();
            System.out.println();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }*/


        //String path = "D:\\test.jar";//外部jar包的路径
        Set<Class<?>> classes = new LinkedHashSet<Class<?>>();//所有的Class对象
        Map<Class<?>, Annotation[]> classAnnotationMap = new HashMap<Class<?>, Annotation[]>();//每个Class对象上的注释对象
        Map<Class<?>, Map<Method, Annotation[]>> classMethodAnnoMap = new HashMap<Class<?>, Map<Method,Annotation[]>>();//每个Class对象中每个方法上的注释对象
        try {
            JarFile jarFile = new JarFile(new File(path0));
            URL url1 = new URL(path);
            URLClassLoader loader = new URLClassLoader(new URL[]{url1}, CodeHandleRegisterDemo.class.getClassLoader());//自己定义的classLoader类，把外部路径也加到load路径里，使系统去该路经load对象
            Enumeration<JarEntry> es = jarFile.entries();
            while (es.hasMoreElements()) {
                JarEntry jarEntry = (JarEntry) es.nextElement();
                String name = jarEntry.getName();

                if (jarEntry.isDirectory() || !jarEntry.getName().endsWith(".class")
                        || jarEntry.getName().indexOf(pkgName) <0) {
                    continue;
                }
                if(name != null && name.endsWith(".class")){//只解析了.class文件，没有解析里面的jar包
                    //默认去系统已经定义的路径查找对象，针对外部jar包不能用
                    //Class<?> c = Thread.currentThread().getContextClassLoader().loadClass(name.replace("/", ".").substring(0,name.length() - 6));
                    //Class<?> c = loader.loadClass(name.replace("/", ".").substring(0,name.length() - 6));//自己定义的loader路径可以找到
                    /*InputStream is= ne.getClass().getResourceAsStream(name);
                    BufferedReader br=new BufferedReader(new InputStreamReader(is));
                    String s="";
                    while((s=br.readLine())!=null)
                        System.out.println(s);*/
                    Class iInterfaceClass = Class.forName(classFile, true, loader);
                    Class<?> c = loader.loadClass(classFile);//自己定义的loader路径可以找到
                    System.out.println(c);
                    classes.add(c);
                    Annotation[] classAnnos = c.getDeclaredAnnotations();
                    classAnnotationMap.put(c, classAnnos);
                    Method[] classMethods = c.getDeclaredMethods();
                    Map<Method, Annotation[]> methodAnnoMap = new HashMap<Method, Annotation[]>();
                    for(int i = 0;i<classMethods.length;i++){
                        Annotation[] a = classMethods[i].getDeclaredAnnotations();
                        methodAnnoMap.put(classMethods[i], a);
                    }
                    classMethodAnnoMap.put(c, methodAnnoMap);
                }
            }
            System.out.println(classes.size());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
