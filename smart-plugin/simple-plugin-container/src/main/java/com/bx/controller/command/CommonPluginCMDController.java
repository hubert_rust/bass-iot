package com.bx.controller.command;

import com.alibaba.fastjson.JSONObject;
import com.bx.PluginContainer;
import com.bx.config.RegisterProcess;
import com.bx.config.SyncConfigData;
import com.bx.enginecore.ConcentratorContext;
import com.bx.enginecore.ConcentratorContextInterface;
import com.bx.enginecore.ConcentratorHolder;
import com.bx.regcode.CodeHandleRegister;
import com.bx.service.IPluginFuncProcess;
import com.bx.service.PluginFuncProcess;
import com.bx.service.PluginMTCommandProcess;
//import com.bx.stream.StreamContainerWrap;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.util.concurrent.RateLimiter;
//import io.swagger.annotations.Api;
import com.smart.EventType;
import com.smart.command.CommandCode;
import com.smart.command.CommandVal;
import com.smart.common.*;
import com.smart.event.HWStateEvent;
import com.smart.monitor.MonitorType;
import com.smart.request.BassHardwardRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import static com.alibaba.fastjson.JSON.toJSONString;

/* 维护类命令
{
        "serviceType":"PLUGIN",
        "serviceId":"HOTMETER-PLUGIN-001",
        "hubAddress": "0759",
        "subType":"mt",
        "serviceOrder":"S000",
        "commandOrder":"",
        "cmdCode":"",   //比填，维护类通过cmdCode执行
        "cmdType":"",
        "autoPush":0,
        "entitys":[{}],
        }*/

@RestController
@RequestMapping("common")
//@Api(value="container", description="container", tags={"container"})
@Scope("prototype")
public class CommonPluginCMDController {
    private static final Logger log=LoggerFactory.getLogger(CommonPluginCMDController.class);

    private static Map<String, ServiceLockObject> hubServiceMap = new ConcurrentHashMap<>();
    public static RateLimiter rateLimiter;
    static {
        rateLimiter = RateLimiter.create(100);  //每秒产生100个
    }


    @Autowired
    private PluginFuncProcess funcProcess;

    private String checkValid(Map<String, Object> map) {
        if (!PluginContainer.applicationState.get()) {
            return  "服务已关闭";
        }

        Object obj = map.get("serviceType");
        if (obj == null || Strings.isNullOrEmpty(obj.toString()))  {
            return "serviceType is invalid";
        }
        obj = map.get("subType");
        if (obj == null || Strings.isNullOrEmpty(obj.toString())) {
            return "subType is invalid";
        }
        if (obj.toString().equals("MT")
                || obj.toString().equals("func")
                || obj.toString().equals("HEART")
                || obj.toString().equals("monitor")
                || obj.toString().equals("syncData")
                || obj.toString().equals("hubState")) {}
        else {
            return "subType is invalid, subType: " + obj.toString();
        }
        return null;
    }

     /**
     * @ Description: {"command_":"", "meter_address":"", "meter_type":"","command": "ELEC0001", }
     * @ Author: Admin
     * @ 所有到平台应用层的返回结果: ResponseModel
    **/ 
    @RequestMapping(value="command", method=RequestMethod.POST)
    public String command(@RequestBody String request) throws Exception {
        //log.info(">>> command request: {}", request);

        ResponseEntity<Map<String, Object>> resp = new ResponseEntity<>();
        Map<String, Object> map = null;
        try {
            map = (Map<String, Object>) JSONObject.parseObject(request);
        } catch (Exception e) {
            e.printStackTrace();
            resp.setReturnCode("FAIL");
            resp.setReturnMsg(e.getMessage());
            return JSONObject.toJSONString(resp);
        }
        String check = checkValid(map);
        if (!StringUtils.isEmpty(check)) {
            resp.setReturnCode("FAIL");
            resp.setReturnMsg(check);
            return JSONObject.toJSONString(resp);
        }

        String subType = map.get("subType").toString();
        //维护类处理
        if (subType.equals("MT")) {
            String ret = PluginMTCommandProcess.mtCommandProcess(map, request);
            if (StringUtils.isEmpty(ret)) {
                resp.setReturnCode("SUCCESS");
            } else {
                resp.setReturnCode("FAIL");
                resp.setReturnMsg(ret);
            }
            return JSONObject.toJSONString(resp);
        }
        else if (subType.equals("HEART")) {

            //获取下集中器状态
            ConcentratorHolder.getConMap().forEach((k, v) -> {
                ConcentratorContextInterface con = (ConcentratorContextInterface)v;
                Map<String, Object> obj = Maps.newHashMap();
                obj.put("hubAddr", con.getAddress());
                obj.put("conStat", con.getConState()== true ? "ok" : "nok");
                resp.getDatas().add(obj);
            });
            return JSONObject.toJSONString(resp);
        }
        else if (subType.equals("hubState")) {
            String hubAddress = map.get("hubAddress").toString();
            ConcentratorContext con = (ConcentratorContext) ConcentratorHolder.getConMap().get(hubAddress);
            if (Objects.isNull(con)) {
                return ResponseUtils.getFailResponse("该集中器不存在");
            }
            Map<String, Object> obj = Maps.newHashMap();
            obj.put("hubAddr", con.getAddress());
            obj.put("conStat", con.getConState()== true ? "ok" : "nok");
            resp.getDatas().add(obj);
            resp.setReturnCode(ResultConst.SUCCESS);
            resp.setResultCode(ResultConst.SUCCESS);
            return JSONObject.toJSONString(resp);
        }
        else if (subType.equals("syncData")) {
            return SyncConfigData.syncHubOrMeterConfig(request);
        }

        //硬件处理
        String funRet = null;
        try {
            long enter = System.currentTimeMillis();
            map.put("reserved", enter);
            IPluginFuncProcess funcProcess = getInterface(map.get("hubAddress").toString());
            if (Objects.isNull(funcProcess)) {
                throw new Exception("集中器地址不正确或者未同步到网关");
            }

            //ServiceLockObject object = getHubLockObject(map.get("hubAddress").toString());
            funRet = funcProcess.process(map, subType);
            //funcProcess.process(map, subType );
        } catch (Exception e) {
            sendRespState(map, false);
            e.printStackTrace();
            return ResponseUtils.getFailResponse(e.getMessage());
        }

        sendRespState(map, true);
        return funRet;
    }
    public static void sendRespState(Map<String, Object> map, boolean success) {
        String hubAddress = map.get("hubAddress").toString();
        String cmdType = map.get("cmdType").toString();
        Map<String, Object> entity = (Map)((List)map.get("entitys")).get(0);
        String meterAddress = entity.get("meterAddress").toString();
        ResponseState sender = new ResponseState();
        sender.setReturnCode(ResultConst.SUCCESS);

        List<HWStateEvent> list = Lists.newArrayList();
        HWStateEvent hwStateEvent = new HWStateEvent();
        hwStateEvent.setHubAddress(hubAddress);
        hwStateEvent.setSubAddress(meterAddress);
        hwStateEvent.setMeterType(cmdType);
        hwStateEvent.setState( success ? "ok" : "nok");

        hwStateEvent.setEventType(CommandVal.EVENT_TYPE_STATE);
        hwStateEvent.setMainClass(MonitorType.MONITOR_CLASS_METER);
        list.add(hwStateEvent);
        sender.setDatas(list);

        //StreamContainerWrap.sendState(JSONObject.toJSON(sender));
    }

    public static ServiceLockObject getHubLockObject(String hubAddress) {
        ServiceLockObject hubLockObject = hubServiceMap.get(hubAddress);

        if (Objects.isNull(hubLockObject)) {
            ServiceLockObject newHubLockObject = new ServiceLockObject();
            hubServiceMap.put(hubAddress, newHubLockObject);
            return newHubLockObject;
        }
        return hubLockObject;
    }
    private IPluginFuncProcess getInterface(String hubAddress) throws Exception{
        //每个集中器分配一个
        IPluginFuncProcess funcProcess = CodeHandleRegister.funProcess.get(hubAddress);
        if (Objects.isNull(funcProcess)) {
            throw new Exception("集中器地址不正确或者未同步到网关");
        }

        return funcProcess;
    }

}
