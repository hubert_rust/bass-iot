package com.bx.controller.command;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-08-08 19:15
 **/
public class ServiceLockObject {
    String hubAddress;
    Object lock;

    public String getHubAddress() {
        return hubAddress;
    }

    public void setHubAddress(String hubAddress) {
        this.hubAddress = hubAddress;
    }

    public Object getLock() {
        return lock;
    }

    public void setLock(Object lock) {
        this.lock = lock;
    }

}
