package com.bx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-07-10 13:54
 **/
@SpringBootApplication(scanBasePackages = {"com.bx"})
public class HighMeterApplication {
    public static void main(String[] args) {
        SpringApplication.run(HighMeterApplication.class, args);
    }
}
