package com.bx.message;

import com.smart.command.CommandCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class MessageEncodeHighPower extends com.bx.message.MethodBase implements com.bx.message.IMeterMessageByte {
    private static Logger log = LoggerFactory.getLogger(MessageEncodeHighPower.class);

    static {
        opCodeToFun.put(CommandCode.HIGH_POWER_OPEN_ACCOUNT,   "getBytesOfOpenAccount");
        opCodeToFun.put(CommandCode.HIGH_POWER_TIME_SYNC,      "getBytesOfTimeSync");
        opCodeToFun.put(CommandCode.HIGH_POWER_STATE,          "getBytesOfState");
        opCodeToFun.put(CommandCode.HIGH_POWER_LEFT_AMOUNT,    "getBytesOfLeftAmount");
        //opCodeToFun.put(CommandCode.HIGH_POWER_BALANCE_CLEAR,  "getBytesOfBalanceClear");
        opCodeToFun.put(CommandCode.HIGH_POWER_RECHARGE_TIMES, "getBytesOfRechargeTimes");
        opCodeToFun.put(CommandCode.HIGH_POWER_RECHARGE,       "getBytesOfRecharge");
        opCodeToFun.put(CommandCode.HIGH_POWER_REMOTE_ON,      "getBytesOfRemoteOn");
        opCodeToFun.put(CommandCode.HIGH_POWER_REMOTE_OFF,     "getBytesOfRemoteOff");
        opCodeToFun.put(CommandCode.HIGH_POWER_READ_METER,     "getBytesOfReadMeter");
        opCodeToFun.put(CommandCode.HIGH_POWER_CLOSE_ACCOUNT,  "getBytesOfCloseAccount");
        //opCodeToFun.put(CommandCode.HIGH_POWER_QUERY_TIME,     "getBytesOfQueryTime");
        opCodeToFun.put(CommandCode.HIGH_POWER_QUERY_DATE,     "getBytesOfQueryDate");

    }

    @Override
    public byte[] getMessageBytes(String cmdCode,
                                  String hubAddress,
                                  String meterAddress,
                                  Map<String, Object> map) {
        byte[] getBytes = getMeterByte(this, cmdCode, hubAddress, meterAddress, map);
        return getBytes;
    }


    public static byte[] getBytesOfQueryDate(String hubAddress,
                                             String meterAddress,
                                             byte seq,
                                             Map<String, Object> map) {
        log.info(">>> MessageEncodeHighPower->getBytesOfQueryDate");
        //Byte seq      = (byte)map.get("seq");
        int  bps      = Integer.valueOf(map.get("bps").toString());
        int  transmit = Integer.valueOf(map.get("commPort").toString());
        com.bx.message.EncodeWattHourProtocol p = new com.bx.message.EncodeWattHourProtocol(hubAddress, meterAddress);
        p.setBps(bps);
        p.setTransmitChannel(transmit);
        p.setFrameSeq((Byte)seq);
        return p.queryDate();
    }
    public static byte[] getBytesOfOpenAccount(String hubAddress,
                                         String meterAddress,
                                         byte seq,
                                         Map<String, Object> map) {
        log.info(">>> MessageEncodeHighPower->getBytesOfOpenAccount");
        //Byte seq      = (byte)map.get("seq");
        int  bps      = Integer.valueOf(map.get("bps").toString());
        int  transmit = Integer.valueOf(map.get("commPort").toString());
        com.bx.message.EncodeWattHourProtocol p = new com.bx.message.EncodeWattHourProtocol(hubAddress, meterAddress);
        p.setBps(bps);
        p.setTransmitChannel(transmit);
        p.setFrameSeq((Byte)seq);
        return p.wattHourOpenAccount(0.01f, 1);
    }
    public static byte[] getBytesOfState(String hubAddress,
                                         String meterAddress,
                                         byte seq,
                                         Map<String, Object> map) {
        log.info(">>> MessageEncodeHighPower->getBytesOfState");
        //Byte seq = (byte)map.get("seq");
        //return  CommonUtils.getHighBytesforStatue(hubAddress, meterAddress, seq, map);

        int  bps      = Integer.valueOf(map.get("bps").toString());
        int  transmit = Integer.valueOf(map.get("commPort").toString());
        //byte seqByte        = byte.
        com.bx.message.EncodeWattHourProtocol p = new com.bx.message.EncodeWattHourProtocol(hubAddress, meterAddress);
        p.setBps(bps);
        p.setTransmitChannel(transmit);
        p.setFrameSeq((Byte)seq);
        return p.queryWattHourState();
    }
    public static byte[] getBytesOfLeftAmount(String hubAddress,
                                              String meterAddress,
                                              byte seq,
                                              Map<String, Object> map) {
        log.info(">>> MessageEncodeHighPower->getBytesOfLeftBalance");
        //Byte seq      = (byte)map.get("seq");
        int  bps      = Integer.valueOf(map.get("bps").toString());
        int  transmit = Integer.valueOf(map.get("commPort").toString());
        //int  state    = Integer.valueOf(StrUtil.getStr(map.get("remote_state")));
        com.bx.message.EncodeWattHourProtocol p = new com.bx.message.EncodeWattHourProtocol(hubAddress, meterAddress);
        p.setBps(bps);
        p.setTransmitChannel(transmit);
        p.setFrameSeq((Byte)seq);
        return p.queryWattSoc();
    }

    @Deprecated
    public static byte[] getBytesOfBalanceClear(String hubAddress,
                                                String meterAddress,
                                                byte seq,
                                                Map<String, Object> map) {
        log.info(">>> MessageEncodeHighPower->getBytesOfBalanceClear");
        return null;
    }

    public static byte[] getBytesOfRechargeTimes(String hubAddress,
                                                 String meterAddress,
                                                 byte seq,
                                                 Map<String, Object> map) {
        log.info(">>> MessageEncodeHighPower->getBytesOfRechargeTimes");
        //Byte seq      = (byte)map.get("seq");
        int  bps      = Integer.valueOf(map.get("bps").toString());
        int  transmit = Integer.valueOf(map.get("commPort").toString());
        //int  state    = Integer.valueOf(StrUtil.getStr(map.get("remote_state")));
        com.bx.message.EncodeWattHourProtocol p = new com.bx.message.EncodeWattHourProtocol(hubAddress, meterAddress);
        p.setBps(bps);
        p.setTransmitChannel(transmit);
        p.setFrameSeq((Byte)seq);
        return p.queryRechargeTimes();
    }

    /**
    * @Description: 大功率电表充值
    * @Param:
    * @return:
    * @Author: admin
    * @Date: 2019/7/10
    */
    public static byte[] getBytesOfRecharge(String hubAddress,
                                            String meterAddress,
                                            byte seq,
                                            Map<String, Object> map) {
        log.info(">>> MessageEncodeHighPower->getBytesOfRecharge");
        //Byte seq      = (byte)map.get("seq");
        int  bps      = Integer.valueOf(map.get("bps").toString());
        int  transmit = Integer.valueOf(map.get("commPort").toString());
        int  rechargeTimes = Integer.valueOf(map.get("rechargeTimes").toString());
        rechargeTimes+=1;
        float amount = Float.valueOf(map.get("capacity").toString());

        com.bx.message.EncodeWattHourProtocol p = new com.bx.message.EncodeWattHourProtocol(hubAddress, meterAddress);
        p.setBps(bps);
        p.setTransmitChannel(transmit);
        p.setFrameSeq((Byte)seq);
        return p.wattHourOpenAccount(amount, rechargeTimes);
    }
    public static byte[] getBytesOfRemoteOff(String hubAddress,
                                             String meterAddress,
                                             byte seq,
                                             Map<String, Object> map) {
        log.info(">>> MessageEncodeHighPower->getBytesOfRemoteOff");
        //Byte seq      = (byte)map.get("seq");
        int  bps      = Integer.valueOf(map.get("bps").toString());
        int  transmit = Integer.valueOf(map.get("commPort").toString());
        //int  state    = Integer.valueOf(StrUtil.getStr(map.get("remote_state")));
        com.bx.message.EncodeWattHourProtocol p = new com.bx.message.EncodeWattHourProtocol(hubAddress, meterAddress);
        p.setBps(bps);
        p.setTransmitChannel(transmit);
        p.setFrameSeq((Byte)seq);
        return p.meterGateClose();
    }
    public static byte[] getBytesOfRemoteOn(String hubAddress,
                                            String meterAddress,
                                            byte seq,
                                            Map<String, Object> map) {
        log.info(">>> MessageEncodeHighPower->getBytesOfRemoteOn");
        //Byte seq      = (byte)map.get("seq");
        int  bps      = Integer.valueOf(map.get("bps").toString());
        int  transmit = Integer.valueOf(map.get("commPort").toString());
        //int  state    = Integer.valueOf(StrUtil.getStr(map.get("remote_state")));
        com.bx.message.EncodeWattHourProtocol p = new com.bx.message.EncodeWattHourProtocol(hubAddress, meterAddress);
        p.setBps(bps);
        p.setTransmitChannel(transmit);
        p.setFrameSeq((Byte)seq);
        return p.meterGateOpen();
    }

    /**
    * @Description: 销户,注意销户的时候,需要抄下表
    * @Param:
    * @return:
    * @Author: admin
    * @Date: 2019/7/10
    */
    public static byte[] getBytesOfCloseAccount(String hubAddress,
                                                String meterAddress,
                                                byte seq,
                                                Map<String, Object> map) {
        log.info(">>> MessageEncodeHighPower->getBytesOfCloseAccount");
        //Byte seq      = (byte)map.get("seq");
        int  bps      = Integer.valueOf(map.get("bps").toString());
        int  transmit = Integer.valueOf(map.get("commPort").toString());
        com.bx.message.EncodeWattHourProtocol p = new com.bx.message.EncodeWattHourProtocol(hubAddress, meterAddress);
        p.setBps(bps);
        p.setTransmitChannel(transmit);
        p.setFrameSeq((Byte)seq);
        return p.meterCloseAccount();
    }
    public static byte[] getBytesOfReadMeter(String hubAddress,
                                             String meterAddress,
                                             byte seq,
                                             Map<String, Object> map) {
        log.info(">>> MessageEncodeHighPower->getBytesOfReadMeter");
        //Byte seq      = (byte)map.get("seq");
        int  bps      = Integer.valueOf(map.get("bps").toString());
        int  transmit = Integer.valueOf(map.get("commPort").toString());
        com.bx.message.EncodeWattHourProtocol p = new com.bx.message.EncodeWattHourProtocol(hubAddress, meterAddress);
        p.setBps(bps);
        p.setTransmitChannel(transmit);
        p.setFrameSeq((Byte)seq);
        return p.readMeter();
    }
    public static byte[] getBytesOfTimeSync(String hubAddress,
                                            String meterAddress,
                                            byte seq,
                                            Map<String, Object> map) {
        log.info(">>> MessageEncodeHighPower->getBytesOfTimeSync");
        //Byte seq      = (byte)map.get("seq");
        int  bps      = Integer.valueOf(map.get("bps").toString());
        int  transmit = Integer.valueOf(map.get("commPort").toString());
        com.bx.message.EncodeWattHourProtocol p = new com.bx.message.EncodeWattHourProtocol(hubAddress, meterAddress);
        p.setBps(bps);
        p.setTransmitChannel(transmit);
        p.setFrameSeq((Byte)seq);
        return p.meterTimeSync();
    }
}
