package com.bx.message;

import com.bx.PluginContainer;
import com.bx.enginecore.ConcentratorHolder;
import com.bx.utils.ByteUtil;
import com.bx.utils.CharacterUtil;
import com.google.common.base.Preconditions;
import com.smart.command.CommandCode;
import com.smart.common.ResultConst;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class DecodeWattHourProtocol {
    private static final Logger log = LoggerFactory.getLogger(DecodeWattHourProtocol.class);
    public static final int DATA_FLAG_LEN = 4;
    //public static final int DATA_LEN_OFFSET = 32;
    public static final int DATA_LEN_OFFSET = 28;
    //public static final int DATA_FLAG_OFFSET = 33;
    public static final int DATA_FLAG_OFFSET = 29;
    public static final int FRAME_SEQ_OFFSET = 13;  //整个帧,包括集中器
    public static final int METER_ADDR_OFFSET = 20;
    // {0x68,
    // 0x8D, 0x00, 0x8D, 0x00,
    // 0x68,
    // 0x88, 控制域
    // 0x02, 0x10, 0x48, 0x02, 0x00,  集中器地址
    // 0x10, 应用层功能码, 0x10表示转发
    // 0x61, seq
    // 00 00 01 00
    // 0x16, 转发长度
    // FE FE FE FE 68 05 08 18 00 59 00 68 91 06 36 38 33 37 33 F3 E3 16 EE 16
    private byte[] meterData = null;
    private int meterDataLen = 0;
    private byte[] meterDataFlag = new byte[4];
    private String hubAddr;
    private String meterAddr;
    private byte controlCode;
    private byte seq;

    public String getHubAddr() {
        return hubAddr;
    }

    public String getMeterAddr() {
        return meterAddr;
    }

    public byte getControlCode() {
        return controlCode;
    }

    public Map<String, Object> getWattHourStateResp(byte[] resp, Map<String, Object> map) {
        byte[] ret = getMeterData();
        //Map<String, Object> map = new HashMap<>();

        //低字节:0位:继电器状态,0通,1断
        if (getControlCode() == (byte) 0xD1) {
            map.put("retState", getMeterData());
        } else {
            String state = ret[0] == 1 ? "拉闸状态" : "开闸状态";
            map.put("state", state);
            map.put("retState", ret[1]);
        }
        map.put("result", ResultConst.SUCCESS);
        return map;
    }

    public Map<String, Object> getWattHourPriceResp(byte[] resp, Map<String, Object> map) {
        byte[] ret = getMeterData();
        //Map<String, Object> map = new HashMap<>();
        if (getControlCode() == (byte) 0xD1) {
            map.put("retState", getMeterData());
        } else {

            String priceStr = String.format("%02x", (byte) ret[3]);
            priceStr += String.format("%02x", (byte) ret[2]);
            priceStr += ".";
            priceStr += String.format("%02x", (byte) ret[1]);
            priceStr += String.format("%02x", (byte) ret[0]);

            float price = Float.valueOf(priceStr);
            map.put("price", price);
        }

        map.put("result", ResultConst.SUCCESS);
        return map;
    }

    public Map<String, Object> getWattHourGateResp(byte[] resp, Map<String, Object> map) {
        byte[] ret = getMeterData();
        int state = (getControlCode() == (byte) 0xDC) ? 0 : 1;
        if (getControlCode() == (byte) 0XDC) {
            map.put("retState", ret);
            map.put("state", "仪表返回失败");
        } else {
            map.put("retState", state);
            map.put("state", "执行成功");
        }
        map.put("result", ResultConst.SUCCESS);
        return map;
    }

    public Map<String, Object> getWattHourKwhResp(Map<String, Object> map) {
        byte[] ret = getMeterData();
        //Map<String, Object> map = new HashMap<>();
        if (getControlCode() == (byte) 0xD1) {
            map.put("retState", getMeterData());
        } else {
            String priceStr = String.format("%02x", (byte) ret[3]);
            priceStr += String.format("%02x", (byte) ret[2]);
            priceStr += String.format("%02x", (byte) ret[1]);
            priceStr += ".";
            priceStr += String.format("%02x", (byte) ret[0]);

            float kwh = Float.valueOf(priceStr);
            map.put("kwh", kwh);
        }
        map.put("result", ResultConst.SUCCESS);
        return map;
    }

    public Map<String, Object> getWattHourRechargeTimes(byte[] resp, Map<String, Object> map) {
        byte[] ret = getMeterData();
        //Map<String, Object> map = new HashMap<>();
        if (getControlCode() == (byte) 0xD1) {
            map.put("retState", getMeterData());
            map.put("state", "仪表返回失败");
        } else {
            int times = Integer.parseInt(String.valueOf(ret[1]))
                    + Integer.parseInt(String.valueOf(ret[0]));
            map.put("rechargeTimes", times);
            map.put("state", "执行成功");
        }
        map.put("result", ResultConst.SUCCESS);
        return map;
    }

    public Map<String, Object> getWattHourSocResp(Map<String, Object> map) {
        byte[] ret = getMeterData();
        //Map<String, Object> map = new HashMap<>();
        if (getControlCode() == (byte) 0xD1) {
            map.put("retState", getMeterData());
            map.put("state", "仪表返回失败");
        } else {
            String priceStr = String.format("%02x", (byte) ret[3]);
            priceStr += String.format("%02x", (byte) ret[2]);
            priceStr += String.format("%02x", (byte) ret[1]);
            priceStr += ".";
            priceStr += String.format("%02x", (byte) ret[0]);
            float kwh = Float.valueOf(priceStr);
            map.put("soc", kwh);
            map.put("state", "执行成功");
        }
        map.put("result", ResultConst.SUCCESS);
        return map;
    }

    public Map<String, Object> getReadMeterDateResp(Map<String, Object> map) {
        byte[] ret = getMeterData();
        //Map<String, Object> map = new HashMap<>();
        if (getControlCode() == (byte) 0xD1) {
            map.put("retState", getMeterData());
            map.put("state", "仪表返回失败");
        } else {

            map.put("state", "执行成功");
        }
        map.put("result", ResultConst.SUCCESS);
        return map;
    }

    public Map<String, Object> getReadMeterResp(Map<String, Object> map) {
        map.put("meterAddress", this.meterAddr);
        map.put("hubAddress", this.hubAddr);
        byte[] ret = getMeterData();
        //Map<String, Object> map = new HashMap<>();
        if (getControlCode() == (byte) 0xD1) {
            map.put("retState", getMeterData());
            map.put("state", "仪表返回失败");
        } else {
            //offset: 9, 4字节
            byte[] allUseAmmountBytes = Arrays.copyOfRange(getMeterData(), 9, 9 + 4);
            String allUseAmount = String.format("%02x", allUseAmmountBytes[3]);
            allUseAmount += String.format("%02x", allUseAmmountBytes[2]);
            allUseAmount += String.format("%02x", allUseAmmountBytes[1]);
            allUseAmount += ".";
            allUseAmount += String.format("%02x", allUseAmmountBytes[0]);
            map.put("allUseAmount", allUseAmount);

            //offset:13, 4字节
            byte[] leftAmountBytes = Arrays.copyOfRange(getMeterData(), 13, 13 + 4);
            String leftAmount = String.format("%02x", leftAmountBytes[3]);
            leftAmount += String.format("%02x", leftAmountBytes[2]);
            leftAmount += String.format("%02x", leftAmountBytes[1]);
            leftAmount += ".";
            leftAmount += String.format("%02x", leftAmountBytes[0]);
            map.put("leftAmount", leftAmount);

            //offset:21, 2字节
            byte[] rechargeTimesBytes = Arrays.copyOfRange(getMeterData(), 21, 21 + 2);
            String rechargeTimes = String.format("%02x", rechargeTimesBytes[1]);
            rechargeTimes += String.format("%02x", rechargeTimesBytes[0]);
            map.put("rechargeTimes", rechargeTimes);

            //offset:23, 5字节
            byte[] lastRechargeDateBytes = Arrays.copyOfRange(getMeterData(), 23, 23 + 5);
            String lastRechargeDate = "20" + String.format("%02x", lastRechargeDateBytes[4]);
            lastRechargeDate += "-";
            lastRechargeDate += String.format("%02x", lastRechargeDateBytes[3]);
            lastRechargeDate += "-";
            lastRechargeDate += String.format("%02x", lastRechargeDateBytes[2]);
            lastRechargeDate += " ";
            lastRechargeDate += String.format("%02x", lastRechargeDateBytes[1]);
            lastRechargeDate += ":";
            lastRechargeDate += String.format("%02x", lastRechargeDateBytes[0]);
            map.put("lastRechargeDate", lastRechargeDate);

            //offset:28, 4字节
            byte[] lastRechargeAmountBytes = Arrays.copyOfRange(getMeterData(), 28, 28 + 4);
            String lastRechargeAmount = String.format("%02x", lastRechargeAmountBytes[3]);
            lastRechargeAmount += String.format("%02x", lastRechargeAmountBytes[2]);
            lastRechargeAmount += String.format("%02x", lastRechargeAmountBytes[1]);
            lastRechargeAmount += ".";
            lastRechargeAmount += String.format("%02x", lastRechargeAmountBytes[0]);
            map.put("lastRechargeAmount", lastRechargeAmount);

            //offset:32, 4字节
            byte[] allRechargeAmountBytes = Arrays.copyOfRange(getMeterData(), 32, 32 + 4);
            String allRechargeAmount = String.format("%02x", allRechargeAmountBytes[3]);
            allRechargeAmount += String.format("%02x", allRechargeAmountBytes[2]);
            allRechargeAmount += String.format("%02x", allRechargeAmountBytes[1]);
            allRechargeAmount += ".";
            allRechargeAmount += String.format("%02x", allRechargeAmountBytes[0]);
            map.put("allRechargeAmount", allRechargeAmount);

            byte[] meterStatusBytes = Arrays.copyOfRange(getMeterData(), 59, 59 + 2);
            String meterGate = meterStatusBytes[0] == 1 ? "close" : "open";
            map.put("meterStatus", meterGate);

            map.put("state", "执行成功");
        }
        map.put("result", ResultConst.SUCCESS);
        return map;
    }

    public byte setSeq(byte[] resp) {
        seq = resp[FRAME_SEQ_OFFSET];
        return seq;
    }

    public byte getSeq() {
        return seq;
    }

    public void prepareRespData(byte[] resp) {
        this.controlCode = resp[DATA_LEN_OFFSET - 1];
        //需要根据不同的命令进行区分
        //关闸,合闸: 正常C=0x9C L=0; 异常C=0xDC, L=1;
        //读数据: 正常C=0x9C L包括数据标识; 异常C=0xD1, L=1;
        setMeterDataLen(resp);
        setMeterDataFlag(resp);
        setMeterData(resp);

        setSeq(resp);
        setMeterAddr(resp);
        setHubAddr(resp);
    }

    public String setMeterAddr(byte[] resp) {
        byte[] meterBytesAddr = new byte[6];
        System.arraycopy(resp, METER_ADDR_OFFSET, meterBytesAddr, 0, 6);
        meterAddr = CharacterUtil.bytesToHexStringlowHigh(meterBytesAddr);
        return meterAddr;
    }

    public String setHubAddr(byte[] resp) {
        byte[] address = Arrays.copyOfRange(resp, 7, 12);
        hubAddr = CharacterUtil.bytesToHexStringlowHigh(address);
        return hubAddr;
    }

    public byte[] setMeterDataFlag(byte[] resp) {
        Preconditions.checkArgument(resp != null, ">>> getMeterDataFlag, resp is null.");
        System.arraycopy(resp, DATA_FLAG_OFFSET, meterDataFlag, 0, 4);
        unwrapMeterData(meterDataFlag, 0, 4);
        return meterDataFlag;
    }

    public byte[] getMeterDataFlag() {
        return meterDataFlag;
    }

    public int setMeterDataLen(byte[] resp) {
        meterDataLen = resp[DATA_LEN_OFFSET];
        return meterDataLen;
    }

    public int getMeterDataLen() {
        return meterDataLen;
    }

    public byte[] getMeterData() {
        return meterData;
    }

    public byte[] setMeterData(byte[] resp) {
        int dataLen = getMeterDataLen();
        if (dataLen <= 0) return null;
        if (dataLen > DATA_FLAG_LEN) {
            meterData = new byte[dataLen - DATA_FLAG_LEN];
            System.arraycopy(resp, DATA_LEN_OFFSET + 5, meterData, 0, dataLen - DATA_FLAG_LEN);
            unwrapMeterData(meterData, 0, meterData.length);
        } else {
            meterData = new byte[dataLen];
            System.arraycopy(resp, DATA_LEN_OFFSET + 1, meterData, 0, dataLen);
            unwrapMeterData(meterData, 0, meterData.length);
        }

        return meterData;
    }

    public void unwrapMeterData(byte[] data, int iStart, int iLen) {
        //数据域-0x33h
        for (int i = iStart; i < iStart + iLen; i++) {
            data[i] -= 0x33;
        }
    }

    public static boolean checkBytesValid(byte[] resp) {
        if (resp.length > 12 && resp[12] != 0x10) {
            log.info(">>> DecodeWattHourProtocol->checkBytesValid, afn is invalid: " + resp[12]);
            return false;
        }

        byte ret = ByteUtil.calc_sum_crc(resp, 23, resp.length - 5);
        if (ret != resp[resp.length - 4]) {
            log.info(">>> DecodeWattHourProtocol->checkBytesValid, meterDate crc check fail, ret: " + ret);
            return false;
        }


        //校验长度
        /*if (resp.length > 19 && (resp.length != (resp[18] & 0xff) + 21)) {
            System.out.println(">>> DecodeWattHourProtocol->checkBytesValid, resp.lenght is invalid: " + resp.length);
            return false;
        }*/
        //校验crc

        return true;
    }

    public static boolean highPowerMeterCheck(byte[] meter, int iPos) {

        byte[] meterAddr = new byte[6];
        System.arraycopy(meter, iPos, meterAddr, 0, 6);
        String meterStr = CharacterUtil.bytesToHexStringlowHigh(meterAddr);
        System.out.println(">>> highPowerMeterCheck, meterStr: " + meterStr);

        int find = PluginContainer.getTable()
                .cellSet()
                .stream()
                .filter(v -> {
                    return v.getColumnKey().equals(meterStr);
                })
                .collect(Collectors.toList()).size();
        //return (find == 1) ? true : false;
        return true;
    }
}
