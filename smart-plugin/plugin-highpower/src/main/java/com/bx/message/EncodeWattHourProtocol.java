package com.bx.message;


import com.bx.utils.ByteUtil;
import com.bx.utils.CharacterUtil;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;


/**
 *
 */
public class EncodeWattHourProtocol extends com.bx.message.BaseProtocol {

    public final static byte[] high_power_gate_on  = {0x10, 0x03, 0x51, 0x1B};
    public final static byte[] high_power_gate_off = {0x10, 0x03, 0x51, 0x1A};

    public final static byte[] write_high_power_passwd   = {0x00, 0x00, 0x00, 0x02};
    public final static byte[] write_high_power_opercode = {(byte)0xC4, (byte)0xC3, (byte)0xC2, (byte)0xC1};

    public final static int meterDataPreLen = 4;
    public final static int CTL_CODE_OFFSET = (8 + 4);      //4标识4个FE FE FE FE

    public final static int DATA_TO_68_OFFSET      = 4;     //4标识4个FE FE FE FE
    public final static float  OPEN_ACCOUNT_AMOUNT = 0.01f; //开户时候, 默认的充值量
    public final static float  OPEN_AMOUNT_BYTES   = 0.01f; //开户时候, 默认的充值量
    public final static byte   PASSWD_PA           = 0x02;
    public final static byte[] PASSWD              = {0x00, 0x00, 0x00};
    //public final static byte[] SYSTEM_PASSWD       = {0x00, 0x00, 0x00};
    public final static byte[] SYSTEM_PASSWD       = {0x37, 0x05, 0x53};
    public final static byte[] AREA_CODE           = {0x01, 0x13, 0x23};    //地区码:0x23, 0x13, 0x01
    public final static byte[] RAND_IMMUTABLE      = {0x0D, (byte)0xC7, 0x22};    //随机数,开户用
    public final static byte[] USER_NUMBER         = {0x01, 0x00, 0x00, 0x00, 0x00, 0x00};

    private byte[] frameData = null;
    private byte[] meterDataBase = {
            (byte)0xFE, (byte)0xFE, (byte)0xFE,(byte)0xFE,
            (byte)0x68, /* 帧起始符 */
            0x00,0x00,0x00,0x00,0x00,0x00, /* 地址域 BCD码*/
            (byte)0x68, /* 帧起始符 */
            (byte)0x11,  /* 控制码 */
            (byte)0x04,  /* 数据长度 */
            //(byte)0x04, (byte)0x00, (byte)0x05, (byte)0x03, /* 数据标识 */
            (byte)0x36, (byte)0x38, (byte)0x33, (byte)0x37,   /* 数据标识 */
            0x3B,       /*校验码*/
            0x16,       /*end*/
            0x00,
            0x16
    };
    private byte[] meterData = null;

    private String meterAddr;
    private int dataDomainLen;

    public EncodeWattHourProtocol(String hepAddr, String meterAddr) {
        super(hepAddr);
        this.meterAddr = meterAddr;
    }

    public void setMeterAddr() {
        Long meterLong = Long.valueOf(this.meterAddr);
        String meterAddrStr = String.format("%012d", meterLong);
        byte[] meterAddrByte = CharacterUtil.hexString2ByteslowHigh(meterAddrStr);
        System.arraycopy(meterAddrByte, 0, meterData, 5, 6);
    }


    private void mergeFrameData() {
        frameData = new byte[hubData.length + meterData.length + 2];
        System.arraycopy(hubData, 0, frameData, 0, hubData.length);
        System.arraycopy(meterData, 0, frameData, hubData.length, meterData.length);
        frameData[frameData.length - 1] = (byte)0x16;

    }
    private byte[] setMeterDateCrc() {
        byte sum_crc = 0;
		for (int i = 4; i < meterData.length-2; i++) {
			//sum_crc += (meterData[i] % 256);
            sum_crc += (byte)meterData[i];
		}
        //sum_crc = DataHandle.clac_sum_crc(meterData, 4, meterData.length -2 );
        meterData[meterData.length - 2] = sum_crc;
        return null;
    }
    private byte[] setFrameDataCrc() {
        //从第7个字节到倒数第3个字节作为整包crc校验数据
        frameData[frameData.length - 2] = ByteUtil.calc_sum_crc(frameData,6,frameData.length-3);
        return null;
    }

    //协议: DI3 DI2 DI1 DI0
    //flag: DI0 DI1 DI2 DI3
    public void setMeterDataFlag(byte[] flag) {
        meterData[14] = flag[0];
        meterData[15] = flag[1];
        meterData[16] = flag[2];
        meterData[17] = flag[3];
    }
    private void setFrameDataLen() {
        int iLen = frameData.length;
        int iFrameLength = (iLen - 8) << 2;
        frameData[1] = (byte)((iFrameLength & 0xFF) + 1 );
        frameData[2] = (byte)((iFrameLength & 0xFF00) >> 8);
        frameData[3] = (byte)((iFrameLength & 0xFF) + 1 );
        frameData[4] = (byte)((iFrameLength & 0xFF00) >> 8);

        /*int iFrameLength = (iLen - 8) * 4;
        frameData[1] = (byte) (iFrameLength + 1);
        frameData[2] = (byte) (iFrameLength / 256);
        frameData[3] = (byte) (iFrameLength + 1);
        frameData[4] = (byte) (iFrameLength / 256);*/
        /*iFrameLength = (iLen - 8) * 4;
        frameData[1] = (byte) (iFrameLength + 1);
        frameData[2] = (byte) (iFrameLength / 256);
        frameData[3] = (byte) (iFrameLength + 1);
        frameData[4] = (byte) (iFrameLength / 256);*/
    }
    private void setTransimtDataLen() {
        frameData[22] = (byte)(frameData.length - 25);
    }

    private void setMeterDataDomain33H() {
        for (int i=0; i<dataDomainLen; i++) {
            meterData[14 + i] += 0x33;
        }
    }

    private byte[] allocMeterDomain() {
        //16表示: 从meterDataBase数据中除去"数据域(数据标识)和最后两位"的长度,
        //包括表自己的crc和0x16,不包括集中器的crc和0x16
        meterData = new byte[dataDomainLen + 16];
        System.arraycopy(meterDataBase, 0, meterData, 0, 12);
        meterData[meterData.length - 1]  = (byte)0x16;

        //meterData
        meterData[13] = (byte)dataDomainLen;
        return meterData;
    }

    private int setDataDomainLen(int iLen) {
        dataDomainLen = iLen;
        return iLen;
    }
    private int getDataDomainLen() {
        return dataDomainLen;
    }


    public void setPasswd(byte[] passwd) {
        System.arraycopy(passwd, 0, meterData, 14, 4);
    }

    public void setOperationCode(byte[] operationCode) {
        System.arraycopy(operationCode, 0, meterData, 18, 4);

    }

    //为了编码方便,第2次购电的数据,都和第1次开户的一样,就是购电次数进行更改为2,以后购电的编码方式都和开户类似
    public byte[] wattHourOpenAccount(float rechargeAmount,
                                      int rechargeTimes) {
        //68H A0…A5 68H 1FH 3BH PA P0 P1 P2 C0…C3 N1…N5 M1-M46(命令数据区) CS 16H
        final int datalen = 0x3B;
        setDataDomainLen(datalen);
        allocMeterDomain();

        meterData[CTL_CODE_OFFSET] = 0x1F;
        meterData[CTL_CODE_OFFSET + 2] = PASSWD_PA;
        System.arraycopy(PASSWD, 0, meterData, CTL_CODE_OFFSET + 3 , 3);

        //C0-C3
        byte[] randBytes = com.bx.message.MeterUtils.getRandOfBytes(4);
        System.arraycopy(randBytes, 4, meterData, CTL_CODE_OFFSET + 6,4);

        //N1
        byte [] amountBytes = com.bx.message.MeterUtils.float2BCDBytes(rechargeAmount, 4);
        byte N1 = com.bx.message.MeterUtils.amountCheckCode(amountBytes, 0, 4);
        meterData[CTL_CODE_OFFSET + 10] = N1;

        //N2-N5               4字节
        // (注意参与计算的数据都为原码,不是通讯加0X33后的数据)
        //N2=(C0异或 OXA7)+0X3D+N1;
        byte N2 =  (byte) ((randBytes[4] ^ 0xA7) + 0x3D + N1);
        meterData[CTL_CODE_OFFSET + 11] = N2;
        //N3=(取反((C1+OX3C) 异或0X4A)) +N1;
        byte N3 = (byte) (~((randBytes[5] + 0x3C) ^ 0x4A) + N1);
        meterData[CTL_CODE_OFFSET + 12] = N3;
        //N4=(取反((C2异或 OX75)+0X6F)) +N1;
        byte N4 = (byte) (~((randBytes[6] ^ 0x75) + 0x6F) + N1);
        meterData[CTL_CODE_OFFSET + 13] = N4;
        //N5=(取反(C3+N1+N2+N3+N4)) +N1;
        byte N5 = (byte) (~(randBytes[7] + N1 + N2 + N3 + N4) + N1);
        meterData[CTL_CODE_OFFSET + 14] = N5;

        //M1-M3 地区码         3字节  相对初始化68偏移23
        System.arraycopy(AREA_CODE, 0, meterData, DATA_TO_68_OFFSET + 23, 3);

        //M4-M9 用户编号       6字节  相对初始化68偏移26
        System.arraycopy(USER_NUMBER, 0, meterData, DATA_TO_68_OFFSET + 26, 6);

        //M10-M12 系统密码     3字节  相对初始化68偏移32
        System.arraycopy(SYSTEM_PASSWD, 0, meterData, DATA_TO_68_OFFSET + 32, 3);

        //M13-M15 随机参数:固定 3字节  相对初始化68偏移35
        System.arraycopy(RAND_IMMUTABLE, 0, meterData, DATA_TO_68_OFFSET + 35, 3);

        //M16-M20 购电日期      5字节  相对初始化68偏移38
        byte rechargeDate[] = com.bx.message.MeterUtils.getDateTimeOfBytes(new SimpleDateFormat("--yyMMddHHmm--"),5);
        System.arraycopy(rechargeDate, 5, meterData, DATA_TO_68_OFFSET + 38, 5);

        //M21-M22 购电次数      2字节  相对初始化68偏移43
        //开户购电次数为1
        byte times[] = com.bx.message.MeterUtils.getIntOfBytes(rechargeTimes, 2);
        System.arraycopy(times, 2,  meterData, DATA_TO_68_OFFSET + 43, 2);

        //M23-M26 购电电量      4字节  相对初始化68偏移45
        byte amountEncode[] = com.bx.message.MeterUtils.highPowerRechargeAmountEncode(Arrays.copyOfRange(amountBytes, 4,8),
                Arrays.copyOfRange(rechargeDate, 5,9));
        System.arraycopy(amountEncode, 0, meterData, DATA_TO_68_OFFSET + 45, 4);

        //M27-M30 声光报警电量   4字节 相对初始化68偏移49
        System.arraycopy(new byte[] {0x00,0x00,0x00,0x00}, 0,  meterData, DATA_TO_68_OFFSET + 49, 4);

        //M31-M34 拉闸报警电量   4字节
        System.arraycopy(new byte[] {0x00,0x00,0x00,0x00}, 0,  meterData, DATA_TO_68_OFFSET + 53, 4);

        //M35-M38 囤积限量：     4字节
        System.arraycopy(new byte[] {0x00,0x00,0x00,0x00}, 0,  meterData, DATA_TO_68_OFFSET + 57, 4);

        //M39-M42 赊欠限量：     4字节
        System.arraycopy(new byte[] {0x00,0x00,0x00,0x00}, 0,  meterData, DATA_TO_68_OFFSET + 61, 4);

        //M43-M45 限容功率：     3字节
        System.arraycopy(new byte[] {0x00,0x00,0x00}, 0,  meterData, DATA_TO_68_OFFSET + 65, 3);

        //M46     限容方式：     1字节
        System.arraycopy(new byte[] {0x00}, 0,  meterData, DATA_TO_68_OFFSET + 68, 1);


        //test
        String tmp = "FEFEFEFE68 56 34 12 00 00 00 68 1F 3B 35 33 33 33 AB 89 67 45 CF EB F6 1E BA 34 46 56 34 33 33 33 33 33 6A 38 86 40 FA 55 43 3C 49 44 4A 34 33 0A E9 79 A2 33 63 33 33 33 43 33 33 33 CC CC 3C 33 33 33 33 33 B3 3B 33 80 16";
        tmp = tmp.replaceAll(" ","");
        byte[] mteerData = CharacterUtil.hexString2Bytes(tmp);
        //test-end

        setMeterAddr();
        setMeterDataDomain33H();
        setMeterDateCrc();

        setHepAddr();
        mergeFrameData();

        setTransimtDataLen();
        setFrameDataLen();
        setFrameDataCrc();

        return frameData;
    }

    public static void main(String[] args) {
        String tmp = "68 56 34 12 00 00 00 68 1F 3B 35 33 33 33 AB 89 67 45 CF EB F6 1E BA 34 46 56 34 33 33 33 33 33 6A 38 86 40 FA 55 43 3C 49 44 4A 34 33 0A E9 79 A2 33 63 33 33 33 43 33 33 33 CC CC 3C 33 33 33 33 33 B3 3B 33 80 16";
        tmp = tmp.replaceAll(" ","");
        byte[] ret = CharacterUtil.hexString2Bytes(tmp);


    }


    public byte[] meterGateClose() {
        final byte N1      = 0x1A;

        meterCommonGate(N1);
        return frameData;
    }
    public byte[] meterGateOpen() {
        final byte N1      = 0x1B;

        meterCommonGate(N1);
        return frameData;
    }
    public byte[] meterCloseAccount() {
        final byte N1      = 0x4A;
        meterCommonGate(N1);
        return frameData;
    }
    public void meterCommonGate(byte N1) {
        //远程拉闸，合闸，后付费模式,恢复费控预付费模式，销户清电表
        //68H	A0	…	A5	68H	1CH	16H	PA	P0	P1	P2	C0	…	C3	N1	…	N14	CS	16H
        final int  datalen = 0x16;
        setDataDomainLen(datalen);
        allocMeterDomain();


        meterData[CTL_CODE_OFFSET] = 0x1C;

        //PA, P0-P2
        meterData[CTL_CODE_OFFSET + 2] = PASSWD_PA;
        System.arraycopy(PASSWD, 0, meterData, CTL_CODE_OFFSET + 3 , 3);

        //C0-C3
        byte[] randBytes = com.bx.message.MeterUtils.getRandOfBytes(4);
        System.arraycopy(randBytes, 4, meterData, CTL_CODE_OFFSET + 6,4);

        //N1
        meterData[CTL_CODE_OFFSET + 10] = N1;

        //N2-N5               4字节
        // (注意参与计算的数据都为原码,不是通讯加0X33后的数据)
        //N2=(C0异或 OXA7)+0X3D+N1;
        byte N2 =  (byte) ((randBytes[4] ^ 0xA7) + 0x3D + N1);
        meterData[CTL_CODE_OFFSET + 11] = N2;
        //N3=(取反((C1+OX3C) 异或0X4A)) +N1;
        byte N3 = (byte) (~((randBytes[5] + 0x3C) ^ 0x4A) + N1);
        meterData[CTL_CODE_OFFSET + 12] = N3;
        //N4=(取反((C2异或 OX75)+0X6F)) +N1;
        byte N4 = (byte) (~((randBytes[6] ^ 0x75) + 0x6F) + N1);
        meterData[CTL_CODE_OFFSET + 13] = N4;
        //N5=(取反(C3+N1+N2+N3+N4)) +N1;
        byte N5 = (byte) (~(randBytes[7] + N1 + N2 + N3 + N4) + N1);
        meterData[CTL_CODE_OFFSET + 14] = N5;

        //N6-N11 用户编号       6字节
        System.arraycopy(USER_NUMBER, 0, meterData, CTL_CODE_OFFSET + 15, 6);

        //N12-N14 系统密码     3字节
        System.arraycopy(SYSTEM_PASSWD, 0, meterData, CTL_CODE_OFFSET + 21, 3);

        setMeterAddr();

        setMeterDataDomain33H();
        setMeterDateCrc();

        setHepAddr();
        mergeFrameData();

        setTransimtDataLen();
        setFrameDataLen();
        setFrameDataCrc();
    }


    public byte[] setHighGateState(int statue) {
        //帧格式:68H	A0-A5 68H 1CH L	PA P0 P1 P2	C0-C3 N1-Nm	CS 16
        //注1: N1为控制命令类型, N1=1AH代表跳闸, N1=1BH代表合闸允许, N1=2AH代表报警, N1=2BH代表报警解除,
        //     N1=3AH代表保电, N1=3BH代表保电解除. N2保留.N3～N8代表命令有效截止时间, 数据格式为ssmmhhDDMMYY
        //注2: 本命令无须硬件配合.
        //注4: 不带安全认证密级为02, N1～Nm为明文, 带安全认证密级为98, N1～Nm为密文.
        final int dataLen = 15;
        setDataDomainLen(dataLen);
        allocMeterDomain();
        setPasswd(write_high_power_passwd);    //
        setOperationCode(write_high_power_opercode);
        meterData[12] = (byte)0x1C;
        //设置时间
        SimpleDateFormat df = new SimpleDateFormat("ssmmHHddMMyy");// 设置日期格式
        String retStr = df.format(new Date());
        meterData[22] = statue == 1 ? (byte)0x1B : (byte)0x1A;
        byte[] dateByte = CharacterUtil.hexString2Bytes(retStr);
        //byte[] dateByte = {0x51, 0x03, 0x10, 0x26, 0x09, 0x18};
        System.arraycopy(dateByte, 0, meterData, 23, 6);
        setMeterDataDomain33H();
        setMeterAddr();
        setMeterDateCrc();

        setHepAddr();
        mergeFrameData();

        setTransimtDataLen();
        setFrameDataLen();
        setFrameDataCrc();

        return frameData;
    }

    public byte[] readMeter() {
        setDataDomainLen(4);
        allocMeterDomain();
        meterData[12] = (byte)0x11; //控制码

        //数据标识
        byte[] flag = {(byte)0xFF, (byte)0xFF, 0x32, 0x03};
        setMeterDataFlag(flag);
        setCommonData();
        return frameData;
    }


    /**
     * @return 充值次数
     */
    public byte[] queryRechargeTimes() {
        setDataDomainLen(4);
        allocMeterDomain();
        meterData[12] = (byte)0x11; //控制码

        //数据标识03 32 01 02
        byte[] flag = {0x02, 0x01, 0x32, 0x03};
        setMeterDataFlag(flag);

        setCommonData();
        return frameData;
    }

    private void setCommonData() {
        setMeterAddr();
        setMeterDataDomain33H();
        setMeterDateCrc();

        setHepAddr();
        mergeFrameData();
        setTransimtDataLen();
        setFrameDataLen();
        setFrameDataCrc();
    }

    public byte[] queryWattHourState() {
        setDataDomainLen(4);
        allocMeterDomain();
        meterData[12] = (byte)0x11;

        //数据标识
        //byte[] flag = {0x03, 0x05, 0x00, 0x04};
        byte[] flag = {0x01, 0x05, 0x00, 0x04};
        setMeterDataFlag(flag);
        setMeterAddr();
        setMeterDataDomain33H();
        setMeterDateCrc();

        setHepAddr();
        mergeFrameData();
        setTransimtDataLen();
        setFrameDataLen();
        setFrameDataCrc();
        return frameData;
    }

    public byte[] queryWattHourPrice() {
        setDataDomainLen(4);
        allocMeterDomain();
        meterData[12] = (byte)0x11;

        //数据标识
        byte[] flag = {0x01, 0x01, 0x05, 0x04};
        setMeterDataFlag(flag);
        setMeterAddr();
        setMeterDataDomain33H();
        setMeterDateCrc();

        setHepAddr();
        mergeFrameData();
        setTransimtDataLen();
        setFrameDataLen();
        setFrameDataCrc();
        return frameData;
    }

    public byte[] queryWattKwh() {
        setDataDomainLen(4);
        allocMeterDomain();
        meterData[12] = (byte)0x11;

        //数据标识
        byte[] flag = {0x00, 0x00, 0x00, 0x00};
        setMeterDataFlag(flag);
        setMeterAddr();
        setMeterDataDomain33H();
        setMeterDateCrc();

        setHepAddr();
        mergeFrameData();
        setTransimtDataLen();
        setFrameDataLen();
        setFrameDataCrc();
        return frameData;
    }



    public byte[] queryWattSoc() {
        setDataDomainLen(4);
        allocMeterDomain();
        meterData[12] = (byte)0x11;

        //数据标识
        byte[] flag = {(byte)0x00, (byte)0x01, (byte)0x90, (byte)0x00};
        setMeterDataFlag(flag);

        setCommonData();
        return frameData;
    }

    public byte[] meterTimeSync() {
        setDataDomainLen(6);
        allocMeterDomain();

        //控制码
        meterData[12] = (byte)0x08;

        //设置时间
        byte[] dateTime = com.bx.message.MeterUtils.getDateTimeOfBytes(new SimpleDateFormat("--yyMMddHHmmss"), 6);
        System.arraycopy(dateTime, 6, meterData ,CTL_CODE_OFFSET + 2, 6);
        setCommonData();
        return frameData;

    }

    public byte[] queryDate() {

        setDataDomainLen(4);
        allocMeterDomain();
        meterData[12] = (byte)0x11;

        //数据标识
        byte[] flag = {(byte)0x01, (byte)0x01, (byte)0x00, (byte)0x04};
        setMeterDataFlag(flag);

        setCommonData();
        return frameData;
    }
}
