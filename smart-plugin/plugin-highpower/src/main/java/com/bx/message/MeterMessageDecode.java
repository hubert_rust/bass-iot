package com.bx.message;

import java.util.Map;

public interface MeterMessageDecode {
    byte[] getMessageBytes(Long opType, String hubAddress, String meterAddress, Map<String, Object> map);


}
