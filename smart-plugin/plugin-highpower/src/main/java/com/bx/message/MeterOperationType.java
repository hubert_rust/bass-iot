package com.bx.message;

public class MeterOperationType {
    //电表
    public static final Long ELEC_METER_TIME_SYNC      = 1000L;
    public static final Long ELEC_LEFT_BALANCE_CLEAR   = 1001L;
    public static final Long ELEC_QUERY_LEFT_BALANCE   = 1001L;

    //冷水表
    public static final Long COLD_METER_TIME_SYNC      = 2000L;
    public static final Long COLD_LEFT_AMOUNT_CLEAR    = 2001L;
    public static final Long COLD_QUERY_LEFT_AMOUNT    = 2002L;

    //热水表
    public static final Long HOT_METER_TIME_SYNC       = 3000L;
    public static final Long HOT_METER_DEL_USER        = 3001L;
    public static final Long HOT_METER_ADD_USER        = 3002L;


    //大功率电表
    public static final Long HIGH_POWER_OPEN_ACCOUNT   = 5000L;
    public static final Long HIGH_POWER_STATE          = 5001L;
    public static final Long HIGH_POWER_LEFT_AMOUNT    = 5002L;
    public static final Long HIGH_POWER_BALANCE_CLEAR  = 5003L;
    public static final Long HIGH_POWER_SET_PRICE      = 5004L;
    public static final Long HIGH_POWER_RECHARGE_TIMES = 5005L;
    public static final Long HIGH_POWER_RECHARGE       = 5006L;
    public static final Long HIGH_POWER_REMOTE_ON      = 5007L;
    public static final Long HIGH_POWER_REMOTE_OFF     = 5008L;
    public static final Long HIGH_POWER_TIME_SYNC      = 5009L;
    public static final Long HIGH_POWER_READ_METER     = 5010L;
    public static final Long HIGH_POWER_CLOSE_ACCOUNT  = 5011L;
    public static final Long HIGH_POWER_QUERY_TIME     = 5012L;
    public static final Long HIGH_POWER_QUERY_DATE     = 5013L;
    public static final Long HIGH_POWER_GET_PRICE      = 5004L;
}
