package com.bx.message;


import com.bx.utils.CharacterUtil;

public class BaseProtocol {

    private String hepAddr;

    protected byte[] hubData = {
            0x68,
            (byte) 0x95, 0x00,
            (byte) 0x95, 0x00,
            0x68,
            0x4B,        /* 控制域:发送-0x4B,接收-0x88 */
            0x49, 0, 1, 0, 2, /* 集中器物理地址 */
            0x10,        /* afn:0x10-转发 */
            0x60,        /* frame seq */
            0, 0,        /* DA */
            1, 0,        /* DT:1,0-f1~p0,表示转发 */
            2,           /* 转发通道：01-mbus,02-485 */
            (byte) 0x0B, /*转发通信控制*/
            (byte) 0xFF, /* 帧响应超时时间 */
            0x32,        /* 字节超时时间 */
            0x14,        /* transmit data len */
            };

    public BaseProtocol(String hepAddr) {
        this.hepAddr = hepAddr;
    }

    public void setTransmitChannel(int val) {
        hubData[18] = (byte)val;
    }

    public void setFrameSeq(Byte seq) {
        hubData[13] = seq;
    }

    public void setTransimtDataLen(byte len) {
        hubData[22] = len;
    }

    public String getHepAddr() {
        return hepAddr;
    }
    public void setBps(int bps) {
        byte curByte=(byte) (hubData[19] & 0x1F);
		hubData[19]=(byte) ((bps << 5) | curByte);
    }

    public void setHepAddr() {
        this.hepAddr = hepAddr;
        byte[] ret = CharacterUtil.getHubBinAddrFromString(hepAddr);
        System.arraycopy(ret, 0, hubData, 7, 5);
    }
}
