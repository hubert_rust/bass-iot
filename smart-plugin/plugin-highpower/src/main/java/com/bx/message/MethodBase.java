package com.bx.message;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public abstract  class MethodBase {
    protected static Map<String, String> opCodeToFun = new HashMap();
    protected static byte[] getMeterByte(IMeterMessageByte meterByteInterface,
                                         String operType,
                                         String hubAddress,
                                         String meterAddress,
                                         Map<String, Object> map) {
        Object getBytes = null;
        String methodName = opCodeToFun.get(operType);
        try {
            Method method = meterByteInterface.getClass().getMethod(methodName, new Class[] {String.class, String.class, Map.class});
            try {
                getBytes = method.invoke(null, new Object[] {hubAddress, meterAddress, map});
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        return (byte[])getBytes;
    }
}
