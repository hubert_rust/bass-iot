package com.bx.message;

import com.google.common.base.Preconditions;

import java.nio.ByteBuffer;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 */
public class MeterUtils {
    /**
     * @param
     * @param byteNums: 设置返回byte的长度
     * @param
     * @return
     */
    public static byte[] float2BCDBytes(float f, int byteNums) {
        String intStr=String.format("%06d", (int) f);
        String deciStr=String.format("%02d", (int) (f * 100) - ((int) f) * 100);
        String numStr=intStr + deciStr;

        byte[] ret=new byte[byteNums * 2];
        int len=numStr.length();
        for (int i=0; i < len; i+=2) {
            //int index = dir ? i/2 : (len/2 - 1 - i/2);
            int index=i / 2;
            byte tmp=(byte) Integer.parseInt(numStr.substring(i, i + 2), 16);
            ret[index]=tmp;
            ret[len - index - 1]=tmp;
        }
        return ret;
    }

    public static byte[] getRandOfBytes(int nums) {
        int len=nums * 2;
        byte[] ret=new byte[nums * 2];
        for (int i=0; i < nums; i++) {
            ret[i]=(byte) new Random().nextInt(99);
            ret[len - i - 1]=ret[i];
        }

        //test
        /*ret[0]=0x12;
        ret[1]=0x34;
        ret[2]=0x56;
        ret[3]=0x78;

        ret[7]=0x12;
        ret[6]=0x34;
        ret[5]=0x56;
        ret[4]=0x78;*/
        //test-end

        return ret;
    }

    //完整格式:yyMMddHHmmss,如果那些不需要用'--'忽略(只能整体忽略) --yyMMddHHmmss, --yyMMddHHmm--
    public static byte[] getDateTimeOfBytes(SimpleDateFormat df, int byteNums) {
        String dateStr=df.format(new Date());
        dateStr=dateStr.replaceAll("-", "");

        //test
        //dateStr = "1711160910";

        int len=dateStr.length();
        Preconditions.checkArgument(len / 2 == byteNums, "byteNums len is invalid");

        byte[] ret=new byte[byteNums * 2];
        for (int i=0; i < len; i+=2) {
            //int index = dir ? i/2 : (len/2 - 1 - i/2);
            int index=i / 2;
            byte tmp=(byte) Integer.parseInt(dateStr.substring(i, i + 2), 16);
            ret[index]=tmp;
            ret[len - index - 1]=tmp;
        }
        return ret;
    }

    //返回的字节数*2 必须>= 整数长度
    public static byte[] getIntOfBytes(int val, int byteNums) {
        int valLen=String.valueOf(val).length();
        Preconditions.checkArgument(valLen <= byteNums * 2, "ValLen > byteNums * 2, invalid");

        String intStr=String.format("%0" + byteNums * 2 + "d", (int) val);
        int len=intStr.length();
        byte[] ret=new byte[byteNums * 2];
        for (int i=0; i < len; i+=2) {
            //int index = dir ? i/2 : (len/2 - 1 - i/2);
            int index=i / 2;
            byte tmp=(byte) Integer.parseInt(intStr.substring(i, i + 2), 16);
            ret[index]=tmp;
            ret[len - index - 1]=tmp;
        }

        return ret;
    }

    public static byte amountCheckCode(byte[] amount, int start, int end) {
        Preconditions.checkArgument(end <= amount.length, "end > amount.length");
        byte ret=0x0;
        int tmp=0;
        for (int i=start; i < end; i++) {
            tmp+=(amount[i] & 0xFF);
        }
        return (byte) (tmp & 0xFF);
    }

    public static byte[] highPowerRechargeAmountEncode(byte[] amount, byte date[]) {
        byte[] constByte=new byte[]{(byte) 0x91, (byte) 0x8B, (byte) 0x42, (byte) 0x7E};

        byte[] ret=new byte[4];
        for (int i=0; i < 4; i++) {
            ret[i]=(byte) (date[i] ^ constByte[i]);
            ret[i]=(byte) (ret[i] ^ amount[i]);
        }
        return ret;
    }
/*

    public static Object getHighRechargeTimes(String hubAddress,
                                              String meterAddress,
                                              String meterType,
                                              Long operType,
                                              Map<String, Object> mapPamam) throws ValidataException {
        Object ret=null;
        CommonHubContext hubContext=CommonHubCore.getHubContext(hubAddress);
        ret=hubContext.sendMessage(meterType, operType, hubAddress, meterAddress, mapPamam);
        return ret;
    }

    public static Object getHighLeftAmount(String hubAddress,
                                           String meterAddress,
                                           String meterType,
                                           Long operType,
                                           Map<String, Object> mapPamam) throws ValidataException {
        Object ret=null;
        CommonHubContext hubContext=CommonHubCore.getHubContext(hubAddress);
        ret=hubContext.sendMessage(meterType, operType, hubAddress, meterAddress, mapPamam);
        return ret;
    }

    public static Object highRecharge(String hubAddress,
                                      String meterAddress,
                                      String meterType,
                                      Long operType,
                                      Map<String, Object> mapPamam) throws ValidataException {
        String status = HepServerCfg.getMeterInfo(meterAddress, "meter_status");
        if (status.equals("销户")) {
            Map<String, Object> retMsg = new HashMap<>();
            retMsg.put("resultMessage", "该表已经销户");
            retMsg.put("state", "FAILURE");
            retMsg.put("resultCode", "FAILURE");

            List<Object> retList = new ArrayList<>();
            retList.add(retMsg);
            Map<String, Object> retMap = new HashMap<>();
            retMap.put("data", retList);

           return  retMap;
        }

        mapPamam.put("hub_address", hubAddress);
        mapPamam.put("meter_address", meterAddress);
        mapPamam.put("meter_type", meterType);
        mapPamam.put("oper_type", operType);
        HighRechargeImpl highRecharge=SpringContextUtils.getBean("HighRechargeImpl");
        return highRecharge.doRecharge(mapPamam);
    }

    public static Object setHighPrice(String meterNo, String hubAddress, String meterAddress, String price) {
        PriceHighMeter priceHighMeter = SpringContextUtils.getBean("PriceHighMeter");
        int ret = priceHighMeter.setPrice(meterNo, hubAddress, meterAddress, price);

        Map<String, Object> retMsg = new HashMap<>();
        retMsg.put("state", "SUCESS");
        retMsg.put("resultMessage","成功设置电价");
        retMsg.put("resultCode", ret);
        return retMsg;
    }
    public static Object getHighPrice(String meterNo, String hubAddress, String meterAddress) {
        PriceHighMeter priceHighMeter = SpringContextUtils.getBean("PriceHighMeter");
        String price = priceHighMeter.getPrice(meterNo, hubAddress, meterAddress);

        List<Object> retList = new ArrayList<>();
        Map<String, Object> retMsg = new HashMap<>();
        retMsg.put("price", price);
        retMsg.put("hub_address", hubAddress);
        retMsg.put("meter_address", meterAddress);
        retList.add(retMsg);
        Map<String, Object> retMap = new HashMap<>();
        retMap.put("data", retList);
        return retMap;
    }
    public static Object highCancelAccount(String hubAddress,
                                      String meterAddress,
                                      String meterType,
                                      Long operType,
                                      Map<String, Object> mapPamam) throws ValidataException {
        CommonHubContext hubContext=CommonHubCore.getHubContext(hubAddress);
        //销户前抄表一次
        Object ret = hubContext.sendMessage(meterType, MeterOperationType.HIGH_POWER_READ_METER,
                hubAddress, meterAddress, mapPamam);

        Map<String, Object> retMsg = new HashMap<>();
        List<Object>  lstTmp = (List<Object>)((Map<String, Object>)ret).get("data");
        Map<String, Object> mapTmp = (Map<String, Object>)lstTmp.get(0);
        retMsg.putAll(mapTmp);

        //销户前在充值记录表保存
        Map<String, Object> saveMap = new HashMap<>();
        float leftAmount = Float.valueOf(StrUtil.getStr(mapTmp.get("left_amount")));
        float price = Float.valueOf(StrUtil.getStr(mapPamam.get("price")));

        saveMap.put("hub_address", hubAddress);
        saveMap.put("meter_address", meterAddress);
        saveMap.put("meter_no", mapPamam.get("meter_no"));
        saveMap.put("meter_type", meterType);
        saveMap.put("capacity", mapTmp.get("left_amount"));
        saveMap.put("recharge_amonut", "0.00");
        saveMap.put("balance", mapTmp.get("left_amount"));

        DecimalFormat df = new DecimalFormat(".00");
        String money = df.format(leftAmount * price);

        saveMap.put("recharge_type", "清零");
        saveMap.put("refund_order_no", null);
        saveMap.put("recharge_amonut", money);
        saveMap.put("capacity", leftAmount);
        saveMap.put("balance", leftAmount);
        saveMap.put("unit", "度");
        saveMap.put("recharge_status", "待支付");
        saveMap.put("task_status", null);
        saveMap.put("remark", "待销户");

        RechareRecord rechareRecord = SpringContextUtils.getBean("RechargeRecord");
        rechareRecord.savaRechargeState(saveMap, OperationType.INSERT_TYPE);

        //然后开始销户
        saveMap.put("recharge_status", "充值成功");
        saveMap.put("remark", "销户成功");

        try {
            ret = hubContext.sendMessage(meterType, MeterOperationType.HIGH_POWER_CLOSE_ACCOUNT,
                    hubAddress, meterAddress, mapPamam);
            lstTmp = (List<Object>)((Map<String, Object>)ret).get("data");
            mapTmp = (Map<String, Object>)lstTmp.get(0);
        } catch (ValidataException e) {
            throw e;
        }

        //更新充值记录状态
        rechareRecord.savaRechargeState(saveMap, OperationType.UPDATE_TYPE);
        retMsg.putAll(mapTmp);
        List<Object> retList = new ArrayList<>();
        retList.add(retMsg);
        Map<String, Object> retMap = new HashMap<>();
        retMap.put("data", retList);

        //更新大功率电表信息状态
        PriceHighMeter priceHighMeter = SpringContextUtils.getBean("PriceHighMeter");
        priceHighMeter.updateMeter(meterAddress, hubAddress, "销户");

        return retMap;
    }

    public static byte[] getBytes(float data) {
        int intBits=Float.floatToIntBits(data);
        return getBytes(intBits);
    }

    public static byte[] getBytes(int data) {
        byte[] bytes=new byte[4];
        bytes[0]=(byte) (data & 0xff);
        bytes[1]=(byte) ((data & 0xff00) >> 8);
        bytes[2]=(byte) ((data & 0xff0000) >> 16);
        bytes[3]=(byte) ((data & 0xff000000) >> 24);
        return bytes;
    }

    public static void main(String[] args) {
        byte[] ret=MeterUtils.getDateTimeOfBytes(new SimpleDateFormat("--yyMMddHHmm--"), 5);
        byte[] temp=Arrays.copyOfRange(ret, 1, 5);

        MeterUtils.getIntOfBytes(12345, 3);
        //MeterUtils.float2BCDBytes(123456.78f, 4);
        //MeterUtils.highPowerRechargeAmountEncode(new byte[] {0x00, 0x12, 0x34, 0x56}, new byte[] {0x11, 0x16, 0x09, 0x10});

        //ret=getBytes(1234.56f);
        byte[] fbyte = CRCEncode.float2ByteArray(-1234.56f);

        float ff = CRCEncode.bytes2Float(fbyte);
        ByteBuffer buf=ByteBuffer.allocateDirect(4); //无额外内存的直接缓存
        //buf=buf.order(ByteOrder.LITTLE_ENDIAN);//默认大端，小端用这行
        buf.put(ret);
        buf.rewind();
        float f2=buf.getFloat();
        //float fret=CRCEncode.bytes2Float(ret);
        System.out.println("ss");

    }
*/


}
