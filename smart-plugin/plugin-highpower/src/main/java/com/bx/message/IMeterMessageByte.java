package com.bx.message;

import java.util.Map;

public interface IMeterMessageByte {
    byte[] getMessageBytes(String cmdCode, String hubAddress, String meterAddress, Map<String, Object> map);


}
