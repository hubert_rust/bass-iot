package com.bx.hotcode;

import com.bx.message.EncodeWattHourProtocol;
import com.bx.message.MessageEncodeHighPower;
import com.bx.plugin.CodeHandle;
import com.bx.plugin.CodeService;
import com.bx.plugin.HDType;
import com.bx.plugin.ISendMessageEncode;
import com.bx.utils.ByteUtil;
import com.bx.utils.CharacterUtil;
import com.bx.utils.StrUtil;
import com.google.common.base.Strings;
import com.smart.command.CommandCode;
import com.smart.command.CommandVal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;
import java.util.Objects;

/*
 * @ program: plugin-hotwater
 * @ description: d
 * @ author: admin
 * @ create: 2019-04-03 09:40
 **/
@CodeService(hdType= HDType.HDTYPE_HIGH_METER, dir=HDType.MSG_DIR_SEND)
public class HighMeterEncoder implements ISendMessageEncode {
    private static final Logger LOGGER = LoggerFactory.getLogger(com.bx.hotcode.HighMeterDecoder.class);

    @CodeHandle(name="codeHandle")
    public byte[] getOpen(String str) {
        return  null;
    }

    @PostConstruct
    public void init() {
        System.out.println();
    }

    public static void main(String[] args) {
        BigDecimal bigDecimal = new BigDecimal("3.145");
        bigDecimal = bigDecimal.setScale(2, RoundingMode.HALF_UP);
        System.out.println(bigDecimal);

    }
    /**
     * @ Description: 发送消息到硬件, 根据commandCode参数(命令),获取到字节码
     * @ Author: Admin
    **/
    @Override
    public byte[] getSendMessage(String commandCode,
                                 String conAddress,
                                 String meterAddress,
                                 byte frameSeq,
                                 Map map) throws Exception{
        byte[] byteTpl = null;
        try {
            int bps = Integer.valueOf(map.get("bps").toString());
            int commPort = Integer.valueOf(map.get("commPort").toString());
            switch (commandCode) {
                case CommandCode.HIGH_POWER_TIME_SYNC: {
                    byteTpl = MessageEncodeHighPower.getBytesOfTimeSync(conAddress,
                            meterAddress, frameSeq, map);
                    LOGGER.info(">>> HighMeterEncoder, getSendMessage, HIGH_POWER_TIME_SYNC: {}",
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    break;
                }

                case CommandCode.HIGH_POWER_STATE: {
                    byteTpl = MessageEncodeHighPower.getBytesOfState(conAddress,
                            meterAddress, frameSeq, map);
                    LOGGER.info(">>> HighMeterEncoder, getSendMessage, HIGH_POWER_STATE: {}",
                            StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    break;
                }
                case CommandCode.HIGH_POWER_LEFT_AMOUNT: {
                    byteTpl = MessageEncodeHighPower.getBytesOfLeftAmount(conAddress,
                            meterAddress, frameSeq, map);
                    LOGGER.info(">>> HighMeterEncoder, getSendMessage, CommandCode: {}, bytes: {}",
                            commandCode, StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    break;
                }
                case CommandCode.HIGH_POWER_RECHARGE_TIMES: {
                    byteTpl = MessageEncodeHighPower.getBytesOfRechargeTimes(conAddress,
                            meterAddress, frameSeq, map);
                    LOGGER.info(">>> HighMeterEncoder, getSendMessage, CommandCode: {}, bytes: {}",
                            commandCode, StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    break;
                }
                case CommandCode.HIGH_POWER_RECHARGE: {
                    byteTpl = MessageEncodeHighPower.getBytesOfRecharge(conAddress,
                            meterAddress, frameSeq, map);
                    LOGGER.info(">>> HighMeterEncoder, getSendMessage, CommandCode: {}, bytes: {}",
                            commandCode, StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    break;
                }
                case CommandCode.HIGH_POWER_REMOTE_ON: {
                    byteTpl = MessageEncodeHighPower.getBytesOfRemoteOn(conAddress,
                            meterAddress, frameSeq, map);
                    LOGGER.info(">>> HighMeterEncoder, getSendMessage, CommandCode: {}, bytes: {}",
                            commandCode, StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    break;
                }
                case CommandCode.HIGH_POWER_REMOTE_OFF: {
                    byteTpl = MessageEncodeHighPower.getBytesOfRemoteOff(conAddress,
                            meterAddress, frameSeq, map);
                    LOGGER.info(">>> HighMeterEncoder, getSendMessage, CommandCode: {}, bytes: {}",
                            commandCode, StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    break;
                }
                case CommandCode.HIGH_POWER_OPEN_ACCOUNT: {
                    byteTpl = MessageEncodeHighPower.getBytesOfOpenAccount(conAddress,
                            meterAddress, frameSeq, map);
                    LOGGER.info(">>> HighMeterEncoder, getSendMessage, CommandCode: {}, bytes: {}",
                            commandCode, StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    break;
                }
                case CommandCode.HIGH_POWER_CLOSE_ACCOUNT: {
                    byteTpl = MessageEncodeHighPower.getBytesOfCloseAccount(conAddress,
                            meterAddress, frameSeq, map);
                    LOGGER.info(">>> HighMeterEncoder, getSendMessage, CommandCode: {}, bytes: {}",
                            commandCode, StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    break;
                }
                case CommandCode.HIGH_POWER_READ_METER: {
                    byteTpl = MessageEncodeHighPower.getBytesOfReadMeter(conAddress,
                            meterAddress, frameSeq, map);
                    LOGGER.info(">>> HighMeterEncoder, getSendMessage, CommandCode: {}, bytes: {}",
                            commandCode, StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    break;
                }
                case CommandCode.HIGH_POWER_QUERY_DATE: {
                    byteTpl = MessageEncodeHighPower.getBytesOfQueryDate(conAddress,
                            meterAddress, frameSeq, map);
                    LOGGER.info(">>> HighMeterEncoder, getSendMessage, CommandCode: {}, bytes: {}",
                            commandCode, StrUtil.splitSpace(CharacterUtil.bytesToHexString(byteTpl)));
                    break;
                }

                default: {
                    LOGGER.error(">>> HighMeterEncoder, getSendMessage, commandCode invalid: {}",
                            commandCode);
                    throw new Exception("no method process for cmdCode: " + commandCode);
                    //break;
                }
            }
        } catch (Exception e) {
           LOGGER.error(">>> HighMeterEncoder, getSendMessage, e: {}", e.getMessage());
           throw new Exception(e.getMessage());
        }

        return byteTpl;
    }

}
