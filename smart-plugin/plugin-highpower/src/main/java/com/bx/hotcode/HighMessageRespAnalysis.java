package com.bx.hotcode;

import com.bx.constants.MsgConst;
import com.bx.message.inside.IMessage;
import com.bx.message.msgtpl.MsgByteTpl;
import com.bx.plugin.CodeService;
import com.bx.plugin.HDType;
import com.bx.plugin.IRespMessageAnalysis;
import com.bx.regcode.CodeHandleRegister;
import com.bx.utils.ByteUtil;
import com.bx.utils.CharacterUtil;
import com.bx.utils.StrUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static com.bx.utils.ByteUtil.calc_sum_crc;
import static com.bx.utils.CharacterUtil.charToByte;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-07 13:43
 **/
@CodeService(hdType= HDType.HDTYPE_HIGH_METER, dir=HDType.MSG_DIR_ANALYSIS)
public class HighMessageRespAnalysis implements IRespMessageAnalysis {
    private static final Logger log = LoggerFactory.getLogger(HighMessageRespAnalysis.class);

    public static HighMessageRespAnalysis instance = new HighMessageRespAnalysis();

    public HighMessageRespAnalysis getInstance() {
        return instance;
    }
    /*public static class Holder {
        public static MessageRespAnalysis instance = new MessageRespAnalysis();
    }*/


    /**
     * @Description: 硬件响应消息处理
     * @Param:
     * @return:
     * @Author: admin
     * @Date: 2019/5/7
     */
    @Override
    public IMessage respMessageProcess(byte[] bytes, Channel channel, boolean addressConvert) {
        if (addressConvert && !checkValid(bytes))  { return null; }

        IMessage map = null;
        com.bx.hotcode.HighMeterDecoder decoder = (com.bx.hotcode.HighMeterDecoder) CodeHandleRegister
                .funDecodeHandle.get(HDType.HDTYPE_ELEC_METER);
        if (addressConvert) { setHubAddr2Dec(bytes, 9, 11); }
        switch (bytes[12]) {
            // 链路接口检测
            case 0x02: {
                //登录
                if (ByteUtil.bytesCompare(bytes, 14,
                        MsgByteTpl.getByteTplByMsg(MsgConst.MSG_TYPE_LOGIN, false),
                        14, 4)) {

                    map = decoder.getRespMessageObj(bytes, MsgConst.MSG_TYPE_LOGIN);
                }
                else if (ByteUtil.bytesCompare(bytes, 14,
                        MsgByteTpl.getByteTplByMsg(MsgConst.MSG_TYPE_BEAT, false),
                         14, 4)) {
                    //心跳
                    map = decoder.getRespMessageObj(bytes, MsgConst.MSG_TYPE_BEAT);
                }
                break;
            }
            //数据转发
            case 0x10: {

                map = decoder.getRespMessageObj(bytes, MsgConst.MSG_TYPE_RESP);
                break;
            }
            default: {
                log.error(">>> respMessageProcess, bytes: {}",
                        StrUtil.splitSpace(CharacterUtil.bytesToHexString(bytes)));
                break;
            }
        }

        return map;
    }


    @Override
    public void splicingPackage(ByteBuf bufferIn, List<Object> out) {
        //log.info(">>> splicingPackage, readableBytes: {}", bufferIn.readableBytes());

        if (bufferIn.readableBytes() < 5) {
            return;
        }

        // 读取开始的位置
        int beginIndex = bufferIn.readerIndex();

        // 开始头
        byte header = bufferIn.readByte();
        // 第一组数据长度
        byte len1byte1 = bufferIn.readByte();
        byte len1byte2 = bufferIn.readByte();

        // 第二组数据长度
        byte len2byte1 = bufferIn.readByte();
        byte len2byte2 = bufferIn.readByte();

        // 根据业务定义计算出数据长度
        int a = Integer.parseInt(CharacterUtil.bytesToHexString(len1byte2), 16);
        int b = Integer.parseInt(CharacterUtil.bytesToHexString(len1byte1), 16);
        int dataLength = (a * 256 + b) >> 2;
        dataLength = dataLength + 3;
        //log.info(">>> splicingPackage, beginIndex: {}, readableBytes: {}, dataLength: {}",
        //        beginIndex, bufferIn.readableBytes(), dataLength);

        // 如果小于数据长度 充值读取索引位置
        if (bufferIn.readableBytes() < dataLength) {
            bufferIn.readerIndex(beginIndex);
            return;
        }

        bufferIn.readerIndex(beginIndex + dataLength+5);
        ByteBuf otherByteBufRef = bufferIn.slice(beginIndex, dataLength+5);
        otherByteBufRef.retain();
        out.add(otherByteBufRef);
    }

    @Override
    public boolean checkValid(byte[] bytes) {
        byte crc = calc_sum_crc(bytes, 6, bytes.length-3);
        int a = Integer.parseInt(CharacterUtil.bytesToHexString(bytes[2]), 16);
        int b = Integer.parseInt(CharacterUtil.bytesToHexString(bytes[1]), 16);
        int len = (a * 256 + b) >> 2;
        if (bytes[0] == 0x68
                && len + 8 == bytes.length
                && bytes[5] == 0x68
                && bytes[bytes.length - 1] == 0x16
                && bytes[bytes.length - 2] == crc) {
            return true;// 校验通过
        }

        log.error(">>> MessageRespAnalysis, checkValid false, bytes: {}",
                StrUtil.splitSpace(CharacterUtil.bytesToHexString(bytes)));
        return false;
    }

    public static void setHubAddr2Dec(byte[] addrA2, int start, int end) {
        int intA2 = ((addrA2[start + 1] & 0xFF) << 4) | (addrA2[start] & 0xFF);

        String strHexA2 = String.format("%06d", intA2);
        strHexA2 = strHexA2.toUpperCase();
        addrA2[start] = (byte)((charToByte(strHexA2.charAt(4)) << 4) | (charToByte(strHexA2.charAt(5))));
        addrA2[start + 1] = (byte)((charToByte(strHexA2.charAt(2)) << 4) | (charToByte(strHexA2.charAt(3))));
        addrA2[start + 2] = (byte)((charToByte(strHexA2.charAt(0)) << 4) | (charToByte(strHexA2.charAt(1))));
    }
}
