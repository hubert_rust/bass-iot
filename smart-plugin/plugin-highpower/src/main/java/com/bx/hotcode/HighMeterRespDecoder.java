package com.bx.hotcode;

import com.bx.PluginContainer;
import com.bx.constants.MsgConst;
import com.bx.enginecore.ConcentratorHolder;
import com.bx.message.DecodeWattHourProtocol;
import com.bx.message.inside.IMessage;
import com.bx.message.inside.MsgCommon;
import com.bx.utils.ByteUtil;
import com.bx.utils.CharacterUtil;
import com.bx.utils.StrUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.smart.command.CommandCode;
import com.smart.common.ResultConst;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-09 16:53
 **/
public class HighMeterRespDecoder {
    private static final Logger log = LoggerFactory.getLogger(HighMeterRespDecoder.class);

    private static final byte[] READ_HIGH_POWER_STATE_FLAG = {0x01, 0x05, 0x00, 0x04};
    private static final byte[] READ_HIGH_POWER_PRICE_FLAG = {0x1, 0x1, 0x5, 0x04};
    private static final byte[] READ_HIGH_POWER_KWH_FLAG = {0x0, 0x0, 0x0, 0x0};
    private static final byte[] READ_HIGH_POWER_SOC_FLAG = {0x0, 0x01, (byte) 0x90, 0x0}; //剩余电量
    private static final byte[] READ_HIGH_RECHARGE_TIME_FLAG = {0x02, 0x01, 0x32, 0x03};
    private static final byte[] READ_HIGH_POWER_READ_METER = {(byte) 0xFF, (byte) 0xFF, 0x32, 0x03};   //抄表
    private static final byte[] READ_HIGH_POWER_METER_DATE = {(byte) 0x01, (byte) 0x01, 0x00, 0x04};   //日期

    public static IMessage respMessageProc(byte[] bytes) {

        Map<String, Object> data = Maps.newHashMap();
        data.put("pluginId", PluginContainer.pluginId);
        MsgCommon<Map> mapMsgCommon = null;
        byte[] address = Arrays.copyOfRange(bytes, 7, 7 + 5);
        String hexAddress = CharacterUtil.bytesToHexStringlowHigh(address);
        byte index_rcv_com_transmit_len = 18;
        byte fbyte_head = 0x68;
        int iHeadPos = ByteUtil.byte_array_locate(bytes, index_rcv_com_transmit_len + 1, fbyte_head);
        if (iHeadPos != 19) {
            log.error(">>> HighMeterRespDecoder, respMessageProc, invalid iHeadPos: {}", iHeadPos);
        }

        if (iHeadPos == 19) {
            byte[] meterAddr = new byte[6];
            System.arraycopy(bytes, iHeadPos + 1, meterAddr, 0, 6);
            String meterStr = CharacterUtil.bytesToHexStringlowHigh(meterAddr);
            data.put("hubAddr", hexAddress);
            data.put("meterAddr", meterStr);
            DecodeWattHourProtocol.checkBytesValid(bytes);
            highPowerRespProcess(bytes, data);
        }
        List<Map> list = Lists.newArrayList();
        list.add(data);


        byte seq = (byte) (int) Integer.valueOf(data.get("seq").toString());
        String hubAddrResp = data.get("hubAddr").toString();
        String meterAddrResp = data.get("meterAddr").toString();

        //投递到消息loop中
        mapMsgCommon = MsgCommon.MsgCommonBuilder.aMsgCommon()
                .withMsgType(MsgConst.MSG_TYPE_RESP)
                .withMsgAddr(hubAddrResp)
                .withMsgSubAddr(meterAddrResp)
                .withMsgSeq(seq)
                .build();
        mapMsgCommon.setRespStr(StrUtil.splitSpace(CharacterUtil.bytesToHexString(bytes)));
        //使用这种方式
        mapMsgCommon.getResp().add(data);

        return mapMsgCommon;
    }

    //响应消息处理
    public static void highPowerRespProcess(byte[] resp, Map<String, Object> ret) {
        //关闸,合闸: 正常C=0x9C L=0; 异常C=0xDC, L=1;
        //读数据: 正常C=0x91 L包括数据标识; 异常C=0xD1, L=1;
        ret.put("seq", resp[13]);
        DecodeWattHourProtocol p = new DecodeWattHourProtocol();
        p.prepareRespData(resp);
        if (p.getControlCode() == (byte) 0x9C || p.getControlCode() == (byte) 0xDC) {
            ret = p.getWattHourGateResp(resp, ret);
        } else if (p.getControlCode() == (byte) 0x91 || p.getControlCode() == (byte) 0xD1) {
            if (p.getControlCode() == (byte) 0xD1) {
                //ret = new HashMap<>();
                ret.put("retState", CharacterUtil.bytesToHexString(p.getMeterData()));
                ret.put("result", ResultConst.SUCCESS);
            } else {
                byte[] dataFlag = p.getMeterDataFlag();
                if (Arrays.equals(dataFlag, READ_HIGH_POWER_STATE_FLAG)) {
                    ret = p.getWattHourStateResp(resp, ret);
                } else if (Arrays.equals(dataFlag, READ_HIGH_POWER_PRICE_FLAG)) {
                    ret = p.getWattHourPriceResp(resp, ret);
                } else if (Arrays.equals(dataFlag, READ_HIGH_POWER_KWH_FLAG)) {
                    ret = p.getWattHourKwhResp(ret);
                } else if (Arrays.equals(dataFlag, READ_HIGH_POWER_SOC_FLAG)) {
                    ret = p.getWattHourSocResp(ret);
                } else if (Arrays.equals(dataFlag, READ_HIGH_RECHARGE_TIME_FLAG)) {
                    ret = p.getWattHourRechargeTimes(resp, ret);
                } else if (Arrays.equals(dataFlag, READ_HIGH_POWER_READ_METER)) {
                    ret = p.getReadMeterResp(ret);
                } else if (Arrays.equals(dataFlag, READ_HIGH_POWER_METER_DATE)) {
                    ret = p.getReadMeterDateResp(ret);
                }
            }

        }

/*        if (ret != null) {
            String hubAddr = p.getHubAddr();
            String meterAddr = p.getMeterAddr();
            byte seq = p.getSeq();
            String key = hubAddr + "_" + meterAddr + "_" + seq;
            ret.put("集中器地址", hubAddr);
            ret.put("表地址", meterAddr);

            ret.put("ctlCode", CharacterUtil.bytesToHexString(p.getControlCode()));
            ReqRspUtil.setResponse(key, ret);
        }
        return ret; */
    }

}
