package com.bx.config;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-07-19 18:42
 **/
public class MixConst {
    public final static byte index_trans_head_offset_meter_type = 1;
    public final static byte index_frame_seq = 13;
    public final static byte index_hub_addr_start = 7;

    public final static byte index_trans_head_offset_meter_data_len = 10;

    /* 水表地址起始相对位置及表地址长度 */
    public final static byte index_rcv_offset_meter_addr_start = 2;
    public final static int fi_meter_addr_len = 7;
    /* 控制字 */
    public final static byte index_trans_head_offset_ctrl_type = 9;
    public final static byte fbyte_enum_ctrl_code_read = 0x01;
    public final static byte fbyte_enum_ctrl_code_write = 0x04;
    public final static byte fbyte_enum_ctrl_type_read_resp_succ = (byte) 0x81;
    public final static byte fbyte_enum_ctrl_type_write_resp_succ = (byte) 0x84;
    public final static byte fbyte_enum_ctrl_type_read_resp_failed = (byte) 0xC1;
    public final static byte fbyte_enum_ctrl_type_write_resp_failed = (byte) 0xC4;


    public final static byte index_trans_head_offset_valve_cmd1 = 11;
    public final static byte fbyte_cmd_read_amount_data_cmd1 = (byte) 0x1F;
    public final static byte fbyte_cmd_read_amount_data_cmd2 = (byte) 0x90;
    public final static byte fbyte_cmd_valve_ctrl_data_cmd1 = (byte) 0x17;
    public final static byte fbyte_cmd_valve_ctrl_data_cmd2 = (byte) 0xA0;
    public final static byte fbyte_cmd_recharge_data_cmd1 = (byte) 0x10;
    public final static byte fbyte_cmd_recharge_data_cmd2 = (byte) 0xA1;
    public final static byte fbyte_cmd_recharge_amount_cmd1 = (byte) 0x11;
    public final static byte fbyte_cmd_recharge_amount_cmd2 = (byte) 0x82;

    public final static byte fbyte_cmd_hot_valve_state_query_cmd1 = (byte) 0x06;
    public final static byte fbyte_cmd_hot_valve_state_query_cmd2 = (byte) 0x31;
    public final static byte fbyte_cmd_hot_valve_open_data_cmd1 = (byte) 0x06;
    public final static byte fbyte_cmd_hot_valve_open_data_cmd2 = (byte) 0xE1;
    public final static byte fbyte_cmd_hot_valve_close_data_cmd1 = (byte) 0x05;
    public final static byte fbyte_cmd_hot_valve_close_data_cmd2 = (byte) 0xE1;
    public final static byte fbyte_cmd_hot_valve_use_record_query_data_cmd1 = (byte) 0x0B;
    public final static byte fbyte_cmd_hot_valve_use_record_query_data_cmd2 = (byte) 0x31;
    public final static byte fbyte_cmd_hot_valve_user_add_data_cmd1 = (byte) 0x07;
    public final static byte fbyte_cmd_hot_valve_user_add_data_cmd2 = (byte) 0xE1;
    public final static byte fbyte_cmd_hot_valve_user_query_data_cmd1 = (byte) 0x08;
    public final static byte fbyte_cmd_hot_valve_user_query_data_cmd2 = (byte) 0x31;
    public final static byte fbyte_cmd_hot_valve_spec_user_query_data_cmd1 = (byte) 0x09;
    public final static byte fbyte_cmd_hot_valve_spec_user_query_data_cmd2 = (byte) 0x31;
    public final static byte fbyte_cmd_hot_valve_user_update_data_cmd1 = (byte) 0x08;
    public final static byte fbyte_cmd_hot_valve_user_update_data_cmd2 = (byte) 0xE1;
    public final static byte fbyte_cmd_hot_valve_user_delete_data_cmd1 = (byte) 0x09;
    public final static byte fbyte_cmd_hot_valve_user_delete_data_cmd2 = (byte) 0xE1;
    public final static byte fbyte_cmd_hot_valve_price_set_data_cmd1 = (byte) 0x04;
    public final static byte fbyte_cmd_hot_valve_price_set_data_cmd2 = (byte) 0xE1;
    public final static byte fbyte_cmd_hot_valve_broadcast_set_time_data_cmd1 = (byte) 0x14;
    public final static byte fbyte_cmd_hot_valve_broadcast_set_time_data_cmd2 = (byte) 0xA1;


    public final static byte fbyte_len_read_amount_resp_data_len = 0x16;
    public final static byte fbyte_len_valve_ctrl_resp_data_len = 0x05;
    public final static byte fbyte_len_recharge_resp_data_len = 0x03;
    public final static byte fbyte_len_hot_valve_state_query_resp_data_len = 0x15;
    public final static byte fbyte_len_hot_valve_state_open_resp_data_len = 0x03;
    public final static byte fbyte_len_hot_valve_state_close_resp_data_len = 0x03;
    public final static int fint_len_hot_valve_use_record_query_resp_data_unit_len = 35; // 一条使用记录的长度
    public final static byte fbyte_len_hot_valve_user_add_resp_data_len = 0x03;
    public final static byte fbyte_len_hot_valve_user_query_resp_data_unit_len = 13;
    public final static byte fbyte_len_hot_valve_user_update_resp_data_len = 0x03;
    public final static byte fbyte_len_hot_valve_user_delete_resp_data_len = 0x03;
    public final static byte fbyte_len_hot_valve_price_set_resp_data_len = 0x08;
    public final static byte fbyte_len_hot_valve_broadcast_set_time_resp_data_len = 0x08;
}
