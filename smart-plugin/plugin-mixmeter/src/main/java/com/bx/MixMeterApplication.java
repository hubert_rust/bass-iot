package com.bx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-07-18 17:24
 **/
@SpringBootApplication
public class MixMeterApplication {
    public static void main(String[] args) {
        SpringApplication.run(MixMeterApplication.class, args);
    }
}
