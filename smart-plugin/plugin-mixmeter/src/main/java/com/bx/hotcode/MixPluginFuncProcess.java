package com.bx.hotcode;

import com.alibaba.fastjson.JSONObject;
import com.bx.PluginContainer;
import com.bx.constants.MsgConst;
import com.bx.enginecore.ConcentratorContextInterface;
import com.bx.enginecore.ConcentratorHolder;
import com.bx.message.inside.MsgCommon;
import com.bx.service.IPluginFuncProcess;
import com.google.common.collect.Maps;
import com.smart.common.ResponseEntity;
import com.smart.common.ResponseUtils;
import com.smart.common.ResultConst;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-06 14:30
 **/
@Service("mix-meterFuncProcess")
@Scope("prototype")
public class MixPluginFuncProcess implements IPluginFuncProcess{
    private static final Logger log = LoggerFactory.getLogger(MixPluginFuncProcess.class);

    private ReentrantLock lock = new ReentrantLock(true);

    private String checkValid(Map<String, Object> map) {

        String conAddr= map.get("hubAddress").toString();
        String cmdOrder = map.get("commandOrder").toString();
        if (map == null || map.size() <=0 || StringUtils.isEmpty(conAddr) || StringUtils.isEmpty(cmdOrder)) {
            return  "请求数据非法";
        }
        if (map.get("hd_type") == null) {
            return  "请求数据非法, 没有:hd_type";
        }

        ConcentratorContextInterface con = ConcentratorHolder.conMap.get(conAddr);
        if (con == null) {
            return conAddr +" :获取不到虚拟集中器";
        }
        if (!con.getConState()) {
            return conAddr + " :集中器状态不可用";
        }

        return null;
    }

    public String checkUpgrade(Map<String, Object> map, String subType) throws Exception {
        return null;
    }

    /**
    * @Description: 在发送消息入队列之前, 将消息码流封装好，message loop中直接发送
    * @Param:
    * @return:
    * @Author: admin
    * @Date: 2019/5/8
    */
    @Override
    public  String process(Map<String, Object> map, String subType) throws Exception {
        if (Objects.isNull(map)) {
            return ResponseUtils.getFailResponse(ResultConst.FAIL);
        }
        Object respObj = null;

        try {
            ResponseEntity<Map<String, Object>> resp = new ResponseEntity<>();
            String conAddr = map.get("hubAddress").toString();
            String cmdOrder = map.get("commandOrder").toString();
            String cmdCode = map.get("cmdCode").toString();
            String cmdType = map.get("cmdType").toString();

            //超时时间，通过配置获取
            long tm = PluginContainer.configEntity.getPlugin().getCountDownTm();

            Map<String, Object> entity = (Map) ((List) map.get("entitys")).get(0);
            if (Objects.isNull(entity) || entity.size() <= 1) {
                throw new Exception("entitys invalid");
            }

            String meterAddr = entity.get("meterAddress").toString();
            String hubAddr = entity.get("hubAddress").toString();
            if (!conAddr.equals(hubAddr)) {
                throw new Exception("hubAddress is not match");
            }

            ConcentratorContextInterface contextInterface = (ConcentratorContextInterface)
                    ConcentratorHolder.getConMap().get(conAddr);
            if (!contextInterface.getConState()) {
                throw new Exception("hubAddress: " + conAddr + " state is fault");
            }

            log.info(">>> PluginFuncProcess, process, starting in queue, {}", System.currentTimeMillis());
            CountDownLatch countDownLatch = null;
            try {
                countDownLatch = ConcentratorHolder
                        .putRequest(cmdType, cmdCode, conAddr, meterAddr, cmdOrder, entity);

            } catch (Exception e) {
                resp.setReturnCode("FAIL");

                resp.setReturnMsg(e.getMessage());
                return JSONObject.toJSONString(resp);
            } finally {
            }

            log.info(">>> PluginFuncProcess, process, tm: {}", tm);
            countDownLatch.await(tm, TimeUnit.SECONDS);
            //释放升级资源, 如果升级中断了，需要处理

            log.info(">>> PluginFuncProcess, process, starting out queue, {}", System.currentTimeMillis());
            respObj = resultProcess(entity, cmdCode, conAddr, cmdOrder, map, resp);
        } catch (Exception e) {
            throw new Exception(e.getMessage());

        } finally {
        }
        return (String)respObj;
    }


    private String resultProcess(Map<String, Object> entity,
                                 String cmdCode,
                                 String conAddr,
                                 String cmdOrder,
                                 Map request,
                                 ResponseEntity resp) throws Exception {
        MsgCommon respData = (MsgCommon) ConcentratorHolder.getRespData(conAddr, cmdOrder);
        resp.setCommandOrder(cmdOrder);
        if (respData == null) {
            log.error(">>> resultProcess, ");
            return "";
        }

        int isRetReqBytes = 0;
        int isRetRespBytes = 0;
        Object obj = request.get("isRetSendBytes");
        if (Objects.nonNull(obj)) {
            isRetReqBytes = Integer.valueOf(request.get("isRetSendBytes").toString());
        }
        obj = request.get("isRetRespBytes");
        if (Objects.nonNull(obj)) {
            isRetRespBytes = Integer.valueOf(request.get("isRetRespBytes").toString());
        }

        //判断状态
        String state = respData.state();
        if (state == MsgConst.MSG_STATE_RESP) {

            PluginContainer.sendMeterState(respData.getMsgAddr(),
                    respData.getMsgSubAddr(),
                    "ok");

            resp.setReturnCode("SUCCESS");
            resp.setReturnMsg("OK");
            //正常响应release

            Map<String, Object> byteMap = Maps.newHashMap();
            resp.setRetBytes(byteMap);
            if (isRetReqBytes > 0) { byteMap.put("req", respData.getReqStr()); }
            if (isRetRespBytes> 0) { byteMap.put("resp", respData.getRespStr()); }

            //将响应结果保存
            List<Map<String, Object>> data = (List<Map<String, Object>>)respData.getResp();
            resp.getDatas().addAll(data);
            resp.setResultCode((Objects.nonNull(data) && data.size()>0) ? "SUCCESS" : "FAIL");
            resp.setResultMsg((Objects.nonNull(data) && data.size()>0) ? "OK" : "no response data");
            //这里实际就是回响应返回给管理层的时间
            respData.upMsgTm();
            resp.setMsgMonitor(respData.getMsgMonitor());

            //获取数据返回, 将返回结果map转化为String,
            String var = JSONObject.toJSONString(resp);//getRespString(true, "", resp);
            //最后清理
            ConcentratorHolder.sendReleaseMessage(conAddr, cmdOrder, MsgConst.MSG_TYPE_RELEASE);
            log.info(">>> resultProcess, conAddress: {}, commondOrder: {}", conAddr, cmdOrder);

            //返回String
            return var;
        }
        else if (state == MsgConst.MSG_STATE_READY) {

            //返回超时, 发送消息到
            if (respData != null) {
                //重新获取下
            }
            PluginContainer.sendMeterState(respData.getMsgAddr(),
                    respData.getMsgSubAddr(),
                    "nok");
            resp.setReturnCode("SUCCESS");
            resp.setReturnMsg("OK");
            resp.setResultCode("FAIL");
            resp.setResultMsg("response timeout");

            Map<String, Object> byteMap = Maps.newHashMap();
            resp.setRetBytes(byteMap);
            if (isRetReqBytes > 0) { byteMap.put("req", respData.getReqStr()); }
            if (isRetRespBytes> 0) { byteMap.put("resp", respData.getRespStr()); }
            String var = JSONObject.toJSONString(resp);//getRespString(false, "定时器超时, timeout=5s", resp);
            //最后清理
            ConcentratorHolder.sendReleaseMessage(conAddr, cmdOrder, MsgConst.MSG_CAUSE_RELEASE_TIMEOUT);
            return var;
        }
        else {
            resp.setReturnMsg(state + "");
            log.error(">>> invalid state: {}", state);
        }
        return JSONObject.toJSONString(resp);
    }

}
