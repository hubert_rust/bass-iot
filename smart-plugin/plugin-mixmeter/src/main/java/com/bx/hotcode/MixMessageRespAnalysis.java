package com.bx.hotcode;

import com.bx.PluginContainer;
import com.bx.constants.MsgConst;
import com.bx.enginecore.ConcentratorContext;
import com.bx.enginecore.ConcentratorHolder;
import com.bx.message.DecodeWattHourProtocol;
import com.bx.message.inside.IMessage;
import com.bx.message.inside.MsgCommon;
import com.bx.message.msgtpl.MsgByteTpl;
import com.bx.plugin.CodeService;
import com.bx.plugin.HDType;
import com.bx.plugin.IRespMessageAnalysis;
import com.bx.plugin.IRespMessageDecode;
import com.bx.regcode.CodeHandleRegister;
import com.bx.utils.ByteUtil;
import com.bx.utils.CharacterUtil;
import com.bx.utils.StrUtil;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.smart.command.CommandCode;
import com.smart.common.ResultConst;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.bx.config.MixConst;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.bx.config.MixConst.*;
import static com.bx.utils.ByteUtil.calc_sum_crc;
import static com.bx.utils.CharacterUtil.charToByte;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-07 13:43
 **/
@CodeService(hdType = HDType.HDTYPE_MIX_METER, dir = HDType.MSG_DIR_ANALYSIS)
public class MixMessageRespAnalysis implements IRespMessageAnalysis {
    private static final Logger log = LoggerFactory.getLogger(MixMessageRespAnalysis.class);

    public static byte[] arb_rcv_data_login_req = {0x68, 0x31, 0, 0x31, 0,
            0x68, (byte) 0xC9, 0x49, 0, 1, 0, 0, 2, 0x7B, 0, 0, 1, 0,
            (byte) 0x91, 0x16};
    public static byte[] arb_rcv_data_heartbeat_req = {0x68, 0x31, 0, 0x31, 0,
            0x68, (byte) 0xC9, 0x49, 0, 1, 0, 0, 2, 0x7C, 0, 0, 4, 0,
            (byte) 0x95, 0x16};

    public final static int fi_hub_addr_byte_len = 5;
    public static byte[] arb_send_data_login_resp = {0x68, 0x31, 0, 0x31, 0,
            0x68, 0x0B, 0x49, 0, 1, 0, 0, 0, 0x6B, 0, 0, 1, 0, (byte) 0xC1,
            0x16};
    public static byte[] arb_send_data_heartbeat_resp = {0x68, 0x31, 0, 0x31,
            0, 0x68, 0x0B, 0x49, 0, 1, 0, 0, 0, 0x6B, 0, 0, 4, 0, 0x1, 0x16};
    private static Map<String, IRespMessageAnalysis> respMessageAnalysisMap = null;
    private static Map<String, IRespMessageDecode> respMessageDecodeMap = null;

    static {
        respMessageDecodeMap = Maps.newHashMap();
        respMessageDecodeMap.put(CommandCode.HUB_TYPE_ELECMETER, new com.bx.hotcode.ElecMeterDecoder());
        respMessageDecodeMap.put(CommandCode.HUB_TYPE_COLDMETER, new com.bx.hotcode.ColdMeterDecoder());
        respMessageDecodeMap.put(CommandCode.HUB_TYPE_HOTMETER, new com.bx.hotcode.HotMeterDecoder());
        respMessageDecodeMap.put(CommandCode.HUB_TYPE_HIGHMETER, new com.bx.hotcode.HighMeterDecoder());
    }

    public static MixMessageRespAnalysis instance = new MixMessageRespAnalysis();

    public MixMessageRespAnalysis getInstance() {
        return instance;
    }
    /*public static class Holder {
        public static MessageRespAnalysis instance = new MessageRespAnalysis();
    }*/


    public static boolean checkAddress(String address) {
        if (Strings.isNullOrEmpty(address)) {
            return false;
        }
        return (ConcentratorHolder.getConMap().get(address) == null) ? false : true;
    }

    public static IRespMessageDecode getDecoder(String cmdType) {
        return respMessageDecodeMap.get(cmdType);
    }

    /**
     * 登录返回消息处理
     *
     * @param rcv_msg
     * @return
     */
    public static byte[] resp_login_proc(byte[] rcv_msg) {

        byte[] arb_send_data_login_resp = {0x68, 0x31, 0, 0x31, 0, 0x68, 0x0B,
                0x49, 0, 1, 0, 0, 0, 0x6B, 0, 0, 1, 0, (byte) 0xC1, 0x16};

        System.arraycopy(rcv_msg, index_hub_addr_start,
                arb_send_data_login_resp, index_hub_addr_start,
                fi_hub_addr_byte_len);
        arb_send_data_login_resp[index_frame_seq] = rcv_msg[index_frame_seq];
        arb_send_data_login_resp[arb_send_data_login_resp.length - 2] = ByteUtil.calc_sum_crc(
                arb_send_data_login_resp, 6,
                arb_send_data_login_resp.length - 3);

        return arb_send_data_login_resp;
    }

    /**
     * 心跳返回消息
     *
     * @param rcv_msg
     * @return
     */
    public static byte[] resp_heartbeat_proc(byte[] rcv_msg) {

        byte[] arb_send_data_heartbeat_resp = {0x68, 0x49, 0x00, 0x49, 0x00,
                0x68, 0x0B, 0x49, 0x00, 0x01, 0x00, 0x00, 0x00, 0x6C, 0x00,
                0x00, 0x04, 0x00, 0x02, 0x00, 0x00, 0x04, 0x00, 0x00,
                (byte) 0xCB, 0x16};

        System.arraycopy(rcv_msg, index_hub_addr_start,
                arb_send_data_heartbeat_resp, index_hub_addr_start,
                fi_hub_addr_byte_len);
        arb_send_data_heartbeat_resp[index_frame_seq] = rcv_msg[index_frame_seq];
        arb_send_data_heartbeat_resp[arb_send_data_heartbeat_resp.length - 2] = ByteUtil.calc_sum_crc(
                arb_send_data_heartbeat_resp, 6,
                arb_send_data_heartbeat_resp.length - 3);

        return arb_send_data_heartbeat_resp;

    }


    /**
     * @Description: 硬件响应消息处理
     * @Param:
     * @return:
     * @Author: admin
     * @Date: 2019/5/7
     */
    @Override
    public IMessage respMessageProcess(byte[] bytes, Channel channel, boolean addressConvert) {
        if (!checkValid(bytes)) {
            return null;
        }

        IMessage iMessage = null;
        setHubAddr2Dec(bytes, 9, 11);
        switch (bytes[12]) {
            // 链路接口检测
            case 0x02: {
                byte[] address = Arrays.copyOfRange(bytes, 7,
                        7 + 5);
                String hexAddress = CharacterUtil.bytesToHexStringlowHigh(address);

                //登录
                if (ByteUtil.bytesCompare(bytes, 14,
                        arb_rcv_data_login_req,
                        14, 4)) {
                    log.info(">>> MixMessageRespAnalyais, respMessageProcess, login message: {}",
                            CharacterUtil.bytesToHexString(bytes));

                    iMessage = MsgConst.getLogin(bytes);
                    iMessage = MsgCommon.MsgCommonBuilder.aMsgCommon()
                            .withMsgType(MsgConst.MSG_TYPE_LOGIN_RESP)
                            .withMsgAddr(hexAddress)
                            .withState("ok")
                            .build();
                    ((MsgCommon) iMessage).setHubType(CommandCode.HUB_TYPE_MIX);
                    ((MsgCommon) iMessage).setBytes(resp_login_proc(bytes));
                } else if (ByteUtil.bytesCompare(bytes, 14,
                        arb_rcv_data_heartbeat_req,
                        //MsgByteTpl.getByteTplByMsg(MsgConst.MSG_TYPE_BEAT, false),
                        14, 4)) {
                    log.info(">>> MixMessageRespAnalyais, respMessageProcess, heartbeat message: {}",
                            CharacterUtil.bytesToHexString(bytes));
                    //心跳
                    iMessage = MsgCommon.MsgCommonBuilder.aMsgCommon()
                            .withMsgType(MsgConst.MSG_TYPE_BEAT_RESP)
                            .withMsgAddr(hexAddress)
                            .withState("ok")
                            .build();
                    ((MsgCommon) iMessage).setBytes(resp_heartbeat_proc(bytes));
                }
                break;
                // 登录
/*                if (ByteUtil.bytesCompare(bytes, 14,
                        arb_rcv_data_login_req, 14, 4)) {
                    byte[] address = Arrays.copyOfRange(bytes,
                            7,
                            7 + 5);
                    String loginAddress = CharacterUtil.bytesToHexStringlowHigh(address);
                    boolean check = checkAddress(loginAddress);
                    if (check) {
                         iMessage = getDecoder(loginAddress).respMessageProcess(bytes, channel, false);

                    } else {
                        log.error(">>> MixMessageRespAnalysis, login: {}, invalid address{}",
                                CharacterUtil.bytesToHexString(bytes),
                                loginAddress);

                        if (channel != null) {
                            log.error(">>> MixMessageRespAnalysis, hubAddress is not reg, channel info: {}, and close.",
                                    channel.toString());
                            channel.close();
                        }

                    }

                    // 存储登录的集中器

                } else if (ByteUtil.bytesCompare(bytes,
                        14,
                        arb_rcv_data_heartbeat_req,
                        14,
                        4)) {

                    byte[] address = Arrays.copyOfRange(bytes,
                            7,
                            7 + 5);
                    String hexAddress = CharacterUtil
                            .bytesToHexStringlowHigh(address);

                    // 心跳
                    iMessage = getDecoder(hexAddress).respMessageProcess(bytes, channel, false);

                } else {
                    log.error(":" + StrUtil.splitSpace(CharacterUtil.bytesToHexString(bytes)));
                }
                break;*/
            }
            // 数据转发
            case 0x10: {
                iMessage = MixProcess(bytes);
            }
            default: {
                break;
            }

        }

        return iMessage;
    }

    public IMessage MixProcess(byte[] rcv_buf) {


        // 0x68,(byte)0xCD,0x00,(byte)0xCD,0x00,
        // 0x68, (byte)0x88,/*控制域:发送-0x4B,接收-0x88*/
        // 0x49,0x00,0x01,0x00,0x02,
        // 0x10,/*afn:02-链路检测，0x10-转发*/ 0x60,0x00,0x00,0x01,0x00,
        // 0x26,/*transmit data len*/
        // (byte)0xFE,(byte)0xFE,(byte)0xFE, /*前导帧，目前没有发*/
        // 0x68, 0x50, /*0x10-热水表，0x50-冷水表*/
        // 0x11,0x11,0x11,0x11,0x11,0x11,0x11, /*表地址*/
        // (byte)0x81,/*read ack：0x81-查询响应，0xC1-异常*/ 0x15,/*data len*/
        // 0x06,(byte)0x31,/*cmd*/ 0x01,/*表数据帧序号*/
        IMessage iMessage = null;
        byte[] address = Arrays.copyOfRange(rcv_buf, 7,
                7 + 5);
        String hexAddress = CharacterUtil.bytesToHexStringlowHigh(address);
        byte index_rcv_com_transmit_len = 18;
        byte fbyte_head = 0x68;
        int iHeadPos = ByteUtil.byte_array_locate(rcv_buf, index_rcv_com_transmit_len + 1, fbyte_head);


/*        byte[] address = Arrays.copyOfRange(rcv_buf, index_hub_addr_start,
                index_hub_addr_start + fi_hub_addr_byte_len);
        String hexAddress = CharacterUtil.bytesToHexStringlowHigh(address);
        Date date = new Date();
        HepServerCfg.heartManager.put(hexAddress, date.getTime() / 1000);

        int iHeadPos = byte_array_locate(rcv_buf,
                index_rcv_com_transmit_len + 1, fbyte_head); */
        byte fbyte_enum_cool_meter = (byte) 0x10;
        byte fbyte_enum_hot_meter = (byte) 0x50;
        if (iHeadPos < 0) {
            IRespMessageDecode decode = getDecoder(CommandCode.HUB_TYPE_ELECMETER);
            iMessage = decode.getRespMessageObj(rcv_buf, MsgConst.MSG_TYPE_RESP);
            return iMessage;

        } else if (fbyte_enum_cool_meter == rcv_buf[iHeadPos + 1]
                || fbyte_enum_hot_meter == rcv_buf[iHeadPos + 1]) {
            ;
        } else {
            if (iHeadPos == 19) {
                if (DecodeWattHourProtocol.highPowerMeterCheck(rcv_buf, iHeadPos + 1)) {
                    DecodeWattHourProtocol.checkBytesValid(rcv_buf);

                    IRespMessageDecode decode = getDecoder(CommandCode.HUB_TYPE_HIGHMETER);
                    iMessage = decode.getRespMessageObj(rcv_buf, MsgConst.MSG_TYPE_RESP);

                    //CommonRespUtils.highPowerRespProcess(rcv_buf);
                    return iMessage;
                    //beak;
                }
            }


            IRespMessageDecode decode = getDecoder(CommandCode.HUB_TYPE_ELECMETER);
            iMessage = decode.getRespMessageObj(rcv_buf, MsgConst.MSG_TYPE_RESP);
            return iMessage;
/*            rcv_ammeter_proc(rcv_buf, data);
            String hubAddr = StrUtil.getStr(data.get("hubAddr"));
            String meterAddr = StrUtil.getStr(data.get("meterAddr"));
            byte seq = (byte) data.get("seq");
            String key = hubAddr + "_" + meterAddr + "_" + seq;
            data.remove("seq");
            data.remove("hubAddr");
            data.remove("meterAddr");
            data.remove("type");
            data.put("集中器地址", hubAddr);
            data.put("表地址", meterAddr);
            DataHandle.saveDatatodb(rcv_buf, "电表请求响应2", null, seq, hubAddr,
                    meterAddr, data);

            ReqRspUtil.setResponse(key, data);

            break;*/
        }
        byte index_com_transmit_dt1 = 15 + 1; // 16;
        byte index_com_transmit_da0 = 13 + 1;

        byte[] ar_rcv_com_da0da1_p0 = {0, 0};
        byte[] ar_rcv_com_dt1dt2_f1 = {1, 0};
        /* 转发公共参数检查 dadt-f1p0，转发 */
        if (ByteUtil.bytesCompare(rcv_buf, index_com_transmit_da0,
                ar_rcv_com_da0da1_p0, 0, ar_rcv_com_da0da1_p0.length)
                && ByteUtil.bytesCompare(rcv_buf, index_com_transmit_dt1,
                ar_rcv_com_dt1dt2_f1, 0,
                ar_rcv_com_da0da1_p0.length)) {

            int trans_len = Integer.parseInt(CharacterUtil
                            .bytesToHexString(rcv_buf[index_rcv_com_transmit_len]),
                    16);

            if (trans_len < 0x10) {
                log.error("数据转发响应长度不够");
            }


            // return msg
/*            data.put("seq", rcv_buf[index_frame_seq]);
            data.put("hubAddr", CRCEncode.bytes2str(Arrays
                    .copyOfRange(rcv_buf, index_hub_addr_start,
                            index_hub_addr_start + 5), false, 0));*/
            byte index_valve_addr = (byte) (iHeadPos + index_rcv_offset_meter_addr_start);
/*            data.put("meterAddr", CRCEncode.bytes2str(Arrays.copyOfRange(
                    rcv_buf, index_valve_addr, index_valve_addr + 7),
                    false, 0));*/

            /* 表返回结果 */

            byte index_rcv_meter_result_addr = (byte) (index_valve_addr + 7);

            // (byte)0x81,/*read ack：0x81-查询响应，0xC1-异常*/ 0x15,/*data len*/
            // 0x06,(byte)0x31,/*cmd*/ 0x01,/*表数据帧序号*/
            switch (rcv_buf[index_rcv_meter_result_addr]) {
                // 表查询响应
                case fbyte_enum_ctrl_type_read_resp_succ: {

                    int iMeterTypePos = iHeadPos
                            + index_trans_head_offset_meter_type;
                    /* 用量查询响应 */
                    int iDataLenPos = iHeadPos
                            + index_trans_head_offset_meter_data_len;
                    int iMeterAppData = iDataLenPos + 1;

                    int iMeterAppDataLen = rcv_buf[iDataLenPos];
                    iMeterAppDataLen = iMeterAppDataLen & 0xff;

                    // byte meter_diser_cmd_data[] = Arrays.copyOfRange(rcv_buf,
                    // iDataLenPos + 1, iDataLenPos + 4
                    // + (int) (char) rcv_buf[iDataLenPos]);
                    //
                    // byte meter_cmd_data[] = Arrays.copyOfRange(rcv_buf,
                    // iDataLenPos + 4, iDataLenPos + 4
                    // + (int) (char) rcv_buf[iDataLenPos]);

                    byte meter_diser_cmd_data[] = Arrays
                            .copyOfRange(rcv_buf, iDataLenPos + 1, iDataLenPos
                                    + 4 + iMeterAppDataLen);

                    byte meter_cmd_data[] = Arrays
                            .copyOfRange(rcv_buf, iDataLenPos + 4, iDataLenPos
                                    + 4 + iMeterAppDataLen);

                    // DI，序号ser为必备内容
                    if (iMeterAppDataLen < 3) {
                        break;
                    }

                    // 冷水抄表
                    if (rcv_buf[iDataLenPos + 1] == fbyte_cmd_read_amount_data_cmd1
                            && rcv_buf[iDataLenPos + 2] == fbyte_cmd_read_amount_data_cmd2) {

                        if (iMeterAppDataLen != fbyte_len_read_amount_resp_data_len) {
                            break;
                        }
                        IRespMessageDecode decode = getDecoder(CommandCode.HUB_TYPE_COLDMETER);
                        iMessage = decode.getRespMessageObj(rcv_buf, MsgConst.MSG_TYPE_RESP);
                        return iMessage;
                        /*rcv_read_amount_resp_proc(meter_cmd_data,
                                rcv_buf[iHeadPos
                                        + index_trans_head_offset_meter_type],
                                data);*/
                    }
                    // 冷水查充值总量
                    else if (rcv_buf[iDataLenPos + 1] == fbyte_cmd_recharge_amount_cmd1
                            && rcv_buf[iDataLenPos + 2] == fbyte_cmd_recharge_amount_cmd2) {

                        IRespMessageDecode decode = getDecoder(CommandCode.HUB_TYPE_COLDMETER);
                        iMessage = decode.getRespMessageObj(rcv_buf, MsgConst.MSG_TYPE_RESP);
                        return iMessage;

                    }
                    // 热水状态查询
                    else if (rcv_buf[iDataLenPos] == (fbyte_len_hot_valve_state_query_resp_data_len + 6)
                            && rcv_buf[iMeterTypePos] == fbyte_enum_hot_meter
                            && rcv_buf[iDataLenPos + 1] == fbyte_cmd_hot_valve_state_query_cmd1
                            && rcv_buf[iDataLenPos + 2] == fbyte_cmd_hot_valve_state_query_cmd2) {


                        IRespMessageDecode decode = getDecoder(CommandCode.HUB_TYPE_HOTMETER);
                        iMessage = decode.getRespMessageObj(rcv_buf, MsgConst.MSG_TYPE_RESP);
                        return iMessage;

                    }
                    // 指定热水用户查询
                    else if (rcv_buf[iDataLenPos + 1] == fbyte_cmd_hot_valve_spec_user_query_data_cmd1
                            && rcv_buf[iDataLenPos + 2] == fbyte_cmd_hot_valve_spec_user_query_data_cmd2) {

                        IRespMessageDecode decode = getDecoder(CommandCode.HUB_TYPE_HOTMETER);
                        iMessage = decode.getRespMessageObj(rcv_buf, MsgConst.MSG_TYPE_RESP);
                        return iMessage;

                        //rcv_hot_spec_user_query_resp_cmd_data_proc(
                        //        meter_diser_cmd_data, data);
                    }
                    // 批量热水用户查询
                    else if (rcv_buf[iMeterTypePos] == fbyte_enum_hot_meter
                            && rcv_buf[iDataLenPos + 1] == fbyte_cmd_hot_valve_user_query_data_cmd1
                            && rcv_buf[iDataLenPos + 2] == fbyte_cmd_hot_valve_user_query_data_cmd2) {

                        IRespMessageDecode decode = getDecoder(CommandCode.HUB_TYPE_HOTMETER);
                        iMessage = decode.getRespMessageObj(rcv_buf, MsgConst.MSG_TYPE_RESP);
                        return iMessage;
                        //rcv_hot_valve_user_query_resp_cmd_data_proc(
                        //        meter_diser_cmd_data, data);
                        // 用户总数查询
                    } else if (rcv_buf[iMeterTypePos] == fbyte_enum_hot_meter
                            && rcv_buf[iDataLenPos + 1] == 0x07
                            && rcv_buf[iDataLenPos + 2] == 0x31) {

                        IRespMessageDecode decode = getDecoder(CommandCode.HUB_TYPE_HOTMETER);
                        iMessage = decode.getRespMessageObj(rcv_buf, MsgConst.MSG_TYPE_RESP);
                        return iMessage;
                        //rcv_hot_user_num_query_resp_cmd_data_proc(
                        //        meter_diser_cmd_data, data);
                    }
                    // 存储块号
                    else if (rcv_buf[iMeterTypePos] == fbyte_enum_hot_meter
                            && rcv_buf[iDataLenPos + 1] == 0x0D
                            && rcv_buf[iDataLenPos + 2] == 0x31) {

                        IRespMessageDecode decode = getDecoder(CommandCode.HUB_TYPE_HOTMETER);
                        iMessage = decode.getRespMessageObj(rcv_buf, MsgConst.MSG_TYPE_RESP);
                        return iMessage;
                        //rcv_hot_user_storage_block_query_resp_cmd_data_proc(
                        //        meter_diser_cmd_data, data)
                        // 热水单价
                    } else if (rcv_buf[iMeterTypePos] == fbyte_enum_hot_meter
                            && rcv_buf[iDataLenPos + 1] == 0x04
                            && rcv_buf[iDataLenPos + 2] == 0x31) {

                        IRespMessageDecode decode = getDecoder(CommandCode.HUB_TYPE_HOTMETER);
                        iMessage = decode.getRespMessageObj(rcv_buf, MsgConst.MSG_TYPE_RESP);
                        return iMessage;
                        //rcv_hot_price_query_resp_cmd_data_proc(
                        //        meter_diser_cmd_data, data);
                    }
                    // 消费记录总数查询
                    else if (rcv_buf[iMeterTypePos] == fbyte_enum_hot_meter
                            && rcv_buf[iDataLenPos + 1] == 0x0A
                            && rcv_buf[iDataLenPos + 2] == 0x31) {

                        IRespMessageDecode decode = getDecoder(CommandCode.HUB_TYPE_HOTMETER);
                        iMessage = decode.getRespMessageObj(rcv_buf, MsgConst.MSG_TYPE_RESP);
                        return iMessage;
                        //rcv_hot_use_record_num_query_resp_cmd_data_proc(
                        //        meter_diser_cmd_data, data);
                    }
                    // 热水使用记录查询
                    else if (rcv_buf[iMeterTypePos] == fbyte_enum_hot_meter
                            && rcv_buf[iDataLenPos + 1] == fbyte_cmd_hot_valve_use_record_query_data_cmd1
                            && rcv_buf[iDataLenPos + 2] == fbyte_cmd_hot_valve_use_record_query_data_cmd2) {
                        int appDataLen = Integer.parseInt(CharacterUtil
                                .bytesToHexString(rcv_buf[iDataLenPos]), 16) - 3;

                        IRespMessageDecode decode = getDecoder(CommandCode.HUB_TYPE_HOTMETER);
                        iMessage = decode.getRespMessageObj(rcv_buf, MsgConst.MSG_TYPE_RESP);
                        return iMessage;
                        //rcv_hot_valve_query_use_record_resp_cmd_data_proc(
                        //        meter_diser_cmd_data, data);

                    }

                    //data.put("type", "查询响应");
                    //data.put("result", Constant.SUCCESS);
                    //break;
                }
                // 表控制响应
                case fbyte_enum_ctrl_type_write_resp_succ: {
                    int iDataLenPos = iHeadPos
                            + index_trans_head_offset_meter_data_len;

                    int iMeterTypePos = iHeadPos
                            + index_trans_head_offset_meter_type;

                    byte meter_app_data[] = Arrays.copyOfRange(rcv_buf,
                            iDataLenPos + 1, iDataLenPos + 1
                                    + (int) (char) rcv_buf[iDataLenPos]);


                    /* 开关阀响应 */
                    if (rcv_buf[iDataLenPos] == fbyte_len_valve_ctrl_resp_data_len

                            && rcv_buf[iDataLenPos + 1] == fbyte_cmd_valve_ctrl_data_cmd1
                            && rcv_buf[iDataLenPos + 2] == fbyte_cmd_valve_ctrl_data_cmd2) {
                        Map<String, Object> data = Maps.newHashMap();
                        data.put("type", "控制响应");
                        data.put("cmd", "开阀");
                        data.put("result", ResultConst.SUCCESS);
                        iMessage = getMixCommonResp(rcv_buf, hexAddress, data);
                        return iMessage;
                        // rcv_valve_ctrl_resp_proc(rcv_buf, data);
                    }
                    // 冷水充值响应
                    else if (rcv_buf[iDataLenPos] == fbyte_len_recharge_resp_data_len
                            && rcv_buf[iDataLenPos + 1] == fbyte_cmd_recharge_data_cmd1
                            && rcv_buf[iDataLenPos + 2] == fbyte_cmd_recharge_data_cmd2) {
                        //data.put("cmd", "冷水充值");
                        // rcv_recharge_resp_proc(rcv_buf, data);
                        IRespMessageDecode decode = getDecoder(CommandCode.HUB_TYPE_COLDMETER);
                        iMessage = decode.getRespMessageObj(rcv_buf, MsgConst.MSG_TYPE_RESP);
                        return iMessage;
                    }
                    // 热水开阀
                    else if (rcv_buf[iDataLenPos] == fbyte_len_hot_valve_state_open_resp_data_len
                            && rcv_buf[iDataLenPos + 1] == fbyte_cmd_hot_valve_open_data_cmd1
                            && rcv_buf[iDataLenPos + 2] == fbyte_cmd_hot_valve_open_data_cmd2) {
                        //data.put("cmd", "热水开阀");
                        // rcv_recharge_resp_proc(rcv_buf, data);
                        IRespMessageDecode decode = getDecoder(CommandCode.HUB_TYPE_HOTMETER);
                        iMessage = decode.getRespMessageObj(rcv_buf, MsgConst.MSG_TYPE_RESP);
                        return iMessage;
                    }
                    // 热水关阀
                    else if (rcv_buf[iDataLenPos] == (byte)0x05
                            && rcv_buf[iDataLenPos + 1] == fbyte_cmd_hot_valve_close_data_cmd1
                            && rcv_buf[iDataLenPos + 2] == fbyte_cmd_hot_valve_close_data_cmd2) {
                        //data.put("cmd", "热水关阀");
                        // rcv_recharge_resp_proc(rcv_buf, data);
                        IRespMessageDecode decode = getDecoder(CommandCode.HUB_TYPE_HOTMETER);
                        iMessage = decode.getRespMessageObj(rcv_buf, MsgConst.MSG_TYPE_RESP);
                        return iMessage;
                    }
                    // 添加用户
                    else if (rcv_buf[iDataLenPos] == fbyte_len_hot_valve_user_add_resp_data_len
                            && rcv_buf[iDataLenPos + 1] == fbyte_cmd_hot_valve_user_add_data_cmd1
                            && rcv_buf[iDataLenPos + 2] == fbyte_cmd_hot_valve_user_add_data_cmd2) {
                        //data.put("cmd", "添加用户");
                        // rcv_recharge_resp_proc(rcv_buf, data);
                        IRespMessageDecode decode = getDecoder(CommandCode.HUB_TYPE_HOTMETER);
                        iMessage = decode.getRespMessageObj(rcv_buf, MsgConst.MSG_TYPE_RESP);
                        return iMessage;
                    }
                    // 修改用户
                    else if (rcv_buf[iDataLenPos] == fbyte_len_hot_valve_user_update_resp_data_len
                            && rcv_buf[iDataLenPos + 1] == fbyte_cmd_hot_valve_user_update_data_cmd1
                            && rcv_buf[iDataLenPos + 2] == fbyte_cmd_hot_valve_user_update_data_cmd2) {
                        //data.put("cmd", "修改用户");
                        // rcv_recharge_resp_proc(rcv_buf, data);
                        IRespMessageDecode decode = getDecoder(CommandCode.HUB_TYPE_HOTMETER);
                        iMessage = decode.getRespMessageObj(rcv_buf, MsgConst.MSG_TYPE_RESP);
                        return iMessage;
                    }
                    // 删除用户
                    else if (rcv_buf[iDataLenPos] == fbyte_len_hot_valve_user_delete_resp_data_len
                            && rcv_buf[iDataLenPos + 1] == fbyte_cmd_hot_valve_user_delete_data_cmd1
                            && rcv_buf[iDataLenPos + 2] == fbyte_cmd_hot_valve_user_delete_data_cmd2) {
                        //data.put("cmd", "添加用户");
                        // rcv_recharge_resp_proc(rcv_buf, data);
                        IRespMessageDecode decode = getDecoder(CommandCode.HUB_TYPE_HOTMETER);
                        iMessage = decode.getRespMessageObj(rcv_buf, MsgConst.MSG_TYPE_RESP);
                        return iMessage;
                    }
                    // 设置单价
                    else if (rcv_buf[iDataLenPos] == (byte)0x03
                            && rcv_buf[iDataLenPos + 1] == fbyte_cmd_hot_valve_price_set_data_cmd1
                            && rcv_buf[iDataLenPos + 2] == fbyte_cmd_hot_valve_price_set_data_cmd2) {
                        //data.put("cmd", "设置单价");
                        // rcv_recharge_resp_proc(rcv_buf, data);
                        IRespMessageDecode decode = getDecoder(CommandCode.HUB_TYPE_HOTMETER);
                        iMessage = decode.getRespMessageObj(rcv_buf, MsgConst.MSG_TYPE_RESP);
                        return iMessage;
                    }
                    // 广播设置时间
                    else if (rcv_buf[iDataLenPos] == fbyte_len_hot_valve_broadcast_set_time_resp_data_len
                            && rcv_buf[iDataLenPos + 1] == fbyte_cmd_hot_valve_broadcast_set_time_data_cmd1
                            && rcv_buf[iDataLenPos + 2] == fbyte_cmd_hot_valve_broadcast_set_time_data_cmd2) {
                        //data.put("cmd", "广播设置时间");
                        // rcv_recharge_resp_proc(rcv_buf, data);
                        IRespMessageDecode decode = getDecoder(CommandCode.HUB_TYPE_HOTMETER);
                        iMessage = decode.getRespMessageObj(rcv_buf, MsgConst.MSG_TYPE_RESP);
                        return iMessage;
                    }

                    else if (rcv_buf[iMeterTypePos] == fbyte_enum_hot_meter
                            && rcv_buf[iDataLenPos + 1] == (byte)0x0B
                            && rcv_buf[iDataLenPos + 2] == (byte)0xE1) {
                        IRespMessageDecode decode = getDecoder(CommandCode.HUB_TYPE_HOTMETER);
                        iMessage = decode.getRespMessageObj(rcv_buf, MsgConst.MSG_TYPE_RESP);
                        return iMessage;

                        //rcv_hot_user_storage_block_query_resp_cmd_data_proc(
                        //        meter_diser_cmd_data, data);
                    }

                    //查询存储块
                    else if (rcv_buf[iMeterTypePos] == fbyte_enum_hot_meter
                            && rcv_buf[iDataLenPos + 1] == (byte)0x0D
                            && rcv_buf[iDataLenPos + 2] == (byte)0x31) {
                        IRespMessageDecode decode = getDecoder(CommandCode.HUB_TYPE_HOTMETER);
                        iMessage = decode.getRespMessageObj(rcv_buf, MsgConst.MSG_TYPE_RESP);
                        return iMessage;

                        //rcv_hot_user_storage_block_query_resp_cmd_data_proc(
                        //        meter_diser_cmd_data, data);
                    }
                    else if (rcv_buf[iDataLenPos] == 0x03
                            && rcv_buf[iDataLenPos + 1] == (byte) 0x15
                            && rcv_buf[iDataLenPos + 2] == (byte) 0xA0) {
                        Map<String, Object> data = Maps.newHashMap();
                        data.put("hubAddress", hexAddress);
                        data.put("cmd", "时间同步");
                        data.put("type", "控制响应");
                        data.put("result", ResultConst.SUCCESS);
                        iMessage = getMixCommonResp(rcv_buf, hexAddress, data);
                        return iMessage;
                    }

                    break;
                }
                // 查询失败
                case fbyte_enum_ctrl_type_read_resp_failed: {
                    Map<String, Object> data = Maps.newHashMap();
                    data.put("hubAddress", hexAddress);
                    data.put("type", "查询响应");
                    data.put("result", ResultConst.FAIL);
                    iMessage = getMixCommonResp(rcv_buf, hexAddress, data);
                    return iMessage;
                }
                // 控制失败
                case fbyte_enum_ctrl_type_write_resp_failed: {
                    Map<String, Object> data = Maps.newHashMap();
                    data.put("hubAddress", hexAddress);
                    data.put("type", "控制响应");
                    data.put("result", ResultConst.FAIL);
                    iMessage = getMixCommonResp(rcv_buf, hexAddress, data);
                    return iMessage;

                }
                default: {
                    break;
                }
            }//switch-end
        } else {

        }

        return null;
    }


    public static IMessage getMixCommonResp(byte[] bytes,
                                            String hubAddrResp,
                                            Map<String, Object> data) {
        byte seq = bytes[13];
        MsgCommon<Map> mapMsgCommon = null;
        mapMsgCommon = MsgCommon.MsgCommonBuilder.aMsgCommon()
                .withMsgType(MsgConst.MSG_TYPE_RESP)
                .withMsgAddr(hubAddrResp)
                //.withMsgSubAddr(meterAddrResp)
                .withMsgSeq(seq)
                .build();
        mapMsgCommon.setRespStr(StrUtil.splitSpace(CharacterUtil.bytesToHexString(bytes)));
        //使用这种方式
        mapMsgCommon.getResp().add(data);
        return mapMsgCommon;
    }

    @Override
    public void splicingPackage(ByteBuf bufferIn, List<Object> out) {
        //log.info(">>> splicingPackage, readableBytes: {}", bufferIn.readableBytes());

        if (bufferIn.readableBytes() < 5) {
            return;
        }

        // 读取开始的位置
        int beginIndex = bufferIn.readerIndex();

        // 开始头
        byte header = bufferIn.readByte();
        // 第一组数据长度
        byte len1byte1 = bufferIn.readByte();
        byte len1byte2 = bufferIn.readByte();

        // 第二组数据长度
        byte len2byte1 = bufferIn.readByte();
        byte len2byte2 = bufferIn.readByte();

        // 根据业务定义计算出数据长度
        int a = Integer.parseInt(CharacterUtil.bytesToHexString(len1byte2), 16);
        int b = Integer.parseInt(CharacterUtil.bytesToHexString(len1byte1), 16);
        int dataLength = (a * 256 + b) >> 2;
        dataLength = dataLength + 3;
        //log.info(">>> splicingPackage, beginIndex: {}, readableBytes: {}, dataLength: {}",
        //        beginIndex, bufferIn.readableBytes(), dataLength);

        // 如果小于数据长度 充值读取索引位置
        if (bufferIn.readableBytes() < dataLength) {
            bufferIn.readerIndex(beginIndex);
            return;
        }

        bufferIn.readerIndex(beginIndex + dataLength + 5);
        ByteBuf otherByteBufRef = bufferIn.slice(beginIndex, dataLength + 5);
        otherByteBufRef.retain();
        out.add(otherByteBufRef);
    }

    @Override
    public boolean checkValid(byte[] bytes) {
        byte crc = calc_sum_crc(bytes, 6, bytes.length - 3);
        int a = Integer.parseInt(CharacterUtil.bytesToHexString(bytes[2]), 16);
        int b = Integer.parseInt(CharacterUtil.bytesToHexString(bytes[1]), 16);
        int len = (a * 256 + b) >> 2;
        if (bytes[0] == 0x68
                && len + 8 == bytes.length
                && bytes[5] == 0x68
                && bytes[bytes.length - 1] == 0x16
                && bytes[bytes.length - 2] == crc) {
            return true;// 校验通过
        }

        log.error(">>> MessageRespAnalysis, checkValid false, bytes: {}",
                StrUtil.splitSpace(CharacterUtil.bytesToHexString(bytes)));
        return false;
    }

    public static void setHubAddr2Dec(byte[] addrA2, int start, int end) {
        int intA2 = ((addrA2[start + 1] & 0xFF) << 4) | (addrA2[start] & 0xFF);

        String strHexA2 = String.format("%06d", intA2);
        strHexA2 = strHexA2.toUpperCase();
        addrA2[start] = (byte) ((charToByte(strHexA2.charAt(4)) << 4) | (charToByte(strHexA2.charAt(5))));
        addrA2[start + 1] = (byte) ((charToByte(strHexA2.charAt(2)) << 4) | (charToByte(strHexA2.charAt(3))));
        addrA2[start + 2] = (byte) ((charToByte(strHexA2.charAt(0)) << 4) | (charToByte(strHexA2.charAt(1))));
    }
}
