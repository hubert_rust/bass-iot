package com.smart.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/*
 * @ program: register-center
 * @ description: 注册信息返回结果
 * @ author: admin
 * @ create: 2019-03-27 09:30
 **/
public class ReturnEntity {
    boolean flag;
    String returnCode;
    String message;
    String returnMsg;
    List<Object> hubs = new ArrayList<>();
    List<Object> datas = new ArrayList<>();

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public List<Object> getHubs() {
        return hubs;
    }

    public void setHubs(List<Object> hubs) {
        this.hubs = hubs;
    }

    public List<Object> getDatas() {
        return datas;
    }

    public void setDatas(List<Object> datas) {
        this.datas = datas;
    }
}
