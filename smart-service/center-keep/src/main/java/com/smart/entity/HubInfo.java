package com.smart.entity;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-05 18:17
 **/
public class HubInfo {
    private String pluginId;
    private String hubAddress;
    private String hubType;
    private String hubMode;
    private String hubIp;
    private int commPort;
    private int bps;
    private String hubParam;

    public String getPluginId() { return pluginId; }
    public void setPluginId(String pluginId) { this.pluginId = pluginId; }
    public String getHubAddress() { return hubAddress; }
    public void setHubAddress(String hubAddress) { this.hubAddress = hubAddress; }
    public String getHubType() { return hubType; }
    public void setHubType(String hubType) { this.hubType = hubType; }
    public String getHubIp() { return hubIp; }
    public void setHubIp(String hubIp) { this.hubIp = hubIp; }
    public int getCommPort() { return commPort; }
    public void setCommPort(int commPort) { this.commPort = commPort; }
    public int getBps() { return bps; }
    public void setBps(int bps) { this.bps = bps; }

    public String getHubMode() {
        return hubMode;
    }

    public void setHubMode(String hubMode) {
        this.hubMode = hubMode;
    }

    public String getHubParam() { return hubParam; }
    public void setHubParam(String hubParam) { this.hubParam = hubParam; }
}
