package com.smart.framework.utils;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.hash.Hashing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.math.BigDecimal;
import java.security.SecureRandom;
import java.text.DecimalFormat;


public class AESUtil {

    private  static final Logger log = LoggerFactory.getLogger(AESUtil.class);
    public  static  final  String aesKey ="$2a$10$WrZwYxHIUKH08qJHFvCh7O/Ve/pl2LOtfiuJ0Vn2r7ufmpjII16a6";
    public  static  void main(String[] args){
        //String aesKey= "36c82834-3fe4-4305-b6e0-39d52e5113d4";

        String password="123456";
        String resPass=new String(encryptAES(password,aesKey));
       // log.debug("aesKey={} 加密的结果： {} ",resPass);
       // System.out.println("aesket:"+aesKey);
        System.out.println("加密的后的结果:"+resPass);

        String srcPass=new String(decryptAES(encryptAES(password,aesKey),aesKey));
        log.debug("解密的结果： {} ",srcPass);
        //System.out.println("解密的结果:"+srcPass);





      /*  //密码加密
        BCryptPasswordEncoder passwordEncoder=new BCryptPasswordEncoder();
        String password = passwordEncoder.encode(seller.getPassword());//加密
        seller.setPassword(password);*/
    }

 /*   public User create(S user){
        //进行加密
        BCryptPasswordEncoder encoder =new BCryptPasswordEncoder();
        sysUser.setPassword(encoder.encode(user.getRawPassword().trim()));
        userDao.create(user);
        return sysUser;*/
    /**
     * AES加密
     *
     * @param content   需要加密的内容
     * @param key  加密密钥
     * @return
     */
    public static byte[] encryptAES(String content, String key) {
        try {
            KeyGenerator kgen = KeyGenerator.getInstance("AES");
            kgen.init(128, new SecureRandom(key.getBytes()));
            SecretKey secretKey = kgen.generateKey();
            byte[] enCodeFormat = secretKey.getEncoded();
            SecretKeySpec keySpec = new SecretKeySpec(enCodeFormat, "AES");
            Cipher cipher = Cipher.getInstance("AES");// 创建密码器
            byte[] byteContent = content.getBytes("utf-8");
            cipher.init(Cipher.ENCRYPT_MODE, keySpec);// 初始化
            byte[] result = cipher.doFinal(byteContent);
            return result; // 加密
        }catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * AES解密
     * @param content  待解密内容
     * @param key 解密密钥
     * @return
     */
    public static byte[] decryptAES(byte[] content, String key) {
        try {
            KeyGenerator kgen = KeyGenerator.getInstance("AES");
            kgen.init(128, new SecureRandom(key.getBytes()));
            SecretKey secretKey = kgen.generateKey();
            byte[] enCodeFormat = secretKey.getEncoded();
            SecretKeySpec keySpec = new SecretKeySpec(enCodeFormat, "AES");
            Cipher cipher = Cipher.getInstance("AES");// 创建密码器
            cipher.init(Cipher.DECRYPT_MODE, keySpec);// 初始化
            byte[] result = cipher.doFinal(content);
            return result; // 加密
        }catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     * TODO: encrypt alg ?
     *
     * @param balance
     * @param account_code
     * @return
     */
    public static String encryptBalance(BigDecimal balance, String account_code) {
        DecimalFormat df = new DecimalFormat();

        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);
        df.setGroupingSize(10000);
        String balanceStr = df.format(balance);

        return MD5(account_code + "iye" + balanceStr);
    }
    public static String MD5(String input) {
        //checkArgument(isNotBlank(input));
        Preconditions.checkArgument(!StringUtils.isEmpty(input), "加密失败");

        return Hashing.md5().newHasher().putString(input, Charsets.UTF_8).hash().toString();
    }
}
