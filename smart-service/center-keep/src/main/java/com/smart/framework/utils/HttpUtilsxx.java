package com.smart.framework.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.UUID;

public class HttpUtilsxx {
    public static String getUuidString() {
        String ret = UUID.randomUUID().toString().toUpperCase().replaceAll("-", "");
        return ret;
    }

    public static Object commonSendHttpPost(String url, String strQuery) throws Exception{
        HttpURLConnection httpURLConnection = buildHttpConnection(url);
        String ret = sendHttpPost(strQuery, httpURLConnection);
        return ret;
    }


    public static HttpURLConnection buildHttpConnection(String sendUrl) throws Exception {
        URL url=new URL(sendUrl);
        HttpURLConnection httpURLConnection=(HttpURLConnection) url.openConnection();
        httpURLConnection.setConnectTimeout(3000);
        httpURLConnection.setReadTimeout(3000);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(true);

        httpURLConnection.setRequestProperty("Content-Type", "text/plain; charset=\"utf8\"");
        httpURLConnection.setRequestMethod("POST");

        httpURLConnection.setRequestProperty("accept", "*/*");
        httpURLConnection.setRequestProperty("connection", "Keep-Alive");
        httpURLConnection.setRequestProperty("user-agent",
                "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");

        return httpURLConnection;
    }

    public static String sendHttpPost(Object sendParam, HttpURLConnection connection) throws Exception {
        PrintWriter out=new PrintWriter(connection.getOutputStream());
        out.print(sendParam);
        out.flush();

        InputStream inputStream=connection.getInputStream();
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder=new StringBuilder();
        String line=null;
        while ((line=bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        bufferedReader.close();

        connection.disconnect();
        return stringBuilder.toString();
    }
}
