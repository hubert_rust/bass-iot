package com.smart.constants;

/*
 * @ program: register-center
 * @ description: common const
 * @ author: admin
 * @ create: 2019-03-28 14:54
 **/
public class CommonConst {
    public final static String PUSH_CONFIG_DATA_HARDWARE = "hardware";
    public final static String PUSH_CONFIG_DATA_PLUGIN = "plugin";
}
