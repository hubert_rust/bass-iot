package com.smart.config;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.smart.entity.PluginEventEntity;
import com.smart.framework.config.ZookeeperListenCallBack;
import com.smart.framework.utils.HttpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

@Component
@Configuration
public class InitConfig implements CommandLineRunner {
    public static Map<String, Object> OUT_SERVICE_NOTIFY = new ConcurrentHashMap<>();
    public static Queue<NotifyOuterEntity> NOTIFY_QUQUE = new ConcurrentLinkedQueue<>();
    public volatile static boolean stop = false;

    public static int BATCH_NOTIFY_NUM = 20;

    public static NotifyLockerEventThread notifyLockerEventThread;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    private ZookeeperListenCallBack listenCallBack;

    @Override
    public void run(String... args) throws Exception {

        listenCallBack.startup(new KeeperEventCallback());

        loadOuterService();

        notifyLockerEventThread = new NotifyLockerEventThread();
        notifyLockerEventThread.start();
    }

    private void loadOuterService() {
        List<Map<String, Object>> list = jdbcTemplate.queryForList("select * from  tb_outer_service_manage");
        list.forEach(val -> {
            String key = val.get("app_key").toString();
            OUT_SERVICE_NOTIFY.put(key, val);
        });
    }

    public static class NotifyOuterEntity {
        private String targetType;
        private String targetUrl;
        private String pluginType;
        private String pluginId;
        private String eventType;
        private String fireTime;
        private String hubAddress;
        private String subAddress;
        private String events;

        public String getTargetType() {
            return targetType;
        }

        public void setTargetType(String targetType) {
            this.targetType = targetType;
        }

        public String getTargetUrl() {
            return targetUrl;
        }

        public void setTargetUrl(String targetUrl) {
            this.targetUrl = targetUrl;
        }

        public String getPluginType() {
            return pluginType;
        }

        public void setPluginType(String pluginType) {
            this.pluginType = pluginType;
        }

        public String getPluginId() {
            return pluginId;
        }

        public void setPluginId(String pluginId) {
            this.pluginId = pluginId;
        }

        public String getEventType() {
            return eventType;
        }

        public void setEventType(String eventType) {
            this.eventType = eventType;
        }

        public String getFireTime() {
            return fireTime;
        }

        public void setFireTime(String fireTime) {
            this.fireTime = fireTime;
        }

        public String getHubAddress() {
            return hubAddress;
        }

        public void setHubAddress(String hubAddress) {
            this.hubAddress = hubAddress;
        }

        public String getSubAddress() {
            return subAddress;
        }

        public void setSubAddress(String subAddress) {
            this.subAddress = subAddress;
        }

        public String getEvents() {
            return events;
        }

        public void setEvents(String events) {
            this.events = events;
        }
    }

    public static void putEvent(PluginEventEntity eventEntity) {
        NotifyOuterEntity outerEntity = new NotifyOuterEntity();
        //
        Map map = (Map) OUT_SERVICE_NOTIFY.get(eventEntity.getPluginType());
        String url = map.get("app_notify").toString();
        outerEntity.setTargetUrl(url);
        outerEntity.setEventType(eventEntity.getEventType());
        outerEntity.setFireTime(eventEntity.getFireTime());
        outerEntity.setHubAddress(eventEntity.getConAddress());
        outerEntity.setSubAddress(eventEntity.getSubAddress());
        outerEntity.setPluginId(eventEntity.getPluginId());
        outerEntity.setEvents(eventEntity.getEvents());
        NOTIFY_QUQUE.offer(outerEntity);
    }

    public class NotifyLockerEventThread extends Thread {
        @Override
        public void run() {
            String targetUrl = "";
            while (!stop) {
                try {
                    TimeUnit.MILLISECONDS.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //批量发送
                List<NotifyOuterEntity> list = null;
                try {
                    for (int i = 0; i < BATCH_NOTIFY_NUM; i++) {
                        NotifyOuterEntity outerEntity = NOTIFY_QUQUE.poll();
                        if (Objects.isNull(outerEntity)) {
                            continue;
                        }
                        if (Objects.isNull(list)) {
                            list = Lists.newArrayList();
                        }
                        list.add(outerEntity);
                    }
                    if (Objects.isNull(list) || list.size() <= 0) {
                        continue;
                    }
                } catch (Exception e1) {
                    e1.printStackTrace();
                }


                try {
                    targetUrl = list.get(0).getTargetUrl();
                    HttpUtils.commonSendHttpPost(targetUrl,
                            JSONObject.toJSONString(list),
                            3000);
                } catch (Exception e) {
                    e.printStackTrace();
                    //重试一次
                    try {

                        HttpUtils.commonSendHttpPost(targetUrl,
                                JSONObject.toJSONString(list),
                                3000);
                    } catch (Exception ex) {
                    }
                }
            }
        }
    }
}
