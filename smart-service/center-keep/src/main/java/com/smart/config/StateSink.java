package com.smart.config;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.smart.EventType;
import com.smart.common.ResponseModel;
import com.smart.common.ResponseState;
import com.smart.entity.*;
import com.smart.event.HWStateEvent;
import com.smart.monitor.MonitorDataCenter;
import com.smart.monitor.MonitorObject;
import com.smart.monitor.MonitorStateEvent;
import com.smart.monitor.MonitorType;
import com.smart.repository.*;
import net.bytebuddy.asm.Advice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;

import javax.persistence.criteria.Predicate;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static com.smart.utils.CommonUtils.mapToEntity;

/**
 * @program: 硬件插件事件上报消息
 * @description:
 * @author: admin
 * @create: 2019-05-16 15:34
 **/

@EnableBinding(IStateSink.class)
public class StateSink {
    private static final Logger log = LoggerFactory.getLogger(StateSink.class);

    @Autowired
    MonitorRepository monitorRepository;
    @Autowired
    MeterRepostory meterRepostory;
    @Autowired
    ConcentratorRepostory hubRepostory;

private String getKey(HWStateEvent v) {
    String key = "";
    if (v.getMainClass().equals(MonitorType.MONITOR_CLASS_PLUGIN)) {
        key = v.getMainClass() + "-" +v.getPluginId();
    }
    else if (v.getMainClass().equals(MonitorType.MONITOR_CLASS_HUB)) {
        key = v.getMainClass() + "-" + v.getHubAddress();
    }
    else if (v.getMainClass().equals(MonitorType.MONITOR_CLASS_METER)) {
        key = v.getMainClass() + "-" +v.getHubAddress() + "-" + v.getSubAddress();
    }
    return key;
}
    @StreamListener(IStateSink.INPUT_EVENT)
    public void process(Message<String> message) {
        if (!MonitorDataCenter.isStart()) {
            return;
        }

        try {
            String var1 = message.getPayload();
            ResponseState resp = JSONObject.parseObject(var1, ResponseState.class);
            if (Objects.isNull(resp.getDatas())) { return; }
            resp.getDatas().forEach(v -> {
                log.info(">>> StateSink, {}", v.toString());
                try {
                    if (!checkCache(v)) {
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String key = getKey(v);

                //收到消息要刷新时间
                MonitorEntity monitorObject = MonitorDataCenter.getMonitorObjectMap().get(key);
                if (Objects.isNull(monitorObject)) {
                    log.error(">>> StateSink, process, from key : {} get null", key);
                } else {
                    //刷新时间，轮训判断会用到
                    String state = (v.getState() == null && v.getMainClass().equals(MonitorType.MONITOR_CLASS_PLUGIN)) ? "ok" : v.getState();
                    //monitorObject.getMonitorEntity().setLastTime(System.currentTimeMillis());
                    MonitorStateEvent monitorStateEvent = new MonitorStateEvent();
                    monitorStateEvent.setNew(false);
                    monitorStateEvent.setObjectKey(key);
                    monitorStateEvent.setState(state);
                    MonitorDataCenter.off(monitorStateEvent);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkCache(HWStateEvent hwStateEvent)throws Exception {
        String key = getKey(hwStateEvent);

        MonitorEntity monitorEntity =  MonitorDataCenter.getMonitorObjectMap().get(key);
        if (!Objects.isNull(monitorEntity)) {
            return true;
        }
        log.error(">>> StateSink, process, from key : {} get null", key);

        MonitorStateEvent monitorStateEvent = new MonitorStateEvent();
        monitorStateEvent.setObjectKey(getKey(hwStateEvent));
        monitorStateEvent.setNew(true);
        monitorStateEvent.setState(hwStateEvent.getState());
        monitorStateEvent.setStateEvent(hwStateEvent);
        MonitorDataCenter.off(monitorStateEvent);

        return false;
    }

}

