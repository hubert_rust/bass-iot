package com.smart.config;

import com.smart.EventType;
import com.smart.entity.CronJobLogEntity;
import com.smart.entity.CronMainJobLogEntity;
import com.smart.entity.PluginEntity;
import com.smart.entity.PluginEventEntity;
import com.smart.repository.CronJobLogRepository;
import com.smart.repository.CronMainJobLogRepository;
import com.smart.repository.PluginEventRepository;
import com.smart.repository.PluginRepostory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.integration.transformer.MapToObjectTransformer;
import org.springframework.messaging.Message;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.lang.reflect.Field;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.smart.utils.CommonUtils.mapToEntity;

/**
 * @program: 硬件插件事件上报消息
 * @description:
 * @author: admin
 * @create: 2019-05-16 15:34
 **/
@EnableBinding(IEventSink.class)
public class EventSink {
    private static final Logger log = LoggerFactory.getLogger(EventSink.class);

    @Autowired
    PluginEventRepository eventRepository;
    @Autowired
    PluginRepostory pluginRepostory;

    @Autowired
    CronMainJobLogRepository mainJobLogRepository;
    @Autowired
    CronJobLogRepository jobLogRepository;

    @StreamListener(IEventSink.INPUT_EVENT)
    public void process(Message<Map<String, Object>> message) {
        Map<String, Object> map = message.getPayload();
        log.info(">>> pluginEventEntity: {}", map.toString());

        String mainEvent;
        try {
            mainEvent = map.get("mainEvent").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        try {
            switch (mainEvent) {
                case EventType.EVENT_TYPE_LOCKER_COMMON: {
                    lockerCommon(map);
                    break;
                }
                case EventType.EVENT_TYPE_MAIN_JOB_LOG: {
                    mainLog(map);
                    break;
                }
                case EventType.EVENT_TYPE_JOB_LOG: {
                    jobLog(map);
                    break;
                }
                default: {

                    log.error(">>> EventSink, process, mainEvent is invalid: {}", mainEvent);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void jobLog(Map<String, Object> map) throws Exception {
        CronJobLogEntity event = mapToEntity(map, CronJobLogEntity.class);
        event.setPluginId(map.get("pluginId").toString());
        String pluginId = event.getPluginId();

        Optional<PluginEntity> pluginInfoBaseEntity = pluginRepostory.findOne((root, query, builder) -> {
            return builder.equal(root.get("pluginId"), pluginId);
        });
        long timestamp = Long.valueOf(map.get("startTime").toString());
        event.setStartTime(Instant.ofEpochMilli(timestamp).atZone(ZoneId.systemDefault()).toLocalDateTime());
        timestamp = Long.valueOf(map.get("finishTime").toString());
        event.setFinishTime(Instant.ofEpochMilli(timestamp).atZone(ZoneId.systemDefault()).toLocalDateTime());

        event.setCreateTime(LocalDateTime.now());
        event.setRemark(map.get("remark").toString());

        event.setDcName(pluginInfoBaseEntity.get().getDcName());
        event.setDcId(pluginInfoBaseEntity.get().getDcId());
        event.setCreateTime(LocalDateTime.now());

        jobLogRepository.save(event);
    }
    private void mainLog(Map<String, Object> map) throws Exception {
        CronMainJobLogEntity event = mapToEntity(map, CronMainJobLogEntity.class);
        mainJobLogRepository.save(event);
    }

    private void lockerCommon(Map<String, Object> map) throws Exception {

        PluginEventEntity event = mapToEntity(map, PluginEventEntity.class);
        String pluginId = event.getPluginId();
        Optional<PluginEntity> pluginInfoBaseEntity = pluginRepostory.findOne((root, query, builder) -> {
            return builder.equal(root.get("pluginId"), pluginId);
        });

        PluginEventEntity eventEntity = new PluginEventEntity();
        eventEntity.setConAddress(event.getConAddress());
        eventEntity.setEventType(event.getEventType());
        eventEntity.setFireTime(event.getFireTime());
        eventEntity.setHex(event.getHex());
        eventEntity.setEvents(event.getEvents());
        eventEntity.setSubAddress(event.getSubAddress());
        eventEntity.setPluginId(event.getPluginId());
        eventEntity.setPluginType(pluginInfoBaseEntity.get().getPluginName());
        eventEntity.setDcName(pluginInfoBaseEntity.get().getDcName());
        eventEntity.setDcId(pluginInfoBaseEntity.get().getDcId());
        eventEntity.setCreateTime(LocalDateTime.now());

        eventRepository.save(eventEntity);

        InitConfig.putEvent(eventEntity);
    }

}

