package com.smart.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;

/**
 * @program: 硬件监测消息
 * @description:
 * @author: admin
 * @create: 2019-05-16 15:34
 **/
@EnableBinding(IMonitorSink.class)
public class MonitorSink {
    private static final Logger log = LoggerFactory.getLogger(MonitorSink.class);

    @StreamListener(IMonitorSink.INPUT_MONITOR)
    public void process(Message<?> message) {
        log.info(">>> MonitorSink: " +message.getPayload());

        Acknowledgment acknowledgment = message.getHeaders().get(KafkaHeaders.ACKNOWLEDGMENT, Acknowledgment.class);
        if (acknowledgment != null) {
            System.out.println("Acknowledgement provided");
            acknowledgment.acknowledge();
        }
    }
}
