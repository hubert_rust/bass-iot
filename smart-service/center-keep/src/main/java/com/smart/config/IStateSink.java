package com.smart.config;

import com.smart.MessageTopic;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-16 15:30
 **/

public interface IStateSink {

    String INPUT_EVENT = MessageTopic.MESSAGE_TOPIC_STATE;
    @Input(INPUT_EVENT)
    SubscribableChannel input();

}
