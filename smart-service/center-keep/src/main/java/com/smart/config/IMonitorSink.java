package com.smart.config;

import com.smart.MessageTopic;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-16 15:30
 **/

public interface IMonitorSink {

    String INPUT_MONITOR = MessageTopic.MESSAGE_TOPIC_MONITOR;
    @Input(INPUT_MONITOR)
    SubscribableChannel input();

/*    String INPUT0 = "input0";
    @Input(INPUT0)
    SubscribableChannel input0();*/
}
