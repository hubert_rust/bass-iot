package com.smart.monitor;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.smart.command.CommandCode;
import com.smart.common.PFConstDef;
import com.smart.common.ResponseUtils;
import com.smart.entity.MonitorEntity;
import com.smart.framework.commonpool.Task;
import com.smart.framework.utils.HttpUtils;
import com.smart.msgtpl.MessageTpl;
import com.smart.repository.MonitorRepository;
import com.smart.repository.PluginRepostory;
import com.smart.request.BassHardwardRequest;
import com.smart.toolkit.IDGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-13 16:18
 **/
@Component("MonitoryProbeTask")
public class MonitoryProbeTask extends Task {
    private static final Logger log = LoggerFactory.getLogger(MonitoryProbeTask.class);

    @Autowired
    MonitorRepository repostory;
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public void process() throws Exception {
        MonitorEntity object = (MonitorEntity) getObject();
        System.out.println(objectThreadLocal.get().toString());

        List<Map<String, Object>> listMap = jdbcTemplate.queryForList("select * from tb_hardware_service_manage");
        Optional<Map<String, Object>> funMap = listMap.stream().filter(v->{
            return v.get("service_type").toString().equals(object.getMonitorType());
        }).findAny();
        if (!funMap.isPresent()) {
            log.error(">>> MonitoryProbeTask, object: {}", object.toString());
            return;
        }
        String url = funMap.get().get("service_url").toString();
        try {

            //
            String cmdCode = "";
            if (object.getMonitorType().equals(CommandCode.HUB_TYPE_LOCKER)) {
                cmdCode = CommandCode.CMD_LOCK_QUERY_STATE;
            }
            else if (object.getMonitorType().equals(CommandCode.HUB_TYPE_HIGHMETER)) {
                cmdCode = CommandCode.HIGH_POWER_STATE;
            }
            else if (object.getMonitorType().equals(CommandCode.HUB_TYPE_COLDMETER)) {
                cmdCode = CommandCode.CMD_QUERY_METER_AMOUNT;
            }
            else if (object.getMonitorType().equals(CommandCode.HUB_TYPE_HOTMETER)) {
                cmdCode = CommandCode.CMD_QUERY_METER_STATE;
            }
            else if (object.getMonitorType().equals(CommandCode.HUB_TYPE_ELECMETER)) {
                cmdCode = CommandCode.CMD_QUERY_METER_STATE;
            }

            BassHardwardRequest request = MessageTpl.getMeterPrepareTpl(object.getMonitorType(),
                    cmdCode,
                    object.getPluginId(),
                    IDGenerator.getIdStr(),
                    object.getConAddress(),
                    object.getSubAddress());

            try {
                HttpUtils.commonSendHttpPost(url, JSONObject.toJSONString(request), PFConstDef.HTTP_FUN_MILL_NOTIFY);

            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
