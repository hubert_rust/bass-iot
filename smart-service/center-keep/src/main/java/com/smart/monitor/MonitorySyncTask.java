package com.smart.monitor;

import com.smart.entity.MonitorEntity;
import com.smart.framework.commonpool.Task;
import com.smart.repository.MonitorParamRepostory;
import com.smart.repository.MonitorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-13 16:18
 **/
@Component("MonitorySyncTask")
public class MonitorySyncTask extends Task {
    private static final Logger log = LoggerFactory.getLogger(MonitorySyncTask.class);

    @Autowired
    MonitorRepository repostory;

    @Override
    public void process() throws Exception {

        try {
            MonitorObject object = (MonitorObject) getObject();
            System.out.println(objectThreadLocal.get().toString());

            Optional<MonitorEntity> monitorEntity = repostory.findById(object.getMonitorEntity().getId());
            if (monitorEntity.isPresent()) {
                if (object.getMonitorEntity().getStatus().equals("ok")) {
                    monitorEntity.get().setLastClear(LocalDateTime.now().toString());
                }
                else {
                    monitorEntity.get().setLastFault(LocalDateTime.now().toString());
                }
                monitorEntity.get().setLastTime(object.getMonitorEntity().getLastTime());
                monitorEntity.get().setStatus(object.getMonitorEntity().getStatus());
                repostory.save(monitorEntity.get());
            }
            else {
                repostory.save(monitorEntity.get());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
