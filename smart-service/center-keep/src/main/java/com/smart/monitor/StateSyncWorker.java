package com.smart.monitor;

import com.smart.framework.commonpool.Task;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-13 16:20
 **/
public class StateSyncWorker extends Thread {
    private Task task;
    private Object obj;

    public StateSyncWorker(Task task, Object obj) {
        this.task = task;
        this.obj = obj;
    }


    @Override
    public void run() {
        try {
            task.setObject(obj);
            task.process();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }
}
