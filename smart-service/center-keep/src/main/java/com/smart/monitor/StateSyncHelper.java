package com.smart.monitor;

import com.smart.framework.commonpool.Worker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-13 13:34
 **/
public class StateSyncHelper {
    private static final Logger log = LoggerFactory.getLogger(StateSyncHelper.class);
    private int blockNum = 50;

    public StateSyncHelper(int blockNum) {
        this.blockNum = blockNum;
    }

    public ThreadPoolExecutor poolExecutor = null;
    static {
    }

    public boolean startup() {

        poolExecutor = new ThreadPoolExecutor(10,
                20,
                1,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(blockNum));




        return true;
    }

    public void submit(StateSyncWorker worker) {
        if (Objects.isNull(poolExecutor)
                || poolExecutor.isTerminated() || poolExecutor.isShutdown()) {

            log.error(">>> StateSyncHelper, submit, poolExecutor is null");
            return;
        }
        poolExecutor.submit(worker);
    }

}
