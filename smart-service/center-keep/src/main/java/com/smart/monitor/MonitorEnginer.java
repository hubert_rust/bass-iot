package com.smart.monitor;

import com.smart.entity.MonitorEntity;
import com.smart.framework.utils.CommonApplication;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.TimeUnit;

import static com.smart.monitor.MonitorDataCenter.stateSyncHelper;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-13 13:34
 **/
public class MonitorEnginer {

    public static MonitorObjectThread monitorObjectThread = new MonitorObjectThread();
    public static Queue<MonitorStateEvent> eventQueue = null;
    static {
        eventQueue = new ConcurrentLinkedDeque<>();
    }

    public boolean startup() {
        monitorObjectThread.start();
        return true;
    }

    /**
    * @Description: 这里其实检测的是否故障了,发送消息到
    * @Param:
    * @return:
    * @Author: admin
    * @Date: 2019/7/13
    */
    public static class MonitorObjectThread extends Thread {
        @Override
        public void run() {
            while (!MonitorDataCenter.isStart()) {
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            while (true) {
                try {
                    TimeUnit.SECONDS.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                int num = 0;
                for (String k : MonitorDataCenter.getMonitorObjectMap().keySet()) {
                    if (num >= 5) {
                        break;
                    }
                    try {
                        MonitorEntity monitor = MonitorDataCenter.getMonitorObjectMap().get(k);

                        long currentTime = System.currentTimeMillis();
                        long span = currentTime - monitor.getLastTime();

                        //如果是故障的就还是故障处理
                        if (monitor.getMainType().equals(MonitorType.MONITOR_CLASS_METER)) {
                            if (monitor.getStatus().equals("ok")) {
                                if (span >= (monitor.getProbeTime() * 1000)) {
                                    //一个周期，发生探测消息
                                    sendProbeMessage(monitor);
                                }
                            }
                        } else {
                            //插件或者集中器,
                            if (span > (monitor.getProbeTime() * 1000)) {
                                sendMessage(monitor.getObjectKey(), "nok");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    num++;
                }

            }

        }
    }

    private static void sendProbeMessage(MonitorEntity monitorEntity) {
        MonitoryProbeTask task = CommonApplication.getBean("MonitoryProbeTask");
        StateSyncWorker syncWorker = new StateSyncWorker(task, monitorEntity);
        stateSyncHelper.submit(syncWorker);
    }

    private static void sendMessage(String key, String state) {
        MonitorStateEvent monitorStateEvent = new MonitorStateEvent();
        monitorStateEvent.setObjectKey(key);
        monitorStateEvent.setState(state);
        MonitorDataCenter.off(monitorStateEvent);
    }
}
