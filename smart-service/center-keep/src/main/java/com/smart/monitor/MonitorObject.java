package com.smart.monitor;

import com.smart.entity.MonitorEntity;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-13 14:11
 **/
public class MonitorObject  {
    private MonitorEntity monitorEntity;
    private boolean dirty = false;

    public MonitorObject(MonitorEntity monitorEntity) {
        this.monitorEntity = monitorEntity;
    }

    public MonitorEntity getMonitorEntity() {
        return monitorEntity;
    }

    public void setMonitorEntity(MonitorEntity monitorEntity) {
        this.monitorEntity = monitorEntity;
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }
}
