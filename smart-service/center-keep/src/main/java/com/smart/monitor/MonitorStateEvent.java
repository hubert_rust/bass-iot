package com.smart.monitor;

import com.smart.event.HWStateEvent;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-13 15:13
 **/
public class MonitorStateEvent {
    private String objectKey;
    private String state;
    private boolean isNew = false;
    private HWStateEvent  stateEvent;

    public String getObjectKey() {
        return objectKey;
    }

    public void setObjectKey(String objectKey) {
        this.objectKey = objectKey;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    public HWStateEvent getStateEvent() {
        return stateEvent;
    }

    public void setStateEvent(HWStateEvent stateEvent) {
        this.stateEvent = stateEvent;
    }
}
