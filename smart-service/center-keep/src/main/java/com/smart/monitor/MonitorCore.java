package com.smart.monitor;

import com.google.common.collect.Table;
import com.smart.entity.MonitorEntity;
import com.smart.entity.MonitorParamEntity;
import com.smart.event.HWStateEvent;
import com.smart.framework.utils.CommonApplication;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import static com.smart.monitor.MonitorDataCenter.stateSyncHelper;

/**
 * @program: smart-service, 更新数据
 * @description:
 * @author: admin
 * @create: 2019-07-13 19:00
 **/
public class MonitorCore {
    public static MonitorProcessThread processThread = new MonitorProcessThread();

    public boolean startup() {
        processThread.start();
        return true;
    }

    public static void findNewMeter(MonitorStateEvent stateEvent) {
        if (stateEvent.getStateEvent().getHubAddress().equals("FFFF")) {
            return;
        }
        Table<String, String, MonitorParamEntity> table = MonitorDataCenter.getParamEntityTable();
        MonitorParamEntity paramEntity = table.get(stateEvent.getStateEvent().getMainClass(),
                stateEvent.getStateEvent().getMeterType());
        String key = stateEvent.getObjectKey();
        MonitorEntity monitorEntity = new MonitorEntity();
        //meter默认是ok
        monitorEntity.setStatus(stateEvent.getStateEvent().getState());
        monitorEntity.setMainType(stateEvent.getStateEvent().getMainClass());
        monitorEntity.setPluginId(stateEvent.getStateEvent().getPluginId());
        monitorEntity.setMonitorType(stateEvent.getStateEvent().getMeterType());
        monitorEntity.setProbeTime(paramEntity.getProbeTime());
        monitorEntity.setProbeTimeout(paramEntity.getProbeTimeout());
        monitorEntity.setConAddress(stateEvent.getStateEvent().getHubAddress());
        monitorEntity.setObjectKey(key);
        monitorEntity.setDcName(paramEntity.getDcName());
        monitorEntity.setDcId(paramEntity.getDcId());
        monitorEntity.setId(0L);
        monitorEntity.setLastTime(System.currentTimeMillis());
        monitorEntity.setLastClear(LocalDateTime.now().toString());
        monitorEntity.setLastFault(LocalDateTime.now().toString());

        //MonitorObject monitorObject = new MonitorObject(monitorEntity);
        //monitorObject.setDirty(false);

        MonitorDataCenter.getMonitorObjectMap().put(key, monitorEntity);
        MonitoryFindNewMeterTask task = CommonApplication.getBean("MonitoryFindNewMeterTask");
        StateSyncWorker syncWorker = new StateSyncWorker(task, monitorEntity);
        stateSyncHelper.submit(syncWorker);
    }
    public static class MonitorProcessThread extends Thread{
        @Override
        public void run() {

            while (!MonitorDataCenter.isStart()) {
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            while (true) {
                try {
                    TimeUnit.MILLISECONDS.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                try {
                    MonitorStateEvent stateEvent = MonitorDataCenter.poll();
                    if (Objects.isNull(stateEvent)) {
                        continue;
                    }
                    //
                    if (stateEvent.isNew()) {
                        findNewMeter(stateEvent);
                    }

                    MonitorEntity object = MonitorDataCenter.getMonitorObjectMap().get(stateEvent.getObjectKey());
                    if (Objects.isNull(object)) {
                    } else {
                        object.setLastTime(System.currentTimeMillis());
                        if (!object.getStatus().equals(stateEvent.getState())) {
                            //状态改变
                            object.setStatus(stateEvent.getState());
                            MonitorDataCenter.submitTask(object);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
