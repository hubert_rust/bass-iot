package com.smart.monitor;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Maps;
import com.google.common.collect.Table;
import com.smart.entity.MonitorEntity;
import com.smart.entity.MonitorParamEntity;
import com.smart.framework.commonpool.Task;
import com.smart.framework.commonpool.Worker;
import com.smart.framework.utils.CommonApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;

import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.TimeUnit;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-13 14:04
 **/
public class MonitorDataCenter {
    private static final Logger log = LoggerFactory.getLogger(MonitorDataCenter.class);

    public static volatile boolean start = false;
    public static MonitorEnginer monitorEnginer = null;
    public static MonitorCore monitorCore = null;
    public static StateSyncHelper stateSyncHelper = null;

    //StateSink和MonitorEnginer都发送消息到此队列，然后在独立线程中处理
    public static Queue<MonitorStateEvent> eventQueue = null;
    //main_class, monitor_type, MonitorParamEntity
    public static Table<String, String, MonitorParamEntity> paramEntityTable = null;
    //public static Map<String, MonitorObject> monitorObjectMap = null;
    public static Map<String, MonitorEntity> monitorObjectMap = null;
    //public static Table<String, String, MonitorEntity> hubTab

    static {
        monitorEnginer = new MonitorEnginer();
        monitorCore = new MonitorCore();
        monitorObjectMap = new ConcurrentHashMap<>();
        paramEntityTable = HashBasedTable.create();
        eventQueue = new ConcurrentLinkedDeque<>();
    }

    public static  MonitorStateEvent poll() {
        return eventQueue.poll();
    }
    public static void off(MonitorStateEvent stateEvent) {
        eventQueue.offer(stateEvent);
    }

    public static Table<String, String, MonitorParamEntity> getParamEntityTable() {return paramEntityTable; }
    public static void setParamEntityTable(Table<String, String, MonitorParamEntity> paramEntityTable) {
        MonitorDataCenter.paramEntityTable = paramEntityTable;
    }

    public static boolean isStart() { return start; }
    public static void setStart(boolean start) { MonitorDataCenter.start = start; }
    public static Map<String, MonitorEntity> getMonitorObjectMap() { return monitorObjectMap; }
    //public static Map<String, MonitorEntity> getMonitorObjectMap() { return monitorObjectMap; }
    public static void setMonitorObjectMap(Map<String, MonitorEntity> monitorObjectMap) {
        MonitorDataCenter.monitorObjectMap = monitorObjectMap;
    }

    public static boolean submitTask(MonitorEntity obj) {
        MonitorySyncTask task = CommonApplication.getBean("MonitorySyncTask");
        StateSyncWorker syncWorker = new StateSyncWorker(task, obj);
        stateSyncHelper.submit(syncWorker);
        return true;
    }
    public static boolean startup() {
        stateSyncHelper = new StateSyncHelper(100);
        stateSyncHelper.startup();
        monitorEnginer.startup();
        monitorCore.startup();
        log.info(">>> MonitorDataCenter startup finished.");
        return true;
    }
}
