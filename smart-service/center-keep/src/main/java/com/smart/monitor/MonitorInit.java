package com.smart.monitor;

import com.google.common.collect.Table;
import com.smart.entity.*;
import com.smart.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-13 13:53
 **/
@Configuration
public class MonitorInit implements CommandLineRunner {
    private static final Logger log = LoggerFactory.getLogger(MonitorInit.class);

    @Autowired
    PluginRepostory pluginRepostory;
    @Autowired
    ConcentratorRepostory concentratorRepostory;
    @Autowired
    MeterRepostory meterRepostory;

    @Autowired
    MonitorRepository monitorRepository;
    @Autowired
    MonitorParamRepostory paramRepostory;

    @Override
    public void run(String... args) throws Exception {

        try {
            loadMonitorParam();
            loadPlugin();
            loadHub();
            loadMeter();
            syncToDB();
        } catch (Exception e) {
            e.printStackTrace();
        }

        MonitorDataCenter.setStart(false);
        MonitorDataCenter.startup();
        MonitorDataCenter.setStart(true);
    }

    private void syncToDB() {
        MonitorDataCenter.getMonitorObjectMap().forEach((k, v)-> {
            Optional<MonitorEntity> monitorEntities = monitorRepository.findOne((root, query, builder) -> {
               return builder.equal(root.get("objectKey"), k);
            });

            if (!monitorEntities.isPresent()) {
                v.setCreateTime(LocalDateTime.now());
                System.out.println(v.toString());
                MonitorEntity ret = monitorRepository.save(v);
                v.setId(ret.getId());
            }
            else {
                log.error(">>> syncToDB, k: {}, v: {}", k, v);
                //内存数据
                MonitorEntity monitorObject = MonitorDataCenter.getMonitorObjectMap().get(monitorEntities.get().getObjectKey());
                monitorObject.setId(monitorEntities.get().getId());
            }

        });
    }

    private void loadMonitorParam() throws Exception{
        List<MonitorParamEntity> list = paramRepostory.findAll();
        Table<String, String, MonitorParamEntity> table = MonitorDataCenter.getParamEntityTable();
        list.forEach(v->{
            table.put(v.getMainClass(), v.getMonitorType(), v);
        });
    }
    private void loadPlugin() throws Exception{
        List<PluginEntity> list = pluginRepostory.findAll();
        Table<String, String, MonitorParamEntity> table = MonitorDataCenter.getParamEntityTable();

        //插件时间统一
        MonitorParamEntity paramEntity = table.get(MonitorType.MONITOR_CLASS_PLUGIN, MonitorType.MONITOR_CLASS_PLUGIN);
        list.forEach(v->{
            String key = MonitorType.MONITOR_PLUGIN + "-" + v.getPluginId();
            MonitorEntity monitorEntity = new MonitorEntity();
            monitorEntity.setStatus("nok");
            monitorEntity.setMainType(MonitorType.MONITOR_CLASS_PLUGIN);
            monitorEntity.setPluginId(v.getPluginId());
            monitorEntity.setMonitorType(v.getPluginName());
            monitorEntity.setProbeTime(paramEntity.getProbeTime());
            monitorEntity.setProbeTimeout(paramEntity.getProbeTimeout());
            monitorEntity.setObjectKey(key);
            monitorEntity.setDcName(v.getDcName());
            monitorEntity.setDcId(v.getDcId());
            monitorEntity.setId(0L);
            monitorEntity.setLastTime(System.currentTimeMillis());
            monitorEntity.setLastClear(LocalDateTime.now().toString());
            monitorEntity.setLastFault(LocalDateTime.now().toString());

            //MonitorObject monitorObject = new MonitorObject(monitorEntity);
            //monitorObject.setDirty(false);
            MonitorDataCenter.getMonitorObjectMap().put(key, monitorEntity);
        });
    }
    private void loadHub() throws Exception{
        List<ConcentratorEntity> list = concentratorRepostory.findAll();
        Table<String, String, MonitorParamEntity> table = MonitorDataCenter.getParamEntityTable();

        list.forEach(v->{
            MonitorParamEntity paramEntity = table.get(MonitorType.MONITOR_CLASS_HUB, v.getHubType());

            String key = MonitorType.MONITOR_CLASS_HUB + "-" + v.getHubAddress();
            MonitorEntity monitorEntity = new MonitorEntity();
            monitorEntity.setStatus("nok");
            monitorEntity.setMainType(MonitorType.MONITOR_CLASS_HUB);
            monitorEntity.setPluginId(v.getPluginId());
            monitorEntity.setMonitorType(v.getHubType());
            monitorEntity.setProbeTime(paramEntity.getProbeTime());
            monitorEntity.setProbeTimeout(paramEntity.getProbeTimeout());
            monitorEntity.setConAddress(v.getHubAddress());
            monitorEntity.setObjectKey(key);
            monitorEntity.setDcName(v.getDcName());
            monitorEntity.setDcId(v.getDcId());
            monitorEntity.setId(0L);
            monitorEntity.setLastTime(System.currentTimeMillis());
            monitorEntity.setLastClear(LocalDateTime.now().toString());
            monitorEntity.setLastFault(LocalDateTime.now().toString());

            //MonitorObject monitorObject = new MonitorObject(monitorEntity);
            //monitorObject.setDirty(false);
            MonitorDataCenter.getMonitorObjectMap().put(key, monitorEntity);

            //
        });

    }
    private void loadMeter() {
        List<MeterEntiry> list = meterRepostory.findAll();
        Table<String, String, MonitorParamEntity> table = MonitorDataCenter.getParamEntityTable();

        list.forEach(v->{
            MonitorParamEntity paramEntity = table.get(MonitorType.MONITOR_CLASS_METER, v.getMeterType());

            String key = MonitorType.MONITOR_CLASS_METER + "-" + v.getHubAddress() + "-"+v.getMeterAddress();
            MonitorEntity monitorEntity = new MonitorEntity();
            //meter默认是ok
            monitorEntity.setStatus("ok");
            monitorEntity.setMainType(MonitorType.MONITOR_CLASS_METER);
            monitorEntity.setPluginId(v.getPluginId());
            monitorEntity.setMonitorType(v.getMeterType());
            monitorEntity.setProbeTime(paramEntity.getProbeTime());
            monitorEntity.setProbeTimeout(paramEntity.getProbeTimeout());
            monitorEntity.setConAddress(v.getHubAddress());
            monitorEntity.setSubAddress(v.getMeterAddress());
            monitorEntity.setObjectKey(key);
            monitorEntity.setDcName(v.getDcName());
            monitorEntity.setDcId(v.getDcId());
            monitorEntity.setId(0L);
            monitorEntity.setLastTime(System.currentTimeMillis());
            monitorEntity.setLastClear(LocalDateTime.now().toString());
            monitorEntity.setLastFault(LocalDateTime.now().toString());

            //MonitorObject monitorObject = new MonitorObject(monitorEntity);
            //monitorObject.setDirty(false);
            MonitorDataCenter.getMonitorObjectMap().put(key, monitorEntity);
        });

    }

}
