package com.smart.monitor;

import com.smart.entity.MonitorEntity;
import com.smart.framework.commonpool.Task;
import com.smart.repository.MonitorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-13 16:18
 **/
@Component("MonitoryFindNewMeterTask")
public class MonitoryFindNewMeterTask extends Task {
    private static final Logger log = LoggerFactory.getLogger(MonitoryFindNewMeterTask.class);

    @Autowired
    MonitorRepository repostory;



    @Override
    public void process() throws Exception {

        try {
            MonitorEntity object = (MonitorEntity) getObject();
            System.out.println(objectThreadLocal.get().toString());


            Optional<MonitorEntity> monitorEntity = repostory.findOne((root, query, builder)->{
                return builder.equal(root.get("objectKey"), object.getObjectKey());
            });
            if (monitorEntity.isPresent()) {
                if (object.getStatus().equals("ok")) {
                    monitorEntity.get().setLastClear(LocalDateTime.now().toString());
                }
                else {
                    monitorEntity.get().setLastFault(LocalDateTime.now().toString());
                }
                monitorEntity.get().setLastTime(object.getLastTime());
                monitorEntity.get().setStatus(object.getStatus());
                repostory.save(monitorEntity.get());
            }
            else {
                repostory.save(object);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
