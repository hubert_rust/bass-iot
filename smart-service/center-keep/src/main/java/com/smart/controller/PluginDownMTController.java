package com.smart.controller;

import com.google.common.base.Preconditions;
import com.smart.service.PluginDownMTServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

//平台维护命令
@RestController
@RequestMapping("mtdown")
@Scope("prototype")
public class PluginDownMTController {
    private static final Logger log=LoggerFactory.getLogger(PluginDownMTController.class);

    @Autowired
    private PluginDownMTServiceImpl mtService;

    /**
     * @ Description: {"commond_code": "", params: {}}
     * @ Author: Admin
     * @ {plugin}是pluginId，唯一确定一个plugin
    **/
    @RequestMapping(value="/command/{plugin}", method=RequestMethod.POST)
    public Object command(@RequestBody String request, @PathVariable("plugin") String pluginId) throws Exception{
        log.info(">>> PluginDownMTController, command, pluginId: {}", pluginId);
        return mtService.command(request, pluginId);
    }

}
