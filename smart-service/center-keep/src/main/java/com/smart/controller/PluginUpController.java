package com.smart.controller;

import com.google.gson.Gson;
import com.smart.framework.cons.BassConst;
import com.smart.framework.utils.ResponseUtils;
import com.smart.request.IBaseEntity;
import com.smart.service.PluginUpServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("keep")
@Scope("prototype")
public class PluginUpController {
    private static final Logger log=LoggerFactory.getLogger(PluginUpController.class);

    @Autowired
    private PluginUpServiceImpl registerService;

    @RequestMapping(value="/proc", method=RequestMethod.POST)
    public String proc(@RequestBody String request) throws Exception{
        log.info(">>> proc...");

        IBaseEntity baseEntity = new Gson().fromJson(request, IBaseEntity.class);
        String subType = baseEntity.getSubType().toUpperCase();
        if (subType.equals(BassConst.BASS_HARDWARD_SERVICE_KEEP_SUBTYPE_REG.toUpperCase())) {
            //加载数据
            return registerService.register(request);
        } else if (subType.equals(BassConst.BASS_HARDWARD_SERVICE_KEEP_SUBTYPE_DOWN.toUpperCase())) {
            //加载配置
            return registerService.download(request);
        } else if (subType.equals(BassConst.BASS_HARDWARD_SERVICE_KEEP_SUBTYPE_HUBSTATE.toUpperCase())) {
            return registerService.hubState(request);

        } else if (subType.equals(BassConst.BASS_HARDWARD_SERVICE_KEEP_SUBTYPE_HEART.toUpperCase())) {
            return registerService.heartBeat(request);
        }
        else {
            return ResponseUtils.getFailResponse("serviceSubType is invalid");
        }
    }

}
