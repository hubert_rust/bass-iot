package com.smart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Configuration;

/**
 * @program: smart-service
 * @description: app
 * @author: Admin
 * @create: 2019-04-16 21:21
 **/

@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"com.smart"})
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}