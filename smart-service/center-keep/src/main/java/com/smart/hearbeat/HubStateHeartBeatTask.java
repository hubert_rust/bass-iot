package com.smart.hearbeat;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.smart.framework.commonpool.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Objects;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-10 11:13
 **/
@Deprecated
@Service("HubStateHeartBeatTask")
@Scope("prototype")
public class HubStateHeartBeatTask extends Task {
    private static final Logger log = LoggerFactory.getLogger(HubStateHeartBeatTask.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;


    public int refreshDB(String hubAddress, String state) {
        //String sql = "update tb_hardware_plugin_manage set status = '"+ state +"', modify_time where plugin_id = '" + pluginId + "'";
        String sql = "update tb_hardware_hub set run_state = '$state', modify_time = now() where hub_address= '$hubAddress'";
        sql = sql.replace("$state", state).replace("$hubAddress", hubAddress);
        int ret = jdbcTemplate.update(sql);
        return ret;
    }
    @Override
    public void process() throws Exception {
        return;

/*        ConfigLoadUtils.hubMap.forEach((k,v)->{
            long curTm = System.currentTimeMillis();
            long lastTm = Long.valueOf(v.get("realTimeState").toString());
            Object obj = v.get("hub_param");
            if (Objects.isNull(obj) || Strings.isNullOrEmpty(obj.toString())) {
                log.error(">>> HubStateHeartBeatTask, process, hub: {} hub_param invalid", k);
                return;
            }
            Map<String, Object> param = JSONObject.parseObject(v.get("hub_param").toString());
            if (param != null) {
                Map<String, Object> ret = (Map)param.get("hwheart");
                String hubAddress = v.get("hub_address").toString();
                Long span = Long.valueOf(ret.get("peerHeartBeatSpan").toString());
                if ((curTm - lastTm) > span) {
                    refreshDB(hubAddress, "nok");

                    log.error(">>> HubStateHeartBeatTask, process, {} timeout", hubAddress);
                }
            }

        });*/
    }
}
