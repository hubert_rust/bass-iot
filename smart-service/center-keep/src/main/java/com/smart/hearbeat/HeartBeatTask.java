package com.smart.hearbeat;

import com.alibaba.fastjson.JSONObject;
import com.smart.framework.commonpool.Task;
import com.smart.framework.cons.BassConst;
import com.smart.framework.utils.HttpUtils;
import com.smart.request.BassHardwardRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @program: smart-service, 心跳暂时可以直接和plugin通信，不经过网关
 * @description:
 * @author: admin
 * @create: 2019-05-06 17:15
 **/
@Deprecated
@Service("HeartBeatTask")
@Scope("prototype")
public class HeartBeatTask extends Task {
    private static final Logger log = LoggerFactory.getLogger(HeartBeatTask.class);
    private String pluginId;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public String getPluginId() { return pluginId; }
    public void setPluginId(String pluginId) { this.pluginId = pluginId; }

    @Override
    public void process() throws Exception {
        //pending
        Map plugin = null;//ConfigLoadUtils.pluginMap.get(pluginId);

        String url = plugin.get("plugin_url").toString();

        BassHardwardRequest baseEntity = new BassHardwardRequest<>();
        baseEntity.setServiceId(pluginId);
        baseEntity.setSubType(BassConst.BASS_HARDWARD_SERVICE_PLUGIN_SUBTYPE_HEART);
        baseEntity.setServiceType(BassConst.BASS_HARDWARD_SERVICE_NAME_PLUGIN);

        boolean testPlugin = false;
        try {
            Object obj = HttpUtils.commonSendHttpPost(url, JSONObject.toJSONString(baseEntity));
            Map<String, Object> ret = JSONObject.parseObject((String)obj, Map.class);
            Object objVal = ret.get("returnCode");
            if (objVal == null || !objVal.toString().equals("SUCCESS")) {
                if (retryPost(url, JSONObject.toJSONString(baseEntity))) {
                    testPlugin = true;
                }
            }
            else {
                testPlugin = true;
            }
        } catch (Exception e) {
            log.error(">>> HeartBeatTask, process, e: {}", e.getMessage());
            if (retryPost(url, JSONObject.toJSONString(baseEntity))) {
                testPlugin = true;
            }
        }

        refreshDB(pluginId, testPlugin == true ? "ok" : "nok");
    }

    public int refreshDB(String pluginId, String state) {
        //String sql = "update tb_hardware_plugin_manage set status = '"+ state +"', modify_time where plugin_id = '" + pluginId + "'";
        String sql = "update tb_hardware_plugin_manage set status = '$state', modify_time = now() where plugin_id = '$pluginId'";
        sql = sql.replace("$state", state).replace("$pluginId", pluginId);
        int ret = jdbcTemplate.update(sql);
        return ret;
    }

    public boolean retryPost(String url, String request) {
        for (int i=0; i<2; i++) {
            try {
                Object obj = HttpUtils.commonSendHttpPost(url, JSONObject.toJSONString(request));
                Map<String, Object> ret = JSONObject.parseObject((String)obj, Map.class);
                Object objVal = ret.get("returnCode");
                if (objVal == null || !objVal.toString().equals("SUCCESS")) {
                }
                else {
                    return true;
                }
            } catch (Exception e) {
                log.error(">>> retryPost, process, e: {}", e.getMessage());
            }
        }
        return false;
    }
}
