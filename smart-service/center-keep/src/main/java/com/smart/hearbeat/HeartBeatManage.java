package com.smart.hearbeat;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.google.common.collect.Maps;
import com.smart.framework.commonpool.AsyncNotifyThreadPool;
import com.smart.framework.commonpool.Worker;
import com.smart.framework.utils.CommonApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-06 17:18
 **/
@Deprecated
@Component
public class HeartBeatManage {
    private static final Logger log = LoggerFactory.getLogger(HeartBeatManage.class);

    //和插件的心跳
    public static final Long HEART_BEAT_SPAN = 10000L;
    //和集中器的心跳
    public static final Long HUB_HEART_BEAT_SPAN = 15000L;
    public static Map<String, Worker> pluginMap = Maps.newConcurrentMap();

    public static void register(String pluginId) {
        //每个插件对应一个心跳单元, 多次注册需要考虑
        HeartBeatTask task = CommonApplication.getBean("HeartBeatTask");
        Worker check = pluginMap.get(pluginId);
        if (check != null) {
            log.info(">>> HeartBeatmanage, register, pluginId: {} is ping", pluginId);
            return;
        }
        task.setPluginId(pluginId);
        Worker worker = new Worker(task, HEART_BEAT_SPAN);
        pluginMap.put(pluginId, worker);
        AsyncNotifyThreadPool.submit(worker);

        //和集中器心跳, 可以在插件心跳中完成，通过插件心跳返回结果
        HubStateHeartBeatTask hubStateTask = CommonApplication.getBean("HubStateHeartBeatTask");
        Worker stateWork = new Worker(hubStateTask, HUB_HEART_BEAT_SPAN);
        AsyncNotifyThreadPool.submit(stateWork);

    }
    public static void unRegister(String pluginId) {
        Worker worker = pluginMap.get(pluginId);
        worker.interrupt();

        log.info(">>> HeartBeatManage, unRegister, pluginId, isInterrupt: {}", worker.isInterrupted());
    }
}
