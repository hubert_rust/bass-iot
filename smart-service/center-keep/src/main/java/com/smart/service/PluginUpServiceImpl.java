package com.smart.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.smart.entity.*;
import com.smart.repository.ConcentratorRepostory;
import com.smart.repository.MeterRepostory;
import com.smart.repository.PluginRepostory;
import com.smart.request.MeterInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;

/*
 * @ program: register-center
 * @ description: register
 * @ author: admin
 * @ create: 2019-03-27 09:59
 **/
@Service
@Scope("prototype")
public class PluginUpServiceImpl {
    private static final Logger LOGGER = LoggerFactory.getLogger(PluginUpServiceImpl.class);

    @Autowired
    PluginRepostory pluginRepostory;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    ConcentratorRepostory conRepostory;
    @Autowired
    MeterRepostory meterRepostory;

    //pluginId是每个插件唯一
    private int saveDB(String pluginId) {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        //String sql = "update tb_plugin_config set plugin_uid = '" + uuid + "' where plugin_pack_id = '" + pluginId + "'";
        String sql = "update tb_hardware_plugin_manage set status = 'ok' where plugin_id = '" + pluginId + "'";
        int ret = jdbcTemplate.update(sql);
        return ret;
    }

    /**
     * @ Description: 插件注册同时，下载表信息
     * @ Author: Admin
     **/
    public String register(String request) throws Exception {
        //获取plugId
        Map<String, Object> map = JSONObject.parseObject(request, Map.class);
        Map<String, Object> pluginMap = (Map) ((JSONArray) map.get("entitys")).get(0);

        String pluginId = pluginMap.get("pluginId").toString();

        ReturnEntity returnEntity = new ReturnEntity();
        returnEntity.setReturnCode("SUCCESS");
        //更新表状态
        try {
            saveDB(pluginId);
        } catch (Exception e) {
            e.printStackTrace();
            returnEntity.setReturnCode("FAIL");
            returnEntity.setReturnMsg(e.getMessage());
            return JSONObject.toJSONString(returnEntity);
        }

        //每次请求重新刷新
        //configLoadUtils.loadHubInof();
        //configLoadUtils.loadMeterInfo();

        //注册的时候返回hub信息和meter信息, 从数据库查询
        //pending
        returnEntity.setFlag(true);
        List<ConcentratorEntity> conList = conRepostory.findAll((root, query, builder)-> {

            Predicate p1 = builder.equal(root.get("pluginId"), pluginId);
            Predicate p2 = builder.equal(root.get("hubState"), "active");
            return builder.and(p1, p2);
        });

        conList.forEach(v-> {
            HubInfo hubInfo = new HubInfo();
            hubInfo.setBps(v.getBps());
            hubInfo.setCommPort(v.getCommPort());
            hubInfo.setHubAddress(v.getHubAddress());
            hubInfo.setHubIp(v.getHubIp());
            hubInfo.setHubType(v.getHubType());
            hubInfo.setHubMode(v.getHubMode());
            hubInfo.setPluginId(v.getPluginId());

            hubInfo.setHubParam(v.getHubParam());
            returnEntity.getHubs().add(hubInfo);
        });
        List<MeterEntiry>  meterList = meterRepostory.findAll(new Specification<MeterEntiry>() {
            @Override
            public Predicate toPredicate(Root<MeterEntiry> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                Predicate p1 = criteriaBuilder.equal(root.get("pluginId"), pluginId);
                Predicate p2 = criteriaBuilder.equal(root.get("meterStatus"), "use");
                return criteriaBuilder.and(p1, p2);
            }
        });
        meterList.forEach(v->{
            MeterInfo meterInfo = new MeterInfo();
            meterInfo.setHubAddress(v.getHubAddress());
            meterInfo.setMeterAddress(v.getMeterAddress());
            meterInfo.setBps(v.getBps());
            meterInfo.setCommPort(v.getCommPort());
            meterInfo.setMeterType(v.getMeterType());
            meterInfo.setId(v.getId());

            returnEntity.getDatas().add(meterInfo);
        });

        /*ConfigLoadUtils.hubMap.forEach((k, v) -> {
            Map<String, Object> hubMap = (Map<String, Object>) v;
            if (hubMap.get("plugin_id").toString().equals(pluginId)) {
                HubInfo hubInfo = new HubInfo();
                hubInfo.setBps(Integer.valueOf(hubMap.get("bps").toString()));
                hubInfo.setCommPort(Integer.valueOf(hubMap.get("comm_port").toString()));
                hubInfo.setHubAddress(hubMap.get("hub_address").toString());
                hubInfo.setHubIp(hubMap.get("hub_ip").toString());
                hubInfo.setHubType(hubMap.get("hub_type") == null ? null : hubMap.get("hub_type").toString());
                hubInfo.setPluginId(hubMap.get("plugin_id").toString());
                hubInfo.setHubParam(hubMap.get("hub_param") == null ? null : hubMap.get("hub_param").toString());
                returnEntity.getHubs().add(hubInfo);
            }
        });*/

        /*ConfigLoadUtils.table.cellSet().stream().filter(v -> {
            Map<String, Object> val = v.getValue();
            String pid = val.get("plugin_id").toString();
            return pid.equals(pluginId) ? true : false;
        }).forEach(v -> {

            String hubAddress = v.getRowKey();
            String meterAddress = v.getColumnKey();

            Map<String, Object> val = v.getValue();
            int hubBps = Integer.valueOf(val.get("bps").toString());
            int hubPort = Integer.valueOf(val.get("comm_port").toString());
            String meterType = val.get("meter_type").toString();

            MeterInfo meterInfo = new MeterInfo();
            meterInfo.setConcentratorAddress(hubAddress);
            meterInfo.setMeterAddress(meterAddress);
            meterInfo.setConcentratorBps(hubBps);
            meterInfo.setConcentratorPort(hubPort);
            meterInfo.setMeterType(meterType);

            returnEntity.getDatas().add(meterInfo);
        });
*/
        return JSONObject.toJSON(returnEntity).toString();
    }

    /**
     * @ Description: 插件下载配置文件, 同时开启心跳功能
     * @ Author: Admin
     **/
    public String download(String request) throws Exception {
        LOGGER.info(">>> download...");
        String sql = "select * from tb_hardware_plugin_manage";

        ReturnEntity returnEntity = new ReturnEntity();

        //获取plugId
        Map<String, Object> map = JSONObject.parseObject(request, Map.class);
        Map<String, Object> pluginMap = (Map) ((JSONArray) map.get("entitys")).get(0);

        String pluginId = pluginMap.get("pluginId").toString();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);

        Map<String, Object> mapRet = Maps.newHashMap();
        rows.forEach(v -> {
            String var1 = v.get("plugin_id").toString();
            if (var1.equals(pluginId)) {
               mapRet.putAll(v);
            }

        });
        if (mapRet != null && mapRet.size() > 0) {
            mapRet.put("returnCode", "SUCCESS");
        } else {
            mapRet.put("returnCode", "FAIL");
            mapRet.put("returnMsg", "plugin can not find in tb_hardware_plugin_manage");
        }

        return JSONObject.toJSONString(mapRet);
    }

    private int refreshHubState(String hubAddr, String state) throws Exception {
        String sql = "update tb_hardware_hub set run_state = '" + state + "', modify_time = now() where hub_address = '" + hubAddr + "'";
        int ret = jdbcTemplate.update(sql);
        return ret;
    }

    public String hubState(String request) throws Exception {
        LOGGER.info(">>> hubState: {}", request);
        ReturnEntity returnEntity = new ReturnEntity();
        Map<String, Object> map = JSONObject.parseObject(request, Map.class);
        Object obj = map.get("hubAddress");
        if (obj == null || Strings.isNullOrEmpty(obj.toString())) {
            returnEntity.setReturnCode("FAIL");
            returnEntity.setReturnMsg("hubAddr is invalid");
            return JSONObject.toJSONString(returnEntity);
        }
        Object reserved = map.get("reserved");
        if (reserved == null || Strings.isNullOrEmpty(reserved.toString())) {
            returnEntity.setReturnCode("FAIL");
            returnEntity.setReturnMsg("reserved is invalid");
            return JSONObject.toJSONString(returnEntity);
        }

        //刷新, pending
/*        DiagnosicMessage message = DiagnosicMessage.DiagnosicMessageBuilder
                .aDiagnosicMessage()
                .withKey(MonitorType.MONITOR_CLASS_HUB + "-" + obj.toString())
                .withMsgType(MonitorType.MONITOR_TYPE_REFRESH)
                .build();
        MonitorManager.offer(message);*/
        returnEntity.setReturnCode("SUCCESS");
        returnEntity.setReturnMsg("OK");
        return JSONObject.toJSONString(returnEntity);
/*
        int ret = refreshHubState(obj.toString(), reserved.toString());
        //刷新定时时间
        Map<String, Object> hub = ConfigLoadUtils.hubMap.get(obj.toString());
        hub.put("realTimeState", System.currentTimeMillis());

        returnEntity.setReturnCode("SUCCESS");
        returnEntity.setReturnMsg("OK");
*/
    }


    public String heartBeat(String request) throws Exception {
        LOGGER.info(">>> heartBeat: {}", request);

        Map<String, Object> map = (Map) JSONObject.parse(request);
        Object obj = map.get("returnCode");
        if (Objects.isNull(obj) || !obj.toString().equals("SUCCESS")) {
            return null;
        }

        map.get("datas");


        //saveDB()
        return "";
    }
}
