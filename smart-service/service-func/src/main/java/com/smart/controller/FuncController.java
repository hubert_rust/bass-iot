package com.smart.controller;

import com.google.gson.Gson;
import com.smart.config.DataLoader;
import com.smart.config.MonitorMessage;
import com.smart.factory.IHubService;
import com.smart.framework.utils.PFConstDef;
import com.smart.framework.utils.ResponseUtils;
import com.smart.request.BassHardwardRequest;
import com.smart.service.ServiceLockObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @program: service-func,
 * @description:
 * @author: admin
 * @create: 2019-05-15 10:29
 **/
@Scope("prototype")
@RestController
@RequestMapping("func")
public class FuncController {
    private static final Logger log = LoggerFactory.getLogger(FuncController.class);
    private static Map<String, ServiceLockObject> hubServiceMap = new ConcurrentHashMap<>();


    private static boolean checkValid() {
        return true;
    }

    /**
    * @Description: 该机制需要考虑数据更改，需要优化
    * @Param:
    * @return:
    * @Author: admin
    * @Date: 2019/5/17
    */
    @PostMapping("proc")
    public Object funcProc(@RequestBody String request) {
        Object obj = null;
        try {
            BassHardwardRequest requestModel = new Gson().fromJson(request, BassHardwardRequest.class);
            String hubAddress = requestModel.getHubAddress();
            IHubService  hubService = DataLoader.hubFactory.create(requestModel.getCmdType());//IHubService)DataLoader.hubServiceMap.get(hubAddress);

            if (Objects.isNull(hubService)) {
                log.error(">>> FuncController, funcProc, subType: {}", requestModel.getCmdType());
                return ResponseUtils.getResponseBody()
                        .put(PFConstDef.RESP_RETURN_CODE, PFConstDef.RESP_RETURN_CODE_FAIL)
                        .put(PFConstDef.RESP_RETURN_MSG, "操作失败: hubAddress is invalid")
                        .getResponseBody();
            }

            ServiceLockObject object = getHubLockObject(requestModel);
            synchronized (object) {
                obj = hubService.process(requestModel, request);
            }
            //monitorMessage.sendMessage((String)obj);

            return obj;
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseUtils.getFailResponse(e.getMessage());
        }
    }

    public static ServiceLockObject getHubLockObject(BassHardwardRequest request) {
        ServiceLockObject hubLockObject = hubServiceMap.get(request.getHubAddress());

        if (Objects.isNull(hubLockObject)) {
            ServiceLockObject newHubLockObject = new ServiceLockObject();
            hubServiceMap.put(request.getHubAddress(), newHubLockObject);
            return newHubLockObject;
        }
        return hubLockObject;
    }
}
