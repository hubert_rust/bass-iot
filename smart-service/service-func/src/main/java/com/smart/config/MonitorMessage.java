package com.smart.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-16 15:14
 **/
@Component
@EnableBinding(IMonitorSource.class)
public class MonitorMessage {
    private static final Logger log = LoggerFactory.getLogger(MonitorMessage.class);
    @Autowired
    private IMonitorSource source;

    public void sendMessage(String entity) {
        //String sendStr = new Gson().toJson(entity);
        source.output().send(MessageBuilder.withPayload(entity).build());
    }
}
