package com.smart.config;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-16 15:11
 **/
public interface IMonitorSource {
    String OUTPUT_MONITOR = "monitor";
    @Output(OUTPUT_MONITOR)
    MessageChannel output();

/*    String OUTPUT1 = "output1";
    @Output(OUTPUT1)
    MessageChannel output1();*/
}
