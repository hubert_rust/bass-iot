package com.smart.config;


import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.smart.command.*;
import com.smart.entity.PluginEntity;
import com.smart.factory.HubFactory;
import com.smart.factory.IHubService;
import com.smart.framework.config.ZookeeperListenCallBack;
import com.smart.framework.utils.CommonApplication;
import com.smart.monitor.ConfigLoadUtils;
import com.smart.monitor.MonitorManager;
import com.smart.monitor.MonitorType;
import com.smart.monitor.object.DiagnosicObjectPlugin;
import com.smart.repository.PluginRepostory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
* @Description: 加载service
* @Param:
* @return:
* @Author: admin
* @Date: 2019/6/24
*/
@Component
public class DataLoader implements ApplicationRunner {
    private static final Logger log = LoggerFactory.getLogger(DataLoader.class);
    //public static Map<String, Object> hubMap = Maps.newHashMap();
    public static Map<String, IHubService> hubServiceMap = Maps.newHashMap();
    //public static Map<String, Object> meterMap = Maps.newHashMap();
    public static Map<String, Object> pluginMap = Maps.newHashMap();


    public static HubFactory hubFactory;

    private final static String BASS_HUB_SELECT = "select * from tb_hardware_hub";
    private final static String BASS_METER_SELECT = "select * from tb_hardware_meter";
    private final static String BASS_PLUGIN_SELECT = "select * from tb_hardware_plugin_manage";

    @Value("${func-type}")
    private String funcType;

    @Autowired
    private ZookeeperListenCallBack listenCallBack;

    @Autowired
    PluginRepostory pluginRepostory;

    @Autowired
    public JdbcTemplate jdbcTemplate;

    @Autowired
    RechargeExceptionTask exceptionTask;

    /**
    * @Description: ======任何修改集中器的的设置需要重新加载======
    * @Param:
    * @return:
    * @Author: admin
    * @Date: 2019/5/17
    */
    public static void initHubService(Map<String, Object> map) {
        String hubAddr = map.get("hub_address").toString();
        String hubType = map.get("hub_type").toString();
        IHubService meterService = hubFactory.create(hubType);
        hubServiceMap.put(hubAddr, meterService);
    }

    //加载集中器信息
    public String getSqlCondition(String mask, String fdName) {
        if (Strings.isNullOrEmpty(mask)) {
            return "";
        }

        String sql = "'$" + fdName.toUpperCase() + "'";
        String sqlSnip = fdName + "= " + sql;
        String finalSql = "";
        String[] typeArr = mask.split(",");
        for (String v: typeArr) {
            String var1 = CommandCode.MASK_MAP.get(v);
            if (Strings.isNullOrEmpty(var1)) {
                continue;
            }
            if (v.equals(var1)) {
                String var2 = sqlSnip;
                var2 = var2.replace("$"+fdName.toUpperCase(), var1);
                if (!Strings.isNullOrEmpty(finalSql)) {
                    finalSql += ("or ") + var2;
                }
                else {
                    finalSql += var2;
                }
            }

        }

        return Strings.isNullOrEmpty(finalSql) ? "" : (" where " + finalSql);
    }
    public void loadHubInof(String funcType) {
        synchronized (ConfigLoadUtils.class) {
/*            String sql = "select * from tb_hardware_hub where hub_type = '$HUB_TYPE'";
            sql = sql.replace("$HUB_TYPE", funcType);
            if (funcType.equals("all")) {
            }*/
            String sql = "select * from tb_hardware_hub";
            sql += getSqlCondition(funcType, "hub_type");
            List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
            rows.forEach(v -> {
                initHubService(v);

            });
            log.info(">>> ConfigLoadUtils, loadHubInfo: {}", rows.toString());
        }
    }
   /* public void loadMeter() {
        //meterMap.clear();
        List<Map<String, Object>> listMap = jdbcTemplate.queryForList(BASS_METER_SELECT);
        listMap.forEach(v -> {
            String hubAddr = v.get("meter_address").toString();
            hubMap.put(hubAddr, v);
        });
    }*/

    public void loadPlugin() {
        //pluginMap.clear();
        List<Map<String, Object>> listMap = jdbcTemplate.queryForList(BASS_PLUGIN_SELECT);
        listMap.forEach(v -> {
            String pluginId = v.get("plugin_id").toString();
            pluginMap.put(pluginId, v);
        });

        /*List<PluginEntity> pluginEntities = pluginRepostory.findAll((root, query, builder)->{
            return builder.equal(root.get("pluginName"), funcType);
        });

        pluginEntities.forEach(v-> {
            DiagnosicObjectPlugin plugin = new DiagnosicObjectPlugin();
            plugin.setPluginId(v.getPluginId());
            plugin.setPluginUrl(v.getPluginUrl());
            plugin.setState("ok");  //默认状态OK
        });
*/
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        hubFactory = HubFactory.factory(builder -> {
            builder.add(CommandCode.HUB_TYPE_COLDMETER, CommonApplication.getBean("ColdMeterServiceImpl"));
            builder.add(CommandCode.HUB_TYPE_HOTMETER, CommonApplication.getBean("HotMeterServiceImpl"));
            builder.add(CommandCode.HUB_TYPE_ELECMETER, CommonApplication.getBean("ElecMeterServiceImpl"));
            builder.add(CommandCode.HUB_TYPE_HIGHMETER, CommonApplication.getBean("HighPowerMeterServiceImpl"));
            builder.add(CommandCode.HUB_TYPE_LOCKER, CommonApplication.getBean("LockerServiceImpl"));
        });


        //loadHub();
        //loadMeter();
        loadPlugin();

        exceptionTask.start();



        /*new Thread() {
            @Override
            public void run() {
                while (true) {
                    try {
                        TimeUnit.SECONDS.sleep(30);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    refresh();
                }
            }
        }.start();*/
    }

    public synchronized void refresh() {
       loadPlugin();
    }
}