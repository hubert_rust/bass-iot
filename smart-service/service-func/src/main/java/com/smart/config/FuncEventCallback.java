package com.smart.config;

import com.smart.framework.config.IEventCallback;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.TreeCacheEvent;
import org.apache.curator.utils.ZKPaths;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-15 08:42
 **/
public class FuncEventCallback implements IEventCallback {
    private static final Logger log = LoggerFactory.getLogger(FuncEventCallback.class);
    @Override
    public void process(CuratorFramework client, TreeCacheEvent event) {
        log.info(">>> FuncEventCallback, process");
        switch (event.getType()) {

            case NODE_ADDED:
                System.out.println("Node add " + ZKPaths.getNodeFromPath(event.getData().getPath()));
                break;
            case NODE_REMOVED:
                System.out.println("Node removed " + ZKPaths.getNodeFromPath(event.getData().getPath()));
                break;
            case NODE_UPDATED:
                System.out.println("Node updated " + ZKPaths.getNodeFromPath(event.getData().getPath()));
                break;

        }
    }
}
