package com.smart.config;

import com.smart.command.CommandCode;
import com.smart.entity.MeterRechargeExceptionEntity;
import com.smart.recharge.ColdMeterRechargeExceptionService;
import com.smart.recharge.ElecMeterRechargeExceptionService;
import com.smart.recharge.HighMeterRechargeExceptionService;
import com.smart.recharge.HotMeterRechargeExceptionService;
import com.smart.repository.MeterRechargeExceptionRepostory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Predicate;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-09-29 13:59
 **/
@Component
public class RechargeExceptionTask extends Thread {
    private static final Logger log = LoggerFactory.getLogger(RechargeExceptionTask.class);
    @Autowired
    ElecMeterRechargeExceptionService elecExceptionService;
    @Autowired
    ColdMeterRechargeExceptionService coldExceptionService;
    @Autowired
    HighMeterRechargeExceptionService highExceptionService;
    @Autowired
    HotMeterRechargeExceptionService hotExceptionService;
    @Autowired
    MeterRechargeExceptionRepostory exceptionRepostory;

    @Override
    public void run() {
        log.info(">>> RechargeExceptionTask, run");

        while (true) {
            try {
                TimeUnit.SECONDS.sleep(30);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            List<MeterRechargeExceptionEntity> entitys = exceptionRepostory.findAll((root, query, builder)-> {
                return builder.equal(root.get("retryStatus1"), "NA");
            });

            for (MeterRechargeExceptionEntity entity: entitys) {
                try {
                    if (entity.getMeterType().equals(CommandCode.HUB_TYPE_ELECMETER)) {
                        elecExceptionService.retryProcess(entity);
                    }
                    else if (entity.getMeterType().equals(CommandCode.HUB_TYPE_COLDMETER)) {
                        coldExceptionService.retryProcess(entity);
                    }
                    else if (entity.getMeterType().equals(CommandCode.HUB_TYPE_HIGHMETER)) {
                        highExceptionService.retryProcess(entity);
                    }
                    else if (entity.getMeterType().equals(CommandCode.HUB_TYPE_HOTMETER)) {
                        hotExceptionService.retryProcess(entity);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
