package com.smart.config;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.smart.PluginUseTable;
import com.smart.dbwatch.DBWatchMessageEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;

import java.util.Map;
import java.util.Objects;

/**
 * @program: 配置监测消息
 * @description:
 * @author: admin
 * @create: 2019-05-16 15:34
 **/
@EnableBinding(IMaintanceSink.class)
public class MaintanceSink {
    private static final Logger log = LoggerFactory.getLogger(MaintanceSink.class);

    @Autowired
    private DataLoader dataLoader;

    @StreamListener(IMaintanceSink.INPUT_MAINTANCE)
    public void process(Message<?> message) {
        log.info(">>> func MaintanceSink: " +message.getPayload());
        DBWatchMessageEntity entity = JSONObject.parseObject((String)message.getPayload(), DBWatchMessageEntity.class);
        if (Objects.isNull(entity.getType())) { return; }

        //pending
        //需要优化
/*        if (entity.getTable().equals(PluginUseTable.PLUGIN_USE_HUB_TABLE)) {
            dataLoader.loadHub();
        }
        else if (entity.getTable().equals(PluginUseTable.PLUGIN_USE_METER_TABLE)) {
            dataLoader.loadMeter();
        }
        else if (entity.getTable().equals(PluginUseTable.HARDWARE_PLUGIN_TABLE)){
            dataLoader.loadPlugin();
        }*/
    }
}
