package com.smart.factory;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-15 15:25
 **/
public interface Builder {
    void add(String hubType, IHubService hubService);
}
