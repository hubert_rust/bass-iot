package com.smart.factory;

import com.google.common.collect.Maps;

import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-15 15:27
 **/
public interface HubFactory {
    IHubService create(String hubType);
    public static HubFactory factory(Consumer<Builder> consumer) {
        Map<String, IHubService> map = Maps.newHashMap();
        consumer.accept(map::put);
        return hubType ->  map.get(hubType);
    }
}
