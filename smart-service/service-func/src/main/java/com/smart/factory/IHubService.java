package com.smart.factory;

import com.smart.request.BassHardwardRequest;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-15 15:21
 **/
public interface IHubService {
    Object process(BassHardwardRequest request, String requestStr) throws Exception;
}
