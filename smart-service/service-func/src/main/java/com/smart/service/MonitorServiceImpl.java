package com.smart.service;

import com.smart.monitor.object.*;
import com.smart.entity.MonitorEntity;
import com.smart.repository.MonitorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @program: smart-service
 * @description: service impl
 * @author: Admin
 * @create: 2019-04-22 16:55
 **/
@Service
public class MonitorServiceImpl {
    private static final Logger log = LoggerFactory.getLogger(MonitorServiceImpl.class);

    @Autowired
    private MonitorRepository repostory;

    public MonitorEntity updateMonitor(DiagnosicObject obj, String state) {
        Optional<MonitorEntity> entity = repostory.findById(obj.getObjectId());
/*        Optional<MonitorEntity> entity = repostory.findOne(new Specification<MonitorEntity>() {
            @Override
            public Predicate toPredicate(Root<MonitorEntity> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get("objectId"), obj.getObjectId());
            }
        });*/
        if (!entity.isPresent()) {
            log.error(">>> MonitorServiceImpl, updateMonitor: {}", obj.toString());
            return initMonitorTable(obj);
        }
        String currentTm = LocalDateTime.now().toString();

        entity.get().setLastTime(Long.valueOf(currentTm));
        if (!state.equals(entity.get().getStatus())) {
            entity.get().setStatus(state);
            repostory.save(entity.get());
            return entity.get();
        }

        entity.get().setStatus(state);
        if (state.equals("ok")) {
            entity.get().setLastClear(currentTm);
        }
        else {
            entity.get().setLastFault(currentTm);
        }

        return repostory.save(entity.get());
    }

    /**
    * @Description: 系统启动的时候初始化
    * @Param:
    * @return:
    * @Author: admin
    * @Date: 2019/6/25
    */
    public MonitorEntity initMonitorTable(DiagnosicObject obj) {
        if (Objects.isNull(obj)) {
            return null;
        }
        else {
            Map<String, Object> map = obj.getData();
            String dcName = map.get("dc_name").toString();
            Integer dcId = Integer.valueOf(map.get("dc_id").toString());
            String conAddr = Objects.isNull(map.get("hub_address")) ? "": map.get("hub_address").toString();
            String subAddr = Objects.isNull(map.get("meter_address")) ? "": map.get("meter_address").toString();

            Optional<MonitorEntity> monitorEntity = repostory.findOne((root, query, builder) -> {
                return builder.equal(root.get("objectKey"), obj.getKey());
            });
            MonitorEntity entity = new MonitorEntity();
            if (monitorEntity.isPresent()) {
                entity = monitorEntity.get();
                entity.setStatus(obj.getState());
                entity.setModifyTime(LocalDateTime.now());
                repostory.save(entity);
                return entity;
            }
            else {
                entity.setMainType(obj.getMonitorClass());
                entity.setObjectKey(obj.getKey());
                entity.setConAddress(conAddr);
                entity.setMonitorType(obj.getMonitorType());
                entity.setPluginId(obj.getPluginId());
                entity.setSubAddress(subAddr);
                entity.setProbeTime(obj.getProbeSpan());
                entity.setStatus(obj.getState());
                entity.setCreateTime(LocalDateTime.now());
                return repostory.save(entity);
            }
        }
    }
}