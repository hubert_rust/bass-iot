package com.smart.service;

import com.google.common.base.Preconditions;
import com.smart.command.CommandCode;
import com.smart.config.DataLoader;
import com.smart.factory.IHubService;
import com.smart.framework.utils.HttpUtils;
import com.smart.recharge.HighMeterRechargeService;
import com.smart.recharge.RechargeType;
import com.smart.request.BassHardwardRequest;
import com.smart.toolkit.IDGenerator;
import org.apache.curator.shaded.com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Objects;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-15 13:43
 **/
@Service("HighPowerMeterServiceImpl")
@Scope("prototype")
public class HighPowerMeterServiceImpl implements IHubService {
    @Autowired
    HighMeterRechargeService rechargeService;

    @Override
    public synchronized Object process(BassHardwardRequest request, String requestStr) throws Exception {
        Object retObj = null;
        String pluginId = request.getServiceId();
        Map<String, Object> obj = (Map) DataLoader.pluginMap.get(pluginId);
        Preconditions.checkArgument(Objects.nonNull(obj),
                "通过pluginId获取大功率电表网关地址失败");

        String url = obj.get("plugin_url").toString();
        Preconditions.checkArgument(!Strings.isNullOrEmpty(url),
                "大功率电表网关地址配置不正确");

        if (request.getCmdCode().equals(CommandCode.CMD_METER_RECHARGE_PREPARE)) {
            try {
                request.setCommandOrder(IDGenerator.getIdStr());
                retObj = rechargeService.prepare(url, request);
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception(e.getMessage());
            }

        } else if (request.getCmdCode().equals(CommandCode.CMD_METER_RECHARGE)
                || request.getCmdCode().equals(CommandCode.CMD_METER_CLEAR)) { //pending
            if (request.getReserved().equals(RechargeType.RECHARGE_TYPE_APP)) {
                //app充值, 需要先生成订单
                try {
                    //request.setCommandOrder(IDGenerator.getIdStr());
                    retObj = rechargeService.serviceRecharge(url, request, null);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new Exception(e.getMessage());
                }
            }
            else {
                //
                try {
                    request.setCommandOrder(IDGenerator.getIdStr());
                    retObj = rechargeService.prepare(url, request);
                    retObj = rechargeService.serviceRecharge(url, request, null);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new Exception(e.getMessage());
                }
            }

        } else if (Objects.nonNull(obj)) {
            try {
                retObj = HttpUtils.commonSendHttpPost(url, requestStr);
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception(e.getMessage());
            }
        }
        return retObj;
    }
}
