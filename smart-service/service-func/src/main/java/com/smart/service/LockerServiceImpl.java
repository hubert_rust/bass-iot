package com.smart.service;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.smart.command.CommandCode;
import com.smart.common.ResultConst;
import com.smart.config.DataLoader;
import com.smart.factory.IHubService;
import com.smart.framework.utils.HttpUtils;
import com.smart.request.BassHardwardRequest;
import com.smart.toolkit.IDGenerator;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import static com.smart.framework.utils.ResponseUtils.getFailResponse;

/**
 * @program: smart-service
 * @description: 每个集中器一个实例，pending
 * @author: admin
 * @create: 2019-05-15 13:40
 **/
@Service("LockerServiceImpl")
@Scope("prototype")
public class LockerServiceImpl implements IHubService {
    private static final Logger log = LoggerFactory.getLogger(LockerServiceImpl.class);

    @Override
    public Object process(BassHardwardRequest request, String requestStr) throws Exception {
        Object retObj = null;
        if (request.getEntitys().size() <= 0) {
            return "entitys invalid";
        }

        String url = null;
        String pluginId = request.getServiceId();
        Map<String, Object> obj = (Map) DataLoader.pluginMap.get(pluginId);
        if (Objects.nonNull(obj)) {
            url = obj.get("plugin_url").toString();
        } else {
            return  getFailResponse("操作失败: 获取插件url失败");
        }

        //远程更新软件特殊处理
        if (request.getCmdCode().equals(CommandCode.CMD_LOCK_UPDATE_SOFTWARE)) {

            try {
                //升级超时设置为5分钟
                retObj = HttpUtils.commonSendHttpPost(url, requestStr, 5*60*1000);
            } catch (Exception e) {
                return  getFailResponse(e.getMessage());
            }

        } else {
            try {
                retObj = HttpUtils.commonSendHttpPost(url, requestStr);
            } catch (Exception e) {
                e.printStackTrace();
                return getFailResponse(e.getMessage());
            }
        }

        return retObj;
    }

    public static BassHardwardRequest getRequest(String hubAddress, String meterAddress) {

        BassHardwardRequest request = new BassHardwardRequest();
        request.setCommandOrder(IDGenerator.getIdStr());
        request.setCmdCode("CMD_METER_STATE");
        request.setCmdType("elec-meter");
        request.setHubAddress(hubAddress);
        request.setSubAddress(meterAddress);
        request.setServiceId("ELECMETER-PLUGIN-001");
        request.setServiceType("FUNC");
        request.setSubType("func");
        request.setIsRetRespBytes(1);
        request.setIsRetSendBytes(1);

        List<Map<String, Object>> list = Lists.newArrayList();
        Map<String, Object> map = Maps.newHashMap();
        map.put("hubAddress", hubAddress);
        map.put("meterAddress", meterAddress);
        map.put("bps", "6");
        map.put("commPort", "2");
        list.add(map);
        request.setEntitys(list);
        return request;
    }

    public static String makeChecksum(String data) {
        if (StringUtils.isEmpty(data)) {
            return "";
        }
        int total = 0;
        int len = data.length();
        int num = 0;
        while (num < len) {
            String s = data.substring(num, num + 2);
            total += Integer.parseInt(s, 16);
            num = num + 2;
        }
        /*
         * 用256求余最大是255，即16进制的FF
         */
        int mod = total % 256;
        String hex = Integer.toHexString(mod);
        len = hex.length();
        // 如果不够校验位的长度，补0,这里用的是两位校验
        if (len < 2) {
            hex = "0" + hex;
        }
        return hex;
    }

}
