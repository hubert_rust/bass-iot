package com.smart.service;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.smart.command.CommandCode;
import com.smart.common.ResponseEntity;
import com.smart.common.ResponseModel;
import com.smart.common.ResponseUtils;
import com.smart.common.ResultConst;
import com.smart.config.DataLoader;
import com.smart.entity.MeterRechargeEntity;
import com.smart.factory.IHubService;
import com.smart.framework.utils.HttpUtils;
import com.smart.msgtpl.MessageTpl;
import com.smart.recharge.AbstractRecharge;
import com.smart.recharge.ElecMeterRechargeService;
import com.smart.recharge.RechargeStages;
import com.smart.recharge.RechargeType;
import com.smart.request.BassHardwardRequest;
import com.smart.toolkit.IDGenerator;
import com.smart.utils.CommonUtils;
import org.apache.curator.shaded.com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-15 13:42
 **/
@Service("ElecMeterServiceImpl")
@Scope("prototype")
public class ElecMeterServiceImpl implements IHubService {
    private static final Logger log = LoggerFactory.getLogger(ElecMeterServiceImpl.class);

    @Autowired
    ElecMeterRechargeService rechargeService;

    @Override
    public  Object process(BassHardwardRequest request, String requestStr) throws Exception {
        Object retObj = null;
        String pluginId = request.getServiceId();
        Map<String, Object> obj = (Map) DataLoader.pluginMap.get(pluginId);
        Preconditions.checkArgument(Objects.nonNull(obj),
                "通过pluginId获取电表网关地址失败");

        String url = obj.get("plugin_url").toString();
        Preconditions.checkArgument(!Strings.isNullOrEmpty(url),
                "电表网关地址配置不正确");

        if (request.getCmdCode().equals(CommandCode.CMD_METER_RECHARGE_PREPARE)) {
            try {
                request.setCommandOrder(IDGenerator.getIdStr());
                retObj = rechargeService.prepare(url, request);
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception(e.getMessage());
            }

        }
        //清零，赠送，换表
        else if (request.getCmdCode().equals(CommandCode.CMD_METER_RECHARGE)
                || request.getCmdCode().equals(CommandCode.CMD_METER_CLEAR)) {
            if (request.getReserved().equals(RechargeType.RECHARGE_TYPE_APP)) {
                //app充值, 需要先生成订单
                try {
                    //request.setCommandOrder(IDGenerator.getIdStr());
                    retObj = rechargeService.serviceRecharge(url, request, null);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new Exception(e.getMessage());
                }
            }
            else {
                //
                try {
                    request.setCommandOrder(IDGenerator.getIdStr());
                    retObj = rechargeService.prepare(url, request);
                    retObj = rechargeService.serviceRecharge(url, request, null);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new Exception(e.getMessage());
                }
            }
        }
        else {
            try {
                retObj = HttpUtils.commonSendHttpPost(url, requestStr);
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception(e.getMessage());
            }
        }
        return retObj;
    }

    /**
    * @Description: prepare阶段尽可能失败就失败
    * @Param:
    * @return:
    * @Author: admin
    * @Date: 2019/7/26
    */
/*    public Object prepare(String url, BassHardwardRequest request) throws Exception {
        Object resp =null;
        Map<String, Object> retMap = null;
        String remark="";
        MeterRechargeEntity getEntity = null;
        try {
            super.prepare(url, request);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }

        MeterRechargeEntity entity = getEntityFromRequest(request);
        //充值前查询剩余金额
        BassHardwardRequest sender = MessageTpl.getMeterPrepareTpl(
                CommandCode.HUB_TYPE_ELECMETER,
                CommandCode.CMD_QUERY_LEFT_MONEY,
                request.getServiceId(),
                IDGenerator.getIdStr(),
                request.getHubAddress(),
                request.getSubAddress());

        try {
            resp = HttpUtils.commonSendHttpPost(url, JSONObject.toJSONString(sender));
            retMap = JSONObject.parseObject((String)resp, Map.class);
            ResultConst.ReturnObject ret = ResultConst.checkResult(retMap);
            if (ret.getCode().equals(ResultConst.SUCCESS)) {
                //更新task状态，remark
                String leftMoney = ResultConst.getDatasByKey(retMap, "leftMoney");
                remark += ("查询剩余金额成功:" + leftMoney + ";");
                entity.setBalance(leftMoney);
                entity.setRemark(remark);
                entity.setModifyTime(LocalDateTime.now());
                getEntity = saveEntity(entity);
            }
            else {
                remark += ("查询剩余金额失败");
                entity.setRemark(remark);
                entity.setModifyTime(LocalDateTime.now());
                getEntity = saveEntity(entity);
                throw new Exception("prepare阶段失败");
            }

        } catch (Exception e) {
            e.printStackTrace();
            entity.setRemark("查询剩余金额失败: " + e.getMessage());
            entity.setModifyTime(LocalDateTime.now());
            getEntity = saveEntity(entity);
            throw new Exception(e.getMessage());
        }
        //充值前查询充值次数
        sender = MessageTpl.getMeterPrepareTpl(
                CommandCode.HUB_TYPE_ELECMETER,
                CommandCode.CMD_TIMES_RECHARGE,
                request.getServiceId(),
                IDGenerator.getIdStr(),
                request.getHubAddress(),
                request.getSubAddress());
        try {
            resp = HttpUtils.commonSendHttpPost(url, JSONObject.toJSONString(sender));
            retMap = JSONObject.parseObject((String)resp, Map.class);
            ResultConst.ReturnObject ret = ResultConst.checkResult(retMap);
            if (ret.getCode().equals(ResultConst.SUCCESS)) {
                //更新task状态，remark
                String rechargeTimes = ResultConst.getDatasByKey(retMap, "rechargeTimes").trim();
                remark += ("查询充值次数成功:" + rechargeTimes + ";");
                //更新remark
                super.updateRechargeIndexRemark(getEntity.getId(), rechargeTimes, remark);
            }
            else {
                remark += ("查询充值次数失败:" + ret.getMessage() + ";");
                super.updateRemark(getEntity.getId(), remark);
                throw new Exception("prepare阶段失败");
            }

        } catch (Exception e) {
            e.printStackTrace();
            remark += ("查询充值次数失败:" + e.getMessage() + ";");
            super.updateRemark(getEntity.getId(), remark);
            throw new Exception("prepare阶段失败");
        }

        ResponseEntity responseModel = CommonUtils.getResponseModel();
        responseModel.setReturnCode(ResultConst.SUCCESS);
        responseModel.setResultCode(ResultConst.SUCCESS);
        responseModel.setResultMsg(ResultConst.STATE_OK);
        responseModel.setReturnMsg(ResultConst.STATE_OK);
        responseModel.setReturnMsg("prepare阶段成功");
        CommonUtils.setVal(responseModel, "commandOrder", getEntity.getCommandOrder());
        CommonUtils.setVal(responseModel, "serviceOrder", getEntity.getServiceOrder());
        CommonUtils.setVal(responseModel, "totalFee", getEntity.getRechargeAmount());
        CommonUtils.setVal(responseModel, "hubAddress", getEntity.getHubAddress());
        CommonUtils.setVal(responseModel, "meterAddress", getEntity.getMeterAddress());
        CommonUtils.setVal(responseModel, "result", ResultConst.SUCCESS);

        return JSONObject.toJSONString(responseModel);
    }*/
}
