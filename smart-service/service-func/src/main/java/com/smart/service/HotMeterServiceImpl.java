package com.smart.service;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Preconditions;
import com.smart.command.*;
import com.smart.common.PFConstDef;
import com.smart.common.ResponseEntity;
import com.smart.common.ResponseModel;
import com.smart.common.ResultConst;
import com.smart.config.DataLoader;
import com.smart.entity.HotMeterBalanceEntity;
import com.smart.entity.MeterEntiry;
import com.smart.factory.IHubService;
import com.smart.framework.utils.HttpUtils;
import com.smart.framework.utils.ResponseUtils;
import com.smart.recharge.HotMeterRechargeService;
import com.smart.recharge.RechargeType;
import com.smart.repository.HotMeterBalanceRepostory;
import com.smart.repository.MeterRepostory;
import com.smart.request.BassHardwardRequest;
import com.smart.toolkit.IDGenerator;
import com.smart.utils.CommonUtils;
import org.apache.curator.shaded.com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.named.NamedContextFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-15 13:41
 **/
@Service("HotMeterServiceImpl")
@Scope("prototype")
public class HotMeterServiceImpl implements IHubService {
    private static final Logger log = LoggerFactory.getLogger(HotMeterServiceImpl.class);

    @Autowired
    HotMeterRechargeService rechargeService;

    @Autowired
    MeterRepostory meterRepostory;

    @Autowired
    HotMeterBalanceRepostory balanceRepostory;


    @Override
    public synchronized Object process(BassHardwardRequest request, String requestStr) throws Exception {
        Object retObj = null;
        String pluginId = request.getServiceId();
        Map<String, Object> obj = (Map) DataLoader.pluginMap.get(pluginId);
        Preconditions.checkArgument(Objects.nonNull(obj),
                "通过pluginId获取热水表网关地址失败");

        String url = obj.get("plugin_url").toString();
        Preconditions.checkArgument(!Strings.isNullOrEmpty(url),
                "热水表网关地址配置不正确");

        if (request.getCmdCode().equals(CommandCode.CMD_METER_RECHARGE_PREPARE)) {
            try {
                request.setCommandOrder(IDGenerator.getIdStr());
                retObj = rechargeService.prepare(url, request);
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception(e.getMessage());
            }

        }
        //清零，赠送，换表
        else if (request.getCmdCode().equals(CommandCode.CMD_METER_RECHARGE)
                || request.getCmdCode().equals(CommandCode.CMD_METER_CLEAR)) {
            if (request.getReserved().equals(RechargeType.RECHARGE_TYPE_APP)) {
                //app充值, 需要先生成订单
                try {
                    //request.setCommandOrder(IDGenerator.getIdStr());
                    retObj = rechargeService.serviceRecharge(url, request, null);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new Exception(e.getMessage());
                }
            }
            else {
                //
                try {
                    request.setCommandOrder(IDGenerator.getIdStr());
                    retObj = rechargeService.prepare(url, request);
                    retObj = rechargeService.serviceRecharge(url, request, null);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new Exception(e.getMessage());
                }
            }
        }
        else {
            //查询用户列表
            if (request.getCmdCode().equals(CommandCode.CMD_QUERY_METER_USER)) {
                try {
                    retObj = queryMeterUser(url, request);
                } catch (Exception e) {
                    throw new Exception(e.getMessage());
                }
            }
            else if (request.getCmdCode().equals(CommandCode.CMD_BIND_USER)
                    || request.getCmdCode().equals(CommandCode.CMD_ADD_USER)) {
                try {
                    retObj = bindMeterUser(url, request);
                } catch (Exception e) {
                    throw new Exception(e.getMessage());
                }
            }
            else if (request.getCmdCode().equals(CommandCode.CMD_DEL_USER)) {
                try {
                    retObj = HttpUtils.commonSendHttpPost(url, requestStr, PFConstDef.HTTP_MILL_FUNC_REQUEST);
                    deleteBalance(request, retObj);

                } catch (Exception e) {
                    e.printStackTrace();
                    throw new Exception(e.getMessage());
                }
            }
            else {
                try {
                    retObj = HttpUtils.commonSendHttpPost(url, requestStr, PFConstDef.HTTP_MILL_FUNC_REQUEST);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new Exception(e.getMessage());
                }
            }
        }
        return retObj;
    }

    private void deleteBalance(BassHardwardRequest request, Object retObj) throws Exception {
        Map<String, Object> retMap = JSONObject.parseObject((String) retObj, Map.class);
        ResultConst.ReturnObject ret = ResultConst.checkResult(retMap);
        if (ret.getCode().equals(ResultConst.SUCCESS)) {
            String cardNo = CommonUtils.getBassRequestDatas(request, "cardNo");
            String userNo = CommonUtils.getBassRequestDatas(request, "userNo");
            Optional<HotMeterBalanceEntity> hotMeterBalanceEntity = balanceRepostory.findOne((root, query, builder)->{
                Predicate p1 = builder.equal(root.get("cardNo"), cardNo);
                Predicate p2 = builder.equal(root.get("userUid"), userNo);
                return builder.and(p1, p2);
            });
            if (hotMeterBalanceEntity.isPresent()) {
                balanceRepostory.deleteById(hotMeterBalanceEntity.get().getId());
            }
        }
    }

    private Object bindMeterUser(String url, BassHardwardRequest request) throws Exception {
        Object retObj = null;
        Preconditions.checkArgument(!Strings.isNullOrEmpty(request.getHubAddress()),
                "hubAddress field is invalid");

        String userName = CommonUtils.getBassRequestDatas(request, "userName");
        String userType = CommonUtils.getBassRequestDatas(request, "userType");
        String cardNo = CommonUtils.getBassRequestDatas(request, "cardNo");
        String userNo = CommonUtils.getBassRequestDatas(request, "userNo");
        String cardPasswd = CommonUtils.getBassRequestDatas(request, "cardPasswd");
        String balance = CommonUtils.getBassRequestDatas(request, "balance");
        String leftAmount = CommonUtils.getBassRequestDatas(request, "leftAmount");
        Preconditions.checkArgument(!Strings.isNullOrEmpty(cardNo), "cardNo is invalid");
        Preconditions.checkArgument(!Strings.isNullOrEmpty(userNo), "userNo is invalid");
        Preconditions.checkArgument(!Strings.isNullOrEmpty(cardPasswd), "cardPasswd is invalid");

        Optional<HotMeterBalanceEntity> hotMeterBalanceEntity = balanceRepostory.findOne((root, query, builder)->{
            Predicate p1 = builder.equal(root.get("cardNo"), cardNo);
            Predicate p2 = builder.equal(root.get("userUid"), userNo);
            return builder.and(p1, p2);
        });

        //
        if (hotMeterBalanceEntity.isPresent()) {
            if (hotMeterBalanceEntity.get().getMeterAddress().equals(request.getSubAddress())
                    && hotMeterBalanceEntity.get().getHubAddress().equals(request.getHubAddress())) {
                //查看绑卡状态, 如果没有绑卡
                //用户已经绑定成功
                if (hotMeterBalanceEntity.get().getBindStatus().equals(ResultConst.SUCCESS)) {
                    throw new Exception("该用户已经存在");
                }
                else {
                    //用户已经绑定，但是失败
                }
            }
            else {
                throw new Exception("this card may had bind to meter: "
                        + hotMeterBalanceEntity.get().getMeterAddress() + ":"
                        + hotMeterBalanceEntity.get().getHubAddress());
            }
        }


        Optional<MeterEntiry> meterEntiryOptional = meterRepostory.findOne((root, query, builder)-> {

            Predicate sp1 = builder.equal(root.get("hubAddress"), request.getHubAddress());
            Predicate sp2 = builder.equal(root.get("meterAddress"), request.getSubAddress());
            return builder.and(sp1, sp2);
        });

/*        Preconditions.checkArgument(!meterEntiryOptional.isPresent(),
                "hot meter or hub not config in platform!");*/

        if (false) {
        }
        else {
            //通过卡号没有查询到, 添加用户:cardNo, userNo, cardPasswd, leftAmount, balance
            LocalDateTime startTime = LocalDateTime.now();
            try {
                request.setCmdCode(CommandCode.CMD_ADD_USER);
                request.setCommandOrder(IDGenerator.getIdStr());
                retObj = HttpUtils.commonSendHttpPost(url, JSONObject.toJSONString(request), PFConstDef.HTTP_MILL_FUNC_REQUEST);
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception("绑卡失败: " + e.getMessage());
            }
            //
            Map<String, Object> retUserMap = JSONObject.parseObject((String)retObj, Map.class);
            ResultConst.ReturnObject ret = ResultConst.checkResult(retUserMap);
            if (!ret.getCode().equals(ResultConst.SUCCESS)) {
                throw new Exception("绑卡失败: " + ret.getMessage());
            }

            if (hotMeterBalanceEntity.isPresent()) {
                hotMeterBalanceEntity.get().setBindStatus(ret.getCode());
                hotMeterBalanceEntity.get().setBalance(Strings.isNullOrEmpty(balance) ? "0.00" : balance);
                hotMeterBalanceEntity.get().setLeftAmount(Strings.isNullOrEmpty(leftAmount) ? "0.00" : leftAmount);
                hotMeterBalanceEntity.get().setModifyTime(LocalDateTime.now());
                balanceRepostory.save(hotMeterBalanceEntity.get());
            }
            else {
                HotMeterBalanceEntity balanceEntity = new HotMeterBalanceEntity();
                balanceEntity.setUserName(userName);
                balanceEntity.setUserType(userType);
                balanceEntity.setUserUid(userNo);
                balanceEntity.setCardNo(cardNo);
                balanceEntity.setPassword(cardPasswd);
                balanceEntity.setBalance(Strings.isNullOrEmpty(balance) ? "0.00" : balance);
                balanceEntity.setLeftAmount(Strings.isNullOrEmpty(leftAmount) ? "0.00" : leftAmount);
                balanceEntity.setHubAddress(request.getHubAddress());
                balanceEntity.setMeterAddress(request.getSubAddress());
                balanceEntity.setBindStatus(ret.getCode());
                balanceEntity.setCreateTime(LocalDateTime.now());
                balanceEntity.setStartTime(startTime);
                balanceEntity.setFinishTime(LocalDateTime.now());
                balanceEntity.setCommandOrder(request.getCommandOrder());

                if (meterEntiryOptional.isPresent()) {
                    balanceEntity.setDcName(meterEntiryOptional.get().getDcName());
                    balanceEntity.setDcId(meterEntiryOptional.get().getDcId());
                }

                balanceRepostory.save(balanceEntity);
            }
        }

        return retObj;
    }
    private Object queryMeterUser(String url, BassHardwardRequest request) throws Exception {
        Object retObj = null;
        try {
            request.setCmdCode(CommandCode.CMD_QUERY_USER_NUMBER);
            retObj = HttpUtils.commonSendHttpPost(url, JSONObject.toJSONString(request), PFConstDef.HTTP_MILL_FUNC_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }

        Map<String, Object> retMap = JSONObject.parseObject((String)retObj, Map.class);
        ResultConst.ReturnObject ret = ResultConst.checkResult(retMap);
        Preconditions.checkArgument(ret.getCode().equals(ResultConst.SUCCESS),
                "查询热水用户失败");

        String userNums = ResultConst.getDatasByKey(retMap, "userNums");
        Preconditions.checkArgument(!Strings.isNullOrEmpty(userNums),
                "获取热水用户信息失败");

        //每次查询四个
        request.setCmdCode(CommandCode.CMD_QUERY_METER_USER);
        try {
            CommonUtils.setBassRequestDatas(request, "startIndex", "1");
            CommonUtils.setBassRequestDatas(request, "endIndex", "5");
            retObj = HttpUtils.commonSendHttpPost(url, JSONObject.toJSONString(request), PFConstDef.HTTP_MILL_FUNC_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }

        Map<String, Object> retUserMap = JSONObject.parseObject((String)retObj, Map.class);
        ret = ResultConst.checkResult(retUserMap);
        Preconditions.checkArgument(ret.getCode().equals(ResultConst.SUCCESS),
                "查询热水用户失败.");

        if (Integer.valueOf(userNums) <=5) {
            return retObj;
        }

        try {
            CommonUtils.setBassRequestDatas(request, "startIndex", "6");
            CommonUtils.setBassRequestDatas(request, "endIndex", userNums);
            retObj = HttpUtils.commonSendHttpPost(url, JSONObject.toJSONString(request), PFConstDef.HTTP_MILL_FUNC_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }

        Map<String, Object> retLastMap = JSONObject.parseObject((String)retObj, Map.class);
        ret = ResultConst.checkResult(retLastMap);
        Preconditions.checkArgument(ret.getCode().equals(ResultConst.SUCCESS),
                "查询热水用户失败..");
        List<Map<String, Object>> datasList1 = (List)retUserMap.get(ResultConst.DATAS);
        Map<String, Object> map1 = datasList1.get(0);
        List<Map<String, Object>> list1 = (List)map1.get("use_record_list");
        List<Map<String, Object>> datasList2 = (List)retLastMap.get(ResultConst.DATAS);
        Map<String, Object> map2 = datasList2.get(0);
        List<Map<String, Object>> list2 = (List)map2.get("use_record_list");
        list1.addAll(list2);

        return JSONObject.toJSONString(retUserMap);
    }

}
