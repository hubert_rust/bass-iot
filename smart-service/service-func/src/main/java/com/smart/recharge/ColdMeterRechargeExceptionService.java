package com.smart.recharge;

import com.smart.entity.MeterRechargeEntity;
import com.smart.entity.MeterRechargeExceptionEntity;
import org.springframework.stereotype.Component;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-09-29 09:33
 **/

@Component
public class ColdMeterRechargeExceptionService implements RechargeExceptionI{
    @Override
    public void process(String notifyUrl,
                        MeterRechargeEntity rechargeEntity) throws Exception {

    }


    public void retryProcess(MeterRechargeExceptionEntity exceptionEntity) throws Exception {

    }
}
