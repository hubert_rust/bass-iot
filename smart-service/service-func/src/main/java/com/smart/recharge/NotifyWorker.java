package com.smart.recharge;

import com.smart.framework.commonpool.Task;

import java.util.concurrent.TimeUnit;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-26 10:08
 **/
public class NotifyWorker extends Thread {
    Task task;
    Object object;

    public NotifyWorker(Task task, Object object) {
        this.task = task;
        this.object = object;
    }

    @Override
    public void run() {
        try {
            task.setObject(object);
            task.process();
        } catch (Exception e) {
            e.printStackTrace();
            retry();
        }
    }

    private void retry() {
        try {
            TimeUnit.SECONDS.sleep(3);
            task.retry();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
