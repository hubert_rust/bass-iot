package com.smart.recharge;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Preconditions;
import com.smart.command.CommandCode;
import com.smart.common.PFConstDef;
import com.smart.common.ResponseEntity;
import com.smart.common.ResultConst;
import com.smart.entity.ElecBalanceReadEntity;
import com.smart.entity.HighBalanceReadEntity;
import com.smart.entity.MeterRechargeEntity;
import com.smart.framework.utils.HttpUtils;
import com.smart.msgtpl.MessageTpl;
import com.smart.repository.ElecMeterBalanceRepostory;
import com.smart.request.BassHardwardRequest;
import com.smart.toolkit.IDGenerator;
import com.smart.utils.CommonUtils;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Predicate;
import javax.swing.text.html.Option;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-26 07:45
 **/
@Component("ElecMeterRechargeService")
public class ElecMeterRechargeService extends AbstractRecharge {
    private static final Logger log = LoggerFactory.getLogger(ElecMeterRechargeService.class);

    @Autowired
    ElecMeterBalanceRepostory balanceRepostory;

    @Override
    public void updateMeterBalance(MeterRechargeEntity entity, String rechargeType) throws Exception {
        BigDecimal left = new BigDecimal(entity.getBalance());
        BigDecimal rechargeAmount = new BigDecimal(entity.getRechargeAmount());
        BigDecimal balance = null; //left.add(rechargeAmount);
        if (rechargeType.equals(RechargeType.RECHARGE_TYPE_CLEAR)) {
            balance = new BigDecimal("0.00");
        }
        else {
            balance = left.add(rechargeAmount);
        }

        int retryNum = 0;
        while (retryNum <= 5) {

            Optional<ElecBalanceReadEntity> optEntity = balanceRepostory.findOne((root, query, builder) -> {
                Predicate p1 = builder.equal(root.get("hubAddress"), entity.getHubAddress());
                Predicate p2 = builder.equal(root.get("meterAddress"), entity.getMeterAddress());
                Predicate p3 = builder.equal(root.get("pluginId"), entity.getPluginId());
                return builder.and(p1, p2, p3);
            });

            if (optEntity.isPresent()) {
                int ret = balanceRepostory.updateElecMeterBalance(optEntity.get().getId(),
                        balance.toString(),
                        optEntity.get().getVersion(),
                        LocalDateTime.now(),
                        LocalDateTime.now(),
                        ResultConst.SUCCESS);
                if (ret == 1) {
                    break;
                }
            }
            else {
                log.error(">>> ElechMeterRechargeService, updateMeterBalance, get entity fail, meter:{}",
                        entity.getMeterAddress());
                break;
            }

            retryNum++;
        }
    }

    @Override
    public void getRechargeRequest(BassHardwardRequest request, MeterRechargeEntity entity) {
    }

    public Object prepare(String url, BassHardwardRequest request) throws Exception{

        Object resp =null;
        Map<String, Object> retMap = null;
        String remark="";
        MeterRechargeEntity getEntity = null;
        try {
            super.checkPrepare(url, request);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }

        MeterRechargeEntity entity = getEntityFromRequest(request);
        //充值前查询剩余金额
        BassHardwardRequest sender = MessageTpl.getMeterPrepareTpl(
                CommandCode.HUB_TYPE_ELECMETER,
                CommandCode.CMD_QUERY_LEFT_MONEY,
                request.getServiceId(),
                IDGenerator.getIdStr(),
                request.getHubAddress(),
                request.getSubAddress());

        try {
            resp = HttpUtils.commonSendHttpPost(url,
                    JSONObject.toJSONString(sender),
                    PFConstDef.HTTP_FUN_MILL_TIMEOUT);
            retMap = JSONObject.parseObject((String)resp, Map.class);
            ResultConst.ReturnObject ret = ResultConst.checkResult(retMap);
            if (ret.getCode().equals(ResultConst.SUCCESS)) {
                //更新task状态，remark
                String leftMoney = ResultConst.getDatasByKey(retMap, "leftMoney");
                //remark += ("查询剩余金额成功:" + leftMoney + ";");
                //这里不区分余额清零
/*                if (request.getCmdCode().equals(CommandCode.CMD_METER_CLEAR)) {
                    String clearBalance = new BigDecimal(leftMoney).multiply(new BigDecimal(-1)).toString();
                    entity.setRechargeAmount(clearBalance);
                    entity.setBalance(leftMoney);
                }
                else {
                }*/
                //
                String totalFee = CommonUtils.getBassRequestDatas(request, "totalFee");
                BigDecimal money = CommonUtils.convertMoney(Integer.valueOf(totalFee));
                entity.setRechargeAmount(money.toString());
                entity.setBalance(leftMoney);
                entity.setRemark(remark);
                entity.setModifyTime(LocalDateTime.now());
                //getEntity = saveEntity(entity);
            }
            else {
                remark += ("查询剩余金额失败");
                entity.setRemark(remark);
                entity.setPrepareStatus(RechargeStages.RECHARGE_PREPARE_STATUS_FAIL);
                entity.setModifyTime(LocalDateTime.now());
                getEntity = saveEntity(entity);
                throw new Exception("prepare阶段失败");
            }

        } catch (Exception e) {
            e.printStackTrace();
            entity.setRemark("查询剩余金额失败: " + e.getMessage());
            entity.setModifyTime(LocalDateTime.now());
            entity.setPrepareStatus(RechargeStages.RECHARGE_PREPARE_STATUS_FAIL);
            getEntity = saveEntity(entity);
            throw new Exception(e.getMessage());
        }
        //充值前查询充值次数
        sender = MessageTpl.getMeterPrepareTpl(
                CommandCode.HUB_TYPE_ELECMETER,
                CommandCode.CMD_TIMES_RECHARGE,
                request.getServiceId(),
                IDGenerator.getIdStr(),
                request.getHubAddress(),
                request.getSubAddress());
        try {
            resp = HttpUtils.commonSendHttpPost(url,
                    JSONObject.toJSONString(sender),
                    PFConstDef.HTTP_FUN_MILL_TIMEOUT);
            retMap = JSONObject.parseObject((String)resp, Map.class);
            ResultConst.ReturnObject ret = ResultConst.checkResult(retMap);
            if (ret.getCode().equals(ResultConst.SUCCESS)) {
                //更新task状态，remark
                String rechargeTimes = ResultConst.getDatasByKey(retMap, "rechargeTimes").trim();
                entity.setRechargeIndex(Long.valueOf(rechargeTimes));
                entity.setPrepareStatus(RechargeStages.RECHARGE_PREPARE_STATUS_SUCCESS);
                baseRepostory.save(entity);
                //remark += ("查询充值次数成功:" + rechargeTimes + ";");
                //更新remark
                /*super.updateRechargeIndexRemark(getEntity.getId(),
                        rechargeTimes,
                        RechargeStages.RECHARGE_PREPARE_STATUS_SUCCESS,
                        remark);*/
            }
            else {
                remark += ("查询充值次数失败:" + ret.getMessage() + ";");
                entity.setRemark(remark);
                entity.setPrepareStatus(RechargeStages.RECHARGE_PREPARE_STATUS_FAIL);
                baseRepostory.save(entity);
                /*super.updateRemark(getEntity.getId(),
                        RechargeStages.RECHARGE_PREPARE_STATUS_FAIL,
                        remark);*/
                throw new Exception("prepare阶段失败");
            }

        } catch (Exception e) {
            e.printStackTrace();
            remark += ("查询充值次数失败:" + e.getMessage() + ";");
            entity.setRemark(remark);
            entity.setPrepareStatus(RechargeStages.RECHARGE_PREPARE_STATUS_FAIL);
            baseRepostory.save(entity);
/*            super.updateRemark(getEntity.getId(),
                    RechargeStages.RECHARGE_PREPARE_STATUS_FAIL,
                    remark);*/
            throw new Exception("prepare阶段失败");
        }

        ResponseEntity responseModel = CommonUtils.getResponseModel();
        responseModel.setReturnCode(ResultConst.SUCCESS);
        responseModel.setResultCode(ResultConst.SUCCESS);
        responseModel.setResultMsg(ResultConst.STATE_OK);
        responseModel.setReturnMsg(ResultConst.STATE_OK);
        responseModel.setReturnMsg("prepare阶段成功");
        CommonUtils.setVal(responseModel, "commandOrder", entity.getCommandOrder());
        CommonUtils.setVal(responseModel, "serviceOrder", entity.getServiceOrder());
        CommonUtils.setVal(responseModel, "totalFee", entity.getRechargeAmount());
        CommonUtils.setVal(responseModel, "hubAddress", entity.getHubAddress());
        CommonUtils.setVal(responseModel, "meterAddress", entity.getMeterAddress());
        CommonUtils.setVal(responseModel, "result", ResultConst.SUCCESS);

        return JSONObject.toJSONString(responseModel);
    }

    /**
    * @Description: 充值阶段
    * @Param:
    * @return:
    * @Author: admin
    * @Date: 2019/7/26
    */
    public Object serviceRecharge(String url,
                           BassHardwardRequest request,
                           MeterRechargeEntity entity) throws Exception {

        Object retObj = null;
        LocalDateTime startTime = LocalDateTime.now();
        //通过serviceOrder找到对应的记录
        Optional<MeterRechargeEntity> rechargeEntity =  findOne((root, query, build)->{
            return build.equal(root.get("commandOrder"), request.getCommandOrder());
        });
        Preconditions.checkArgument(Objects.nonNull(rechargeEntity),
                "充值异常: 查询订单(" + request.getServiceOrder() + ")失败");

        //开始充值, 可以统一
        try {
            retObj = baseRecharge(url, request, rechargeEntity.get());
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }

        return retObj;
    }


}
