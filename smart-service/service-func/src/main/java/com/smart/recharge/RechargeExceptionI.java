package com.smart.recharge;

import com.smart.entity.MeterRechargeEntity;
import com.smart.entity.MeterRechargeExceptionEntity;

import java.time.LocalDateTime;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-09-29 09:34
 **/
public interface RechargeExceptionI {
    void process(String notifyUrl, MeterRechargeEntity rechargeEntity) throws Exception;
    default void convert(MeterRechargeEntity rechargeEntity, MeterRechargeExceptionEntity exceptionEntity) {
        exceptionEntity.setCommandOrder(rechargeEntity.getCommandOrder());
        exceptionEntity.setServiceOrder(rechargeEntity.getServiceOrder());
        exceptionEntity.setHubAddress(rechargeEntity.getHubAddress());
        exceptionEntity.setMeterAddress(rechargeEntity.getMeterAddress());
        exceptionEntity.setMeterType(rechargeEntity.getMeterType());
        exceptionEntity.setDcId(rechargeEntity.getDcId());
        exceptionEntity.setDcName(rechargeEntity.getDcName());
        exceptionEntity.setPluginId(rechargeEntity.getPluginId());
        exceptionEntity.setUserName(rechargeEntity.getUserName());
        exceptionEntity.setUserUid(rechargeEntity.getUserUid());
        exceptionEntity.setCreateTime(LocalDateTime.now());
        exceptionEntity.setMeterType(rechargeEntity.getUserType());

        exceptionEntity.setRechargeAmount(rechargeEntity.getCapacity());
        exceptionEntity.setBalance(rechargeEntity.getBalance());
        exceptionEntity.setRechargeBalance(rechargeEntity.getRechargeAmount());

        exceptionEntity.setUserNo(rechargeEntity.getUserNo());
        exceptionEntity.setCardNo(rechargeEntity.getCardNo());
        exceptionEntity.setUnit(rechargeEntity.getUnit());
        exceptionEntity.setReserved(rechargeEntity.getReserved());
        exceptionEntity.setRechargeType(rechargeEntity.getRechargeType());
        exceptionEntity.setRetryStatus1("NA");
        exceptionEntity.setRetryStatus2("NA");
        exceptionEntity.setRetryStatus3("NA");
    }
}
