package com.smart.recharge;

import com.smart.entity.MeterRechargeEntity;
import com.smart.request.BassHardwardRequest;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-25 08:43
 **/
public interface IRecharge {
    Object prepare(String url, BassHardwardRequest request) throws Exception;
    Object recharge(String url, BassHardwardRequest request, MeterRechargeEntity entity) throws Exception;
    Object resultNotify() throws Exception;
}
