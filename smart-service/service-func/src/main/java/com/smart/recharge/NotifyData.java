package com.smart.recharge;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-26 11:20
 **/
public class NotifyData {
    Long id;
    String remark;
    String notifyUrl;
    String notifyFireTime;
    String notifySendTime;
    String notifyRespTime;
    String notifyRequest;

    public NotifyData(Long id, String notifyUrl, String notifyRequest) {
        this.id = id;
        this.notifyUrl = notifyUrl;
        this.notifyRequest = notifyRequest;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getNotifyFireTime() {
        return notifyFireTime;
    }

    public void setNotifyFireTime(String notifyFireTime) {
        this.notifyFireTime = notifyFireTime;
    }

    public String getNotifySendTime() {
        return notifySendTime;
    }

    public void setNotifySendTime(String notifySendTime) {
        this.notifySendTime = notifySendTime;
    }

    public String getNotifyRespTime() {
        return notifyRespTime;
    }

    public void setNotifyRespTime(String notifyRespTime) {
        this.notifyRespTime = notifyRespTime;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getNotifyRequest() {
        return notifyRequest;
    }

    public void setNotifyRequest(String notifyRequest) {
        this.notifyRequest = notifyRequest;
    }
}
