package com.smart.recharge;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Preconditions;
import com.smart.command.CommandCode;
import com.smart.command.CommandVal;
import com.smart.common.PFConstDef;
import com.smart.common.ResponseEntity;
import com.smart.common.ResultConst;
import com.smart.entity.MeterEntiry;
import com.smart.entity.MeterRechargeEntity;
import com.smart.framework.utils.HttpUtils;
import com.smart.repository.MeterRepostory;
import com.smart.request.BassHardwardRequest;
import com.smart.service.BaseServiceImpl;
import com.smart.toolkit.IDGenerator;
import com.smart.toolkit.StringUtils;
import com.smart.utils.CommonUtils;
import org.jboss.netty.util.internal.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.criteria.Predicate;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-25 08:45
 **/
public abstract class AbstractRecharge  extends BaseServiceImpl<MeterRechargeEntity>  {

    @Autowired
    MeterRepostory meterRepostory;
    @Autowired
    AbstractRechargeException rechargeException;

    public abstract void getRechargeRequest(BassHardwardRequest request, MeterRechargeEntity entity);
    public abstract void updateMeterBalance(MeterRechargeEntity entity, String rechargeType) throws Exception;

    public Object  checkPrepare(String url, BassHardwardRequest request) throws Exception {
        //check
        Preconditions.checkArgument(Objects.nonNull(request.getEntitys()),
                "参数entitys非法");
        Preconditions.checkArgument(request.getEntitys().size() >0,
                "参数entitys非法");
        Map<String, Object> map = (Map)request.getEntitys().get(0);
        Preconditions.checkArgument(Objects.nonNull(map.get("userType")),
        "参数userType未指定");
        Preconditions.checkArgument(Objects.nonNull(map.get("userUid")),
                "参数userUid未指定");
        Preconditions.checkArgument(Objects.nonNull(map.get("userName")),
                "参数userName未指定");
        Preconditions.checkArgument(Objects.nonNull(map.get("totalFee")),
                "参数totalFee未指定");
/*        Preconditions.checkArgument(Objects.nonNull(map.get("rechargeType")),
                "参数rechargeType未指定");*/
        String totalFee = map.get("totalFee").toString();
        /*Preconditions.checkArgument(org.apache.commons.lang.StringUtils.isNumeric(totalFee),
                "参数totalFee非法");*/

        if (request.getCmdCode().equals(CommandCode.CMD_METER_RECHARGE)) {
            if (request.getReserved().equals(RechargeType.RECHARGE_TYPE_SWITCH)) {

            }
            else {
                Preconditions.checkArgument(Integer.valueOf(totalFee) > 0,
                        "参数totalFee非法");
            }
        }
        if (request.getCmdCode().equals(CommandCode.CMD_METER_CLEAR)) {
            Preconditions.checkArgument(Integer.valueOf(totalFee) == 0,
                    "参数totalFee非法");
        }

        return null;

    };

    private void notify(String notifyUrl,
                        BassHardwardRequest request,
                        Map<String, Object> retMap,
                        MeterRechargeEntity entity) {
        if (request.getReserved().equals(RechargeType.RECHARGE_TYPE_APP)
                && (request.getAutoPush() == RechargeConst.RECHARGE_NOTIFY_ASYNC_1)) {
            Object resp = getNotifyResponse(notifyUrl, retMap, entity);
            notifyResult(resp);
        }
    }

    /**
    * @Description: 统一充值处理, 注意热水处理
    * @Param: [url, request, entity]
    * @return: java.lang.Object
    * @Author: admin
    * @Date: 2019/7/26
    */
    public Object baseRecharge(String url,
                               BassHardwardRequest request,
                               MeterRechargeEntity entity) throws Exception{
        Object resp = null;
        Object syncResp = null;
        LocalDateTime startTime = LocalDateTime.now();
        Map<String, Object> retMap = null;
        String remark = entity.getRemark();

        if (entity.getTaskStatus().equals(ResultConst.SUCCESS)
                && request.getServiceOrder().equals(entity.getServiceOrder())) {
            throw new Exception("该订单已经充值成功, 请勿重复充值");
        }

        String notifyUrl = CommonUtils.getBassRequestDatas(request,
                RechargeConst.RECHARGE_ELEC_DATAS_FD_NOTIFYURL);
        //String sign = CommonUtils.getBassRequestDatas(request,
        //        RechargeConst.RECHARGE_ELEC_DATAS_FD_SIGN);

        try {
            checkPrepare(url, request);
            rechargeCheck(request, entity);
            updateRechargeStart(entity.getId(),
                    startTime,
                    LocalDateTime.now(),
                    RechargeStages.RECHARGE_STAGES_RECHARGING,
                    RechargeStages.RECHARGE_TASK_STATUS_PENDING,
                    remark);
        } catch (Exception e) {
            notify(notifyUrl, request, null, entity);
            updateRechargeStart(entity.getId(),
                    startTime,
                    LocalDateTime.now(),
                    RechargeStages.RECHARGE_STAGES_RECHARGING,
                    RechargeStages.RECHARGE_TASK_STATUS_PENDING,
                    remark + ";充值校验失败:" + e.getMessage() + ";");
            throw new Exception(e.getMessage());
        }

        //getRechargeRequest(request, entity);

        //开始充值, 需要对集中器进行加锁, pending
        if (request.getCmdType().equals(CommandCode.HUB_TYPE_ELECMETER)) {
            CommonUtils.setBassRequestDatas(request,
                    RechargeConst.RECHARGE_ELEC_DATAS_FD_TIMES,
                    String.valueOf(entity.getRechargeIndex()));

            //区分下充值和清零
            if (request.getCmdCode().equals(CommandCode.CMD_METER_RECHARGE)) {
                CommonUtils.setBassRequestDatas(request,
                        RechargeConst.RECHARGE_ELEC_DATAS_FD_BALANCE,
                        entity.getRechargeAmount());
            }
            else if (request.getCmdCode().equals(CommandCode.CMD_METER_CLEAR)) {
                BigDecimal recharge = new BigDecimal(entity.getBalance())
                        .multiply(new BigDecimal("-1"));
                CommonUtils.setBassRequestDatas(request,
                        RechargeConst.RECHARGE_ELEC_DATAS_FD_BALANCE,
                        recharge.toString());
            }

            request.setCmdCode(CommandCode.CMD_METER_RECHARGE);
        }
        else if (request.getCmdType().equals(CommandCode.HUB_TYPE_HIGHMETER)) {
            if (request.getCmdCode().equals(CommandCode.CMD_METER_RECHARGE)) {
                request.setCmdCode(CommandCode.HIGH_POWER_RECHARGE);
                CommonUtils.setBassRequestDatas(request,
                        RechargeConst.RECHARGE_HIGH_DATAS_FD_TIMES,
                        String.valueOf(entity.getRechargeIndex()));
                CommonUtils.setBassRequestDatas(request,
                        RechargeConst.RECHARGE_HIGH_DATAS_FD_BALANCE,
                        entity.getCapacity());
            }
            else if (request.getCmdCode().equals(CommandCode.CMD_METER_CLEAR)) {
                request.setCmdCode(CommandCode.HIGH_POWER_CLOSE_ACCOUNT);
            }

        }
        else if (request.getCmdType().equals(CommandCode.HUB_TYPE_HOTMETER)) {
            if (request.getCmdCode().equals(CommandCode.CMD_METER_RECHARGE)) {
                BigDecimal capacity =new BigDecimal(entity.getCapacity());
                BigDecimal totalAmount = new BigDecimal(entity.getReserved());
                BigDecimal balance = new BigDecimal(entity.getBalance());
                BigDecimal recharge = new BigDecimal(entity.getRechargeAmount());
                balance = balance.add(recharge);
                totalAmount = totalAmount.add(capacity);
                CommonUtils.setBassRequestDatas(request, "balance", balance.toString());
                CommonUtils.setBassRequestDatas(request, "leftAmount", totalAmount.toString());
            }
            else if (request.getCmdCode().equals(CommandCode.CMD_METER_CLEAR)) {

                CommonUtils.setBassRequestDatas(request, "balance", "0.00");
                CommonUtils.setBassRequestDatas(request, "leftAmount", "0.00");
            }

            request.setCmdCode(CommandCode.CMD_METER_RECHARGE);
        }
        else if (request.getCmdType().equals(CommandCode.HUB_TYPE_COLDMETER)) {
            BigDecimal capacity =new BigDecimal(entity.getCapacity());
            BigDecimal total = new BigDecimal(entity.getReserved());
            BigDecimal balance = null;
            if (request.getCmdCode().equals(CommandCode.CMD_METER_RECHARGE)) {
                balance = capacity.add(total);
            }
            else if (request.getCmdCode().equals(CommandCode.CMD_METER_CLEAR)) {
                BigDecimal left = new BigDecimal(entity.getBalance());
                balance = total.subtract(left)
                        .multiply(new BigDecimal(1))
                        .setScale(2, BigDecimal.ROUND_HALF_UP);
            }

            request.setCmdCode(CommandCode.CMD_METER_RECHARGE);
            CommonUtils.setBassRequestDatas(request,
                    RechargeConst.RECHARGE_COLD_DATAS_FD_BALANCE,
                    balance.toString());
        }

        try {
            syncResp = HttpUtils.commonSendHttpPost(url,
                    JSONObject.toJSONString(request),
                    PFConstDef.HTTP_FUN_MILL_TIMEOUT);
            retMap = JSONObject.parseObject((String)syncResp, Map.class);
            ResultConst.ReturnObject ret = ResultConst.checkResult(retMap);
            if (ret.getCode().equals(ResultConst.SUCCESS)) {
                notify(notifyUrl, request, retMap, entity);
                updateRechargeStart(entity.getId(),
                        startTime,
                        LocalDateTime.now(),
                        RechargeStages.RECHARGE_STAGES_RECHARGE_NOTIFY,
                        RechargeStages.RECHARGE_TASK_STATUS_SUCCESS,
                        entity.getRemark() + "充值成功;");
                //充值成功需要更新balance表，pending
                updateMeterBalance(entity, request.getReserved());
            }
            else {
                //记录充值异常
                rechargeException.process(notifyUrl, entity);
                notify(notifyUrl, request, retMap, entity);
                updateRechargeStart(entity.getId(),
                        startTime,
                        LocalDateTime.now(),
                        RechargeStages.RECHARGE_STAGES_RECHARGING,
                        RechargeStages.RECHARGE_TASK_STATUS_FAIL,
                        entity.getRemark() + "充值失败:" + ret.getMessage() + ";");
            }

        } catch (Exception e) {
            //记录充值异常
            rechargeException.process(notifyUrl, entity);
            e.printStackTrace();
            notify(notifyUrl, request, null, entity);
            updateRechargeStart(entity.getId(),
                    startTime,
                    LocalDateTime.now(),
                    RechargeStages.RECHARGE_STAGES_RECHARGING,
                    RechargeStages.RECHARGE_TASK_STATUS_FAIL,
                    entity.getRemark() + "充值失败:" + e.getMessage() + ";");

            throw new Exception(e.getMessage());
        }

        return (syncResp);
    }

    public static void main(String[] args) {
        String totalFee = new BigDecimal("0.30").multiply(new BigDecimal(100)).toString();
        System.out.println();
    }
    private void notifyResult(Object notify) {
        NotifyTheadPoolManage.submitNotifyTask(notify);
    }

    /**
    * @Description: 1、充值前校验失败通知（通过notifyUrl）,2、充值结果通知
    * @Param:
    * @return:
    * @Author: admin
    * @Date: 2019/7/26
    */
    protected NotifyData getNotifyResponse(String notifyUrl,
                                       Map<String, Object> mapRet,
                                       MeterRechargeEntity getEntity) {

        ResultConst.ReturnObject ret = ResultConst.checkResult(mapRet);
        ResponseEntity responseModel = CommonUtils.getResponseModel();
        responseModel.setReturnCode(Objects.isNull(mapRet) ? ResultConst.FAIL : ret.getCode());
        responseModel.setResultCode(Objects.isNull(mapRet) ? ResultConst.FAIL : ret.getCode());
        responseModel.setReturnMsg(ret.getMessage());

        String totalFee = new BigDecimal(getEntity.getRechargeAmount())
                .multiply(new BigDecimal(100))
                .setScale(0, RoundingMode.HALF_UP).toString();
        Long id = 0L;

        if (Objects.nonNull(getEntity)) {

            CommonUtils.setVal(responseModel, "cmdType", getEntity.getMeterType());
            CommonUtils.setVal(responseModel, "commandOrder", getEntity.getCommandOrder());
            CommonUtils.setVal(responseModel, "serviceOrder", getEntity.getServiceOrder());
            CommonUtils.setVal(responseModel, "totalFee",     totalFee);
            CommonUtils.setVal(responseModel, "hubAddress",   getEntity.getHubAddress());
            CommonUtils.setVal(responseModel, "meterAddress", getEntity.getMeterAddress());
            CommonUtils.setVal(responseModel, "result",       ret.getCode());
            id = getEntity.getId();
        }
        NotifyData notifyData = new NotifyData(id,
                notifyUrl,
                JSONObject.toJSONString(responseModel));
        notifyData.setRemark(Objects.nonNull(getEntity) ? getEntity.getRemark() : "");
        notifyData.setNotifyFireTime(String.valueOf(System.currentTimeMillis()));

        return (notifyData);
    }
    protected Object rechargeCheck(BassHardwardRequest request,
                                   MeterRechargeEntity entity) throws Exception {

        String var1 = CommonUtils.getBassRequestDatas(request, "userUid");
        Preconditions.checkArgument(entity.getUserUid().equals(var1),
                "充值用户UID和订单用户UID不一致");
        var1 = CommonUtils.getBassRequestDatas(request, "userType");
        Preconditions.checkArgument(entity.getUserType().equals(var1),
                "用户类型和订单用户类型不一致");
        //var1 = CommonUtils.getBassRequestDatas(request, "rechargeType");
        var1 = request.getReserved();
        Preconditions.checkArgument(entity.getRechargeType().equals(var1),
                "充值类型和订单充值类型不一致");
        if (!request.getReserved().equals(RechargeType.RECHARGE_TYPE_CLEAR)){
            var1 = CommonUtils.getBassRequestDatas(request, "totalFee");
            var1 = CommonUtils.convertMoney(Integer.valueOf(var1)).toString();
            Preconditions.checkArgument(entity.getRechargeAmount().equals(var1),
                    "充值金额和订单金额不一致");
        }

        Preconditions.checkArgument(request.getHubAddress().equals(entity.getHubAddress()),
        "集中器地址和订单集中器地址不一致");
        Preconditions.checkArgument(request.getSubAddress().equals(entity.getMeterAddress()),
                "仪表地址和订单中仪表地址不一致");

        return null;
    }

    public Object resultNotify() throws Exception {
        return null;
    }

/*    public static void main(String[] args) {
        String val = "-123";
        Integer var1 = Integer.valueOf(val);
        String ret = CommonUtils.convertMoney(var1).toString();
        System.out.println(ret);
    }*/
    protected MeterRechargeEntity getEntityFromRequest(BassHardwardRequest request) {

        Optional<MeterEntiry> meterEntiry = meterRepostory.findOne((root, query, builder) -> {
            Predicate p1 = builder.equal(root.get("pluginId"), request.getServiceId());
            Predicate p2 = builder.equal(root.get("hubAddress"), request.getHubAddress());
            Predicate p3 = builder.equal(root.get("meterAddress"), request.getSubAddress());
            return builder.and(p1, p2, p3);
        });

        MeterRechargeEntity rechargeEntity = new MeterRechargeEntity();

        rechargeEntity.setCreateTime(LocalDateTime.now());
        rechargeEntity.setPluginId(request.getServiceId());
        rechargeEntity.setServiceOrder(request.getServiceOrder());
        rechargeEntity.setCommandOrder(request.getCommandOrder());
        rechargeEntity.setHubAddress(request.getHubAddress());
        rechargeEntity.setMeterAddress(request.getSubAddress());
        rechargeEntity.setMeterType(request.getCmdType());
        Map<String, Object> datas = (Map)request.getEntitys().get(0);
        String userUid  = datas.get("userUid").toString();
        String userType = datas.get("userType").toString();
        String userName = datas.get("userName").toString();
        String totalFee = datas.get("totalFee").toString();

        String rechargeType = request.getReserved();
        rechargeEntity.setRechargeType(rechargeType);
        rechargeEntity.setUserName(userName);
        rechargeEntity.setUserType(userType);
        rechargeEntity.setUserUid(userUid);

        if (meterEntiry.isPresent()) {
            rechargeEntity.setDcId(meterEntiry.get().getDcId());
            rechargeEntity.setDcName(meterEntiry.get().getDcName());
        }

        rechargeEntity.setRechargeAmount(CommonUtils.convertMoney(Integer.valueOf(totalFee)).toString());
        rechargeEntity.setRechargeStages(RechargeStages.RECHARGE_STAGES_PREPARE);
        rechargeEntity.setTaskStatus(RechargeStages.RECHARGE_TASK_STATUS_PENDING);
        if (request.getCmdType().equals(CommandCode.HUB_TYPE_ELECMETER)) {
            rechargeEntity.setUnit(RechargeConst.RECHARGE_METER_UNIT_YUAN);
        }
        else if (request.getCmdType().equals(CommandCode.HUB_TYPE_HOTMETER)) {
            rechargeEntity.setUnit(RechargeConst.RECHARGE_METER_UNIT_L);
        }
        else if (request.getCmdType().equals(CommandCode.HUB_TYPE_COLDMETER)) {
            rechargeEntity.setUnit(RechargeConst.RECHARGE_METER_UNIT_CUMETER);
        }
        else if (request.getCmdType().equals(CommandCode.HUB_TYPE_HIGHMETER)) {
            rechargeEntity.setUnit(RechargeConst.RECHARGE_METER_UNIT_DEGREE);
        }

        return rechargeEntity;
    }

    public static BigDecimal getAmount(String money, String price) {
        BigDecimal priceBig = new BigDecimal(price);
        BigDecimal moneyBig = new BigDecimal(money);
        BigDecimal amount = moneyBig.divide(priceBig, RoundingMode.HALF_UP);
        BigDecimal ret = amount.setScale(2, RoundingMode.HALF_UP);
        return ret;
    }
}
