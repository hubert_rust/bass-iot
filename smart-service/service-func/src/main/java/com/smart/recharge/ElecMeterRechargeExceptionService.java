package com.smart.recharge;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.smart.command.CommandCode;
import com.smart.command.CommandVal;
import com.smart.common.PFConstDef;
import com.smart.common.ResultConst;
import com.smart.config.DataLoader;
import com.smart.entity.MeterRechargeEntity;
import com.smart.entity.MeterRechargeExceptionEntity;
import com.smart.framework.utils.HttpUtils;
import com.smart.request.BassHardwardRequest;
import com.smart.service.BaseServiceImpl;
import com.smart.toolkit.IDGenerator;
import com.smart.utils.CommonUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-09-29 09:33
 **/

@Component
public class ElecMeterRechargeExceptionService extends BaseServiceImpl<MeterRechargeExceptionEntity> implements RechargeExceptionI{
    @Override
    public void process(String notifyUrl,
                        MeterRechargeEntity rechargeEntity) throws Exception {
        MeterRechargeExceptionEntity exceptionEntity = new MeterRechargeExceptionEntity();
        convert(rechargeEntity, exceptionEntity);
        saveEntity(exceptionEntity);
    }

    public void retryProcess(MeterRechargeExceptionEntity exceptionEntity) throws Exception {
        Map<String, Object> obj = (Map) DataLoader.pluginMap.get(exceptionEntity.getPluginId());
        Preconditions.checkArgument(Objects.nonNull(obj),
                "通过pluginId获取网关地址失败");
        String url = obj.get("plugin_url").toString();
        BassHardwardRequest request = new BassHardwardRequest();
        request.setCommandOrder(IDGenerator.getIdStr());
        request.setHubAddress(exceptionEntity.getHubAddress());
        request.setSubAddress(exceptionEntity.getMeterAddress());
        request.setCmdCode(CommandCode.CMD_QUERY_LEFT_MONEY);
        request.setCmdType(CommandCode.HUB_TYPE_ELECMETER);
        request.setServiceId(exceptionEntity.getPluginId());
        request.setServiceType("PLUGIN");
        request.setSubType("func");

        List<Map<String, Object>> list = Lists.newArrayList();
        Map<String, Object> map = Maps.newHashMap();
        map.put("hubAddress", exceptionEntity.getHubAddress());
        map.put("commPort", "2");
        map.put("bps", "6");
        map.put("meterAddress", exceptionEntity.getMeterAddress());
        map.put("pluginId", exceptionEntity.getPluginId());
        map.put("meterType", exceptionEntity.getMeterType());
        list.add(map);
        request.setEntitys(list);
        Object syncResp = HttpUtils.commonSendHttpPost(url,
                JSONObject.toJSONString(request),
                PFConstDef.HTTP_FUN_MILL_TIMEOUT);
        Map<String, Object> retMap = JSONObject.parseObject((String)syncResp, Map.class);
        ResultConst.ReturnObject ret = ResultConst.checkResult(retMap);
        if (ret.getCode().equals(ResultConst.SUCCESS)) {
            String leftMoney = CommonUtils.getBassRequestDatas(request,
                    RechargeConst.RECHARGE_ELEC_DATAS_FD_LEFT);

            BigDecimal recharge = new BigDecimal(exceptionEntity.getRechargeBalance());
            BigDecimal balance = new BigDecimal(exceptionEntity.getBalance());
            recharge = recharge.add(balance);

            //充值成功
            if (leftMoney.equals(recharge.toString())) {
                exceptionEntity.setRetryStatus1(ResultConst.SUCCESS);
            }
            else if (leftMoney.equals(balance.toString())){
                exceptionEntity.setRetryStatus1(ResultConst.FAIL);
            }
            else {
                exceptionEntity.setRetryStatus1(ResultConst.UNKNOW);
            }
            exceptionEntity.setRemark1("leftMoney: " + leftMoney);
            saveEntity(exceptionEntity);
        }
        else {
            exceptionEntity.setRemark1(ret.getMessage());
            saveEntity(exceptionEntity);
        }
    }
}
