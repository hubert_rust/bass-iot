package com.smart.recharge;

import com.alibaba.fastjson.JSONObject;
import com.smart.common.PFConstDef;
import com.smart.common.ResultConst;
import com.smart.entity.MeterRechargeEntity;
import com.smart.framework.commonpool.Task;
import com.smart.framework.utils.HttpUtils;
import com.smart.repository.MeterRechargeRepostory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-26 10:02
 **/
@Component("NotifyRechargeTask")
public class NotifyRechargeTask extends Task {
    private static final Logger log = LoggerFactory.getLogger(NotifyRechargeTask.class);

    @Autowired
    MeterRechargeRepostory rechargeRepostory;

    @Override
    public void process() throws Exception {
        Object resp = null;
        Map<String, Object> retMap = null;

        NotifyData notifyData = (NotifyData) getObject();
        try {
            resp = sendNotify(notifyData.getNotifyUrl(), notifyData.getNotifyRequest(), 0);
        } catch (Exception e) {
            //e.printStackTrace();
            retry();
        }
    }

    private Object sendNotify(String url, String request, int retry) throws Exception {

        Object resp = null;
        Map<String, Object> retMap = null;

        NotifyData notifyData = (NotifyData) getObject();
        String remark = notifyData.getRemark();
        //notify0:[fireTime=1355,sendTime=122,respTime=ddd,FAIL=];
        //notify1:[fireTime=1355,sendTime=122,respTime=ddd,SUCCESS];
        remark += ("notify" + retry + ":[fireTime=" + notifyData.getNotifyFireTime());
        remark += (",sendTime=" + System.currentTimeMillis());
        remark += (",respTime=");

        try {
            resp = HttpUtils.commonSendHttpPost(notifyData.getNotifyUrl(),
                    notifyData.getNotifyRequest(),
                    PFConstDef.HTTP_FUN_MILL_NOTIFY);
            retMap = JSONObject.parseObject((String)resp, Map.class);
            ResultConst.ReturnObject ret = ResultConst.checkResult(retMap);
            remark += (System.currentTimeMillis() + ",SUCCESS]");
            if (ret.getCode().equals(ResultConst.SUCCESS)) {
                rechargeRepostory.updateRechargeNotify(notifyData.getId(),
                        RechargeStages.RECHARGE_STAGES_RECHARGE_FINISH,
                        RechargeStages.RECHARGE_NOTIFY_STATUS_SUCCESS,
                        remark);
            }
            else {
                remark += (System.currentTimeMillis() + ",FAIL=");
                remark += (ret.getMessage() + "];");
                rechargeRepostory.updateRechargeNotify(notifyData.getId(),
                        RechargeStages.RECHARGE_STAGES_RECHARGE_NOTIFY,
                        RechargeStages.RECHARGE_NOTIFY_STATUS_FAIL,
                        remark);
                throw new Exception(ret.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();

            remark += (System.currentTimeMillis() + ",FAIL=");
            remark += (e.getMessage() + "];");
            rechargeRepostory.updateRechargeNotify(notifyData.getId(),
                    RechargeStages.RECHARGE_STAGES_RECHARGE_FINISH,
                    RechargeStages.RECHARGE_NOTIFY_STATUS_FAIL,
                    remark);
            throw new Exception(e.getMessage());
        }

        return resp;
    }
    @Override
    public void retry() throws Exception {
        Object resp = null;
        Map<String, Object> retMap = null;

        NotifyData notifyData = (NotifyData) getObject();
        Optional<MeterRechargeEntity> entity = rechargeRepostory.findById(notifyData.getId());
        if (entity.isPresent()) {
            notifyData.setRemark(entity.get().getRemark());
            notifyData.setNotifyFireTime(String.valueOf(System.currentTimeMillis()));
            setObject(notifyData);
            resp = sendNotify(notifyData.getNotifyUrl(), notifyData.getNotifyRequest(), 1);
        }
        else {
            rechargeRepostory.updateRechargeNotify(notifyData.getId(),
                    RechargeStages.RECHARGE_STAGES_RECHARGE_FINISH,
                    RechargeStages.RECHARGE_NOTIFY_STATUS_FAIL,
                    entity.get().getRemark() + "重试通知时获取充值记录失败;");
        }
    }
}
