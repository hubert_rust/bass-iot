package com.smart.recharge;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.smart.command.CommandCode;
import com.smart.common.PFConstDef;
import com.smart.common.ResponseEntity;
import com.smart.common.ResultConst;
import com.smart.entity.HighBalanceReadEntity;
import com.smart.entity.MeterEntiry;
import com.smart.entity.MeterRechargeEntity;
import com.smart.framework.utils.HttpUtils;
import com.smart.msgtpl.MessageTpl;
import com.smart.repository.HighMeterBalanceRepostory;
import com.smart.repository.HighMeterReadRepostory;
import com.smart.repository.MeterRepostory;
import com.smart.request.BassHardwardRequest;
import com.smart.toolkit.IDGenerator;
import com.smart.utils.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Predicate;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-26 07:45
 **/
@Component("HighMeterRechargeService")
public class HighMeterRechargeService extends AbstractRecharge {
    private static final Logger log = LoggerFactory.getLogger(HighMeterRechargeService.class);

    @Autowired
    HighMeterBalanceRepostory balanceRepostory;

    /**
     * @Description:
     * @Param: recharge: 本次充值的量; rechargeBeforeLeft: 冲量前剩余的量
     * @return:
     * @Author: admin
     * @Date: 2019/7/29
     */
    @Override
    public void updateMeterBalance(MeterRechargeEntity entity, String rechargeType) throws Exception {
        BigDecimal left = new BigDecimal(entity.getBalance());
        BigDecimal rechargeAmount = new BigDecimal(entity.getCapacity());
        if (rechargeType.equals(RechargeType.RECHARGE_TYPE_CLEAR)) {
            BigDecimal balance = new BigDecimal("0.00");
            tryUpdateBalance(balance, entity);
        }
        else {
            BigDecimal balance = left.add(rechargeAmount);
            tryUpdateBalance(balance, entity);
        }
    }

    private int tryUpdateBalance(BigDecimal balance, MeterRechargeEntity entity) {
        int retryNum = 0;
        while (retryNum <= 5) {

            Optional<HighBalanceReadEntity> optEntity = balanceRepostory.findOne((root, query, builder) -> {
                Predicate p1 = builder.equal(root.get("hubAddress"), entity.getHubAddress());
                Predicate p2 = builder.equal(root.get("meterAddress"), entity.getMeterAddress());
                return builder.and(p1, p2);
            });

            if (optEntity.isPresent()) {
                int ret = balanceRepostory.updateHighMeterBalance(optEntity.get().getId(),
                        balance.toString(),
                        optEntity.get().getVersion(),
                        LocalDateTime.now(),
                        LocalDateTime.now(),
                        ResultConst.SUCCESS);

                if (ret == 1) {
                    break;
                }
            }
            else {
                log.error(">>> HighMeterRechargeService, tryUpdateBalance, get entity fail, meter:{}",
                        entity.getMeterAddress());
                break;
            }

            retryNum++;
        }


        return 1;
    }

    @Override
    public void getRechargeRequest(BassHardwardRequest request, MeterRechargeEntity entity) {
    }

    private String getPrice(BassHardwardRequest request) throws Exception {
        Optional<MeterEntiry> meterEntiry = meterRepostory.findOne((root, query, builder) -> {
            Predicate p1 = builder.equal(root.get("pluginId"), request.getServiceId());
            Predicate p2 = builder.equal(root.get("hubAddress"), request.getHubAddress());
            Predicate p3 = builder.equal(root.get("meterAddress"), request.getSubAddress());
            return builder.and(p1, p2, p3);
        });
        String price = "";
        if (!meterEntiry.isPresent()) {
            Map<String, Object> map = (Map) request.getEntitys().get(0);
            if (Objects.isNull(map.get("price"))
                    || Strings.isNullOrEmpty(map.get("price").toString())) {
                throw new Exception("没有设置单价");
            }
            return map.get("price").toString();
        } else {
            Map<String, Object> map = (Map) request.getEntitys().get(0);
            Object priceObj = map.get("price");
            if (Strings.isNullOrEmpty(meterEntiry.get().getPrice())
                    && Objects.isNull(priceObj)
                    && Strings.isNullOrEmpty(priceObj.toString())) {

                throw new Exception("没有设置单价");
            }
            if (!Strings.isNullOrEmpty(meterEntiry.get().getPrice())
                    && (Objects.isNull(priceObj) || Strings.isNullOrEmpty(priceObj.toString()))) {
                return meterEntiry.get().getPrice();
            }
            if (Strings.isNullOrEmpty(meterEntiry.get().getPrice())
                    && !Objects.isNull(priceObj)
                    && !Strings.isNullOrEmpty(priceObj.toString())) {

                return map.get("price").toString();
            }
            if (!Strings.isNullOrEmpty(meterEntiry.get().getPrice())
                    && !Objects.isNull(priceObj)
                    && !Strings.isNullOrEmpty(priceObj.toString())) {

                if (!meterEntiry.get().getPrice().equals(priceObj.toString())) {
                    throw new Exception("单价异常");
                }
                return meterEntiry.get().getPrice();
            }
        }
        throw new Exception("获取单价异常");
    }

    public static void main(String[] args) {
        String price = "0.04";
        String moneyStr = "11.22";
        BigDecimal priceB = new BigDecimal(price);
        BigDecimal money = new BigDecimal(moneyStr);
        BigDecimal amount = money.divide(priceB, RoundingMode.HALF_UP);
        BigDecimal ret = amount.setScale(2, RoundingMode.HALF_UP);

        System.out.println(ret);

    }



    public Object prepare(String url, BassHardwardRequest request) throws Exception {

        Object resp = null;
        Map<String, Object> retMap = null;
        String remark = "";
        MeterRechargeEntity getEntity = null;
        try {
            super.checkPrepare(url, request);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }

        MeterRechargeEntity entity = getEntityFromRequest(request);
        //充值前查询剩余金额
        BassHardwardRequest sender = MessageTpl.getMeterPrepareTpl(
                CommandCode.HUB_TYPE_HIGHMETER,
                CommandCode.HIGH_POWER_READ_METER,
                request.getServiceId(),
                IDGenerator.getIdStr(),
                request.getHubAddress(),
                request.getSubAddress());

        try {
            resp = HttpUtils.commonSendHttpPost(url,
                    JSONObject.toJSONString(sender),
                    PFConstDef.HTTP_FUN_MILL_TIMEOUT);
            retMap = JSONObject.parseObject((String) resp, Map.class);
            ResultConst.ReturnObject ret = ResultConst.checkResult(retMap);
            if (ret.getCode().equals(ResultConst.SUCCESS)) {
                //获取单价, 请求消息带单价，和服务器端保持的校验
                String price = getPrice(request);
                entity.setPrice(price);
                String rechargeTimes = ResultConst.getDatasByKey(retMap, "rechargeTimes");
                String leftAmount = ResultConst.getDatasByKey(retMap, "leftAmount");
                String totalFee = CommonUtils.getBassRequestDatas(request, "totalFee");

                BigDecimal money = CommonUtils.convertMoney(Integer.valueOf(totalFee));
                BigDecimal amount = getAmount(money.toString(), price);
                entity.setCapacity(amount.toString());


                //remark += ("查询剩余金额成功:" + leftMoney + ";");
                //清零的时候，充值记录里面也保存的是查询出来的实际值
                entity.setBalance(leftAmount);

                entity.setRechargeIndex(Long.valueOf(rechargeTimes));
                entity.setRemark(remark);
                entity.setModifyTime(LocalDateTime.now());
                entity.setPrepareStatus(RechargeStages.RECHARGE_PREPARE_STATUS_SUCCESS);
                baseRepostory.save(entity);
                //getEntity = saveEntity(entity);
            } else {
                remark += ("查询大功率电表信息失败");
                entity.setRemark(remark);
                entity.setPrepareStatus(RechargeStages.RECHARGE_PREPARE_STATUS_FAIL);
                entity.setModifyTime(LocalDateTime.now());
                baseRepostory.save(entity);
                //getEntity = saveEntity(entity);
                throw new Exception("prepare阶段失败");
            }

        } catch (Exception e) {
            e.printStackTrace();
            entity.setRemark("查询大功率电表信息异常: " + e.getMessage());
            entity.setModifyTime(LocalDateTime.now());
            entity.setPrepareStatus(RechargeStages.RECHARGE_PREPARE_STATUS_FAIL);
            baseRepostory.save(entity);
            //getEntity = saveEntity(entity);
            throw new Exception(e.getMessage());
        }

        ResponseEntity responseModel = CommonUtils.getResponseModel();
        responseModel.setReturnCode(ResultConst.SUCCESS);
        responseModel.setResultCode(ResultConst.SUCCESS);
        responseModel.setResultMsg(ResultConst.STATE_OK);
        responseModel.setReturnMsg(ResultConst.STATE_OK);
        responseModel.setReturnMsg("prepare阶段成功");
        CommonUtils.setVal(responseModel, "commandOrder", entity.getCommandOrder());
        CommonUtils.setVal(responseModel, "serviceOrder", entity.getServiceOrder());
        CommonUtils.setVal(responseModel, "totalFee", entity.getRechargeAmount());
        CommonUtils.setVal(responseModel, "hubAddress", entity.getHubAddress());
        CommonUtils.setVal(responseModel, "meterAddress", entity.getMeterAddress());
        CommonUtils.setVal(responseModel, "result", ResultConst.SUCCESS);

        return JSONObject.toJSONString(responseModel);
    }

    /**
     * @Description: 充值阶段
     * @Param:
     * @return:
     * @Author: admin
     * @Date: 2019/7/26
     */
    public Object serviceRecharge(String url,
                           BassHardwardRequest request,
                           MeterRechargeEntity entity) throws Exception {

        Object retObj = null;
        LocalDateTime startTime = LocalDateTime.now();


        //通过serviceOrder找到对应的记录
        Optional<MeterRechargeEntity> rechargeEntity = findOne((root, query, build) -> {
            return build.equal(root.get("commandOrder"), request.getCommandOrder());
        });
        Preconditions.checkArgument(Objects.nonNull(rechargeEntity),
                "充值异常: 查询订单(" + request.getServiceOrder() + ")失败");

        //开始充值, 可以统一
        try {
            retObj = baseRecharge(url, request, rechargeEntity.get());
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }

        return retObj;
    }

    @Override
    public Object resultNotify() throws Exception {
        return null;
    }
}
