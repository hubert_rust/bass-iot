package com.smart.recharge;

import com.smart.framework.commonpool.Task;
import com.smart.framework.commonpool.Worker;
import com.smart.framework.utils.CommonApplication;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-26 10:03
 **/
public class NotifyTheadPoolManage {
    public static ThreadPoolExecutor poolExecutor = null;
    public static final int RUNNING_NUM = 50;

    static {

        poolExecutor = new ThreadPoolExecutor(1,
                RUNNING_NUM,
                10,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(RUNNING_NUM * 2));
    }

    public static boolean submitNotifyTask(Object object) {
        Task task = CommonApplication.getBean("NotifyRechargeTask");
        NotifyWorker worker = new NotifyWorker(task, object);
        poolExecutor.submit(worker);
        return true;
    }

}
