package com.smart.recharge;

import com.smart.command.CommandCode;
import com.smart.command.CommandVal;
import com.smart.entity.MeterRechargeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-09-29 09:31
 **/

@Component
public class AbstractRechargeException {

    @Autowired
    ElecMeterRechargeExceptionService elecExceptionService;
    @Autowired
    ColdMeterRechargeExceptionService coldExceptionService;
    @Autowired
    HighMeterRechargeExceptionService highExceptionService;
    @Autowired
    HotMeterRechargeExceptionService hotExceptionService;

    public void process(String notifyUrl,
                        MeterRechargeEntity rechargeEntity) throws Exception {

        if (rechargeEntity.getMeterType().equals(CommandCode.HUB_TYPE_ELECMETER)) {
            elecExceptionService.process(notifyUrl, rechargeEntity);
        }
        else if (rechargeEntity.getMeterType().equals(CommandCode.HUB_TYPE_COLDMETER)) {
            coldExceptionService.process(notifyUrl, rechargeEntity);
        }
        else if (rechargeEntity.getMeterType().equals(CommandCode.HUB_TYPE_HIGHMETER)) {
            highExceptionService.process(notifyUrl, rechargeEntity);
        }
        else if (rechargeEntity.getMeterType().equals(CommandCode.HUB_TYPE_HOTMETER)) {
            hotExceptionService.process(notifyUrl, rechargeEntity);
        }
    }
}
