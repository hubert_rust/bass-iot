package com.smart.monitor.object;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-06-22 17:50
 **/
public interface IDiagnosic {
    boolean parentState();
    boolean checkSend();
    void setParent(IDiagnosic parent);
    IDiagnosic getParent();

}
