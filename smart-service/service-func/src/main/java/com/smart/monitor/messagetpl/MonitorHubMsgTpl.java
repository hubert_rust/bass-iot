package com.smart.monitor.messagetpl;

import com.alibaba.fastjson.JSONObject;
import com.smart.framework.cons.BassConst;
import com.smart.monitor.ConfigLoadUtils;
import com.smart.monitor.object.DiagnosicObject;
import com.smart.monitor.object.DiagnosicObjectHub;
import com.smart.request.BassHardwardRequest;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-23 15:46
 **/
public class MonitorHubMsgTpl implements IMonitorMsgTpl{
    @Override
    public String getMonitorMsg(String key, DiagnosicObject ob) {

        DiagnosicObjectHub obj = (DiagnosicObjectHub) ConfigLoadUtils.diagnosicObjectMap.get(key);
        BassHardwardRequest request = null;
        if (obj instanceof DiagnosicObjectHub) {
            request = new BassHardwardRequest();
            request.setHubAddress(obj.getHubAddr());
            request.setServiceType("FUNC");
            request.setServiceId(obj.getPluginId());
            request.setSubType(BassConst.BASS_HARDWARD_SERVICE_PLUGIN_SUBTYPE_HEART);
            request.setServiceType(BassConst.BASS_HARDWARD_SERVICE_NAME_PLUGIN);
        }
        return JSONObject.toJSONString(request);
    }
}
