package com.smart.monitor.task;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-23 09:26
 **/
public interface IDiagnosicTask {
    boolean probe();
}
