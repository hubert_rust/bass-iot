package com.smart.monitor.messagetpl;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.smart.command.CommandCode;
import com.smart.framework.cons.BassConst;
import com.smart.monitor.ConfigLoadUtils;
import com.smart.monitor.MonitorType;
import com.smart.monitor.object.DiagnosicObject;
import com.smart.monitor.object.DiagnosicObjectMeter;
import com.smart.request.BassHardwardRequest;

import java.util.List;
import java.util.Map;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-23 15:45
 **/
public class MonitorLockerMsgTpl implements  IMonitorMsgTpl{
    @Override
    public String getMonitorMsg(String key, DiagnosicObject ob) {
        DiagnosicObjectMeter obj = (DiagnosicObjectMeter) ConfigLoadUtils.diagnosicObjectMap.get(key);
        BassHardwardRequest request = null;
        if (obj instanceof DiagnosicObjectMeter) {
            request = new BassHardwardRequest();
            request.setHubAddress(obj.getHubAddr());
            request.setSubAddress(obj.getMeterAddr());
            request.setServiceType(BassConst.BASS_HARDWARD_SERVICE_NAME_FUNC);
            request.setCommandOrder(String.valueOf(System.currentTimeMillis()));
            request.setServiceId(obj.getPluginId());
            request.setSubType(BassConst.BASS_HARDWARD_SERVICE_KEEP_SUBTYPE_MONITOR);
            request.setCmdCode(CommandCode.CMD_LOCK_QUERY_STATE);
            request.setCmdType(MonitorType.MONITOR_LOCKER);

            List<Map> list = Lists.newArrayList();
            Map<String, Object> map = Maps.newHashMap();
            map.put("hubAddress", obj.getHubAddr());
            map.put("meterAddress", obj.getMeterAddr());
            list.add(map);
            request.setEntitys(list);
        }

        return JSONObject.toJSONString(request);
    }
}
