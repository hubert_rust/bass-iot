package com.smart.monitor;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.smart.monitor.object.DiagnosicObject;
import com.smart.monitor.object.IDiagnosic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Queue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

/**
 * @program: smart-service
 * @description: 对plugin, hub, meter进行统一监测
 * @author: admin
 * @create: 2019-05-23 13:17
 **/
public class LoopDiagnosicObjectThread extends Thread {
    private static final Logger log = LoggerFactory.getLogger(LoopDiagnosicObjectThread.class);

    private Map<String, DiagnosicObject> diagnosicObjectMap = Maps.newConcurrentMap();
    private Map<String, MonitorParam> monitorParamMap;
    private Queue diagnosicMessageQueue;

    public LoopDiagnosicObjectThread(Map<String, DiagnosicObject> map,
                                     Map<String, MonitorParam> paramMap,
                                     Queue<DiagnosicMessage> messageQueue) {
        this.diagnosicObjectMap = map;
        this.monitorParamMap = paramMap;
        this.diagnosicMessageQueue = messageQueue;
    }

    /**
    * @Description: 发送心跳的时候，需要平滑发送，避免一次同时发送很多条消息，初始meter都是ok，不用发送心跳
    * @Param:
    * @return:
    * @Author: admin
    * @Date: 2019/7/8
    */
    @Override
    public void run() {
        while (true) {
            try {
                TimeUnit.MILLISECONDS.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            for (DiagnosicObject val : diagnosicObjectMap.values()) {
                try {
                    String monitorType = val.getMonitorType();
                    if (!Strings.isNullOrEmpty(monitorType)) {
                        //如果插件故障，不监测集中器和集中器下面挂接的子设备
                        //如果集中器故障，不检测集中器和下面挂接的子设备
                        if (!val.checkSend()) {
                            continue;
                        }

                        MonitorParam monitorParam = monitorParamMap.get(monitorType);
                        monitorParam.getProbeTime();

                        long lastTm = val.getLastDiagnosicTm();
                        long currTm = System.currentTimeMillis();

                        //检测放到了业务测，此处功能去掉2019-09-17, haha
                        if ((currTm - lastTm) > 1000 * monitorParam.getProbeTime()) {
                            //注意此处
/*                            val.setLastDiagnosicTm(System.currentTimeMillis());

                            Supplier<DiagnosicMessage> supplier = DiagnosicMessage::new;
                            DiagnosicMessage message = supplier.get();
                            message.setKey(val.getKey());
                            message.setMsgType(MonitorType.MONITOR_TYPE_NORMAL);
                            diagnosicMessageQueue.offer(message);*/
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        Map<String, DiagnosicMessage> map = Maps.newHashMap();

        for (int i = 0; i < 10000; i++) {
            map.put(String.valueOf(i), new DiagnosicMessage());
        }

        long t1 = System.currentTimeMillis();
        map.entrySet().forEach((val) -> {
            log.info(">>> {}", val.getKey());
        });

        long t2 = System.currentTimeMillis() - t1;
        log.info("total time: {}", t2);
    }
}
