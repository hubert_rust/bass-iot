package com.smart.monitor.task;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.smart.common.ResponseEntity;
import com.smart.framework.utils.HttpUtils;
import com.smart.monitor.ConfigLoadUtils;
import com.smart.monitor.DiagnosicMessage;
import com.smart.monitor.MonitorManager;
import com.smart.monitor.MonitorType;
import com.smart.monitor.object.DiagnosicObject;
import com.smart.monitor.object.DiagnosicObjectHub;
import com.smart.monitor.object.DiagnosicObjectMeter;
import com.smart.monitor.object.DiagnosicObjectPlugin;
import com.smart.monitor.task.IDiagnosicTask;
import com.smart.service.MonitorServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.function.Supplier;

import static com.alibaba.fastjson.JSON.parseObject;
import static com.smart.common.ResultConst.STATE_NOK;
import static com.smart.common.ResultConst.SUCCESS;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-23 09:26
 **/


@Service("DiagnosicTask")
@Scope("prototype")
public class DiagnosicTask implements IDiagnosicTask {
    private static final Logger log = LoggerFactory.getLogger(DiagnosicTask.class);

    private String key;  //响应结果获取后刷新状态(通过消息刷新状态)
    private String url;
    private String jsonStr;
    private int probeTimeout; //探测消息超时时间

    @Autowired
    MonitorServiceImpl monitorService;

    public void config(String url, String jsonStr, String key, int probeTimeout) {
        this.url = url;
        this.jsonStr = jsonStr;
        this.key = key;
        this.probeTimeout = probeTimeout * 1000;
    }

    @Override
    public boolean probe() {
        DiagnosicObject obj = null;


        try {
            obj = ConfigLoadUtils.diagnosicObjectMap.get(key);
            String ret = (String) HttpUtils.commonSendHttpPost(url, jsonStr, probeTimeout);
            log.info(">>> DiagnosicTask, probe ret: {}", ret);

            commonProcess(obj, ret);
        } catch (Exception e) {
            e.printStackTrace();
            commonProcess(obj, null);
        }

        return false;
    }
    private void commonProcess(DiagnosicObject obj, String resp) {
        Supplier<DiagnosicMessage> supplier = DiagnosicMessage::new;
        DiagnosicMessage message = supplier.get();
        message.setKey(this.key);
        message.setMsgType(MonitorType.MONITOR_TYPE_REFRESH);

        long objID = obj.getObjectId();
        String objectState = Strings.isNullOrEmpty(resp) ? "nok" : "ok";
        if (obj instanceof DiagnosicObjectPlugin) {
        }
        else if (obj instanceof DiagnosicObjectHub) {
        }
        else if (obj instanceof DiagnosicObjectMeter) {
            if (!Strings.isNullOrEmpty(resp)) {
                ResponseEntity<Map<String, Object>> ret = JSONObject.parseObject(resp, ResponseEntity.class);
                if (ret.getResultCode().equals(SUCCESS)) {
                }
                else {
                    //如果集中器没有连接, resultCode超时
                    objectState = STATE_NOK;
                }
            }
        }

        message.setObjectState(objectState);
        monitorService.updateMonitor(obj, objectState);

        MonitorManager.diagnosicMessageQueue.offer(message);
    }



}
