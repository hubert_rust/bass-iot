package com.smart.monitor;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Queues;
import com.smart.monitor.object.DiagnosicObject;
import com.smart.monitor.object.DiagnosicObjectPlugin;
import com.smart.repository.PluginRepostory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

/**
 * @program: smart-service
 * @description: 获取监测消息
 * @author: admin
 * @create: 2019-05-22 17:39
 **/
public class MonitorManager {

    private static final Logger log = LoggerFactory.getLogger(MonitorManager.class);
    public static final int PLUGIN_POOLS = 2;   //插件监测worker线程池大小
    public static final int HUB_POOLS = 10;     //集中器监测worker线程池大小
    public static final int METER_POOLS = 50;

    public static final long PULL_TICKER = 5;
    public static final int MAX_NUM_PER_THREAD = 1000;  //每个线程监控对象数量

    private static LoopDiagnosicObjectThread loopDiagnosicObjectThread;

    public static DiagnosicFactory factory;

    //监测对象
    //public static Map<String, DiagnosicObject> diagnosicObjectMap = Maps.newConcurrentMap();

    //拉取监测消息放入
    private static PullDiagnosicThread pullDiagnosicThread;


    //从队列取出的消息放入线程池
    public static Queue<DiagnosicMessage> diagnosicMessageQueue = Queues.newConcurrentLinkedQueue();


    //
    public static List<DiagnosicObjectPlugin> pluginList = Lists.newArrayList();

    public static void initMonitorManager(Map<String, DiagnosicObject> map) throws Exception {
        initFactory();
        DiagnosicThreadPoolManage.initThreadPool(PLUGIN_POOLS, HUB_POOLS, METER_POOLS);

        pullDiagnosicThread = new PullDiagnosicThread();
        pullDiagnosicThread.start();
        Thread.currentThread().sleep(100);

        loopDiagnosicObjectThread = new LoopDiagnosicObjectThread(
                ConfigLoadUtils.diagnosicObjectMap ,
                ConfigLoadUtils.moniotorParamMap,
                diagnosicMessageQueue);

        loopDiagnosicObjectThread.start();
    }

    public static void offer(DiagnosicMessage message) {
        diagnosicMessageQueue.offer(message);
    }

    private static void initFactory(){
/*        factory = DiagnosicFactory.factory(builder -> {
            builder.add(MonitorType.MONITOR_CLASS_METER,
                    CommonApplication.getBean("MeterDiagnosic"));
            builder.add(MonitorType.MONITOR_CLASS_HUB,
                    CommonApplication.getBean("HubDiagnosic"));
            builder.add(MonitorType.MONITOR_CLASS_PLUGIN,
                    CommonApplication.getBean("PluginDiagnosic"));
        });*/
    }


    public static class PullDiagnosicThread extends Thread {
        @Override
        public void run() {
            while (true) {
                try {
                    TimeUnit.MILLISECONDS.sleep(PULL_TICKER);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                try {
                    DiagnosicMessage message = diagnosicMessageQueue.poll();
                    if (Objects.isNull(message)) continue;

                    if (message.getMsgType().equals(MonitorType.MONITOR_TYPE_REFRESH)) {
                        log.info(">>> PullDiagnosicThread, key: {}, messageType: {}",
                                message.getKey(), message.getMsgType());
                        //直接刷新状态
                        DiagnosicObject obj = ConfigLoadUtils.diagnosicObjectMap.get(message.getKey());
                        obj.setLastDiagnosicTm(System.currentTimeMillis());
                        obj.setState(message.getObjectState());
                        //DiagnosicThreadPoolManage.submit(message);

                    } else if (message.getMsgType().equals(MonitorType.MONITOR_TYPE_NORMAL)) {
                        DiagnosicThreadPoolManage.submit(message);
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
