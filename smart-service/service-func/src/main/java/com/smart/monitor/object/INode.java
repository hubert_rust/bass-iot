package com.smart.monitor.object;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static com.google.common.base.Preconditions.checkState;

/**
 * Created by Administrator on 2017/4/13.
 * tag interface for all nodes in tree of yaml
 */
public interface INode {
    String getName();

    void setName(String name);

    INode gotParent();

    void setParent(INode parent);

    default List<INode> gotChildren() {
        return Lists.newArrayList();
    }

    default List<INode> gotDescedents() {
        List<INode> nodes = Lists.newArrayList();
        nodes.addAll(gotChildren());
        for (INode child : gotChildren()) {
            if (child != null) {
                nodes.addAll(child.gotDescedents());
            }
        }

        return nodes;
    }

    default Stream<INode> getNodes(Predicate<INode> predicate) {
        return gotDescedents().stream().filter(predicate);
    }


    default <T> Stream<T> getNodesByInterface(Class<T> clazz) {
        return gotDescedents().stream()
                .filter(node -> node != null && clazz.isAssignableFrom(node.getClass()))
                .map(node -> (T) node);
    }

    default <T extends INode> Stream<T> getNodes(Class<T> clazz) {
        return (Stream<T>) gotDescedents().stream().filter(node -> node != null && clazz.isAssignableFrom(node.getClass()));
    }

    default <T extends INode>  Stream<T> getExactNodes(Class<T> clazz) {
        return (Stream<T>) gotDescedents().stream().filter(node -> node != null && node.getClass().equals(clazz) );
    }


    default void dfs(BinaryOperator<INode> operator) {
        for (INode child : gotChildren()) {
            if (child != null) {
                operator.apply(this, child);
                child.dfs(operator);
            }
        }
    }

    default void buildRef2Parent() {
        dfs((parent, child) -> {
            child.setParent(parent);
            checkState(child.gotParent() != null);

            return parent;
        });
    }

    default void gotAscendancyChain(StringBuilder sb) {
        if (gotParent() != null) {
            gotParent().gotAscendancyChain(sb);
        }
        sb.append("->").append(getName());
    }

    default <T extends INode> T getAncestor(Class<T> clazz){
        if(gotParent() != null){
            if( clazz.isAssignableFrom(gotParent().getClass()) ){
                return (T) gotParent();
            } else {
                return gotParent().getAncestor(clazz);
            }
        } else {
            return null;
        }
    }

    default <T extends INode> INode getAncestorIncludeSelf(Class<T> clazz){
        if(clazz.isAssignableFrom( getClass())){
            return this;
        } else {
            return getAncestor(clazz);
        }
    }

}
