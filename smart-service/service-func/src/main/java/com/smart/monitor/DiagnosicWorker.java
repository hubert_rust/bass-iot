package com.smart.monitor;

import com.smart.monitor.task.DiagnosicTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-23 09:26
 **/

public class DiagnosicWorker extends Thread {
    private static final Logger log = LoggerFactory.getLogger(DiagnosicWorker.class);
    private DiagnosicTask diagnosicTask;

    public void config(DiagnosicTask task) {
        this.diagnosicTask = task;
    }

    @Override
    public void run() {
        this.diagnosicTask.probe();
    }
}
