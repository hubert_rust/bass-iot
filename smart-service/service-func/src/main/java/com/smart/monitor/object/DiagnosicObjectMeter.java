package com.smart.monitor.object;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-23 11:25
 **/
public class DiagnosicObjectMeter extends DiagnosicObject {
    private String meterType;
    private String hubAddr;
    private String meterAddr;

    public String getMeterType() { return meterType; }
    public void setMeterType(String meterType) { this.meterType = meterType; }
    public String getHubAddr() { return hubAddr; }
    public void setHubAddr(String hubAddr) { this.hubAddr = hubAddr; }
    public String getMeterAddr() { return meterAddr; }
    public void setMeterAddr(String meterAddr) { this.meterAddr = meterAddr; }
}
