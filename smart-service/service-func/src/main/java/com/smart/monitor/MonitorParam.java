package com.smart.monitor;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-23 11:08
 **/
public class MonitorParam {
    private String monitorType;
    private long probeTime;
    private int probeTimeout;

    public String getMonitorType() {
        return monitorType;
    }

    public void setMonitorType(String monitorType) {
        this.monitorType = monitorType;
    }

    public long getProbeTime() {
        return probeTime;
    }

    public void setProbeTime(long probeTime) {
        this.probeTime = probeTime;
    }

    public int getProbeTimeout() {
        return probeTimeout;
    }

    public void setProbeTimeout(int probeTimeout) {
        this.probeTimeout = probeTimeout;
    }

    public static final class MonitorParamBuilder {
        private String monitorType;
        private long probeTime;
        private int probeTimeout;

        private MonitorParamBuilder() {
        }

        public static MonitorParamBuilder aMonitorParam() {
            return new MonitorParamBuilder();
        }

        public MonitorParamBuilder withMonitorType(String monitorType) {
            this.monitorType = monitorType;
            return this;
        }

        public MonitorParamBuilder withProbeTime(long probeTime) {
            this.probeTime = probeTime;
            return this;
        }

        public MonitorParamBuilder withProbeTimeout(int probeTimeout) {
            this.probeTimeout = probeTimeout;
            return this;
        }

        public MonitorParam build() {
            MonitorParam monitorParam = new MonitorParam();
            monitorParam.probeTimeout = this.probeTimeout;
            monitorParam.probeTime = this.probeTime;
            monitorParam.monitorType = this.monitorType;
            return monitorParam;
        }
    }
}
