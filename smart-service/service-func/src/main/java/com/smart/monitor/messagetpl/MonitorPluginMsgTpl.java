package com.smart.monitor.messagetpl;

import com.alibaba.fastjson.JSONObject;
import com.smart.framework.cons.BassConst;
import com.smart.monitor.object.DiagnosicObject;
import com.smart.request.BassHardwardRequest;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-23 15:45
 **/
public class MonitorPluginMsgTpl implements IMonitorMsgTpl{
    @Override
    public String getMonitorMsg(String key, DiagnosicObject ob) {

        BassHardwardRequest baseEntity = new BassHardwardRequest<>();
        baseEntity.setServiceId(ob.getPluginId());
        baseEntity.setSubType(BassConst.BASS_HARDWARD_SERVICE_PLUGIN_SUBTYPE_HEART);
        baseEntity.setServiceType(BassConst.BASS_HARDWARD_SERVICE_NAME_PLUGIN);
        return JSONObject.toJSONString(baseEntity);
    }
}
