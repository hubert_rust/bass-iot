package com.smart.monitor;

import com.google.common.collect.Maps;
import com.smart.monitor.task.IDiagnosicTask;

import java.util.Map;
import java.util.function.Consumer;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-22 17:47
 **/
public interface DiagnosicFactory {
    IDiagnosicTask create(String diagnosicType);

    public static DiagnosicFactory factory(Consumer<Builder> consumer) {
        Map<String, IDiagnosicTask> map = Maps.newHashMap();
        consumer.accept(map::put);
        return type -> map.get(type);
    }
}
