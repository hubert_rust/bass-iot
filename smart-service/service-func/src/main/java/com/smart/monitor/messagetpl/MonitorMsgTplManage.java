package com.smart.monitor.messagetpl;

import com.google.common.collect.Maps;
import com.smart.monitor.MonitorType;
import com.smart.monitor.object.DiagnosicObject;

import java.util.Map;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-23 15:50
 **/
public class MonitorMsgTplManage {
    public static Map<String, IMonitorMsgTpl> msgTplMap = Maps.newHashMap();

    static {
        msgTplMap.put(MonitorType.MONITOR_COLD_METER, new MonitorColdMeterMsgTpl());
        msgTplMap.put(MonitorType.MONITOR_ELEC_METER, new MonitorElecMeterMsgTpl());
        msgTplMap.put(MonitorType.MONITOR_HOT_METER, new MonitorHotMeterMsgTpl());
        msgTplMap.put(MonitorType.MONITOR_HIGH_POWER, new MonitorHighPowerMsgTpl());
        msgTplMap.put(MonitorType.MONITOR_LOCKER, new MonitorLockerMsgTpl());
        msgTplMap.put(MonitorType.MONITOR_HUB, new MonitorHubMsgTpl());
        msgTplMap.put(MonitorType.MONITOR_PLUGIN, new MonitorPluginMsgTpl());
    }

    public static String getMonitorMsg(String key, DiagnosicObject object) {
        IMonitorMsgTpl monitorMsgTpl = null;
        String type = object.getMonitorType();
        String mainClass = object.getMonitorClass();
        if (mainClass.equals(MonitorType.MONITOR_CLASS_HUB)
                || mainClass.equals(MonitorType.MONITOR_PLUGIN)) {
            monitorMsgTpl = msgTplMap.get(mainClass);
        }
        else {
            monitorMsgTpl = msgTplMap.get(type);
        }

        return monitorMsgTpl.getMonitorMsg(key, object);
    }
}
