package com.smart.monitor;

import com.smart.monitor.task.IDiagnosicTask;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-22 17:45
 **/
public interface Builder {
    void add(String monitorType, IDiagnosicTask diagnosic);
}
