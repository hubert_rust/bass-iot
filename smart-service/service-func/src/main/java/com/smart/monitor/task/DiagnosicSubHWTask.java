package com.smart.monitor.task;

import com.alibaba.fastjson.JSONObject;
import com.smart.common.ResponseEntity;
import com.smart.framework.utils.HttpUtils;
import com.smart.monitor.ConfigLoadUtils;
import com.smart.monitor.DiagnosicMessage;
import com.smart.monitor.MonitorManager;
import com.smart.monitor.MonitorType;
import com.smart.monitor.object.DiagnosicObject;
import com.smart.monitor.object.DiagnosicObjectHub;
import com.smart.monitor.object.DiagnosicObjectMeter;
import com.smart.monitor.object.DiagnosicObjectPlugin;
import com.smart.service.MonitorServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.function.Supplier;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-23 09:26
 **/

@Service("DiagnosicSubHWTask")
@Scope("prototype")
public class DiagnosicSubHWTask extends DiagnosicTask {
    private static final Logger log = LoggerFactory.getLogger(DiagnosicSubHWTask.class);


    @Autowired
    MonitorServiceImpl monitorService;

    public void config(String url, String jsonStr, String key, int probeTimeout) {
       super.config(url, jsonStr, key, probeTimeout);
    }

    @Override
    public boolean probe() {
        return super.probe();
    }
}
