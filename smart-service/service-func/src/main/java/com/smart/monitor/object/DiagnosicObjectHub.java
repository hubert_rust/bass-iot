package com.smart.monitor.object;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-23 11:24
 **/
public class DiagnosicObjectHub extends DiagnosicObject {
    //peer_485, peer_ip
    private String hubType;
    private String peerType;
    private String hubAddr;
    private String hubIp;

    //子对象只能是表, 门锁
    //private List<IDiagnosic> subObjectList = Lists.newArrayList();

    public String getHubType() { return hubType; }
    public void setHubType(String hubType) { this.hubType = hubType; }
    public String getPeerType() { return peerType; }
    public void setPeerType(String peerType) { this.peerType = peerType; }
    public String getHubAddr() { return hubAddr; }
    public void setHubAddr(String hubAddr) { this.hubAddr = hubAddr; }
    public String getHubIp() { return hubIp; }
    public void setHubIp(String hubIp) { this.hubIp = hubIp; }

}
