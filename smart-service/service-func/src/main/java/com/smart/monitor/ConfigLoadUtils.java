package com.smart.monitor;

/*
 * @ program: func-center
 * @ description: config
 * @ author: admin
 * @ create: 2019-03-27 16:52
 **/

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.smart.command.CommandCode;
import com.smart.config.DataLoader;
import com.smart.monitor.object.DiagnosicObject;
import com.smart.monitor.object.DiagnosicObjectHub;
import com.smart.monitor.object.DiagnosicObjectMeter;
import com.smart.repository.ConcentratorRepostory;
import com.smart.repository.MeterRepostory;
import com.smart.entity.MonitorEntity;
import com.smart.repository.PluginRepostory;
import com.smart.service.MonitorServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Component
public class ConfigLoadUtils implements CommandLineRunner {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigLoadUtils.class);

    @Autowired
    PluginRepostory pluginRepostory;
    @Autowired
    ConcentratorRepostory conRepository;
    @Autowired
    MeterRepostory  meterRepostory;
    @Autowired
    MonitorServiceImpl monitorService;

    @Value("${func-type}")
    private String funcType;

    //保存pluginId
    public static Map<String, Map<String, Object>> pluginMap = Maps.newHashMap();

    //public static Map<String, Map<String, Object>> hubMap = Maps.newHashMap();

    //public static Table<String, String, Map<String, Object>> table = HashBasedTable.create();

    //监测对象时长
    public static Map<String, MonitorParam> moniotorParamMap = Maps.newConcurrentMap();

    //监测对象, String: key
    //plugin: plugin-xxx（pluginId）
    //hub: hub-xxx(hubAddress)
    //meter: meter-hubAddress-meterAddress
    public static Map<String, DiagnosicObject> diagnosicObjectMap = Maps.newConcurrentMap();

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void run(String... args) throws Exception {
        LOGGER.info(">>> ConfigLoadUtils, initDiagnosic start: {}.", LocalDateTime.now().toString());
        initDiagnosic();
        LOGGER.info(">>> ConfigLoadUtils, initDiagnosic finish: {}.", LocalDateTime.now().toString());
    }

    /**
    * @Description: 注意加载顺序
    * @Param:
    * @return:
    * @Author: admin
    * @Date: 2019/6/25
    */
    public void initDiagnosic() {
        if (true) {
            return;
        }

        loaderMoniorParam();
        loadPluginConfig(funcType);
        loadHubInof(funcType);
        loadMeterInfo(funcType);

        try {
            MonitorManager.initMonitorManager(this.getDiagnosicObjectMap());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Map getDiagnosicObjectMap() { return diagnosicObjectMap; }
    public void initDiagnosicObject( String monitorTypeClass, Map map) {
        long putId = Long.valueOf(map.get("id").toString());
        String key = "";
        DiagnosicObject diagnosicObject = null;
        String pluginId = map.get("plugin_id").toString();
        if (Objects.isNull(pluginMap.get(pluginId))) {
            LOGGER.error(">>> getDiagnosicObjectMap, pluginId: {}", pluginId );
            return;
        }
        String pluginUrl = pluginMap.get(pluginId).get("plugin_url").toString();
        if (monitorTypeClass.equals(MonitorType.MONITOR_CLASS_PLUGIN)) {
            diagnosicObject = new DiagnosicObjectHub();
            key = monitorTypeClass + "-" + pluginId;
            diagnosicObject.setPluginId(pluginId);
            diagnosicObject.setMonitorClass(monitorTypeClass);
            diagnosicObject.setMonitorType(map.get("plugin_name").toString());
            diagnosicObject.setPluginUrl(map.get("plugin_url").toString());
            MonitorParam monitorParam = moniotorParamMap.get("plugin");
            diagnosicObject.setProbeSpan(monitorParam.getProbeTime());
            diagnosicObject.setProbeTimeout(monitorParam.getProbeTimeout());
            diagnosicObject.setParent(null);
            diagnosicObject.setState("nok");
        } else if (monitorTypeClass.equals(MonitorType.MONITOR_CLASS_HUB)) {
            diagnosicObject = new DiagnosicObjectHub();
            diagnosicObject.setPluginId(pluginId);
            diagnosicObject.setPluginUrl(pluginUrl);
            diagnosicObject.setMonitorClass(monitorTypeClass);
            ((DiagnosicObjectHub) diagnosicObject).setHubAddr(map.get("hub_address").toString());
            ((DiagnosicObjectHub) diagnosicObject).setHubIp(map.get("hub_ip").toString());
            ((DiagnosicObjectHub) diagnosicObject).setHubType(map.get("hub_type").toString());
            diagnosicObject.setMonitorType(map.get("hub_type").toString());
            key = monitorTypeClass + "-" + map.get("hub_address").toString();

            MonitorParam monitorParam = moniotorParamMap.get(MonitorType.MONITOR_CLASS_HUB);
            diagnosicObject.setProbeSpan(monitorParam.getProbeTime());
            diagnosicObject.setProbeTimeout(monitorParam.getProbeTimeout());


            //设置parent节点
            String parentKey = (MonitorType.MONITOR_CLASS_PLUGIN) + "-" + pluginId;
            diagnosicObject.setParent(diagnosicObjectMap.get(parentKey));
            diagnosicObject.setState("nok");
            DataLoader.initHubService(map);
        } else if (monitorTypeClass.equals(MonitorType.MONITOR_CLASS_METER)) {
            diagnosicObject = new DiagnosicObjectMeter();
            diagnosicObject.setPluginId(pluginId);
            diagnosicObject.setPluginUrl(pluginUrl);
            diagnosicObject.setMonitorClass(monitorTypeClass);
            ((DiagnosicObjectMeter) diagnosicObject).setHubAddr(map.get("hub_address").toString());
            ((DiagnosicObjectMeter) diagnosicObject).setMeterAddr(map.get("meter_address").toString());
            ((DiagnosicObjectMeter) diagnosicObject).setMeterType(map.get("meter_type").toString());
            diagnosicObject.setMonitorType(map.get("meter_type").toString());
            key = monitorTypeClass + "-";
            key += map.get("hub_address").toString();
            key += "-";
            key += map.get("meter_address").toString();

            MonitorParam monitorParam = moniotorParamMap.get(map.get("meter_type").toString());
            diagnosicObject.setProbeSpan(monitorParam.getProbeTime());
            diagnosicObject.setProbeTimeout(monitorParam.getProbeTimeout());
            //设置parent节点
            String parentKey = (MonitorType.MONITOR_CLASS_HUB) + "-" + map.get("hub_address").toString();
            diagnosicObject.setParent(diagnosicObjectMap.get(parentKey));
            diagnosicObject.setState("ok");
        }

        if (Strings.isNullOrEmpty(key) || Objects.isNull(diagnosicObject)) {
            LOGGER.error(">>> ConfigLoadUtils, initDiagnosicObject, key or diagnosicObject invalid," +
                    "  monitorTypeClass:", monitorTypeClass);
        }

        diagnosicObject.setKey(key);
        diagnosicObject.setData(map);
        diagnosicObject.setObjectId(putId);
        diagnosicObjectMap.put(key, diagnosicObject);
        MonitorEntity monitorEntity = monitorService.initMonitorTable(diagnosicObject);
        diagnosicObject.setObjectId(monitorEntity.getId());
    }
    //pending, 需要考虑动态增加，修改，删除接口

    //加载集中器信息
    public String getSqlCondition(String mask, String fdName) {
        if (Strings.isNullOrEmpty(mask)) {
            return "";
        }

        String sql = "'$" + fdName.toUpperCase() + "'";
        String sqlSnip = fdName + "= " + sql;
        String finalSql = "";
        String[] typeArr = mask.split(",");
        for (String v: typeArr) {
            String var1 = CommandCode.MASK_MAP.get(v);
            if (Strings.isNullOrEmpty(var1)) {
                continue;
            }
            if (v.equals(var1)) {
                String var2 = sqlSnip;
                var2 = var2.replace("$"+fdName.toUpperCase(), var1);
                if (!Strings.isNullOrEmpty(finalSql)) {
                    finalSql += ("or ") + var2;
                }
                else {
                    finalSql += var2;
                }
            }

        }

        return Strings.isNullOrEmpty(finalSql) ? "" : (" where " + finalSql);
    }
    public void loadHubInof(String funcType) {
        synchronized (ConfigLoadUtils.class) {
/*            String sql = "select * from tb_hardware_hub where hub_type = '$HUB_TYPE'";
            sql = sql.replace("$HUB_TYPE", funcType);
            if (funcType.equals("all")) {
            }*/
            String sql = "select * from tb_hardware_hub";
            sql += getSqlCondition(funcType, "hub_type");
            List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
            rows.forEach(v -> {
                initDiagnosicObject(MonitorType.MONITOR_CLASS_HUB, v);

            });
            LOGGER.info(">>> ConfigLoadUtils, loadHubInfo: {}", rows.toString());
        }
    }

    /**
     * @Description: 需要优化，分批读取
     * @Param:
     * @return:
     * @Author: admin
     * @Date: 2019/5/16
     */
    public void loadMeterInfo(String funcType) {
        synchronized (ConfigLoadUtils.class) {
/*            String sql = "select * from tb_hardware_meter where meter_status = 'use' and meter_type = '$METER_TYPE'";
            sql = sql.replace("$METER_TYPE", funcType);
            if (funcType.equals("all")) {
                sql = "select * from tb_hardware_meter"
            }*/

            String sql = "select * from tb_hardware_meter";
            sql += getSqlCondition(funcType, "meter_type");

            List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
            rows.forEach(val -> {
                initDiagnosicObject(MonitorType.MONITOR_CLASS_METER, val);
            });
        }
    }

    public void loadPluginConfig(String funcType) {
        synchronized (ConfigLoadUtils.class) {
/*
            String sql = "select * from tb_hardware_plugin_manage where plugin_name = '$FUNC_TYPE'";
            sql = sql.replace("$FUNC_TYPE", funcType);
*/


            String sql = "select * from tb_hardware_plugin_manage ";
            sql += getSqlCondition(funcType, "plugin_name");
            List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
            rows.forEach(v -> {
                String pluginId = v.get("plugin_id").toString();
                Map<String, Object> map = Maps.newHashMap();
                map.putAll(v);
                pluginMap.put(pluginId, map);
            });
            rows.forEach(v -> {
                initDiagnosicObject(MonitorType.MONITOR_CLASS_PLUGIN, v);
            });
        }
    }

    public void loaderMoniorParam() {
        String sql = "select * from tb_hardware_monitor_param";
        moniotorParamMap.clear();
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
        list.forEach(v -> {
            MonitorParam monitorParam = MonitorParam
                    .MonitorParamBuilder
                    .aMonitorParam()
                    .withMonitorType(v.get("monitor_type").toString())
                    .withProbeTime(Integer.valueOf(v.get("probe_time").toString()))
                    .withProbeTimeout(Integer.valueOf(v.get("probe_timeout").toString()))
                    .build();
            moniotorParamMap.put(v.get("monitor_type").toString(), monitorParam);
        });
    }

}
