package com.smart.monitor;

import com.google.common.base.Strings;
import com.smart.framework.utils.CommonApplication;
//import com.smart.framework.utils.ConfigLoadUtils;
import com.smart.monitor.messagetpl.MonitorMsgTplManage;
import com.smart.monitor.object.DiagnosicObject;
import com.smart.monitor.task.DiagnosicSubHWTask;
import com.smart.monitor.task.DiagnosicTask;
import com.smart.monitor.task.IDiagnosicTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DiagnosicThreadPoolManage {

    private static final Logger LOGGER = LoggerFactory.getLogger(DiagnosicThreadPoolManage.class);

    public static ExecutorService pluginPool = null;
    public static ExecutorService hubPool = null;
    public static ExecutorService meterPool = null;

    public static void initThreadPool(int pluginNum, int hubNum, int meterNum) {
        pluginPool = Executors.newFixedThreadPool(pluginNum);
        hubPool = Executors.newFixedThreadPool(hubNum);
        meterPool = Executors.newFixedThreadPool(meterNum);
    }
    public static void submit(DiagnosicMessage message) {
        DiagnosicObject object = ConfigLoadUtils.diagnosicObjectMap.get(message.getKey());
        object.getMonitorType();
        String url = object.getPluginUrl();
        String key = object.getKey();

        String jsonStr= MonitorMsgTplManage.getMonitorMsg(message.getKey(), object);
        if (Strings.isNullOrEmpty(jsonStr)) {
            LOGGER.error(">>> DiagnosicThreadPoolManage, submit, get invalid jsonStr");
            return;
        }

        if (object.getMonitorClass().equals(MonitorType.MONITOR_CLASS_METER)) {
/*
            DiagnosicTask task = CommonApplication.getBean("DiagnosicTask");
            task.config(url, jsonStr, key, object.getProbeTimeout());
            DiagnosicWorker worker = new DiagnosicWorker();
            worker.config(task);
            meterPool.submit(worker);*/
            taskSubmit(meterPool, url, jsonStr, key, object.getProbeTimeout(), DiagnosicSubHWTask.class);
        }
        else if (object.getMonitorClass().equals(MonitorType.MONITOR_CLASS_HUB)) {
            //hubPool.submit(worker);
            taskSubmit(meterPool, url, jsonStr, key, object.getProbeTimeout(), DiagnosicTask.class);
        }
        else if (object.getMonitorClass().equals(MonitorType.MONITOR_CLASS_PLUGIN)) {
            //pluginPool.submit(worker);
            taskSubmit(meterPool, url, jsonStr, key, object.getProbeTimeout(), DiagnosicTask.class);
        }
        else {
            LOGGER.error(">>> DiagnosicThreadPoolManage, submit, monitorClass: {}",
                    object.getMonitorClass());
        }
    }

    private static void taskSubmit(ExecutorService ex,
                                   String url,
                                   String jsonStr,
                                   String key,
                                   int timeOut,
                                   Class<?> cls) {
        DiagnosicTask task = CommonApplication.getBean(cls);

        task.config(url, jsonStr, key, timeOut);
        DiagnosicWorker worker = new DiagnosicWorker();
        worker.config(task);
        ex.submit(worker);
    }

}
