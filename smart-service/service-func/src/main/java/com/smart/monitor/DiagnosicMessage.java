package com.smart.monitor;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-23 08:53
 **/
public class DiagnosicMessage {
    private String key;
    private String msgType;      //refresh, normal
    private String objectState;  //ok, nok

    public String getKey() {return key;}
    public void setKey(String key) { this.key = key; }
    public String getMsgType() { return msgType; }
    public void setMsgType(String msgType) { this.msgType = msgType; }

    public String getObjectState() { return objectState; }
    public void setObjectState(String objectState) { this.objectState = objectState; }


    public static final class DiagnosicMessageBuilder {
        private String key;
        private String msgType;      //refresh, normal
        private String objectState;  //ok, nok

        private DiagnosicMessageBuilder() {
        }

        public static DiagnosicMessageBuilder aDiagnosicMessage() {
            return new DiagnosicMessageBuilder();
        }

        public DiagnosicMessageBuilder withKey(String key) {
            this.key = key;
            return this;
        }

        public DiagnosicMessageBuilder withMsgType(String msgType) {
            this.msgType = msgType;
            return this;
        }

        public DiagnosicMessageBuilder withObjectState(String objectState) {
            this.objectState = objectState;
            return this;
        }

        public DiagnosicMessage build() {
            DiagnosicMessage diagnosicMessage = new DiagnosicMessage();
            diagnosicMessage.setKey(key);
            diagnosicMessage.setMsgType(msgType);
            diagnosicMessage.setObjectState(objectState);
            return diagnosicMessage;
        }
    }
}
