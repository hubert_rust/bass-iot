package com.smart.monitor.object;

import com.google.common.collect.Maps;

import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-23 09:50
 **/
public class DiagnosicObject implements IDiagnosic{

    //多长时间检测一次
    //private long probeTm;
    //系统自动分配，用于监控对象分段
    private IDiagnosic parent;
    private long objectId;    //监测表的ID
    private String state;
    private String monitorType;
    private String monitorClass;
    private long probeSpan;    //探测时间长度
    private int probeTimeout; //探测消息超时时长
    //初始值为0, 需要判断在监测的时候
    private AtomicLong lastDiagnosicTm = new AtomicLong(0L);

    private String key;
    private String pluginId;
    private String pluginUrl;

    private Map<String, Object> data = Maps.newHashMap();

    public String getMonitorClass() {
        return monitorClass;
    }

    public void setMonitorClass(String monitorClass) {
        this.monitorClass = monitorClass;
    }

    public  String getState() { return state; }
    public  void setState(String state) { this.state = state; }
    public String getMonitorType() { return monitorType; }
    public void setMonitorType(String monitorType) { this.monitorType = monitorType; }
    public long getLastDiagnosicTm() {
        return lastDiagnosicTm.get();
    }
    public void setLastDiagnosicTm(long lastDiagnosicTm) {
        this.lastDiagnosicTm.set(lastDiagnosicTm);
    }

    public long getObjectId() { return objectId; }
    public void setObjectId(long objectId) { this.objectId = objectId; }
    public String getPluginId() { return pluginId; }
    public void setPluginId(String pluginId) { this.pluginId = pluginId; }
    public String getPluginUrl() { return pluginUrl; }
    public void setPluginUrl(String pluginUrl) { this.pluginUrl = pluginUrl; }

    public long getProbeSpan() { return probeSpan; }
    public void setProbeSpan(long probeSpan) { this.probeSpan = probeSpan; }

    public int getProbeTimeout() { return probeTimeout; }
    public void setProbeTimeout(int probeTimeout) { this.probeTimeout = probeTimeout; }

    public String getKey() { return key; }
    public void setKey(String key) { this.key = key; }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data.putAll(data);
    }

    @Override
    public void setParent(IDiagnosic parent) {
        this.parent = parent;
    }

    @Override
    public IDiagnosic getParent() {
        return this.parent;
    }

    @Override
    public boolean parentState() {
        return this.getState().equals("ok") ? true : false;
    }

    /**
    * @Description: 如果父节点故障，不用发送
     *              对于集中器子节点，
    * @Param:
    * @return:
    * @Author: admin
    * @Date: 2019/6/25
    */
    @Override
    public boolean checkSend() {
       IDiagnosic parent = this.parent;
       while (parent != null) {
           if (!parent.parentState()) {
               return false;
           }
           parent = parent.getParent();
       }
        return true;
    }
}
