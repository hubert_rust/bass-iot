package com.smart.monitor;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-22 17:41
 **/
public class MonitorType {
    public static final String MONITOR_COLD_METER = "cold-meter";
    public static final String MONITOR_ELEC_METER = "elec-meter";
    public static final String MONITOR_HIGH_POWER = "high-power";
    public static final String MONITOR_HOT_METER = "hot-meter";
    public static final String MONITOR_LOCKER = "locker";
    public static final String MONITOR_HUB = "hub";
    public static final String MONITOR_PLUGIN = "plugin";

    public static final String MONITOR_CLASS_PLUGIN = "plugin";
    public static final String MONITOR_CLASS_HUB = "hub";
    public static final String MONITOR_CLASS_METER = "meter";

    //plugin消息刷新，超时或正常
    public static final String MONITOR_TYPE_REFRESH = "refresh";
    public static final String MONITOR_TYPE_NORMAL = "normal";

    public static final String MONITOR_OBJECT_STATE_OK = "ok";
    public static final String MONITOR_OBJECT_STATE_NOK = "nok";
}
