package com.smart.entity;

import com.smart.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @program: smart-service
 * @description: 门锁离线密钥
 * @author: Admin
 * @create: 2019-04-22 15:28
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name="tb_hardware_locker_offline_key")
public class OFFLineSecretKeyEntity extends BaseEntity<Long> {
    private static final long serialVersionUID = 1L;
/*    @JsonSerialize(using= ToStringtb_hardware_monitor_paramSerializer.class)
    @Column(name="id")
    private Long id;*/

    @Column(name="secret_key")
    private String secretKey;
    @Column(name="hub_address")
    private String hubAddress;
    @Column(name="sub_address")
    private String subAddress;
    @Column(name="send_state")
    private String sendState;
    @Column(name="fail_times")
    private String failTimes;
    @Column(name="set_time")
    private String setTime;
}