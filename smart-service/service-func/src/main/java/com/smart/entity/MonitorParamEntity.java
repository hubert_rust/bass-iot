package com.smart.entity;

import com.smart.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @program: smart-service
 * @description: 硬件监测参数
 * @author: Admin
 * @create: 2019-04-22 15:28
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name="tb_hardware_monitor_param")
public class MonitorParamEntity extends BaseEntity<Long> {
    private static final long serialVersionUID = 1L;
/*    @JsonSerialize(using= ToStringtb_hardware_monitor_paramSerializer.class)
    @Column(name="id")
    private Long id;*/

    @Column(name="monitor_type")
    private String monitorType;
    @Column(name="probe_time")
    private String probeTime;
    @Column(name="probe_timeout")
    private String probeTimeout;

    public String getMonitorType() {
        return monitorType;
    }

    public void setMonitorType(String monitorType) {
        this.monitorType = monitorType;
    }

    public String getProbeTime() {
        return probeTime;
    }

    public void setProbeTime(String probeTime) {
        this.probeTime = probeTime;
    }

    public String getProbeTimeout() {
        return probeTimeout;
    }

    public void setProbeTimeout(String probeTimeout) {
        this.probeTimeout = probeTimeout;
    }
}