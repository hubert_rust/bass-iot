package com.smart.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static com.bx.utils.AESUtil.MD5;


public class EncryptUtil {
    public  static  final  String payKey ="pa;";
    public  static  final  String signSalt = "sg#";
    public  static  final  String balanceSalt ="ba;";
    public static String   payPasswordEncryption(String account_no, String in_pay_password) {
        String expectedSaltedPassword = MD5(account_no + payKey + in_pay_password);
        return expectedSaltedPassword;
    }
    public static String signEncryption(String srcUid, String accountNumber) {
        return MD5(srcUid + signSalt + accountNumber);
    }
    public static String encryptBalance(String accountCode, String balance) {
        return encryptBalance(accountCode, new BigDecimal(balance));
    }
    public static String encryptBalance( String account_code,  BigDecimal balance) {
        BigDecimal bigDecimal = new BigDecimal(balance.toString());
        bigDecimal = bigDecimal.setScale(2, RoundingMode.HALF_UP);
        String balanceStr = bigDecimal.toString();

        return MD5(account_code + balanceSalt + balanceStr);
    }
}
