package com.smart.utils;

/*
 * @ program: register-center
 * @ description: config
 * @ author: admin
 * @ create: 2019-03-27 16:52
 **/

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.*;
import com.smart.entity.MeterInfo;
import com.smart.entity.ReturnEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class ConfigLoadUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigLoadUtils.class);

    //public static List<Map<String, Object>> LIST =Lists.newArrayList();
    public static Map<String, Map<String, Object>> pluginMap =Maps.newHashMap();
    public static Table<String, String, Map<String, Object>> table =HashBasedTable.create();

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void loadMeterInfo() {
        synchronized (this) {
            String sql = "select * from tb_platform_hub_meter where meter_status = 'use'";
            List<Map<String, Object>> rows=jdbcTemplate.queryForList(sql);
            table.clear();
            rows.forEach(val -> {
                Map<String, Object> map = (Map<String, Object>)val;
                String hubAddress = map.get("hub_address").toString();
                String meterAddress = map.get("meter_address").toString();
                Map<String, Object> cache = Maps.newHashMap();
                cache.putAll(map);
                table.put(hubAddress, meterAddress, cache);
            });
        }
    }
    public void loadPluginConfig() {
        synchronized (this) {
            String sql = "select * from tb_plugin_config";
            List<Map<String, Object>> rows=jdbcTemplate.queryForList(sql);

            pluginMap.clear();
            rows.forEach(v-> {
                String pluginId = v.get("plugin_pack_id").toString();
                Map<String, Object> map = Maps.newHashMap();
                map.putAll(v);
                pluginMap.put(pluginId, map);
            });
        }
    }
    public static String getPluginConfigOfHardWare(String pluginId) {
        ReturnEntity returnEntity = new ReturnEntity();

        ConfigLoadUtils.table.cellSet().stream().filter(v-> {
            Map<String, Object> val = v.getValue();
            String pid = val.get("plugin_id").toString();
            return pid.equals(pluginId) ? true : false;
        }).forEach(v -> {

            String hubAddress = v.getRowKey();
            String meterAddress = v.getColumnKey();

            Map<String, Object> val = v.getValue();
            int hubBps =Integer.valueOf(val.get("bps").toString());
            int hubPort = Integer.valueOf(val.get("comm_port").toString());
            String meterType = val.get("meter_type").toString();

            MeterInfo meterInfo = new MeterInfo();
            meterInfo.setConcentratorAddress(hubAddress);
            meterInfo.setMeterAddress(meterAddress);
            meterInfo.setConcentratorBps(hubBps);
            meterInfo.setConcentratorPort(hubPort);
            meterInfo.setMeterType(meterType);

            returnEntity.getDatas().add(meterInfo);
        });

        return JSONObject.toJSON(returnEntity).toString();
    }
    public static String getPlugInConfigOfPlugin(String pluginId) {
/*
        List<Map<String, Object>> ret = ConfigLoadUtils.LIST.stream().filter(v-> {
            String get = v.get("plugin_pack_id").toString();
            if (StringUtils.isEmpty(get)) return false;
            return get.equals(pluginId) ? true: false;
        }).collect(Collectors.toList());
*/

        Map ret = ConfigLoadUtils.pluginMap.get(pluginId);


        Map<String, Object> map = Maps.newHashMap();
        if (ret != null && ret.size() > 0) {
            map.putAll(ret);
            map.put("return_code", "SUCCESS");
        }
        else {
            map.put("return_code", "FAIL");
        }

        return JSON.toJSONString(map);
    }

}
