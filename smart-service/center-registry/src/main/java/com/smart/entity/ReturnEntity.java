package com.smart.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/*
 * @ program: register-center
 * @ description: 注册信息返回结果
 * @ author: admin
 * @ create: 2019-03-27 09:30
 **/
@Data
public class ReturnEntity {
    boolean flag;
    String message;
    List<Object> datas = new ArrayList<>();
}
