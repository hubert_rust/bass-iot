package com.smart.entity;

/*
 * @ program: register-center
 * @ description: meter info
 * @ author: admin
 * @ create: 2019-03-27 10:43
 **/
public class MeterInfo {

    public String concentratorAddress;
    public int concentratorPort;
    public int concentratorBps;

    public String meterType;
    public String meterAddress;

    public int getConcentratorPort() { return concentratorPort; }
    public void setConcentratorPort(int concentratorPort) { this.concentratorPort=concentratorPort; }
    public int getConcentratorBps() { return concentratorBps; }
    public void setConcentratorBps(int concentratorBps) { this.concentratorBps=concentratorBps; }


    public String getConcentratorAddress() { return concentratorAddress; }
    public void setConcentratorAddress(String concentratorAddress) { this.concentratorAddress=concentratorAddress; }
    public String getMeterAddress() { return meterAddress; }
    public void setMeterAddress(String meterAddress) { this.meterAddress=meterAddress; }
    public String getMeterType() { return meterType; }
    public void setMeterType(String meterType) { this.meterType=meterType; }
}
