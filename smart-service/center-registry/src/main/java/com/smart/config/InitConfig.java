package com.smart.config;

import com.smart.utils.ConfigLoadUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class InitConfig implements ApplicationRunner {

    @Autowired
    private ConfigLoadUtils configLoadUtils;

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {

        //加载所以meter, hub信息
        configLoadUtils.loadMeterInfo();
        configLoadUtils.loadPluginConfig();
    }
}
