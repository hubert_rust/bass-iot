package com.smart.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class RedisConfig {

    /**
     * default select db0 for system cache
     * @param factory
     * @return
     */
    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory factory) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<String, Object>();
        redisTemplate.setConnectionFactory(factory);

        // 设置键（key）的序列化采用StringRedisSerializer。
        RedisSerializer<String> stringSerializer = new StringRedisSerializer();
        redisTemplate.setKeySerializer(stringSerializer);
        redisTemplate.setHashKeySerializer(stringSerializer);

        /*// 设置值（value）的序列化采用KyroRedisSerializer。
        RedisSerializer<Object> kyroRedisSerializer = new KryoRedisSerializer();
        redisTemplate.setValueSerializer(kyroRedisSerializer);
        redisTemplate.setHashValueSerializer(kyroRedisSerializer);*/

        redisTemplate.afterPropertiesSet();

        return redisTemplate;
    }
}
