package com.smart.controller;

import com.smart.service.PluginUpServiceImpl;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("keep")
@Api(value = "keep", description = "keep", tags = {"plugin keep"})
@Scope("prototype")
public class PluginUpController {
    private static final Logger log=LoggerFactory.getLogger(PluginUpController.class);

    @Autowired
    private PluginUpServiceImpl registerService;

    @RequestMapping(value="/register", method=RequestMethod.POST)
    public String reqIndex(@RequestBody String request) throws Exception{
        log.info(">>> register...");
        return registerService.register(request);
    }

    @RequestMapping(value="/heartbeat", method=RequestMethod.POST)
    public String heartbeat(@RequestBody String request) throws Exception{
        log.info(">>> heartbeat...");
        return registerService.register(request);
    }

    @RequestMapping(value="/download", method=RequestMethod.POST)
    public String download(@RequestBody String request) throws Exception{
        log.info(">>> download..");
        return registerService.download(request);
    }
}
