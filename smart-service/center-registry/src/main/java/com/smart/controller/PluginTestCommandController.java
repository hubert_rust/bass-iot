package com.smart.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Table;
import com.smart.utils.ConfigLoadUtils;
import com.smart.utils.HttpUtils;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/*
 * @ program: register-center
 * @ description: test
 * @ author: admin
 * @ create: 2019-04-01 09:45
 **/
@RestController
@RequestMapping("test")
@Api(value = "test", description = "test", tags = {"plugin keep"})
public class PluginTestCommandController {

    public static volatile long index = 0;

    public String send() {
        String order = "ORD" + Strings.padStart(String.valueOf(index), 6, '0');
        index = index +1;
        Map<String, Object> map =Maps.newHashMap();
        map.put("command_order", order);
        map.put("command_type", "PLAT");
        map.put("command_code", "");

        Set<Table.Cell<String, String, Map<String, Object>>> set = ConfigLoadUtils.table.cellSet();
        Iterator<Table.Cell<String, String, Map<String, Object>>> iterable = set.iterator();
        while (iterable.hasNext()) {
            Table.Cell<String, String, Map<String, Object>> v = iterable.next();
            String conAddr = v.getRowKey();
            String meterAddr = v.getColumnKey();
            map.put("con_address", conAddr);
            map.put("meter_address", meterAddr);
            break;
        }
        String json = JSON.toJSONString(map);
        //String json =JSONObject.toJSONString(new JSONObject(map));
        String ret = null;
        try {
            ret=(String) HttpUtils.commonSendHttpPost("http://192.168.1.188:9996/common/command", json);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    @RequestMapping(value="/command", method=RequestMethod.POST)
    public Object test() {
        Object ret = null;
        String resp = "SUCCESS";
        //return "SUCCESS";
        try {
            new Thread() {
                @Override
                public void run() {
                    while (true) {

                        try { sleep(20); } catch (InterruptedException e) { e.printStackTrace(); }

                        String order = "ORD" + Strings.padStart(String.valueOf(index), 6, '0');
                        index = index+1;
                        Map<String, Object> map =Maps.newHashMap();
                        map.put("command_order", order);
                        map.put("command_type", "PLAT");
                        map.put("command_code", "");

                        Set<Table.Cell<String, String, Map<String, Object>>> set = ConfigLoadUtils.table.cellSet();
                        Iterator<Table.Cell<String, String, Map<String, Object>>> iterable = set.iterator();
                        int rnd = new Random().nextInt(5);
                        int ind = 0;
                        while (iterable.hasNext()) {
                            Table.Cell<String, String, Map<String, Object>> v = iterable.next();
                            String conAddr = v.getRowKey();
                            String meterAddr = v.getColumnKey();
                            map.put("con_address", conAddr);
                            map.put("meter_address", meterAddr);
                            ind++;
                            if (ind>= rnd)
                                break;
                        }
/*                        ConfigLoadUtils.table.cellSet().forEach(v->{
                            String conAddr = v.getRowKey();
                            String meterAddr = v.getColumnKey();
                        });*/

                        /*String json="{\n" +
                                "\t\"command_order\": \"$pluginType\",\n" +
                                "\t\"command_type\": \"$ipAddress\",\n" +
                                "\t\"command_code\": \"$ipPort\"\n" +
                                "}";*/
                        String json = JSON.toJSONString(map);
                        //String json =JSONObject.toJSONString(new JSONObject(map));
                        String ret = null;
                        try {
                            ret=(String)HttpUtils.commonSendHttpPost("http://192.168.1.188:9996/common/command", json);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.start();

        } catch (Exception e) {
            System.out.println(">>> command, e: {}." + e.getMessage());
            e.printStackTrace();
        }

        return resp;
    }



}
