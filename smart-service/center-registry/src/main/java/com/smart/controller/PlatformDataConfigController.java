package com.smart.controller;

import com.smart.service.PlatformDataConfigServiceImpl;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @ Description: 平台数据配置入口
 * @ Author: Admin
**/
@RestController
@RequestMapping("config")
@Api(value = "config", description = "config", tags = {"config"})
@Scope("prototype")
public class PlatformDataConfigController {
    private static final Logger log=LoggerFactory.getLogger(PlatformDataConfigController.class);

    /**
     * @ Description: 1:表和集中器配置, 2:plugin配置文件
     * @ {"data_type":"hardware or plugin", "command_type": "push or pull", 暂时支持push}
     * @ Author: Admin
    **/
    @RequestMapping(value="/push/{plugin}", method=RequestMethod.POST)
    public Object push(@RequestBody String request, @PathVariable("plugin") String pluginId) throws Exception{
        log.info(">>> PlatformDataConfigController, push, plugin: {}", pluginId);
        return PlatformDataConfigServiceImpl.push(pluginId, request);
    }
}
