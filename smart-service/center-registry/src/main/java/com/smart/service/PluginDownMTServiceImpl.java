package com.smart.service;

import com.smart.utils.ConfigLoadUtils;
import com.smart.utils.HttpUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Map;

/*
 * @ program: register-center
 * @ description: register
 * @ author: admin
 * @ create: 2019-03-27 09:59
 **/
@Service
@Scope("prototype")
public class PluginDownMTServiceImpl {
    private static final Logger LOGGER = LoggerFactory.getLogger(PluginDownMTServiceImpl.class);

    public Object command(String request, String pluginId) {
        LOGGER.info(request);

        Map<String, Object> map = ConfigLoadUtils.pluginMap.get(pluginId);
        String url = map.get("plugin_url").toString();

        Object ret = null;
        try {
            ret= HttpUtils.commonSendHttpPost(url + "/mt/command", request);
        } catch (Exception e) {
            LOGGER.error(">>> command, e: {}.", e.getMessage());
            e.printStackTrace();
        }

        return (String)ret;
    }

}
