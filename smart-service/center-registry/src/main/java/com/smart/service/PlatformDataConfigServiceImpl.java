package com.smart.service;

import com.alibaba.fastjson.JSONObject;
import com.smart.constants.CommonConst;
import com.smart.utils.ConfigLoadUtils;
import com.smart.utils.HttpUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Map;

/*
 * @ program: register-center
 * @ description: register
 * @ author: admin
 * @ create: 2019-03-27 09:59
 **/
@Service
@Scope("prototype")
public class PlatformDataConfigServiceImpl {
    private static final Logger LOGGER = LoggerFactory.getLogger(PlatformDataConfigServiceImpl.class);

    public static Object sendHttpPostToPlugin(String pluginId, String data) {
        Map<String, Object> map = ConfigLoadUtils.pluginMap.get(pluginId);
        String url = map.get("plugin_url").toString();

        Object ret = null;
        try {
            ret= HttpUtils.commonSendHttpPost(url + "/mt/command", data);
        } catch (Exception e) {
            LOGGER.error(">>> command, e: {}.", e.getMessage());
            e.printStackTrace();
        }

        return (String)ret;
    }
    public static Object push(String pluginId, String request) {
        LOGGER.info(request);

        Map<String, Object> map =JSONObject.parseObject(request);
        String dataType  = map.get("data_type").toString();

        String sendData = "{}";
        if (dataType.equals(CommonConst.PUSH_CONFIG_DATA_HARDWARE)) {
            sendData = ConfigLoadUtils.getPluginConfigOfHardWare(pluginId);
        }
        else if (dataType.equals(CommonConst.PUSH_CONFIG_DATA_PLUGIN)) {
            sendData = ConfigLoadUtils.getPlugInConfigOfPlugin(pluginId);
        }
        else {
            return "data_type invalid";
        }

        return sendHttpPostToPlugin(pluginId, sendData);
    }

}
