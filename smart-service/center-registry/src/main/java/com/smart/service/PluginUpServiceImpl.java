package com.smart.service;

import com.alibaba.fastjson.JSONObject;
import com.smart.entity.MeterInfo;
import com.smart.entity.ReturnEntity;
import com.smart.utils.ConfigLoadUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.UUID;

/*
 * @ program: register-center
 * @ description: register
 * @ author: admin
 * @ create: 2019-03-27 09:59
 **/
@Service
@Scope("prototype")
public class PluginUpServiceImpl {
    private static final Logger LOGGER = LoggerFactory.getLogger(PluginUpServiceImpl.class);

    @Autowired
    JdbcTemplate jdbcTemplate;

    //pluginId是每个插件唯一
    private int saveDB(String pluginId) {
        String uuid =UUID.randomUUID().toString().replaceAll("-", "");
        String sql = "update tb_plugin_config set plugin_uid = '" + uuid + "' where plugin_pack_id = '"+ pluginId +"'";
        int ret = jdbcTemplate.update(sql);
        return ret;
    }

    /**
     * @ Description: 插件注册同时，下载表信息
     * @ Author: Admin
    **/
    public String register(String request) throws Exception {
        Map<String, Object> map = JSONObject.parseObject(request, Map.class);

        String pluginId = map.get("plugin_id").toString();
        //更新表
        saveDB(pluginId);

        String pluginType = map.get("plugin_type").toString();
        if (pluginType.equals("elec_meter")) {
            ReturnEntity returnEntity = new ReturnEntity();

            ConfigLoadUtils.table.cellSet().stream().filter(v-> {
                Map<String, Object> val = v.getValue();
                String pid = val.get("plugin_id").toString();
                return pid.equals(pluginId) ? true : false;
            }).forEach(v -> {

                String hubAddress = v.getRowKey();
                String meterAddress = v.getColumnKey();

                Map<String, Object> val = v.getValue();
                int hubBps =Integer.valueOf(val.get("bps").toString());
                int hubPort = Integer.valueOf(val.get("comm_port").toString());
                String meterType = val.get("meter_type").toString();

                MeterInfo meterInfo = new MeterInfo();
                meterInfo.setConcentratorAddress(hubAddress);
                meterInfo.setMeterAddress(meterAddress);
                meterInfo.setConcentratorBps(hubBps);
                meterInfo.setConcentratorPort(hubPort);
                meterInfo.setMeterType(meterType);

                returnEntity.getDatas().add(meterInfo);
            });

            return JSONObject.toJSON(returnEntity).toString();
        }
        return "";
    }

    /**
     * @ Description: 插件下载配置文件
     * @ Author: Admin
    **/
    public String download(String request) throws Exception {
        LOGGER.info(">>> download...");

        ReturnEntity returnEntity = new ReturnEntity();

        Map<String, Object> map = JSONObject.parseObject(request, Map.class);
        String pluginId = map.get("plugin_id").toString();

        return ConfigLoadUtils.getPlugInConfigOfPlugin(pluginId);
    }
}
