package com.smart.toolkit;

import com.smart.Application;
import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.util.Properties;
import java.util.UUID;

/**
 * @program: smart-service
 * @description: id
 * @author: Admin
 * @create: 2019-04-22 16:07
 **/

/**
 * <p>
 * 高效GUID产生算法(sequence),基于Snowflake实现64位自增ID算法。 <br>
 * 优化开源项目 http://git.oschina.net/yu120/sequence
 * </p>
 *
 * @author hubin
 * @since 2016-08-01
 */
public class IDGenerator implements IdentifierGenerator, Configurable{

    /**
     * 主机和进程的机器码
     */
    private static Sequence WORKER = null;
    static {
        try {
            WORKER = new Sequence(Application.dcId, Application.appId);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static long getId() {
        return WORKER.nextId();
    }

    public static String getIdStr() {
        return String.valueOf(WORKER.nextId());
    }

    /**
     * <p>
     * 有参构造器
     * </p>
     *
     * @param workerId     工作机器 ID
     * @param datacenterId 序列号
     */
    public static void initSequence(long workerId, long datacenterId) {
        try {
            WORKER = new Sequence(workerId, datacenterId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * <p>
     * 获取去掉"-" UUID
     * </p>
     */
    public static synchronized String get32UUID() {
        return UUID.randomUUID().toString().replace(StringPool.DASH, StringPool.EMPTY);
    }

    @Override
    public void configure(Type type, Properties params, ServiceRegistry serviceRegistry) throws MappingException {


    }

    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
        return getId();
    }
}

