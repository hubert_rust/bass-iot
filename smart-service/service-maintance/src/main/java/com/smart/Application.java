package com.smart;

import com.smart.factory.HardwarFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.freemarker.FreeMarkerProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ApplicationContext;

import java.util.Arrays;

/**
 * @program: smart-service
 * @description: app
 * @author: Admin
 * @create: 2019-04-17 19:58
 **/

@EnableDiscoveryClient
@SpringBootApplication
@EnableConfigurationProperties(value= FreeMarkerProperties.class)
public class Application {
    //生产表id时用
    public static long dcId = 1;
    public static long appId = 0;

    @Autowired
    private ApplicationContext appContext;

    public static HardwarFactory hardwarFactory;


    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

   /* @Override
    public void run(String... args) throws Exception {
        System.out.println(">>>");

        String[] beans = appContext.getBeanDefinitionNames();

        Arrays.sort(beans);

        for (String bean : beans)

        {

            System.out.println(bean + " of Type :: " + appContext.getBean(bean).getClass());

        }
    }*/
}