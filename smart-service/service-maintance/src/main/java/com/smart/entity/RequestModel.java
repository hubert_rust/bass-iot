package com.smart.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @program: smart-service
 * @description: request
 * @author: Admin
 * @create: 2019-04-23 09:18
 **/

@EqualsAndHashCode(callSuper = false)
public class RequestModel<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    private String serviceOrder;  // create by service side
    private String commandOrder;  // create by hardward platform
    private String hardwareType;  // HUB, HOT-METER
    private String cmdCode;       //
    private String cmdType;
    private List<T> entitys;

    public String getHardwareType() { return hardwareType; }
    public void setHardwareType(String hardwareType) { this.hardwareType = hardwareType; }
    public String getCmdCode() { return cmdCode; }
    public void setCmdCode(String cmdCode) { this.cmdCode = cmdCode; }
    public List<T> getEntitys() { return entitys; }
    public void setEntitys(List<T> entitys) { this.entitys = entitys; }
}