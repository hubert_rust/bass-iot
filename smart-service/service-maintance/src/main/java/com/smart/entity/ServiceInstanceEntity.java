package com.smart.entity;

/**
 * @program: smart-service
 * @description: a
 * @author: Admin
 * @create: 2019-04-28 08:28
 **/
public class ServiceInstanceEntity {
    private String serviceName;
    private String ip;
    private String port;
    private String id;
    private String regTime;

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRegTime() {
        return regTime;
    }

    public void setRegTime(String regTime) {
        this.regTime = regTime;
    }
}