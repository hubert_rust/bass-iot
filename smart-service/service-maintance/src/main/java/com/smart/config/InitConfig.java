package com.smart.config;

import com.smart.Application;
import com.smart.factory.HardwarFactory;
import com.smart.framework.utils.CommonApplication;
import com.smart.framework.utils.PFConstDef;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.TreeCache;
import org.apache.curator.framework.recipes.cache.TreeCacheEvent;
import org.apache.curator.framework.recipes.cache.TreeCacheListener;
import org.apache.curator.utils.ZKPaths;
import org.apache.zookeeper.CreateMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * @program: smart-service
 * @description: init
 * @author: Admin
 * @create: 2019-04-22 14:54
 **/

@Component
public class InitConfig implements CommandLineRunner {

    @Autowired
    private CuratorFramework curatorFramework;

    private TreeCache treeCache;

    @Autowired
    JdbcTemplate jdbcTemplate;


    private void configNode() {
        try {
            if (curatorFramework.checkExists().forPath("/datas") == null) {
                curatorFramework.create().withMode(CreateMode.PERSISTENT).forPath("/datas");
            }
            if (curatorFramework.checkExists().forPath("/datas/hubs") == null) {
                curatorFramework.create().withMode(CreateMode.PERSISTENT).forPath("/datas/hubs");
            }
            if (curatorFramework.checkExists().forPath("/datas/meters") == null) {
                curatorFramework.create().withMode(CreateMode.PERSISTENT).forPath("/datas/meters");
            }

            treeCache = new TreeCache(curatorFramework, "/datas");
            treeCache.start();
            TreeCacheListener treeCacheListener = new TreeCacheListener() {
                @Override
                public void childEvent(CuratorFramework client, TreeCacheEvent event) throws Exception {
                    switch (event.getType()) {
                        case NODE_ADDED:
                            System.out.println("Node add " + ZKPaths.getNodeFromPath(event.getData().getPath()));
                            break;
                        case NODE_REMOVED:
                            System.out.println("Node removed " + ZKPaths.getNodeFromPath(event.getData().getPath()));
                            break;
                        case NODE_UPDATED:
                            System.out.println("Node updated " + ZKPaths.getNodeFromPath(event.getData().getPath()));
                            break;

                    }
                }
            };
            treeCache.getListenable().addListener(treeCacheListener);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //初始化加载service
    /**
    * @ Description: 业务处理，主要是硬件配置service在这里加载
    * @ Param:
    * @ return:
    * @ Author: Admin
    * @ Date:
    */
    @Override
    public void run(String... args) throws Exception {
        System.out.println(">>> InitConfig");

        //创建zookeeper节点数据配置节点
        configNode();

        Application.hardwarFactory = HardwarFactory.factory(builder -> {
            builder.add(PFConstDef.REQ_HARDWARE_TYPE_HUB, CommonApplication.getBean("HubHardware"));
            builder.add(PFConstDef.REQ_HARDWARE_TYPE_HOT_METER, CommonApplication.getBean("MeterHardware"));
            builder.add(PFConstDef.REQ_HARDWARE_TYPE_METER, CommonApplication.getBean("MeterHardware"));
            builder.add(PFConstDef.REQ_HARDWARE_TYPE_MONITOR, CommonApplication.getBean("MonitorHardware"));
            builder.add(PFConstDef.REQ_HARDWARE_TYPE_PLUGIN, CommonApplication.getBean("PluginHardware"));
            builder.add(PFConstDef.REQ_HARDWARE_TYPE_SECRETKEY, CommonApplication.getBean("OfflineSecretKey"));
            builder.add(PFConstDef.REQ_HARDWARE_TYPE_LOCKER, CommonApplication.getBean("OfflineSecretKey"));

            //抄表数据，2种: balance和基本数据表
            builder.add(PFConstDef.REQ_HARDWARE_TYPE_SECRETKEY, CommonApplication.getBean("OfflineSecretKey"));

            //抄表
            builder.add(PFConstDef.REQ_HARDWARE_TYPE_COLD_READ, CommonApplication.getBean("MeterColdRead"));
            builder.add(PFConstDef.REQ_HARDWARE_TYPE_COLD_BALANCE, CommonApplication.getBean("MeterColdReadBalance"));
            builder.add(PFConstDef.REQ_HARDWARE_TYPE_ELEC_READ, CommonApplication.getBean("MeterElecRead"));
            builder.add(PFConstDef.REQ_HARDWARE_TYPE_ELEC_BALANCE, CommonApplication.getBean("MeterElecReadBalance"));
            builder.add(PFConstDef.REQ_HARDWARE_TYPE_HIGH_READ, CommonApplication.getBean("MeterHighRead"));

            //hot-read
            builder.add(PFConstDef.REQ_HARDWARE_TYPE_HOT_READ, CommonApplication.getBean("MeterHotRead"));
            builder.add(PFConstDef.REQ_HARDWARE_TYPE_HOT_BALANCE, CommonApplication.getBean("MeterHotReadBalance"));
            builder.add(PFConstDef.REQ_HARDWARE_TYPE_HOT_USER, CommonApplication.getBean("MeterHotReadUser"));


            //job-log
            builder.add(PFConstDef.REQ_HARDWARE_TYPE_JOB_LOG, CommonApplication.getBean("JobLogRead"));

            //meter-recharge
            builder.add(PFConstDef.REQ_HARDWARE_TYPE_METER_RECHARGE, CommonApplication.getBean("MeterRecharge"));
        });

    }

    //程序启动的时候，将数据加载到zookeeper，
    @Deprecated
    private void loadDataToZookeeper() {

    }

}