package com.smart.config;

import com.smart.MessageTopic;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-16 15:11
 **/
public interface IPluginDataSource {
    String OUTPUT_PLUGINDATA = MessageTopic.MESSAGE_TOPIC_PLUGINDATA;
    @Output(OUTPUT_PLUGINDATA)
    MessageChannel output();

/*    String OUTPUT1 = "output1";
    @Output(OUTPUT1)
    MessageChannel output1();*/
}
