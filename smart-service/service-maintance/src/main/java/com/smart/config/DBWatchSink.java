package com.smart.config;

import com.smart.framework.bassentity.hardware.PluginRegisterVo;
import com.smart.framework.utils.CacheUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;

/**
 * @program: 数据库监听消息, 因为消息队列消费端涉及分组，在管理层，采用将数据保持到缓存中，
 * 对于插件层，通知插件去keep模块获取，插件层数据的获取只有这个通道
 * 在管理层数据保存在redis中，key规则：表名+id(例如：tb_hardware_hub_19294798329)
 * @description:
 * @author: admin
 * @create: 2019-05-16 15:34
 **/
@EnableBinding(IDBWatchSink.class)
public class DBWatchSink {
    private static final Logger log = LoggerFactory.getLogger(DBWatchSink.class);

    @Autowired
    private CacheUtils cacheUtils;

    @Autowired
    private PluginDataSource pluginDataSource;

    @Autowired
    private MaintanceMessage maintanceMessage;


    /**
    * @Description: 插件发生通知消息
    * @Param:
    * @return:
    * @Author: admin
    * @Date: 2019/5/17
    */
    @StreamListener(IDBWatchSink.INPUT_DBWATCH)
    public void process(Message<?> message) {
        log.info(">>> DBWatchSink: " +message.getPayload());

        //需要过滤
        cacheUtils.refreshCache((String) message.getPayload());
        //过滤，
        pluginDataSource.sendMessage((String)message.getPayload());

        //auto,
/*        Acknowledgment acknowledgment = message.getHeaders().get(KafkaHeaders.ACKNOWLEDGMENT, Acknowledgment.class);
        if (acknowledgment != null) {
            System.out.println("pluginDataSource Acknowledgement provided");
            acknowledgment.acknowledge();
        }*/

        maintanceMessage.sendMessage((String)message.getPayload());
    }

    private boolean filter(String message) {

        return true;
    }
}
