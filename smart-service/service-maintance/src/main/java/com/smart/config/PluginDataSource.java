package com.smart.config;

import com.google.gson.Gson;
import com.smart.DBWatchMessage;
import com.smart.PluginUseTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-16 15:14
 **/
@Component
@EnableBinding(IPluginDataSource.class)
public class PluginDataSource {
    private static final Logger log = LoggerFactory.getLogger(PluginDataSource.class);
    @Autowired
    private IPluginDataSource source;

    public void sendMessage(String entity) {
        if (filter(entity)) {
            source.output().send(MessageBuilder.withPayload(entity).build());
        }
    }

    private static boolean filter(String data) {
        Map<String, Object> map = new Gson().fromJson(data, Map.class);
        if (!map.get(DBWatchMessage.WATCH_MESSAGE_FD_DATABASE).toString().equals(PluginUseTable.DATABASE_NAME)) return false;
        if (map.get(DBWatchMessage.WATCH_MESSAGE_FD_TABLE).toString().equals(PluginUseTable.PLUGIN_USE_HUB_TABLE)
                || map.get(DBWatchMessage.WATCH_MESSAGE_FD_TABLE).toString().equals(PluginUseTable.PLUGIN_USE_METER_TABLE)) {
            return true;
        }
        return false;
    }
}
