package com.smart.service;

import com.google.common.base.Strings;
import com.smart.DBWatchMessage;
import com.smart.framework.utils.CacheUtils;
import com.smart.framework.utils.PFConstDef;
import com.smart.framework.utils.ResponseUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-17 15:49
 **/
@Service
public class CacheServiceImpl {
    private static final Logger log = LoggerFactory.getLogger(CacheServiceImpl.class);

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    CacheUtils cacheUtils;

    public String refreshCache() throws Exception {
        List<Map<String, Object>> mapList = jdbcTemplate.queryForList("select * from tb_cached_tables");
        mapList.forEach(v -> {
            Map<String, Object> map = (Map) v;
            String tableName = map.get("table_name").toString();
            cachedTable(tableName);
        });
        return ResponseUtils.getResponseBody()
                .put(PFConstDef.RESP_RETURN_CODE, PFConstDef.RESP_RETURN_CODE_SUCCESS)
                .put(PFConstDef.RESP_RETURN_MSG, "操作成功")
                .getResponseBody();
    }

    public String getCache(String tableName, String id) throws Exception {
        List<Map> list = cacheUtils.getCache(tableName, id);

        return ResponseUtils.getResponseBody()
                .put(PFConstDef.RESP_RETURN_CODE, PFConstDef.RESP_RETURN_CODE_SUCCESS)
                .put(PFConstDef.RESP_RETURN_MSG, "操作成功")
                .put(PFConstDef.RESP_RETURN_DATAS, list)
                .getResponseBody();
    }

    public String checkCache(String tableName, String id) throws Exception {
        String sql = "select * from " + tableName;
        List<Map<String, Object>> checkList = jdbcTemplate.queryForList(sql);
        /*List checkList = mapList.stream().filter(v -> {
            String table = v.get("table_name").toString();
            return Strings.isNullOrEmpty(tableName) ? true : table.equals(tableName);
        }).collect(Collectors.toList());*/

        //求差集，以数据库的为准
        List<Map> cacheMap = cacheUtils.getCache(tableName, null);
        checkList.removeAll(cacheMap);
        return ResponseUtils.getResponseBody()
                .put(PFConstDef.RESP_RETURN_CODE, PFConstDef.RESP_RETURN_CODE_SUCCESS)
                .put(PFConstDef.RESP_RETURN_MSG, "操作成功")
                .put(PFConstDef.RESP_RETURN_DATAS, checkList)
                .getResponseBody();
    }

    private void cachedTable(String tableName) {
        List<Map<String, Object>> mapList = jdbcTemplate.queryForList("select * from " + tableName);
        mapList.forEach(v -> {
            cacheUtils.syncCache(tableName, v, DBWatchMessage.WATCH_MESSAGE_INSERT);
        });

    }
}
