package com.smart.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: smart-service
 * @description: bass平台登录，一次登录所有都可访问, 更新表tb_outer_service_manage中的url
 * 在service-registry中使用, plugin层或者平台层消息推送
 * @author: Admin
 * @create: 2019-04-23 19:29
 **/

@RestController
@RequestMapping("bass")
public class BassLoginController {

    private static final Logger log = LoggerFactory.getLogger(BassLoginController.class);

    @PostMapping("login")
    public String login(@RequestBody String request) throws Exception {

        return "";
    }

    @PostMapping("logout")
    public String logout() throws Exception {
        return "";
    }
}