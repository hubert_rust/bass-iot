package com.smart.controller;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.smart.Application;
import com.smart.entity.RequestModel;
import com.smart.factory.IHardware;
import com.smart.framework.utils.PFConstDef;
import com.smart.framework.utils.ResponseUtils;
import com.smart.request.BassHardwardRequest;
import com.smart.service.CacheServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @program: smart-service
 * @description: maintance
 * @author: Admin
 * @create: 2019-04-22 16:53
 **/
@RestController
@RequestMapping("locker")
@Scope("prototype")
public class LockerController extends BaseController {
    private static final Logger log = LoggerFactory.getLogger(LockerController.class);

    @Autowired
    CacheServiceImpl cacheService;

    @PostMapping("proc")
    public String requestProcess(@RequestBody String request) {
        try {
            //RequestModel requestModel = new Gson().fromJson(request, RequestModel.class);
            BassHardwardRequest requestModel = new Gson().fromJson(request, BassHardwardRequest.class);
            checkValid(requestModel.getSubType());
            IHardware hardware = Application.hardwarFactory.create(requestModel.getSubType().toUpperCase());

            List ret = hardware.process(requestModel.getSubType(),
                    requestModel.getEntitys(), requestModel.getCmdCode(),
                    requestModel.getCmdCode(), requestModel.getAutoPush());
            return ResponseUtils.getResponseBody()
                    .put(PFConstDef.RESP_RETURN_CODE, PFConstDef.RESP_RETURN_CODE_SUCCESS)
                    .put(PFConstDef.RESP_RETURN_MSG, "操作成功")
                    .put(PFConstDef.RESP_RETURN_DATAS, ret)
                    .getResponseBody();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseUtils.getResponseBody()
                    .put(PFConstDef.RESP_RETURN_CODE, PFConstDef.RESP_RETURN_CODE_FAIL)
                    .put(PFConstDef.RESP_RETURN_MSG, e.getMessage())
                    .getResponseBody();
        }

    }




    public static void main(String[] args) {
        RequestModel requestModel = new RequestModel();
        Map entity = Maps.newHashMap();
        entity.put("demo", "vv");
        List<Map> lst = Lists.newArrayList();
        lst.add(entity);

        requestModel.setEntitys(lst);

        String var1 = new Gson().toJson(requestModel);

        RequestModel var2 = new Gson().fromJson(var1, RequestModel.class);
        System.out.println();
    }
}