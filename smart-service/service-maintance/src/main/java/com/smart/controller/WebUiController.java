package com.smart.controller;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.smart.entity.ServiceInstanceEntity;
import org.apache.curator.framework.CuratorFramework;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @program: smart-service
 * @description: d
 * @author: Admin
 * @create: 2019-04-26 09:38
 **/
@RestController
@RequestMapping("web")
public class WebUiController {
    public static final String ROOT_NAME = "/services";

    private static final Logger log = LoggerFactory.getLogger(WebUiController.class);

    @Autowired
    CuratorFramework curatorFramework;

    @RequestMapping(value ="/index")
    public ModelAndView index(ModelMap map) throws Exception{
        map.addAttribute("name", "maintance");
        ModelAndView modelAndView = new ModelAndView("index");
        List<ServiceInstanceEntity> modList = Lists.newArrayList();
        try {
            List<String> list = curatorFramework.getChildren().forPath("/services");

            for (String v: list) {
                String var1 = ROOT_NAME + "/" + v;
                List<String> services = curatorFramework.getChildren().forPath(var1);

                for (String node: services) {
                    String jsonStr = new String(curatorFramework.getData().forPath(var1 + "/" + node));
                    Map<String, Object> ndMap = new Gson().fromJson(jsonStr, Map.class);
                    ServiceInstanceEntity instanceEntity = new ServiceInstanceEntity();
                    instanceEntity.setIp(ndMap.get("address").toString());
                    instanceEntity.setPort(ndMap.get("port").toString());
                    instanceEntity.setServiceName(ndMap.get("name").toString());
                    instanceEntity.setId(ndMap.get("id").toString());
                    instanceEntity.setRegTime(ndMap.get("registrationTimeUTC").toString());

                    modList.add(instanceEntity);
                }

            };
            modelAndView.addObject("serviceList", modList);
/*            String path = "/services/" + list.get(0);
            curatorFramework.checkExists()
            curatorFramework.getZookeeperClient().isConnected();*/





        } catch (Exception e) {
            e.printStackTrace();
        }

        return modelAndView;
    }
}