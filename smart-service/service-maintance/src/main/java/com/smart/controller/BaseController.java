package com.smart.controller;

import com.google.common.base.Strings;
import com.smart.base.BaseEntity;
import com.smart.framework.utils.PFConstDef;

/**
 * @program: smart-service
 * @description: base
 * @author: Admin
 * @create: 2019-04-22 17:43
 **/

public class BaseController<T extends BaseEntity<Long>> {

    protected boolean checkValid(String hdType) throws Exception {
        if (Strings.isNullOrEmpty(hdType)) {
            throw new Exception("subType invalid");
        }
        return true;

/*        if (hdType.equals(PFConstDef.REQ_HARDWARE_TYPE_HUB)
             || hdType.equals(PFConstDef.REQ_HARDWARE_TYPE_HOT_METER)
             || hdType.equals(PFConstDef.REQ_HARDWARE_TYPE_MONITOR)
             || hdType.equals(PFConstDef.REQ_HARDWARE_TYPE_SECRETKEY)
                || hdType.equals(PFConstDef.REQ_HARDWARE_TYPE_PLUGIN)) {
            return true;
        }
        else {

            throw new Exception("subType 类型不正确");
        }*/
    }

}