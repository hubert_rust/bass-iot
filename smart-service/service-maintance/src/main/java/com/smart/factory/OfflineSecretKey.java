package com.smart.factory;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.smart.common.ResponseEntity;
import com.smart.entity.LockerOfflineKeyEntity;
import com.smart.entity.MonitorEntity;
import com.smart.framework.utils.PFConstDef;
import com.smart.service.BaseServiceImpl;
import com.smart.utils.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @program: smart-service
 * @description: meter
 * @author: Admin
 * @create: 2019-04-22 19:25
 **/

@Component("OfflineSecretKey")
public class OfflineSecretKey extends BaseServiceImpl<LockerOfflineKeyEntity> implements IHardware {
    private static final Logger log = LoggerFactory.getLogger(MeterHardware.class);

    @Override
    public List process(String hdType,
                        List list,
                        String cmdCode,
                        String cmdType,
                        int autoPush) throws Exception {

        try {
            if (cmdCode.equals("password")) {
                if (Objects.isNull(list) || list.size() <= 0) {
                    throw new Exception("list is null or empty");
                }
                Map map = (Map<String, Object>) list.get(0);
                String pluginId = map.get("pluginId").toString();
                String hubAddr = map.get("hubAddress").toString();
                String subAddr = map.get("subAddress").toString();
                String setTime = map.get("setTime").toString();
                String timeSpanMode = map.get("timeSpanMode").toString();
                String startTime = map.get("startTime").toString();
                String finishTime = map.get("finishTime").toString();

                Optional<LockerOfflineKeyEntity> ret = super.findOne((root, query, builder) -> {
                    Predicate p1 = builder.equal(root.get("hubAddress"), hubAddr);
                    Predicate p2 = builder.equal(root.get("PluginId"), pluginId);
                    Predicate p3 = builder.equal(root.get("subAddress"), subAddr);
                    Predicate p4 = builder.equal(root.get("setTime"), setTime);
                    return builder.and(p1, p2, p3, p4);
                });
                if (!ret.isPresent()) {
                    throw new Exception("没有查询到密钥");
                }

                String secretKey = "";
                if (timeSpanMode.equals("15")) {
                    secretKey = createPasswordFifteenMin(Long.valueOf(startTime),
                            Long.valueOf(finishTime),
                            Long.valueOf(ret.get().getSecretKey()));
                } else if (timeSpanMode.equals("60")) {
                    secretKey = createPasswordOneHour(Long.valueOf(startTime),
                            Long.valueOf(finishTime),
                            Long.valueOf(ret.get().getSecretKey()));

                }

                List<Map<String, Object>> listResp = Lists.newArrayList();
                Map<String, Object> mapResp = Maps.newHashMap();
                mapResp.put("password", secretKey);
                mapResp.putAll(map);
                listResp.add(mapResp);
                return listResp;
                //查询接口
            } else if (cmdCode.equals("serverTime")) {
                List<Map<String, Object>> listResp = Lists.newArrayList();
                Map<String, Object> mapResp = Maps.newHashMap();
                mapResp.put("serverTime", LocalDateTime.now().toString().replace("T", " "));
                listResp.add(mapResp);
                return listResp;
            } else {
                log.error(">>> HubHardware, process, cmdCode: {} is invalid", cmdCode);
                throw new Exception("cmdCode正确值应该为: password");

            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public static long leftdata(long data, long left) {
        return ((data << left) + (data >> (19 - left))) & 0x7ffff;
    }

    public static long rightdata(long data, long left) {
        return ((data >> left) + (data << (19 - left))) & 0x7ffff;
    }

    /**
     * @Description:
     * @Param: 时间段: 8:00-9:00 startTime:8  endTime:9
     * @return:
     * @Author: admin
     * @Date: 2019/6/28
     */
    public static String createPasswordOneHour(long startTime, long endTime, long secretKey) {
        long data = ((startTime * 24) + endTime) * (secretKey % 910);
        for (int i = 0; i < 8; i++) {
            data = leftdata(data, (secretKey >> 4) & 0x0f);
            data ^= rightdata(secretKey, i * 2 + 1);
        }
        String password = String.valueOf(data);
        //如果密码小于6位,则在前面加0
        for (int i = password.length(); i < 6; i++) {
            password = "0" + password;
        }
        return password;
    }

    /**
     * @Description:
     * @Param: startTime和endTime单位是时钟刻度(将时钟分为96格 ， 每格15分钟)(例如8 : 15 - 8 : 30对应 ( 8 * 60 + 15)/15-(8*60+30)/15)
     * @return:
     * @Author: admin
     * @Date: 2019/7/1
     */
    public static String createPasswordFifteenMin(long startTime, long endTime, long secretKey) {
        LocalDateTime dateTime = LocalDateTime.ofEpochSecond(startTime, 0, ZoneOffset.ofHours(8));

        System.out.println(">>> createPassword, startTime: +" + dateTime.toString());
        long data = ((startTime * 24 * 4) + endTime) * (secretKey % 56);
        for (int i = 0; i < 8; i++) {
            data = leftdata(data, (secretKey >> 4) & 0x0f);
            data ^= rightdata(secretKey, i * 2 + 1);
        }
        String password = String.valueOf(data);
        //如果密码小于6位,则在前面加0
        for (int i = password.length(); i < 6; i++) {
            password = "0" + password;
        }
        return password;
    }

    public static void main(String[] args) {
        String passwd =createPasswordFifteenMin((17*60+0)/15, (17*60+15)/15, 1388000549);
        System.out.println(passwd);
    }
}