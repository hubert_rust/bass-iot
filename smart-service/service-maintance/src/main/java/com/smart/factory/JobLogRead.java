package com.smart.factory;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.smart.entity.CronJobLogEntity;
import com.smart.entity.HighMeterReadEntity;
import com.smart.service.BaseServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @program: smart-service
 * @description: meter
 * @author: Admin
 * @create: 2019-04-22 19:25
 **/

@Component("JobLogRead")
public class JobLogRead extends BaseServiceImpl<CronJobLogEntity> implements IHardware {
    private static final Logger log = LoggerFactory.getLogger(JobLogRead.class);
    @Override
    public List process(String hdType, List list, String cmdCode, String cmdType, int autoPush) throws Exception {
        Preconditions.checkArgument(cmdCode.equals("readJobLog"),
                "cmdType应该为: readJobLog");
        Preconditions.checkArgument((Objects.nonNull(list) && list.size()>0),
                "list is invalid");


        List<CronJobLogEntity> lists = Lists.newArrayList();
        for (int i=0; i<list.size(); i++) {
            Map<String, Object> map = (Map<String, Object>) list.get(i);
            Preconditions.checkArgument(Objects.nonNull(map.get("pluginId")),
                    "pluginId is invalid");
            Preconditions.checkArgument(Objects.nonNull(map.get("subAddress")),
                    "subAddress is invalid");
            Preconditions.checkArgument(Objects.nonNull(map.get("hubAddress")),
                    "hubAddress is invalid");
            Preconditions.checkArgument(Objects.nonNull(map.get("setTime")),
                    "setTime is invalid");


            List<CronJobLogEntity> opt = findLockerSyncTime(map.get("pluginId").toString(),
                    map.get("hubAddress").toString(),
                    map.get("subAddress").toString(),
                    map.get("setTime").toString());

            lists.addAll(opt);
        }

        return lists;
    }
}