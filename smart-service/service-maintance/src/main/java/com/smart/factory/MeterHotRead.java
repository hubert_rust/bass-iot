package com.smart.factory;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.smart.entity.ColdMeterReadEntity;
import com.smart.entity.HotMeterReadEntity;
import com.smart.service.BaseServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @program: smart-service
 * @description: meter
 * @author: Admin
 * @create: 2019-04-22 19:25
 **/

@Component("MeterHotRead")
public class MeterHotRead extends BaseServiceImpl<HotMeterReadEntity> implements IHardware {
    private static final Logger log = LoggerFactory.getLogger(MeterHotRead.class);

    //电压：readMeterInfo
    //余额：readMeterBalance
    /*list: [{
        "pluginId": "LOCKER-PLUGIN-001",
                "hubAddress": "0002471002",
                "subAddress": "188",
                "setTime": "2019-07-17",
    }]*/
    //subType: COLD-READ 使用量
    @Override
    public List process(String hdType, List list, String cmdCode, String cmdType, int autoPush) throws Exception {
        Preconditions.checkArgument(cmdCode.equals("readMeterInfo"),
                "热水表抄表(总使用量)cmdType应该为: readMeterInfo");
        Preconditions.checkArgument((Objects.nonNull(list) && list.size()>0),
                "list is invalid");


        List<HotMeterReadEntity> lists = Lists.newArrayList();
        for (int i=0; i<list.size(); i++) {
            Map<String, Object> map = (Map<String, Object>) list.get(i);
            Preconditions.checkArgument(Objects.nonNull(map.get("pluginId")),
                    "pluginId is invalid");
            Preconditions.checkArgument(Objects.nonNull(map.get("subAddress")),
                    "subAddress is invalid");
            Preconditions.checkArgument(Objects.nonNull(map.get("hubAddress")),
                    "hubAddress is invalid");
            Preconditions.checkArgument(Objects.nonNull(map.get("setTime")),
                    "setTime is invalid");

            String hubAddress= map.get("hubAddress").toString();
            String subAddress = map.get("subAddress").toString();
            String setTime = map.get("setTime").toString();
            int year = Integer.valueOf(setTime.split("-")[0]);
            int month = Integer.valueOf(setTime.split("-")[1]);
            int day = Integer.valueOf(setTime.split("-")[2]);
            LocalDateTime localDateTime1 = LocalDateTime.of(year, month ,day, 0, 0, 0);
            LocalDateTime localDateTime2 = LocalDateTime.of(year, month ,day, 23, 59, 59);

            List<HotMeterReadEntity> opt = baseRepostory.findAll((root, query, builder)->{
                Predicate p1 = builder.equal(root.get("hubAddress"), hubAddress);
                Predicate p2 = builder.equal(root.get("meterAddress"), subAddress);
                Predicate p3 = builder.between(root.get("createTime"), localDateTime1, localDateTime2);
                return builder.and(p1, p2, p3);
            });

/*            List<HotMeterReadEntity> opt = findColdRead(map.get("pluginId").toString(),
                    map.get("hubAddress").toString(),
                    map.get("subAddress").toString(),
                    map.get("setTime").toString());*/

            lists.addAll(opt);
        }

        return lists;
    }
}