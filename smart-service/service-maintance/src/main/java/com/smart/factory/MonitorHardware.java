package com.smart.factory;

import com.smart.entity.MonitorEntity;
import com.smart.service.BaseServiceImpl;
import org.springframework.stereotype.Component;

/**
 * @program: smart-service
 * @description: meter
 * @author: Admin
 * @create: 2019-04-22 19:25
 **/

@Component("MonitorHardware")
public class MonitorHardware extends BaseServiceImpl<MonitorEntity> implements IHardware {
    /*private static final Logger log = LoggerFactory.getLogger(MeterHardware.class);

    @Autowired
    MeterRepostory meterRepostory;
    @Override
    public String process(Map map, String cmdCode) throws Exception {
        super(map, cmdCode);
        return null;
    }*/
}