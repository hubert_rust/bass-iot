package com.smart.factory;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.smart.common.PFConstDef;
import com.smart.common.ResultConst;
import com.smart.entity.ConcentratorEntity;
import com.smart.entity.PluginEntity;
import com.smart.framework.utils.HttpUtils;
import com.smart.repository.PluginRepostory;
import com.smart.request.BassHardwardRequest;
import com.smart.service.BaseServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @program: smart-service
 * @description: hub
 * @author: Admin
 * @create: 2019-04-22 19:25
 **/

@Component("HubHardware")
public class HubHardware extends BaseServiceImpl<ConcentratorEntity> implements IHardware {
    private static final Logger log = LoggerFactory.getLogger(HubHardware.class);

    @Autowired
    PluginRepostory pluginRepostory;

    @Override
    public List process(String hdType, List list, String cmdCode, String cmdType, int autoPush) throws Exception {
        if (cmdCode.equals("serverTime")) {
            List<Map<String, Object>> timeList = Lists.newArrayList();
            Map<String, Object> map = Maps.newHashMap();
            map.put("time", LocalDateTime.now().toString().replace("T", " "));
            timeList.add(map);
            return timeList;
        }
        else {
            List<Map<String, Object>> configList = null;

            try {
                configList =  super.process(hdType, list, cmdCode, cmdType, autoPush);
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception(e.getMessage());
            }

            if (cmdCode.equals(PFConstDef.REQ_CMD_CODE_ADD)
                    || cmdCode.equals(PFConstDef.REQ_CMD_CODE_DEL)
                    || cmdCode.equals(PFConstDef.REQ_CMD_CODE_MOD)) {

                if (list.size() <=0) {
                    return configList;
                }
                Object obj = ((Map<String, Object>)list.get(0)).get("pluginId");;
                if (Objects.isNull(obj)) {
                    throw new Exception("pluginId is invalid");
                }

                String pluginId = obj.toString();
                Optional<PluginEntity> entity = pluginRepostory.findOne((root, query, builder) -> {
                    return builder.equal(root.get("pluginId"), pluginId);
                });

                if (!entity.isPresent()) {
                    throw new Exception("plugin have not config");
                }

                //单条记录
                List<Map<String, Object>> realList = (List<Map<String, Object>>)list;
                for (Map<String, Object> val: realList) {
                    String url = entity.get().getPluginUrl();
                    BassHardwardRequest request = new BassHardwardRequest();
                    request.setServiceType("hub");
                    request.setCmdCode(cmdCode);
                    request.setSubType("syncData");
                    List<Map<String, Object>> sendList = Lists.newArrayList();
                    sendList.add(val);
                    request.setEntitys(list);
                    String hubAddress = val.get("hubAddress").toString();
                    try {
                        obj = HttpUtils.commonSendHttpPost(url, JSONObject.toJSONString(request));
                    } catch (Exception e) {
                        sendList.clear();
                        throw new Exception("集中器网关异常: " + hubAddress);
                    }
                    Map<String, Object> respMap = JSONObject.parseObject((String)obj, Map.class);
                    if (Objects.nonNull(respMap.get(PFConstDef.RESP_RETURN_CODE))
                            && respMap.get(PFConstDef.RESP_RETURN_CODE).toString().equals(ResultConst.SUCCESS)) {
                    }
                    else {
                        throw new Exception("集中器网关异常: " + hubAddress);
                    }

                }
            }

            return configList;
        }
    }
}