package com.smart.factory;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.smart.common.ResponseUtils;
import com.smart.entity.ColdMeterBalanceEntity;
import com.smart.entity.ElecBalanceReadEntity;
import com.smart.entity.HotMeterBalanceEntity;
import com.smart.service.BaseServiceImpl;
import com.smart.utils.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @program: smart-service
 * @description: meter
 * @author: Admin
 * @create: 2019-04-22 19:25
 **/

@Component("MeterHotReadBalance")
public class MeterHotReadBalance extends BaseServiceImpl<HotMeterBalanceEntity> implements IHardware {
    private static final Logger log = LoggerFactory.getLogger(MeterHotReadBalance.class);

    //电压：readMeterInfo
    //余额：readMeterBalance
    /*list: [{
        "pluginId": "LOCKER-PLUGIN-001",
                "hubAddress": "0002471002",
                "subAddress": "188",
                "setTime": "2019-07-17",
    }]*/
    //subType: COLD-BALANCE 总充值量
    @Override
    public List process(String hdType, List list, String cmdCode, String cmdType, int autoPush) throws Exception {
        if (cmdCode.equals("updateMeterBalance")) {

            for (int i = 0; i < list.size(); i++) {
                Map<String, Object> map = (Map<String, Object>) list.get(i);
                Preconditions.checkArgument(Objects.nonNull(map.get("cardNo")),
                        "cardNo is invalid");
                Preconditions.checkArgument(Objects.nonNull(map.get("userNo")),
                        "userNo is invalid");
                Preconditions.checkArgument(Objects.nonNull(map.get("newHubAddress")),
                        "new hubAddress is invalid");
                Preconditions.checkArgument(Objects.nonNull(map.get("newMeterAddress")),
                        "new meterAddress is invalid");
                Preconditions.checkArgument(Objects.nonNull(map.get("newPluginId")),
                        "new pluginId is invalid");

                Optional<HotMeterBalanceEntity> hotMeterBalanceEntity = baseRepostory.findOne ((root, query, builder)-> {
                    Predicate p1 = builder.equal(root.get("cardNo"), map.get("cardNo").toString());
                    Predicate p2 = builder.equal(root.get("userUid"), map.get("userNo").toString());
                    return builder.and(p1, p2);
                });

                if (hotMeterBalanceEntity.isPresent()) {
                    Object obj = map.get("balance");
                    if (Objects.nonNull(obj)) {
                        hotMeterBalanceEntity.get().setBalance(obj.toString());
                    }
                    obj = map.get("leftAmount");
                    if (Objects.nonNull(obj)) {
                        hotMeterBalanceEntity.get().setLeftAmount(obj.toString());
                    }

                    hotMeterBalanceEntity.get().setHubAddress(map.get("newHubAddress").toString());
                    hotMeterBalanceEntity.get().setMeterAddress(map.get("newMeterAddress").toString());
                    hotMeterBalanceEntity.get().setPluginId(map.get("newPluginId").toString());
                    baseRepostory.save(hotMeterBalanceEntity.get());
                }
                else {
                    throw new Exception("该记录在balance表中不存在");
                }
            }

            //return ResponseUtils.getSuccessResponse("");
            return null;
        }
        else {
            Preconditions.checkArgument(cmdCode.equals("readMeterBalance"),
                    "热水表抄表(总充值量)cmdType应该为: readMeterBalance");
            Preconditions.checkArgument((Objects.nonNull(list) && list.size() > 0),
                    "list is invalid");

            List<HotMeterBalanceEntity> lists = Lists.newArrayList();
            for (int i = 0; i < list.size(); i++) {
                Map<String, Object> map = (Map<String, Object>) list.get(i);
                Preconditions.checkArgument(Objects.nonNull(map.get("pluginId")),
                        "pluginId is invalid");
                Preconditions.checkArgument(Objects.nonNull(map.get("subAddress")),
                        "subAddress is invalid");
                Preconditions.checkArgument(Objects.nonNull(map.get("hubAddress")),
                        "hubAddress is invalid");
                Preconditions.checkArgument(Objects.nonNull(map.get("userNo")),
                        "userNo is invalid");
/*            Preconditions.checkArgument(Objects.nonNull(map.get("setTime")),
                    "setTime is invalid");*/

                String hubAddress = map.get("hubAddress").toString();
                String subAddress = map.get("subAddress").toString();
                String userNo = map.get("userNo").toString();
/*            String setTime = map.get("setTime").toString();
            int year = Integer.valueOf(setTime.split("-")[0]);
            int month = Integer.valueOf(setTime.split("-")[1]);
            int day = Integer.valueOf(setTime.split("-")[2]);
            LocalDateTime localDateTime1 = LocalDateTime.of(year, month ,day, 0, 0, 0);
            LocalDateTime localDateTime2 = LocalDateTime.of(year, month ,day, 23, 59, 59);*/
                List<HotMeterBalanceEntity> opt = baseRepostory.findAll((root, query, builder) -> {
                    Predicate p1 = builder.equal(root.get("hubAddress"), hubAddress);
                    Predicate p2 = builder.equal(root.get("meterAddress"), subAddress);
                    Predicate p3 = builder.equal(root.get("userUid"), userNo);
                    return builder.and(p1, p2, p3);
                });
                lists.addAll(opt);
            }

            return lists;
        }
    }
}