package com.smart.factory;

/**
 * @program: smart-service
 * @description: builder
 * @author: Admin
 * @create: 2019-04-22 19:32
 **/
public interface Builder {
    void add(String hdType, IHardware supplier);
}