package com.smart.factory;

import com.smart.entity.MeterRechargeEntity;
import com.smart.entity.MonitorEntity;
import com.smart.service.BaseServiceImpl;
import org.springframework.stereotype.Component;

/**
 * @program: smart-service
 * @description: meter
 * @author: Admin
 * @create: 2019-04-22 19:25
 **/

@Component("MeterRecharge")
public class MeterRecharge extends BaseServiceImpl<MeterRechargeEntity> implements IHardware {
}