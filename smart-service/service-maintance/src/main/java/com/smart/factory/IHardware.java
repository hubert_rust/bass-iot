package com.smart.factory;

import java.util.List;

public interface IHardware {
    List process(String hdType,
                   List entitys,
                   String cmdCode,
                   String cmdType,
                   int autoPush) throws Exception;
}
