package com.smart.factory;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.smart.entity.ElecMeterReadEntity;
import com.smart.entity.PluginEntity;
import com.smart.service.BaseServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Predicate;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @program: smart-service
 * @description: meter
 * @author: Admin
 * @create: 2019-04-22 19:25
 **/

@Component("MeterElecRead")
public class MeterElecRead extends BaseServiceImpl<ElecMeterReadEntity> implements IHardware {
    private static final Logger log = LoggerFactory.getLogger(MeterElecRead.class);

    //电压：readMeterInfo
    //余额：readMeterBalance
    /*list: [{
        "pluginId": "LOCKER-PLUGIN-001",
                "hubAddress": "0002471002",
                "subAddress": "188",
                "setTime": "2019-07-17",
    }]*/
    //subType: ELEC-READ
    @Override
    public List process(String hdType, List list, String cmdCode, String cmdType, int autoPush) throws Exception {
        Preconditions.checkArgument(cmdCode.equals("readMeterInfo"),
                "电表抄表cmdType应该为: readMeterInfo");
        Preconditions.checkArgument((Objects.nonNull(list) && list.size()>0),
                "list is invalid");


        List<ElecMeterReadEntity> lists = Lists.newArrayList();
        for (int i=0; i<list.size(); i++) {
            Map<String, Object> map = (Map<String, Object>) list.get(i);
            Preconditions.checkArgument(Objects.nonNull(map.get("pluginId")),
                    "pluginId is invalid");
            Preconditions.checkArgument(Objects.nonNull(map.get("subAddress")),
                    "subAddress is invalid");
            Preconditions.checkArgument(Objects.nonNull(map.get("hubAddress")),
                    "hubAddress is invalid");
            Preconditions.checkArgument(Objects.nonNull(map.get("setTime")),
                    "setTime is invalid");


            List<ElecMeterReadEntity> opt = findElecRead(map.get("pluginId").toString(),
                    map.get("hubAddress").toString(),
                    map.get("subAddress").toString(),
                    map.get("setTime").toString());

            lists.addAll(opt);
        }

        return lists;
    }
}