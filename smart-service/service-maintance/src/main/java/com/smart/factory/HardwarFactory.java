package com.smart.factory;

import com.google.common.collect.Maps;

import java.util.Map;
import java.util.function.Consumer;

public interface HardwarFactory {
    IHardware create(String hdType);
    public static HardwarFactory factory(Consumer<Builder> consumer) {
        Map<String, IHardware> map = Maps.newHashMap();
        consumer.accept(map::put);
        return hdType -> map.get(hdType);
    }
}
