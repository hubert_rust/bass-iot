package com.smart.utils;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * @program: service
 * @description: restful req
 * @author: Admin
 * @create: 2019-04-19 19:17
 **/

@Component
public class RestfulUtils<T> {

    @Autowired
    private RestTemplate restTemplate;

    public <T> Object postType(String url,
                                T requestEntity,
                                String token,
                                String uid)  throws Exception {

        //Class<T> clz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

        String request = new Gson().toJson(requestEntity);
        return  this.post(url, request, token, uid);
    }

    public Object post(String url,
                        String requestBody,
                        String token,
                        String uid) throws Exception {
        //restTemplate.pos
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("text/plain; charset=UTF-8");
        headers.setContentType(type);
        headers.add("uid", "uid2018");
        HttpEntity<String> formEntity = new HttpEntity<String>(requestBody, headers);

        String result = restTemplate.postForObject(url, formEntity, String.class);
        return new Gson().toJson(result);
    }

    public Object get(String url) throws Exception {
        return restTemplate.getForObject(url, String.class);
    }
}