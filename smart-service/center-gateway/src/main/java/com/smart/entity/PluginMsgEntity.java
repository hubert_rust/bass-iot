package com.smart.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Map;


/*"tenant_id": "",  //学校id
        "plugin_id": "",     //插件id
        "command_order":"",  //命令单号   在上层应用平台生成，类似支付模块的订单号，传递到插件层
        "command_type":"" //PLATFORM, PLUGIN
        "command_code":"",   //命令码
        "con_address":"",
        "meter_address":"",
        "meter_type":"",
        "meter_no":"",
        "datas": [{},{}];*/

/**
 * @program: smart-service
 * @description: message
 * @author: Admin
 * @create: 2019-04-28 16:19
 **/
public class PluginMsgEntity implements Serializable {
    private String dcFlag;
    private String pluginId;
    private String commandOrder;
    private String commandType;        //PLATFORM, PLUGIN,
    private String commandCode;        //ADD, DEL, MOD
    private String conAddress;
    private String meterAddress;
    private String meterType;
    private String meterNo;
    private String pluginReqNo;
    private List<Map<String, Object>> datas;

    public String getDcFlag() {
        return dcFlag;
    }

    public void setDcFlag(String dcFlag) {
        this.dcFlag = dcFlag;
    }

    public String getPluginId() {
        return pluginId;
    }

    public void setPluginId(String pluginId) {
        this.pluginId = pluginId;
    }

    public String getCommandOrder() {
        return commandOrder;
    }

    public void setCommandOrder(String commandOrder) {
        this.commandOrder = commandOrder;
    }

    public String getCommandType() {
        return commandType;
    }

    public void setCommandType(String commandType) {
        this.commandType = commandType;
    }

    public String getCommandCode() {
        return commandCode;
    }

    public void setCommandCode(String commandCode) {
        this.commandCode = commandCode;
    }

    public String getConAddress() {
        return conAddress;
    }

    public void setConAddress(String conAddress) {
        this.conAddress = conAddress;
    }

    public String getMeterAddress() {
        return meterAddress;
    }

    public void setMeterAddress(String meterAddress) {
        this.meterAddress = meterAddress;
    }

    public String getMeterType() {
        return meterType;
    }

    public void setMeterType(String meterType) {
        this.meterType = meterType;
    }

    public String getMeterNo() {
        return meterNo;
    }

    public void setMeterNo(String meterNo) {
        this.meterNo = meterNo;
    }

    public String getPluginReqNo() {
        return pluginReqNo;
    }

    public void setPluginReqNo(String pluginReqNo) {
        this.pluginReqNo = pluginReqNo;
    }

    public List<Map<String, Object>> getDatas() {
        return datas;
    }

    public void setDatas(List<Map<String, Object>> datas) {
        this.datas = datas;
    }
}