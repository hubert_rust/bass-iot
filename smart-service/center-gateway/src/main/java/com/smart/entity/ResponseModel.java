package com.smart.entity;

import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @program: smart-service
 * @description: request
 * @author: Admin
 * @create: 2019-04-23 09:18
 **/

@EqualsAndHashCode(callSuper = false)
public class ResponseModel<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    private String returnCode;
    private String returnMsg;
    private List<T> datas;
    private List<Map<String, Object>> bassList;
    private List<Map<String, Object>> serviceList;
    private List<Map<String, Object>> pluginList;

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public List<T> getDatas() {
        return datas;
    }

    public void setDatas(List<T> datas) {
        this.datas = datas;
    }

    public List<Map<String, Object>> getBassList() { return bassList; }
    public void setBassList(List<Map<String, Object>> bassList) { this.bassList = bassList; }

    public List<Map<String, Object>> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<Map<String, Object>> serviceList) {
        this.serviceList = serviceList;
    }

    public List<Map<String, Object>> getPluginList() {
        return pluginList;
    }

    public void setPluginList(List<Map<String, Object>> pluginList) {
        this.pluginList = pluginList;
    }
}