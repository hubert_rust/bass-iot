package com.smart.http;

/**
 * @program: smart-service
 * @description: httpapi
 * @author: Admin
 * @create: 2019-04-28 15:32
 **/
public interface HttpApi {
    Object post(String url, Object request) throws Exception;
    Object get(String url) throws Exception;
}