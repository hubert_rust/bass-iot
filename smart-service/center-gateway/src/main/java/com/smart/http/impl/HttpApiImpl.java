package com.smart.http.impl;

import com.smart.http.HttpApi;
import com.smart.framework.utils.HttpUtils;
import org.springframework.stereotype.Component;

/**
 * @program: smart-service
 * @description: d
 * @author: Admin
 * @create: 2019-04-28 15:33
 **/
@Component
public class HttpApiImpl implements HttpApi {
    @Override
    public Object post(String url, Object request) throws Exception {
        HttpUtils.commonSendHttpPost(url, (String)request);
        return null;
    }

    @Override
    public Object get(String url) throws Exception {
        return null;
    }
}