/*
package com.smart.config;

import com.bx.service.base.IBassService;
import com.google.common.base.Strings;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Maps;
import com.google.common.collect.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

*/
/**
 * @program: service
 * @description: bass manager
 * @author: Admin
 * @create: 2019-04-19 10:40
 **//*

@Component
public class BassManager {

    @Autowired
    DBConfigInfo dbConfigInfo;

    private static final Logger log = LoggerFactory.getLogger(BassManager.class);

    private static AtomicBoolean initFinish = new AtomicBoolean(false);
    //private static Map<String, Object> bassInfo = Maps.newHashMap();

    public static Table<String, String, Object> bassInfo = null;
    public static Map<String, IBassService> bassServiceMap;

    static {
       bassServiceMap = Maps.newHashMap();
       bassInfo = HashBasedTable.create();
    }


    public void init(String bassName) throws Exception {
        dbConfigInfo.load(bassInfo);
        bassServiceLoad.load(bassName, bassServiceMap);

        initFinish.set(true);
    }

    public void register(String bassName) throws Exception {

        if (Strings.isNullOrEmpty(bassName)) { throw new Exception("参数非法"); }
        if (!initFinish.get()) {
            init(bassName);
        }
        if (Objects.isNull(bassServiceMap.get(bassName))) { throw  new Exception("没有该Bass类"); }
        //业务层启动后注册, 注册后登录
        bassServiceMap.get(bassName).login("", "");
    }

    public  Object invoke(String bassName,
                          String fun,
                          String request,
                          String token,
                          String uid)  throws Exception {
        Map<String, Object> map = (Map<String, Object>)bassInfo.get(bassName, fun);
        if (Objects.isNull(map) || map.size() <=0) {
            throw new Exception("tb_bass_service_manage没有配置该Bass");
        }

        String url = map.get("bass_url").toString();
        IBassService bassService = bassServiceMap.get(bassName);
        return bassService.invoke(url, request, token, uid);
    }

}*/
