package com.smart.config;


import ch.qos.logback.core.util.COWArrayList;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.smart.command.CommandCode;
import com.smart.common.ResultConst;
import com.smart.entity.ResponseModel;
import org.nustaq.kson.JSonSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.xml.transform.Result;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
public class DBConfigInfo implements ApplicationRunner {
    private static final Logger log = LoggerFactory.getLogger(DBConfigInfo.class);
    public static CopyOnWriteArrayList<Map<String, Object>> bassList = new CopyOnWriteArrayList<>();
    public static CopyOnWriteArrayList<Map<String, Object>> bassPluginList = new CopyOnWriteArrayList<>();
    public static CopyOnWriteArrayList<Map<String, Object>> bassServiceList = new CopyOnWriteArrayList<>();

    private final static String BASS_MANAGE_SELECT = "select * from tb_bass_manage";
    private final static String BASS_HARDWARE_PLUGIN_SELECT = "select * from tb_hardware_plugin_manage";
    private final static String BASS_HARDWARE_SERVICE_SELECT = "select * from tb_hardware_service_manage";

    private final static Object lock = new Object();

    /*@Value("${dcFlag}")
    public String dcFlag;

    @Value("${dcName}")
    public String dcName;*/
    @Autowired
    public JdbcTemplate jdbcTemplate;

    public  void loadBass() {
        bassList.clear();
        loadTable(bassList, BASS_MANAGE_SELECT);
    }

    public  void loadPlugins() {
        bassPluginList.clear();
        loadTable(bassPluginList, BASS_HARDWARE_PLUGIN_SELECT);
    }
    private  void loadHardwareServices() {
        bassServiceList.clear();
        loadTable(bassServiceList, BASS_HARDWARE_SERVICE_SELECT);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
       loadBass();
       loadPlugins();
       loadHardwareServices();

        TimeUnit.SECONDS.sleep(5);
        new Thread() {
            @Override
            public void run() {
                try {
                    TimeUnit.SECONDS.sleep(60);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                refresh();
            }
        }.start();
    }

    private void loadTable(List list, String tbName) {

        List<Map<String, Object>> listMap = jdbcTemplate.queryForList(tbName);
        listMap.forEach(v-> {
            Map<String, Object> map = Maps.newHashMap();
            map.putAll(v);
            list.add(map);
        });

        log.info(list.toString());
    }

    public void refresh() {
        log.info(">>> DBConfig, refresh...");
        synchronized (lock) {
            loadBass();
            loadPlugins();
            loadHardwareServices();
            lock.notifyAll();
        }
        System.out.println("finish");
    }

    public Object get(String bassName) {
        ResponseModel<Map<String, Object>> responseModel = new ResponseModel<>();
        synchronized (lock) {
            List<Map<String, Object>> ret = DBConfigInfo.bassList.stream().filter(v-> {
                if (v.get("bass_name") == null) { return false; }
                else {

                    return v.get("bass_name").toString().equals(bassName);
                }
            }).collect(Collectors.toList());


            if (Objects.isNull(ret) || ret.size() <=0) {
                responseModel.setReturnCode("FAIL");
                responseModel.setReturnMsg("no available bass service");
            }
            else {
                responseModel.setBassList(ret);
                responseModel.setServiceList(DBConfigInfo.bassServiceList);
                responseModel.setPluginList(DBConfigInfo.bassPluginList);
                responseModel.setReturnCode(ResultConst.SUCCESS);
                responseModel.setReturnMsg(ResultConst.STATE_OK);
            }
            lock.notifyAll();
        }
        return JSONObject.toJSONString(responseModel);
    }
}