package com.smart.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;

/**
 * @program: 维护监测消息
 * @description:
 * @author: admin
 * @create: 2019-05-16 15:34
 **/
@EnableBinding(IMaintanceSink.class)
public class MaintanceSink {
    private static final Logger log = LoggerFactory.getLogger(MaintanceSink.class);

    @Autowired
    private DBConfigInfo dbConfigInfo;

    @StreamListener(IMaintanceSink.INPUT_MAINTANCE)
    public void process(Message<?> message) {
        log.info(">>> gateWay maintanceSink: " +message.getPayload());
        dbConfigInfo.refresh();

        Acknowledgment acknowledgment = message.getHeaders().get(KafkaHeaders.ACKNOWLEDGMENT, Acknowledgment.class);
        if (acknowledgment != null) {
            System.out.println("Acknowledgement provided");
            acknowledgment.acknowledge();
        }
    }
}
