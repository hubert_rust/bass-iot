package com.smart.controller;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.smart.config.DBConfigInfo;
import com.smart.entity.ResponseModel;
import com.smart.framework.cons.BassConst;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-04-29 11:37
 **/
@RestController
@RequestMapping("service")
public class BassSearchServiceController {
    private static final Logger log = LoggerFactory.getLogger(BassSearchServiceController.class);

    @Autowired
    DBConfigInfo dbConfigInfo;

    @GetMapping("plugins")
    public Object plugins() throws Exception {
        log.info(">>> BassSearchServiceController, plugins");
        ResponseModel<Map<String, Object>> responseModel = new ResponseModel<>();

        List<Map<String, Object>> ret = DBConfigInfo.bassList.stream().filter(v-> {
            if (v.get("bass_name") == null) { return false; }
            else {
                return v.get("bass_name").toString().equals(BassConst.BASS_NAME_HARDWARE);
            }
        }).collect(Collectors.toList());

        //
        if (DBConfigInfo.bassPluginList.size() <=0) {
            responseModel.setReturnCode("FAIL");
            responseModel.setReturnMsg("no available bass plugin");
            return new Gson().toJson(responseModel);
        }
        else {
            responseModel.setReturnCode("SUCCESS");
            responseModel.setReturnMsg("OK");
            responseModel.setDatas(DBConfigInfo.bassPluginList);
            return new Gson().toJson(responseModel);

        }


    }

    @GetMapping("refresh")
    public Object refresh() throws Exception {
        dbConfigInfo.refresh();
        ResponseModel<Map<String, Object>> responseModel = new ResponseModel<>();
        responseModel.setReturnCode("SUCCESS");
        responseModel.setReturnMsg("refresh ok");
        return new Gson().toJson(responseModel);

    }

    /** 
    * @Description: 加载全部 tb_bass_manage,tb_hardware_service_manage和plugin
    * @Param:
    * @return:  
    * @Author: admin 
    * @Date: 2019/7/9 
    */
    @GetMapping("list/{bass}")
    public Object list(@PathVariable("bass") String bassName,
                       @RequestParam(name="pluginId", required = false) String pluginId) throws Exception {
        log.info(">>> BassSearchServiceController, list, bass: {}", bassName);
        ResponseModel<Map<String, Object>> responseModel = new ResponseModel<>();
        if (Strings.isBlank(bassName)) {
            responseModel.setReturnCode("FAIL");
            responseModel.setReturnMsg("bassName invalid");
            return new Gson().toJson(responseModel);
        }

        return dbConfigInfo.get(bassName);

        /*List<Map<String, Object>> ret = DBConfigInfo.bassList.stream().filter(v-> {
            if (v.get("bass_name") == null) { return false; }
            else {

                return v.get("bass_name").toString().equals(bassName);
            }
        }).collect(Collectors.toList());


        if (Objects.isNull(ret) || ret.size() <=0) {
            responseModel.setReturnCode("FAIL");
            responseModel.setReturnMsg("no available bass service");
            return new Gson().toJson(responseModel);
        }
        else {

            responseModel.setReturnCode("SUCCESS");
            responseModel.setReturnMsg("OK");
            responseModel.setDatas(ret);
            return new Gson().toJson(responseModel);

        }*/
    }
}
