package com.smart.controller;

import com.google.gson.Gson;
import com.smart.config.DBConfigInfo;
import com.smart.framework.cons.BassConst;
import com.smart.framework.utils.HttpUtils;
import com.smart.framework.utils.ResponseUtils;
import com.smart.toolkit.IDGenerator;
import org.apache.curator.shaded.com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @program: 硬件平台统一入口，对plugin层需要特殊处理
 * @description: plugin
 * @author: Admin
 * @create: 2019-04-28 14:59
 **/

@Scope("prototype")
@RestController
@RequestMapping("api")
public class APIController {
    private static final Logger log = LoggerFactory.getLogger(APIController.class);

    @Autowired
    DBConfigInfo dbConfigInfo;
    /*"tenantId": "",  //学校id
      "pluginId": "",     //插件id
      "commandOrder":"",  //命令单号   在上层应用平台生成，类似支付模块的订单号，传递到插件层
      "commandType":"" //PLATFORM, PLUGIN
      "commandCode":"",   //命令码
      "conAddress":"",
      "meterAddress":"",
      "meterType":"",
      "meterNo":"",
      "datas": [{},{}]*/
    public String checkValid(Map<String, Object> msgEntity) {
        if (Objects.isNull(msgEntity.get("serviceType"))){
            return "serviceType is invalid";
        }

        if (Objects.isNull(msgEntity.get("dcName"))){
            return "dcName is invalid";
        }
        return null;
    }

    private Object routeProcess(Map map, String serviceType) throws Exception {
        String dcName = map.get("dcName").toString();
        Optional<Map<String, Object>> opt = DBConfigInfo.bassServiceList
                .stream().filter(v -> {
                    return (v.get("service_name").toString().toUpperCase().equals(serviceType)
                            && v.get("dc_name").toString().equals(dcName));
                }).findAny();
        String dcFlag = opt.get().get("dc_flag").toString();
        String url = opt.get().get("service_url").toString();
        String seq = IDGenerator.getIdStr();// SEQGenerator.generatorSeq(dcFlag, "0");
        map.put("commandOrder",seq);
        return HttpUtils.commonSendHttpPost(url, (String) new Gson().toJson(map));
    }

    @Deprecated
    private Object routePlugin(Map map, String serviceType) throws Exception {
        String dcName = map.get("dcName").toString();
        Object serviceId = map.get("serviceId");
        if (serviceId == null || Strings.isNullOrEmpty(serviceType.toString())) {
            return ResponseUtils.getFailResponse("serviceId is NullorEmpty");
        }
        Optional<Map<String, Object>> opt = DBConfigInfo.bassPluginList
                .stream().filter(v -> {
                    return (v.get("plugin_id").toString().toUpperCase().equals(serviceId.toString())
                            && v.get("dc_name").toString().equals(dcName));
                }).findFirst();
        String url = opt.get().get("plugin_url").toString();
        String dcFlag = opt.get().get("dc_flag").toString();
        String seq = IDGenerator.getIdStr(); //SEQGenerator.generatorSeq(dcFlag, "0");
        map.put("commandOrder",seq);
        return HttpUtils.commonSendHttpPost(url, (String) new Gson().toJson(map));
    }

    @PostMapping("hardware")
    public Object hardward(@RequestBody String request) throws Exception {
        Map<String, Object> msgEntity = new Gson().fromJson(request, Map.class);

        String ret = checkValid(msgEntity);
        if (ret != null) { return ResponseUtils.getFailResponse(ret); }
        String serviceType = msgEntity.get("serviceType").toString();
        Object obj = null;

        //fun根据不同业务（elec，hot，cold，lock等分开，在业务层json中serviceType区分和表
        //tb_hardware_service_manage中serviceName对应，分开部署，每个业务可以部署多个
        try {
            if (BassConst.BASS_HARDWARD_SERVICE_NAME_PLUGIN.equals(serviceType)) {
                obj = routeProcess(msgEntity, BassConst.BASS_HARDWARD_SERVICE_NAME_FUNC);
            }
            else {
                obj = routeProcess(msgEntity, serviceType);
            }

        } catch (Exception e) {
            e.printStackTrace();
            log.error(">>> APIController, hardward. exception: {}", e.getMessage());
            obj = ResponseUtils.getFailResponse(e.getMessage());
        }

        return obj;
    }
}