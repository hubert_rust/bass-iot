<!DOCTYPE html>
<#--<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<h1>
    Hello web: ${name}
</h1>
</body>
</html>-->


<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Hello, world!</title>
</head>
<body>
<h3>service</h3>
<table class="table table-striped">
    <thead>
    <tr>
        <th>ID</th>
        <th>Service Name</th>
        <th>IP</th>
        <th>Port</th>
        <th>Reg Time</th>
        <th>Status</th>
        <th>Remark</th>
    </tr>
    </thead>
    <tbody>
    <#list serviceList as list>
        <tr>
            <td>${list.id}</td>
            <td>${list.serviceName}</td>
            <td>${list.ip}</td>
            <td>${list.port}</td>
            <td>${list.regTime}</td>
            <td>ok</td>
            <td></td>
        </tr>
    </#list>

    </tbody>
</table>

<h3>plugin</h3>
<table class="table table-striped">
    <thead>
    <tr>
        <th>ID</th>
        <th>service Name</th>
        <th>status</th>
        <th>remark</th>
    </tr>
    </thead>
    <tbody>
    <#list serviceList as list>
        <tr>
            <td>${list_index}</td>
            <td>${list}</td>
            <td>OK</td>
            <td></td>
        </tr>
    </#list>

    </tbody>
</table>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.bootcss.com/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.bootcss.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>