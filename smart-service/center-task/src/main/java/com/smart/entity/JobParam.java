package com.smart.entity;

import java.io.Serializable;
import java.util.List;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-06-28 11:15
 **/
public class JobParam implements Serializable {
    private static final long serialVersionUID = 1L;

    int daysData;     //
    String runMode;   //sync, async,
    int runNums;
    List<RunParam> datas;

    public int getDaysData() { return daysData; }
    public void setDaysData(int daysData) { this.daysData = daysData; }
    public String getRunMode() { return runMode; }
    public void setRunMode(String runMode) { this.runMode = runMode; }
    public List<RunParam> getDatas() { return datas; }
    public void setDatas(List<RunParam> datas) { this.datas = datas; }

    public int getRunNums() { return runNums; }
    public void setRunNums(int runNums) { this.runNums = runNums; }

    public static class RunParam {
        public String hubAddr;
        public String subAddr;
        public int timeSpan; //15: 15分钟, 60: 60分钟
        public String secretKey;

        public String getHubAddr() { return hubAddr; }
        public void setHubAddr(String hubAddr) { this.hubAddr = hubAddr; }
        public String getSubAddr() { return subAddr; }
        public void setSubAddr(String subAddr) { this.subAddr = subAddr; }
        public String getSecretKey() { return secretKey; }
        public void setSecretKey(String secretKey) { this.secretKey = secretKey; }
        public int getTimeSpan() { return timeSpan; }
        public void setTimeSpan(int timeSpan) { this.timeSpan = timeSpan; }
    }
}
