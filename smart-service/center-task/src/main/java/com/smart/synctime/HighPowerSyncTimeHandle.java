package com.smart.synctime;

import com.smart.command.CommandCode;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @program: 给每个表同步时间
 * @description:
 * @author: admin
 * @create: 2019-07-11 08:44
 **/
@JobHandler(value = "HighPowerSyncTimeHandle")
@Component
public class HighPowerSyncTimeHandle extends IJobHandler {
    private static final Logger log = LoggerFactory.getLogger(HighPowerSyncTimeHandle.class);

    @Autowired
    SyncTimeHelper syncTimeHelper;

    @Override
    public ReturnT<String> execute(String param) throws Exception {
        syncTimeHelper.execute(CommandCode.HUB_TYPE_MIX,
                CommandCode.HUB_TYPE_HIGHMETER,
                param, 2);
        return ReturnT.SUCCESS;
    }
}
