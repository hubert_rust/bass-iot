package com.smart.synctime;

import com.smart.framework.commonpool.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-06-20 15:41
 **/
public class SemaphoreJobWorker extends Thread{
    private static final Logger log = LoggerFactory.getLogger(SemaphoreJobWorker.class);
    private Task task;
    private CyclicBarrier cyclicBarrier;
    private Semaphore semaphore;
    private int count; //申请信号量的大小,1

    public SemaphoreJobWorker(Task task,
                              int count,
                              CyclicBarrier cyclicBarrier) {
        this.task = task;
        //this.semaphore = semaphore;
        this.count = count;
        this.cyclicBarrier = cyclicBarrier;
    }

    @Override
    public void run() {
        log.info(">>> SemaphoreJobWorker, run...");
        try {
            //semaphore.acquire(count);
            task.process();
            cyclicBarrier.await();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }

        /*if (!Objects.isNull(semaphore)) {
            semaphore.release(count);
            log.info(">>> SemaphoreJobWorker, finish and release");
        }*/

    }
}
