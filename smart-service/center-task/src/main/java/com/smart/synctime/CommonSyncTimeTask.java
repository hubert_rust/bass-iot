package com.smart.synctime;

import com.alibaba.fastjson.JSONObject;
import com.smart.command.CommandCode;
import com.smart.common.PFConstDef;
import com.smart.common.ResponseUtils;
import com.smart.common.ResultConst;
import com.smart.entity.MeterEntiry;
import com.smart.framework.commonpool.Task;
import com.smart.framework.utils.HttpUtils;
import com.smart.msgtpl.MessageTpl;
import com.smart.repository.CronJobLogRepository;
import com.smart.repository.MeterRepostory;
import com.smart.request.BassHardwardRequest;
import com.smart.task.TaskHelper;
import com.smart.toolkit.IDGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-11 08:47
 **/

@Component("commonSyncTimeTask")
@Scope("prototype")
public class CommonSyncTimeTask extends Task {
    private static final Logger log = LoggerFactory.getLogger(CommonSyncTimeTask.class);

    @Autowired
    TaskHelper helper;
    @Autowired
    private CronJobLogRepository logRepository;

    @Autowired
    MeterRepostory meterRepostory;

    private String mainOrder;
    private String hubAddress;
    private String jobHandleName;
    private String hubType;
    private String meterType;
    private String pluginId;

    public String getMainOrder() { return mainOrder; }
    public void setMainOrder(String mainOrder) { this.mainOrder = mainOrder; }
    public String getHubAddress() { return hubAddress; }
    public void setHubAddress(String hubAddress) { this.hubAddress = hubAddress; }
    public String getJobHandleName() { return jobHandleName; }
    public void setJobHandleName(String jobHandleName) { this.jobHandleName = jobHandleName; }

    public String getHubType() { return hubType; }
    public void setHubType(String hubType) { this.hubType = hubType; }

    public String getMeterType() { return meterType; }
    public void setMeterType(String meterType) { this.meterType = meterType; }

    public String getPluginId() { return pluginId; }
    public void setPluginId(String pluginId) { this.pluginId = pluginId; }

    @Override
    public void process() throws Exception {
        Map map = helper.getPluginUrlMap(this.hubType);

        List<MeterEntiry> list = meterRepostory.findAll((root, query, builder)-> {
            Predicate p1 = builder.equal(root.get("hubAddress"), this.hubAddress);
            Predicate p2 = builder.equal(root.get("meterType"), this.meterType);
            return builder.and(p1, p2);
        });

        if (list.size()<=0) {
            helper.cronJobLogSave(
                    this.pluginId,
                    this.jobHandleName,
                    this.mainOrder,
                    IDGenerator.getIdStr(),
                    LocalDateTime.now(),
                    this.hubAddress,
                    "",
                    this.hubType,
                    ResultConst.SUCCESS,
                    "subAddress数量为0");
            return;
        }
        for (MeterEntiry val: list) {
            TimeUnit.MILLISECONDS.sleep(50);
            if (!val.getMeterType().equals(this.meterType)) {
                continue;
            }

            BassHardwardRequest request = MessageTpl.getSyncTimeTpl(CommandCode.CMD_TIME_SYNC,
                    val.getMeterType(),
                    val.getPluginId(),
                    IDGenerator.getIdStr(),
                    this.hubAddress,
                    val.getMeterAddress(),
                    String.valueOf(val.getBps()),
                    String.valueOf(val.getCommPort()));

            LocalDateTime startTime = LocalDateTime.now();

            long startTm = System.currentTimeMillis();
            String resp = null;
            try {
                String url = map.get(val.getPluginId()).toString();
                resp = (String) HttpUtils.commonSendHttpPost(url, JSONObject.toJSONString(request), PFConstDef.HTTP_MILL_TIMEOUT);

            } catch (Exception e) {
                e.printStackTrace();
                resp = ResponseUtils.getFailResponse(e.getMessage());
            }

            Map<String, Object> respMap = JSONObject.parseObject(resp, Map.class);
            ResultConst.ReturnObject retObj = ResultConst.checkResult(respMap);
            helper.cronJobLogSave(val.getPluginId(),
                    this.jobHandleName,
                    this.mainOrder,
                    IDGenerator.getIdStr(),
                    startTime,
                    this.hubAddress,
                    val.getMeterAddress(),
                    this.hubType,
                    retObj.getCode(),
                    retObj.getMessage());
        }
    }
}
