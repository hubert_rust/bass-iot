package com.smart.synctime;

import com.smart.common.ResultConst;
import com.smart.entity.ConcentratorEntity;
import com.smart.entity.JobParam;
import com.smart.framework.utils.CommonApplication;
import com.smart.task.TaskHelper;
import com.smart.toolkit.IDGenerator;
import com.xxl.job.core.biz.model.ReturnT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @program: 给每个表同步时间
 * @description:
 * @author: admin
 * @create: 2019-07-11 08:44
 **/
@Component
public class SyncTimeHelper {
    private static final Logger log = LoggerFactory.getLogger(SyncTimeHelper.class);
    public static ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(5,
            10,
            10,
            TimeUnit.SECONDS,
            new ArrayBlockingQueue<>(200));
    @Autowired
    TaskHelper taskHelper;

    public ReturnT<String> execute(String hubType,
                                   String meteryType,
                                   String param,
                                   int runNum) throws Exception {
        String mainOrder = IDGenerator.getIdStr();
        String jobHandleName = "syncTime-";
        LocalDateTime localDateTime = LocalDateTime.now();
        List<ConcentratorEntity> list = taskHelper.getHubConfig(null, hubType);
        if (list.size() <=0 ) {
            taskHelper.cronMainJobLogSave(jobHandleName,
                    mainOrder,
                    localDateTime,
                    ResultConst.SUCCESS,
                    "集中器数量为0");
            return ReturnT.SUCCESS;
        }

        JobParam jobParam = null;
        try {
            jobParam = taskHelper.parseJobParam(param);
        } catch (Exception e) {
            e.printStackTrace();
            return ReturnT.FAIL;
        }

        //控制每次执行线程数量2
        int num = (jobParam == null || jobParam.getRunNums()<=0) ?
                (runNum ) :
                jobParam.getRunNums();



        //同时执行线程数
        int count = (jobParam == null || jobParam.getRunNums() <=0) ? 1 : jobParam.getRunNums();
        CyclicBarrier cyclicBarrier = new CyclicBarrier(list.size() + 1);
        for(ConcentratorEntity v: list) {
            CommonSyncTimeTask task = CommonApplication.getBean("commonSyncTimeTask");
            task.setMainOrder(mainOrder);
            task.setHubAddress(v.getHubAddress());
            task.setHubType(v.getHubType());
            task.setMeterType(meteryType);
            task.setPluginId(v.getPluginId());
            jobHandleName = "syncTime-" + meteryType;
            task.setJobHandleName(jobHandleName);
            SemaphoreJobWorker worker = new SemaphoreJobWorker(task,
                    1,
                    cyclicBarrier);
            poolExecutor.submit(worker);
            //worker.start();
        }

        //善后
        cyclicBarrier.await();
        taskHelper.cronMainJobLogSave(jobHandleName,
                mainOrder,
                localDateTime,
                "", ""
                );

        return ReturnT.SUCCESS;
    }
}
