package com.smart.base;

import com.smart.framework.commonpool.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-06-20 15:41
 **/
public class DefaultWorker extends Thread{
    private static final Logger log = LoggerFactory.getLogger(DefaultWorker.class);
    private Task task;
    private String typeStr;

    private CyclicBarrier cyclicBarrier;

    public DefaultWorker(Task task, CyclicBarrier cyclicBarrier, String typeStr) {
        this.task = task;
        this.cyclicBarrier = cyclicBarrier;
    }

    @Override
    public void run() {
        log.info(">>> DefaultWorker, run, type: {}", typeStr);
        try {
            //semaphore.acquire(count);
            task.process();
            cyclicBarrier.await();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }

    }
}
