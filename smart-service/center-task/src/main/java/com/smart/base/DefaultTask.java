package com.smart.base;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.smart.command.CommandCode;
import com.smart.common.PFConstDef;
import com.smart.common.ResponseUtils;
import com.smart.common.ResultConst;
import com.smart.entity.MeterEntiry;
import com.smart.framework.commonpool.Task;
import com.smart.framework.utils.HttpUtils;
import com.smart.msgtpl.MessageTpl;
import com.smart.repository.BaseRepostory;
import com.smart.repository.CronJobLogRepository;
import com.smart.request.BassHardwardRequest;
import com.smart.task.TaskHelper;
import com.smart.toolkit.IDGenerator;
import com.smart.utils.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-11 08:47
 **/

@Component("DefaultTask")
@Scope("prototype")
public class DefaultTask<T extends BaseEntity<Long>> extends Task {
    private static final Logger log = LoggerFactory.getLogger(DefaultTask.class);

    @Autowired
    TaskHelper helper;

    @Autowired
    BaseRepostory<T> baseRepostory;


    private String dcName;
    private String dcId;
    private String mainOrder;
    private String hubAddress;
    private String jobHandleName;
    private String hubType;
    private String pluginId;

    public String getDcName() { return dcName; }
    public void setDcName(String dcName) { this.dcName = dcName; }
    public String getDcId() { return dcId; }
    public void setDcId(String dcId) { this.dcId = dcId; }

    public String getMainOrder() { return mainOrder; }
    public void setMainOrder(String mainOrder) { this.mainOrder = mainOrder; }
    public String getHubAddress() { return hubAddress; }
    public void setHubAddress(String hubAddress) { this.hubAddress = hubAddress; }
    public String getJobHandleName() { return jobHandleName; }
    public void setJobHandleName(String jobHandleName) { this.jobHandleName = jobHandleName; }

    public String getHubType() { return hubType; }
    public void setHubType(String hubType) { this.hubType = hubType; }

    public String getPluginId() { return pluginId; }
    public void setPluginId(String pluginId) { this.pluginId = pluginId; }


    @Override
    public void process() throws Exception {
        Map map = helper.getPluginUrlMap(this.hubType);

        List<MeterEntiry> list = helper.getSubList(this.hubType);
        if (list.size()<=0) {
            return;
        }

        for (MeterEntiry val: list) {
            TimeUnit.MILLISECONDS.sleep(10);
            BassHardwardRequest request = MessageTpl.getMeterReadTpl(CommandCode.HUB_TYPE_ELECMETER,
                    "",
                    val.getPluginId(),
                    IDGenerator.getIdStr(),
                    this.hubAddress,
                    val.getMeterAddress());

            LocalDateTime startTime = LocalDateTime.now();

            long startTm = System.currentTimeMillis();
            String resp = null;
            try {
                String url = map.get(val.getPluginId()).toString();
                resp = (String) HttpUtils.commonSendHttpPost(url, JSONObject.toJSONString(request), PFConstDef.HTTP_MILL_TIMEOUT);

            } catch (Exception e) {
                e.printStackTrace();
                resp = ResponseUtils.getFailResponse(e.getMessage());
            }

            Map<String, Object> respMap = JSONObject.parseObject(resp, Map.class);
            ResultConst.ReturnObject retObj = ResultConst.checkResult(respMap);

            if (retObj.getCode().equals(ResultConst.SUCCESS)) {
                saveReadResult(respMap,
                        startTime,
                        request.getCommandOrder(),
                        retObj.getCode());
            }
            else {
                Class<T> cls = (Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
                T entity = cls.newInstance();
                Method mth = cls.getMethod("setHubAddress", String.class);
                mth.invoke(entity, request.getHubAddress());
                mth = cls.getMethod("setMeterAddress", String.class);
                mth.invoke(entity, request.getSubAddress());
                setCommonData(cls,
                        entity,
                        startTime,
                        request.getCommandOrder(),
                        retObj.getCode());
                baseRepostory.save(entity);
            }

        }
    }

    private void setCommonData(Class<?> cls,
                               T ret,
                               LocalDateTime startTime,
                               String commandOrder,
                               String result) {

        try {
            Method mth = cls.getMethod("setStartTime", LocalDateTime.class);
            mth.invoke(ret, startTime);
            mth = cls.getMethod("setFinishTime", LocalDateTime.class);
            mth.invoke(ret, LocalDateTime.now());
            mth = cls.getMethod("setMainOrder", String.class);
            mth.invoke(ret, mainOrder);

            mth = cls.getMethod("setCommandOrder", String.class);
            mth.invoke(ret, commandOrder);

            mth = cls.getMethod("setReadType", String.class);
            mth.invoke(ret, PFConstDef.READ_TYPE_AUTO);

            mth = cls.getMethod("setReadStatus", String.class);
            mth.invoke(ret, result);

            ret.setDcId(this.getDcId());
            ret.setDcName(this.getDcName());
            ret.setCreateTime(LocalDateTime.now());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
    public void saveReadResult(Map<String, Object> respMap,
                               LocalDateTime startTime,
                               String commandOrder,
                               String result) {

        try {
            List<Map<String, Object>> list = (List<Map<String, Object>>)respMap.get("datas");
            if (list.size() >0) {

                Class<T> cls = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
                T ret = CommonUtils.mapToEntity(list.get(0), cls);
                setCommonData(cls,
                        ret,
                        startTime,
                        commandOrder,
                        result);
                baseRepostory.save(ret);
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}
