package com.smart.base;

import com.smart.common.ResultConst;
import com.smart.entity.ConcentratorEntity;
import com.smart.entity.JobParam;
import com.smart.framework.utils.CommonApplication;
import com.smart.synctime.SemaphoreJobWorker;
import com.smart.task.TaskHelper;
import com.smart.toolkit.IDGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.ParameterizedType;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @program: smart-service
 * @description: base
 * @author: Admin
 * @create: 2019-04-22 18:19
 **/
public class DefaultServiceImpl<S extends DefaultTask> {
    private static final Logger log = LoggerFactory.getLogger(DefaultServiceImpl.class);

    @Autowired
    TaskHelper helper;

    public String execute(String jobHandleName,
                          String typeStr,
                          String param,
                          int asyncNum) throws Exception {
        String mainOrder = IDGenerator.getIdStr();
        LocalDateTime localDateTime = LocalDateTime.now();
        List<ConcentratorEntity> list = helper.getHubConfig(null, typeStr);
        if (list.size() <=0 ) {
            return ResultConst.SUCCESS;
        }

        JobParam jobParam = null;
        try {
            jobParam = helper.parseJobParam(param);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultConst.FAIL;
        }

        Map map = helper.getPluginUrlMap(typeStr);
        //控制每次执行线程数量2
        int num = (jobParam == null || jobParam.getRunNums()<=0) ?
                (asyncNum) :
                jobParam.getRunNums();

        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(1,
                num,
                10,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(list.size()+1));

        //同时执行线程数
        int count = (jobParam == null || jobParam.getRunNums() <=0) ? 1 : jobParam.getRunNums();
        CyclicBarrier cyclicBarrier = new CyclicBarrier(list.size() + 1);
        for(ConcentratorEntity v: list) {
            S task = null;
            try {
                Class<S> cls = (Class<S>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
                task = CommonApplication.getBean(cls.getSimpleName());
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }

            task.setDcName(v.getDcName());
            task.setDcId(v.getDcId());
            task.setMainOrder(mainOrder);
            task.setHubAddress(v.getHubAddress());
            task.setHubType(v.getHubType());
            task.setPluginId(v.getPluginId());
            task.setJobHandleName(jobHandleName);
            DefaultWorker worker = new DefaultWorker(task,
                    cyclicBarrier,
                    v.getHubType());
            poolExecutor.submit(worker);
        }

        //善后
        cyclicBarrier.await();
        helper.cronMainJobLogSave(jobHandleName,
                mainOrder,
                localDateTime,
                "", ""
        );

        poolExecutor.shutdown();

        return ResultConst.SUCCESS;
    }

}