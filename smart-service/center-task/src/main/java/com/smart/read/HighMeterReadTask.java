package com.smart.read;

import com.smart.ann.TaskService;
import com.smart.base.DefaultTask;
import com.smart.command.CommandCode;
import com.smart.common.ResultConst;
import com.smart.container.CommonReadTask;
import com.smart.container.CommonTask;
import com.smart.container.ITask;
import com.smart.container.ReadType;
import com.smart.entity.HighBalanceReadEntity;
import com.smart.entity.HighMeterReadEntity;
import com.smart.entity.MeterEntiry;
import com.smart.framework.utils.ResponseUtils;
import com.smart.recharge.RechargeConst;
import com.smart.repository.HighMeterBalanceRepostory;
import com.smart.request.BassHardwardRequest;
import com.smart.service.BaseServiceImpl;
import com.smart.utils.CommonUtils;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Predicate;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;

/**
 * @program: high read meter, one task
 * @description:
 * @author: admin
 * @create: 2019-07-11 08:44
 **/
@Component("HighMeterReadTask")
@Scope("prototype")
@TaskService(meterType = "high-meter", cmdCode = CommandCode.HIGH_POWER_READ_METER, readType = ReadType.READ_TYPE_MIX)
public class HighMeterReadTask extends CommonReadTask<HighMeterReadEntity> implements ITask {
    private static final Logger log = LoggerFactory.getLogger(HighMeterReadTask.class);
/**
* @Description: 抄表和balance在同一个请求完成
* @Param:
* @return:
* @Author: admin
* @Date: 2019/7/29
*/
    @Autowired
    HighMeterBalanceRepostory repostory;
    @Override
    public void processFinish(Map<String, Object> respMap,
                              MeterEntiry meterEntiry,
                              BassHardwardRequest request,
                              String mainOrder,
                              LocalDateTime startTime) throws Exception {
        ResultConst.ReturnObject retObj = ResultConst.checkResult(respMap);
        String leftAmount = ResultConst.getDatasByKey(respMap, "leftAmount");
        leftAmount = new BigDecimal(leftAmount).toString();
        String hubAddress = ResultConst.getDatasByKey(respMap, "hubAddress");
        String meterAddress = ResultConst.getDatasByKey(respMap, "meterAddress");

        int retryNum = 0;
        while (retryNum <=5) {
            Optional<HighBalanceReadEntity> entity = repostory.findOne((root, query, builder) -> {
                Predicate p1 = builder.equal(root.get("hubAddress"), hubAddress);
                Predicate p2 = builder.equal(root.get("meterAddress"), meterAddress);
                return builder.and(p1, p2);
            });
            if (entity.isPresent()) {
                int ret = repostory.updateHighMeterBalance(entity.get().getId(),
                        leftAmount,
                        entity.get().getVersion(),
                        startTime,
                        LocalDateTime.now(),
                        retObj.getCode());
                if (ret == 1) {
                    break;
                } else {
                    log.error(">>> HighMeterReadTask, processFinish. meter:{} update leftAmout: {} fail.",
                            meterAddress, leftAmount);
                }
            } else {
                HighBalanceReadEntity balanceReadEntity = new HighBalanceReadEntity();
                balanceReadEntity.setMeterAddress(meterAddress);
                balanceReadEntity.setHubAddress(hubAddress);
                balanceReadEntity.setLeftAmount(leftAmount);
                balanceReadEntity.setCreateTime(LocalDateTime.now());
                balanceReadEntity.setDcId(meterEntiry.getDcId());
                balanceReadEntity.setDcName(meterEntiry.getDcName());
                balanceReadEntity.setPluginId(meterEntiry.getPluginId());
                balanceReadEntity.setMainOrder(mainOrder);
                balanceReadEntity.setCommandOrder(request.getCommandOrder());
                balanceReadEntity.setStartTime(startTime);
                balanceReadEntity.setFinishTime(LocalDateTime.now());
                balanceReadEntity.setReadStatus(retObj.getCode());
                balanceReadEntity.setUnit(RechargeConst.RECHARGE_METER_UNIT_DEGREE);
                repostory.save(balanceReadEntity);
                break;
            }
            retryNum++;
        }
    }
}
