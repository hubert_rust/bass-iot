package com.smart.read;

import com.smart.ann.TaskService;
import com.smart.base.DefaultTask;
import com.smart.command.CommandCode;
import com.smart.common.ResultConst;
import com.smart.container.CommonReadTask;
import com.smart.container.CommonTask;
import com.smart.container.ITask;
import com.smart.container.ReadType;
import com.smart.entity.ElecBalanceReadEntity;
import com.smart.entity.ElecMeterReadEntity;
import com.smart.entity.HighBalanceReadEntity;
import com.smart.entity.MeterEntiry;
import com.smart.repository.ElecMeterBalanceRepostory;
import com.smart.request.BassHardwardRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;

/**
 * @program: 大功率电表抄表
 * @description:
 * @author: admin
 * @create: 2019-07-11 08:44
 **/
@Component("ElecMeterBalanceTask")
@Scope("prototype")
@TaskService(meterType = "elec-meter", cmdCode = CommandCode.CMD_QUERY_LEFT_MONEY, readType = ReadType.READ_TYPE_BALANCE)
public class ElecMeterBalanceTask extends CommonReadTask<ElecBalanceReadEntity> implements ITask {
    private static final Logger log = LoggerFactory.getLogger(ElecMeterBalanceTask.class);
    @Autowired
    ElecMeterBalanceRepostory repostory;

    @Override
    public void processFinish(Map<String, Object> respMap,
                              MeterEntiry meterEntiry,
                              BassHardwardRequest request,
                              String mainOrder,
                              LocalDateTime startTime) throws Exception {

        ResultConst.ReturnObject retObj = ResultConst.checkResult(respMap);
        String leftAmount = ResultConst.getDatasByKey(respMap, "leftMoney");
        String hubAddress = ResultConst.getDatasByKey(respMap, "hubAddress");
        String meterAddress = ResultConst.getDatasByKey(respMap, "meterAddress");

        int retryNum = 0;
        while (retryNum <=5) {
            Optional<ElecBalanceReadEntity> entity = repostory.findOne((root, query, builder) -> {
                Predicate p1 = builder.equal(root.get("hubAddress"), hubAddress);
                Predicate p2 = builder.equal(root.get("meterAddress"), meterAddress);
                return builder.and(p1, p2);
            });
            if (entity.isPresent()) {
                int ret = repostory.updateElecMeterBalance(entity.get().getId(),
                        leftAmount,
                        entity.get().getVersion(),
                        startTime,
                        LocalDateTime.now(),
                        retObj.getCode());
                if (ret == 1) {
                    break;
                } else {
                    log.error(">>> ElecMeterReadTask, processFinish. meter:{} update leftAmout: {} fail.",
                            meterAddress, leftAmount);
                }
            } else {
                ElecBalanceReadEntity balanceReadEntity = new ElecBalanceReadEntity();
                balanceReadEntity.setMeterAddress(meterAddress);
                balanceReadEntity.setHubAddress(hubAddress);
                balanceReadEntity.setLeftMoney(leftAmount);
                balanceReadEntity.setCreateTime(LocalDateTime.now());
                balanceReadEntity.setDcId(meterEntiry.getDcId());
                balanceReadEntity.setDcName(meterEntiry.getDcName());
                balanceReadEntity.setPluginId(meterEntiry.getPluginId());
                balanceReadEntity.setMainOrder(mainOrder);
                balanceReadEntity.setCommandOrder(request.getCommandOrder());
                balanceReadEntity.setStartTime(LocalDateTime.now());
                balanceReadEntity.setFinishTime(LocalDateTime.now());
                balanceReadEntity.setReadType("auto");
                balanceReadEntity.setReadStatus(retObj.getCode());

                repostory.save(balanceReadEntity);
                break;
            }
            retryNum++;
        }
    }
}
