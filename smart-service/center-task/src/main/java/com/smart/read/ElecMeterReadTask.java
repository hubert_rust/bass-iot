package com.smart.read;

import com.smart.ann.TaskService;
import com.smart.base.DefaultTask;
import com.smart.command.CommandCode;
import com.smart.container.CommonReadTask;
import com.smart.container.CommonTask;
import com.smart.container.ITask;
import com.smart.container.ReadType;
import com.smart.entity.ElecMeterReadEntity;
import com.smart.entity.HighMeterReadEntity;
import com.smart.entity.MeterEntiry;
import com.smart.request.BassHardwardRequest;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * @program: 大功率电表抄表
 * @description:
 * @author: admin
 * @create: 2019-07-11 08:44
 **/
@Component("ElecMeterReadTask")
@Scope("prototype")
@TaskService(meterType = "elec-meter", cmdCode = CommandCode.CMD_QUERY_METER_STATE, readType = ReadType.READ_TYPE_READ)
public class ElecMeterReadTask extends CommonReadTask<ElecMeterReadEntity> implements ITask {

    @Override
    public void processFinish(Map<String, Object> respMap, MeterEntiry meterEntiry, BassHardwardRequest request, String mainOrder, LocalDateTime startTime) throws Exception {

    }
}
