package com.smart.read;

import com.smart.ann.TaskService;
import com.smart.command.CommandCode;
import com.smart.common.ResultConst;
import com.smart.container.CommonReadTask;
import com.smart.container.ITask;
import com.smart.container.ReadType;
import com.smart.entity.HighBalanceReadEntity;
import com.smart.entity.HighMeterReadEntity;
import com.smart.entity.HotMeterReadEntity;
import com.smart.entity.MeterEntiry;
import com.smart.recharge.RechargeConst;
import com.smart.repository.HighMeterBalanceRepostory;
import com.smart.repository.HotMeterReadRepostory;
import com.smart.request.BassHardwardRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Predicate;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;

/**
 * @program: high read meter, one task
 * @description:
 * @author: admin
 * @create: 2019-07-11 08:44
 **/
@Component("HotMeterReadTask")
@Scope("prototype")
@TaskService(meterType = "hot-meter", cmdCode = CommandCode.CMD_QUERY_METER_AMOUNT, readType = ReadType.READ_TYPE_READ)
public class HotMeterReadTask extends CommonReadTask<HotMeterReadEntity> implements ITask {
    private static final Logger log = LoggerFactory.getLogger(HotMeterReadTask.class);
/**
* @Description:
* @Param:
* @return:
* @Author: admin
* @Date: 2019/7/29
*/
    @Autowired
HotMeterReadRepostory repostory;

    @Override
    public void processFinish(Map<String, Object> respMap,
                              MeterEntiry meterEntiry,
                              BassHardwardRequest request,
                              String mainOrder,
                              LocalDateTime startTime) throws Exception {
        /*ResultConst.ReturnObject retObj = ResultConst.checkResult(respMap);
        String leftAmount = ResultConst.getDatasByKey(respMap, "leftAmount");
        leftAmount = new BigDecimal(leftAmount).toString();
        String hubAddress = ResultConst.getDatasByKey(respMap, "hubAddress");
        String meterAddress = ResultConst.getDatasByKey(respMap, "meterAddress");

        int retryNum = 0;
        while (retryNum <=5) {
            Optional<HotMeterReadEntity> entity = repostory.findOne((root, query, builder) -> {
                Predicate p1 = builder.equal(root.get("hubAddress"), hubAddress);
                Predicate p2 = builder.equal(root.get("meterAddress"), meterAddress);
                return builder.and(p1, p2);
            });
            if (entity.isPresent()) {
                int ret = repostory.updateHighMeterBalance(entity.get().getId(),
                        leftAmount,
                        entity.get().getVersion(),
                        startTime,
                        LocalDateTime.now(),
                        retObj.getCode());
                if (ret == 1) {
                    break;
                } else {
                    log.error(">>> HighMeterReadTask, processFinish. meter:{} update leftAmout: {} fail.",
                            meterAddress, leftAmount);
                }
            } else {
                HotMeterReadEntity balanceReadEntity = new HotMeterReadEntity();
                balanceReadEntity.setMeterAddress(meterAddress);
                balanceReadEntity.setHubAddress(hubAddress);
                balanceReadEntity.setCreateTime(LocalDateTime.now());
                balanceReadEntity.setDcId(meterEntiry.getDcId());
                balanceReadEntity.setDcName(meterEntiry.getDcName());
                balanceReadEntity.setMainOrder(mainOrder);
                balanceReadEntity.setCommandOrder(request.getCommandOrder());
                balanceReadEntity.setStartTime(startTime);
                balanceReadEntity.setFinishTime(LocalDateTime.now());
                balanceReadEntity.setReadStatus(retObj.getCode());
                balanceReadEntity.setUnit(RechargeConst.RECHARGE_METER_UNIT_DEGREE);
                repostory.save(balanceReadEntity);
                break;
            }
            retryNum++;
        }*/
    }
}
