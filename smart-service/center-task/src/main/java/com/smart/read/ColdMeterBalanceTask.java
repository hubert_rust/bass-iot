package com.smart.read;

import com.google.common.base.Strings;
import com.smart.ann.TaskService;
import com.smart.command.CommandCode;
import com.smart.common.ResultConst;
import com.smart.container.CommonReadTask;
import com.smart.container.CommonTask;
import com.smart.container.ITask;
import com.smart.container.ReadType;
import com.smart.entity.ColdMeterBalanceEntity;
import com.smart.entity.ColdMeterReadEntity;
import com.smart.entity.ElecBalanceReadEntity;
import com.smart.entity.MeterEntiry;
import com.smart.recharge.RechargeConst;
import com.smart.recharge.RechargeType;
import com.smart.repository.ColdMeterBalanceRepostory;
import com.smart.repository.ColdMeterReadRepostory;
import com.smart.repository.ElecMeterBalanceRepostory;
import com.smart.request.BassHardwardRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Predicate;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;

/**
 * @program: cold meter read, total use amount
 * @description:
 * @author: admin
 * @create: 2019-07-11 08:44
 **/
@Component("ColdMeterBalanceTask")
@Scope("prototype")
@TaskService(meterType = "cold-meter", cmdCode = CommandCode.CMD_QUERY_METER_RECHARGE_ALL, readType = ReadType.READ_TYPE_BALANCE)
public class ColdMeterBalanceTask extends CommonReadTask<ColdMeterBalanceEntity> implements ITask {
    private static final Logger log = LoggerFactory.getLogger(ColdMeterBalanceTask.class);

    @Autowired
    ColdMeterBalanceRepostory repostory;
    @Autowired
    ColdMeterReadRepostory readRepostory;

    @Override
    public void processFinish(Map<String, Object> respMap,
                              MeterEntiry meterEntiry,
                              BassHardwardRequest request,
                              String mainOrder,
                              LocalDateTime startTime) throws Exception {

        ResultConst.ReturnObject retObj = ResultConst.checkResult(respMap);
        String totalRecharge = ResultConst.getDatasByKey(respMap, "useTotalRecharge");
        String hubAddress = ResultConst.getDatasByKey(respMap, "hubAddress");
        String meterAddress = ResultConst.getDatasByKey(respMap, "meterAddress");

        int retryNum = 0;
        while (retryNum <=5) {
            //查询抄表记录
            Optional<ColdMeterReadEntity> entity = readRepostory.findOne((root, query, builder) -> {
                Predicate p1 = builder.equal(root.get("hubAddress"), hubAddress);
                Predicate p2 = builder.equal(root.get("meterAddress"), meterAddress);
                Predicate p3 = builder.equal(root.get("mainOrder"), mainOrder);
                return builder.and(p1, p2, p3);
            });
            Optional<ColdMeterBalanceEntity> balanceEntity = repostory.findOne((root, query, builder)-> {
                Predicate p1 = builder.equal(root.get("hubAddress"), hubAddress);
                Predicate p2 = builder.equal(root.get("meterAddress"), meterAddress);
                return builder.and(p1, p2);
            });


            String leftAmount = "";
            if (balanceEntity.isPresent()) {
                leftAmount = balanceEntity.get().getLeftAmount();
                if (entity.isPresent() && !Strings.isNullOrEmpty(entity.get().getUseTotalAmount())) {
                    BigDecimal bigDecimal = new BigDecimal(totalRecharge);
                    BigDecimal totalUse = new BigDecimal(entity.get().getUseTotalAmount());
                    leftAmount = bigDecimal.subtract(totalUse).toString();
                    //balanceEntity.get().setLeftAmount(leftAmount);
                }


                int ret = repostory.updateColdMeterBalance(balanceEntity.get().getId(),
                        leftAmount,
                        totalRecharge,
                        balanceEntity.get().getVersion(),
                        startTime,
                        LocalDateTime.now(),
                        retObj.getCode());
                if (ret == 1) {
                    break;
                } else {
                    log.error(">>> ColdMeterReadTask, processFinish. meter:{} update leftAmout: {} fail.",
                            meterAddress, leftAmount);
                }
            } else {

                ColdMeterBalanceEntity balanceReadEntity = new ColdMeterBalanceEntity();
                balanceReadEntity.setMeterAddress(meterAddress);
                balanceReadEntity.setHubAddress(hubAddress);
                //balanceReadEntity.setLeftMoney(leftAmount);
                if (entity.isPresent() && !Strings.isNullOrEmpty(entity.get().getUseTotalAmount())) {
                    BigDecimal bigDecimal = new BigDecimal(totalRecharge);
                    BigDecimal totalUse = new BigDecimal(entity.get().getUseTotalAmount());
                    leftAmount = bigDecimal.subtract(totalUse).toString();
                    balanceReadEntity.setLeftAmount(leftAmount);
                }

                balanceReadEntity.setUseTotalRecharge(totalRecharge);

                balanceReadEntity.setCreateTime(LocalDateTime.now());
                balanceReadEntity.setDcId(meterEntiry.getDcId());
                balanceReadEntity.setDcName(meterEntiry.getDcName());
                balanceReadEntity.setPluginId(meterEntiry.getPluginId());
                balanceReadEntity.setMainOrder(mainOrder);
                balanceReadEntity.setCommandOrder(request.getCommandOrder());
                balanceReadEntity.setStartTime(startTime);
                balanceReadEntity.setFinishTime(LocalDateTime.now());
                balanceReadEntity.setReadStatus(retObj.getCode());
                balanceReadEntity.setReadType("auto");
                balanceReadEntity.setUnit(RechargeConst.RECHARGE_METER_UNIT_CUMETER);
                repostory.save(balanceReadEntity);
                break;
            }
            retryNum++;
        }
    }
}

