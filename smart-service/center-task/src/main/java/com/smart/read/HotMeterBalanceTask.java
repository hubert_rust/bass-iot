package com.smart.read;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.smart.ann.TaskService;
import com.smart.command.CommandCode;
import com.smart.common.PFConstDef;
import com.smart.common.ResponseUtils;
import com.smart.common.ResultConst;
import com.smart.container.CommonReadTask;
import com.smart.container.ITask;
import com.smart.container.ReadType;
import com.smart.entity.HotMeterBalanceEntity;
import com.smart.entity.HotMeterReadEntity;
import com.smart.entity.HotMeterUseRecordEntity;
import com.smart.entity.MeterEntiry;
import com.smart.framework.commonpool.Task;
import com.smart.framework.utils.HttpUtils;
import com.smart.msgtpl.MessageTpl;
import com.smart.recharge.RechargeConst;
import com.smart.repository.HotMeterBalanceRepostory;
import com.smart.repository.HotMeterReadRepostory;
import com.smart.repository.HotMeterUseRecordRepostory;
import com.smart.request.BassHardwardRequest;
import com.smart.toolkit.IDGenerator;
import com.smart.utils.CommonUtils;
import net.bytebuddy.asm.Advice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @program: high read meter, one task
 * @description:
 * @author: admin
 * @create: 2019-07-11 08:44
 **/
@Component("HotMeterBalanceTask")
@Scope("prototype")
@TaskService(meterType = "hot-meter", cmdCode = CommandCode.CMD_USE_RECORD_NUM, readType = ReadType.READ_TYPE_READ)
public class HotMeterBalanceTask extends CommonReadTask<HotMeterBalanceEntity> implements ITask {
    private static final Logger log = LoggerFactory.getLogger(HotMeterBalanceTask.class);
/**
* @Description:
* @Param:
* @return:
* @Author: admin
* @Date: 2019/7/29
*/
    @Autowired
    HotMeterBalanceRepostory balanceRepostory;
    @Autowired
    HotMeterUseRecordRepostory recordRepostory;

    @Override
    public void process() throws Exception {
        Map map = helper.getPluginUrlMap(CommandCode.HUB_TYPE_MIX);
        MeterEntiry val = (MeterEntiry) getMeterHelper();
        String cmdCode = getClass().getAnnotation(TaskService.class).cmdCode();

        BassHardwardRequest request = MessageTpl.getMeterReadTpl(
                val.getMeterType(),
                cmdCode,
                val.getPluginId(),
                IDGenerator.getIdStr(),
                val.getHubAddress(),
                val.getMeterAddress());


        LocalDateTime startTime = LocalDateTime.now();
        long startTm = System.currentTimeMillis();
        String resp = null;
        String url = map.get(val.getPluginId()).toString();
        try {
            resp = (String) HttpUtils.commonSendHttpPost(url, JSONObject.toJSONString(request), PFConstDef.HTTP_MILL_TIMEOUT);

        } catch (Exception e) {
            e.printStackTrace();
            resp = ResponseUtils.getFailResponse(e.getMessage());
            log.error(">>> CommonTask, send post err: {}", request.toString());
        }

        Map<String, Object> retUserMap = JSONObject.parseObject((String)resp, Map.class);
        ResultConst.ReturnObject ret  = ResultConst.checkResult(retUserMap);
        Preconditions.checkArgument(ret.getCode().equals(ResultConst.SUCCESS),
                "查询热水表使用记录失败.");

        String unReadNum = ResultConst.getDatasByKey(retUserMap, "unreadNum");
        //String unReadNum = "23";
        if (!Strings.isNullOrEmpty(unReadNum) && Integer.valueOf(unReadNum) >0) {
            int start = Integer.valueOf(ResultConst.getDatasByKey(retUserMap, "unreadStartIndex"));

            while (true) {
                LocalDateTime recordStart = LocalDateTime.now();
                CommonUtils.setBassRequestDatas(request, "startIndex", String.valueOf(start));
                request.setCommandOrder(IDGenerator.getIdStr());

                try {
                    request.setCmdCode(CommandCode.CMD_USE_BATCH_RECORD);
                    resp = (String) HttpUtils.commonSendHttpPost(url, JSONObject.toJSONString(request), PFConstDef.HTTP_MILL_TIMEOUT);
                } catch (Exception e) {
                    e.printStackTrace();
                    break;
                }
                LocalDateTime finishTime = LocalDateTime.now();

                Map<String, Object> retMap = JSONObject.parseObject((String)resp, Map.class);
                ResultConst.ReturnObject retResult  = ResultConst.checkResult(retUserMap);
                if (!retResult.getCode().equals(ResultConst.SUCCESS)) {
                    break;
                }
                List<Map<String, Object>> datasList = (List)retMap.get(ResultConst.DATAS);
                Map<String, Object> mapList = datasList.get(0);
                List<Map<String, Object>> recordList = (List)mapList.get("use_record_list");
                try {
                    saveBalance(recordStart,
                            finishTime,
                            this.getMainOrder(),
                            request.getCommandOrder(),
                            recordList,
                            val);
                } catch (Exception e) {
                    e.printStackTrace();
                    break;
                }

                start += 4;
            }

        }
        else {

        }

    }
    private void saveBalance(LocalDateTime startTime,
                             LocalDateTime finishTime,
                             String mainOrder,
                             String commandOrder,
                             List<Map<String, Object>> recordList,
                             MeterEntiry meterEntiry
                             ) throws Exception {
        for (Map<String, Object> val: recordList) {
            try {
                String userNo = val.get("userNo").toString();
                String cardNo = val.get("cardNo").toString();
                String leftAmount = val.get("leftAmount").toString();
                String balance = val.get("balance").toString();

                //user record
                HotMeterUseRecordEntity  recordEntity = new HotMeterUseRecordEntity();
                recordEntity.setDcId(meterEntiry.getDcId());
                recordEntity.setDcName(meterEntiry.getDcName());
                recordEntity.setPluginId(meterEntiry.getPluginId());
                recordEntity.setMeterAddress(meterEntiry.getMeterAddress());
                recordEntity.setHubAddress(meterEntiry.getHubAddress());
                recordEntity.setLeftAmount(val.get("leftAmount").toString());
                recordEntity.setUseAmount(val.get("useAmount").toString());
                recordEntity.setUseMoney(val.get("useMoney").toString());
                recordEntity.setBalance(val.get("balance").toString());
                recordEntity.setCardNo(val.get("cardNo").toString());
                recordEntity.setUseIndex(val.get("recordIndex").toString());
                recordEntity.setUseStartTime(val.get("startUseTime").toString());
                recordEntity.setUseEndTime(val.get("endUseTime").toString());
                recordEntity.setUseType(val.get("useType").toString());
                recordEntity.setUserUid(val.get("userNo").toString());
                recordEntity.setUnit(RechargeConst.RECHARGE_METER_UNIT_L);
                recordEntity.setMainOrder(mainOrder);
                recordEntity.setCommandOrder(commandOrder);
                recordEntity.setStartTime(startTime);
                recordEntity.setFinishTime(finishTime);
                recordEntity.setCreateTime(LocalDateTime.now());
                recordRepostory.save(recordEntity);


                //user balance
                Optional<HotMeterBalanceEntity> entityOptional = balanceRepostory.findOne((root, query, builder)->{
                    return builder.equal(root.get("cardNo"), cardNo);
                });
                if (entityOptional.isPresent()) {

                    //更新balance
                    int index = 0;
                    while (true) {
                        int ret = balanceRepostory.updateHotMeterBalance(entityOptional.get().getId(),
                                balance,
                                leftAmount,
                                entityOptional.get().getVersion(),
                                LocalDateTime.now());
                        if (ret == 1 || index >=5) {
                            break;
                        }
                        entityOptional = balanceRepostory.findOne((root, query, builder)->{
                            return builder.equal(root.get("cardNo"), cardNo);
                        });
                        index++;
                    }
                }
                else {
                    HotMeterBalanceEntity balanceEntity = new HotMeterBalanceEntity();
                    balanceEntity.setStartTime(startTime);
                    balanceEntity.setHubAddress(meterEntiry.getHubAddress());
                    balanceEntity.setMeterAddress(meterEntiry.getMeterAddress());
                    balanceEntity.setPluginId(meterEntiry.getPluginId());
                    balanceEntity.setMainOrder(this.getMainOrder());
                    balanceEntity.setDcId(meterEntiry.getDcId());
                    balanceEntity.setDcName(meterEntiry.getDcName());
                    balanceEntity.setPrice(meterEntiry.getPrice());
                    balanceEntity.setCardNo(cardNo);
                    balanceEntity.setUserUid(userNo);
                    balanceEntity.setFinishTime(finishTime);
                    balanceEntity.setCreateTime(LocalDateTime.now());
                    balanceRepostory.save(balanceEntity);
                }


            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
        }

    }

    @Override
    public void processFinish(Map<String, Object> respMap,
                              MeterEntiry meterEntiry,
                              BassHardwardRequest request,
                              String mainOrder,
                              LocalDateTime startTime) throws Exception {
    }
}
