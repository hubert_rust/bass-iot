package com.smart.read;

import com.smart.ann.TaskService;
import com.smart.base.DefaultTask;
import com.smart.command.CommandCode;
import com.smart.container.CommonReadTask;
import com.smart.container.CommonTask;
import com.smart.container.ITask;
import com.smart.container.ReadType;
import com.smart.entity.ColdMeterReadEntity;
import com.smart.entity.HighMeterReadEntity;
import com.smart.entity.MeterEntiry;
import com.smart.request.BassHardwardRequest;
import groovyjarjarpicocli.CommandLine;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * @program: cold meter read, total use amount
 * @description:
 * @author: admin
 * @create: 2019-07-11 08:44
 **/
@Component("ColdMeterReadTask")
@Scope("prototype")
@TaskService(meterType = "cold-meter", cmdCode = CommandCode.CMD_QUERY_METER_AMOUNT, readType = ReadType.READ_TYPE_READ)
public class ColdMeterReadTask extends CommonReadTask<ColdMeterReadEntity> implements ITask {
    @Override
    public void processFinish(Map<String, Object> respMap, MeterEntiry meterEntiry, BassHardwardRequest request, String mainOrder, LocalDateTime startTime) throws Exception {

    }
}
