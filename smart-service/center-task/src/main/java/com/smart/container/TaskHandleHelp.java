package com.smart.container;

import com.google.common.collect.Lists;
import com.smart.ann.TaskService;
import com.smart.command.CommandCode;
import com.smart.config.InitConfig;
import com.smart.entity.ConcentratorEntity;
import com.smart.entity.MeterEntiry;
import com.smart.framework.commonpool.Task;
import com.smart.framework.utils.CommonApplication;
import com.smart.repository.MeterRepostory;
import com.smart.task.TaskHelper;
import com.smart.toolkit.IDGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

/**
 * @program: 从集中器列表中取出meter, 生成task, 提交到队列
 * @description:
 * @author: admin
 * @create: 2019-07-22 11:39
 **/
@Component
@Scope("prototype")
public class TaskHandleHelp extends Task {
    private static final Logger log = LoggerFactory.getLogger(TaskHandleHelp.class);
    private List<ConcentratorEntity> entiryList;

    @Autowired
    MeterRepostory meterRepostory;
    @Autowired
    TaskHelper taskHelper;

    Class<?> taskCls;

    public List<ConcentratorEntity> getEntiryList() {
        return entiryList;
    }

    public void init(List<ConcentratorEntity> entiryList,
                     Class<?> taskCls) {
        this.entiryList = entiryList;
    }

    //getTask
    private List<Task> getTask(MeterEntiry meterEntiry, String mainOrder) {
        List<Class<?>> clsList = InitConfig.METER_TASK.get(meterEntiry.getMeterType());
        if (Objects.isNull(clsList)) {
            log.error(">>> TaskHandleHelp, getTask, meterType: {} no cls", meterEntiry.toString());
            return null;
        }
        //
        if (meterEntiry.getMeterType().equals(CommandCode.HUB_TYPE_COLDMETER)) {
            if (clsList.size() == 2) {
                Class<?> cls = clsList.get(1);
                if (cls.getAnnotation(TaskService.class).readType().equals(ReadType.READ_TYPE_READ)) {
                    clsList.remove(1);
                    clsList.add(0, cls);
                }
            }
        }
        List<Task> tasks = Lists.newArrayList();
        clsList.forEach(v->{
            try {
                Task task = CommonApplication.getBean(v.getSimpleName());
                //Task task = (Task)v.newInstance();
                String hubType = taskHelper.getHubTypeByHubAddr(meterEntiry.getPluginId(), meterEntiry.getHubAddress());
                (task).setMeterHelper(meterEntiry);
                (task).setHubType(hubType);
                (task).setMainOrder(mainOrder);
                (task).setDcId(meterEntiry.getDcId());
                (task).setDcName(meterEntiry.getDcName());
                tasks.add(task);

            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });



        return tasks;
    }

    @Override
    public void process() {
        String mainOrder = IDGenerator.getIdStr();
        int index = 0;
        while (index < this.entiryList.size()) {
            try {
                TimeUnit.MILLISECONDS.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Queue<Task> queue = TaskContainer.getInstance().getEmptyQueue();
            if (Objects.nonNull(queue)) {
                try {
                    List<MeterEntiry> meterEntiries = taskHelper.getSubListByHub(entiryList.get(index).getHubAddress());
                    if (Objects.nonNull(meterEntiries)) {
                        for (int i = 0; i < meterEntiries.size(); i++) {
                            List<Task> taskList = getTask(meterEntiries.get(i), mainOrder);
                            if (Objects.isNull(taskList)) {
                                continue;
                            }
                            for (int j = 0; j < taskList.size(); j++) {
                                queue.offer(taskList.get(j));
                                log.info(">>> TaskHandleHelp, put task to queue: this: {}, task: {}",
                                        taskList.get(j),
                                        (taskList.get(j)).toString());
                            }
                        }
                    }


/*                    meterEntiries.forEach(v->{
                        List<Task> taskList = getTask(v, mainOrder);
                        if (Objects.nonNull(taskList)) {
                            taskList.forEach(task->{
                                queue.offer(task);
                            });
                        }
                    });*/
                    index++;
                } catch (Exception e) {
                    e.printStackTrace();
                    index++;
                }
            }

        }
    }
}
