package com.smart.container;

import com.google.common.collect.Lists;
import com.smart.base.BaseEntity;
import com.smart.framework.commonpool.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.*;

/**
 * @program: smart-service, 一个容器执行一种类型的任务
 * @description:
 * @author: admin
 * @create: 2019-07-22 09:20
 **/
public class TaskContainer<T extends BaseEntity<Long>, S extends Task> {
    private static final Logger log = LoggerFactory.getLogger(TaskContainer.class);
    public static int ASYNC_RUN_NUM = 10;
    private volatile boolean stop = false;
    public int RUNNING_NUM = 0;

    //后续使用集中器的混合模式，此处精确到meterType
    private String taskType;
    private List<TaskEngine> engineQueueList;
    private ThreadPoolExecutor poolExecutor = null;
    private TaskEngine failureTaskEngine;

    public static TaskContainer getInstance() {
        return Holder.taskContainer;
    }
    public static class Holder {
        public static TaskContainer taskContainer = new TaskContainer();
    }


    public int getRUNNING_NUM() { return RUNNING_NUM; }
    public void setRUNNING_NUM(int RUNNING_NUM) { this.RUNNING_NUM = RUNNING_NUM; }

    public Queue getEmptyQueue() {
        for (int i=0; i<engineQueueList.size(); i++) {
            if (engineQueueList.get(i).isEmptyTask()) {
                return engineQueueList.get(i).getTaskQueue();
            }
        }
        return null;
    }


    /*public TaskContainer(String taskType) {
        this.taskType = taskType;
    }*/

    public boolean submit(Task task) {
        TaskWorker worker = new TaskWorker();
        worker.init(task, null ,false);
        poolExecutor.submit(worker);
        return true;
    }

    public boolean startup(int runNums) {
        this.RUNNING_NUM = runNums > ASYNC_RUN_NUM ? ASYNC_RUN_NUM : runNums;

        poolExecutor = new ThreadPoolExecutor(1,
                this.getRUNNING_NUM()+1,
                10,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(this.RUNNING_NUM * 2));


        engineQueueList = Lists.newArrayList();
        for(int i=0; i<this.RUNNING_NUM; i++) {
            TaskEngine taskEngine = new TaskEngine(i);
            engineQueueList.add(taskEngine);
            taskEngine.startup();
        }

        failureTaskEngine = new TaskEngine(engineQueueList.size());
        failureTaskEngine.startup();

        return true;
    }

}
