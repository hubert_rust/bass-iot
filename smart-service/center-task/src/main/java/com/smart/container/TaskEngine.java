package com.smart.container;

import com.smart.framework.commonpool.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @program: 这里提交的task
 * @description:
 * @author: admin
 * @create: 2019-07-22 10:02
 **/
public class TaskEngine {
    private static final Logger log = LoggerFactory.getLogger(TaskEngine.class);
    public static volatile AtomicInteger ENGINE_ID = new AtomicInteger(0);

    private String taskEngineId;

    private Queue<Task> taskQueue;
    private volatile boolean stop = false;
    private TakeTaskThread takeTaskThread;
    public void setStop(boolean stop) {
        this.stop = stop;
    }

    public TaskEngine(int taskEngineId) {
        this.taskEngineId = String.valueOf(taskEngineId);
    }

    public boolean startup() {
        taskQueue = new ConcurrentLinkedQueue<>();
        takeTaskThread = new TakeTaskThread(String.valueOf(ENGINE_ID.getAndAdd(1)));
        takeTaskThread.start();
        return true;
    }
    public void engineStop() {
        this.stop = true;
    }
    public boolean offer(Task task) {
        taskQueue.offer(task);
        return true;
    }
    public Task poll() {
        return taskQueue.poll();
    }

    public boolean isEmptyTask() {
        return taskQueue.isEmpty();
    }

    public Queue<Task> getTaskQueue() {
        return taskQueue;
    }

    public void setTaskQueue(Queue<Task> taskQueue) {
        this.taskQueue = taskQueue;
    }

    public class TakeTaskThread extends Thread {
        private String taskEngineThreadId;
        public TakeTaskThread(String taskEngineId) {
            this.taskEngineThreadId = "thread" + taskEngineId;
        }

        public String getTaskEngineThreadId() {
            return taskEngineThreadId;
        }

        public void setTaskEngineThreadId(String taskEngineThreadId) {
            this.taskEngineThreadId = taskEngineThreadId;
        }

        @Override
        public void run() {
            while (!stop) {
                try {
                    TimeUnit.MILLISECONDS.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                //take task, 然后执行，执行完之后，再执行下一个
                Task task = poll();
                if (Objects.isNull(task)) {
                    continue;
                }
                try {
                    log.info(">>> thread engineId: {}", this.getTaskEngineThreadId(),
                            task.getMeterHelper().toString());
                    task.process();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            log.info(">>> TaskEngine has stopped");
        }
    }
}
