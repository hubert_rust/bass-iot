package com.smart.container;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.smart.ann.TaskService;
import com.smart.base.BaseEntity;
import com.smart.command.CommandCode;
import com.smart.common.PFConstDef;
import com.smart.common.ResponseUtils;
import com.smart.common.ResultConst;
import com.smart.entity.MeterEntiry;
import com.smart.framework.commonpool.Task;
import com.smart.framework.utils.HttpUtils;
import com.smart.msgtpl.MessageTpl;
import com.smart.repository.BaseRepostory;
import com.smart.request.BassHardwardRequest;
import com.smart.task.TaskHelper;
import com.smart.toolkit.IDGenerator;
import com.smart.utils.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * @program: 这部分功能如果需要有手动抄表，在maintance中增加此功能
 * @description:
 * @author: admin
 * @create: 2019-07-11 08:47
 **/

@Component("CommonTask")
@Scope("prototype")
public abstract class CommonReadTask<T extends BaseEntity<Long>> extends Task {
    private static final Logger log = LoggerFactory.getLogger(CommonReadTask.class);

    private String taskState; //"init", "first-retry"

    private Object meterHelper;
    @Autowired
    protected TaskHelper helper;

    @Autowired
    BaseRepostory<T> baseRepostory;

    private String dcName;
    private String dcId;
    private String mainOrder;
    private String hubAddress;
    private String jobHandleName;
    private String hubType;
    private String pluginId;
    private String commandCode;

    public abstract void processFinish(Map<String, Object> respMap,
                                       MeterEntiry meterEntiry,
                                       BassHardwardRequest request,
                                       String mainOrder,
                                       LocalDateTime startTime) throws Exception;

    public Object getMeterHelper() {
        return meterHelper;
    }

    public void setMeterHelper(Object meterHelper) {
        this.meterHelper = meterHelper;
    }
    public String getCommandCode() {
        return commandCode;
    }

    public void setCommandCode(String commandCode) {
        this.commandCode = commandCode;
    }

    public String getTaskState() {
        return taskState;
    }

    public void setTaskState(String taskState) {
        this.taskState = taskState;
    }

    public String getDcName() {
        return dcName;
    }

    public void setDcName(String dcName) {
        this.dcName = dcName;
    }

    public String getDcId() {
        return dcId;
    }

    public void setDcId(String dcId) {
        this.dcId = dcId;
    }

    public String getMainOrder() {
        return mainOrder;
    }

    public void setMainOrder(String mainOrder) {
        this.mainOrder = mainOrder;
    }

    public String getHubAddress() {
        return hubAddress;
    }

    public void setHubAddress(String hubAddress) {
        this.hubAddress = hubAddress;
    }

    public String getJobHandleName() {
        return jobHandleName;
    }

    public void setJobHandleName(String jobHandleName) {
        this.jobHandleName = jobHandleName;
    }

    public String getHubType() {
        return hubType;
    }

    public void setHubType(String hubType) {
        this.hubType = hubType;
    }

    public String getPluginId() {
        return pluginId;
    }

    public void setPluginId(String pluginId) {
        this.pluginId = pluginId;
    }


    /**
     * @Description: 2019-07-23: 抽象为只执行单个命令
     * @Param:
     * @return:
     * @Author: admin
     * @Date: 2019/7/23
     */
    @Override
    public void process() throws Exception {
        Map map = helper.getPluginUrlMap(CommandCode.HUB_TYPE_MIX);
        MeterEntiry val = (MeterEntiry) getMeterHelper();
        String cmdCode = getClass().getAnnotation(TaskService.class).cmdCode();

        BassHardwardRequest request = MessageTpl.getMeterReadTpl(
                val.getMeterType(),
                cmdCode,
                val.getPluginId(),
                IDGenerator.getIdStr(),
                val.getHubAddress(),
                val.getMeterAddress());

        if (request.getHubAddress().equals("0000010018")) {
            log.info(">>> process, request: {}", request.toString());
        }

        LocalDateTime startTime = LocalDateTime.now();

        long startTm = System.currentTimeMillis();
        String resp = null;
        try {
            String url = map.get(val.getPluginId()).toString();
            resp = (String) HttpUtils.commonSendHttpPost(url, JSONObject.toJSONString(request), PFConstDef.HTTP_MILL_TIMEOUT);

        } catch (Exception e) {
            e.printStackTrace();
            resp = ResponseUtils.getFailResponse(e.getMessage());
            log.error(">>> CommonTask, send post err: {}", request.toString());
        }

        Map<String, Object> respMap = JSONObject.parseObject(resp, Map.class);
        ResultConst.ReturnObject retObj = ResultConst.checkResult(respMap);
        if (retObj.getCode().equals(ResultConst.FAIL)) {
            if (((JSONArray)respMap.get("datas")).size() <=0) {
                JSONObject obj = new JSONObject();
                obj.put("hubAddress", val.getHubAddress());
                obj.put("meterAddress", val.getMeterAddress());
                ((JSONArray)respMap.get("datas")).add(obj);
            }
        }


        Class<T> cls = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        String readType = this.getClass().getAnnotation(TaskService.class).readType();
        if (retObj.getCode().equals(ResultConst.SUCCESS)) {
            if (readType.contains(ReadType.READ_TYPE_READ)){
                saveReadResult(respMap,
                        startTime,
                        request.getCommandOrder(),
                        retObj.getCode());
            }
            if (readType.contains(ReadType.READ_TYPE_BALANCE)){
                processFinish(respMap,
                        val,
                        request,
                        getMainOrder(),
                        startTime);
            }
        } else {
            //Class<T> cls = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
            if (readType.contains(ReadType.READ_TYPE_BALANCE)) {
                processFinish(respMap,
                        val,
                        request,
                        getMainOrder(),
                        startTime);
            }
            else {

                T entity = cls.newInstance();
                Method mth = cls.getMethod("setHubAddress", String.class);
                mth.invoke(entity, request.getHubAddress());
                mth = cls.getMethod("setMeterAddress", String.class);
                mth.invoke(entity, request.getSubAddress());
                setCommonData(cls,
                        entity,
                        startTime,
                        request.getCommandOrder(),
                        retObj.getCode());
                baseRepostory.save(entity);
            }
        }

    }

    private void setCommonData(Class<?> cls,
                               T ret,
                               LocalDateTime startTime,
                               String commandOrder,
                               String result) {

        try {
            Method mth = cls.getMethod("setStartTime", LocalDateTime.class);
            mth.invoke(ret, startTime);
            mth = cls.getMethod("setFinishTime", LocalDateTime.class);
            mth.invoke(ret, LocalDateTime.now());
            mth = cls.getMethod("setMainOrder", String.class);
            mth.invoke(ret, getMainOrder());

            mth = cls.getMethod("setCommandOrder", String.class);
            mth.invoke(ret, commandOrder);

            mth = cls.getMethod("setReadType", String.class);
            mth.invoke(ret, PFConstDef.READ_TYPE_AUTO);

            mth = cls.getMethod("setReadStatus", String.class);
            mth.invoke(ret, result);

            ret.setDcId(this.getDcId());
            ret.setDcName(this.getDcName());
            ret.setCreateTime(LocalDateTime.now());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void saveReadResult(Map<String, Object> respMap,
                               LocalDateTime startTime,
                               String commandOrder,
                               String result) {

        try {
            List<Map<String, Object>> list = (List<Map<String, Object>>) respMap.get("datas");
            if (list.size() > 0) {

                Class<T> cls = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
                T ret = CommonUtils.mapToEntity(list.get(0), cls);
                setCommonData(cls,
                        ret,
                        startTime,
                        commandOrder,
                        result);
                baseRepostory.save(ret);
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }


    @Override
    public String toString() {
        return "CommonTask{" +
                "meterHelper=" + meterHelper +
                ", mainOrder='" + mainOrder + '\'' +
                ", hubAddress='" + hubAddress + '\'' +
                ", hubType='" + hubType + '\'' +
                ", pluginId='" + pluginId + '\'' +
                ", commandCode='" + commandCode + '\'' +
                '}';
    }
}
