package com.smart.container;

import com.smart.command.CommandCode;
import com.smart.entity.ConcentratorEntity;
import com.smart.task.TaskHelper;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.CyclicBarrier;
import java.util.stream.Collectors;

/**
 * @program:
 * @description:
 * @author: admin
 * @create: 2019-07-11 08:44
 **/
@JobHandler(value = "MeterReadHandle")
@Component
public class MeterReadHandle extends IJobHandler {
    private static final Logger log = LoggerFactory.getLogger(MeterReadHandle.class);


    @Autowired
    TaskHelper taskHelper;

    @Autowired
    TaskHandleHelp taskHandleHelp;

    @Override
    public ReturnT<String> execute(String param) throws Exception {
        List<ConcentratorEntity> list = taskHelper.getAllHubConfig()
                .stream()
                .filter(v->{
                    return (v.getHubType().equals(CommandCode.HUB_TYPE_HOTMETER)
                            || v.getHubType().equals(CommandCode.HUB_TYPE_HIGHMETER)
                            || v.getHubType().equals(CommandCode.HUB_TYPE_COLDMETER)
                            || v.getHubType().equals(CommandCode.HUB_TYPE_ELECMETER)
                    || v.getHubType().equals(CommandCode.HUB_TYPE_MIX));
                })
                .collect(Collectors.toList());

        taskHandleHelp.init(list, CommonTask.class);

        CyclicBarrier cyclicBarrier = new CyclicBarrier(2);
        TaskWorker taskWorker = new TaskWorker();
        taskWorker.init(taskHandleHelp, cyclicBarrier, true);
        taskWorker.start();
        cyclicBarrier.await();

        list.clear();
        return ReturnT.SUCCESS;
    }
}
