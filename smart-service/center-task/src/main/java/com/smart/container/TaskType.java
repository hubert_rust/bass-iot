package com.smart.container;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-22 09:32
 **/
public class TaskType {
    public static final String TASK_TYPE_HUB = "hub-task";
    public static final String TASK_TYPE_METER = "meter-task";
}
