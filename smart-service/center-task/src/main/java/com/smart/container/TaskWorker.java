package com.smart.container;

import com.smart.framework.commonpool.Task;

import java.util.concurrent.CyclicBarrier;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-06-20 15:41
 **/
public class TaskWorker extends Thread {
    private Task task;
    private CyclicBarrier cyclicBarrier;
    private boolean wait;

    public void init(Task task, CyclicBarrier cyclicBarrier, boolean wait) {
        this.task = task;
        this.cyclicBarrier = cyclicBarrier;
        this.wait = wait;
    }

    @Override
    public void run() {
        try {
            task.process();
            if (wait) {
                cyclicBarrier.await();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
