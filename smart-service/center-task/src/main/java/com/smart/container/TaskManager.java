package com.smart.container;

import com.smart.framework.commonpool.Task;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-22 11:02
 **/
public class TaskManager {

    public static TaskContainer taskContainer = null;

    public static TaskManager getInstance() {
        return TaskManagerHolder.getTaskManager();
    }

    public static class TaskManagerHolder  {
        public static TaskManager taskManager = new TaskManager();

        public static TaskManager getTaskManager() {
            return taskManager;
        }
    }

    public static boolean startup() {
       taskContainer = TaskContainer.getInstance();
       taskContainer.startup(10);
       return true;
    }


}
