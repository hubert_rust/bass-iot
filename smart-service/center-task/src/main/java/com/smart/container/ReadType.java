package com.smart.container;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-29 10:06
 **/
public class ReadType {
    public static final String READ_TYPE_READ = "read";
    public static final String READ_TYPE_BALANCE = "balance";
    public static final String READ_TYPE_MIX = "read-balance";
}
