package com.smart.container;

/**
 * @program: 集中器和meter实现该接口
 * @description:
 * @author: admin
 * @create: 2019-07-22 09:29
 **/
public interface IProcess {
    void process();
}
