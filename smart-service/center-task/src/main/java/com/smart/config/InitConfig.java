package com.smart.config;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.smart.ann.TaskService;
import com.smart.container.ITask;
import com.smart.container.TaskManager;
import com.smart.framework.commonpool.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-22 14:06
 **/
@Component
public class InitConfig implements ApplicationRunner {
    private static final Logger log = LoggerFactory.getLogger(InitConfig.class);
    public static Map<String, List<Class<?>>> METER_TASK = Maps.newHashMap();
    public static final String PACKATE_NAME = "com.smart";

    public final static String PACKAGE_PATH_FLAG = "com/smart";

    @Override
    public void run(ApplicationArguments args) throws Exception {
        loadTaskCls();
        TaskManager.startup();
    }

    private void findClass(String strPath, List<String> classFile) {
        File file = new File(strPath);
        File[] files = file.listFiles();

        List<String> list = Lists.newArrayList();
        for (int i = 0; i < files.length; i++) {
            if (files[i].isDirectory()) {
                list.add(files[i].getPath());
            } else {
                classFile.add(files[i].getPath());
            }
        }

        for (int j = 0; j < list.size(); j++) {
            findClass(list.get(j), classFile);
        }
        list.clear();
    }

    private void findClassFromJar(String jar, List<String> classFile) {

    }

    private void loadTaskCls() throws Exception {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        URL url = loader.getResource(PACKATE_NAME.replace(".", "/"));

        String jarFilePath = url.getFile().substring(0, url.getPath().indexOf("!"));
        jarFilePath = jarFilePath.replace("file:", "");

        //String jarFilePath = System.getProperty("user.dir");
        //jarFilePath += File.separator;
        //String jarName = findJar(jarFilePath);
        //jarFilePath += jarName;

        //从jar包中加载
        JarFile jarFile = new JarFile(jarFilePath);
        Enumeration<JarEntry> enumeration = jarFile.entries();
        while (enumeration.hasMoreElements()) {
            JarEntry jarEntry = enumeration.nextElement();
            if (jarEntry.isDirectory() || !jarEntry.getName().endsWith(".class")
                    || jarEntry.getName().indexOf(PACKAGE_PATH_FLAG) < 0
                    || jarEntry.getName().indexOf("$") >= 0) {
                continue;
            }
            log.info(jarEntry.getName());

            String className = jarEntry.getName();
            className = className.substring(className.indexOf(PACKAGE_PATH_FLAG));
            className = className.substring(0, className.lastIndexOf(".class"));
            className = className.replace(File.separator, ".");

            try {
                Class<?> cls = Class.forName(className);
                if (Objects.nonNull(cls.getAnnotation(TaskService.class))) {

                    //然后找到实现ITask接口的类
                    if (ITask.class.isAssignableFrom(cls)) {
                        String meterType = cls.getAnnotation(TaskService.class).meterType();
                        if (Objects.isNull(METER_TASK.get(meterType))) {
                            List<Class<?>> list = Lists.newArrayList();
                            list.add(cls);

                            METER_TASK.put(meterType, list);
                        } else {
                           METER_TASK.get(meterType).add(cls);
                        }
                    }
                }

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        }
        //List<String> fileClassList = Lists.newArrayList();
        //findClass(url.getPath(), fileClassList);



        /*try {
            Enumeration<URL> enumeration = loader.getResources(PACKATE_NAME.replace(".", "/"));
            while (enumeration.hasMoreElements()) {
                URL resoure = enumeration.nextElement();
                System.out.println(resoure.getPath());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        File file = new File(url.getPath());

        File[] files = file.listFiles();
        System.out.println("");*/
    }
}
