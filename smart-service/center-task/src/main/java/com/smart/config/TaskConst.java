package com.smart.config;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-06-27 14:30
 **/
public class TaskConst {
    public static final String JOB_HANDLE_LOCKER_UPDATE_SECRETKEY = "updateLockerSecretKey";
    public static final String JOB_RUN_MODE_SYNC = "sync";
    public static final String JOB_RUN_MODE_ASYNC = "async";

    public static final String JOB_MAIN_LOG_STATE_PREPARE = "prepare";
    public static final String JOB_MAIN_LOG_STATE_FINISH = "finish";
    public static final int JOB_LOCKER_TIME_SPAN_15 = 15;
    public static final int JOB_LOCKER_TIME_SPAN_60 = 60;

    /**
     * @Description: 公共函数，生成秘钥
     * @Param:
     * @return:
     * @Author: admin
     * @Date: 2019/6/20
     */
    public static String getSecretkey() {
        long secret = getSix();
        String secretKey = Long.toHexString(secret);
        String hex = secretKey;
        int len = secretKey.length();
        int verdict = len % 2;
        for (int i = len; i < 8; i++) {
            hex = "0" + secretKey;
            //如果密钥位数不是2的倍数,前面补0
            if (verdict != 0) {
                hex = "0" + secretKey;
            }
        }

        return hex;
    }

    public static long getSix() {
        long result = 0;
        outer:
        while (true) {
            result = 0;
            int i;
            for (i = 0; i < 32; i++) {
                int n = (int) (Math.random() * 2);
                result += Math.pow(2, i) * n;
            }
            if (result > 4294967295L) {
                i = 0;
                result = 0;
            } else {
                break outer;
            }
        }
        return result;
    }

    public static long leftdata(long data,long left) {
        return ((data<<left)+(data>>(19-left)))&0x7ffff;
    }

    public static long rightdata(long data,long left) {
        return ((data>>left)+(data<<(19-left)))&0x7ffff;
    }

    /**
     * @Description:
     * @Param: 时间段: 8:00-9:00 startTime:8  endTime:9
     * @return:
     * @Author: admin
     * @Date: 2019/6/28
     */
    public static String createPasswordOneHour(long startTime, long endTime, long secretKey) {
        //LocalDateTime dateTime =LocalDateTime.ofEpochSecond(startTime,0, ZoneOffset.ofHours(8));

        //System.out.println(">>> createPassword, startTime: +" + dateTime.toString());
        long data=((startTime*24)+endTime)*(secretKey%910);
        for(int i=0;i<8;i++)
        {
            data=  leftdata(data,(secretKey>>4)&0x0f);
            data^= rightdata(secretKey,i*2+1);
        }
        String password = String.valueOf(data);
        //如果密码小于6位,则在前面加0
        for(int i = password.length();i < 6;i++){
            password = "0" + password;
        }
        return password;
    }

    /**
     * @Description:
     * @Param:  startTime和endTime单位是时钟刻度(将时钟分为96格，每格15分钟)(例如8:15-8:30对应(8*60+15)/15-(8*60+30)/15)
     * @return:
     * @Author: admin
     * @Date: 2019/7/1
     */
    public static String createPasswordFifteenMin(long startTime, long endTime, long secretKey) {
        LocalDateTime dateTime =LocalDateTime.ofEpochSecond(startTime,0, ZoneOffset.ofHours(8));

        System.out.println(">>> createPassword, startTime: +" + dateTime.toString());
        long data=((startTime*24*4)+endTime)*(secretKey%56);
        for(int i=0;i<8;i++)
        {
            data=  leftdata(data,(secretKey>>4)&0x0f);
            data^= rightdata(secretKey,i*2+1);
        }
        String password = String.valueOf(data);
        //如果密码小于6位,则在前面加0
        for(int i = password.length();i < 6;i++){
            password = "0" + password;
        }
        return password;
    }


    public static boolean Decrypt(long key, long secret_key)//-
    {
        int i = 8;
        long start_time,end_time,data,DAYS=86400;
        long TIME = System.currentTimeMillis()/1000;
        //TIME = 9*3600;
        int tok = 56; //1小时的话是910

        data=key;
        for(i=8;i>0;i--)
        {
            data^=rightdata(secret_key,i*2-1);
            data=rightdata(data,(secret_key>>4)&0x0f);
        }
        if((data%(secret_key%tok))!=0)
        {
            return false;
        }
        //start_time=((data/(secret_key%tok))/24)*3600;
        //end_time=((data/(secret_key%tok))%24)*3600;
        start_time=((data/(secret_key%tok))/96)*900;
        end_time=((data/(secret_key%tok))%96)*900;
        data=(TIME+28800)%DAYS;
        if((start_time<=data)&&(data<=end_time))
        {
            return true;
        }
        return false;
    }

}
