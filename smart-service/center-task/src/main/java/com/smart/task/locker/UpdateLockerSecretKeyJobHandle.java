package com.smart.task.locker;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.smart.command.CommandCode;
import com.smart.common.PFConstDef;
import com.smart.common.ResponseUtils;
import com.smart.common.ResultConst;
import com.smart.config.TaskConst;
import com.smart.entity.*;
import com.smart.framework.cons.BassConst;
import com.smart.framework.utils.CommonApplication;
import com.smart.framework.utils.HttpUtils;
import com.smart.repository.ConcentratorRepostory;
import com.smart.repository.LockerOfflineKeyRepository;
import com.smart.request.BassHardwardRequest;
import com.smart.task.BaseJobHandle;
import com.smart.task.JobWorker;
import com.smart.toolkit.IDGenerator;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

/**
 * @program: smart-service
 * @description: 更新锁密钥, 每天00:00:10开始更新，并且保存当前
 * 1、系统初始化，生成1周密钥，
 * 2、主要是同步执行，后续锁多了(超过200)，可以采用按照集中器下发命令
 * @author: admin
 * @create: 2019-06-20 14:36
 **/
@JobHandler(value = "updateLockerSecretKey")
@Component
public class UpdateLockerSecretKeyJobHandle extends IJobHandler {
    private static final Logger log = LoggerFactory.getLogger(UpdateLockerSecretKeyJobHandle.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private ConcentratorRepostory conRepostory;

    @Autowired
    private BaseJobHandle baseJobHandle;

    @Autowired
    private LockerOfflineKeyRepository keyRepository;

    public static int DAYS_DATA = 7;
    private static int TIME_SPAN = TaskConst.JOB_LOCKER_TIME_SPAN_15;

    public static Map<String, PluginEntity> pluginMap = Maps.newHashMap();

    private List<ConcentratorEntity> getHubConfig() {
        return conRepostory.findAll((root, query, builder) -> {
            return builder.equal(root.get("hubType"), CommandCode.HUB_TYPE_LOCKER);
        });
    }

    /**
     * @Description: {conAddr:"", subAddr: ""}
     * @Param:
     * @return:
     * @Author: admin
     * @Date: 2019/6/20
     */
    private void withParam(String param) throws Exception {

        CyclicBarrier cb = new CyclicBarrier(2);
        Map<String, Object> map = new Gson().fromJson(param, Map.class);

        UpdateLockerSecretKeyTask task = CommonApplication.getBean("updateLockerSecretKeyTask");
        task.setConAddress(map.get("conAddr").toString());
        task.setSubAddress(map.get("subAddr").toString());
        task.setJobHandleName("updateLockerSecretKey");
        JobWorker jobWorker = new JobWorker();
        jobWorker.init(task, cb);
        jobWorker.start();

        //在主线程等待
        cb.await();
    }

    /**
     * @Description: 保留7天的数据
     * @Param:
     * @return:
     * @Author: admin
     * @Date: 2019/7/1
     */
    public void clearHistory(int beforeDays) {

        List<LockerOfflineKeyEntity> list = keyRepository.findAll((root, query, builder) -> {
            return builder.lessThan(root.get("setTime"), LocalDate.now().minusDays(beforeDays).toString());
        });

        keyRepository.deleteAll(list);
    }

    @Override
    public ReturnT<String> execute(String param) throws Exception {
        XxlJobLogger.log(">>> UpdateLockerSecretKeyJobHandle, execute");

        //删除7天之前的数据
        //clearHistory(7);
        JobParam jobParam = null;
        try {
            jobParam = baseJobHandle.parseJobParam(param);
        } catch (Exception e) {
            e.printStackTrace();
            return ReturnT.FAIL;
        }

        //初始化
        pluginMap.putAll(baseJobHandle.getPluginMap());
        if (jobParam == null || jobParam.getRunMode().equals(TaskConst.JOB_RUN_MODE_SYNC)) {
            syncJobExecute(jobParam);
        } else if (jobParam.getRunMode().equals(TaskConst.JOB_RUN_MODE_ASYNC)) {
            //暂时不需要, 门锁比较少
            //asyncJobExecute();
        } else {
            XxlJobLogger.log(">>> SyncTimeJobHandle, invalid runMode: {}",
                    jobParam.getRunMode());
        }
        return ReturnT.SUCCESS;
    }

    @Deprecated
    private void asyncJobExecute(JobParam jobParam) throws Exception {
        //不带参数自动执行
        //先查询出, 按照集中器分组执行
        List<ConcentratorEntity> hubList = getHubConfig();
        CyclicBarrier cb = new CyclicBarrier(hubList.size() + 1);
        for (int i = 0; i < hubList.size(); i++) {
            if (Objects.isNull(hubList.get(i))) {
                continue;
            }
            if (Objects.isNull(hubList.get(i).getHubAddress())) {
                continue;
            }

            UpdateLockerSecretKeyTask task = CommonApplication.getBean("updateLockerSecretKeyTask");
            task.setConAddress(hubList.get(i).getHubAddress());
            task.setJobHandleName(TaskConst.JOB_HANDLE_LOCKER_UPDATE_SECRETKEY);
            JobWorker jobWorker = new JobWorker();
            jobWorker.init(task, cb);
            jobWorker.start();
        }

        cb.await();
    }

    public static void main(String[] args) {
        LocalDateTime localDateTime = LocalDateTime.now();
        LocalDateTime next = localDateTime.plusDays(1);

        //System.out.println();
    }

    private void initKeyData(List<MeterEntiry> list, int startDay, int endDay) {
        String commandOrder = IDGenerator.getIdStr();
        for (int i = startDay; i < endDay; i++) {
            final int plus = i;
            list.forEach(val -> {
                LockerOfflineKeyEntity keyEntity = baseJobHandle.secretKeySave(commandOrder, val.getPluginId(),
                        val.getHubAddress(), val.getMeterAddress(), LocalDate.now().plusDays(plus).toString());

            });
        }
    }

    private void syncJobExecute(JobParam jobParam) {
        String pluginId = "";
        String dcName = "";
        String dcId = "";

        List<MeterEntiry> subList = baseJobHandle.getSubHWConfig(CommandCode.HUB_TYPE_LOCKER);
        if (subList.size() > 0) {
            pluginId = subList.get(0).getPluginId();
            dcName = subList.get(0).getDcName();
            dcId = subList.get(0).getDcId();
        } else {
            XxlJobLogger.log(">>> syncJobExecute, subList.size() <=0");
            return;
        }
        //如果是初次使用，days表示除了生成当天数据，还有生产几天（days天）后的key
        int days = (jobParam == null || jobParam.getDatas() == null) ? 7 : jobParam.getDaysData();
        if (keyRepository.count() <= 0) {
            initKeyData(subList, 0, days + 1);
        } else {
            //生成
            initKeyData(subList, days, days + 1);
        }

        //获取当天数据，然后下发到锁
        List<MeterEntiry> executeList = null;
        if (jobParam == null
                || jobParam.getDatas() == null
                || jobParam.getDatas().size() <= 0) {
            //全量执行
            executeList = subList;
        } else {
            //
            if (Objects.nonNull(jobParam.getDatas()) && jobParam.getDatas().size() > 0) {
                TIME_SPAN = jobParam.getDatas().get(0).getTimeSpan();
                TIME_SPAN = Objects.isNull(TIME_SPAN) ? TIME_SPAN : TaskConst.JOB_LOCKER_TIME_SPAN_15;
            }
            //根据参数执行
            executeList = Lists.newArrayList();
            for (JobParam.RunParam v : jobParam.getDatas()) {
                String hubAddr = v.getHubAddr();
                String subAddr = v.getSubAddr();
                if (Strings.isNullOrEmpty(hubAddr)) {
                    CronMainJobLogEntity mainEnity = baseJobHandle.createEntity(CronMainJobLogEntity.class);
                    mainEnity.setDcId(dcId);
                    mainEnity.setDcId(dcName);
                    mainEnity.setJobHandle(TaskConst.JOB_HANDLE_LOCKER_UPDATE_SECRETKEY);
                    mainEnity.setState(TaskConst.JOB_MAIN_LOG_STATE_PREPARE);
                    baseJobHandle.saveMainLog(mainEnity);
                    continue;
                }
                List<MeterEntiry> retList = baseJobHandle.getSubHWListByHubList(hubAddr, subAddr, CommandCode.HUB_TYPE_LOCKER);
                executeList.addAll(retList);
            }
        }

        executeProcess(executeList);
    }

    private void executeProcess(List<MeterEntiry> meterList) {
        List<MeterEntiry> failList = Lists.newArrayList();
        executeCommand(meterList, failList, 0);

        //失败重试一次
        List<MeterEntiry> failList2 = Lists.newArrayList();
        executeCommand(failList, failList2, 1);
    }

    private void executeCommand(List<MeterEntiry> meterList,
                                List<MeterEntiry> commandOrderList,
                                int retrys) {
        LocalDateTime startTm = LocalDateTime.now();
        String mainOrder = IDGenerator.getIdStr();
        Map<String, LockerOfflineKeyEntity> offKeyMap = null;

        Object resp = null;
        for (MeterEntiry val : meterList) {
            if (val.getRemark() != null && val.getRemark().equals(ResultConst.SUCCESS)) {
                continue;
            }
            try {
                TimeUnit.MILLISECONDS.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            LocalDateTime postTime = LocalDateTime.now();
            String commandOrder = "";
            try {
                if (Objects.isNull(offKeyMap)) {
                    offKeyMap = baseJobHandle.getTodaySecretKey(val.getPluginId(), LocalDate.now().toString());
                    if (offKeyMap == null) {
                        log.error(">>> secretKey->executeCommand, offKeyMap is null");
                        throw new Exception("secretkey->executeCommand, offkeyMap is null");
                    }
                }
                String mapKey = val.getPluginId() + val.getHubAddress() + val.getMeterAddress();
                LockerOfflineKeyEntity keyEntity = offKeyMap.get(mapKey);
                if (Objects.nonNull(keyEntity)) {
                    commandOrder = keyEntity.getCommandOrder();
                    resp = secretKeyProcess(val, commandOrder, keyEntity.getHexKey());
                } else {
                    String var = "没有查询到 hubAddress: " + val.getHubAddress() + ", subAddress: " + val.getMeterAddress();
                    var += " secretKey.";
                    resp = ResponseUtils.getFailResponse(var);
                }
            } catch (Exception e) {
                e.printStackTrace();
                String var = "hubAddress: " + val.getHubAddress() + ", subAddress: " + val.getMeterAddress();
                var += " 下发secretKey异常: ";
                var += e.getMessage();
                resp = ResponseUtils.getFailResponse(var);
            }


            Map map = (Map) JSONObject.parseObject((String) resp, Map.class);
            ResultConst.ReturnObject ret = ResultConst.checkResult(map);
            val.setRemark(ret.getCode());
            if (ret.getCode().equals(ResultConst.FAIL)) {
                commandOrderList.add(val);
            }

            keyRepository.updateResult(ret.getCode(),retrys, commandOrder);
            saveJobLob(val, postTime, mainOrder, commandOrder, ret.getCode(), ret.getMessage());
        }

        CronMainJobLogEntity entity = baseJobHandle.createEntity(CronMainJobLogEntity.class);
        entity.setMainOrder(mainOrder);
        entity.setJobHandle(TaskConst.JOB_HANDLE_LOCKER_UPDATE_SECRETKEY);
        entity.setState(TaskConst.JOB_MAIN_LOG_STATE_FINISH);
        entity.setStartTime(startTm);
        entity.setFinishTime(LocalDateTime.now());
        baseJobHandle.saveMainLog(entity);
    }

    private void saveJobLob(MeterEntiry val,
                            LocalDateTime startTime,
                            String mainOrder,
                            String commandOrder,
                            String result,
                            String remark) {

        CronJobLogEntity jobLogEntity = CronJobLogEntity.builder()
                .startTime(startTime)
                .mainOrder(mainOrder)
                .commandOrder(commandOrder)
                .PluginId(val.getPluginId())
                .hubType(CommandCode.HUB_TYPE_LOCKER)
                .hubAddress(val.getHubAddress())
                .subAddress(val.getMeterAddress())
                .result(result)
                .remark(remark)
                .jobHandle(TaskConst.JOB_HANDLE_LOCKER_UPDATE_SECRETKEY)
                .build();
        jobLogEntity.setDcId(val.getDcId());
        jobLogEntity.setDcName(val.getDcName());

        baseJobHandle.savaLog(jobLogEntity);
    }

    private String secretKeyProcess(MeterEntiry val,
                                    String commandOrder,
                                    String hexKey) {

        BassHardwardRequest request = getRequest(commandOrder, val.getPluginId(),
                val.getHubAddress(), val.getMeterAddress(), hexKey);

        String url = pluginMap.get(val.getPluginId()).getPluginUrl();
        String resp = null;
        try {
            resp = (String) HttpUtils.commonSendHttpPost(url, JSONObject.toJSONString(request), PFConstDef.HTTP_MILL_TIMEOUT);
            Map<String, Object> respMap = JSONObject.parseObject(resp, Map.class);
            ResultConst.ReturnObject retObj = ResultConst.checkResult(respMap);
        } catch (Exception e) {
            resp = ResponseUtils.getFailResponse(ResultConst.FAIL);
        }
        return resp;
    }

    private BassHardwardRequest getRequest(String commandOrder,
                                           String plugIn,
                                           String conAddr,
                                           String subAddr,
                                           String secretKey) {
        BassHardwardRequest request = null;
        request = new BassHardwardRequest();
        request.setServiceType(BassConst.BASS_HARDWARD_SERVICE_NAME_PLUGIN);
        request.setServiceId(plugIn);
        request.setCommandOrder(commandOrder);
        request.setHubAddress(conAddr);
        request.setSubAddress(subAddr);
        request.setCmdCode(CommandCode.CMD_LOCK_SET_OFFLINE_SECRETKEY);
        request.setCmdType(CommandCode.HUB_TYPE_LOCKER);
        request.setSubType(BassConst.BASS_HARDWARD_SERVICE_PLUGIN_SUBTYPE_FUNC);
        request.setIsRetSendBytes(1);
        request.setIsRetRespBytes(1);

        Map<String, Object> map = Maps.newHashMap();
        map.put("hubAddress", conAddr);
        map.put("meterAddress", subAddr);
        map.put("secretKey", secretKey);   //16进制
        List<Map<String, Object>> list = Lists.newArrayList();
        list.add(map);
        request.setEntitys(list);

        return request;
    }

}
