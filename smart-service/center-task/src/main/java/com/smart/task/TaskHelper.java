package com.smart.task;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.smart.entity.*;
import com.smart.repository.CronJobLogRepository;
import com.smart.repository.CronMainJobLogRepository;
import com.smart.service.HubHardware;
import com.smart.service.MeterHardware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-11 09:38
 **/

@Component
@Scope("prototype")
public class TaskHelper {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    MeterHardware meterHardware;
    @Autowired
    HubHardware hubHardware;
    @Autowired
    CronJobLogRepository jobLogRepository;
    @Autowired
    CronMainJobLogRepository mainLogRepository;

    public Map getPluginUrlMap(String hubType) throws Exception {
        List<Map<String, Object>> list = getPlugInConfig(hubType);
        Map<String, String> ret = Maps.newHashMap();
        list.forEach(v -> {
            ret.put(v.get("plugin_id").toString(), v.get("plugin_url").toString());
        });
        list.clear();
        return ret;
    }

    public String getHubTypeByHubAddr(String pluginId,
                                      String hubAddress) throws Exception {
        Optional<ConcentratorEntity> concentratorEntity = hubHardware.findOne((root, query, builder)->{
            Predicate p1 = builder.equal(root.get("pluginId"), pluginId);
            Predicate p2 = builder.equal(root.get("hubAddress"), hubAddress);
            return builder.and(p1, p2);
        });

        return concentratorEntity.isPresent() ? concentratorEntity.get().getHubType() : "";
    }

    public List<Map<String, Object>> getPlugInConfig(String pluginName) throws Exception{
        if (Strings.isNullOrEmpty(pluginName)) {
            return jdbcTemplate.queryForList("select * from tb_hardware_plugin_manage");
        } else {
            return jdbcTemplate.queryForList("select * from tb_hardware_plugin_manage where plugin_name = '" + pluginName + "'");
        }
    }

    public JobParam parseJobParam(String param) {
        if (Strings.isNullOrEmpty(param)) {
            return null;
        }
        return JSONObject.parseObject(param, JobParam.class);
    }

    public List<MeterEntiry> getSubList(String subType) throws Exception {
        List<MeterEntiry> ret = meterHardware.findAll((root, query, builder) -> {
            return builder.equal(root.get("meterType"), subType);
        });
        return ret;
    }

    public List<ConcentratorEntity> getAllHubConfig() throws Exception {
        return hubHardware.findAll();
    }

    public List<MeterEntiry> getSubListByHub(String hubAddr) throws Exception {
        List<MeterEntiry> ret = meterHardware.findAll((root, query, builder) -> {
            return builder.equal(root.get("hubAddress"), hubAddr);
        });
        return ret;
    }
    public List<ConcentratorEntity> getHubConfig(String hubAddr, String hubType) throws Exception {
        if (Strings.isNullOrEmpty(hubAddr)) {
            return hubHardware.findAll((root, query, builder) -> {
                return builder.equal(root.get("hubType"), hubType);
            });
        } else {
            return hubHardware.findAll((root, query, builder) -> {
                Predicate p1 = builder.equal(root.get("hubAddress"), hubAddr);
                Predicate p2 = builder.equal(root.get("hubType"), hubType);
                return builder.and(p1, p2);
            });
        }
    }

    public void cronMainJobLogSave(String jobHandleName,
                                   String mainOrder,
                                   LocalDateTime startTime,
                                   String retCode,
                                   String remark) {
        LocalDateTime finish = LocalDateTime.now();
        CronMainJobLogEntity mainJobLogEntity = new CronMainJobLogEntity();
        mainJobLogEntity.setMainOrder(mainOrder);
        mainJobLogEntity.setStartTime(startTime);
        mainJobLogEntity.setFinishTime(finish);
        mainJobLogEntity.setCreateTime(finish);
        mainJobLogEntity.setState(retCode);
        mainJobLogEntity.setRemark(remark);
        mainJobLogEntity.setJobHandle(jobHandleName);
        mainLogRepository.save(mainJobLogEntity);
    }

    //定时任务执行日志保存
    public void cronJobLogSave(String pluginId,
                               String jobHandleName,
                               String mainOrder,
                               String commandOrder,
                               LocalDateTime startTime,
                               String conAddr,
                               String subAddr,
                               String hubType,
                               String result,
                               String remark) throws Exception {
        try {
            CronJobLogEntity cronJobLogEntity = CronJobLogEntity.builder()
                    .startTime(startTime)
                    .finishTime(LocalDateTime.now())
                    .hubAddress(conAddr)
                    .subAddress(subAddr)
                    .jobHandle(jobHandleName)
                    .hubType(hubType)
                    .result(result)
                    .build();
            cronJobLogEntity.setPluginId(pluginId);
            cronJobLogEntity.setMainOrder(mainOrder);
            cronJobLogEntity.setCommandOrder(commandOrder);
            cronJobLogEntity.setCreateTime(LocalDateTime.now());
            cronJobLogEntity.setHubType(hubType);
            cronJobLogEntity.setResult(result);
            cronJobLogEntity.setRemark(remark);
            jobLogRepository.save(cronJobLogEntity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
