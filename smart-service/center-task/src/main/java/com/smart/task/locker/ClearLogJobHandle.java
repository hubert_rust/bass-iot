package com.smart.task.locker;

import com.smart.common.ResultConst;
import com.smart.entity.*;
import com.smart.repository.ClearJobConfigRepostory;
import com.smart.repository.CronJobLogRepository;
import com.smart.repository.CronMainJobLogRepository;
import com.smart.repository.PluginEventRepository;
import com.smart.task.BaseJobHandle;
import com.smart.toolkit.IDGenerator;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @program: center-task
 * @description: 定时清理过期数据，凌晨4:00 (0 0 4 * * ?)
 * @author: admin
 * @create: 2019-06-20 14:36
 **/
@JobHandler(value = "clearLogJobHandle")
@Component
public class ClearLogJobHandle extends IJobHandler {
    private static final Logger log = LoggerFactory.getLogger(ClearLogJobHandle.class);
    public static final int KEEP_DAYS_DEFAULT = 7;
    @Autowired
    private ClearJobConfigRepostory clearJobRepostory;
    @Autowired
    public PluginEventRepository eventRepository;

    @Autowired
    public CronJobLogRepository jobLogRepository;
    @Autowired
    public CronMainJobLogRepository mainJobLogRepository;

    @Autowired
    BaseJobHandle baseJobHandle;

    public interface BaseClearLog {
        void process(ClearLogConfigEntity val);
    }
    public class EventRecord implements BaseClearLog {
        public EventRecord() {
        }

        @Override
        public void process(ClearLogConfigEntity val) {
            log.info(">>> ClearLogJobHandle, EventRecord, process, keep: {}", val.getKeepDays());
            int days = (val.getKeepDays() <=0) ? KEEP_DAYS_DEFAULT : val.getKeepDays();
            try {
                List<PluginEventEntity> list = eventRepository.findAll((root, query, builder)->{
                    return builder.lessThan(root.get("createTime"), LocalDateTime.now().minusDays(days));
                });
                eventRepository.deleteAll(list);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
    public class JobLog implements BaseClearLog {
        public JobLog() {
        }

        @Override
        public void process(ClearLogConfigEntity val) {
            log.info(">>> ClearLogJobHandle, JobLog, process, keep: {}", val.getKeepDays());
            int days = (val.getKeepDays() <=0) ? KEEP_DAYS_DEFAULT : val.getKeepDays();
            try {
                List<CronJobLogEntity> list = jobLogRepository.findAll((root, query, builder)->{
                    return builder.lessThan(root.get("createTime"), LocalDateTime.now().minusDays(days));
                });
                jobLogRepository.deleteAll(list);
            } catch (Exception e) {
                e.printStackTrace();
            }

            //删除main
            try {
                List<CronMainJobLogEntity> list = mainJobLogRepository.findAll((root, query, builder)->{
                    return builder.lessThan(root.get("createTime"), LocalDateTime.now().minusDays(days));
                });
                mainJobLogRepository.deleteAll(list);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
    * @Description:
    * @Param:
    * @return:
    * @Author: admin
    * @Date: 2019/7/1
    */
    @Override
    public ReturnT<String> execute(String param) throws Exception {
        XxlJobLogger.log(">>> clearLogJobHandle, execute");
        final ClearLogJobHandle _this = this;

        List<ClearLogConfigEntity> list = clearJobRepostory.findAll();
        list.forEach(v-> {
            String remark = "";
            String retCode = ResultConst.SUCCESS;
            LocalDateTime localDateTime = LocalDateTime.now();
            String mainOrder = IDGenerator.getIdStr();

            try {
                //内部类需要传入this
                Class cls = Class.forName("com.smart.task.locker.ClearLogJobHandle$"+ v.getServiceName());
                BaseClearLog er = (BaseClearLog) (cls.getConstructors()[0].newInstance(_this));
                er.process(v);
            } catch (Exception e) {
                e.printStackTrace();
                remark = e.getMessage();
                retCode = ResultConst.FAIL;
            } finally {

                LocalDateTime finishTime = LocalDateTime.now();
                CronJobLogEntity cronJobLogEntity = new CronJobLogEntity();
                cronJobLogEntity.setMainOrder(mainOrder);
                cronJobLogEntity.setJobHandle("ClearLogJobHandle-" + v.getServiceName());
                cronJobLogEntity.setFinishTime(finishTime);
                cronJobLogEntity.setCreateTime(finishTime);
                cronJobLogEntity.setStartTime(localDateTime);
                cronJobLogEntity.setResult(retCode);
                cronJobLogEntity.setRemark(remark);
                cronJobLogEntity.setCommandOrder(IDGenerator.getIdStr());
                baseJobHandle.savaLog(cronJobLogEntity);

                CronMainJobLogEntity mainJobLogEntity = new CronMainJobLogEntity();
                mainJobLogEntity.setMainOrder(mainOrder);
                mainJobLogEntity.setStartTime(localDateTime);
                mainJobLogEntity.setFinishTime(finishTime);
                mainJobLogEntity.setState(retCode);
                mainJobLogEntity.setRemark(remark);
                mainJobLogEntity.setJobHandle("ClearLogJobHandle-" + v.getServiceName());
                baseJobHandle.saveMainLog(mainJobLogEntity);
            }

        });

        return ReturnT.SUCCESS;
    }
}
