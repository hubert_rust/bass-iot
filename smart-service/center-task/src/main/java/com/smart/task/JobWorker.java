package com.smart.task;

import com.smart.framework.commonpool.Task;

import java.util.concurrent.CyclicBarrier;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-06-20 15:41
 **/
public class JobWorker extends Thread{
    private Task task;
    private CyclicBarrier cyclicBarrier;

    public void init(Task task, CyclicBarrier cyclicBarrier) {
        this.task = task;
        this.cyclicBarrier = cyclicBarrier;
    }

    @Override
    public void run() {
        //super.run();
        try {
            task.process();
            cyclicBarrier.await();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
