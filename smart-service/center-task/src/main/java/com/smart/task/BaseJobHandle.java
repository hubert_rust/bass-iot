package com.smart.task;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.smart.entity.*;
import com.smart.repository.*;
import com.smart.toolkit.IDGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Predicate;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static com.smart.config.TaskConst.getSecretkey;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-06-28 10:36
 **/

@Component
@Scope("prototype")
public class BaseJobHandle {
    @Autowired
    private ConcentratorRepostory conRepostory;
    @Autowired
    private MeterRepostory meterRepostory;

    @Autowired
    private CronMainJobLogRepository mainJobLogRepository;

    @Autowired
    private CronJobLogRepository logRepository;

    @Autowired
    private PluginRepostory pluginRepostory;

    @Autowired
    private LockerOfflineKeyRepository keyRepository;

    public Map getPluginMap() {
        Map<String, PluginEntity> map = Maps.newHashMap();
        pluginRepostory.findAll().forEach(v-> {
            map.put(v.getPluginId(), v);
        });

        return map;
    }

    /**
     * @Description: 保留beforeDays天的数据
     * @Param:
     * @return:
     * @Author: admin
     * @Date: 2019/7/1
     */
    public void clearHistory(int beforeDays) {

        List<LockerOfflineKeyEntity> list = keyRepository.findAll((root, query, builder)->{
            return builder.lessThan(root.get("setTime"), LocalDate.now().minusDays(beforeDays).toString());
        });

        keyRepository.deleteAll(list);
    }
    public LockerOfflineKeyEntity secretKeySave(String commandOrder,
                                                String pluginId,
                                                String mainAddr,
                                                String subAddr,
                                                String setTime) {
        String secretKey = getSecretkey();

        LockerOfflineKeyEntity keyEntity = new LockerOfflineKeyEntity();
        keyEntity.setCommandOrder(IDGenerator.getIdStr());
        keyEntity.setPluginId(pluginId);
        keyEntity.setHubAddress(mainAddr);
        keyEntity.setSecretKey(String.valueOf(Long.parseLong(secretKey, 16)));
        keyEntity.setHexKey(secretKey);
        keyEntity.setSubAddress(subAddr);
        keyEntity.setCreateTime(LocalDateTime.now());
        //2019-06-20
        keyEntity.setSetTime(setTime);
        return keyRepository.save(keyEntity);
    }


    public void savaLog(CronJobLogEntity entity) {
        LocalDateTime lc = LocalDateTime.now();
        entity.setCreateTime(lc);
        entity.setFinishTime(lc);
        logRepository.save(entity);
    }
    public void saveMainLog(CronMainJobLogEntity entity) {
        entity.setCreateTime(LocalDateTime.now());
        mainJobLogRepository.save(entity);
    }

    public JobParam parseJobParam(String param) {
        if (Strings.isNullOrEmpty(param)) {
            return null;
        }
        return JSONObject.parseObject(param, JobParam.class);
    }

    public <T> T createEntity(Class<T> cls) {
        try {
            return  (T)cls.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<MeterEntiry> getSubHWListByHubList(String conAddr, String subAddr, String hubType) {
        List<MeterEntiry> meterList = null;
        if (Strings.isNullOrEmpty(subAddr)) {
            meterList = meterRepostory.findAll((root, query, builder) -> {
                Predicate p1 = builder.equal(root.get("hubAddress"), conAddr);
                Predicate p2 = builder.equal(root.get("meterType"), hubType);
                return builder.and(p1, p2);
            });
        }
        else {
            meterList = meterRepostory.findAll((root, query, builder) -> {
                Predicate p1 = builder.equal(root.get("hubAddress"), conAddr);
                Predicate p2 = builder.equal(root.get("meterAddress"), subAddr);
                Predicate p3 = builder.equal(root.get("hubType"), hubType);
                return builder.and(p1, p2);
            });
        }
        return meterList;
    }

    public List<ConcentratorEntity> getHubConfig(String hubAddr, String hubType) {
        if (Strings.isNullOrEmpty(hubAddr)) {
            return conRepostory.findAll((root, query, builder) -> {
                return builder.equal(root.get("hubType"), hubType);
            });
        }
        else {
            return conRepostory.findAll((root, query, builder) -> {
                Predicate p1 = builder.equal(root.get("hubAddress"), hubAddr);
                Predicate p2 = builder.equal(root.get("hubType"), hubType);
                return builder.and(p1, p2);
            });
        }
    }

    public List<MeterEntiry> getSubHWConfig(String subHwType) {
        return meterRepostory.findAll((root, query, builder) -> {
            return builder.equal(root.get("meterType"), subHwType);
        });
    }

    public Map<String, LockerOfflineKeyEntity> getTodaySecretKey(String pluginId,
                                                 String setTime) {
        List<LockerOfflineKeyEntity> keyList = keyRepository.findAll((root, query, builder)->{
            Predicate p1 = builder.equal(root.get("PluginId"), pluginId);
            Predicate p4 = builder.equal(root.get("setTime"), setTime);

            return builder.and(p1, p4);
        });

        //key: pluginId+hubAddress+conAddress
        Map<String, LockerOfflineKeyEntity> map = Maps.newHashMap();
        keyList.forEach(v->{
            map.put(v.getPluginId()+v.getHubAddress()+v.getSubAddress(), v);
        });
        return map;
    }

}
