package com.smart.task.locker;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.smart.command.CommandCode;
import com.smart.common.ResultConst;
import com.smart.entity.CronJobLogEntity;
import com.smart.entity.LockerOfflineKeyEntity;
import com.smart.entity.MeterEntiry;
import com.smart.framework.commonpool.Task;
import com.smart.framework.cons.BassConst;
import com.smart.framework.utils.HttpUtils;
import com.smart.repository.CronJobLogRepository;
import com.smart.repository.LockerOfflineKeyRepository;
import com.smart.repository.MeterRepostory;
import com.smart.request.BassHardwardRequest;
import com.smart.toolkit.IDGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.smart.config.TaskConst.*;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-06-20 15:44
 **/
@Component("updateLockerSecretKeyTask")
public class UpdateLockerSecretKeyTask extends Task {
    private static final Logger log = LoggerFactory.getLogger(UpdateLockerSecretKeyTask.class);

    private String jobHandleName;
    private String conAddress;
    private String subAddress;
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    private CronJobLogRepository jobLogRepository;
    @Autowired
    private MeterRepostory meterRepostory;
    @Autowired
    private LockerOfflineKeyRepository keyRepository;


    public String getConAddress() {
        return conAddress;
    }

    public void setConAddress(String conAddress) {
        this.conAddress = conAddress;
    }

    public String getSubAddress() {
        return subAddress;
    }

    public void setSubAddress(String subAddress) {
        this.subAddress = subAddress;
    }

    public String getJobHandleName() {
        return jobHandleName;
    }

    public void setJobHandleName(String jobHandleName) {
        this.jobHandleName = jobHandleName;
    }

    private List<Map<String, Object>> getPlugInConfig() {
        return jdbcTemplate.queryForList("select * from tb_hardware_plugin_manage");
    }


    private List<MeterEntiry> getLockerConfig() {
        List<MeterEntiry> meterList = null;
        //定时自动执行，不带参数
        if (Strings.isNullOrEmpty(subAddress)) {
            meterList = meterRepostory.findAll(new Specification<com.smart.entity.MeterEntiry>() {
                @Override
                public Predicate toPredicate(Root<com.smart.entity.MeterEntiry> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                    return criteriaBuilder.equal(root.get("hubAddress"), conAddress);
                }
            });
        } else {
            //收到执行，带参数
            meterList = meterRepostory.findAll(new Specification<com.smart.entity.MeterEntiry>() {
                @Override
                public Predicate toPredicate(Root<com.smart.entity.MeterEntiry> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                    Predicate p1 = criteriaBuilder.equal(root.get("hubAddress"), conAddress);
                    Predicate p2 = criteriaBuilder.equal(root.get("meterAddress"), subAddress);
                    return criteriaBuilder.and(p1, p2);
                }
            });
        }

        return meterList;
    }

    /**
     * @Description: 保存锁对应的秘钥
     * @Param:
     * @return:
     * @Author: admin
     * @Date: 2019/6/20
     */
    private LockerOfflineKeyEntity secretKeySave(String commandOrder,
                                                 String pluginId,
                                                 String mainAddr,
                                                 String subAddr) {
        String secretKey = getSecretkey();

        LockerOfflineKeyEntity keyEntity = new LockerOfflineKeyEntity();
        keyEntity.setCommandOrder(commandOrder);
        keyEntity.setPluginId(pluginId);
        keyEntity.setHubAddress(mainAddr);
        keyEntity.setSecretKey(String.valueOf(Long.parseLong(secretKey, 16)));
        keyEntity.setHexKey(secretKey);
        keyEntity.setSubAddress(subAddr);
        keyEntity.setCreateTime(LocalDateTime.now());
        //2019-06-20
        keyEntity.setSetTime(LocalDate.now().toString());
        return keyRepository.save(keyEntity);
    }

    private BassHardwardRequest getRequest(String commandOrder,
                                           String plugIn,
                                           String conAddr,
                                           String subAddr,
                                           String secretKey) {
        BassHardwardRequest request = null;
        request = new BassHardwardRequest();
        request.setServiceType(BassConst.BASS_HARDWARD_SERVICE_NAME_PLUGIN);
        request.setServiceId(plugIn);
        request.setCommandOrder(commandOrder);
        request.setHubAddress(conAddress);
        request.setSubAddress(subAddr);
        request.setCmdCode(CommandCode.CMD_LOCK_SET_OFFLINE_SECRETKEY);
        request.setCmdType(CommandCode.HUB_TYPE_LOCKER);
        request.setSubType(BassConst.BASS_HARDWARD_SERVICE_PLUGIN_SUBTYPE_FUNC);

        Map<String, Object> map = Maps.newHashMap();
        map.put("secretKey", secretKey);
        map.put("hubAddress", conAddr);
        map.put("meterAddress", subAddr);
        List<Map<String, Object>> list = Lists.newArrayList();
        list.add(map);
        request.setEntitys(list);

        return request;
    }

    @Override
    public void process() throws Exception {
        Map<String, String> pluginUrl = Maps.newHashMap();
        List<Map<String, Object>> pluginList = getPlugInConfig();
        pluginList.forEach(v -> {
            String plugin = v.get("plugin_id").toString();
            String url = v.get("plugin_url").toString();
            pluginUrl.put(plugin, url);
        });

        List<MeterEntiry> subList = getLockerConfig();
        for (int i = 0; i < subList.size(); i++) {
            String conAddr = subList.get(i).getHubAddress();
            String subAddr = subList.get(i).getMeterAddress();
            String pluginId = subList.get(i).getPluginId();
            String hubType = subList.get(i).getMeterType();

            String commandOrder = IDGenerator.getIdStr();

            try {
                TimeUnit.MILLISECONDS.sleep(200);

                //先在数据库保存密钥
                LockerOfflineKeyEntity keyEntity = null;
                try {
                    keyEntity = secretKeySave(commandOrder, pluginId, this.conAddress, subList.get(i).getMeterAddress());
                } catch (Exception e) {
                    cronJobLogSave(commandOrder, LocalDateTime.now(), conAddr, subAddr, hubType,"FAIL",
                            "数据库保存密钥失败: conAddr=" + conAddr + ", subAddr=" + subAddr);
                    e.printStackTrace();
                    continue;
                }

                BassHardwardRequest hardwardRequest = getRequest(commandOrder, subList.get(i).getPluginId(),
                        conAddr, subAddr, keyEntity.getHexKey());

                String url = pluginUrl.get(subList.get(i).getPluginId());
                LocalDateTime startTime = LocalDateTime.now();

                long startTm = System.currentTimeMillis();
                String resp = null;
                try {
                    resp = (String) HttpUtils.commonSendHttpPost(url, JSONObject.toJSONString(hardwardRequest), 1000);
                } catch (Exception e) {
                    keyEntity.setSendState("FAIL");
                    keyRepository.save(keyEntity);
                    cronJobLogSave(commandOrder, startTime, conAddr, subAddr, hubType,"FAIL", e.getMessage());
                    e.printStackTrace();
                    continue;
                }

                long timeSpan = System.currentTimeMillis() - startTm;
                Map<String, Object> respMap = JSONObject.parseObject(resp, Map.class);
                ResultConst.ReturnObject retObj = ResultConst.checkResult(respMap);

                keyEntity.setTimeSpan(timeSpan);
                keyEntity.setSendState(retObj.getCode());
                keyRepository.save(keyEntity);
                cronJobLogSave(commandOrder, startTime, conAddr, subAddr, hubType,
                        retObj.getCode(), retObj.getMessage());

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                log.info(">>> finally, process");
            }
        }
    }



    //定时任务执行日志保存
    private void cronJobLogSave(String commandOrder,
                                LocalDateTime startTime,
                                String conAddr,
                                String subAddr,
                                String hubType,
                                String result,
                                String remark) {
        try {
            CronJobLogEntity cronJobLogEntity = CronJobLogEntity.builder()
                    .startTime(startTime)
                    .finishTime(LocalDateTime.now())
                    .hubAddress(conAddr)
                    .subAddress(subAddr)
                    .jobHandle(jobHandleName)
                    .hubType(hubType)
                    .result(result)
                    .build();
            cronJobLogEntity.setHubType(hubType);
            cronJobLogEntity.setResult(result);
            cronJobLogEntity.setRemark(remark);
            jobLogRepository.save(cronJobLogEntity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        int curHour = 18;
        int day = 29;
        long secretKey = Long.parseLong(getSecretkey(), 16);

        int cpu = Runtime.getRuntime().availableProcessors();

        LocalDateTime var1 = LocalDateTime.of(2019, 06, day, curHour,00, 00 );
        LocalDateTime var2 = LocalDateTime.of(2019, 06, day, curHour,59, 59 );
        LocalDateTime var3 = LocalDateTime.of(2019, 06, day, curHour,30, 0 );
        LocalDateTime var4 = LocalDateTime.of(2019, 06, day, curHour,45, 0 );


        LocalDateTime var5 = LocalDateTime.of(2019, 06, day, curHour+1,00, 00);
        long v5 = var5.toInstant(ZoneOffset.of("+8")).getEpochSecond();
        long cur = System.currentTimeMillis()/1000;
        long end = cur + 600;

        /*String p1 = createPassword(var1.toInstant(ZoneOffset.of("+8")).getEpochSecond(),
                var2.toInstant(ZoneOffset.of("+8")).getEpochSecond(), secretKey);
        String p2 = createPassword(var2.toInstant(ZoneOffset.of("+8")).getEpochSecond(),
                var3.toInstant(ZoneOffset.of("+8")).getEpochSecond(), secretKey);
        String p3 = createPassword(var3.toInstant(ZoneOffset.of("+8")).getEpochSecond(),
                var4.toInstant(ZoneOffset.of("+8")).getEpochSecond(), secretKey);
        String p4 = createPassword(var4.toInstant(ZoneOffset.of("+8")).getEpochSecond(),
                var5.toInstant(ZoneOffset.of("+8")).getEpochSecond(), secretKey);
*/

        //String p1 = createPasswordOneHour(var1.toInstant(ZoneOffset.of("+8")).getEpochSecond(),
        //        v5, secretKey);


        secretKey = 1429108871L;
        System.out.println("secretkey: " + secretKey);
        //String p1 = createPasswordOneHour(17,18,   secretKey);
        String p1 = createPasswordFifteenMin((17*60+0)/15, (17*60+15)/15 ,secretKey);
        System.out.println("password: " + p1);
        System.out.println("2019-06-28 "+ curHour + ":00:00  pwd: " + p1);
/*        System.out.println("2019-06-28 "+ curHour + ":15:00  pwd: " + p2);
        System.out.println("2019-06-28 "+ curHour + ":30:00  pwd: " + p3);
        System.out.println("2019-06-28 "+ curHour + ":45:00  pwd: " + p4);*/

        System.out.println(LocalDateTime.now().plusDays(5).toString());

        Decrypt(Long.valueOf(p1), secretKey);

    }
}
