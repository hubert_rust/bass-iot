package com.smart.task.locker;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.smart.command.CommandCode;
import com.smart.common.ResultConst;
import com.smart.config.TaskConst;
import com.smart.entity.*;
import com.smart.framework.cons.BassConst;
import com.smart.framework.utils.CommonApplication;
import com.smart.framework.utils.HttpUtils;
import com.smart.repository.ConcentratorRepostory;
import com.smart.request.BassHardwardRequest;
import com.smart.task.BaseJobHandle;
import com.smart.task.JobWorker;
import com.smart.toolkit.IDGenerator;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

/**
 * @program: smart-service
 * @description: 同步时间
 * @author: admin
 * @create: 2019-06-20 14:36
 **/
@JobHandler(value = "syncTimeJobHandle")
@Component
public class SyncTimeJobHandle extends IJobHandler {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private ConcentratorRepostory conRepostory;
    @Autowired
    private BaseJobHandle baseJobHandle;

    public static Map<String, PluginEntity> pluginMap = Maps.newHashMap();


    /**
     * @Description: {conAddr:"", subAddr: ""}
     * @Param:
     * @return:
     * @Author: admin
     * @Date: 2019/6/20
     */
    private void withParam(String param) throws Exception {

        CyclicBarrier cb = new CyclicBarrier(2);
        Map<String, Object> map = new Gson().fromJson(param, Map.class);

        UpdateLockerSecretKeyTask task = CommonApplication.getBean("updateLockerSecretKeyTask");
        task.setConAddress(map.get("conAddr").toString());
        task.setSubAddress(map.get("subAddr").toString());
        task.setJobHandleName("updateLockerSecretKey");
        JobWorker jobWorker = new JobWorker();
        jobWorker.init(task, cb);
        jobWorker.start();

        //在主线程等待
        cb.await();
    }

    private void syncJobExecute(JobParam jobParam) {
        String pluginId = "";
        String dcName = "";
        String dcId = "";
        List<MeterEntiry> subList = baseJobHandle.getSubHWConfig(CommandCode.HUB_TYPE_LOCKER);
        if (subList.size() > 0) {
            pluginId = subList.get(0).getPluginId();
            dcName = subList.get(0).getDcName();
            dcId = subList.get(0).getDcId();
        }

        List<MeterEntiry> executeList = null;
        if (jobParam == null
                || jobParam.getDatas() == null
                || jobParam.getDatas().size() <= 0) {
            //全量执行
            executeList = subList;
        } else {
            //根据参数执行
            executeList = Lists.newArrayList();
            for (JobParam.RunParam v : jobParam.getDatas()) {
                String hubAddr = v.getHubAddr();
                String subAddr = v.getSubAddr();
                if (Strings.isNullOrEmpty(hubAddr)) {
                    CronMainJobLogEntity mainEnity = baseJobHandle.createEntity(CronMainJobLogEntity.class);
                    mainEnity.setDcId(dcId);
                    mainEnity.setDcId(dcName);
                    mainEnity.setJobHandle("SyncTimeJobHandle");
                    mainEnity.setState(TaskConst.JOB_MAIN_LOG_STATE_PREPARE);
                    baseJobHandle.saveMainLog(mainEnity);
                    continue;
                }
                List<MeterEntiry> retList = baseJobHandle.getSubHWListByHubList(hubAddr, subAddr, CommandCode.HUB_TYPE_LOCKER);
                executeList.addAll(retList);
            }
        }

        //
        executeCommand(executeList);
    }

    private void executeCommand(List<MeterEntiry> meterList) {
        String mainOrder = IDGenerator.getIdStr();
        LocalDateTime startTm = LocalDateTime.now();
        CronMainJobLogEntity entity = baseJobHandle.createEntity(CronMainJobLogEntity.class);
        entity.setMainOrder(mainOrder);
        entity.setJobHandle("syncTimeJobHandle");
        entity.setState(TaskConst.JOB_MAIN_LOG_STATE_FINISH);
        entity.setStartTime(startTm);

        List<ConcentratorEntity> conList = baseJobHandle.getHubConfig(null, CommandCode.HUB_TYPE_LOCKER);
        for (ConcentratorEntity val: conList) {
            String commandOrder = IDGenerator.getIdStr();
            try { TimeUnit.MILLISECONDS.sleep(100); }
            catch (InterruptedException e) { e.printStackTrace(); }
            try {
                MeterEntiry meterEntiry = new MeterEntiry();
                meterEntiry.setPluginId(val.getPluginId());
                meterEntiry.setDcId(val.getDcId());
                meterEntiry.setDcName(val.getDcName());
                meterEntiry.setHubAddress(val.getHubAddress());
                meterEntiry.setMeterAddress("00");
                //先同步集中器
                syncTimeProcess(entity, meterEntiry, mainOrder, commandOrder);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        for (MeterEntiry val: meterList) {
            String commandOrder = IDGenerator.getIdStr();
            try { TimeUnit.MILLISECONDS.sleep(100); }
            catch (InterruptedException e) { e.printStackTrace(); }
            try {
                syncTimeProcess(entity, val, mainOrder, commandOrder);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        entity.setFinishTime(LocalDateTime.now());
        baseJobHandle.saveMainLog(entity);
    }

    private void syncTimeProcess(CronMainJobLogEntity entity,
                                 MeterEntiry val,
                                 String mainOrder,
                                 String commandOrder) throws Exception {

        BassHardwardRequest request = getRequest(commandOrder, val.getPluginId(),
                val.getHubAddress(), val.getMeterAddress());

        String url = pluginMap.get(val.getPluginId()).getPluginUrl();

        LocalDateTime startTime = LocalDateTime.now();

        long start = System.currentTimeMillis();
        String resp = null;
        CronJobLogEntity jobLogEntity = CronJobLogEntity.builder()
                .startTime(startTime)
                .mainOrder(mainOrder)
                .commandOrder(commandOrder)
                .PluginId(val.getPluginId())
                .hubType(CommandCode.HUB_TYPE_LOCKER)
                .hubAddress(val.getHubAddress())
                .subAddress(val.getMeterAddress())
                .result(ResultConst.FAIL)
                .jobHandle("SyncTimeJobHandle")
                .build();
        jobLogEntity.setDcId(val.getDcId());
        jobLogEntity.setDcName(val.getDcName());

        entity.setDcId(val.getDcId());
        entity.setDcName(val.getDcName());
        try {
            resp = (String) HttpUtils.commonSendHttpPost(url, JSONObject.toJSONString(request), 10000);
            Map<String, Object> respMap = JSONObject.parseObject(resp, Map.class);
            ResultConst.ReturnObject retObj = ResultConst.checkResult(respMap);
            jobLogEntity.setResult(retObj.getCode());
            jobLogEntity.setRemark(retObj.getMessage());
            baseJobHandle.savaLog(jobLogEntity);
        } catch (Exception e) {
            jobLogEntity.setRemark(e.getMessage());
            baseJobHandle.savaLog(jobLogEntity);
            e.printStackTrace();
        }
    }
    private BassHardwardRequest getRequest(String commandOrder,
                                           String plugIn,
                                           String conAddr,
                                           String subAddr) {
        BassHardwardRequest request = null;
        request = new BassHardwardRequest();
        request.setServiceType(BassConst.BASS_HARDWARD_SERVICE_NAME_PLUGIN);
        request.setServiceId(plugIn);
        request.setCommandOrder(commandOrder);
        request.setHubAddress(conAddr);
        request.setSubAddress(subAddr);
        request.setCmdCode(CommandCode.CMD_LOCK_TIME_SYNC);
        request.setCmdType(CommandCode.HUB_TYPE_LOCKER);
        request.setSubType(BassConst.BASS_HARDWARD_SERVICE_PLUGIN_SUBTYPE_FUNC);
        request.setIsRetRespBytes(1);
        request.setIsRetSendBytes(1);

        Map<String, Object> map = Maps.newHashMap();
        map.put("hubAddress", conAddr);
        map.put("meterAddress", subAddr);
        List<Map<String, Object>> list = Lists.newArrayList();
        list.add(map);
        request.setEntitys(list);

        return request;
    }

    private void asyncJobExecute() {

    }

    /**
    * @Description:
    * @Param:  {"runMode":"sync或者async", [{"hubAddr": "", "subAddr":""}]}
    * @return:
    * @Author: admin
    * @Date: 2019/7/1
    */
    @Override
    public ReturnT<String> execute(String param) throws Exception {
        XxlJobLogger.log(">>> SyncTimeJobHandle, execute");

        JobParam jobParam = null;
        try {
            jobParam = baseJobHandle.parseJobParam(param);
        } catch (Exception e) {
            e.printStackTrace();
            return ReturnT.FAIL;
        }

        //初始化
        pluginMap.putAll(baseJobHandle.getPluginMap());

        if (jobParam == null || jobParam.getRunMode().equals(TaskConst.JOB_RUN_MODE_SYNC)) {
            syncJobExecute(jobParam);
        } else if (jobParam.getRunMode().equals(TaskConst.JOB_RUN_MODE_ASYNC)) {
            //暂时不需要, 门锁比较少
            asyncJobExecute();
        } else {
            XxlJobLogger.log(">>> SyncTimeJobHandle, invalid runMode: {}",
                    jobParam.getRunMode());
        }

        return ReturnT.SUCCESS;
    }
}
