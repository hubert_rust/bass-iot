package com.smart.service;

import com.smart.entity.MeterEntiry;
import com.smart.service.BaseServiceImpl;
import org.springframework.stereotype.Component;

/**
 * @program: smart-service
 * @description: meter
 * @author: Admin
 * @create: 2019-04-22 19:25
 **/

@Component("MeterHardware")
public class MeterHardware extends BaseServiceImpl<MeterEntiry>  {

}