package com.smart.service;

import com.smart.entity.ConcentratorEntity;
import com.smart.entity.MeterEntiry;
import com.smart.service.BaseServiceImpl;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @program: smart-service
 * @description: meter
 * @author: Admin
 * @create: 2019-04-22 19:25
 **/

@Component("HubHardware")
public class HubHardware extends BaseServiceImpl<ConcentratorEntity>  {
    public static void main(String[] args) {
        String totalFee = new BigDecimal("0.30").multiply(new BigDecimal(100)).setScale(0, RoundingMode.HALF_UP).toString();
        System.out.println();
    }
}