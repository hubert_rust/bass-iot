package com.smart.entity;

import com.smart.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @program: smart-service
 * @description: 集中器
 * @author: Admin
 * @create: 2019-04-22 15:28
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name="tb_hardware_hub")
public class ConcentratorEntity extends BaseEntity<Long> {
    private static final long serialVersionUID = 1L;
/*    @JsonSerialize(using= ToStringSerializer.class)
    @Column(name="id")
    private Long id;*/

    @Column(name="plugin_id")
    private String pluginId;

    @Column(name="plugin_name")
    private String pluginName;
    @Column(name="hub_name")
    private String hubName;

    @Column(name="hub_address")
    private String hubAddress;

    @Column(name="hub_type")
    private String hubType;
    @Column(name="hub_mode")
    private String hubMode;

    @Column(name="hub_ip")
    private String hubIp;

    @Column(name="hub_state")
    private String hubState = "active";

    @Column(name="run_state")
    private String runState = "nok";

    @Column(name="comm_port")
    private int commPort;

    @Column(name="bps")
    private int bps;

    @Column(name="hub_param")
    private String hubParam;

    public String getHubMode() { return hubMode; }
    public void setHubMode(String hubMode) { this.hubMode = hubMode; }

    public String getHubAddress() {
        return hubAddress;
    }

    public void setHubAddress(String hubAddress) {
        this.hubAddress = hubAddress;
    }

    public String getHubType() {
        return hubType;
    }

    public void setHubType(String hubType) {
        this.hubType = hubType;
    }

    public String getHubIp() {
        return hubIp;
    }

    public void setHubIp(String hubIp) {
        this.hubIp = hubIp;
    }

    public String getHubState() {
        return hubState;
    }

    public void setHubState(String hubState) {
        this.hubState = hubState;
    }

    public String getRunState() {
        return runState;
    }

    public void setRunState(String runState) {
        this.runState = runState;
    }

    public int getCommPort() {
        return commPort;
    }

    public void setCommPort(int commPort) {
        this.commPort = commPort;
    }

    public int getBps() {
        return bps;
    }

    public void setBps(int bps) {
        this.bps = bps;
    }

    public String getHubName() {
        return hubName;
    }

    public void setHubName(String hubName) {
        this.hubName = hubName;
    }

    public String getPluginId() {
        return pluginId;
    }

    public void setPluginId(String pluginId) {
        this.pluginId = pluginId;
    }

    public String getPluginName() {
        return pluginName;
    }

    public void setPluginName(String pluginName) { this.pluginName = pluginName; }

    public String getHubParam() { return hubParam; }
    public void setHubParam(String hubParam) { this.hubParam = hubParam; }
}