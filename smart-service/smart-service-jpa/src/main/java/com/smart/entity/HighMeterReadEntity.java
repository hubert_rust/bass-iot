package com.smart.entity;

import com.smart.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @program: smart-service
 * @description: meter
 * @author: Admin
 * @create: 2019-04-23 18:42
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name="tb_hardware_read_high_meter")
public class HighMeterReadEntity extends BaseEntity<Long> {
    private static final long serialVersionUID = 1L;
/*    @JsonSerialize(using= ToStringSerializer.class)
    @Column(name="id")
    private Long id;*/

    @Column(name="main_order")
    private String mainOrder;
    @Column(name="command_order")
    private String commandOrder;
    @Column(name="meter_no")
    private String meterNo;
    @Column(name="start_time")
    private LocalDateTime startTime;
    @Column(name="finish_time")
    private LocalDateTime finishTime;
    @Column(name="plugin_id")
    private String pluginId;
    @Column(name="hub_address")
    private String hubAddress;
    @Column(name="meter_address")
    private String meterAddress;
    @Column(name="all_recharge_amount")
    private String allRechargeAmount;
    @Column(name="all_use_amount")
    private String allUseAmount;
    @Column(name="left_amount")
    private String leftAmount;
    @Column(name="recharge_times")
    private String rechargeTimes;
    @Column(name="last_recharge_date")
    private String lastRechargeDate;
    @Column(name="last_recharge_amount")
    private String lastRechargeAmount;
    @Column(name="meter_status")
    private String meterStatus;
    @Column(name="read_type")
    private String readType;
    @Column(name="read_status")
    private String readStatus;

    public String getMainOrder() {
        return mainOrder;
    }

    public void setMainOrder(String mainOrder) {
        this.mainOrder = mainOrder;
    }

    public String getCommandOrder() {
        return commandOrder;
    }

    public void setCommandOrder(String commandOrder) {
        this.commandOrder = commandOrder;
    }

    public String getMeterNo() {
        return meterNo;
    }

    public void setMeterNo(String meterNo) {
        this.meterNo = meterNo;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(LocalDateTime finishTime) {
        this.finishTime = finishTime;
    }

    public String getPluginId() { return pluginId; }
    public void setPluginId(String pluginId) { this.pluginId = pluginId; }

    public String getHubAddress() {
        return hubAddress;
    }

    public void setHubAddress(String hubAddress) {
        this.hubAddress = hubAddress;
    }

    public String getMeterAddress() {
        return meterAddress;
    }

    public void setMeterAddress(String meterAddress) {
        this.meterAddress = meterAddress;
    }

    public String getAllRechargeAmount() {
        return allRechargeAmount;
    }

    public void setAllRechargeAmount(String allRechargeAmount) {
        this.allRechargeAmount = allRechargeAmount;
    }

    public String getAllUseAmount() {
        return allUseAmount;
    }

    public void setAllUseAmount(String allUseAmount) {
        this.allUseAmount = allUseAmount;
    }

    public String getLeftAmount() {
        return leftAmount;
    }

    public void setLeftAmount(String leftAmount) {
        this.leftAmount = leftAmount;
    }

    public String getRechargeTimes() {
        return rechargeTimes;
    }

    public void setRechargeTimes(String rechargeTimes) {
        this.rechargeTimes = rechargeTimes;
    }

    public String getLastRechargeDate() {
        return lastRechargeDate;
    }

    public void setLastRechargeDate(String lastRechargeDate) {
        this.lastRechargeDate = lastRechargeDate;
    }

    public String getLastRechargeAmount() {
        return lastRechargeAmount;
    }

    public void setLastRechargeAmount(String lastRechargeAmount) {
        this.lastRechargeAmount = lastRechargeAmount;
    }

    public String getMeterStatus() {
        return meterStatus;
    }

    public void setMeterStatus(String meterStatus) {
        this.meterStatus = meterStatus;
    }

    public String getReadType() {
        return readType;
    }

    public void setReadType(String readType) {
        this.readType = readType;
    }

    public String getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(String readStatus) {
        this.readStatus = readStatus;
    }
}