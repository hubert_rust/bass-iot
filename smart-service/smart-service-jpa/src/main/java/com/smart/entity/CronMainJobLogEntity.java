package com.smart.entity;

import com.smart.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * @program: smart-service
 * @description: 集中器
 * @author: Admin
 * @create: 2019-04-22 15:28
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name="tb_hardware_main_job_log")
public class CronMainJobLogEntity extends BaseEntity<Long> {
    private static final long serialVersionUID = 1L;
/*    @JsonSerialize(using= ToStringtb_hardware_monitor_paramSerializer.class)
    @Column(name="id")
    private Long id;*/

    @Column(name="main_order")
    private String mainOrder;
    @Column(name="state")
    private String state;

    @Column(name="job_handle")
    private String jobHandle;
    @Column(name="start_time")
    private LocalDateTime startTime;
    @Column(name="finish_time")
    private LocalDateTime finishTime;

    public String getMainOrder() {
        return mainOrder;
    }

    public void setMainOrder(String mainOrder) {
        this.mainOrder = mainOrder;
    }

    public String getJobHandle() {
        return jobHandle;
    }

    public void setJobHandle(String jobHandle) {
        this.jobHandle = jobHandle;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(LocalDateTime finishTime) {
        this.finishTime = finishTime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}