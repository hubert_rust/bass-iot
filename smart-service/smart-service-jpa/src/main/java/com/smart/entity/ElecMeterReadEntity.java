package com.smart.entity;

import com.smart.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @program: smart-service
 * @description: meter
 * @author: Admin
 * @create: 2019-04-23 18:42
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name="tb_hardware_read_elec_meter")
public class ElecMeterReadEntity extends BaseEntity<Long> {
    private static final long serialVersionUID = 1L;
/*    @JsonSerialize(using= ToStringSerializer.class)
    @Column(name="id")
    private Long id;*/

    @Column(name="main_order")
    private String mainOrder;
    @Column(name="command_order")
    private String commandOrder;
    @Column(name="meter_no")
    private String meterNo;
    @Column(name="start_time")
    private LocalDateTime startTime;
    @Column(name="finish_time")
    private LocalDateTime finishTime;
    @Column(name="hub_address")
    private String hubAddress;
    @Column(name="meter_address")
    private String meterAddress;
    @Column(name="first_degree")
    private String firstDegree;
    @Column(name="second_degree")
    private String secondDegree;
    @Column(name="price")
    private String price;
    @Column(name="current")
    private String current;
    @Column(name="voltage")
    private String voltage;
    @Column(name="active_degree")
    private String activeDegree;
    @Column(name="active_power")
    private String activePower;
    @Column(name="reactive_power")
    private String reactivePower;
    @Column(name="read_type")
    private String readType;
    @Column(name="read_status")
    private String readStatus;

    public String getHubAddress() {
        return hubAddress;
    }

    public void setHubAddress(String hubAddress) {
        this.hubAddress = hubAddress;
    }

    public String getMeterAddress() {
        return meterAddress;
    }

    public void setMeterAddress(String meterAddress) {
        this.meterAddress = meterAddress;
    }

    public String getFirstDegree() {
        return firstDegree;
    }

    public void setFirstDegree(String firstDegree) {
        this.firstDegree = firstDegree;
    }

    public String getSecondDegree() {
        return secondDegree;
    }

    public void setSecondDegree(String secondDegree) {
        this.secondDegree = secondDegree;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCurrent() {
        return current;
    }

    public void setCurrent(String current) {
        this.current = current;
    }

    public String getVoltage() {
        return voltage;
    }

    public void setVoltage(String voltage) {
        this.voltage = voltage;
    }

    public String getActiveDegree() {
        return activeDegree;
    }

    public void setActiveDegree(String activeDegree) {
        this.activeDegree = activeDegree;
    }

    public String getActivePower() {
        return activePower;
    }

    public void setActivePower(String activePower) {
        this.activePower = activePower;
    }

    public String getReactivePower() {
        return reactivePower;
    }

    public void setReactivePower(String reactivePower) {
        this.reactivePower = reactivePower;
    }

    public String getMainOrder() { return mainOrder; }
    public void setMainOrder(String mainOrder) { this.mainOrder = mainOrder; }
    public String getCommandOrder() { return commandOrder; }
    public void setCommandOrder(String commandOrder) { this.commandOrder = commandOrder; }
    public String getMeterNo() { return meterNo; }
    public void setMeterNo(String meterNo) { this.meterNo = meterNo; }
    public LocalDateTime getStartTime() { return startTime; }
    public void setStartTime(LocalDateTime startTime) { this.startTime = startTime; }
    public LocalDateTime getFinishTime() { return finishTime; }
    public void setFinishTime(LocalDateTime finishTime) { this.finishTime = finishTime; }
    public String getReadType() { return readType; }
    public void setReadType(String readType) { this.readType = readType; }
    public String getReadStatus() { return readStatus; }
    public void setReadStatus(String readStatus) { this.readStatus = readStatus; }
}