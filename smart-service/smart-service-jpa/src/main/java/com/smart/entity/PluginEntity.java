package com.smart.entity;

import com.smart.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @program: smart-service
 * @description: meter
 * @author: Admin
 * @create: 2019-04-23 18:42
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name="tb_hardware_plugin_manage")
public class PluginEntity extends BaseEntity<Long> {
    private static final long serialVersionUID = 1L;
/*    @JsonSerialize(using= ToStringSerializer.class)
    @Column(name="id")
    private Long id;*/

    @Column(name="dc_flag")
    private String dcFlag;
    @Column(name="plugin_id")
    private String pluginId;
    @Column(name="plugin_name")
    private String pluginName;
    @Column(name="plugin_mode")
    private String pluginMode;

    @Column(name="plugin_ip")
    private String pluginIp;
    @Column(name="plugin_port")
    private String pluginPort;
    @Column(name="plugin_url")
    private String pluginUrl;
    @Column(name="plugin_config")
    private String pluginConfig;
    @Column(name="supported")
    private int supported;
    @Column(name="status")
    private String status;

    public String getDcFlag() { return dcFlag; }
    public void setDcFlag(String dcFlag) { this.dcFlag = dcFlag; }
    public String getPluginId() { return pluginId; }
    public void setPluginId(String pluginId) { this.pluginId = pluginId; }
    public String getPluginName() { return pluginName; }
    public void setPluginName(String pluginName) { this.pluginName = pluginName; }

    public String getPluginMode() { return pluginMode; }
    public void setPluginMode(String pluginMode) { this.pluginMode = pluginMode; }

    public String getPluginIp() { return pluginIp; }
    public void setPluginIp(String pluginIp) { this.pluginIp = pluginIp; }
    public String getPluginPort() { return pluginPort; }
    public void setPluginPort(String pluginPort) { this.pluginPort = pluginPort; }
    public String getPluginUrl() { return pluginUrl; }
    public void setPluginUrl(String pluginUrl) { this.pluginUrl = pluginUrl; }
    public String getPluginConfig() { return pluginConfig; }
    public void setPluginConfig(String pluginConfig) { this.pluginConfig = pluginConfig; }
    public int getSupported() { return supported; }
    public void setSupported(int supported) { this.supported = supported; }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}