package com.smart.entity;

import com.smart.base.BaseEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * @program: smart-service
 * @description: 集中器
 * @author: Admin
 * @create: 2019-04-22 15:28
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name="tb_hardware_job_log")
public class CronJobLogEntity extends BaseEntity<Long> {
    private static final long serialVersionUID = 1L;
/*    @JsonSerialize(using= ToStringtb_hardware_monitor_paramSerializer.class)
    @Column(name="id")
    private Long id;*/

    @Column(name="plugin_id")
    private String PluginId;

    @Column(name="main_order")
    private String mainOrder;
    @Column(name="command_order")
    private String commandOrder;
    @Column(name="job_handle")
    private String jobHandle;
    @Column(name="start_time")
    private LocalDateTime startTime;
    @Column(name="finish_time")
    private LocalDateTime finishTime;
    @Column(name="hub_address")
    private String hubAddress;
    @Column(name="sub_address")
    private String subAddress;
    @Column(name="hub_type")
    private String hubType;
    @Column(name="result")
    private String result;
    @Column(name="retry_times")
    private int retryTimes;

    public CronJobLogEntity() {
    }

    public String getPluginId() {
        return PluginId;
    }

    public void setPluginId(String pluginId) {
        PluginId = pluginId;
    }

    public String getMainOrder() {
        return mainOrder;
    }

    public void setMainOrder(String mainOrder) {
        this.mainOrder = mainOrder;
    }

    public String getCommandOrder() {
        return commandOrder;
    }

    public void setCommandOrder(String commandOrder) {
        this.commandOrder = commandOrder;
    }

    public String getJobHandle() {
        return jobHandle;
    }

    public void setJobHandle(String jobHandle) {
        this.jobHandle = jobHandle;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(LocalDateTime finishTime) {
        this.finishTime = finishTime;
    }

    public String getHubAddress() {
        return hubAddress;
    }

    public void setHubAddress(String hubAddress) {
        this.hubAddress = hubAddress;
    }

    public String getSubAddress() {
        return subAddress;
    }

    public void setSubAddress(String subAddress) {
        this.subAddress = subAddress;
    }

    public String getHubType() {
        return hubType;
    }

    public void setHubType(String hubType) {
        this.hubType = hubType;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getRetryTimes() {
        return retryTimes;
    }

    public void setRetryTimes(int retryTimes) {
        this.retryTimes = retryTimes;
    }

    public static CronJobLogEntityBuilder builder() {
        return new CronJobLogEntityBuilder();
    }


    public static final class CronJobLogEntityBuilder {
        protected Long version = 1L;
        protected String remark;
        protected LocalDateTime createTime;
        protected LocalDateTime modifyTime;
        protected String createUser;
        protected String modifyUser;
        private String PluginId;
        private String mainOrder;
        private String commandOrder;
        private String jobHandle;
        private LocalDateTime startTime;
        private LocalDateTime finishTime;
        private String hubAddress;
        private String subAddress;
        private String hubType;
        private String result;
        private String dcName;
        private String dcId;
        private int retryTimes;

        private CronJobLogEntityBuilder() {
        }

        public static CronJobLogEntityBuilder CronJobLogEntity() {
            return new CronJobLogEntityBuilder();
        }

        public CronJobLogEntityBuilder PluginId(String PluginId) {
            this.PluginId = PluginId;
            return this;
        }

        public CronJobLogEntityBuilder mainOrder(String mainOrder) {
            this.mainOrder = mainOrder;
            return this;
        }

        public CronJobLogEntityBuilder commandOrder(String commandOrder) {
            this.commandOrder = commandOrder;
            return this;
        }

        public CronJobLogEntityBuilder jobHandle(String jobHandle) {
            this.jobHandle = jobHandle;
            return this;
        }

        public CronJobLogEntityBuilder startTime(LocalDateTime startTime) {
            this.startTime = startTime;
            return this;
        }

        public CronJobLogEntityBuilder finishTime(LocalDateTime finishTime) {
            this.finishTime = finishTime;
            return this;
        }

        public CronJobLogEntityBuilder hubAddress(String hubAddress) {
            this.hubAddress = hubAddress;
            return this;
        }

        public CronJobLogEntityBuilder subAddress(String subAddress) {
            this.subAddress = subAddress;
            return this;
        }

        public CronJobLogEntityBuilder hubType(String hubType) {
            this.hubType = hubType;
            return this;
        }

        public CronJobLogEntityBuilder result(String result) {
            this.result = result;
            return this;
        }

        public CronJobLogEntityBuilder dcName(String dcName) {
            this.dcName = dcName;
            return this;
        }

        public CronJobLogEntityBuilder dcId(String dcId) {
            this.dcId = dcId;
            return this;
        }

        public CronJobLogEntityBuilder retryTimes(int retryTimes) {
            this.retryTimes = retryTimes;
            return this;
        }

        public CronJobLogEntityBuilder version(Long version) {
            this.version = version;
            return this;
        }

        public CronJobLogEntityBuilder remark(String remark) {
            this.remark = remark;
            return this;
        }

        public CronJobLogEntityBuilder createTime(LocalDateTime createTime) {
            this.createTime = createTime;
            return this;
        }

        public CronJobLogEntityBuilder modifyTime(LocalDateTime modifyTime) {
            this.modifyTime = modifyTime;
            return this;
        }

        public CronJobLogEntityBuilder createUser(String createUser) {
            this.createUser = createUser;
            return this;
        }

        public CronJobLogEntityBuilder modifyUser(String modifyUser) {
            this.modifyUser = modifyUser;
            return this;
        }

        public CronJobLogEntity build() {
            CronJobLogEntity cronJobLogEntity = new CronJobLogEntity();
            cronJobLogEntity.setPluginId(PluginId);
            cronJobLogEntity.setMainOrder(mainOrder);
            cronJobLogEntity.setCommandOrder(commandOrder);
            cronJobLogEntity.setJobHandle(jobHandle);
            cronJobLogEntity.setStartTime(startTime);
            cronJobLogEntity.setFinishTime(finishTime);
            cronJobLogEntity.setHubAddress(hubAddress);
            cronJobLogEntity.setSubAddress(subAddress);
            cronJobLogEntity.setHubType(hubType);
            cronJobLogEntity.setResult(result);
            cronJobLogEntity.setDcName(dcName);
            cronJobLogEntity.setDcId(dcId);
            cronJobLogEntity.setRetryTimes(retryTimes);
            cronJobLogEntity.setVersion(version);
            cronJobLogEntity.setRemark(remark);
            cronJobLogEntity.setCreateTime(createTime);
            cronJobLogEntity.setModifyTime(modifyTime);
            cronJobLogEntity.setCreateUser(createUser);
            cronJobLogEntity.setModifyUser(modifyUser);
            return cronJobLogEntity;
        }
    }
}