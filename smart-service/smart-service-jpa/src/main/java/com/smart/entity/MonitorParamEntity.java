package com.smart.entity;

import com.smart.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @program: smart-service
 * @description: 集中器
 * @author: Admin
 * @create: 2019-04-22 15:28
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name="tb_hardware_monitor_param")
public class MonitorParamEntity extends BaseEntity<Long> {
    private static final long serialVersionUID = 1L;
/*    @JsonSerialize(using= ToStringtb_hardware_monitor_paramSerializer.class)
    @Column(name="id")
    private Long id;*/

    @Column(name="mainClass")
    private String mainClass;
    @Column(name="monitor_type")
    private String monitorType;
    @Column(name="probe_time")
    private long probeTime;

    @Column(name="probe_timeout")
    private long probeTimeout;

    public String getMainClass() {
        return mainClass;
    }

    public void setMainClass(String mainClass) {
        this.mainClass = mainClass;
    }

    public String getMonitorType() {
        return monitorType;
    }

    public void setMonitorType(String monitorType) {
        this.monitorType = monitorType;
    }

    public long getProbeTime() {
        return probeTime;
    }

    public void setProbeTime(long probeTime) {
        this.probeTime = probeTime;
    }

    public long getProbeTimeout() {
        return probeTimeout;
    }

    public void setProbeTimeout(long probeTimeout) {
        this.probeTimeout = probeTimeout;
    }
}