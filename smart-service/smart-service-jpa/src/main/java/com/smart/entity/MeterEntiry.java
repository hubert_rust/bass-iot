package com.smart.entity;

import com.smart.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @program: smart-service
 * @description: meter
 * @author: Admin
 * @create: 2019-04-23 18:42
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name="tb_hardware_meter")
public class MeterEntiry extends BaseEntity<Long> {
    private static final long serialVersionUID = 1L;
/*    @JsonSerialize(using= ToStringSerializer.class)
    @Column(name="id")
    private Long id;*/

    @Column(name="plugin_id")
    private String pluginId;
    @Column(name="plugin_name")
    private String pluginName;
    @Column(name="hub_address")
    private String hubAddress;
    @Column(name="hub_name")
    private String hubName;
    @Column(name="meter_no")
    private String meterNo;
    @Column(name="meter_name")
    private String meterName;
    @Column(name="meter_type")
    private String meterType;
    @Column(name="meter_status")
    private String meterStatus = "use";
    @Column(name="show_status")
    private String showStatus ;
    @Column(name="meter_model")
    private String meterModel;
    @Column(name="meter_address")
    private String meterAddress;
    @Column(name="comm_port")
    private int commPort;
    @Column(name="bps")
    private int bps;

    @Column(name="price")
    private String price;

    public String getPluginId() { return pluginId; }
    public void setPluginId(String pluginId) { this.pluginId = pluginId; }
    public String getPluginName() { return pluginName; }
    public void setPluginName(String pluginName) { this.pluginName = pluginName; }
    public String getHubAddress() { return hubAddress; }
    public void setHubAddress(String hubAddress) { this.hubAddress = hubAddress; }
    public String getHubName() { return hubName; }
    public void setHubName(String hubName) { this.hubName = hubName; }
    public String getMeterNo() { return meterNo; }
    public void setMeterNo(String meterNo) { this.meterNo = meterNo; }
    public String getMeterName() { return meterName; }
    public void setMeterName(String meterName) { this.meterName = meterName; }
    public String getMeterType() { return meterType; }
    public void setMeterType(String meterType) { this.meterType = meterType; }
    public String getMeterStatus() { return meterStatus; }
    public void setMeterStatus(String meterStatus) { this.meterStatus = meterStatus; }
    public String getShowStatus() { return showStatus; }
    public void setShowStatus(String showStatus) { this.showStatus = showStatus; }
    public String getMeterModel() { return meterModel; }
    public void setMeterModel(String meterModel) { this.meterModel = meterModel; }
    public String getMeterAddress() { return meterAddress; }
    public void setMeterAddress(String meterAddress) { this.meterAddress = meterAddress; }
    public int getCommPort() { return commPort; }
    public void setCommPort(int commPort) { this.commPort = commPort; }
    public int getBps() { return bps; }
    public void setBps(int bps) { this.bps = bps; }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "MeterEntiry{" +
                "pluginId='" + pluginId + '\'' +
                ", hubAddress='" + hubAddress + '\'' +
                ", meterType='" + meterType + '\'' +
                ", meterAddress='" + meterAddress + '\'' +
                '}';
    }
}