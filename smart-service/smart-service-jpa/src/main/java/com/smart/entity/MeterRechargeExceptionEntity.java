package com.smart.entity;

import com.smart.base.BaseEntity;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * @program: smart-service
 * @description: meter
 * @author: Admin
 * @create: 2019-04-23 18:42
 **/

@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name="tb_hardware_meter_recharge_exception")
public class MeterRechargeExceptionEntity extends BaseEntity<Long> {
    private static final long serialVersionUID = 1L;
/*    @JsonSerialize(using= ToStringSerializer.class)
    @Column(name="id")
    private Long id;*/

    @Column(name="plugin_id")
    private String pluginId;
    @Column(name="user_type")
    private String userType;
    @Column(name="user_uid")
    private String userUid;

    @Column(name="user_no")
    private String userNo;

    @Column(name="card_no")
    private String cardNo;

    @Column(name="user_name")
    private String userName;
    @Column(name="service_order")
    private String serviceOrder;
    @Column(name="command_order")
    private String commandOrder;
    @Column(name="meter_type")
    private String meterType;
    @Column(name="hub_address")
    private String hubAddress;
    @Column(name="meter_address")
    private String meterAddress;

    @Column(name="recharge_type")
    private String rechargeType;

    @Column(name="recharge_time")
    private LocalDateTime rechargeTime;

    @Column(name="recharge_balance")
    private String rechargeBalance;

    @Column(name="recharge_amount")
    private String rechargeAmount;

    @Column(name="amount")
    private String amount;

    @Column(name="balance")
    private String balance;
    @Column(name="unit")
    private String unit;

    @Column(name="price")
    private String price;

    @Column(name="reserved")
    private String reserved;

    @Column(name="query_index1")
    private String queryIndex1;
    @Column(name="query_balance1")
    private String queryBalance1;
    @Column(name="query_amount1")
    private String queryAmount1;
    @Column(name="retry_status1")
    private String retryStatus1;

    @Column(name="query_index2")
    private String queryIndex2;
    @Column(name="query_balance2")
    private String queryBalance2;
    @Column(name="query_amount2")
    private String queryAmount2;
    @Column(name="retry_status2")
    private String retryStatus2;

    @Column(name="query_index3")
    private String queryIndex3;
    @Column(name="query_balance3")
    private String queryBalance3;
    @Column(name="query_amount3")
    private String queryAmount3;
    @Column(name="retry_status3")
    private String retryStatus3;

    @Column(name="retry_time1")
    private String retryTime1;
    @Column(name="retry_time2")
    private String retryTime2;
    @Column(name="retry_time3")
    private String retryTime3;

    @Column(name="remark1")
    private String remark1;

    @Column(name="remark2")
    private String remark2;

    @Column(name="remark3")
    private String remark3;

    public String getPluginId() {
        return pluginId;
    }

    public void setPluginId(String pluginId) {
        this.pluginId = pluginId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserUid() {
        return userUid;
    }

    public void setUserUid(String userUid) {
        this.userUid = userUid;
    }

    public String getUserNo() { return userNo; }
    public void setUserNo(String userNo) { this.userNo = userNo; }
    public String getCardNo() { return cardNo; }
    public void setCardNo(String cardNo) { this.cardNo = cardNo; }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getServiceOrder() {
        return serviceOrder;
    }

    public void setServiceOrder(String serviceOrder) {
        this.serviceOrder = serviceOrder;
    }

    public String getCommandOrder() {
        return commandOrder;
    }

    public void setCommandOrder(String commandOrder) {
        this.commandOrder = commandOrder;
    }

    public String getMeterType() {
        return meterType;
    }

    public void setMeterType(String meterType) {
        this.meterType = meterType;
    }

    public String getHubAddress() {
        return hubAddress;
    }

    public void setHubAddress(String hubAddress) {
        this.hubAddress = hubAddress;
    }

    public String getMeterAddress() {
        return meterAddress;
    }

    public void setMeterAddress(String meterAddress) {
        this.meterAddress = meterAddress;
    }

    public String getRechargeType() {
        return rechargeType;
    }

    public void setRechargeType(String rechargeType) {
        this.rechargeType = rechargeType;
    }

    public LocalDateTime getRechargeTime() {
        return rechargeTime;
    }

    public void setRechargeTime(LocalDateTime rechargeTime) {
        this.rechargeTime = rechargeTime;
    }

    public String getRechargeBalance() {
        return rechargeBalance;
    }

    public void setRechargeBalance(String rechargeBalance) {
        this.rechargeBalance = rechargeBalance;
    }

    public String getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(String rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getReserved() {
        return reserved;
    }

    public void setReserved(String reserved) {
        this.reserved = reserved;
    }

    public String getQueryIndex1() {
        return queryIndex1;
    }

    public void setQueryIndex1(String queryIndex1) {
        this.queryIndex1 = queryIndex1;
    }

    public String getQueryBalance1() {
        return queryBalance1;
    }

    public void setQueryBalance1(String queryBalance1) {
        this.queryBalance1 = queryBalance1;
    }

    public String getQueryAmount1() {
        return queryAmount1;
    }

    public void setQueryAmount1(String queryAmount1) {
        this.queryAmount1 = queryAmount1;
    }

    public String getRetryStatus1() {
        return retryStatus1;
    }

    public void setRetryStatus1(String retryStatus1) {
        this.retryStatus1 = retryStatus1;
    }

    public String getQueryIndex2() {
        return queryIndex2;
    }

    public void setQueryIndex2(String queryIndex2) {
        this.queryIndex2 = queryIndex2;
    }

    public String getQueryBalance2() {
        return queryBalance2;
    }

    public void setQueryBalance2(String queryBalance2) {
        this.queryBalance2 = queryBalance2;
    }

    public String getQueryAmount2() {
        return queryAmount2;
    }

    public void setQueryAmount2(String queryAmount2) {
        this.queryAmount2 = queryAmount2;
    }

    public String getRetryStatus2() {
        return retryStatus2;
    }

    public void setRetryStatus2(String retryStatus2) {
        this.retryStatus2 = retryStatus2;
    }

    public String getQueryIndex3() {
        return queryIndex3;
    }

    public void setQueryIndex3(String queryIndex3) {
        this.queryIndex3 = queryIndex3;
    }

    public String getQueryBalance3() {
        return queryBalance3;
    }

    public void setQueryBalance3(String queryBalance3) {
        this.queryBalance3 = queryBalance3;
    }

    public String getQueryAmount3() {
        return queryAmount3;
    }

    public void setQueryAmount3(String queryAmount3) {
        this.queryAmount3 = queryAmount3;
    }

    public String getRetryStatus3() {
        return retryStatus3;
    }

    public void setRetryStatus3(String retryStatus3) {
        this.retryStatus3 = retryStatus3;
    }

    public String getRetryTime1() {
        return retryTime1;
    }

    public void setRetryTime1(String retryTime1) {
        this.retryTime1 = retryTime1;
    }

    public String getRetryTime2() {
        return retryTime2;
    }

    public void setRetryTime2(String retryTime2) {
        this.retryTime2 = retryTime2;
    }

    public String getRetryTime3() {
        return retryTime3;
    }

    public void setRetryTime3(String retryTime3) {
        this.retryTime3 = retryTime3;
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3;
    }
}