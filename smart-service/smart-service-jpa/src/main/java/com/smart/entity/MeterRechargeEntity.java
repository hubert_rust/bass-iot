package com.smart.entity;

import com.smart.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * @program: smart-service
 * @description: meter
 * @author: Admin
 * @create: 2019-04-23 18:42
 **/

@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name="tb_hardware_meter_recharge")
public class MeterRechargeEntity extends BaseEntity<Long> {
    private static final long serialVersionUID = 1L;
/*    @JsonSerialize(using= ToStringSerializer.class)
    @Column(name="id")
    private Long id;*/

    @Column(name="plugin_id")
    private String pluginId;
    @Column(name="user_type")
    private String userType;
    @Column(name="user_uid")
    private String userUid;
    @Column(name="user_no")
    private String userNo;
    @Column(name="card_no")
    private String cardNo;
    @Column(name="user_name")
    private String userName;
    @Column(name="service_order")
    private String serviceOrder;
    @Column(name="command_order")
    private String commandOrder;
    @Column(name="meter_type")
    private String meterType;
    @Column(name="hub_address")
    private String hubAddress;
    @Column(name="meter_address")
    private String meterAddress;

    @Column(name="recharge_type")
    private String rechargeType;

    @Column(name="recharge_start")
    private LocalDateTime rechargeStart;
    @Column(name="recharge_finish")
    private LocalDateTime rechargeFinish;
    @Column(name="recharge_index")
    private Long rechargeIndex;
    @Column(name="recharge_amount")
    private String rechargeAmount;

    @Column(name="capacity")
    private String capacity;

    @Column(name="balance")
    private String balance;
    @Column(name="unit")
    private String unit;

    @Column(name="price")
    private String price;
    @Column(name="reserved")
    private String reserved;
    @Column(name="recharge_stages")
    private String rechargeStages;

    @Column(name="prepare_status")
    private String prepareStatus;

    @Column(name="task_status")
    private String taskStatus;
    @Column(name="notify_status")
    private String notifyStatus;

    public String getPluginId() {
        return pluginId;
    }

    public void setPluginId(String pluginId) {
        this.pluginId = pluginId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserUid() {
        return userUid;
    }

    public void setUserUid(String userUid) {
        this.userUid = userUid;
    }

    public String getUserNo() { return userNo; }
    public void setUserNo(String userNo) { this.userNo = userNo; }
    public String getCardNo() { return cardNo; }
    public void setCardNo(String cardNo) { this.cardNo = cardNo; }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getServiceOrder() {
        return serviceOrder;
    }

    public void setServiceOrder(String serviceOrder) {
        this.serviceOrder = serviceOrder;
    }

    public String getCommandOrder() {
        return commandOrder;
    }

    public void setCommandOrder(String commandOrder) {
        this.commandOrder = commandOrder;
    }

    public String getMeterType() {
        return meterType;
    }

    public void setMeterType(String meterType) {
        this.meterType = meterType;
    }

    public String getHubAddress() {
        return hubAddress;
    }

    public void setHubAddress(String hubAddress) {
        this.hubAddress = hubAddress;
    }

    public String getMeterAddress() {
        return meterAddress;
    }

    public void setMeterAddress(String meterAddress) {
        this.meterAddress = meterAddress;
    }

    public String getRechargeType() {
        return rechargeType;
    }

    public void setRechargeType(String rechargeType) {
        this.rechargeType = rechargeType;
    }

    public LocalDateTime getRechargeStart() { return rechargeStart; }
    public void setRechargeStart(LocalDateTime rechargeStart) { this.rechargeStart = rechargeStart; }
    public LocalDateTime getRechargeFinish() { return rechargeFinish; }
    public void setRechargeFinish(LocalDateTime rechargeFinish) { this.rechargeFinish = rechargeFinish; }

    public Long getRechargeIndex() { return rechargeIndex; }
    public void setRechargeIndex(Long rechargeIndex) { this.rechargeIndex = rechargeIndex; }

    public String getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(String rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getReserved() {
        return reserved;
    }

    public void setReserved(String reserved) {
        this.reserved = reserved;
    }

    public String getPrepareStatus() {
        return prepareStatus;
    }

    public void setPrepareStatus(String prepareStatus) {
        this.prepareStatus = prepareStatus;
    }

    public String getRechargeStages() {
        return rechargeStages;
    }

    public void setRechargeStages(String rechargeStages) {
        this.rechargeStages = rechargeStages;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getNotifyStatus() {
        return notifyStatus;
    }

    public void setNotifyStatus(String notifyStatus) {
        this.notifyStatus = notifyStatus;
    }
}