package com.smart.entity;

import com.smart.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * @program: smart-service
 * @description: 集中器
 * @author: Admin
 * @create: 2019-04-22 15:28
 **/
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name="tb_hardware_monitor")
public class MonitorEntity extends BaseEntity<Long> {
    private static final long serialVersionUID = 1L;
/*    @JsonSerialize(using= ToStringtb_hardware_monitor_paramSerializer.class)
    @Column(name="id")
    private Long id;*/

    @Column(name="object_key")
    private String objectKey;
    @Column(name="main_type")
    private String mainType;
    @Column(name="monitor_type")
    private String monitorType;
    @Column(name="con_address")
    private String conAddress;
    @Column(name="sub_address")
    private String subAddress;
    @Column(name="plugin_id")
    private String pluginId;
    @Column(name="status")
    private String status;
    @Column(name="probe_time")
    private long probeTime;
    @Column(name="probe_timeout")
    private long probeTimeout;
    @Column(name="last_fault")
    private String lastFault;
    @Column(name="last_clear")
    private String lastClear;
    @Column(name="last_time")
    private long lastTime;

    public String getMainType() { return mainType; }
    public void setMainType(String mainType) { this.mainType = mainType; }
    public String getObjectKey() {
        return objectKey;
    }

    public void setObjectKey(String objectKey) {
        this.objectKey = objectKey;
    }

    public String getMonitorType() {
        return monitorType;
    }

    public void setMonitorType(String monitorType) {
        this.monitorType = monitorType;
    }

    public String getConAddress() {
        return conAddress;
    }

    public void setConAddress(String conAddress) {
        this.conAddress = conAddress;
    }

    public String getSubAddress() {
        return subAddress;
    }

    public void setSubAddress(String subAddress) {
        this.subAddress = subAddress;
    }

    public String getPluginId() {
        return pluginId;
    }

    public void setPluginId(String pluginId) {
        this.pluginId = pluginId;
    }

    public String getStatus() { return status; }
    public void setStatus(String status) { this.status = status; }

    public long getProbeTime() {
        return probeTime;
    }

    public void setProbeTime(long probeTime) {
        this.probeTime = probeTime;
    }

    public String getLastFault() {
        return lastFault;
    }

    public void setLastFault(String lastFault) {
        this.lastFault = lastFault;
    }

    public String getLastClear() {
        return lastClear;
    }

    public void setLastClear(String lastClear) {
        this.lastClear = lastClear;
    }

    public long getLastTime() {
        return lastTime;
    }

    public void setLastTime(long lastTime) {
        this.lastTime = lastTime;
    }

    public long getProbeTimeout() {
        return probeTimeout;
    }

    public void setProbeTimeout(long probeTimeout) {
        this.probeTimeout = probeTimeout;
    }
}