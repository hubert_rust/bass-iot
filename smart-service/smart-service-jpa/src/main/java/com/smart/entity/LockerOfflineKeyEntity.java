package com.smart.entity;

import com.smart.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @program: smart-service
 * @description: 集中器
 * @author: Admin
 * @create: 2019-04-22 15:28
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name="tb_hardware_locker_offline_key")
public class LockerOfflineKeyEntity extends BaseEntity<Long> {
    private static final long serialVersionUID = 1L;
/*    @JsonSerialize(using= ToStringtb_hardware_monitor_paramSerializer.class)
    @Column(name="id")
    private Long id;*/


    @Column(name="main_order")
    private String mainOrder;
    @Column(name="command_order")
    private String commandOrder;
    @Column(name="plugin_id")
    private String PluginId;
    @Column(name="secret_key")
    private String secretKey;
    @Column(name="hex_key")
    private String hexKey;
    @Column(name="hub_address")
    private String hubAddress;
    @Column(name="sub_address")
    private String subAddress;
    @Column(name="send_state")
    private String sendState;
    @Column(name="set_time")
    private String setTime;
    @Column(name="time_span")
    private long timeSpan;

    public String getMainOrder() { return mainOrder; }
    public void setMainOrder(String mainOrder) { this.mainOrder = mainOrder; }
    public String getCommandOrder() {
        return commandOrder;
    }

    public void setCommandOrder(String commandOrder) {
        this.commandOrder = commandOrder;
    }

    public String getPluginId() {
        return PluginId;
    }

    public void setPluginId(String pluginId) {
        PluginId = pluginId;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getHubAddress() {
        return hubAddress;
    }

    public void setHubAddress(String hubAddress) {
        this.hubAddress = hubAddress;
    }

    public String getSubAddress() {
        return subAddress;
    }

    public void setSubAddress(String subAddress) {
        this.subAddress = subAddress;
    }

    public String getSendState() {
        return sendState;
    }

    public void setSendState(String sendState) {
        this.sendState = sendState;
    }

    public String getSetTime() {
        return setTime;
    }

    public void setSetTime(String setTime) {
        this.setTime = setTime;
    }

    public long getTimeSpan() {
        return timeSpan;
    }

    public void setTimeSpan(long timeSpan) {
        this.timeSpan = timeSpan;
    }

    public String getHexKey() {
        return hexKey;
    }

    public void setHexKey(String hexKey) {
        this.hexKey = hexKey;
    }
}
