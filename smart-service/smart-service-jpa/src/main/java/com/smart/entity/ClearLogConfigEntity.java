package com.smart.entity;

import com.smart.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @program: smart-service
 * @description: meter
 * @author: Admin
 * @create: 2019-04-23 18:42
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name="tb_bass_clear_log_config")
public class ClearLogConfigEntity extends BaseEntity<Long> {
    private static final long serialVersionUID = 1L;
/*    @JsonSerialize(using= ToStringSerializer.class)
    @Column(name="id")
    private Long id;*/

    @Column(name="bass_name")
    private String bassName;
    @Column(name="service_name")
    private String serviceName;
    @Column(name="keep_days")
    private int keepDays;

    public String getBassName() { return bassName; }
    public void setBassName(String bassName) { this.bassName = bassName; }
    public String getServiceName() { return serviceName; }
    public void setServiceName(String serviceName) { this.serviceName = serviceName; }
    public int getKeepDays() { return keepDays; }
    public void setKeepDays(int keepDays) { this.keepDays = keepDays; }

}