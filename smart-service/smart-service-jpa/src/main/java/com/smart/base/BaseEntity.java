package com.smart.base;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @program: smart-service
 * @description: base entity
 * @author: Admin
 * @create: 2019-04-22 15:45
 **/
//@MappedSuperclass通过这个注解，我们可以将该实体类当成基类实体，它不会隐射到数据库表，
// 但继承它的子类实体在隐射时会自动扫描该基类实体的隐射属性，添加到子类实体的对应数据库表中。
@MappedSuperclass
@Data
public class BaseEntity<PK extends Serializable> implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "idGenerator") //strategy=GenerationType.IDENTITY
    @GenericGenerator(name = "idGenerator", strategy = "com.smart.toolkit.IDGenerator")
    @JsonSerialize(using= ToStringSerializer.class)
    @Column(name="id")
    protected PK id;

    @Column(name="dc_name")
    private String dcName;
    @Column(name="dc_id")
    private String dcId;

    @Column(name="version")
    protected Long version = 1L;

    @Column(name="remark")
    protected String remark;

    @Column(name="create_time")
    protected LocalDateTime createTime;

    @Column(name="modify_time")
    protected LocalDateTime modifyTime;

    @Column(name="create_user")
    protected String createUser;

    @Column(name="modify_user")
    protected String modifyUser;

    public PK getId() {
        return id;
    }
    public void setId(PK id) {
        this.id = id;
    }
    public Long getVersion() {
        return version;
    }
    public void setVersion(Long version) {
        this.version = version;
    }
    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }
    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
    public LocalDateTime getModifyTime() {
        return modifyTime;
    }
    public void setModifyTime(LocalDateTime modifyTime) {
        this.modifyTime = modifyTime;
    }
    public String getCreateUser() {
        return createUser;
    }
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }
    public String getModifyUser() { return modifyUser; }
    public void setModifyUser(String modifyUser) { this.modifyUser = modifyUser; }
    public String getDcName() { return dcName; }
    public void setDcName(String dcName) { this.dcName = dcName; }
    public String getDcId() { return dcId; }
    public void setDcId(String dcId) { this.dcId = dcId; }
}