package com.smart.repository;

import com.smart.entity.ElecMeterReadEntity;
import com.smart.entity.HighMeterReadEntity;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

/**
 * @program: smart-service
 * @description: hub
 * @author: Admin
 * @create: 2019-04-22 16:34
 **/

public interface ElecMeterReadRepostory extends BaseRepostory<ElecMeterReadEntity> {

}