package com.smart.repository;

import com.smart.entity.ClearLogConfigEntity;
import com.smart.entity.HighMeterReadEntity;

/**
 * @program: smart-service
 * @description: hub
 * @author: Admin
 * @create: 2019-04-22 16:34
 **/

public interface HighMeterReadRepostory extends BaseRepostory<HighMeterReadEntity> {

}