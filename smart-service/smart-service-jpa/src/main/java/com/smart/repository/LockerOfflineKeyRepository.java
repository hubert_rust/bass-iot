package com.smart.repository;


import com.smart.entity.LockerOfflineKeyEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-24 12:37
 **/
public interface LockerOfflineKeyRepository extends BaseRepostory<LockerOfflineKeyEntity> {
    @Modifying
    @Transactional
    @Query(value = "update tb_hardware_locker_offline_key set send_state = ?1, failtures_number=?2 where command_order = ?3",nativeQuery = true)
    public int updateResult(String sendState, int fails, String commandOrder);
}
