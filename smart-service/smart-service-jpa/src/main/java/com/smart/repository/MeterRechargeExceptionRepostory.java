package com.smart.repository;

import com.smart.entity.MeterRechargeEntity;
import com.smart.entity.MeterRechargeExceptionEntity;

/**
 * @program: smart-service
 * @description: hub
 * @author: Admin
 * @create: 2019-04-22 16:34
 **/

public interface MeterRechargeExceptionRepostory extends BaseRepostory<MeterRechargeExceptionEntity> {

}