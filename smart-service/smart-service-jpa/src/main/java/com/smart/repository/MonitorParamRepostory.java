package com.smart.repository;

import com.smart.entity.MeterEntiry;
import com.smart.entity.MonitorParamEntity;

/**
 * @program: smart-service
 * @description: hub
 * @author: Admin
 * @create: 2019-04-22 16:34
 **/

public interface MonitorParamRepostory extends BaseRepostory<MonitorParamEntity> {

}