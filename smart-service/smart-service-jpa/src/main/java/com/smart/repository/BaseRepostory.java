package com.smart.repository;

import com.smart.entity.ElecMeterReadEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * @program: smart-service
 * @description: hub
 * @author: Admin
 * @create: 2019-04-22 16:34
 **/

@NoRepositoryBean
public interface BaseRepostory<T> extends JpaRepository<T, Long> , JpaSpecificationExecutor<T> {

    //电表抄表数据
    @Query(value = "select a.* from tb_hardware_read_elec_meter a where " +
            "a.hub_address = ?1 " +
            "and a.meter_address = ?2 " +
            "and (a.create_time like ?3%) ",nativeQuery = true)
    public List<T> findElecRead(String hubAddress, String subAddress, String time);
    //电表balance抄表数据
    @Query(value = "select a.* from tb_hardware_read_elec_balance a where " +
            "a.hub_address = ?1 " +
            "and a.meter_address = ?2 ",nativeQuery = true)
    public List<T> findElecBalance(String hubAddress, String subAddress, String time);
    //high抄表数据
    @Query(value = "select a.* from tb_hardware_read_high_meter a where " +
            "a.hub_address = ?1 " +
            "and a.meter_address = ?2 " +
            "and (a.create_time like ?3%) ",nativeQuery = true)
    public List<T> findHighRead(String hubAddress, String subAddress, String time);


    //job log
    @Query(value = "select a.* from tb_hardware_job_log a where a.job_handle=?1 " +
            "and a.hub_address = ?2 " +
            "and a.sub_address = ?3 " +
            "and (a.create_time like ?4%) ",nativeQuery = true)
    public List<T> findLockerSyncTime(String jobHandle, String hubAddress, String subAddress, String time);

    //cold-meter
    @Query(value = "select a.* from tb_hardware_read_cold_meter a where " +
            "a.hub_address = ?1 " +
            "and a.meter_address = ?2 " +
            "and (a.create_time like ?3%) ",nativeQuery = true)
    public List<T> findColdRead(String hubAddress, String subAddress, String time);
    //cold-meter-balance
    @Query(value = "select a.* from tb_hardware_read_cold_balance a where " +
            "a.hub_address = ?1 " +
            "and a.meter_address = ?2 " +
            "and (a.create_time like ?3%) ",nativeQuery = true)
    public List<T> findColdBalance(String hubAddress, String subAddress, String time);

    //set price
    @Modifying
    @Transactional
    @Query(value = "update tb_hardware_meter set price = ?1 where " +
            " plugin_id = ?2 and hub_address = ?3 and meter_address = ?4", nativeQuery = true)
    public int updatePrice(String price, String pluginId, String hubAddress, String meterAddress);

    //recharge-prepare
    @Modifying
    @Transactional
    @Query(value = "update tb_hardware_meter_recharge set recharge_stages = ?2, " +
            " task_status = ?3 " +
            " where id = ?1", nativeQuery = true)
    public int updateRecharge(Long id, String rechargeStages, String taskStatus);
    //recharge-prepare
    @Modifying
    @Transactional
    @Query(value = "update tb_hardware_meter_recharge " +
            " set prepare_status = ?2, remark = ?3, modify_time = ?4 where id = ?1", nativeQuery = true)
    public int updateRemark(Long id,
                            String prepareStatus,
                            String remark,
                            LocalDateTime modifyTime);

    //recharge-prepare
    @Modifying
    @Transactional
    @Query(value = "update tb_hardware_meter_recharge " +
            " set balance = ?2, remark = ?3 , modify_time = ?4 where id = ?1", nativeQuery = true)
    public int updateLeftResult(Long id, String balance, String remark, LocalDateTime modifyTime);

    //recharge-prepare
    @Modifying
    @Transactional
    @Query(value = "update tb_hardware_meter_recharge " +
            " set recharge_index = :rechargeIndex, prepare_status = :prepareStatus, remark = :remark , modify_time = :modifyTime  where id = :id", nativeQuery = true)
    public int updateRechargeIndexResult(@Param("id") Long id,
                                         @Param("rechargeIndex") Long rechargeIndex,
                                         @Param("prepareStatus") String prepareStatus,
                                         @Param("remark") String remark,
                                         @Param("modifyTime") LocalDateTime modifyTime);

    //recharge-recharge
    @Modifying
    @Transactional
    @Query(value = "update tb_hardware_meter_recharge " +
            " set recharge_start = ?2, recharge_finish = ?3 , recharge_stages = ?4, task_status = ?5, remark = ?6  " +
            "where id = ?1", nativeQuery = true)
    public int updateRechargeStart(Long id,
                                   LocalDateTime start,
                                   LocalDateTime finish,
                                   String stage,
                                   String task,
                                   String remark);


    //recharge-notify
    @Modifying
    @Transactional
    @Query(value = "update tb_hardware_meter_recharge " +
            " set recharge_stages = ?2, notify_status = ?3, remark = ?4  " +
            "where id = ?1", nativeQuery = true)
    public int updateRechargeNotify(Long id,
                                   String stage,
                                   String notifyStatus,
                                   String remark);

    //cold-meter-update-balance
    @Modifying
    @Transactional
    @Query(value = "update tb_hardware_read_cold_balance " +
            " set left_amount = :leftAmount, use_total_recharge=:recharge, version = version + 1, " +
            "start_time = :start, finish_time = :end, read_status = :status " +
            " where id = :id and version = :version ", nativeQuery = true)
    public int updateColdMeterBalance(@Param("id") Long id,
                                      @Param("leftAmount") String leftAmount,
                                      @Param("recharge") String recharge,
                                      @Param("version") Long version,
                                      @Param("start") LocalDateTime start,
                                      @Param("end") LocalDateTime end,
                                      @Param("status") String status);

    //elec-meter-update-balance
    @Modifying
    @Transactional
    @Query(value = "update tb_hardware_read_elec_balance " +
            " set balance = :recharge, version = version + 1, start_time = :start, " +
            "finish_time = :end, read_status = :status" +
            " where id = :id and version = :version ", nativeQuery = true)
    public int updateElecMeterBalance(@Param("id") Long id,
                                      @Param("recharge") String recharge,
                                      @Param("version") Long version,
                                      @Param("start") LocalDateTime start,
                                      @Param("end") LocalDateTime end,
                                      @Param("status") String status);

    //high-meter-update-balance
    @Modifying
    @Transactional
    @Query(value = "update tb_hardware_read_high_balance" +
            " set left_amount = :recharge, version = version + 1, start_time = :start," +
            " finish_time = :end, read_status = :status " +
            " where id = :id and version = :version ", nativeQuery = true)
    public int updateHighMeterBalance(@Param("id") Long id,
                                      @Param("recharge") String recharge,
                                      @Param("version") Long version,
                                      @Param("start") LocalDateTime start,
                                      @Param("end") LocalDateTime end,
                                      @Param("status") String status);

    //update hot-meter balance by read
    @Modifying
    @Transactional
    @Query(value = "update tb_hardware_hotmeter_balance " +
            " set balance =:balance, left_amount =:leftAmount, version = version + 1 " +
            " where id =:id and version =:version", nativeQuery = true)
    public int updateHotMeterBalanceByRead(@Param("id") Long id,
                                           @Param("balance") String balance,
                                           @Param("leftAmount") String leftAmount,
                                           @Param("version") Long version);

    @Modifying
    @Transactional
    @Query(value = "update tb_hardware_hotmeter_balance " +
            " set balance =:balance, left_amount =:leftAmount, version = version + 1, modify_time=:modifyTime " +
            " where id =:id and version =:version", nativeQuery = true)
    public int updateHotMeterBalance(@Param("id") Long id,
                                     @Param("balance") String balance,
                                     @Param("leftAmount") String leftAmount,
                                     @Param("version") Long version,
                                     @Param("modifyTime") LocalDateTime modifyTime);

}