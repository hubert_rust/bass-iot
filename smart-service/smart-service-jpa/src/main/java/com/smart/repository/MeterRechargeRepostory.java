package com.smart.repository;

import com.smart.entity.ColdMeterReadEntity;
import com.smart.entity.MeterRechargeEntity;
import io.micrometer.core.instrument.Meter;

/**
 * @program: smart-service
 * @description: hub
 * @author: Admin
 * @create: 2019-04-22 16:34
 **/

public interface MeterRechargeRepostory extends BaseRepostory<MeterRechargeEntity> {

}