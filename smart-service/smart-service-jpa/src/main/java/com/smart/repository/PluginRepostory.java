package com.smart.repository;

import com.smart.entity.PluginEntity;

/**
 * @program: smart-service
 * @description: hub
 * @author: Admin
 * @create: 2019-04-22 16:34
 **/

public interface PluginRepostory extends BaseRepostory<PluginEntity> {

}