package com.smart.repository;

import com.smart.entity.ColdMeterBalanceEntity;
import com.smart.entity.ColdMeterReadEntity;

/**
 * @program: smart-service
 * @description: hub
 * @author: Admin
 * @create: 2019-04-22 16:34
 **/

public interface ColdMeterBalanceRepostory extends BaseRepostory<ColdMeterBalanceEntity> {

}