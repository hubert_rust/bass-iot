package com.smart.repository;


import com.smart.entity.CronJobLogEntity;
import com.smart.entity.CronMainJobLogEntity;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-24 12:37
 **/
public interface CronMainJobLogRepository extends BaseRepostory<CronMainJobLogEntity> {
}
