package com.smart.repository;

import com.smart.entity.MonitorEntity;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-24 12:37
 **/
public interface MonitorRepository extends BaseRepostory<MonitorEntity> {
}
