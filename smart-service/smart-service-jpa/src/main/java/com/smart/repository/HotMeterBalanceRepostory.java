package com.smart.repository;

import com.smart.entity.HotMeterBalanceEntity;
import com.smart.entity.HotMeterReadEntity;

/**
 * @program: smart-service
 * @description: hub
 * @author: Admin
 * @create: 2019-04-22 16:34
 **/

public interface HotMeterBalanceRepostory extends BaseRepostory<HotMeterBalanceEntity> {

}