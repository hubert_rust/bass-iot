package com.smart.repository;

import com.smart.entity.ElecBalanceReadEntity;
import com.smart.entity.HotMeterUseRecordEntity;

/**
 * @program: smart-service
 * @description: hub
 * @author: Admin
 * @create: 2019-04-22 16:34
 **/

public interface HotMeterUseRecordRepostory extends BaseRepostory<HotMeterUseRecordEntity> {

}