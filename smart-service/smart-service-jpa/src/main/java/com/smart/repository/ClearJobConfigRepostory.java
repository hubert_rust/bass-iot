package com.smart.repository;

import com.smart.entity.ClearLogConfigEntity;

/**
 * @program: smart-service
 * @description: hub
 * @author: Admin
 * @create: 2019-04-22 16:34
 **/

public interface ClearJobConfigRepostory extends BaseRepostory<ClearLogConfigEntity> {

}