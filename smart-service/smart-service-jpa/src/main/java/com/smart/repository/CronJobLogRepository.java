package com.smart.repository;


import com.smart.entity.CronJobLogEntity;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-24 12:37
 **/
public interface CronJobLogRepository extends BaseRepostory<CronJobLogEntity> {
}
