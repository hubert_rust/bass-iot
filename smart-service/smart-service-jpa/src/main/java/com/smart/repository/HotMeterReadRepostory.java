package com.smart.repository;

import com.smart.entity.HighBalanceReadEntity;
import com.smart.entity.HotMeterReadEntity;

/**
 * @program: smart-service
 * @description: hub
 * @author: Admin
 * @create: 2019-04-22 16:34
 **/

public interface HotMeterReadRepostory extends BaseRepostory<HotMeterReadEntity> {

}