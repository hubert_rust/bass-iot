package com.smart.repository;

import com.smart.entity.ConcentratorEntity;

/**
 * @program: smart-service
 * @description: hub
 * @author: Admin
 * @create: 2019-04-22 16:34
 **/

public interface ConcentratorRepostory extends BaseRepostory<ConcentratorEntity> {

}