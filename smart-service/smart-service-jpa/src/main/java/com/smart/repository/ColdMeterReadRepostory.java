package com.smart.repository;

import com.smart.entity.ColdMeterReadEntity;
import com.smart.entity.ElecMeterReadEntity;

/**
 * @program: smart-service
 * @description: hub
 * @author: Admin
 * @create: 2019-04-22 16:34
 **/

public interface ColdMeterReadRepostory extends BaseRepostory<ColdMeterReadEntity> {

}