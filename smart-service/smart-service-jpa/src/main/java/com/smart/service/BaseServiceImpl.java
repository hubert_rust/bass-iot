package com.smart.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.smart.base.BaseEntity;
import com.smart.entity.LockerOfflineKeyEntity;
import com.smart.entity.MeterEntiry;
import com.smart.repository.BaseRepostory;
import com.smart.framework.utils.PFConstDef;
import com.smart.repository.LockerOfflineKeyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.lang.reflect.ParameterizedType;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @program: smart-service
 * @description: base
 * @author: Admin
 * @create: 2019-04-22 18:19
 **/
public class BaseServiceImpl<T extends BaseEntity<Long>> {
    private static final Logger log = LoggerFactory.getLogger(BaseServiceImpl.class);

    @Autowired
    protected BaseRepostory<T> baseRepostory;

    protected <T> List<T> getEntity(List list) throws Exception {
        Class<T> cls = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        List<T> rets = Lists.newArrayList();
        list.forEach(v -> {
            ObjectMapper objectMapper = new ObjectMapper();
            T var = (T) objectMapper.convertValue(v, cls);
            rets.add(var);
        });
        return rets;
    }

    /**
     * @ Description: 配置完后
     * @ Param:
     * @ return:
     * @ Author: Admin
     * @ Date:
     */
    public List process(String hdType,
                          List list,
                          String cmdCode,
                          String cmdType,
                          int autoPush) throws Exception {

        log.info(">>> BaseServiceImpl, process, hdType: {}, cmdCode: {}, cmdType: {}."
                , hdType, cmdCode, cmdType);
        log.info(">>> BaseServiceImpl, list.toString: {}", list.toString());


        //自动同步到插件, 同步到zookeeper，在keep中进行同步
        try {

            if (cmdCode.equals(PFConstDef.REQ_CMD_CODE_ADD)) {
                List<T> entitys = getEntity(list);
                List ret = baseRepostory.saveAll(entitys);
                return ret;
            } else if (cmdCode.equals(PFConstDef.REQ_CMD_CODE_MOD)) {
                List<T> entitys = getEntity(list);
                baseRepostory.saveAll(entitys);
            } else if (cmdCode.equals(PFConstDef.REQ_CMD_CODE_DEL)) {
                List<T> entitys = getEntity(list);
                //2019-07-23,调试locker，统一修改为通过id删除
                try {
                    entitys.forEach(v->{
                        baseRepostory.deleteById(v.getId());
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new Exception("请通过id删除," + e.getMessage());
                }
                //baseRepostory.deleteInBatch(entitys);
            } else {
                //查询接口
                return queryInterface(list, cmdCode);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }

        //手动同步
        //if (autoPush == 1) {
        //    syncPushToPlugin(hdType, cmdCode, entitys);
        //}
        return null;
    }

    /**
     * @ Description: 同步到插件层
     * @ Param:
     * @ return:
     * @ Author: Admin
     * @ Date:
     */
    public String syncPushToPlugin(String hdType,
                                   String cmdCode,
                                   List<T> entitys) throws Exception {


        return "";
    }

    public List queryInterface(List list, String cmdCode) throws Exception{
        switch (cmdCode) {
            case "findMeterRecharge": {
                if (Objects.isNull(list) || list.size() <=0) {
                    throw new Exception("list is null or empty");
                }

                Map map = (Map<String, Object>)list.get(0);
                String commandOrder = map.get("commandOrder").toString();
                String serviceOrder = map.get("serviceOrder").toString();
                return baseRepostory.findAll((root, query, builder)-> {
                    Predicate p1 = builder.equal(root.get("commandOrder"), commandOrder);
                    Predicate p2 = builder.equal(root.get("serviceOrder"), serviceOrder);
                    return builder.and(p1, p2);
                });
            }
            case "findPluginState" : {
                if (Objects.isNull(list) || list.size() <=0) {
                    throw new Exception("list is null or empty");
                }
                Map map = (Map<String, Object>)list.get(0);
                String pluginId = map.get("pluginId").toString();
                return baseRepostory.findAll((root, query, builder)->{
                    Predicate p1 = builder.equal(root.get("mainType"), "plugin");
                    Predicate p2 = builder.equal(root.get("pluginId"), pluginId);
                    return builder.and(p1, p2);
                });
                //break;
            }
            case "findHubState" : {
                if (Objects.isNull(list) || list.size() <=0) {
                    throw new Exception("list is null or empty");
                }
                Map map = (Map<String, Object>)list.get(0);
                String pluginId = map.get("pluginId").toString();
                String hubAddr = map.get("hubAddress").toString();
                return baseRepostory.findAll((root, query, builder)->{
                    Predicate p1 = builder.equal(root.get("conAddress"), hubAddr);
                    Predicate p2 = builder.equal(root.get("pluginId"), pluginId);
                    Predicate p3 = builder.equal(root.get("mainType"), "hub");
                    return builder.and(p1, p2, p3);
                });

            }
            case "findSubState" : {
                if (Objects.isNull(list) || list.size() <=0) {
                    throw new Exception("list is null or empty");
                }
                Map map = (Map<String, Object>)list.get(0);
                String pluginId = map.get("pluginId").toString();
                String hubAddr = map.get("hubAddress").toString();
                String subAddr = map.get("subAddress").toString();
                return baseRepostory.findAll((root, query, builder)->{
                    Predicate p1 = builder.equal(root.get("conAddress"), hubAddr);
                    Predicate p2 = builder.equal(root.get("pluginId"), pluginId);
                    Predicate p3 = builder.equal(root.get("subAddress"), subAddr);
                    return builder.and(p1, p2, p3);
                });
            }

            case "pluginList" : {
                if (Objects.isNull(list) || list.size() <=0) {
                    throw new Exception("list is null or empty");
                }

                Map map = (Map<String, Object>)list.get(0);
                if (Objects.isNull(map.get("pluginName"))) {
                    throw new Exception("pluginName is invalid");
                }
                if (Strings.isNullOrEmpty(map.get("pluginName").toString())) {
                    throw new Exception("pluginName is empty or invalid");
                }
                String pluginName = map.get("pluginName").toString();
                return baseRepostory.findAll((root, query, builder)->{
                    return builder.equal(root.get("pluginName"), pluginName);
                });
            }
            case  "hubState": {
                if (Strings.isNullOrEmpty(list.toString())) {
                    Map map = Maps.newHashMap();
                    if (Objects.isNull(map.get("pluginName"))) {
                        throw new Exception("pluginName is invalid");
                    }
                    if (Strings.isNullOrEmpty(map.get("pluginName").toString())) {
                        throw new Exception("pluginName is empty or invalid");
                    }
                    String pluginName = map.get("pluginName").toString();
                    return baseRepostory.findAll((root, query, builder)->{
                        return builder.equal(root.get("pluginName"), pluginName);
                    });
                }
            }
            case "eventState": {
                Map map = Maps.newHashMap();
                if (Objects.isNull(map.get("eventType"))) {
                    throw new Exception("eventType is invalid");
                }
                if (Strings.isNullOrEmpty(map.get("pluginName").toString())) {
                    throw new Exception("pluginName is empty or invalid");
                }
                break;
            }

            default: {
                log.error(">>>BaseServiceImpl, queryInterface, invalid cmdCode: {}", cmdCode);
                break;
            }
        }

        return null;

    }
    public String findPluginState(String pluginId) {
        baseRepostory.findById(1L);
        return "";
    }

    public T saveEntity(T entity) {
        return baseRepostory.save(entity);
    }

    public Optional<T> findById(long id) {
        return baseRepostory.findById(id);
    }
    public List<T> findAll() {
        return baseRepostory.findAll();
    }
    public List<T> findAll(Specification spec) {
        return baseRepostory.findAll(spec);
    }

    public Optional<T> findOne(Specification<T> spec) {
        return baseRepostory.findOne(spec);
    }
    //locker查询密码，


    public List<T> findElecRead(String pluginId, String hubAddress, String meterAddress, String time) {
        return  baseRepostory.findElecRead(hubAddress, meterAddress, time);
    }

    public List<T> findElecBalance(String pluginId, String hubAddress, String meterAddress, String time) {
        return  baseRepostory.findElecBalance(hubAddress, meterAddress, time);
    }
    public List<T> findHighRead(String pluginId, String hubAddress, String meterAddress, String time) {
        return  baseRepostory.findHighRead(hubAddress, meterAddress, time);
    }

    public List<T> findLockerSyncTime(String pluginId, String hubAddress, String meterAddress, String time) {
        return  baseRepostory.findLockerSyncTime("SyncTimeJobHandle", hubAddress, meterAddress, time);
    }

    public List<T> findColdRead(String pluginId, String hubAddress, String meterAddress, String time) {
        return  baseRepostory.findColdRead(hubAddress, meterAddress, time);
    }

    public List<T> findColdBalance(String pluginId, String hubAddress, String meterAddress, String time) {
        return  baseRepostory.findColdBalance(hubAddress, meterAddress, time);
    }


    //设置单价
    public List<T> updatePrice(String pluginId, String hubAddress, String meterAddress, String price) {
        int ret = baseRepostory.updatePrice(price, pluginId, hubAddress, meterAddress);
        List<T> list = baseRepostory.findAll((root, query, builder) -> {
            Predicate p1 = builder.equal(root.get("pluginId"), pluginId);
            Predicate p2 = builder.equal(root.get("hubAddress"), hubAddress);
            Predicate p3 = builder.equal(root.get("meterAddress"), meterAddress);
            return builder.and(p1, p2, p3);
        });
        return list;
    }


    //充值
    public int updateRemark(Long id, String prepareStatus, String remark) {
        return baseRepostory.updateRemark(id,
                prepareStatus,
                remark,
                LocalDateTime.now());
    }

    public int updateLeftRemark(Long id, String leftMoney, String remark) {
        return baseRepostory.updateLeftResult(id,
                leftMoney,
                remark,
                LocalDateTime.now());
    }
    //recharge-prepare
    @Transactional
    public int updateRechargeIndexRemark(Long id,
                                         String rechargeIndex,
                                         String prepareStatus,
                                         String remark) {
        return baseRepostory.updateRechargeIndexResult(id,
                Long.valueOf(rechargeIndex),
                prepareStatus,
                remark,
                LocalDateTime.now());
    }
    public int updateRechargeStart(Long id,
                                   LocalDateTime start,
                                   LocalDateTime finish,
                                   String stage,
                                   String task,
                                   String remark) {
        return baseRepostory.updateRechargeStart(id,
                start,
                finish,
                stage,
                task,
                remark);
    }



}