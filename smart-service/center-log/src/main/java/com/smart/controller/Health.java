package com.smart.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: smart-service
 * @description: health
 * @author: Admin
 * @create: 2019-04-16 19:18
 **/
@RestController
@RequestMapping("actuator")
public class Health {
    @RequestMapping("health")
    public String health() {
        return "OK";
    }
}