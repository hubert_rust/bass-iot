package com.smart.config;

import com.smart.entity.AppConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @program: plugin
 * @description: app config
 * @author: Admin
 * @create: 2019-04-12 17:14
 **/
@Configuration
@Component
public class InitConfig {
    @Autowired
    Environment environment;

    Demo demo;

    @PostConstruct
    public void init() {

        AppConfig.mongocfg.setHost(environment.getProperty("server.data.mongodb.host", String.class));
        AppConfig.mongocfg.setPort(environment.getProperty("server.data.mongodb.port", Integer.class));
        AppConfig.mongocfg.setUserName(environment.getProperty("server.data.mongodb.username", String.class));
        AppConfig.mongocfg.setPassWord(environment.getProperty("server.data.mongodb.password", String.class));
        AppConfig.mongocfg.setAuthSource(environment.getProperty("server.data.mongodb.authsource", String.class));


        System.out.println("");
    }



}

/*import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.context.annotation.Configuration;
        import org.springframework.core.env.Environment;

*//**
 * A configuration bean.
 * @author <a href="http://escoffier.me">Clement Escoffier</a>
 *//*
@Configuration
public class AppConfiguration {

    @Autowired
    Environment environment;

    int httpPort() {
        return environment.getProperty("http.port", Integer.class, 8080);
    }

}*/
