package com.smart.entity;

import com.sun.org.apache.bcel.internal.generic.INSTANCEOF;
import org.springframework.stereotype.Component;

/**
 * @program: plugin
 * @description: app config
 * @author: Admin
 * @create: 2019-04-12 17:22
 **/

public enum  AppConfig {
    INSTANCE;
    public static String LOG_COLLECTION = "smart_iot_logs";

    public static Mongocfg mongocfg = new Mongocfg();


    public static  Kafkacfg kafkacfg = new Kafkacfg();

    public static class Kafkacfg {

    }

    public static class Mongocfg {
        private String host;
        private int port;
        private String userName;
        private String passWord;
        private String authSource;
        //private String dbName;


        public String getHost() { return host; }
        public void setHost(String host) { this.host = host; }
        public int getPort() { return port; }
        public void setPort(int port) { this.port = port; }
        public String getUserName() { return userName; }
        public void setUserName(String userName) { this.userName = userName; }
        public String getPassWord() { return passWord; }
        public void setPassWord(String passWord) { this.passWord = passWord; }
        public String getAuthSource() { return authSource; }
        public void setAuthSource(String authSource) { this.authSource = authSource; }
    }
}