package com.smart.service;

import com.smart.config.InitConfig;
import com.smart.entity.AppConfig;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @program: plugin
 * @description: mongo client
 * @author: Admin
 * @create: 2019-04-12 17:58
 **/

@Component
public class MongoDBClientVerticle extends AbstractVerticle {

    private static final Logger log= LoggerFactory.getLogger(MongoDBClientVerticle.class);

    @Autowired
    InitConfig initConfig;

    private MongoClient mongoClient;

    @Override
    public void start() throws Exception {
        JsonObject config = Vertx.currentContext().config();
        String uri = "mongodb://";
        uri += AppConfig.mongocfg.getHost();
        uri += ":";
        uri += AppConfig.mongocfg.getPort();

        String db = AppConfig.mongocfg.getAuthSource();

        JsonObject mongoconfig = new JsonObject()
                .put("connection_string", uri)
                .put("db_name", db)
                .put("username", AppConfig.mongocfg.getUserName())
                .put("password", AppConfig.mongocfg.getPassWord());

        mongoClient = MongoClient.createShared(vertx, mongoconfig);
        mongoClient.getCollections(res -> {
            if (res.succeeded()) {
                List<String> collections = res.result();
                if (!collections.contains(AppConfig.LOG_COLLECTION)) {
                    createCollection(AppConfig.LOG_COLLECTION);
                }
            } else {
                res.cause().printStackTrace();
            }
        });


    }

    public void createCollection(String var) {
        mongoClient.createCollection(var, res-> {
            if (res.succeeded()) {
                log.info(">>> create collection: {} success.", var);
            } else {
                res.cause().printStackTrace();
                log.error(">>> create collection: {} failed.", var);
            }
        });
    }
}