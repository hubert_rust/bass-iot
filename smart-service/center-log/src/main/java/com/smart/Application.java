package com.smart;

import com.smart.service.MongoDBClientVerticle;
import io.vertx.core.Vertx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

/**
 * @program: smart-service
 * @description: app
 * @author: Admin
 * @create: 2019-04-16 18:21
 **/

@SpringBootApplication(scanBasePackages = {"com.smart"})
public class Application {
    private static final Logger log = LoggerFactory.getLogger(Application.class);
    @Autowired
    private MongoDBClientVerticle mongoDBClientVerticle;
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    @PostConstruct
    public void deploy() {
        log.info(">>> Application, deploy.");
        Vertx.vertx().deployVerticle(mongoDBClientVerticle);
    }
}