package com.smart.framework.commonpool;

public abstract class Task {
    protected static ThreadLocal<Object> objectThreadLocal = new ThreadLocal<>();

    protected Object meterHelper;
    protected String dcName;
    protected String dcId;
    protected String mainOrder;
    protected String hubAddress;
    protected String jobHandleName;
    protected String hubType;
    protected String pluginId;
    protected String commandCode;
    protected String taskState; //"init", "first-retry"

    public abstract  void process() throws Exception;
    public void retry() throws Exception {}

    public void setObject(Object obj) {
        objectThreadLocal.set(obj);
    }
    public Object getObject() {
        return objectThreadLocal.get();
    }


    public Object getMeterHelper() {
        return meterHelper;
    }

    public void setMeterHelper(Object meterHelper) {
        this.meterHelper = meterHelper;
    }


    public String getCommandCode() {
        return commandCode;
    }

    public void setCommandCode(String commandCode) {
        this.commandCode = commandCode;
    }

    public String getTaskState() {
        return taskState;
    }

    public void setTaskState(String taskState) {
        this.taskState = taskState;
    }

    public String getDcName() {
        return dcName;
    }

    public void setDcName(String dcName) {
        this.dcName = dcName;
    }

    public String getDcId() {
        return dcId;
    }

    public void setDcId(String dcId) {
        this.dcId = dcId;
    }

    public String getMainOrder() {
        return mainOrder;
    }

    public void setMainOrder(String mainOrder) {
        this.mainOrder = mainOrder;
    }

    public String getHubAddress() {
        return hubAddress;
    }

    public void setHubAddress(String hubAddress) {
        this.hubAddress = hubAddress;
    }

    public String getJobHandleName() {
        return jobHandleName;
    }

    public void setJobHandleName(String jobHandleName) {
        this.jobHandleName = jobHandleName;
    }

    public String getHubType() {
        return hubType;
    }

    public void setHubType(String hubType) {
        this.hubType = hubType;
    }

    public String getPluginId() {
        return pluginId;
    }

    public void setPluginId(String pluginId) {
        this.pluginId = pluginId;
    }

    @Override
    public String toString() {
        return "CommonTask{" +
                "meterHelper=" + meterHelper +
                ", mainOrder='" + mainOrder + '\'' +
                ", hubAddress='" + hubAddress + '\'' +
                ", hubType='" + hubType + '\'' +
                ", pluginId='" + pluginId + '\'' +
                ", commandCode='" + commandCode + '\'' +
                '}';
    }
}
