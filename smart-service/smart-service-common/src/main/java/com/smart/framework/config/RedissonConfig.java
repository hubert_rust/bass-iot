package com.smart.framework.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@SpringBootConfiguration
public class RedissonConfig {

	@Autowired
	private Environment env;
	
	//@Bean(name="redissonClient",destroyMethod="shutdown")
	@Bean(name="redissonClient")
	public RedissonClient redissonClient() throws IOException {
		String[] profiles = env.getActiveProfiles();
		String profile = "";
		if(profiles.length > 0) {
			profile = "-" + profiles[0];
		}

		String filePath = System.getProperty("user.dir");
		filePath += File.separator;
		filePath += ("config" + File.separator + "conf" + File.separator + "redisson");
		filePath += profile;
		filePath += ".yaml";
		File file = new File(filePath);
		RedissonClient ret = null;
		if (file.exists()) {
			ret = Redisson.create(
					Config.fromYAML(new FileInputStream(filePath))
			);
		}
		else {
			ret = Redisson.create(
					Config.fromYAML(new ClassPathResource("/conf/redisson" + profile + ".yaml").getInputStream())
			);
		}

		return ret;
	}
}
