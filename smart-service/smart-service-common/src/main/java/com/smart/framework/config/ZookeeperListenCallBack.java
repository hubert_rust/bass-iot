package com.smart.framework.config;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.TreeCache;
import org.apache.curator.framework.recipes.cache.TreeCacheEvent;
import org.apache.curator.framework.recipes.cache.TreeCacheListener;
import org.apache.curator.utils.ZKPaths;
import org.apache.zookeeper.CreateMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-15 08:03
 **/
@Component
public class ZookeeperListenCallBack {
    @Autowired
    private CuratorFramework curatorFramework;

    private TreeCache treeCache;

    public void startup(IEventCallback cb) {
        try {
            if (curatorFramework.checkExists().forPath("/datas") == null) {
                curatorFramework.create().withMode(CreateMode.PERSISTENT).forPath("/datas");
            }
            if (curatorFramework.checkExists().forPath("/datas/hubs") == null) {
                curatorFramework.create().withMode(CreateMode.PERSISTENT).forPath("/datas/hubs");
            }
            if (curatorFramework.checkExists().forPath("/datas/meters") == null) {
                curatorFramework.create().withMode(CreateMode.EPHEMERAL).forPath("/datas/meters");
            }

            treeCache = new TreeCache(curatorFramework, "/datas");
            treeCache.start();
            TreeCacheListener treeCacheListener = new TreeCacheListener() {
                @Override
                public void childEvent(CuratorFramework client, TreeCacheEvent event) throws Exception {
                    cb.process(client, event);
                    /*switch (event.getType()) {

                        case NODE_ADDED:
                            System.out.println("Node add " + ZKPaths.getNodeFromPath(event.getData().getPath()));
                            break;
                        case NODE_REMOVED:
                            System.out.println("Node removed " + ZKPaths.getNodeFromPath(event.getData().getPath()));
                            break;
                        case NODE_UPDATED:
                            System.out.println("Node updated " + ZKPaths.getNodeFromPath(event.getData().getPath()));
                            break;

                    }*/
                }
            };
            treeCache.getListenable().addListener(treeCacheListener);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
