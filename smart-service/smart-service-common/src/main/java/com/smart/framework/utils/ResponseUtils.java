package com.smart.framework.utils;

import com.google.common.collect.Maps;
import com.google.gson.Gson;

import java.util.Map;
import java.util.Objects;

/**
 * @program: smart-service
 * @description: response
 * @author: Admin
 * @create: 2019-04-22 17:01
 **/
public class ResponseUtils {
    public static ResponseHolder getResponseBody() {
        return new ResponseHolder();
    }

    public static String getFailResponse(String msg) {
        return ResponseUtils.getResponseBody()
                .put(PFConstDef.RESP_RETURN_CODE, PFConstDef.RESP_RETURN_CODE_FAIL)
                .put(PFConstDef.RESP_RETURN_MSG, msg)
                .getResponseBody();
    }
    public static String getSuccessResponse(String msg) {
        return ResponseUtils.getResponseBody()
                .put(PFConstDef.RESP_RETURN_CODE, PFConstDef.RESP_RETURN_CODE_SUCCESS)
                .put(PFConstDef.RESP_RETURN_MSG, msg)
                .getResponseBody();
    }

    public static class ResponseHolder{
        private Map<String, Object> map = Maps.newHashMap();
        public ResponseHolder put(String key, Object obj) {
            if (Objects.isNull(obj)) return this;
            map.put(key, obj);
            return this;
        }
        public Map getRawResponseBody() {
            return map;
        }
        public String getResponseBody() {
            return new Gson().toJson(map);
        }

    }
}