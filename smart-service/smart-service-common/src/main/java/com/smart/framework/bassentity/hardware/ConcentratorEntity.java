package com.smart.framework.bassentity.hardware;

import com.smart.request.IBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @program: smart-service
 * @description: 集中器信息
 * @author: Admin
 * @create: 2019-04-22 15:28
 **/
@Data
public class ConcentratorEntity extends IBaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    private String dcName;
    private String dcId;
    protected String remark;
    private String pluginId;
    private String pluginName;
    private String hubName;
    private String hubAddress;
    private String hubType;
    private String hubIp;
    private String hubState = "active";
    private String runState = "nok";
    private String commPort;
    private int bps;

}