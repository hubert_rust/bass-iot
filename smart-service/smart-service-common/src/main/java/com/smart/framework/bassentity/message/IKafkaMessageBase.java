package com.smart.framework.bassentity.message;

import java.io.Serializable;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-16 14:11
 **/
public class IKafkaMessageBase<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    private String messageTopic;
    private T data;

    public String getMessageTopic() { return messageTopic; }
    public void setMessageTopic(String messageTopic) { this.messageTopic = messageTopic; }
    public T getData() { return data; }
    public void setData(T data) { this.data = data; }
}
