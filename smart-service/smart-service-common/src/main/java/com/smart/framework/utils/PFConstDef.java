package com.smart.framework.utils;

/**
 * @program: smart-service
 * @description: const
 * @author: Admin
 * @create: 2019-04-22 17:11
 **/
public class PFConstDef {

    public static final String RESP_RETURN_CODE = "returnCode";
    public static final String RESP_RETURN_MSG = "returnMsg";
    public static final String RESP_RETURN_DATAS = "datas";

    public static final String RESP_RETURN_CODE_SUCCESS = "SUCCESS";
    public static final String RESP_RETURN_CODE_FAIL= "FAIL";

    //维护类操作命令
    public static final String REQ_CMD_CODE_ADD = "ADD";
    public static final String REQ_CMD_CODE_MOD = "MOD";
    public static final String REQ_CMD_CODE_DEL = "DEL";
    public static final String REQ_CMD_CODE_QRY = "QRY";
    public static final String REQ_CMD_CODE_SET_PRICE = "SET-PRICE";

    //自动同步
    public static final String REQ_CMD_CODE_TYPE_AUTO ="AUTO";
    public static final String REQ_CMD_CODE_TYPE_SYNC = "SYNC";


    //硬件类型, subType
    public static final String REQ_HARDWARE_TYPE_HUB = "HUB";
    public static final String REQ_HARDWARE_TYPE_METER = "METER";
    public static final String REQ_HARDWARE_TYPE_HOT_METER = "HOT-METER";
    public static final String REQ_HARDWARE_TYPE_COLD_METER = "COLD-METER";
    public static final String REQ_HARDWARE_TYPE_COLD_READ = "COLD-READ";
    public static final String REQ_HARDWARE_TYPE_COLD_BALANCE = "COLD-BALANCE";

    public static final String REQ_HARDWARE_TYPE_HOT_READ = "HOT-READ";
    public static final String REQ_HARDWARE_TYPE_HOT_BALANCE = "HOT-BALANCE";
    public static final String REQ_HARDWARE_TYPE_HOT_USER = "HOT-USER";

    public static final String REQ_HARDWARE_TYPE_ELEC_METER = "ELEC-METER";
    public static final String REQ_HARDWARE_TYPE_ELEC_READ = "ELEC-READ";
    public static final String REQ_HARDWARE_TYPE_ELEC_BALANCE = "ELEC-BALANCE";
    public static final String REQ_HARDWARE_TYPE_HIGH_METER = "HIGH-METER";
    public static final String REQ_HARDWARE_TYPE_HIGH_READ = "HIGH-READ";
    public static final String REQ_HARDWARE_TYPE_MONITOR = "MONITOR";
    public static final String REQ_HARDWARE_TYPE_PLUGIN = "PLUGIN";
    public static final String REQ_HARDWARE_TYPE_SECRETKEY = "SECRET-KEY";
    public static final String REQ_HARDWARE_TYPE_LOCKER = "locker";
    public static final String REQ_HARDWARE_TYPE_JOB_LOG = "JOB-LOG";
    public static final String REQ_HARDWARE_TYPE_METER_RECHARGE = "METER-RECHARGE";

}