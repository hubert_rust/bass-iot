package com.smart.framework.commonpool;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Worker  extends Thread{
    private static final Logger LOGGER = LoggerFactory.getLogger(Worker.class);
    public Task task;
    public long sleepSpan = 60 *1000;

    public Worker(Task task, long sleepSpan) {
        this.task=task;
        this.sleepSpan = sleepSpan;
    }

    @Override
    public void run() {
        while (true) {
            try { sleep(sleepSpan); } catch (InterruptedException e) { e.printStackTrace(); }

            try {
                task.process();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
