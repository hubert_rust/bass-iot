package com.smart.framework.commonpool;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AsyncNotifyThreadPool {

    private static final Logger LOGGER = LoggerFactory.getLogger(AsyncNotifyThreadPool.class);

    public static ExecutorService executor = Executors.newFixedThreadPool(10);
    public static ExecutorService hubExecutor = Executors.newFixedThreadPool(20);

    public static void initThreadPool(int num) {

    }
    public static void submit(Thread worker) {
        executor.submit(worker);
    }
    public static void submitHubPool(Thread worker) { hubExecutor.submit(worker); }

}
