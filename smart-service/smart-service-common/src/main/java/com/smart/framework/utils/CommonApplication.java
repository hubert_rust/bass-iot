package com.smart.framework.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;


@Component
public class CommonApplication implements ApplicationContextAware {
    private static ApplicationContext appContext;

    public static ApplicationContext getApplicationContext() {
        checkValid();
        return appContext;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        appContext=applicationContext;
        checkValid();
        //Map<String, IPayChannel> map = appContext.getBeansOfType(IPayChannel.class);

    }

    public static void checkValid() {
        if (appContext == null) throw new IllegalStateException("ApplicationContex注入失败");
    }

    public static <T> T getBean(String name) {
        checkValid();
        return  (T) appContext.getBean(name);
    }

    public static  <T> T getBean(Class<?> cls) {
        checkValid();
        String sip = cls.getSimpleName();
        return getBean(sip);
        //return appContext.getBean(cls);
    }
}
