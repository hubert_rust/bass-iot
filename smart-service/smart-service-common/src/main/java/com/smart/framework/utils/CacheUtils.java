package com.smart.framework.utils;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.smart.DBWatchMessage;
import com.smart.dbwatch.DBWatchMessageEntity;
import org.redisson.Redisson;
import org.redisson.api.RBucket;
import org.redisson.api.RKeys;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-17 11:41
 **/
@Component
public class CacheUtils {
    private static final Logger log = LoggerFactory.getLogger(CacheUtils.class);

    @Autowired
    private RedissonClient redissonClient;


    public boolean refreshCache(String data) {

        //Type type = new TypeToken<Map<String, Object>>(){}.getType();
        DBWatchMessageEntity<Map<String, Object>> dbWatchMessageEntity = JSONObject.parseObject(data, DBWatchMessageEntity.class);

        String tableName = dbWatchMessageEntity.getTable();
        boolean commit = dbWatchMessageEntity.isCommit();
        if (!commit) {
            log.error(">>> CacheUtils, refreshCache, commit fail, data: {}", data);
        }

        Map<String, Object> mapData = dbWatchMessageEntity.getData();
        String messageType = dbWatchMessageEntity.getType();
        syncCache(tableName, mapData, messageType);

        return true;
    }
    public List getCache(String tableName, String id) {

        Redisson redssion = (Redisson)redissonClient;
        List<Map<String, Object>> retList = Lists.newArrayList();
        String key = tableName + "-" + (Strings.isNullOrEmpty(id) ? "*" : id);
        RLock rLock = redssion.getLock(key.toUpperCase());
        try {
            rLock.lock(5, TimeUnit.SECONDS);

            RKeys keys = redissonClient.getKeys();
            Iterable<String> foundedKeys = keys.getKeysByPattern(key);
            for (String k: foundedKeys) {
                RBucket<Object> rBuckets = redssion.getBucket(k);
                Map<String, Object> retMap = (Map<String, Object>)rBuckets.get();
                Map<String, Object> tmp = Maps.newHashMap();
                tmp.putAll(retMap);
                retList.add(tmp);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            return retList;
        }
        finally {
            if (rLock != null) { rLock.unlock(); }
        }
        return retList;
    }
    public boolean syncCache(String tableName,
                              Map<String, Object> objectMap,
                              String type) {
        Redisson redssion = (Redisson)redissonClient;
        String id = objectMap.get("id").toString();
        String key = tableName + "-" + id;

        RLock rLock = redssion.getLock(key.toUpperCase());
        try {
            rLock.lock(3, TimeUnit.SECONDS);
            RBucket<Object> rBuckets = redssion.getBucket(key);
            if (type.equals(DBWatchMessage.WATCH_MESSAGE_INSERT)) {
                rBuckets.set(objectMap);
            }
            else if (type.equals(DBWatchMessage.WATCH_MESSAGE_UPDATE)) {
                rBuckets.set(objectMap);
            }
            else if (type.equals(DBWatchMessage.WATCH_MESSAGE_DELETE)) {
                rBuckets.delete();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        finally {
            if (rLock != null) { rLock.unlock(); }
        }
        return true;
    }
}
