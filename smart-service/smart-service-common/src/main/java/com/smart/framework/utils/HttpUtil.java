package com.smart.framework.utils;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * Created by Administrator on 2017/2/14.
 */
public class HttpUtil {
    private static final Logger log = LoggerFactory.getLogger(HttpUtil.class);

    /**
     * it is recommended to have a single instance of HttpClient per communication component or even per application.
     */
    private static CloseableHttpClient client;
    private static RequestConfig requestConfig;

    static {
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
        connectionManager.setMaxTotal(300);
        connectionManager.setDefaultMaxPerRoute(50);

        requestConfig = RequestConfig.custom()
                .setSocketTimeout(5)
                .setConnectTimeout(5)
                .setConnectionRequestTimeout(5)
                .build();

        client = HttpClientBuilder.create()
                .setConnectionManager(connectionManager)
                .setConnectionManagerShared(true)
                .setDefaultRequestConfig(requestConfig)
                .setConnectionTimeToLive(10, TimeUnit.MINUTES)
                .build();
    }

    public static CloseableHttpResponse sendHttpRequest(String url, String cookie, String json)
            throws IOException {
        log.debug(">>> send req to {} with cookie {}, data: {}", url, cookie, json);
        HttpPost post = new HttpPost(url);

        // add header
        if (!Strings.isNullOrEmpty(cookie)) {
            post.setHeader("Cookie", cookie);
        }

        if (!Strings.isNullOrEmpty(json)) {
            post.setEntity(new StringEntity(json, ContentType.APPLICATION_JSON));
        }
        CloseableHttpResponse  httpResponse = client.execute(post);
        if(httpResponse.getStatusLine().getStatusCode() != 200){
            log.error(">>> bad response {}", httpResponse.getStatusLine().getStatusCode());
            log.error(">>> request json: {}", json);
        }
        return httpResponse;
    }

    public static void main(String[] args) {
        List<String> list = Lists.newArrayList("");
        //boolean notEmpty = list.stream().anyMatch( i -> !isBlank(i));

        list = null;
    }
}
