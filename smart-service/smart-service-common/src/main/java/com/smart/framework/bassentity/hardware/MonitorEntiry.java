package com.smart.framework.bassentity.hardware;

import com.smart.request.IBaseEntity;
import lombok.Data;


/**
 * @program: smart-service
 * @description: meter信息
 * @author: Admin
 * @create: 2019-04-23 18:42
 **/
@Data
public class MonitorEntiry extends IBaseEntity {
    private static final long serialVersionUID = 1L;

    private String dcName;
    private String dcId;
    protected String remark;
    private String pluginId;
    private String pluginName;
    private String hubAddress;
    private String hubName;
    private String meterNo;
    private String meterName;
    private String meterType;
    private String meterStatus;
    private String showStatus;
    private String meterModel;
    private String meterAddress;
    private String commPort;
    private String bps;
}