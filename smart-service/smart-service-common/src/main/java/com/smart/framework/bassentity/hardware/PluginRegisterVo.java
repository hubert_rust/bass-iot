package com.smart.framework.bassentity.hardware;

import lombok.Data;

import java.io.Serializable;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-05 09:57
 **/
@Data
public class PluginRegisterVo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String ipAddress;
    private int port;
    private String uuid;
    private String serverName;

}
