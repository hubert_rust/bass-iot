package com.smart.framework.cons;

/**
 * @program: service
 * @description: bass相关定义
 * @author: Admin
 * @create: 2019-04-19 11:07
 **/
public class BassConst {

    //bass平台名称
    public static final String BASS_NAME_HARDWARE = "bass-hardware";
    public static final String BASS_NAME_MESSAGE = "bass-message";
    public static final String BASS_NAME_PAY = "bass-pay";

    //BASS_HARDWARE
    public static final String BASS_HARDWARE_SERVICE_NAME_MAINTANCE = "MAINTANCE";
    public static final String BASS_HARDWARD_SERVICE_NAME_PLUGIN = "PLUGIN";
    public static final String BASS_HARDWARD_SERVICE_NAME_FUNC = "FUNC";
    public static final String BASS_HARDWARD_SERVICE_NAME_KEEP = "KEEP";

    //maintance子类：
    public static final String BASS_HARDWARD_SERVICE_MAINTANCE_SUBTYPE_HUB = "hub";
    public static final String BASS_HARDWARD_SERVICE_MAINTANCE_SUBTYPE_METER = "meter";

    //plugin子类：subType
    public static final String BASS_HARDWARD_SERVICE_PLUGIN_SUBTYPE_MT = "MT";
    public static final String BASS_HARDWARD_SERVICE_PLUGIN_SUBTYPE_HEART = "HEART";
    public static final String BASS_HARDWARD_SERVICE_PLUGIN_SUBTYPE_FUNC = "func";

    //plugin对于的cmdType
    public static final String BASS_HARDWARD_SERVICE_PLUGIN_CMDTYPE_HOT = "hot-meter";
    public static final String BASS_HARDWARD_SERVICE_PLUGIN_CMDTYPE_COLD = "cold-meter";
    public static final String BASS_HARDWARD_SERVICE_PLUGIN_CMDTYPE_ELEC = "elec-meter";
    public static final String BASS_HARDWARD_SERVICE_PLUGIN_CMDTYPE_HIGH = "high-meter";

    //keep子类
    public static final String BASS_HARDWARD_SERVICE_KEEP_SUBTYPE_REG = "register";
    public static final String BASS_HARDWARD_SERVICE_KEEP_SUBTYPE_DOWN = "download";
    public static final String BASS_HARDWARD_SERVICE_KEEP_SUBTYPE_HUBSTATE = "hubstate";
    public static final String BASS_HARDWARD_SERVICE_KEEP_SUBTYPE_HEART = "heartbeat";
    public static final String BASS_HARDWARD_SERVICE_KEEP_SUBTYPE_MONITOR = "monitor";

    public static final String RESP_RETURN_CODE = "returnCode";
    public static final String RESP_RETURN_MSG = "returnMsg";

    public static final String RESP_RETURN_CODE_SUCCESS = "SUCCESS";
    public static final String RESP_RETURN_CODE_FAIL= "FAIL";

    //维护类操作命令
    public static final String REQ_CMD_CODE_ADD = "ADD";
    public static final String REQ_CMD_CODE_MOD = "MOD";
    public static final String REQ_CMD_CODE_DEL = "DEL";

    //自动同步
    public static final String REQ_CMD_CODE_TYPE_AUTO ="AUTO";
    public static final String REQ_CMD_CODE_TYPE_SYNC = "SYNC";


    //硬件类型
    public static final String REQ_HARDWARE_TYPE_HUB = "HUB";
    public static final String REQ_HARDWARE_TYPE_METER = "METER";
}