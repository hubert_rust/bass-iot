package com.smart.framework.config;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.TreeCacheEvent;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-15 08:38
 **/
public interface IEventCallback {
    void process(CuratorFramework client, TreeCacheEvent event);
}
