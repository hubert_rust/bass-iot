package com.smart.framework.bassentity.hardware;

import com.smart.request.IBaseEntity;

import java.util.List;

/**
 * @program: service
 * @description:
 * @author: admin
 * @create: 2019-04-29 17:08
 **/
public class BassHardwardRequestXDEP<T> extends IBaseEntity {
    private String cmdCode;       // MAINTANCE :{ADD, DEL, MOD}, FUNC: {OPEN, }
    private String cmdType;       // for sub cmd
    private int autoPush;     //1: auto push to plugin, 0, default:1


    private List<T> entitys;      //可以是Map或者实体类

    public String getCmdCode() { return cmdCode; }
    public void setCmdCode(String cmdCode) { this.cmdCode = cmdCode; }
    public String getCmdType() { return cmdType; }
    public void setCmdType(String cmdType) { this.cmdType = cmdType; }
    public int getAutoPush() { return autoPush; }
    public void setAutoPush(int autoPush) { this.autoPush = autoPush; }
    public List<T> getEntitys() { return entitys; }
    public void setEntitys(List<T> entitys) { this.entitys = entitys; }

    public static InnerBuilder Builder() {
        return new InnerBuilder();
    }


    public static final class InnerBuilder<T> {
        private List<T> entitys;
        private String serviceType;   // MAINTANCE, FUNC,  classify for controller of hardward platform
        private String subType;  // HUB, HOT-METER
        private String cmdCode;       // MAINTANCE :{ADD, DEL, MOD}, FUNC: {OPEN, }
        private String cmdType;       // for sub cmd
        private int autoPush;     //1: auto push to plugin, 0, default:1
        private String serviceOrder;  // create by service side
        private String commandOrder;  // create by hardward platform

        private InnerBuilder() {
        }

        public static InnerBuilder aHardwardReqEntity() {
            return new InnerBuilder();
        }

        public InnerBuilder serviceType(String serviceType) {
            this.serviceType = serviceType;
            return this;
        }

        public InnerBuilder subType(String subType) {
            this.subType = subType;
            return this;
        }

        public InnerBuilder cmdCode(String cmdCode) {
            this.cmdCode = cmdCode;
            return this;
        }

        public InnerBuilder cmdType(String cmdType) {
            this.cmdType = cmdType;
            return this;
        }

        public InnerBuilder autoPush(int autoPush) {
            this.autoPush = autoPush;
            return this;
        }

        public InnerBuilder serviceOrder(String serviceOrder) {
            this.serviceOrder = serviceOrder;
            return this;
        }

        public InnerBuilder commandOrder(String commandOrder) {
            this.commandOrder = commandOrder;
            return this;
        }

        public InnerBuilder entitys(List<T> entitys) {
            this.entitys = entitys;
            return this;
        }


        public BassHardwardRequestXDEP build() {
            BassHardwardRequestXDEP hardwardReqEntity = new BassHardwardRequestXDEP();
            hardwardReqEntity.cmdType = this.cmdType;
            hardwardReqEntity.serviceType = this.serviceType;
            hardwardReqEntity.subType = this.subType;
            hardwardReqEntity.commandOrder = this.commandOrder;
            hardwardReqEntity.autoPush = this.autoPush;
            hardwardReqEntity.entitys = this.entitys;
            hardwardReqEntity.cmdCode = this.cmdCode;
            hardwardReqEntity.serviceOrder = this.serviceOrder;
            hardwardReqEntity.entitys = this.entitys;
            return hardwardReqEntity;
        }
    }
}
