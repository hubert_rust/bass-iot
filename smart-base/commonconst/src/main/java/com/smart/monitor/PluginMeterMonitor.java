package com.smart.monitor;

import java.io.Serializable;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-07-12 14:23
 **/
public class PluginMeterMonitor implements Serializable {
    public static final Long  serialVersionUID = 0L;
    private String meterAddress;
    private String concentratorAddress;
    private int concentratorPort;
    private int concentratorBps;
    private String meterType;
    private int monitorSpan = 600; //秒

    //序号，检测用
    private int seqNum;
    private volatile boolean state = true;
    private volatile Long lastMonitorTime;
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getMonitorSpan() {
        return monitorSpan;
    }

    public void setMonitorSpan(int monitorSpan) {
        this.monitorSpan = monitorSpan;
    }

    public String getMeterAddress() {
        return meterAddress;
    }

    public void setMeterAddress(String meterAddress) {
        this.meterAddress = meterAddress;
    }

    public String getConcentratorAddress() {
        return concentratorAddress;
    }

    public void setConcentratorAddress(String concentratorAddress) {
        this.concentratorAddress = concentratorAddress;
    }

    public int getConcentratorPort() {
        return concentratorPort;
    }

    public void setConcentratorPort(int concentratorPort) {
        this.concentratorPort = concentratorPort;
    }

    public int getConcentratorBps() {
        return concentratorBps;
    }

    public void setConcentratorBps(int concentratorBps) {
        this.concentratorBps = concentratorBps;
    }

    public String getMeterType() {
        return meterType;
    }

    public void setMeterType(String meterType) {
        this.meterType = meterType;
    }

    public int getSeqNum() {
        return seqNum;
    }

    public void setSeqNum(int seqNum) {
        this.seqNum = seqNum;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public Long getLastMonitorTime() {
        return lastMonitorTime;
    }

    public void setLastMonitorTime(Long lastMonitorTime) {
        this.lastMonitorTime = lastMonitorTime;
    }
}
