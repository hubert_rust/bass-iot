package com.smart.common;


import com.smart.event.HWStateEvent;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @program: 内部使用
 * @description: request
 * @author: Admin
 * @create: 2019-04-23 09:18
 **/

public class ResponseState implements Serializable {
    private static final long serialVersionUID = 1L;

    private String returnCode;
    private String returnMsg;
    private List<HWStateEvent> datas;

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public List<HWStateEvent> getDatas() {
        return datas;
    }

    public void setDatas(List<HWStateEvent> datas) {
        this.datas = datas;
    }
}