package com.smart.common;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-10 14:04
 **/
public class MsgMonitor {
    protected long downMsgTm;          //插件层收到请求时间
    protected long upMsgTm;            //插件层收到响应时间,
    protected long downEncodeStartTm;  //编码开始时间
    protected long downEncodeEndTm;    //编码完成时间
    protected long upDecodeStartTm;    //解码开始时间,
    protected long upDecodeEndTm;      //解码完成时间
    protected long downMsgInQueueTm;   //入队列时间,请求消息,下行消息
    protected long downMsgOutQueueTm;  //出队列时间,请求消息,下行消息
    protected long upMsgInQueueTm;     //上行消息入队列时间
    protected long upMsgOutQueueTm;    //上行消息出队列时间


    public long getDownMsgTm() { return downMsgTm; }
    public void setDownMsgTm(long downMsgTm) { this.downMsgTm = downMsgTm; }
    public long getUpMsgTm() { return upMsgTm; }
    public void setUpMsgTm(long upMsgTm) { this.upMsgTm = upMsgTm; }
    public long getDownEncodeStartTm() { return downEncodeStartTm; }
    public void setDownEncodeStartTm(long downEncodeStartTm) { this.downEncodeStartTm = downEncodeStartTm; }
    public long getDownEncodeEndTm() { return downEncodeEndTm; }
    public void setDownEncodeEndTm(long downEncodeEndTm) { this.downEncodeEndTm = downEncodeEndTm; }
    public long getUpDecodeStartTm() { return upDecodeStartTm; }
    public void setUpDecodeStartTm(long upDecodeStartTm) { this.upDecodeStartTm = upDecodeStartTm; }
    public long getUpDecodeEndTm() { return upDecodeEndTm; }
    public void setUpDecodeEndTm(long upDecodeEndTm) { this.upDecodeEndTm = upDecodeEndTm; }
    public long getDownMsgInQueueTm() { return downMsgInQueueTm; }
    public void setDownMsgInQueueTm(long downMsgInQueueTm) { this.downMsgInQueueTm = downMsgInQueueTm; }
    public long getDownMsgOutQueueTm() { return downMsgOutQueueTm; }
    public void setDownMsgOutQueueTm(long downMsgOutQueueTm) { this.downMsgOutQueueTm = downMsgOutQueueTm; }
    public long getUpMsgInQueueTm() { return upMsgInQueueTm; }
    public void setUpMsgInQueueTm(long upMsgInQueueTm) { this.upMsgInQueueTm = upMsgInQueueTm; }
    public long getUpMsgOutQueueTm() { return upMsgOutQueueTm; }
    public void setUpMsgOutQueueTm(long upMsgOutQueueTm) { this.upMsgOutQueueTm = upMsgOutQueueTm; }
}
