package com.smart.common;

import com.google.common.collect.Maps;

import java.beans.Transient;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/*
 * @ program: register-center
 * @ description: 回结果
 * @ author: admin
 * @ create: 2019-03-27 09:30
 **/
public class ResponseEntity<T> {
    private  String returnCode;
    private  String returnMsg;
    private  String resultCode;
    private  String resultMsg;
    private  String commandOrder;
    private  MsgMonitor msgMonitor = new MsgMonitor();
    private Map<String, Object> retBytes;       //码流
    private List<T> datas = new ArrayList<>(); //保存返回结果
    private List<Map<String, Object>> bassList;
    private List<Map<String, Object>> serviceList;
    private List<Map<String, Object>> pluginList;

    /*@Transient
    public static ResponseEntity<Map<String, Object>> getCommandResp(String result) {
        ResponseEntity<Map<String, Object>> resp = new ResponseEntity<>();
        Map<String, Object> map = Maps.newHashMap();
        map.put("result",  result);
        resp.getDatas().add(map);
        return resp;
    }

    @Transient
    public T getCommandRespDatas() {
        return getDatas().get(0);
    }*/
    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public List<T> getDatas() {
        return datas;
    }

    public void setDatas(List<T> datas) {
        this.datas = datas;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMsg() {
        return resultMsg;
    }

    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }

    public String getCommandOrder() {
        return commandOrder;
    }

    public void setCommandOrder(String commandOrder) {
        this.commandOrder = commandOrder;
    }

    public MsgMonitor getMsgMonitor() {
        return msgMonitor;
    }

    public void setMsgMonitor(MsgMonitor msgMonitor) {
        this.msgMonitor = msgMonitor;
    }

    public Map<String, Object> getRetBytes() {
        return retBytes;
    }

    public void setRetBytes(Map<String, Object> retBytes) {
        this.retBytes = retBytes;
    }

    public List<Map<String, Object>> getBassList() {
        return bassList;
    }

    public void setBassList(List<Map<String, Object>> bassList) {
        this.bassList = bassList;
    }

    public List<Map<String, Object>> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<Map<String, Object>> serviceList) {
        this.serviceList = serviceList;
    }

    public List<Map<String, Object>> getPluginList() {
        return pluginList;
    }

    public void setPluginList(List<Map<String, Object>> pluginList) {
        this.pluginList = pluginList;
    }
}
