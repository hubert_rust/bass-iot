package com.smart.common;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @program: service
 * @description:
 * @author: admin
 * @create: 2019-04-29 14:12
 **/
public class BassRespEntity implements Serializable {
    private String returnCode;
    private String returnMsg;
    private String resultCode;
    private String resultMsg;
    private List<Map<String, Object>> datas;
    private List<Map<String, Object>> bassList;
    private List<Map<String, Object>> serviceList;
    private List<Map<String, Object>> pluginList;
    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public List<Map<String, Object>> getDatas() {
        return datas;
    }

    public void setDatas(List<Map<String, Object>> datas) {
        this.datas = datas;
    }

    public List<Map<String, Object>> getBassList() {
        return bassList;
    }

    public void setBassList(List<Map<String, Object>> bassList) {
        this.bassList = bassList;
    }

    public List<Map<String, Object>> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<Map<String, Object>> serviceList) {
        this.serviceList = serviceList;
    }

    public List<Map<String, Object>> getPluginList() {
        return pluginList;
    }

    public void setPluginList(List<Map<String, Object>> pluginList) {
        this.pluginList = pluginList;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMsg() {
        return resultMsg;
    }

    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }
}
