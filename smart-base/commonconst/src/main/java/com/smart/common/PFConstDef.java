package com.smart.common;

/**
 * @program: smart-service
 * @description: const
 * @author: Admin
 * @create: 2019-04-22 17:11
 **/
public class PFConstDef {

    public static final int HTTP_FUN_MILL_TIMEOUT = 15000; //15秒超时
    public static final int HTTP_FUN_MILL_NOTIFY = 3000;   //通知超时3秒
    public static final int HTTP_MILL_TIMEOUT = 10000; //10秒钟
    public static final int HTTP_MILL_FUNC_REQUEST = 8000; //10秒钟

    public static final String READ_TYPE_AUTO = "auto";
    public static final String READ_TYPE_MANUAL = "manual";
    public static final String PLUGIN_MODE_MIX = "mix";
    public static final String PLUGIN_MODE_SINGLE = "single";

    public static final String RESP_RETURN_CODE = "returnCode";
    public static final String RESP_RETURN_MSG = "returnMsg";
    public static final String RESP_RETURN_DATAS = "datas";

    public static final String RESP_RETURN_CODE_SUCCESS = "SUCCESS";
    public static final String RESP_RETURN_CODE_FAIL= "FAIL";

    //维护类操作命令
    public static final String REQ_CMD_CODE_ADD = "ADD";
    public static final String REQ_CMD_CODE_MOD = "MOD";
    public static final String REQ_CMD_CODE_DEL = "DEL";
    public static final String REQ_CMD_CODE_QRY = "QRY";

    //自动同步
    public static final String REQ_CMD_CODE_TYPE_AUTO ="AUTO";
    public static final String REQ_CMD_CODE_TYPE_SYNC = "SYNC";


    //硬件类型
    public static final String REQ_HARDWARE_TYPE_HUB = "HUB";
    public static final String REQ_HARDWARE_TYPE_HOT_METER = "HOT-METER";
    public static final String REQ_HARDWARE_TYPE_COLD_METER = "COLD-METER";
    public static final String REQ_HARDWARE_TYPE_ELEC_METER = "ELEC-METER";
    public static final String REQ_HARDWARE_TYPE_HIGH_METER = "HIGH-METER";
    public static final String REQ_HARDWARE_TYPE_MONITOR = "MONITOR";
    public static final String REQ_HARDWARE_TYPE_PLUGIN = "PLUGIN";
    public static final String REQ_HARDWARE_TYPE_SECRETKEY = "SECRET-KEY";

}