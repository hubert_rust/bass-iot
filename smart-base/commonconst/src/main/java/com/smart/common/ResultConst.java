package com.smart.common;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-06-27 08:44
 **/
public class ResultConst {
    public static final String SUCCESS = "SUCCESS";
    public static final String FAIL = "FAIL";
    public static final String UNKNOW = "UNKNOW";

    public static final String STATE_OK = "ok";
    public static final String STATE_NOK = "nok";

    public static final String RETURN_CODE = "returnCode";
    public static final String RESULT_CODE = "resultCode";
    public static final String RETURN_MSG = "returnMsg";
    public static final String RESULT_MSG = "resultMsg";

    public static final String DATAS = "datas";
    public static final String RESULT = "result";

    public static class ReturnObject {
        String code;
        String message;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }


    public static String getDatasByKey(Map respMap, String key) {
        List<Map<String, Object>> datasList = (List)respMap.get(ResultConst.DATAS);
        Map<String, Object> map = datasList.get(0);
        return Objects.nonNull(map.get(key)) ? map.get(key).toString() : null;
    }
    public static ResultConst.ReturnObject checkResult(Map respMap) {
        ResultConst.ReturnObject ret = new ResultConst.ReturnObject();
        if (respMap == null) {
            ret.setCode(ResultConst.FAIL);
            return ret;
        }

        Object obj = respMap.get(ResultConst.RETURN_CODE);
        ret.setCode(ResultConst.SUCCESS);
        ret.setMessage(ResultConst.SUCCESS);
        if (Objects.nonNull(obj) && obj.toString().equals(ResultConst.SUCCESS)) {
            obj = respMap.get(ResultConst.RESULT_CODE);
            if (Objects.nonNull(obj) && obj.toString().equals(ResultConst.SUCCESS)) {
                List<Map<String, Object>> datasList = (List)respMap.get(ResultConst.DATAS);
                if (Objects.nonNull(datasList) && datasList.size()>0) {
                    obj = datasList.get(0).get("result");
                    if (Objects.nonNull(obj)) {
                        ret.setCode(obj.toString());
                    }
                }
                else {
                    ret.setCode(ResultConst.FAIL);
                    ret.setMessage("");
                }
                //System.out.println(respMap.get(ResultConst.DATAS));
            } else {
                ret.setCode(ResultConst.FAIL);
                ret.setMessage(respMap.get(ResultConst.RESULT_MSG).toString());
            }
        } else {
            ret.setCode(ResultConst.FAIL);
            obj = respMap.get(ResultConst.RETURN_MSG);
            ret.setMessage(Objects.nonNull(obj) ? obj.toString() : ResultConst.FAIL);
        }

        if (Objects.nonNull(respMap.get("retBytes"))) {
            ret.setMessage(ret.getMessage() + respMap.get("retBytes").toString());
        }

        return ret;
    }
}

