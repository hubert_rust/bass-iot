package com.smart.dbwatch;

import java.io.Serializable;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-17 14:12
 **/
public class DBWatchMessageEntity<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    private String database;
    private String table;
    private String type;
    private String ts;
    private String xid;
    private boolean commit;
    private T data;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getXid() {
        return xid;
    }

    public void setXid(String xid) {
        this.xid = xid;
    }

    public boolean isCommit() {
        return commit;
    }

    public void setCommit(boolean commit) {
        this.commit = commit;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    //{"database":"smart_campus_cloud","table":"tb_hardware_hub","type":"insert","ts":1558059451,"xid":16124,"commit":true,"data":{"id":222,"dc_name":"LOULOU","dc_id":1,"plugin_id":"LLL-001","plugin_name":null,"hub_name":null,"hub_address":"000234111","hub_type":"xx","hub_ip":"127.0.0.1","hub_state":"active","run_state":"nok","comm_port":null,"bps":3,"version":0,"hub_param":null,"remark":null,"create_time":"2019-05-17 02:17:31","create_user":null,"modify_time":"2019-05-17 02:17:31","modify_user":null}}

}
