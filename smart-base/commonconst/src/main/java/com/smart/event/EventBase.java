package com.smart.event;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-06-22 10:47
 **/
public class EventBase {
    private String dcName;
    private String dcId;
    private String pluginType;
    private String pluginId;
    private String conAddress;
    private String subAddress;
    private String eventType;
    private String fireTime;
    private String hex;
    private String events;


    public String getEventType() { return eventType; }
    public void setEventType(String eventType) { this.eventType = eventType; }
    public String getFireTime() { return fireTime; }
    public void setFireTime(String fireTime) { this.fireTime = fireTime; }
    public String getHex() { return hex; }
    public void setHex(String hex) { this.hex = hex; }
    public String getEvents() { return events; }
    public void setEvents(String events) { this.events = events; }
}
