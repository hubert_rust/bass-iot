package com.smart.event;

import java.io.Serializable;

/**
 * @program: plugin, 上报状态，在ResponseModel中包装
 * @description:
 * @author: admin
 * @create: 2019-07-12 10:21
 **/
public class HWStateEvent implements Serializable {
    public static final Long serialVersionUID = 1L;

    private String eventType;
    private String mainClass;  //plugin, hub, sub
    private String pluginId;
    private String hubAddress;
    private String subAddress;
    private String state; //na, ok, nok
    private Long fireTime;
    private String meterType;

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getMainClass() {
        return mainClass;
    }

    public void setMainClass(String mainClass) {
        this.mainClass = mainClass;
    }

    public String getPluginId() {
        return pluginId;
    }

    public void setPluginId(String pluginId) {
        this.pluginId = pluginId;
    }

    public String getHubAddress() {
        return hubAddress;
    }

    public void setHubAddress(String hubAddress) {
        this.hubAddress = hubAddress;
    }

    public String getSubAddress() {
        return subAddress;
    }

    public void setSubAddress(String subAddress) {
        this.subAddress = subAddress;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getFireTime() {
        return fireTime;
    }

    public void setFireTime(Long fireTime) {
        this.fireTime = fireTime;
    }

    public String getMeterType() { return meterType; }
    public void setMeterType(String meterType) { this.meterType = meterType; }

    @Override
    public String toString() {
        return "HWStateEvent{" +
                "eventType='" + eventType + '\'' +
                ", mainClass='" + mainClass + '\'' +
                ", meterType='" + meterType + '\'' +
                ", pluginId='" + pluginId + '\'' +
                ", hubAddress='" + hubAddress + '\'' +
                ", subAddress='" + subAddress + '\'' +
                ", state='" + state + '\'' +
                ", fireTime=" + fireTime +
                '}';
    }
}
