package com.smart;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-17 09:51
 **/
public class PluginUseTable {
    public static final String DATABASE_NAME = "smart_campus_cloud";
    public static final String PLUGIN_USE_HUB_TABLE = "tb_hardware_hub";
    public static final String PLUGIN_USE_METER_TABLE = "tb_hardware_meter";
    public static final String HARDWARE_PLUGIN_TABLE = "tb_hardware_plugin_manage";
    public static final String HARDWARE_MONITOR_PARAM = "tb_hardware_monitor_param";
}
