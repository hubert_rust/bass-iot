package com.smart.command;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-15 13:31
 **/
public class CommandCode {

    //hub type
    public static final String HUB_TYPE_HOTMETER = "hot-meter";
    public static final String HUB_TYPE_COLDMETER ="cold-meter";
    public static final String HUB_TYPE_ELECMETER="elec-meter";
    public static final String HUB_TYPE_HIGHMETER = "high-meter";
    public static final String HUB_TYPE_LOCKER = "locker";
    public static final String HUB_TYPE_GATE = "gate";
    public static final String HUB_TYPE_MIX = "mix-meter";
    //meter type same with hub type value
    public static final Map<String, String> MASK_MAP = Maps.newHashMap();
    static {
        MASK_MAP.put(CommandCode.HUB_TYPE_LOCKER, CommandCode.HUB_TYPE_LOCKER);
        MASK_MAP.put(CommandCode.HUB_TYPE_ELECMETER, CommandCode.HUB_TYPE_ELECMETER);
        MASK_MAP.put(CommandCode.HUB_TYPE_COLDMETER, CommandCode.HUB_TYPE_COLDMETER);
        MASK_MAP.put(CommandCode.HUB_TYPE_HOTMETER, CommandCode.HUB_TYPE_HOTMETER);
        MASK_MAP.put(CommandCode.HUB_TYPE_HIGHMETER, CommandCode.HUB_TYPE_HIGHMETER);
        MASK_MAP.put(CommandCode.HUB_TYPE_GATE, CommandCode.HUB_TYPE_GATE);
    }

    //维护命令
    //停止服务
    public static final String MT_SHUTDOWN_SERVER = "MT_STOP_PLUGIN";
    //重启服务器
    public static final String MT_RESTART_SERVER = "MT_START_PLUGIN";
    //重启Netty
    public static final String MT_RESTART_NETTY = "MT_RESTART_NETTY";
    //停止Netty
    public static final String MT_SHUTDOWN_NETTY = "MT_SHUTDOWN_NETTY";
    //更新配置
    public static final String MT_RELOAD_CONFIG = "MT_RELOAD_CONFIG";

    public static final String MT_RELOAD_HARDWARE_DATA = "MT_RELOAD_HARDWARE_DATA";
    public static final String MT_RELOAD_PLUGIN_DATA = "MT_RELOAD_PLUGIN_DATA";


    //public
    //充值检测阶段
    public static final String CMD_METER_RECHARGE_PREPARE = "CMD_METER_RECHARGE_PREPARE";

    public static final String CMD_SEND_HEART_BEAT = "CMD_SEND_HEART_BEAT";
    public static final String CMD_TIME_SYNC = "CMD_TIME_SYNC";
    public static final String CMD_SUB_STATE = "CMD_SUB_STATE";
    public static final String CMD_METER_CLEAR = "CMD_METER_CLEAR";


    //对应热水表{"userNo":"0用户类型+身份证(20位)"}
    public static final String CMD_ON = "CMD_ON";
    public static final String CMD_OFF = "CMD_OFF";

    //热水表
    public static final String CMD_QUERY_METER_SPEC_USER = "CMD_METER_SPEC_USER";       //指定用户
    //绑卡参数: "serviceOrder":"",
    //entitys:{"userUid":"", "userName":"", "cardNo":"", "userType":"", "hubAddress":"", "meterAddress":""}
    public static final String CMD_BIND_USER = "CMD_BIND_USER";   //热水表绑卡, 给业务用
    public static final String CMD_DEL_USER = "CMD_DEL_USER";
    public static final String CMD_ADD_USER = "CMD_ADD_USER";
    public static final String CMD_QUERY_METER_USER = "CMD_METER_USER";
    public static final String CMD_QUERY_UNIT_PRICE = "CMD_UNIT_PRICE";
    public static final String CMD_QUERY_USER_NUMBER = "CMD_USER_NUM";

    public static final String CMD_SET_STORE_BLOCK = "CMD_SET_STORE_BLOCK";
    public static final String CMD_QUERY_STORE_BLOCK = "CMD_STORE_BLOCK";
    public static final String CMD_USE_RECORD_NUM = "CMD_USE_RECORD_NUM";
    public static final String CMD_USE_BATCH_RECORD = "CMD_USE_BATCH_RECORD";
    public static final String CMD_QUERY_METER_STATE = "CMD_METER_STATE";
    public static final String CMD_QUERY_METER_AMOUNT = "CMD_METER_AMOUNT"; //总用量
    public static final String CMD_QUERY_METER_RECHARGE_ALL = "CMD_METER_RECHARGE_ALL"; //总充值量，冷水用


    //表操作
    public static final String CMD_METER_ON = "CMD_METER_ON";
    public static final String CMD_METER_OFF = "CMD_METER_OFF";
    public static final String CMD_METER_RECHARGE = "CMD_METER_RECHARGE";

    //消息解码类型定义
    //普通电表
    public static final String MESSAGE_CODE_SIMPLE_ELEC = "simple_elec_code";
    public static final String MESSAGE_CODE_HIGHPOWER_ELEC = "high_elec_code";
    public static final String MESSAGE_CODE_HOT_WATER = "hot_water_code";
    public static final String MESSAGE_CODE_COLD_WATER = "cold_water_code";
    public static final String MESSAGE_CODE_GATE = "gate_code";
    public static final String MESSAGE_CODE_PIANO = "piano_code";
    public static final String MESSAGE_CODE_LOCK = "lock_code";

    //电表操作，电表线圈状态
    public static final String CMD_QUERY_CIRCLE_STATE = "CMD_CIRCLE_STATE";
    public static final String CMD_QUERY_LEFT_MONEY = "CMD_LEFT_MONEY";
    public static final String CMD_AMOUNT_RECHARGE = "CMD_AMOUNT_RECHARGE"; //查询累计充值金额;
    public static final String CMD_TIMES_RECHARGE = "CMD_TIMES_RECHARGE";   //查询累计充值次数;
    public static final String CMD_CTRL_STATE = "CMD_CTRL_STATE";           //查询电表控制状态;
    public static final String CMD_TIMING_BRAKE_STATE = "CMD_TIMING_BRAKE_STATE";     //查询电表定时拉闸状态;
    public static final String CMD_THRESHOLD_POWER = "CMD_THRESHOLD_POWER"; //查询门限功率
    public static final String CMD_SINGLE_THRESHOLD_POWER = "CMD_SINGLE_THRESHOLD_POWER"; //查询门限功率

    public static final String CMD_SET_UNIT_PRICE = "CMD_SET_UNIT_PRICE"; //设置单价
    public static final String CMD_SET_TIMING_OPENCLOSE = "CMD_SET_TIMING_OPENCLOSE"; //设置定时开闸关闸
    public static final String CMD_SET_PROTECT_ELEC = "CMD_SET_PROTECT_ELEC";         //执行保电
    public static final String CMD_CANCEL_PROTECT_ELEC = "CMD_CANCEL_PROTECT_ELEC";   //取消保电
    public static final String CMD_SET_LIMIT_ELEC = "CMD_SET_LIMIT_ELEC";             //执行限电
    public static final String CMD_CANCEL_LIMIT_ELEC = "CMD_CANCEL_LIMIT_ELEC";       //取消限电
    public static final String CMD_OVER_POWER_SET = "CMD_OVER_POWER_SET";             //超功率设置
    public static final String CMD_OVER_POWER_CLEAR = "CMD_OVER_POWER_CLEAR";         //超功率清除

    //冷水表命令
    public static final String COLD_CLEAR = CMD_METER_CLEAR;

    //大功率电表
    //开户
    public static final String HIGH_POWER_OPEN_ACCOUNT   = "HIGH_POWER_OPEN_ACCOUNT";
    //状态
    public static final String HIGH_POWER_STATE          = "HIGH_POWER_STATE";
    //剩余量
    public static final String HIGH_POWER_LEFT_AMOUNT    = "HIGH_POWER_LEFT_AMOUNT";
    //public static final String HIGH_POWER_BALANCE_CLEAR  = "HIGH_POWER_BALANCE_CLEAR";
    //充值次数
    public static final String HIGH_POWER_RECHARGE_TIMES = "HIGH_POWER_RECHARGE_TIMES";
    //充值
    public static final String HIGH_POWER_RECHARGE       = CMD_METER_RECHARGE;
    //开
    public static final String HIGH_POWER_REMOTE_ON      = "HIGH_POWER_REMOTE_ON";
    //关
    public static final String HIGH_POWER_REMOTE_OFF     = "HIGH_POWER_REMOTE_OFF";
    public static final String HIGH_POWER_TIME_SYNC      = CMD_TIME_SYNC;
    //抄表
    public static final String HIGH_POWER_READ_METER     = "HIGH_POWER_READ_METER";

    //销户
    public static final String HIGH_POWER_CLOSE_ACCOUNT  = "HIGH_POWER_CLOSE_ACCOUNT";
    public static final String HIGH_POWER_QUERY_TIME     = "HIGH_POWER_QUERY_TIME";
    public static final String HIGH_POWER_QUERY_DATE     = "HIGH_POWER_QUERY_DATE";
    //价格在业务层
    //public static final String HIGH_POWER_GET_PRICE      = "HIGH_POWER_GET_PRICE";
    //public static final String HIGH_POWER_SET_PRICE      = "HIGH_POWER_SET_PRICE";


    //琴房门锁-begin
    public static final String CMD_LOCK_TIME_SYNC = "CMD_LOCK_TIME_SYNC";
    public static final String CMD_LOCK_GET_TIME = "CMD_LOCK_GET_TIME";
    //设置管理员密码(10个)
    public static final String CMD_LOCK_SET_ADMIN_PWD = "CMD_LOCK_SET_ADMIN_PWD";
    //设置管理员卡号(10个)
    public static final String CMD_LOCK_SET_ADMIN_CARDNO = "CMD_LOCK_SET_ADMIN_CARDNO" ;
    //远程开锁
    public static final String CMD_LOCK_REMOTE_UNLOCK = "CMD_LOCK_REMOTE_UNLOCK";
    //查询状态
    public static final String CMD_LOCK_QUERY_STATE = "CMD_LOCK_QUERY_STATE";

    //更新锁地址列表，在集中器中也维护了绑定的锁
    public static final String CMD_LOCK_UPDATE_LOCK_ADDRLIST = "CMD_LOCK_UPDATE_LOCK_ADDRLIST";

    //更新临时卡
    public static final String CMD_LOCK_UPDATE_TEMP_CARD = "CMD_LOCK_UPDATE_TEMP_CARD";

    //远程重启集中器
    public static final String CMD_LOCK_REMOTE_RESTART_HUB = "CMD_LOCK_REMOTE_RESTART_HUB";


    //pending
    //设置离线密码密钥
    public static final String CMD_LOCK_SET_OFFLINE_SECRETKEY = "CMD_LOCK_SET_OFFLINE_SECRETKEY";
    //远程更新软件
    //目前是将需要更新的软件包在管理层func对应的jar包目录下
    public static final String CMD_LOCK_UPDATE_SOFTWARE = "CMD_LOCK_UPDATE_SOFTWARE";
    public static final String CMD_LOCK_TRANSMIT_DATA = "CMD_LOCK_TRANSMIT_DATA";

    public static final String UPGRADE_STATE_BEGIN = "01";
    public static final String UPGRADE_STATE_FINISH = "02";


    public static final Integer UPGRADE_TYPE_NO = 0;
    public static final Integer UPGRADE_TYPE_HUB = 1;      //升级集中器应用软件包
    public static final Integer UPGRADE_TYPE_LOCKER = 2;   //升级锁应用软件包
    public static final Integer UPGRADE_TYPE_HUB_SYS = 3;  //升级集中器底层软件
    public static final Integer UPGRADE_TYPE_LOCKER_SYS = 4; //升级锁底层软件

    //琴房门锁-end

    //闸机-begin
    public static final String GATE_INOUT_TYPE_SWIPE = "swipe";     //刷卡
    public static final String GATE_INOUT_TYPE_BARCODE = "barcode"; //条形码
    public static final String GATE_INOUT_STATUS_IN = "in";         //进
    public static final String GATE_INOUT_STATUS_OUT = "out";       //出
    public static final String GATE_INOUT_STATUS_NOIN = "no-in";    //未进
    public static final String GATE_INOUT_STATUS_NOOUT = "no-out";  //未出

    public static final String GATE_CARD_TYPE_MONTH = "0";
    public static final String GATE_CARD_TYPE_TEMP = "2";
    public static final String GATE_CARD_TYPE_ONCE = "1";
    public static final String GATE_CARD_STATE_STOP = "0";
    public static final String GATE_CARD_STATE_NORAML = "1";

    //参数设置0xA0
    public static final String CMD_GATE_READ_CHANNEL_PARAM = "CMD_GATE_READ_CHANNEL_PARAM"; //读取通道参数

    //用户管理0xA1
    //datas中字段:{"cardNo": "", "cardType":"", "cardStatus":"", "startTime:"20190701123212",
    // "endTime": "20191220124012"}
    public static final String CMD_GATE_ADD_USER = "CMD_GATE_ADD_USER"; //绑卡，就是添加用户
    //datas中字段: {"cardNo":""}
    public static final String CMD_GATE_DELETE_USER = "CMD_GATE_DELETE_USER"; //删除用户
    public static final String CMD_GATE_DELETE_ALL_USER = "CMD_GATE_DELETE_ALL_USER"; //删除所有用户

    //远程控制0xA2
    //dir 0:进 ,1出
    //datas中字段: {"cardNo":"", "dir": "0"}
    public static final String CMD_GATE_REMOTE_OPEN = "CMD_GATE_REMOTE_OPEN";            //远程开闸
    //dir 0:进 ,1出
    //datas中字段: {"cardNo":"", "dir": "0"}
    public static final String CMD_GATE_BAR_FREE_ENABLE = "CMD_GATE_BAR_FREE_ENABLE";    //无障碍模式开启
    public static final String CMD_GATE_BAR_FREE_DISABLE = "CMD_GATE_BAR_FREE_DISABLE";  //无障碍模式关闭

    public static final String CMD_GATE_REMOTE_ALWAYS_OPEN_ENABLE = "CMD_GATE_REMOTE_ALWAYS_OPEN_ENABLE";     //远程常开打开
    public static final String CMD_GATE_REMOTE_ALWAYS_OPEN_DISABLE = "CMD_GATE_REMOTE_ALWAYS_OPEN_DISABLE";   //远程常开取消
    public static final String CMD_GATE_REMOTE_ALWAYS_CLOSE_ENABLE = "CMD_GATE_REMOTE_ALWAYS_CLOSE_ENABLE";   //远程常关打开
    public static final String CMD_GATE_REMOTE_ALWAYS_CLOSE_DISABLE = "CMD_GATE_REMOTE_ALWAYS_CLOSE_DISABLE"; //远程常关取消

    //记录管理0xA3
    public static final String CMD_GATE_GET_UNREAD_RECORD = "CMD_GATE_GET_UNREAD_RECORD";   //顺序读取未读记录
    public static final String CMD_GATE_GET_NEW_RECORD = "CMD_GATE_GET_NEW_RECORD";         //读取最新记录
    public static final String CMD_GATE_CLEAR_ALL_RECORD = "CMD_GATE_CLEAR_ALL_RECORD";     //清除所有记录(无法恢复)

    //闸机-end

}
