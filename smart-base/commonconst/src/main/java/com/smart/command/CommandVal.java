package com.smart.command;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-05-22 11:23
 **/
public class CommandVal {
    public static final String OPEN = "OPEN";
    public static final String CLOSE = "CLOSE";

    public static final String SET_PROTECT_ELEC = "SET_PROTECT_ELEC";
    public static final String CANCEL_PROTECT_ELEC = "CANCEL_PROTECT_ELEC";
    public static final String SET_LIMIT_ELEC = "SET_LIMIT_ELEC";
    public static final String CANCEL_LIMIT_ELEC = "CANCEL_LIMIT_ELEC";
    public static final String OVER_POWER_CLEAR = "OVER_POWER_CLEAR";

    public static final String MAIN_CLASS_PLUGIN = "plugin";
    public static final String MAIN_CLASS_HUB = "hub";
    public static final String MAIN_CLASS_METER = "meter";

    //状态上报
    public static final String EVENT_TYPE_STATE = "statereport";
}
