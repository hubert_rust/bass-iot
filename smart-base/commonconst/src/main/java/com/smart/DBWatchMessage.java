package com.smart;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-17 10:11
 **/
public class DBWatchMessage {
    public static final String WATCH_MESSAGE_FD_DATABASE = "database";
    public static final String WATCH_MESSAGE_FD_TABLE = "table";
    public static final String WATCH_MESSAGE_FD_TYPE = "type";
    public static final String WATCH_MESSAGE_FD_COMMIT = "commit";
    public static final String WATCH_MESSAGE_FD_DATA = "data";


    public static final String WATCH_MESSAGE_INSERT = "insert";
    public static final String WATCH_MESSAGE_UPDATE = "update";
    public static final String WATCH_MESSAGE_DELETE = "delete";
    //{"database":"smart_campus_cloud","table":"tb_hardware_hub","type":"insert","ts":1558059451,"xid":16124,"commit":true,"data":{"id":222,"dc_name":"LOULOU","dc_id":1,"plugin_id":"LLL-001","plugin_name":null,"hub_name":null,"hub_address":"000234111","hub_type":"xx","hub_ip":"127.0.0.1","hub_state":"active","run_state":"nok","comm_port":null,"bps":3,"version":0,"hub_param":null,"remark":null,"create_time":"2019-05-17 02:17:31","create_user":null,"modify_time":"2019-05-17 02:17:31","modify_user":null}}
    //{"database":"smart_campus_cloud","table":"tb_bass_dc_manage","type":"update","ts":1558058244,"xid":13004,"commit":true,"data":{"id":1,"dc_name":"CUNJIN","dc_flag":"CJ","version":1,"remark":"测试","create_user":"admin","create_time":"2019-05-17 01:57:24","modify_user":null,"modify_time":"0000-00-00 00:00:00"},"old":{"remark":"测试1","create_time":"2019-05-17 01:52:58"}}
    //{"database":"smart_campus_cloud","table":"tb_bass_dc_manage","type":"delete","ts":1558053974,"xid":1912,"commit":true,"data":{"id":3,"dc_name":"YINXIN","dc_flag":"YX","version":2,"remark":null,"create_user":null,"create_time":"2019-05-17 00:44:54","modify_user":null,"modify_time":"2019-05-17 00:42:15"}}
}
