package com.smart.ann;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface TaskService {

    //对接meter件类型:
    String meterType();
    String cmdCode();
    String readType();
    //String readType();

}