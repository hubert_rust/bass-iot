package com.smart.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;

import java.util.Map;

public class MapEntityUtil {
    public static Object mapToObject(Map<String, Object> map, Class<?> beanClass) throws Exception {
        Preconditions.checkArgument(map != null, "mapToObject, map is null");
        ObjectMapper objectMapper = new ObjectMapper();
        Object obj = objectMapper.convertValue(map, beanClass);
        return obj;
    }

    public static Map<String, Object> objToMap(Object obj) throws Exception{
        Preconditions.checkArgument(obj != null, "objToMap, obj is null");
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> map = objectMapper.convertValue(obj, Map.class);
        return map;
    }
/*    public static Object map2Object(Map<String, Object> map, Class<?> beanClass) throws Exception {
        Preconditions.checkArgument(map != null, "map2Object, map is null");
        Object obj = beanClass.newInstance();
        org.apache.commons.beanutils.BeanUtils.populate(obj, map);
        return obj;
    }

    public static Map<?, ?> object2Map(Object obj) {
        return obj == null ? null : new org.apache.commons.beanutils.BeanMap(obj);
    }*/
}
