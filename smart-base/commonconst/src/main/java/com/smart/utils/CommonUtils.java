package com.smart.utils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.smart.common.ResponseEntity;
import com.smart.common.ResponseModel;
import com.smart.request.BassHardwardRequest;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-07-04 12:39
 **/
public class CommonUtils {

    public static <T> T mapToEntity(Map<String, Object> map, Class<T> entity) {
        T t = null;
        try {
            t = entity.newInstance();
            for (Field field : entity.getDeclaredFields()) {
                if (map.containsKey(field.getName())) {
                    boolean flag = field.isAccessible();
                    field.setAccessible(true);
                    Object object = map.get(field.getName());
                    if (object != null && field.getType().isAssignableFrom(object.getClass())) {
                        field.set(t, object);
                    }
                    field.setAccessible(flag);
                }
            }
            return t;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return t;
    }


    public static BigDecimal convertMoney(int money) {
        BigDecimal ret =  new BigDecimal(money/100f);
        ret = ret.setScale(2, RoundingMode.HALF_UP);
        return ret;
    }

    public static void setVal(ResponseEntity responseModel, String key, String val) {
        Map<String, Object> map = (Map)responseModel.getDatas().get(0);
        map.put(key, val);
    }

    public static void setBassRequestDatas(BassHardwardRequest request, String key, String val) {
        List<Map<String, Object>> list =  request.getEntitys();
        Map<String, Object> map = list.get(0);

        map.put(key, val);
    }
    public static String getBassRequestDatas(BassHardwardRequest request, String key) {
       List<Map<String, Object>> list =  request.getEntitys();
       Map<String, Object> map = list.get(0);
       return (Objects.nonNull(map.get(key))) ? map.get(key).toString() : null;
    }
    public static ResponseEntity<Map<String, Object>> getResponseModel() {
        ResponseEntity<Map<String, Object>> responseModel = new ResponseEntity<>();
        List<Map<String, Object>> list = Lists.newArrayList();
        Map<String, Object> map = Maps.newHashMap();
        list.add(map);
        responseModel.setDatas(list);
        return responseModel;
    }
}
