package com.smart.request;

/**
 * @program: service
 * @description:
 * @author: admin
 * @create: 2019-04-29 17:52
 **/
public class IBaseEntity {

    protected String dcName;
    protected String serviceType;   // MAINTANCE, PLUGIN,  就是平台的服务名称classify for controller of hardward platform
    protected String subType;       //子类，硬件平台：HUB，HOT-METER
    protected String serviceOrder;  // create by service side
    protected String commandOrder;  // create by hardward platform
    protected String serviceId;     //主要针对plugin,
    protected String hubAddress;    //主要是针对plugin,
    protected String subAddress;    //和集中器相连设备地址
    protected String reserved;      //预留字段
    protected String operType = "MANUAL";      //MANUAL(默认值), AUTO
    protected int isRetSendBytes;   //
    protected int isRetRespBytes;

    public String getDcName() {
        return dcName;
    }

    public void setDcName(String dcName) {
        this.dcName = dcName;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public String getServiceOrder() {
        return serviceOrder;
    }

    public void setServiceOrder(String serviceOrder) {
        this.serviceOrder = serviceOrder;
    }

    public String getCommandOrder() {
        return commandOrder;
    }

    public void setCommandOrder(String commandOrder) {
        this.commandOrder = commandOrder;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getHubAddress() {
        return hubAddress;
    }

    public void setHubAddress(String hubAddress) {
        this.hubAddress = hubAddress;
    }

    public String getSubAddress() { return subAddress; }
    public void setSubAddress(String subAddress) { this.subAddress = subAddress; }

    public String getReserved() {
        return reserved;
    }

    public void setReserved(String reserved) {
        this.reserved = reserved;
    }

    public String getOperType() {
        return operType;
    }

    public void setOperType(String operType) {
        this.operType = operType;
    }

    public int getIsRetSendBytes() { return isRetSendBytes; }
    public void setIsRetSendBytes(int isRetSendBytes) { this.isRetSendBytes = isRetSendBytes; }
    public int getIsRetRespBytes() { return isRetRespBytes; }
    public void setIsRetRespBytes(int isRetRespBytes) { this.isRetRespBytes = isRetRespBytes; }

}
