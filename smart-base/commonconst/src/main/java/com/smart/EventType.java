package com.smart;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-07-04 09:33
 **/
public class EventType {
    public static final String EVENT_TYPE_LOCKER_COMMON = "locker-common";
    public static final String EVENT_TYPE_LOCKER_UPGRADE = "locker-upgrade";
    public static final String EVENT_TYPE_JOB_LOG = "job-log";
    public static final String EVENT_TYPE_MAIN_JOB_LOG = "main-job-log";
}
