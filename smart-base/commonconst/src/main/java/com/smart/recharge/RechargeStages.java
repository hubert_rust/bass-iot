package com.smart.recharge;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-07-25 13:50
 **/
public class RechargeStages {
    public static final String RECHARGE_STAGES_PREPARE = "prepare";
    public static final String RECHARGE_STAGES_RECHARGING = "recharging";
    public static final String RECHARGE_STAGES_RECHARGE_NOTIFY = "notify";
    public static final String RECHARGE_STAGES_RECHARGE_FINISH = "finish";

    public static final String RECHARGE_PREPARE_STATUS_SUCCESS = "SUCCESS";
    public static final String RECHARGE_PREPARE_STATUS_FAIL = "FAIL";

    public static final String RECHARGE_TASK_STATUS_SUCCESS = "SUCCESS";
    public static final String RECHARGE_TASK_STATUS_FAIL = "FAIL";

    public static final String RECHARGE_TASK_STATUS_PENDING = "pending"; //待处理

    public static final String RECHARGE_NOTIFY_STATUS_SUCCESS = "SUCCESS";
    public static final String RECHARGE_NOTIFY_STATUS_FAIL = "FAIL";


}
