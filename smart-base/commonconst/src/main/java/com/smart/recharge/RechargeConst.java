package com.smart.recharge;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-25 14:55
 **/
public class RechargeConst {
    //public static final String RECHARGE_METER_UNIT_CUMETER = "cu.meter"; //立方米
    //public static final String RECHARGE_METER_UNIT_DEGREE = "degree"; //度
    //public static final String RECHARGE_METER_UNIT_L = "L"; //升
    public static final String RECHARGE_METER_UNIT_CUMETER = "立方米"; //cold
    public static final String RECHARGE_METER_UNIT_DEGREE = "度"; //high
    public static final String RECHARGE_METER_UNIT_L = "升"; //hot
    public static final String RECHARGE_METER_UNIT_YUAN = "元"; //elec


    public static final String RECHARGE_ELEC_DATAS_FD_TIMES = "times";
    public static final String RECHARGE_ELEC_DATAS_FD_LEFT = "leftMoney";
    public static final String RECHARGE_HIGH_DATAS_FD_TIMES = "rechargeTimes";
    public static final String RECHARGE_ELEC_DATAS_FD_BALANCE = "balance";
    public static final String RECHARGE_HIGH_DATAS_FD_BALANCE = "capacity";
    public static final String RECHARGE_COLD_DATAS_FD_BALANCE = "leftAmount";
    public static final String RECHARGE_ELEC_DATAS_FD_NOTIFYURL = "notifyUrl";
    public static final String RECHARGE_ELEC_DATAS_FD_SIGN = "sign";
    public static final String RECHARGE_NOTIFY_ANSYNC = "ansync";
    public static final String RECHARGE_NOTIFY_SYNC = "sync";

    public static final int RECHARGE_NOTIFY_SYNC_0 = 0;
    public static final int RECHARGE_NOTIFY_ASYNC_1 = 1;

}
