package com.smart.recharge;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-25 11:41
 **/
public class UserType {
    public static final String USER_TYPE_STUDENT = "student";
    public static final String USER_TYPE_TEACHER = "teacher";
    public static final String USER_TYPE_MERCHENT = "merchent";
}
