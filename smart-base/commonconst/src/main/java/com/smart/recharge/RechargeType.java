package com.smart.recharge;

/**
 * @program: plugin
 * @description:
 * @author: admin
 * @create: 2019-07-25 09:57
 **/
public class RechargeType {
    /*充值类型：
    recharge
    clear   //清零
    switch  //换表
    offline //线下充值
    donate  //赠送
    */
    public static final String RECHARGE_TYPE_APP ="app";
    public static final String RECHARGE_TYPE_CLEAR = "clear";
    public static final String RECHARGE_TYPE_SWITCH = "switch";
    public static final String RECHARGE_TYPE_OFFINE = "offline";
    public static final String RECHARGE_TYPE_DONATE = "donate";
}
