package com.smart;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-05-16 14:15
 **/
public class MessageTopic {
    public static final String MESSAGE_TOPIC_MAINTANCE = "maintance";
    public static final String MESSAGE_TOPIC_CONFIG = "config";
    public static final String MESSAGE_TOPIC_MONITOR = "monitor";
    public static final String MESSAGE_TOPIC_DBWATCH = "maxwell";
    public static final String MESSAGE_TOPIC_EVENT = "eventreport";
    public static final String MESSAGE_TOPIC_STATE = "statereport";

    //和插件层的数据交换
    public static final String MESSAGE_TOPIC_PLUGINDATA = "plugindata";
}
