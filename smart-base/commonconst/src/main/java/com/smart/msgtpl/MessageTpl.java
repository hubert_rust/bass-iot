package com.smart.msgtpl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.smart.command.CommandCode;
import com.smart.request.BassHardwardRequest;
import org.springframework.util.IdGenerator;

import java.util.List;
import java.util.Map;

/**
 * @program: smart-service
 * @description:
 * @author: admin
 * @create: 2019-07-11 08:49
 **/
public class MessageTpl {

    public static BassHardwardRequest getSyncTimeTpl(String cmdCode,
                                                     String cmdType,
                                                     String plugIn,
                                                     String commandOrder,
                                                     String conAddr,
                                                     String subAddr,
                                                     String bps,
                                                     String commPort) {
        BassHardwardRequest request = new BassHardwardRequest();
        request.setServiceType("FUNC");
        request.setServiceId(plugIn);
        request.setCommandOrder(commandOrder);
        request.setHubAddress(conAddr);
        request.setSubAddress(subAddr);
        request.setCmdCode(cmdCode);
        request.setCmdType(cmdType);
        request.setSubType("func");
        request.setIsRetSendBytes(1);
        request.setIsRetRespBytes(1);


        Map<String, Object> map = Maps.newHashMap();
        map.put("hubAddress", conAddr);
        map.put("meterAddress", subAddr);
        map.put("bps", bps);
        map.put("commPort", commPort);
        List<Map<String, Object>> list = Lists.newArrayList();
        list.add(map);
        request.setEntitys(list);


        return request;
    }


    public static BassHardwardRequest getMeterPrepareTpl(String meterType,
                                                      String cmdCode,
                                                      String plugIn,
                                                      String commandOrder,
                                                      String conAddr,
                                                      String subAddr) {
        BassHardwardRequest request = new BassHardwardRequest();
        request.setServiceType("FUNC");
        request.setServiceId(plugIn);
        request.setCommandOrder(commandOrder);
        request.setHubAddress(conAddr);
        request.setSubAddress(subAddr);
        request.setCmdCode(cmdCode);

        Map<String, Object> map = Maps.newHashMap();
        map.put("hubAddress", conAddr);
        map.put("meterAddress", subAddr);
        if (meterType.equals(CommandCode.HUB_TYPE_ELECMETER)) {
            map.put("bps", "6");
            map.put("commPort", "2");

        }
        else if (meterType.equals(CommandCode.HUB_TYPE_COLDMETER)) {
            map.put("commPort", "2");
            map.put("bps", "3");
        }
        else if (meterType.equals(CommandCode.HUB_TYPE_HIGHMETER)) {
            map.put("commPort", "2");
            map.put("bps", "3");
        }
        else if (meterType.equals(CommandCode.HUB_TYPE_HOTMETER)) {
            map.put("commPort", "2");
            map.put("bps", "3");
        }

        request.setCmdType(meterType);
        request.setSubType("func");
        request.setIsRetSendBytes(0);
        request.setIsRetRespBytes(0);

        List<Map<String, Object>> list = Lists.newArrayList();
        list.add(map);
        request.setEntitys(list);
        return request;
    }
    public static BassHardwardRequest getMeterReadTpl(String meterType,
                                                     String cmdCode,
                                                     String plugIn,
                                                     String commandOrder,
                                                     String conAddr,
                                                     String subAddr) {
        BassHardwardRequest request = new BassHardwardRequest();
        request.setServiceType("FUNC");
        request.setServiceId(plugIn);
        request.setCommandOrder(commandOrder);
        request.setHubAddress(conAddr);
        request.setSubAddress(subAddr);
        request.setCmdCode(cmdCode);

        Map<String, Object> map = Maps.newHashMap();
        map.put("hubAddress", conAddr);
        map.put("meterAddress", subAddr);
        if (meterType.equals(CommandCode.HUB_TYPE_ELECMETER)) {
            map.put("bps", "6");
            map.put("commPort", "2");

        }
        else if (meterType.equals(CommandCode.HUB_TYPE_COLDMETER)) {
            map.put("commPort", "2");
            map.put("bps", "3");
        }
        else if (meterType.equals(CommandCode.HUB_TYPE_HIGHMETER)) {
            map.put("commPort", "2");
            map.put("bps", "3");
        }
        else if (meterType.equals(CommandCode.HUB_TYPE_HOTMETER)) {
            map.put("commPort", "2");
            map.put("bps", "3");
        }

        request.setCmdType(meterType);
        request.setSubType("func");
        request.setIsRetSendBytes(1);
        request.setIsRetRespBytes(1);

        List<Map<String, Object>> list = Lists.newArrayList();
        list.add(map);
        request.setEntitys(list);
        return request;
    }
}
